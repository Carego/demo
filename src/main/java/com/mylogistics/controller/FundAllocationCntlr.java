package com.mylogistics.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.itextpdf.text.FontFactory;
import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.BranchStockLeafDetDAO;
import com.mylogistics.DAO.BrokerDAO;
import com.mylogistics.DAO.ChallanDAO;
import com.mylogistics.DAO.CnmtDAO;
import com.mylogistics.DAO.FundAllocationDao;
import com.mylogistics.DAO.LhpvStatusDAO;
import com.mylogistics.DAO.OwnerDAO;
import com.mylogistics.DAO.PetroCardDAO;
import com.mylogistics.DAO.VehicleVendorDAO;
//import com.mylogistics.model.AllowAdvance;
import com.mylogistics.model.Branch;
import com.mylogistics.model.BranchStockLeafDet;
import com.mylogistics.model.Broker;
import com.mylogistics.model.Challan;
import com.mylogistics.model.Cnmt;
import com.mylogistics.model.FundAllocation;
import com.mylogistics.model.FundAllocationDetails;
import com.mylogistics.model.Owner;
import com.mylogistics.model.PetroCard;
import com.mylogistics.model.PetroCardAdv;
import com.mylogistics.model.User;
import com.mylogistics.model.VehicleVendorMstr;
import com.mylogistics.model.lhpv.LhpvAdv;
import com.mylogistics.model.lhpv.LhpvStatus;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.FundAllocationService;
import com.mylogistics.services.mail.SmtpMailSender;

@Controller
public class FundAllocationCntlr {

	@Autowired
	BranchDAO branchDao;

	@Autowired
	FundAllocationDao fundAllocationDao;

	@Autowired
	HttpSession session;
	
	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	CnmtDAO cnmtDao;
	
	@Autowired
	ChallanDAO challanDao;
	
	@Autowired
	BrokerDAO brokerDAO;
	
	@Autowired
	LhpvStatusDAO  lhpvStatusDAO;
	
	@Autowired
	BranchStockLeafDetDAO brsLFDao;
	
	@Autowired
	VehicleVendorDAO vehicleVendorDAO;
	
	@Autowired
	private OwnerDAO ownerDAO;


	//List<FundAllocationDetails> fundAllocationDetList = new ArrayList<>();

	@RequestMapping(value = "/getAllOpenBranches", method = RequestMethod.POST)
	public @ResponseBody Object getAllBranchesList() {
		System.out.println("In open Branch ");
		Map<String, Object> resultMap = new HashMap<>();
		try {
			List<Branch> branchList = branchDao.getAllActiveBranches();
			if (!branchList.isEmpty()) {
				resultMap.put("branchList", branchList);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return resultMap;
	}
	
	@RequestMapping(value = "/verifyCnmtFrAdv", method = RequestMethod.POST)
	public @ResponseBody Object verifyCnmtFrAdv(@RequestBody String cnmtCode) {
		System.out.println("In verifyCnmtFrAdv() "+cnmtCode);
		Map<String, Object> resultMap = new HashMap<>();
		BranchStockLeafDet brs=brsLFDao.getCodeByCode(cnmtCode, "cnmt");
		
		if(brs == null) {
			Cnmt cnmt=cnmtDao.getCnmtFrFndAlctn(cnmtCode);
			if(cnmt != null) {
				resultMap.put("result", "success");
				resultMap.put("cnmtCode", cnmt.getCnmtCode());
			}else {
				resultMap.put("result", "error");
			}
			
		}else {
			resultMap.put("result", "success");
			resultMap.put("cnmtCode", brs.getBrsLeafDetSNo());
		}

		return resultMap;
	}
	
	
	@RequestMapping(value = "/verifyChlnFrAdv", method = RequestMethod.POST)
	public @ResponseBody Object verifyChlnFrAdv(@RequestBody String chlnCode) {
		System.out.println("In verifyChlnFrAdv() ");
		Map<String, Object> resultMap = new HashMap<>();
		BranchStockLeafDet brs=brsLFDao.getCodeByCode(chlnCode, "chln");
		
		if(brs == null) {
			resultMap=challanDao.getChlnDetFrAdvPay(chlnCode);
		}else {
			resultMap.put("result", "success");
			resultMap.put("chlnCode", brs.getBrsLeafDetSNo());
			resultMap.put("chln", null);
		}

		return resultMap;
	}
	
	@RequestMapping(value = "/verifyDuplicateChlnFrAdv", method = RequestMethod.POST)
	public @ResponseBody Object verifyDuplicateChlnFrAdv(@RequestBody String chlnCode) {
		System.out.println("In verifyDuplicateChlnFrAdv() ");
		Map<String, Object> resultMap = new HashMap<>();
			boolean b=fundAllocationDao.checkChallanNo(chlnCode);
		if(b) {
			resultMap.put("result", "success");
		}else {
			resultMap.put("result", "error");
		}

		return resultMap;
	}

	
	@RequestMapping(value = "/verifyDuplicateChlnFrAdvPetro", method = RequestMethod.POST)
	public @ResponseBody Object verifyDuplicateChlnFrAdvPetro(@RequestBody String chlnCode) {
		System.out.println("In verifyDuplicateChlnFrAdvPetro() ");
		Map<String, Object> resultMap = new HashMap<>();
			boolean b=fundAllocationDao.checkChallanNoInPetro(chlnCode);
		if(b) {
			resultMap.put("result", "success");
		}else {
			resultMap.put("result", "error");
		}

		return resultMap;
	}
	
	/*@RequestMapping(value = "/getAlwdVehicleFrAdv", method = RequestMethod.POST)
	public @ResponseBody Object getAlwdVehicleFrAdv(@RequestBody Map<String, String> reqMap) {
		System.out.println("In getAlwdVehicleFrAdv() ");
		Map<String, Object> resultMap = new HashMap<>();
		
		
		AllowAdvance aa=fundAllocationDao.getAlwdVehclFrAdv(reqMap);
		
		if(aa==null) {
			resultMap.put("result", "error");
		}else{
			resultMap.put("result", "success");
			resultMap.put("isAlwd", aa.isAllow());
		}
		

		return resultMap;
	}*/
	
	/*@RequestMapping(value = "/sendVehclAdvAlwReq", method = RequestMethod.POST)
	public @ResponseBody Object sendVehclAdvAlwReq(@RequestBody Map<String, String> reqMap) {
		System.out.println("In sendVehclAdvAlwReq() ");
		Map<String, Object> resultMap = new HashMap<>();
		int i=fundAllocationDao.saveVehclAdvAlwReq(reqMap);
		
		if(i>0) {
			resultMap.put("result", "success");
		}else {
			resultMap.put("result", "error");
		}

		return resultMap;
	}*/

	
	
	@RequestMapping(value = "/getVehclDetFrAdv", method = RequestMethod.POST)
	public @ResponseBody Object getVehclDetFrAdv(@RequestBody String vehNo){
		//logger.info("Enter into /getVehclDetFrAdv : VehNo = "+vehNo);		
		Map<String,Object> map = new HashMap<String,Object>();
		if(vehNo != null){
			VehicleVendorMstr veh = vehicleVendorDAO.getVehByVehNo(vehNo);
			if(veh != null){
				
				
				map.put("own", veh.getOwner());
				map.put("brk", veh.getBroker());
											
				map.put("vehNo",veh.getVvRcNo());
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			//	logger.info("Success....");
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		//		logger.info("Error.....");
			}
		}else{
		//	logger.info("Error........");
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		//logger.info("Exit from /selectVehFC()");
		return map;
	}
	
	
	
	@RequestMapping(value = "/getBrokerForAdvByBrkName", method = RequestMethod.POST)
	public @ResponseBody Object getBrokerForAdvByBrkName(@RequestBody String brkName) {
		
		Map<String, Object> map = new HashMap<String, Object>();		
		List<Broker> brkList = brokerDAO.getBrkFrAdvByName(brkName);
		if (!brkList.isEmpty()) {	 
			map.put("list", brkList);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}
	

	@RequestMapping(value = "/saveFundAllocation", method = RequestMethod.POST)
	public @ResponseBody
	Object saveFundAllocation(
			@RequestBody FundAllocationService allocationService) {
		System.out.println("************* hello *********************");

		Map<String, Object> resposneMap = new HashMap<>();

		User user = (User) session.getAttribute("currentUser");

		FundAllocation fundAllocation = allocationService.getAllocation();
		List<FundAllocationDetails> allocationDetails = allocationService
				.getFadList();

		fundAllocation.setAmount(fundAllocation.getInstrumentAmount());
		fundAllocation.setFundAllocationDetails(allocationDetails);
		fundAllocation.setbCode(user.getUserBranchCode());
		fundAllocation.setUserCode(user.getUserCode());
		fundAllocation.setIsGenerated("No");
		fundAllocation.setChequeTmDate(Calendar.getInstance());
		
		String ifscCode = fundAllocation.getIfscCode();
		if(fundAllocation.getStaffCode() != null && fundAllocation.getTransactionType().equalsIgnoreCase("ATM")){
			fundAllocation.setTransactionType("ATM");
		}else if (ifscCode.substring(0, 4).equalsIgnoreCase("HDFC")) {
			fundAllocation.setTransactionType("I");
		} else {
			if (fundAllocation.getInstrumentAmount() > 200000) {

				fundAllocation.setTransactionType("R");
			} else {
				fundAllocation.setTransactionType("N");
			}
		}

		int i = fundAllocationDao.saveFundAllocation(fundAllocation);
		if (i > 0) {
			resposneMap.put("value", "success");
		} else
			resposneMap.put("value", "Error");

		return resposneMap;
	}

	/*@RequestMapping(value = "/generateFundAllocation", method = RequestMethod.POST)
	public @ResponseBody
	Object generateFundAllocationDetails() {
		Map<String, Object> resultMap = new HashMap<>();
		List<FundAllocation> allocations = fundAllocationDao
				.getCurrentDateFundAllocationList();
		System.out.println("Size of List is " + allocations.size());

		if (allocations.size() > 0) {
			System.out.println("bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb");
			System.out.println(allocations.get(0).getFundAllocationDetails());
			resultMap.put("msg", "Success");
			resultMap.put("faList", allocations);
			return resultMap;
		} else {
			resultMap.put("msg", "No Fund Details for Current Date");
			return resultMap;
		}

	}*/
	
	
	@RequestMapping(value = "/generateFundAllocation", method = RequestMethod.POST)
	public @ResponseBody
	Object generateFundAllocationDetails() {
		Map<String, Object> resultMap = new HashMap<>();
		List<FundAllocation> allocations = fundAllocationDao
				.getCurrentDateFundAllocationList();
		System.out.println("Size of List is " + allocations.size());

		if (allocations.size() > 0) {
			System.out.println("bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb");
			System.out.println(allocations.get(0).getFundAllocationDetails());
			
			List<Map<String,Object> > faListMap=new ArrayList<>();
			
			for(int i=0;i<allocations.size();i++) {
				
				Map<String, Object> map=new HashMap<>();
				
				map.put("fa", allocations.get(i));
				if(!allocations.get(i).getFundAllocationDetails().isEmpty())
					map.put("fd", allocations.get(i).getFundAllocationDetails().get(0));
				
				faListMap.add(map);
				
			}
			
			resultMap.put("msg", "Success");
			resultMap.put("faList", allocations);
			resultMap.put("mapList", faListMap);
			return resultMap;
		} else {
			resultMap.put("msg", "No Fund Details for Current Date");
			return resultMap;
		}

	}
	
	
	
	@RequestMapping(value = "/rejectFundAllocation", method = RequestMethod.POST)
	public @ResponseBody
	Object rejectFundAllocation(@RequestBody FundAllocation fundAllocation) {
		Map<String, Object> responseMap = new HashMap<>();
		
		System.out.println("faId="+fundAllocation.getFaId());
		int i = fundAllocationDao.rejectFundAllocation(fundAllocation);
		if (i > 0) {
			responseMap.put("result", "success");
			responseMap.put("msg", "Rejected");
		} else {
			responseMap.put("result", "error");
			responseMap.put("msg", "There is some problem");
		}
			

		return responseMap;
	}
	
	

	@RequestMapping(value = "/updateFundAllocation", method = RequestMethod.POST)
	public @ResponseBody
	Object updateFundAllocation(@RequestBody FundAllocationService fundAllMap) {
		Map<String, Object> responseMap = new HashMap<>();
		User user=(User) session.getAttribute("currentUser");
		Session session=this.sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		FundAllocation fundAllocation=(FundAllocation) session.get(FundAllocation.class, fundAllMap.getAllocation().getFaId());
		FundAllocationDetails fundAllocationDetails=fundAllMap.getFd();
		
		List<String> phoneNoList=new ArrayList<>();
		String bankFa=null;
		
		if(fundAllMap.getBankFaCode() != null || !fundAllMap.getBankFaCode().equalsIgnoreCase(""))
			bankFa=fundAllMap.getBankFaCode();
		else
			bankFa="0700003";
		
        SmtpMailSender mailSend=new SmtpMailSender();
		
		if(fundAllocationDetails.getOwnCode()!=null && !fundAllocationDetails.getOwnCode().equalsIgnoreCase("")) {
			Owner owner=ownerDAO.getOwnByCode(fundAllocationDetails.getOwnCode(),session);
			
			if(owner !=null) {
				if(owner.getOwnPhNoList()!=null && !owner.getOwnPhNoList().isEmpty()) {
					for(String phNo:owner.getOwnPhNoList()) {
						phoneNoList.add(phNo);
					}
				}
			}
			
		}
		if(fundAllocationDetails.getBrkCode()!=null && !fundAllocationDetails.getBrkCode().equalsIgnoreCase("")) {
			
			Broker broker=brokerDAO.getBrkByCode(fundAllocationDetails.getBrkCode(),session);
			
			if(broker !=null) {
				if(broker.getBrkPhNoList()!=null && !broker.getBrkPhNoList().isEmpty()) {
					for(String phNo:broker.getBrkPhNoList()) {
						phoneNoList.add(phNo);
					}
				}
			}
			
		}
		
		fundAllocation.setApproved(true);
		fundAllocation.setRejected(false);
		System.out.println("Fund Allocation="+fundAllocation.getFaId());
		try {
			Date d=new Date();
			Long dt=d.getTime();
			java.sql.Date date=new java.sql.Date(dt);
			LhpvStatus ls=lhpvStatusDAO.getLssByDt("1", date);
			//PetroCard petroCard=fundAllocationDao.getCardByCardNo(petroAllocation.getCardNo());
			List<Challan> chlnList=challanDao.getChallanByCode(fundAllocationDetails.getChallanNo());
			if(chlnList.isEmpty()) {
				
				BranchStockLeafDet brs=brsLFDao.getCodeByCodeFrLa(fundAllocationDetails.getChallanNo(), "chln");
				
				if(brs != null) {
					Challan challan=new Challan();
					challan.setbCode(fundAllocation.getbCode());
					challan.setBranchCode(fundAllocation.getbCode());
					challan.setCancel(false);
					challan.setChlnAdvance(String.valueOf(fundAllocationDetails.getLryAmount()));
					challan.setChlnArId(-1);
					challan.setChlnBalance(fundAllocationDetails.getChlnFreight()-fundAllocationDetails.getLryAmount());
					challan.setChlnBrRate(String.valueOf(fundAllocationDetails.getLryRate()));
					challan.setChlnCode(fundAllocationDetails.getChallanNo());
					challan.setChlnDt(fundAllocationDetails.getChlnDt());
					challan.setChlnFreight(String.valueOf(fundAllocationDetails.getChlnFreight()));
					challan.setChlnFromStn(fundAllocationDetails.getFromStCode());
					challan.setChlnLryNo(fundAllocationDetails.getVehicleNo());
					challan.setChlnLryRate(String.valueOf(fundAllocationDetails.getLryRate()));
					challan.setChlnPayAt("1");
					challan.setChlnRemAdv(fundAllocationDetails.getLryAmount()-fundAllocation.getAmount()-fundAllocation.getTdsAmt());
					challan.setChlnRemBal(fundAllocationDetails.getChlnFreight()-fundAllocationDetails.getLryAmount());
					challan.setChlnToStn(fundAllocationDetails.getToStCode());
					challan.setChlnTotalFreight(fundAllocationDetails.getChlnFreight());
					challan.setUserCode(user.getUserCode());
					challan.setAccountNo(fundAllocation.getBeneficiaryAccountNo());
					challan.setBankName(fundAllocation.getBeneBnkName());
					challan.setIfscCode(fundAllocation.getIfscCode());
					challan.setPayeeName(fundAllocation.getBeneficiaryName());
					
					int chlnId=challanDao.saveChallan(challan,session);
					if(chlnId>0) {
						if(ls !=null) {
							
							LhpvAdv la=new LhpvAdv();
							la.setbCode("1");
							la.setChallan(challan);
							la.setLaBankCode(bankFa);
							la.setLaBrhCode("0100001");
							la.setLaDt(date);
							la.setLaFinalTot(fundAllocation.getAmount());
							la.setLaLryAdvP(fundAllocation.getAmount()+fundAllocation.getTdsAmt());
							la.setLaPayMethod('R');
							la.setLaTdsR(fundAllocation.getTdsAmt());
							la.setLaTotPayAmt(fundAllocation.getAmount()+fundAllocation.getTdsAmt());
							la.setLaTotRcvrAmt(fundAllocation.getTdsAmt());
							la.setLaNo(1);
							la.setLhpvStatus(ls);
							//la.setPayToStf(petroAllocation.getCardType());
							//la.setStafCode(petroCard.getCardFaCode());
							la.setUserCode(user.getUserCode());
							
							int laId=lhpvStatusDAO.saveLhpvAdv(la,session);
							
							fundAllocationDao.updateFundAllocation(fundAllocation,session);
							
							session.flush();
							session.clear();
							transaction.commit();
							responseMap.put("result", "success");
						}else {
							LhpvAdv la=new LhpvAdv();
							la.setbCode("1");
							la.setChallan(challan);
							la.setLaBankCode(bankFa);
							la.setLaBrhCode("0100001");
							la.setLaDt(date);
							la.setLaFinalTot(fundAllocation.getAmount());
							la.setLaLryAdvP(fundAllocation.getAmount()+fundAllocation.getTdsAmt());
							la.setLaPayMethod('R');
							la.setLaTdsR(fundAllocation.getTdsAmt());
							la.setLaTotPayAmt(fundAllocation.getAmount()+fundAllocation.getTdsAmt());
							la.setLaTotRcvrAmt(fundAllocation.getTdsAmt());
							la.setLaNo(1);
							//la.setLhpvStatus(ls);
							//la.setPayToStf(petroAllocation.getCardType());
							//la.setStafCode(petroCard.getCardFaCode());
							la.setUserCode(user.getUserCode());
							
							int laId=lhpvStatusDAO.saveLhpvAdv(la,session);
							
							fundAllocationDao.updateFundAllocation(fundAllocation,session);
							
							session.flush();
							session.clear();
							transaction.commit();
							responseMap.put("result", "success");
						}
						
						//TODO
						
						 String msg="Dear Vendor, an advance payment of amount Rs. "+fundAllocation.getAmount()+" has been made to your account no. XXX"+fundAllocation.getBeneficiaryAccountNo().substring(fundAllocation.getBeneficiaryAccountNo().length()-4)+" for the vehicle no. "+fundAllocationDetails.getVehicleNo()+". Regards CareGo Logistics.";
					        if(!phoneNoList.isEmpty()) {
					        	mailSend.sendSMS(phoneNoList,msg);
					        }
						
					}else {
						responseMap.put("msg", "challan not saved");
						responseMap.put("result", "error");
					}
					
				}else {
					responseMap.put("msg", "Stationary not found");
					responseMap.put("result", "error");
				}
				
				
			}else {
				
				Challan chln=chlnList.get(0);
				if(chln.getChlnRemAdv()-fundAllocation.getAmount()-fundAllocation.getTdsAmt()>=0) {
					chln.setChlnRemAdv(chln.getChlnRemAdv()-fundAllocation.getAmount()-fundAllocation.getTdsAmt());
					chln.setAccountNo(fundAllocation.getBeneficiaryAccountNo());
					chln.setBankName(fundAllocation.getBeneBnkName());
					chln.setIfscCode(fundAllocation.getIfscCode());
					chln.setPayeeName(fundAllocation.getBeneficiaryName());
					
					challanDao.updateChallan(chln,session);
					

					if(ls !=null) {
						
						LhpvAdv la=new LhpvAdv();
						la.setbCode("1");
						la.setChallan(chln);
						la.setLaBankCode(bankFa);
						la.setLaBrhCode("0100001");
						la.setLaDt(date);
						la.setLaFinalTot(fundAllocation.getAmount());
						la.setLaLryAdvP(fundAllocation.getAmount()+fundAllocation.getTdsAmt());
						la.setLaPayMethod('R');
						la.setLaTdsR(fundAllocation.getTdsAmt());
						la.setLaTotPayAmt(fundAllocation.getAmount()+fundAllocation.getTdsAmt());
						la.setLaTotRcvrAmt(fundAllocation.getTdsAmt());
						la.setLaNo(1);
						la.setLhpvStatus(ls);
						//la.setPayToStf(petroAllocation.getCardType());
						//la.setStafCode(petroCard.getCardFaCode());
						la.setUserCode(user.getUserCode());
						
						int laId=lhpvStatusDAO.saveLhpvAdv(la,session);
						
						fundAllocationDao.updateFundAllocation(fundAllocation,session);
						
						session.flush();
						session.clear();
						transaction.commit();
						responseMap.put("result", "success");
						
					}else {
						LhpvAdv la=new LhpvAdv();
						la.setbCode("1");
						la.setChallan(chln);
						la.setLaBankCode(bankFa);
						la.setLaBrhCode("0100001");
						la.setLaDt(date);
						la.setLaFinalTot(fundAllocation.getAmount());
						la.setLaLryAdvP(fundAllocation.getAmount()+fundAllocation.getTdsAmt());
						la.setLaPayMethod('R');
						la.setLaTdsR(fundAllocation.getTdsAmt());
						la.setLaTotPayAmt(fundAllocation.getAmount()+fundAllocation.getTdsAmt());
						la.setLaTotRcvrAmt(fundAllocation.getTdsAmt());
						la.setLaNo(1);
						//la.setLhpvStatus(ls);
						//la.setPayToStf(petroAllocation.getCardType());
						//la.setStafCode(petroCard.getCardFaCode());
						la.setUserCode(user.getUserCode());
						
						int laId=lhpvStatusDAO.saveLhpvAdv(la,session);
						
						fundAllocationDao.updateFundAllocation(fundAllocation,session);
						
						session.flush();
						session.clear();
						transaction.commit();
						responseMap.put("result", "success");
					}
				//TODO
					 String msg="Dear Vendor, an advance payment of amount Rs. "+fundAllocation.getAmount()+" has been made to your account no. XXX"+fundAllocation.getBeneficiaryAccountNo().substring(fundAllocation.getBeneficiaryAccountNo().length()-4)+" for the vehicle no. "+fundAllocationDetails.getVehicleNo()+". Regards CareGo Logistics.";
				        if(!phoneNoList.isEmpty()) {
				        	mailSend.sendSMS(phoneNoList,msg);
				        }
				}else {
					responseMap.put("result", "error");
					responseMap.put("msg", "Amount can't be greater than pending advance");
				}
				
			}
			
			
		}catch(Exception e) {
			e.printStackTrace();
			responseMap.put("result", "exc");
			responseMap.put("msg", e);
			transaction.rollback();
		}finally {
			session.close();
		}

		return responseMap;
	}
	
	/*@RequestMapping(value = "/updateFundAllocation", method = RequestMethod.POST)
	public @ResponseBody
	Object updateFundAllocation(@RequestBody FundAllocation fundAllocation) {
		Map<String, Object> responseMap = new HashMap<>();
		fundAllocation.setAmount(fundAllocation.getInstrumentAmount());
		int i = fundAllocationDao.updateFundAllocation(fundAllocation);
		if (i > 0) {
			responseMap.put("msg", "Updated Successfully");
		} else
			responseMap.put("msg", "Error");

		return responseMap;
	}*/

	@RequestMapping(value = "/downloadFundAllExcel", method = RequestMethod.POST)
	public void downloadFundAllExcel(HttpServletResponse response) {
		System.out.println("In Excel Generator method");
		List<FundAllocation> fundAllocationList = fundAllocationDao
				.getCurrentDateFundAllocationListFrExcel();
		if (!fundAllocationList.isEmpty()) {
			System.out.println("Here........................................");
			XSSFWorkbook workbook = new XSSFWorkbook();

			// Header font
			XSSFFont fontHeader = workbook.createFont();
			fontHeader.setFontHeightInPoints((short) 11);
			fontHeader.setColor(IndexedColors.WHITE.getIndex());
			fontHeader.setFontName(FontFactory.COURIER_BOLD);
			fontHeader.setBold(true);

			// Header style
			XSSFCellStyle styleHeader = workbook.createCellStyle();
			styleHeader.setFont(fontHeader);
			styleHeader.setAlignment(XSSFCellStyle.ALIGN_CENTER);
			styleHeader.setFillForegroundColor(IndexedColors.BLUE_GREY
					.getIndex());
			styleHeader.setFillPattern(CellStyle.SOLID_FOREGROUND);
			styleHeader.setBorderTop(XSSFCellStyle.BORDER_MEDIUM);
			styleHeader.setBorderRight(XSSFCellStyle.BORDER_MEDIUM);
			styleHeader.setBorderBottom(XSSFCellStyle.BORDER_MEDIUM);
			styleHeader.setBorderLeft(XSSFCellStyle.BORDER_MEDIUM);

			//
			XSSFCellStyle styleData = workbook.createCellStyle();
			styleData.setAlignment(XSSFCellStyle.ALIGN_CENTER);

			XSSFSheet sheet = workbook.createSheet("Fund Allocation");
			sheet.setColumnWidth(0, 20 * 256);
			sheet.setColumnWidth(1, 25 * 256);
			sheet.setColumnWidth(2, 35 * 256);
			sheet.setColumnWidth(3, 30 * 256);
			sheet.setColumnWidth(4, 30 * 256);
			sheet.setColumnWidth(5, 30 * 256);
			sheet.setColumnWidth(6, 30 * 256);
			sheet.setColumnWidth(7, 30 * 256);
			sheet.setColumnWidth(8, 30 * 256);
			sheet.setColumnWidth(9, 30 * 256);
			sheet.setColumnWidth(10, 30 * 256);
			sheet.setColumnWidth(11, 30 * 256);
			sheet.setColumnWidth(12, 40 * 256);
			sheet.setColumnWidth(13, 40 * 256);
			sheet.setColumnWidth(14, 30 * 256);
			sheet.setColumnWidth(15, 30 * 256);
			sheet.setColumnWidth(16, 30 * 256);
			sheet.setColumnWidth(17, 30 * 256);
			sheet.setColumnWidth(18, 30 * 256);
			sheet.setColumnWidth(19, 30 * 256);
			sheet.setColumnWidth(20, 30 * 256);
			sheet.setColumnWidth(21, 30 * 256);
			sheet.setColumnWidth(22, 30 * 256);
			sheet.setColumnWidth(23, 30 * 256);
			sheet.setColumnWidth(24, 30 * 256);
			sheet.setColumnWidth(25, 30 * 256);
			sheet.setColumnWidth(26, 30 * 256);
			sheet.setColumnWidth(27, 30 * 256);
			sheet.setColumnWidth(28, 30 * 256);
			sheet.setColumnWidth(29, 30 * 256);
			sheet.setColumnWidth(30, 30 * 256);
			sheet.setColumnWidth(31, 30 * 256);
			sheet.setColumnWidth(32, 30 * 256);
			sheet.setColumnWidth(33, 30 * 256);
			sheet.setColumnWidth(34, 30 * 256);
			sheet.setColumnWidth(35, 30 * 256);
			sheet.setColumnWidth(36, 30 * 256);
			sheet.setColumnWidth(37, 30 * 256);
			sheet.setColumnWidth(38, 30 * 256);
			XSSFRow row = sheet.createRow(0);

			XSSFCell cell = null;
			cell = row.createCell(0);
			cell.setCellValue("Transaction Type");
			cell.setCellStyle(styleHeader);

			cell = row.createCell(1);
			cell.setCellValue("Beneficiary Code");
			cell.setCellStyle(styleHeader);

			cell = row.createCell(2);
			cell.setCellValue("Beneficiary Account Number");
			cell.setCellStyle(styleHeader);

			cell = row.createCell(3);
			cell.setCellValue("Instrument Amount");
			cell.setCellStyle(styleHeader);

			cell = row.createCell(4);
			cell.setCellValue("Beneficiary Name");
			cell.setCellStyle(styleHeader);

			cell = row.createCell(5);
			cell.setCellValue("Drawee Location");
			cell.setCellStyle(styleHeader);

			cell = row.createCell(6);
			cell.setCellValue("Print Location");
			cell.setCellStyle(styleHeader);

			cell = row.createCell(7);
			cell.setCellValue("Bene Address 1");
			cell.setCellStyle(styleHeader);

			cell = row.createCell(8);
			cell.setCellValue("Bene Address 2");
			cell.setCellStyle(styleHeader);

			cell = row.createCell(9);
			cell.setCellValue("Bene Address 3");
			cell.setCellStyle(styleHeader);

			cell = row.createCell(10);
			cell.setCellValue("Bene Address 4");
			cell.setCellStyle(styleHeader);

			cell = row.createCell(11);
			cell.setCellValue("Bene Address 5");
			cell.setCellStyle(styleHeader);

			cell = row.createCell(12);
			cell.setCellValue("Instruction Reference Number");
			cell.setCellStyle(styleHeader);

			cell = row.createCell(13);
			cell.setCellValue("Customer Reference Number");
			cell.setCellStyle(styleHeader);

			cell = row.createCell(14);
			cell.setCellValue("Payment details 1");
			cell.setCellStyle(styleHeader);

			cell = row.createCell(15);
			cell.setCellValue("Payment details 2");
			cell.setCellStyle(styleHeader);

			cell = row.createCell(16);
			cell.setCellValue("Payment details 3");
			cell.setCellStyle(styleHeader);

			cell = row.createCell(17);
			cell.setCellValue("Payment details 4");
			cell.setCellStyle(styleHeader);

			cell = row.createCell(18);
			cell.setCellValue("Payment details 5");
			cell.setCellStyle(styleHeader);

			cell = row.createCell(19);
			cell.setCellValue("Payment details 6");
			cell.setCellStyle(styleHeader);

			cell = row.createCell(20);
			cell.setCellValue("Payment details 7");
			cell.setCellStyle(styleHeader);

			cell = row.createCell(21);
			cell.setCellValue("Cheque Number");
			cell.setCellStyle(styleHeader);

			cell = row.createCell(22);
			cell.setCellValue("Chq/Tm Date");
			cell.setCellStyle(styleHeader);

			cell = row.createCell(23);
			cell.setCellValue("MICR Number");
			cell.setCellStyle(styleHeader);

			cell = row.createCell(24);
			cell.setCellValue("IFSC Code");
			cell.setCellStyle(styleHeader);

			cell = row.createCell(25);
			cell.setCellValue("Bene Bank Name");
			cell.setCellStyle(styleHeader);

			cell = row.createCell(26);
			cell.setCellValue("Bene Bank Branch Name ");
			cell.setCellStyle(styleHeader);

			cell = row.createCell(27);
			cell.setCellValue("Beneficiary email id");
			cell.setCellStyle(styleHeader);
			
			cell = row.createCell(28);
			cell.setCellValue("Staff Code");
			cell.setCellStyle(styleHeader);
			
			cell = row.createCell(29);
			cell.setCellValue("CNMT No.");
			cell.setCellStyle(styleHeader);
			
			cell = row.createCell(30);
			cell.setCellValue("Challan No.");
			cell.setCellStyle(styleHeader);
			
			cell = row.createCell(31);
			cell.setCellValue("Lorry No.");
			cell.setCellStyle(styleHeader);
			
			cell = row.createCell(32);
			cell.setCellValue("Lorry Freight");
			cell.setCellStyle(styleHeader);
			
			cell = row.createCell(33);
			cell.setCellValue("Lorry Advance");
			cell.setCellStyle(styleHeader);
			
			cell = row.createCell(34);
			cell.setCellValue("Lorry Date");
			cell.setCellStyle(styleHeader);
			
			cell = row.createCell(35);
			cell.setCellValue("From ");
			cell.setCellStyle(styleHeader);
			
			cell = row.createCell(36);
			cell.setCellValue("To");
			cell.setCellStyle(styleHeader);
			
			cell = row.createCell(37);
			cell.setCellValue("Broker Code");
			cell.setCellStyle(styleHeader);
			
			cell = row.createCell(38);
			cell.setCellValue("Owner Code");
			cell.setCellStyle(styleHeader);
			
			

			int rowToCreate = 1;
			XSSFRow dataRow = null;
			XSSFCell dataCell = null;
			for (FundAllocation fundAllocation : fundAllocationList) {
				dataRow = sheet.createRow(rowToCreate);
				dataCell = dataRow.createCell(0);
				if (fundAllocation.getTransactionType() != null) {
					dataCell.setCellValue(fundAllocation.getTransactionType());
					dataCell.setCellStyle(styleData);
				} else
					dataCell.setCellValue("");

				dataCell = dataRow.createCell(1);
				if (fundAllocation.getBeneficiaryCode() != null) {
					dataCell.setCellValue(fundAllocation.getBeneficiaryCode());
					dataCell.setCellStyle(styleData);
				} else
					dataCell.setCellValue("");

				dataCell = dataRow.createCell(2);
				if (fundAllocation.getBeneficiaryAccountNo() != null) {
					dataCell.setCellValue(fundAllocation
							.getBeneficiaryAccountNo());
					dataCell.setCellStyle(styleData);
				} else
					dataCell.setCellValue("");

				dataCell = dataRow.createCell(3);
				if (fundAllocation.getInstrumentAmount() > 0) {
					dataCell.setCellValue(fundAllocation.getInstrumentAmount());
					dataCell.setCellStyle(styleData);
				} else
					dataCell.setCellValue(0.0);

				dataCell = dataRow.createCell(4);
				if (fundAllocation.getBeneficiaryName() != null) {
					dataCell.setCellValue(fundAllocation.getBeneficiaryName());
					dataCell.setCellStyle(styleData);
				} else
					dataCell.setCellValue("");

				dataCell = dataRow.createCell(5);
				if (fundAllocation.getDraweeLocation() != null) {
					dataCell.setCellValue(fundAllocation.getDraweeLocation());
					dataCell.setCellStyle(styleData);
				} else
					dataCell.setCellValue("");

				dataCell = dataRow.createCell(6);
				if (fundAllocation.getPrintLocation() != null) {
					dataCell.setCellValue(fundAllocation.getPrintLocation());
					dataCell.setCellStyle(styleData);
				} else
					dataCell.setCellValue("");

				dataCell = dataRow.createCell(7);
				if (fundAllocation.getBeneAddress1() != null) {
					dataCell.setCellValue(fundAllocation.getBeneAddress1());
					dataCell.setCellStyle(styleData);
				} else
					dataCell.setCellValue("");

				dataCell = dataRow.createCell(8);
				if (fundAllocation.getBeneAddress2() != null) {
					dataCell.setCellValue(fundAllocation.getBeneAddress2());
					dataCell.setCellStyle(styleData);
				} else
					dataCell.setCellValue("");

				dataCell = dataRow.createCell(9);
				if (fundAllocation.getBeneAddress3() != null) {
					dataCell.setCellValue(fundAllocation.getBeneAddress3());
					dataCell.setCellStyle(styleData);
				} else
					dataCell.setCellValue("");

				dataCell = dataRow.createCell(10);
				if (fundAllocation.getBeneAddress4() != null) {
					dataCell.setCellValue(fundAllocation.getBeneAddress4());
					dataCell.setCellStyle(styleData);
				} else
					dataCell.setCellValue("");

				dataCell = dataRow.createCell(11);
				if (fundAllocation.getBeneAddress5() != null) {
					dataCell.setCellValue(fundAllocation.getBeneAddress5());
					dataCell.setCellStyle(styleData);
				} else
					dataCell.setCellValue("");

				dataCell = dataRow.createCell(12);

				if (fundAllocation.getInstructionRefNo() != null) {
					dataCell.setCellValue(fundAllocation.getInstructionRefNo());
					dataCell.setCellStyle(styleData);
				} else
					dataCell.setCellValue("");

				dataCell = dataRow.createCell(13);

				if (fundAllocation.getCustomerRefNo() != null) {
					dataCell.setCellValue(fundAllocation.getCustomerRefNo());
					dataCell.setCellStyle(styleData);
				} else
					dataCell.setCellValue("");

				dataCell = dataRow.createCell(14);

				if (fundAllocation.getPaymentDetail1() != null) {
					dataCell.setCellValue(fundAllocation.getPaymentDetail1());
					dataCell.setCellStyle(styleData);
				} else
					dataCell.setCellValue("");

				dataCell = dataRow.createCell(15);

				if (fundAllocation.getPaymentDetail2() != null) {
					dataCell.setCellValue(fundAllocation.getPaymentDetail2());
					dataCell.setCellStyle(styleData);
				} else
					dataCell.setCellValue("");

				dataCell = dataRow.createCell(16);
				if (fundAllocation.getPaymentDetail3() != null) {
					dataCell.setCellValue(fundAllocation.getPaymentDetail3());
					dataCell.setCellStyle(styleData);
				} else
					dataCell.setCellValue("");

				dataCell = dataRow.createCell(17);
				if (fundAllocation.getPaymentDetail4() != null) {
					dataCell.setCellValue(fundAllocation.getPaymentDetail4());
					dataCell.setCellStyle(styleData);
				} else
					dataCell.setCellValue("");

				dataCell = dataRow.createCell(18);
				if (fundAllocation.getPaymentDetail5() != null) {
					dataCell.setCellValue(fundAllocation.getPaymentDetail5());
					dataCell.setCellStyle(styleData);
				} else
					dataCell.setCellValue("");

				dataCell = dataRow.createCell(19);
				if (fundAllocation.getPaymentDetail6() != null) {
					dataCell.setCellValue(fundAllocation.getPaymentDetail6());
					dataCell.setCellStyle(styleData);
				} else
					dataCell.setCellValue("");

				dataCell = dataRow.createCell(20);
				if (fundAllocation.getPaymentDetail7() != null) {
					dataCell.setCellValue(fundAllocation.getPaymentDetail7());
					dataCell.setCellStyle(styleData);
				} else
					dataCell.setCellValue("");

				dataCell = dataRow.createCell(21);
				if (fundAllocation.getChequeNo() != null) {
					dataCell.setCellValue(fundAllocation.getChequeNo());
					dataCell.setCellStyle(styleData);
				} else
					dataCell.setCellValue("");

				dataCell = dataRow.createCell(22);
				if (fundAllocation.getChequeTmDate() != null) {
					Date date = fundAllocation.getChequeTmDate().getTime();
					System.out.println("Date " + date);
					SimpleDateFormat dateFormat = new SimpleDateFormat(
							"dd/MM/yyyy");
					String string = dateFormat.format(date);
					dataCell.setCellValue(string);
					dataCell.setCellStyle(styleData);
				} else
					dataCell.setCellValue("");

				dataCell = dataRow.createCell(23);
				if (fundAllocation.getMicrNo() != null) {
					dataCell.setCellValue(fundAllocation.getMicrNo());
					dataCell.setCellStyle(styleData);
				} else
					dataCell.setCellValue("");

				dataCell = dataRow.createCell(24);
				if (fundAllocation.getIfscCode() != null) {
					dataCell.setCellValue(fundAllocation.getIfscCode());
					dataCell.setCellStyle(styleData);
				} else
					dataCell.setCellValue("");

				dataCell = dataRow.createCell(25);
				if (fundAllocation.getBeneBnkName() != null) {
					dataCell.setCellValue(fundAllocation.getBeneBnkName());
					dataCell.setCellStyle(styleData);
				} else
					dataCell.setCellValue("");

				dataCell = dataRow.createCell(26);
				if (fundAllocation.getBeneBnkBranchName() != null) {
					dataCell.setCellValue(fundAllocation.getBeneBnkBranchName());
					dataCell.setCellStyle(styleData);
				} else
					dataCell.setCellValue("");

				dataCell = dataRow.createCell(27);
				if (fundAllocation.getBeneficiaryEmailId() != null) {
					dataCell.setCellValue(fundAllocation
							.getBeneficiaryEmailId());
					dataCell.setCellStyle(styleData);
				} else
					dataCell.setCellValue("");
				
				dataCell = dataRow.createCell(28);
				if (fundAllocation.getStaffCode() != null) {
					dataCell.setCellValue(fundAllocation
							.getStaffCode());
					dataCell.setCellStyle(styleData);
				} else
					dataCell.setCellValue("");
				
				
				for(int j=0;j<fundAllocation.getFundAllocationDetails().size();j++){
					
					if(j>0)
						dataRow = sheet.createRow(rowToCreate);
					
					
					dataCell = dataRow.createCell(29);
					if(fundAllocation.getFundAllocationDetails().get(j).getCnmtCode()==null)
						dataCell.setCellValue("");
					else
						dataCell.setCellValue(fundAllocation.getFundAllocationDetails().get(j).getCnmtCode());
					
					dataCell.setCellStyle(styleData);
					
					dataCell = dataRow.createCell(30);
					if(fundAllocation.getFundAllocationDetails().get(j).getChallanNo()==null)
						dataCell.setCellValue("");
					else
						dataCell.setCellValue(fundAllocation.getFundAllocationDetails().get(j).getChallanNo());
					
					dataCell.setCellStyle(styleData);
					
					
					dataCell = dataRow.createCell(31);
					dataCell.setCellValue(fundAllocation.getFundAllocationDetails().get(j).getVehicleNo());
					dataCell.setCellStyle(styleData);
					System.out.println("vehicleNo="+fundAllocation.getFundAllocationDetails().get(j).getVehicleNo());
					
					dataCell = dataRow.createCell(32);
					dataCell.setCellValue(fundAllocation.getFundAllocationDetails().get(j).getChlnFreight());
					dataCell.setCellStyle(styleData);
					
					dataCell = dataRow.createCell(33);
					dataCell.setCellValue(fundAllocation.getFundAllocationDetails().get(j).getLryAmount());
					dataCell.setCellStyle(styleData);
					System.out.println("vehicleNo="+fundAllocation.getFundAllocationDetails().get(j).getLryAmount());
					
					
					SimpleDateFormat dateFormatN = new SimpleDateFormat(
							"dd/MM/yyyy");
					dataCell = dataRow.createCell(34);
					dataCell.setCellValue(dateFormatN.format(fundAllocation.getFundAllocationDetails().get(j).getChlnDt()));
					dataCell.setCellStyle(styleData);
					
					dataCell = dataRow.createCell(35);
					dataCell.setCellValue(fundAllocation.getFundAllocationDetails().get(j).getFromStCode());
					dataCell.setCellStyle(styleData);
					
					dataCell = dataRow.createCell(36);
					dataCell.setCellValue(fundAllocation.getFundAllocationDetails().get(j).getToStCode());
					dataCell.setCellStyle(styleData);
					
					dataCell = dataRow.createCell(37);
					dataCell.setCellValue(fundAllocation.getFundAllocationDetails().get(j).getBrkCode());
					dataCell.setCellStyle(styleData);
					
					dataCell = dataRow.createCell(38);
					dataCell.setCellValue(fundAllocation.getFundAllocationDetails().get(j).getOwnCode());
					dataCell.setCellStyle(styleData);
					
					
					//if(j>0)
					rowToCreate++;

				}
				
				rowToCreate++;

			}

			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition",
					"attachment; filename=fundAllocation.xlsx");
			ServletOutputStream out;
			try {
				out = response.getOutputStream();
				workbook.write(out);
				out.flush();
				out.close();

				fundAllocationDao.setGenerated(fundAllocationList);

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}/*finally{
				this.fundAllocationDetList.clear();
			}*/
		}
	}
	
	
	
	@RequestMapping(value = "/savePetroFund", method = RequestMethod.POST)
	public @ResponseBody
	Object savePetroFund(
			@RequestBody PetroCardAdv petro) {
		System.out.println("************* hello *********************");

		Map<String, Object> resposneMap = new HashMap<>();

		User user = (User) session.getAttribute("currentUser");

		PetroCardAdv adv=new PetroCardAdv();

		adv.setAmount(petro.getAmount());
		adv.setAmountReq(petro.getAmount());
		adv.setEmail(petro.getEmail());
		adv.setbCode(user.getUserBranchCode());
		adv.setCardNo(petro.getCardNo());
		adv.setCardType(petro.getCardType());
		adv.setChallanNo(petro.getChallanNo());
		adv.setChlnDt(petro.getChlnDt());
		adv.setCnmtNo(petro.getCnmtNo());
		adv.setFromStn(petro.getFromStn());
		adv.setLoryNo(petro.getLoryNo());
		adv.setToStn(petro.getToStn());
		adv.setAdvance(petro.getAdvance());
		adv.setBrkCode(petro.getBrkCode());
		adv.setFreight(petro.getFreight());
		adv.setRate(petro.getRate());
		adv.setTdsAmt(petro.getTdsAmt());
		adv.setUserCode(user.getUserCode());
		adv.setIsGenerated("No");
		adv.setCreationTS(Calendar.getInstance());
		

		int i = fundAllocationDao.savePetroAdv(adv);
		if (i > 0) 
			resposneMap.put("value", "success");
		else
			resposneMap.put("value", "Error");

		return resposneMap;
	}
	
	
	@RequestMapping(value = "/generatePetroAllocation", method = RequestMethod.POST)
	public @ResponseBody
	Object generatePetroAllocation() {
		Map<String, Object> resultMap = new HashMap<>();
		List<PetroCardAdv> allocations = fundAllocationDao
				.getCurrentDatePetroAllocationList();
		System.out.println("Size of List is " + allocations.size());

		if (allocations.size() > 0) {
			System.out.println("bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb");
			resultMap.put("msg", "Success");
			resultMap.put("list", allocations);
			return resultMap;
		} else {
			resultMap.put("msg", "No Fund Details for Current Date");
			return resultMap;
		}

	}
	

	@RequestMapping(value = "/rejectPetroAllocation", method = RequestMethod.POST)
	public @ResponseBody
	Object rejectPetroAllocation(@RequestBody PetroCardAdv petroAllocation) {
		Map<String, Object> responseMap = new HashMap<>();
		petroAllocation.setApproved(false);
		petroAllocation.setRejected(true);
		int i = fundAllocationDao.updatePetroAllocation(petroAllocation);
		if (i > 0) {
			responseMap.put("result", "success");
			responseMap.put("msg", "Rejected");
		} else {
			responseMap.put("result", "error");
			responseMap.put("msg", "There is some problem");
		}
			

		return responseMap;
	}
	
	@RequestMapping(value = "/updatePetroAllocation", method = RequestMethod.POST)
	public @ResponseBody
	Object updatePetroAllocation(@RequestBody PetroCardAdv petroAllocation) {
		Map<String, Object> responseMap = new HashMap<>();
		User user=(User) session.getAttribute("currentUser");
		Session session=this.sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		petroAllocation.setApproved(true);
		petroAllocation.setRejected(false);
		System.out.println("Petro Allocation="+petroAllocation.getCardNo());
		try {
			Date d=new Date();
			Long dt=d.getTime();
			java.sql.Date date=new java.sql.Date(dt);
			LhpvStatus ls=lhpvStatusDAO.getLssByDt("1", date);
			PetroCard petroCard=fundAllocationDao.getCardByCardNo(petroAllocation.getCardNo());
			List<Challan> chlnList=challanDao.getChallanByCode(petroAllocation.getChallanNo());
			if(chlnList.isEmpty()) {
				
				BranchStockLeafDet brs=brsLFDao.getCodeByCodeFrLa(petroAllocation.getChallanNo(), "chln");
				
				if(brs != null) {
					Challan challan=new Challan();
					challan.setbCode(petroAllocation.getbCode());
					challan.setBranchCode(petroAllocation.getbCode());
					challan.setCancel(false);
					challan.setChlnAdvance(String.valueOf(petroAllocation.getAdvance()));
					challan.setChlnArId(-1);
					challan.setChlnBalance(petroAllocation.getFreight()-petroAllocation.getAdvance());
					challan.setChlnBrRate(String.valueOf(petroAllocation.getRate()));
					challan.setChlnCode(petroAllocation.getChallanNo());
					challan.setChlnDt(petroAllocation.getChlnDt());
					challan.setChlnFreight(String.valueOf(petroAllocation.getFreight()));
					challan.setChlnFromStn(petroAllocation.getFromStn());
					challan.setChlnLryNo(petroAllocation.getLoryNo());
					challan.setChlnLryRate(String.valueOf(petroAllocation.getRate()));
					challan.setChlnPayAt("1");
					challan.setChlnRemAdv(petroAllocation.getAdvance()-petroAllocation.getAmount()-petroAllocation.getTdsAmt());
					challan.setChlnRemBal(petroAllocation.getFreight()-petroAllocation.getAdvance());
					challan.setChlnToStn(petroAllocation.getToStn());
					challan.setChlnTotalFreight(petroAllocation.getFreight());
					challan.setUserCode(user.getUserCode());
					int chlnId=challanDao.saveChallan(challan,session);
					if(chlnId>0) {
						if(ls !=null) {
							
							LhpvAdv la=new LhpvAdv();
							la.setbCode("1");
							la.setChallan(challan);
							//la.setLaBankCode(bankFa);
							la.setLaBrhCode("0100001");
							la.setLaDt(date);
							la.setLaFinalTot(petroAllocation.getAmount());
							la.setLaLryAdvP(petroAllocation.getAmount()+petroAllocation.getTdsAmt());
							la.setLaPayMethod('P');
							la.setLaTdsR(petroAllocation.getTdsAmt());
							la.setLaTotPayAmt(petroAllocation.getAmount()+petroAllocation.getTdsAmt());
							la.setLaTotRcvrAmt(petroAllocation.getTdsAmt());
							la.setLaNo(1);
							la.setLhpvStatus(ls);
							la.setPayToStf(petroAllocation.getCardType());
							la.setStafCode(petroCard.getCardFaCode());
							la.setUserCode(user.getUserCode());
							
							int laId=lhpvStatusDAO.saveLhpvAdv(la,session);
							
							fundAllocationDao.updatePetroAllocation(petroAllocation,session);
							
							session.flush();
							session.clear();
							transaction.commit();
							responseMap.put("result", "success");
						}else {
							LhpvAdv la=new LhpvAdv();
							la.setbCode("1");
							la.setChallan(challan);
							//la.setLaBankCode(bankFa);
							la.setLaBrhCode("0100001");
							la.setLaDt(date);
							la.setLaFinalTot(petroAllocation.getAmount());
							la.setLaLryAdvP(petroAllocation.getAmount()+petroAllocation.getTdsAmt());
							la.setLaPayMethod('P');
							la.setLaTdsR(petroAllocation.getTdsAmt());
							la.setLaTotPayAmt(petroAllocation.getAmount()+petroAllocation.getTdsAmt());
							la.setLaTotRcvrAmt(petroAllocation.getTdsAmt());
							la.setLaNo(1);
							//la.setLhpvStatus(ls);
							la.setPayToStf(petroAllocation.getCardType());
							la.setStafCode(petroCard.getCardFaCode());
							la.setUserCode(user.getUserCode());
							
							int laId=lhpvStatusDAO.saveLhpvAdv(la,session);
							
							fundAllocationDao.updatePetroAllocation(petroAllocation,session);
							session.flush();
							session.clear();
							transaction.commit();
							responseMap.put("result", "success");
						}
					}else {
						responseMap.put("msg", "challan not saved");
						responseMap.put("result", "error");
					}
					
				}else {
					responseMap.put("msg", "Stationary not found");
					responseMap.put("result", "error");
				}
				
				
			}else {
				
				Challan chln=chlnList.get(0);
				if(chln.getChlnRemAdv()-petroAllocation.getAmount()-petroAllocation.getTdsAmt()>=0) {
					chln.setChlnRemAdv(chln.getChlnRemAdv()-petroAllocation.getAmount()-petroAllocation.getTdsAmt());
					
					challanDao.updateChallan(chln,session);
					

					if(ls !=null) {
						
						LhpvAdv la=new LhpvAdv();
						la.setbCode("1");
						la.setChallan(chln);
						//la.setLaBankCode(bankFa);
						la.setLaBrhCode("0100001");
						la.setLaDt(date);
						la.setLaFinalTot(petroAllocation.getAmount());
						la.setLaLryAdvP(petroAllocation.getAmount()+petroAllocation.getTdsAmt());
						la.setLaPayMethod('P');
						la.setLaTdsR(petroAllocation.getTdsAmt());
						la.setLaTotPayAmt(petroAllocation.getAmount()+petroAllocation.getTdsAmt());
						la.setLaTotRcvrAmt(petroAllocation.getTdsAmt());
						la.setLaNo(1);
						la.setLhpvStatus(ls);
						la.setPayToStf(petroAllocation.getCardType());
						la.setStafCode(petroCard.getCardFaCode());
						la.setUserCode(user.getUserCode());
						
						int laId=lhpvStatusDAO.saveLhpvAdv(la,session);
						
						fundAllocationDao.updatePetroAllocation(petroAllocation,session);
						
						session.flush();
						session.clear();
						transaction.commit();
						responseMap.put("result", "success");
						
					}else {
						LhpvAdv la=new LhpvAdv();
						la.setbCode("1");
						la.setChallan(chln);
						//la.setLaBankCode(bankFa);
						la.setLaBrhCode("0100001");
						la.setLaDt(date);
						la.setLaFinalTot(petroAllocation.getAmount());
						la.setLaLryAdvP(petroAllocation.getAmount()+petroAllocation.getTdsAmt());
						la.setLaPayMethod('P');
						la.setLaTdsR(petroAllocation.getTdsAmt());
						la.setLaTotPayAmt(petroAllocation.getAmount()+petroAllocation.getTdsAmt());
						la.setLaTotRcvrAmt(petroAllocation.getTdsAmt());
						la.setLaNo(1);
						//la.setLhpvStatus(ls);
						la.setPayToStf(petroAllocation.getCardType());
						la.setStafCode(petroCard.getCardFaCode());
						la.setUserCode(user.getUserCode());
						
						int laId=lhpvStatusDAO.saveLhpvAdv(la,session);
						
						fundAllocationDao.updatePetroAllocation(petroAllocation,session);
						
						session.flush();
						session.clear();
						transaction.commit();
						
						responseMap.put("result", "success");
					}
				
					
				}else {
					responseMap.put("result", "error");
					responseMap.put("msg", "Amount can't be greater than pending advance");
				}
				
			}
			
			
		}catch(Exception e) {
			e.printStackTrace();
			responseMap.put("result", "exc");
			responseMap.put("msg", e);
			transaction.rollback();
		}finally {
			session.close();
		}

		return responseMap;
	}
	
	
	
	
	
	@RequestMapping(value = "/downloadPetroAllExcel", method = RequestMethod.POST)
	public void downloadPetroAllExcel(HttpServletResponse response) {
		System.out.println("In Excel Generator method");
		
		List<Branch> branchList=branchDao.getAllBranch();
		HashMap<Integer, String> branchMap=new HashMap<>();
		if (!branchList.isEmpty()) {
			for (Branch branch : branchList) {
				branchMap.put(branch.getBranchId(), branch.getBranchName());
			}
		}
		
		
		/*List<PetroCardAdv> fundAllocationList = fundAllocationDao
				.getCurrentDatePetroAllocationList();*/
		List<PetroCardAdv> fundAllocationList = fundAllocationDao
				.getPetroAllocationListFrExl();
		
		if (!fundAllocationList.isEmpty()) {
			System.out.println("Here........................................");
			XSSFWorkbook workbook = new XSSFWorkbook();

			// Header font
			XSSFFont fontHeader = workbook.createFont();
			fontHeader.setFontHeightInPoints((short) 11);
			fontHeader.setColor(IndexedColors.WHITE.getIndex());
			fontHeader.setFontName(FontFactory.COURIER_BOLD);
			fontHeader.setBold(true);

			// Header style
			XSSFCellStyle styleHeader = workbook.createCellStyle();
			styleHeader.setFont(fontHeader);
			styleHeader.setAlignment(XSSFCellStyle.ALIGN_CENTER);
			styleHeader.setFillForegroundColor(IndexedColors.BLUE_GREY
					.getIndex());
			styleHeader.setFillPattern(CellStyle.SOLID_FOREGROUND);
			styleHeader.setBorderTop(XSSFCellStyle.BORDER_MEDIUM);
			styleHeader.setBorderRight(XSSFCellStyle.BORDER_MEDIUM);
			styleHeader.setBorderBottom(XSSFCellStyle.BORDER_MEDIUM);
			styleHeader.setBorderLeft(XSSFCellStyle.BORDER_MEDIUM);

			//
			XSSFCellStyle styleData = workbook.createCellStyle();
			styleData.setAlignment(XSSFCellStyle.ALIGN_CENTER);

			XSSFSheet sheet = workbook.createSheet("Petro Card");
			sheet.setColumnWidth(0, 20 * 256);
			sheet.setColumnWidth(1, 25 * 256);
			sheet.setColumnWidth(2, 35 * 256);
			sheet.setColumnWidth(3, 30 * 256);
			sheet.setColumnWidth(4, 30 * 256);
			sheet.setColumnWidth(5, 30 * 256);
			sheet.setColumnWidth(6, 30 * 256);
			sheet.setColumnWidth(7, 30 * 256);
			sheet.setColumnWidth(8, 40 * 256);
			XSSFRow row = sheet.createRow(0);

			XSSFCell cell = null;
			cell = row.createCell(0);
			cell.setCellValue("Branch");
			cell.setCellStyle(styleHeader);

			cell = row.createCell(1);
			cell.setCellValue("Email Id");
			cell.setCellStyle(styleHeader);

			cell = row.createCell(2);
			cell.setCellValue("CNMT No.");
			cell.setCellStyle(styleHeader);

			cell = row.createCell(3);
			cell.setCellValue("Challan No.");
			cell.setCellStyle(styleHeader);

			cell = row.createCell(4);
			cell.setCellValue("Challan Date");
			cell.setCellStyle(styleHeader);

			cell = row.createCell(5);
			cell.setCellValue("Lorry No.");
			cell.setCellStyle(styleHeader);
			
			
			cell = row.createCell(6);
			cell.setCellValue("Card Type");
			cell.setCellStyle(styleHeader);

			cell = row.createCell(7);
			cell.setCellValue("Card No.");
			cell.setCellStyle(styleHeader);

			cell = row.createCell(8);
			cell.setCellValue("Amount");
			cell.setCellStyle(styleHeader);


			

			int rowToCreate = 1;
			XSSFRow dataRow = null;
			XSSFCell dataCell = null;
			for (PetroCardAdv fundAllocation : fundAllocationList) {
				dataRow = sheet.createRow(rowToCreate);

				dataCell = dataRow.createCell(0);
				if (fundAllocation.getbCode() != null) {
					dataCell.setCellValue(branchMap.get(Integer.parseInt(fundAllocation
							.getbCode())));
					dataCell.setCellStyle(styleData);
				} else
					dataCell.setCellValue("");

				dataCell = dataRow.createCell(1);
				if (fundAllocation.getEmail() != null) {
					dataCell.setCellValue(fundAllocation.getEmail());
					dataCell.setCellStyle(styleData);
				} else
					dataCell.setCellValue("");

				dataCell = dataRow.createCell(2);
				if (fundAllocation.getCnmtNo() != null) {
					dataCell.setCellValue(fundAllocation.getCnmtNo());
					dataCell.setCellStyle(styleData);
				} else
					dataCell.setCellValue("");

				dataCell = dataRow.createCell(3);
				if (fundAllocation.getChallanNo() != null) {
					dataCell.setCellValue(fundAllocation.getChallanNo());
					dataCell.setCellStyle(styleData);
				} else
					dataCell.setCellValue("");

				SimpleDateFormat dateFormatN = new SimpleDateFormat(
						"dd/MM/yyyy");
				dataCell = dataRow.createCell(4);
				if (fundAllocation.getChlnDt() != null) {
					dataCell.setCellValue(dateFormatN.format(fundAllocation.getChlnDt()));
					dataCell.setCellStyle(styleData);
				} else
					dataCell.setCellValue("");
				
				
				dataCell = dataRow.createCell(5);
				if (fundAllocation.getLoryNo() != null) {
					dataCell.setCellValue(fundAllocation.getLoryNo());
					dataCell.setCellStyle(styleData);
				} else
					dataCell.setCellValue("");
				

				dataCell = dataRow.createCell(6);
				if (fundAllocation.getCardType() != null) {
					dataCell.setCellValue(fundAllocation.getCardType());
					dataCell.setCellStyle(styleData);
				} else
					dataCell.setCellValue("");

				dataCell = dataRow.createCell(7);
				if (fundAllocation.getCardNo() != null) {
					dataCell.setCellValue(fundAllocation.getCardNo());
					dataCell.setCellStyle(styleData);
				} else
					dataCell.setCellValue("");


				dataCell = dataRow.createCell(8);
				if (fundAllocation.getAmount() > 0) {
					dataCell.setCellValue(fundAllocation.getAmount());
					dataCell.setCellStyle(styleData);
				} else
					dataCell.setCellValue(0.0);
				
				
					rowToCreate++;

			}

			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition",
					"attachment; filename=PetroAllocation.xlsx");
			ServletOutputStream out;
			try {
				out = response.getOutputStream();
				workbook.write(out);
				out.flush();
				out.close();

				fundAllocationDao.setPetroGenerated(fundAllocationList);

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}/*finally{
				this.fundAllocationDetList.clear();
			}*/
		}
	}
	
	
	@RequestMapping(value = "/alwChlnFrLryPmnt", method = RequestMethod.POST)
	public @ResponseBody
	Object alwChlnFrLryPmnt(@RequestBody Map<String, String> clientMap) {
		Map<String, String> responseMap = new HashMap<>();
		String chlnCode=clientMap.get("chlnNo");
		String alwType=clientMap.get("alwType");
		
		if(alwType.equalsIgnoreCase("AFA")) {
			System.out.println("AFA");
			responseMap=fundAllocationDao.alwChlnFrFundAdvanceMT(chlnCode);
		}else if(alwType.equalsIgnoreCase("APC")) {
			System.out.println("APC");
			responseMap=fundAllocationDao.alwChlnFrPetroAdvanceMT(chlnCode);
		}else if(alwType.equalsIgnoreCase("OB")) {
			System.out.println("OB");
			responseMap=challanDao.alwOldBalance(chlnCode);
		}
		
			

		return responseMap;
	}
	
	
	
}
