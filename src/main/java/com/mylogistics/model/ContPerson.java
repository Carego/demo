package com.mylogistics.model;

import java.util.ArrayList;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "contperson")
public class ContPerson {
	
	@Id
	@Column(name="cpId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int cpId;
	
	@Column(name="cpName")
	private String cpName;
	
	@Column(name="cpMobile")
	private ArrayList<String> cpMobile = new ArrayList<String>();
	
	@Column(name="cpPhone")
	private String cpPhone;
	
	@Column(name="cpPanNo")
	private String cpPanNo;
	
	@Column(name="cpRefCode")
	private String cpRefCode;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;
	
	@Column(name="bCode")
	private String bCode;

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}


	public int getCpId() {
		return cpId;
	}

	public void setCpId(int cpId) {
		this.cpId = cpId;
	}

	public String getCpName() {
		return cpName;
	}

	public void setCpName(String cpName) {
		this.cpName = cpName;
	}

	/*public String getCpMobile() {
		return cpMobile;
	}

	public void setCpMobile(String cpMobile) {
		this.cpMobile = cpMobile;
	}*/
	
	

	public String getCpPhone() {
		return cpPhone;
	}

	public ArrayList<String> getCpMobile() {
		return cpMobile;
	}

	public void setCpMobile(ArrayList<String> cpMobile) {
		this.cpMobile = cpMobile;
	}

	public void setCpPhone(String cpPhone) {
		this.cpPhone = cpPhone;
	}

	public String getCpPanNo() {
		return cpPanNo;
	}

	public void setCpPanNo(String cpPanNo) {
		this.cpPanNo = cpPanNo;
	}

	public String getCpRefCode() {
		return cpRefCode;
	}

	public void setCpRefCode(String cpRefCode) {
		this.cpRefCode = cpRefCode;
	}
}
