package com.mylogistics.model;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name="fundAllocationDetails")
public class FundAllocationDetails implements Serializable{
	
	@Id
	@GeneratedValue
	@Column(name="faDId")
	private long faDId;
	
	@Column(name="cnmtCode", length=50)
	private String cnmtCode;
	
	@Column(name="challanNo", length=50)
	private String challanNo;
	
	@Column(name="chlnDt",nullable=false,columnDefinition="date")
	private Date chlnDt;
	
	@Column(name="fromStCode",length=20,nullable=false)
	private String fromStCode;
	
	@Column(name="toStCode",length=20,nullable=false)
	private String toStCode;
	
	@Column(name="lryAmount",length=20,nullable=false)
	private double lryAmount;
	
	@Column(name="vehicleNo",length=20,nullable=false)
	private String vehicleNo;
	
	@Column(name="ownCode",length=20)
	private String ownCode;
	
	@Column(name="brkCode",length=20)
	private String brkCode;
	
	@Column(name="chlnFreight")
	private Double chlnFreight;
	
	@Column(name="lryRate")
	private Double lryRate;
	
	
	public String getCnmtCode() {
		return cnmtCode;
	}

	public void setCnmtCode(String cnmtCode) {
		this.cnmtCode = cnmtCode;
	}

	public String getChallanNo() {
		return challanNo;
	}

	public void setChallanNo(String challanNo) {
		this.challanNo = challanNo;
	}

	public Date getChlnDt() {
		return chlnDt;
	}

	public void setChlnDt(Date chlnDt) {
		this.chlnDt = chlnDt;
	}

	public String getFromStCode() {
		return fromStCode;
	}

	public void setFromStCode(String fromStCode) {
		this.fromStCode = fromStCode;
	}

	public String getToStCode() {
		return toStCode;
	}

	public void setToStCode(String toStCode) {
		this.toStCode = toStCode;
	}

	public double getLryAmount() {
		return lryAmount;
	}

	public void setLryAmount(double lryAmount) {
		this.lryAmount = lryAmount;
	}

	public String getVehicleNo() {
		return vehicleNo;
	}

	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}

	public long getFaDId() {
		return faDId;
	}

	public void setFaDId(long faDId) {
		this.faDId = faDId;
	}

	public String getOwnCode() {
		return ownCode;
	}

	public void setOwnCode(String ownCode) {
		this.ownCode = ownCode;
	}

	public String getBrkCode() {
		return brkCode;
	}

	public void setBrkCode(String brkCode) {
		this.brkCode = brkCode;
	}

	public Double getChlnFreight() {
		return chlnFreight;
	}

	public void setChlnFreight(Double chlnFreight) {
		this.chlnFreight = chlnFreight;
	}

	public Double getLryRate() {
		return lryRate;
	}

	public void setLryRate(Double lryRate) {
		this.lryRate = lryRate;
	}

	
}
