package com.mylogistics.DAOImpl.webservice;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.werservice.WebServiceDAO;
import com.mylogistics.constants.BranchSStatsCNTS;
import com.mylogistics.constants.BranchStockLeafDetCNTS;
import com.mylogistics.constants.CnmtAppCNTS;
import com.mylogistics.constants.CnmtCNTS;
import com.mylogistics.constants.ContToStnCNTS;
import com.mylogistics.constants.DailyContractCNTS;
import com.mylogistics.constants.RateByKmCNTS;
import com.mylogistics.constants.RegularContractCNTS;
import com.mylogistics.constants.webservice.ChallanAppCNTS;
import com.mylogistics.constants.webservice.OwnBrkAppCNTS;
import com.mylogistics.constants.webservice.VehicleAppCNTS;
import com.mylogistics.model.BranchSStats;
import com.mylogistics.model.BranchStockLeafDet;
import com.mylogistics.model.Cnmt;
import com.mylogistics.model.ContToStn;
import com.mylogistics.model.DailyContract;
import com.mylogistics.model.RateByKm;
import com.mylogistics.model.RegularContract;
import com.mylogistics.model.User;
import com.mylogistics.model.app.AppLoginOTP;
//import com.mylogistics.model.app.SupKycDocs;
//import com.mylogistics.model.app.Supplier;
import com.mylogistics.model.webservice.ChallanApp;
import com.mylogistics.model.webservice.CnmtApp;
import com.mylogistics.model.webservice.OwnBrkApp;
//import com.mylogistics.model.webservice.SupplierApp;
import com.mylogistics.model.webservice.VehicleApp;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.InvoiceService;
import com.mylogistics.services.InvoiceServiceImpl;

public class WebServiceDAOImpl implements WebServiceDAO {

	private SessionFactory sessionFactory;
	private Session session;
	private org.hibernate.Transaction transaction;
	
	@Autowired
	private HttpSession httpSession;
	
	public static Logger logger = Logger.getLogger(WebServiceDAOImpl.class);
	
	@Autowired
	public WebServiceDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Transactional
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getCodeList(String branchCode,String status){
		System.out.println("getCodeList().......");		
		List<Map<String, Object>> cnmtCodeList = new ArrayList<Map<String, Object>>();
		try{
			session = this.sessionFactory.openSession();
			cnmtCodeList = session.createQuery("SELECT brsLeafDetId AS brsLeafDetId, brsLeafDetBrCode AS brsLeafDetBrCode, brsLeafDetSNo AS brsLeafDetSNo FROM BranchStockLeafDet  WHERE brsLeafDetStatus = :brsLeafDetStatus AND  brsLeafDetBrCode = :brsLeafDetBrCode)")
					.setParameter(BranchStockLeafDetCNTS.BRSLD_STATUS, status)
					.setParameter(BranchStockLeafDetCNTS.BRSLD_BRCODE, branchCode)
					.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP)
					.list();		
			session.flush();
		}
		catch(Exception e){
			System.out.println("Error in getCodeList - WebServiceDAOImpl : "+e);			
		} 
		session.clear();
		session.close();
		return cnmtCodeList;
	}
	
	@Transactional
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getConsigneeList(){
		System.out.println("getConsigneeList().......");
		List<Map<String, Object>> consigneeList = new ArrayList<Map<String, Object>>();
		try{
			session = this.sessionFactory.openSession();
			consigneeList = session.createQuery("SELECT custId AS custId, custCode AS custCode, custName AS custName, custIsDailyContAllow AS custIsDailyContAllow FROM Customer")
					.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP)
					.list();		
			session.flush();
		}
		catch (Exception e) {			
			System.out.println("Error in getConsigneeList - WebServiceDAOImpl : "+e);
		}
		session.clear();
		session.close();
		return consigneeList;
	}
	
	@Transactional
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getCustomerList(){
		List<Map<String, Object>> customerList = new ArrayList<Map<String, Object>>();
		System.out.println("getCustomerList().......");
		try{
			session = this.sessionFactory.openSession();
			customerList = session.createQuery("SELECT custId AS custId, custCode AS custCode, custName AS custName, custIsDailyContAllow AS custIsDailyContAllow FROM Customer")				
					.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP)
					.list();
		}catch(Exception e){
			System.out.println("Eror in getCustomerList - WebServiceDAOImpl : "+e);
		}
		session.flush();
		session.clear();
		session.close();
		return customerList;
	}
	
	@Transactional
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getAllEmployeeList(){
		List<Map<String, Object>> employeeList = new ArrayList<Map<String, Object>>();
		try{
			session = this.sessionFactory.openSession();
			employeeList = session.createQuery("SELECT empId AS empId, empName AS empName, empFaCode AS empFaCode, empCodeTemp AS empCodeTemp FROM Employee WHERE isTerminate = :isTerminate")					
					.setParameter("isTerminate", "false")
					.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP)
					.list();
		}catch(Exception e){
			System.out.println("Error in getAllEmployeeList - WebServiceDAOImpl : "+e);
		}
		session.flush();
		session.clear();
		session.close();		
		return employeeList;
	}
	
	@Transactional
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getStateList(){
		List<Map<String, Object>> stateList = new ArrayList<Map<String, Object>>();
		try{
			session = this.sessionFactory.openSession();
			stateList = session.createQuery("SELECT stateId AS stateId, stateCode AS stateCode, stateName AS stateName, stateLryPrefix AS stateLryPrefix FROM State")
					.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP)
					.list();
		}catch(Exception e){
			System.out.println("Error in getAllEmployeeList - WebServiceDAOImpl : "+e);
		}
		session.flush();
		session.clear();
		session.close();		
		return stateList;
	}
	
	@Transactional
	@SuppressWarnings("unchecked")
	public void deleteAllInv(){
		try{
			InvoiceService invoiceService = new InvoiceServiceImpl();
			invoiceService.deleteAll();
		}catch(Exception e){
			System.out.println("Error in deleteAllInv - WebServiceDAOImpl : "+e);
		}
	}
	
	@Transactional
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getStationDataList(){
		List<Map<String, Object>> stationDataList = new ArrayList<Map<String, Object>>();
		try{
			session = this.sessionFactory.openSession();
			stationDataList = session.createQuery("SELECT stnId AS stnId, stnName AS stnName, stnDistrict AS stnDistrict  FROM Station")
					.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP)
					.list();
		}catch(Exception e){
			System.out.println("Error in getStationDataList - WebSericeDAOImpl : "+e);
		}
		session.flush();
		session.clear();
		session.close();
		return stationDataList;
	}
	
	@Transactional
	@SuppressWarnings("unchecked")
	public List<String> getProductTypeList(){
		List<String> ptList = new ArrayList<String>();
		try{
			session = this.sessionFactory.openSession();
			ptList = session.createQuery("SELECT ptName AS ptName FROM ProductType")
					.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP)
					.list();
		}catch(Exception e){
			System.out.println("Errro in getProductTypeList- WebServiceDAOImpl : "+e);
		}
		session.flush();
		session.clear();
		session.close();		
		return ptList;
	}
	
	@Transactional
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getVehicleTypeList(){
		List<Map<String, Object>> vtList = new ArrayList<Map<String,Object>>();
		try{
			session = this.sessionFactory.openSession();			
			vtList = session.createQuery("SELECT vtId AS vtId, vtCode AS vtCode, vtVehicleType AS vtVehicleType FROM VehicleType")
					.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP)
					.list();				
		}catch(Exception e){
			System.out.println("Error in getVehicleTypeList - WebServiceDAOImpl : "+e);
		}
		session.flush();
		session.clear();
		session.close();
		return vtList;
	}
	
	@Transactional
	@SuppressWarnings("unchecked")
	public Map<String, Object> getRegContList(String custCode, Date cnmtDate, String cnmtFromSt){
		Map<String, Object> map = new HashMap<String, Object>();
		try{
			session = this.sessionFactory.openSession();
			Criteria regContCriteria = session.createCriteria(RegularContract.class);
			regContCriteria.add(Restrictions.eq(RegularContractCNTS.REG_CONT_BLPM_CODE, custCode));
			regContCriteria.add(Restrictions.eq(RegularContractCNTS.REG_CONT_FROM_STATION, cnmtFromSt));		
			regContCriteria.add(Restrictions.and(Restrictions.le(RegularContractCNTS.REG_CONT_FROM_DT, cnmtDate), Restrictions.ge(RegularContractCNTS.REG_CONT_TO_DT, cnmtDate)));
			
			List<RegularContract> regList = regContCriteria.list();			
			Iterator<RegularContract> regIterator = regList.iterator();
			while(regIterator.hasNext()){
				RegularContract regCont = (RegularContract)regIterator.next();				
				map.put("cont", "reg");
				map.put("contId", regCont.getRegContId());
				map.put("contCode", regCont.getRegContCode());
				map.put("contFaCode", regCont.getRegContFaCode());
				map.put("contDc", regCont.getRegContDc());
				map.put("contDDL", regCont.getRegContDdl());
				map.put("contCostGrade", regCont.getRegContCostGrade());
				map.put("contType",regCont.getRegContType());
				map.put("fromStation", regCont.getRegContFromStation());
				map.put("toStation", regCont.getRegContToStation());
				map.put("proportionate", regCont.getRegContProportionate());
				if(regCont.getRegContRate() == 0)
					map.put("contRate", "");
				else
					map.put("contRate", regCont.getRegContRate());
			}
			
		}catch(Exception e){
			System.out.println("Error in getRegContList - WebServiceDAOImpl : "+e);
		}
		session.flush();
		session.clear();
		session.close();
		return map;
	}
	
	
	@Transactional
	@SuppressWarnings("unchecked")
	public Map<String, Object> getRegContForCnmtPending(String contCode){
		Map<String, Object> map = new HashMap<String, Object>();
		try{			
			Criteria regContCriteria = session.createCriteria(RegularContract.class);
			regContCriteria.add(Restrictions.eq(RegularContractCNTS.REG_CONT_CODE, contCode));			
			List<RegularContract> regList = regContCriteria.list();			
			Iterator<RegularContract> regIterator = regList.iterator();
			while(regIterator.hasNext()){
				System.out.println("RegularContract --------");
				RegularContract regCont = (RegularContract)regIterator.next();				
				map.put("cont", "reg");
				map.put("contId", regCont.getRegContId());
				map.put("contCode", regCont.getRegContCode());
				map.put("contFaCode", regCont.getRegContFaCode());
				map.put("contDC", regCont.getRegContDc());
				map.put("contDDL", regCont.getRegContDdl());
				map.put("contCostGrade", regCont.getRegContCostGrade());
				map.put("contType",regCont.getRegContType());
				map.put("fromStation", regCont.getRegContFromStation());
				map.put("toStation", regCont.getRegContToStation());
				map.put("proportionate", regCont.getRegContProportionate());
				if(regCont.getRegContRate() == 0)
					map.put("contRate", "");
				else
					map.put("contRate", regCont.getRegContRate());
			}
			
		}catch(Exception e){
			System.out.println("Error in getRegContList - WebServiceDAOImpl : "+e);
		}
		session.flush();
		session.clear();		
		return map;
	}
	
	@Transactional
	@SuppressWarnings("unchecked")
	public Map<String, Object> getDailyContList(String custCode, Date cnmtDate, String cnmtFromSt){
		Map<String, Object> map = new HashMap<String, Object>();
		try{
			session = this.sessionFactory.openSession();
			Criteria dailyContCriteria =session.createCriteria(DailyContract.class);
			dailyContCriteria.add(Restrictions.eq(DailyContractCNTS.DLY_CONT_BLPM_CODE,custCode));			
			dailyContCriteria.add(Restrictions.eq(DailyContractCNTS.DLY_CONT_FROM_STATION,cnmtFromSt));	 
			dailyContCriteria.add(Restrictions.and(Restrictions.le(DailyContractCNTS.DLY_CONT_START_DT, cnmtDate), Restrictions.ge(DailyContractCNTS.DLY_CONT_END_DT, cnmtDate)));		 
			
			List<DailyContract> dailyCont = dailyContCriteria.list();
			Iterator<DailyContract> dailyContIterator = dailyCont.iterator();
			while(dailyContIterator.hasNext()){
				System.out.println("DailyContract --------");
				DailyContract contract = (DailyContract)dailyContIterator.next();				
				map.put("cont", "dly");
				map.put("contId", contract.getDlyContId());
				map.put("contCode", contract.getDlyContCode());
				map.put("contFaCode", contract.getDlyContFaCode());
				map.put("contDc", contract.getDlyContDc());				
				map.put("contDDL", contract.getDlyContDdl());
				map.put("contCostGrade", contract.getDlyContCostGrade());
				map.put("contType", contract.getDlyContType());
				map.put("fromStation", contract.getDlyContFromStation());
				map.put("toStation", contract.getDlyContToStation());
				map.put("proportionate", contract.getDlyContProportionate());
				if(contract.getDlyContRate() == 0)
					map.put("contRate", "");
				else
					map.put("contRate", contract.getDlyContRate());				
			}
		}catch(Exception e){
			System.out.println("Error in getDailyContList - WebServiceDAOImpl : "+e);
		}
		session.flush();
		session.clear();
		session.close();
		return map;
	}
	
	
	@Transactional
	@SuppressWarnings("unchecked")
	public Map<String, Object> getDailyContForCnmtPending(String contCode){
		Map<String, Object> map = new HashMap<String, Object>();
		try{			
			Criteria dailyContCriteria =session.createCriteria(DailyContract.class);
			dailyContCriteria.add(Restrictions.eq(DailyContractCNTS.DLY_CONT_CODE, contCode));		 
			
			List<DailyContract> dailyCont = dailyContCriteria.list();
			Iterator<DailyContract> dailyContIterator = dailyCont.iterator();
			while(dailyContIterator.hasNext()){
				DailyContract contract = (DailyContract)dailyContIterator.next();
				System.out.println("Daily Contract ---------");
				map.put("cont", "dly");
				map.put("contId", contract.getDlyContId());
				map.put("contCode", contract.getDlyContCode());
				map.put("contFaCode", contract.getDlyContFaCode());
				map.put("contDC", contract.getDlyContDc());
				System.out.println("DC : "+contract.getDlyContDc());
				map.put("contDDL", contract.getDlyContDdl());
				map.put("contCostGrade", contract.getDlyContCostGrade());
				map.put("contType", contract.getDlyContType());
				map.put("fromStation", contract.getDlyContFromStation());
				map.put("toStation", contract.getDlyContToStation());
				map.put("proportionate", contract.getDlyContProportionate());
				if(contract.getDlyContRate() == 0)
					map.put("contRate", "");
				else
					map.put("contRate", contract.getDlyContRate());				
			}
		}catch(Exception e){
			System.out.println("Error in getDailyContList - WebServiceDAOImpl : "+e);
		}
		session.flush();
		session.clear();		
		return map;
	}
	
	@Transactional
	@SuppressWarnings("unchecked")
	public List<RegularContract> getRegContByContCode(String contCode){
		List<RegularContract> regContList = new ArrayList<RegularContract>();
		try{
			session = this.sessionFactory.openSession();
			regContList = session.createCriteria(RegularContract.class)
					.add(Restrictions.eq(RegularContractCNTS.REG_CONT_CODE, contCode))
					.list();
		}catch(Exception e){
			System.out.println("Error in getRegContByContCode - WebServiceDaoImpl : "+e);
		}
		session.flush();
		session.clear();
		session.close();
		return regContList;
	}
	
	@Transactional
	@SuppressWarnings("unchecked")
	public List<ContToStn> getContToStnRateCnmtW(String contCode, String cnmtToSta, String cnmtVehicleType, Date cnmtDt){
		List<ContToStn> contToStnList = new ArrayList<ContToStn>();
		try{
			session = this.sessionFactory.openSession();
			Criteria cr=session.createCriteria(ContToStn.class);
			cr.add(Restrictions.eq(ContToStnCNTS.CTS_CONT_CODE, contCode));//"ctsContCode"
			cr.add(Restrictions.eq(ContToStnCNTS.CONT_TO_STN, cnmtToSta));//"ctsToStn"
			cr.add(Restrictions.eq(ContToStnCNTS.CTS_VEHICLE_TYPE, cnmtVehicleType));//"ctsVehicleType"
			cr.add(Restrictions.le(ContToStnCNTS.CTS_FR_DT, cnmtDt));//"ctsFrDt"
			cr.add(Restrictions.ge(ContToStnCNTS.CTS_TO_DT, cnmtDt));//"ctsToDt"
			
			contToStnList = cr.list();
		}catch(Exception e){
			System.out.println("Error in getContToStnRateCnmtW - WebServiceDAOImpl : "+e);
		}
		session.flush();
		session.clear();
		session.close();
		return contToStnList;
	}
	
	@Transactional
	@SuppressWarnings("unchecked")
	public List<ContToStn> getContToStnRateCnmtQ(String contCode, String cnmtToSta, String cnmtProductType, Date cnmtDt){
		List<ContToStn> contToStnList = new ArrayList<ContToStn>();
		try{
			session = this.sessionFactory.openSession();
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr=session.createCriteria(ContToStn.class);
			cr.add(Restrictions.eq("ctsContCode", contCode));
			cr.add(Restrictions.eq("ctsToStn", cnmtToSta));
			cr.add(Restrictions.eq("ctsProductType", cnmtProductType));
			
			contToStnList = cr.list();
		}catch(Exception e){
			System.out.println("Error in getContToStnRateCnmtW - WebServiceDAOImpl : "+e);
		}
		session.flush();
		session.clear();
		session.close();
		return contToStnList;
	}
	
	@Transactional
	@SuppressWarnings("unchecked")
	public List<RegularContract> getRegContractData(String contCode){
		List<RegularContract> rgContList = new ArrayList<RegularContract>();
		try{
			session = this.sessionFactory.openSession();
			session = this.sessionFactory.openSession();			
			Criteria cr=session.createCriteria(RegularContract.class);
			cr.add(Restrictions.eq(RegularContractCNTS.REG_CONT_CODE,contCode));
			rgContList =cr.list();		
		}catch(Exception e){
			System.out.println("Error in getRegContractData - WebServiceDAOImpl : "+e);
		}
		session.flush();
		session.clear();
		session.close();
		return rgContList;
	}
	
	@Transactional
	@SuppressWarnings("unchecked")
	public List<DailyContract> getDailyContractData(String contCode){
		List<DailyContract> dlyContList = new ArrayList<DailyContract>();
		try{
			session = this.sessionFactory.openSession();		
			Criteria cr=session.createCriteria(DailyContract.class);
			cr.add(Restrictions.eq(RegularContractCNTS.REG_CONT_CODE,contCode));
			dlyContList =cr.list();		
		}catch(Exception e){
			System.out.println("Error in getDailyContractData - WebServiceDAOImpl : "+e);
		}
		session.flush();
		session.clear();
		session.close();
		return dlyContList;
	}
	
	@Transactional
	@SuppressWarnings("unchecked")
	public List<RateByKm> getRbkmRate(String cnmtState, String contCode){
		List<RateByKm> rateByKmList = new ArrayList<RateByKm>();
		try{
			 session = this.sessionFactory.openSession();
			 Criteria cr=session.createCriteria(RateByKm.class);
			 cr.add(Restrictions.eq(RateByKmCNTS.RBKM_STATE_CODE, cnmtState));			 
			 cr.add(Restrictions.eq(RateByKmCNTS.RBKM_CONT_CODE, contCode));
			 rateByKmList = cr.list();
		}catch(Exception e){
			System.out.println("Error in getRbKmRate - WebServiceDAOImpl : "+e);
		}
		session.flush();
		session.clear();
		session.close();
		return rateByKmList;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public double getLastMonthPerDayUsed(Date startDate,Date endDate,String type){
		
		List<BranchSStats> branchSStats = new ArrayList<BranchSStats>();
		int cnmtUsed=0;
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 Criteria criteria = session.createCriteria(BranchSStats.class);
			 criteria.add(Restrictions.ge(BranchSStatsCNTS.BRS_DT,endDate)); 
			 criteria.add(Restrictions.lt(BranchSStatsCNTS.BRS_DT,startDate));
			 criteria.add(Restrictions.eq(BranchSStatsCNTS.BRS_STATUS_TYPE,type)); 
			 branchSStats = criteria.list();
			 int listSize=branchSStats.size();
			 
			 HashSet<Date> hashSet = new HashSet<Date>();
			 for(int i=0;i<branchSStats.size();i++){
				 hashSet.add(branchSStats.get(i).getBrsStatsDt());
			 }
			 
			 int hashSize=hashSet.size();
			 
			 if(hashSize > 0){
				 cnmtUsed=listSize/hashSize;
			 }			 
			 transaction.commit();
			 session.flush();
			
		 }
		 catch(Exception e){
			 e.printStackTrace(); 
		 }
		 session.clear();
		 session.close();
		 return cnmtUsed;
	 }
	
	@SuppressWarnings("unchecked")
	@Transactional
	public Date getFirstEntryDate(String type){
		System.out.println("--------------------enter into getFirstEntryDate");
		List<BranchSStats> branchSStats = new ArrayList<BranchSStats>();
		 Date date = null;
		 try{
			 session = this.sessionFactory.openSession();
			 org.hibernate.Query query  = session.createQuery("from BranchSStats where brsStatsType=:type order by brsStatsId ").setMaxResults(1);
			 query.setParameter("type",type);
			 branchSStats=query.list();
			 date = branchSStats.get(0).getBrsStatsDt();
			 System.out.println("The first date is---------------"+date);
			 session.flush();
			
		 }
		 catch(Exception e){
			 e.printStackTrace();
		 }
		 session.clear();
		 session.close();
		 return date;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public double getNoOfStationary(Date startDate,Date endDate,String type){
		
		List<BranchSStats> branchSStats = new ArrayList<BranchSStats>();
		double listSize=0;
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 Criteria criteria = session.createCriteria(BranchSStats.class);
			 criteria.add(Restrictions.ge(BranchSStatsCNTS.BRS_DT,endDate)); 
			 criteria.add(Restrictions.lt(BranchSStatsCNTS.BRS_DT,startDate));
			 criteria.add(Restrictions.ge(BranchSStatsCNTS.BRS_STATUS_TYPE,type)); 
			 branchSStats = criteria.list();
			 
			 listSize=branchSStats.size();
			 transaction.commit();
			 session.flush();
			
		 }
		 catch(Exception e){
			 e.printStackTrace(); 
		 }
		 session.clear();
		 session.close();
		 return listSize;
	 }
	
	@SuppressWarnings("unchecked")
	@Transactional
	public Map<String, Object> saveCnmtToDB(CnmtApp cnmtApp){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try{
			cnmtApp = fillFaCode(cnmtApp);
			session = this.sessionFactory.openSession();		
			
			transaction = session.beginTransaction();			
			Integer cnmtId = (Integer)session.save(cnmtApp);		
			
			if(cnmtId != null && cnmtId > 0)
				resultMap.put(ConstantsValues.STATUS, ConstantsValues.SUCCESS);
			else
				resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
			transaction.commit();
		}catch(Exception e){
			System.out.println("Error in saveCnmtToDB - WebServiceDAOImpl : "+e);
		}
		session.flush();
		session.clear();
		session.close();
		return resultMap;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public CnmtApp fillFaCode(CnmtApp cnmtApp){
		try{
			session = this.sessionFactory.openSession();
			List<String> list = new ArrayList<String>();
			list =	session.createQuery("SELECT custFaCode FROM Customer WHERE custCode = :custCode")
					.setParameter("custCode", cnmtApp.getCnmtCustCode())
					.list();
			if(! list.isEmpty())
				cnmtApp.setCnmtCustFaCode(list.get(0));
			
			list = session.createQuery("SELECT stnName FROM Station WHERE stnCode = :stnCode")
					.setParameter("stnCode", cnmtApp.getCnmtFromStCode())
					.list();
			if(! list.isEmpty())
				cnmtApp.setCnmtFromStName(list.get(0));
			
			list = session.createQuery("SELECT stnName FROM Station WHERE stnCode = :stnCode")
					.setParameter("stnCode", cnmtApp.getCnmtFromStCode())
					.list();
			if(! list.isEmpty())
				cnmtApp.setCnmtFromStName(list.get(0));
			
			if(cnmtApp.getCnmtContCode().contains("reg")){
				list = session.createQuery("SELECT regContFaCode FROM RegularContract WHERE regContCode = :regContCode")
						.setParameter("regContCode", cnmtApp.getCnmtContCode())
						.list();
				if(! list.isEmpty())
					cnmtApp.setCnmtContFaCode(list.get(0));
			}else if(cnmtApp.getCnmtContCode().contains("dly")){
				list = session.createQuery("SELECT dlyContFaCode FROM DailyContract WHERE dlyContCode = :dlyContCode")
						.setParameter("dlyContCode", cnmtApp.getCnmtContCode())
						.list();
				if(! list.isEmpty())
					cnmtApp.setCnmtContFaCode(list.get(0));
			}
			
			list = session.createQuery("SELECT stnName FROM Station WHERE stnCode = :stnCode")
					.setParameter("stnCode", cnmtApp.getCnmtToStCode())
					.list();
			if(! list.isEmpty())
				cnmtApp.setCnmtToStName(list.get(0));
			
			list = session.createQuery("SELECT custFaCode FROM Customer WHERE custCode = :custCode")
					.setParameter("custCode", cnmtApp.getCnmtConsignorCode())
					.list();
			if(! list.isEmpty())
				cnmtApp.setCnmtConsignorFaCode(list.get(0));
			
			list = session.createQuery("SELECT custFaCode FROM Customer WHERE custCode = :custCode")
					.setParameter("custCode", cnmtApp.getCnmtConsigneeCode())
					.list();
			if(! list.isEmpty())
				cnmtApp.setCnmtConsigneeFaCode(list.get(0));
			
			list = session.createQuery("SELECT vtVehicleType FROM VehicleType WHERE vtCode = :vtCode")
					.setParameter("vtCode", cnmtApp.getCnmtVehicleTypeCode())
					.list();			
			if(! list.isEmpty())
				cnmtApp.setCnmtVehicleType(list.get(0));
			
			list = session.createQuery("SELECT vtVehicleType FROM VehicleType WHERE vtCode = :vtCode")
					.setParameter("vtCode", cnmtApp.getCnmtVehicleTypeCode())
					.list();			
			if(! list.isEmpty())
				cnmtApp.setCnmtVehicleType(list.get(0));
			
			list = session.createQuery("SELECT stateName FROM State WHERE stateCode = :stateCode")
					.setParameter("stateCode", cnmtApp.getCnmtStateCode())
					.list();			
			if(! list.isEmpty())
				cnmtApp.setCnmtState(list.get(0));
			
			list = session.createQuery("SELECT branchFaCode FROM Branch WHERE branchCode = :branchCode")
					.setParameter("branchCode", cnmtApp.getCnmtPayAtCode())
					.list();			
			if(! list.isEmpty())
				cnmtApp.setCnmtPayAtFaCode(list.get(0));
			
			list = session.createQuery("SELECT branchFaCode FROM Branch WHERE branchCode = :branchCode")
					.setParameter("branchCode", cnmtApp.getCnmtBillAtCode())
					.list();			
			if(! list.isEmpty())
				cnmtApp.setCnmtBillAtFaCode(list.get(0));
			
			list = session.createQuery("SELECT empFaCode FROM Employee WHERE empCode = :empCode")
					.setParameter("empCode", cnmtApp.getCnmtEmpCode())
					.list();			
			if(! list.isEmpty())
				cnmtApp.setCnmtEmpFaCode(list.get(0));			
			
		}catch(Exception e){
			System.out.println("Error in fillFaCode -WebServiceDaoImpl : "+e);
		}
		session.flush();
		session.clear();
		session.close();
		return cnmtApp;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Map<String, Object>> getBranchList(){
		List<Map<String, Object>> branchList = new ArrayList<Map<String, Object>>();
		try{
			session = this.sessionFactory.openSession();
			branchList = session.createQuery("SELECT branchName AS branchName, branchCode AS branchCode FROM Branch")
					.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP)
					.list();
		}catch(Exception e){
			System.out.println("Error in saveCnmtToDB - WebServiceDAOImpl : "+e);
		}
		session.flush();
		session.clear();
		session.close();
		return branchList;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public Map<String, Object> getCnmtCodeListForApp(){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		List<String> cnmtCodeList = null;
		try{
			session = this.sessionFactory.openSession();			
			Criteria cnmtCodeListCriteria = session.createCriteria(CnmtApp.class);
			ProjectionList projectionList = Projections.projectionList();
			projectionList.add(Projections.property("cnmtCode"));
			cnmtCodeListCriteria.setProjection(projectionList);
			cnmtCodeList = cnmtCodeListCriteria.list();
			if(! cnmtCodeList.isEmpty()){
				resultMap.put(ConstantsValues.STATUS, ConstantsValues.SUCCESS);
				resultMap.put("cnmtCodeList", cnmtCodeList);
			}else{
				resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
				resultMap.put("msg", "No pending CNMT");
			}
		}catch(Exception e){
			resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
			resultMap.put("msg", "Database Exception !");
			System.out.println("Error in getCnmtCodeListForApp - WebServiceDAOImpl : "+e);
		}
		session.flush();
		session.clear();
		session.close();
		return resultMap;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public Map<String, Object> getPendingCnmtDetail(String cnmtCode, String branchCode){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		List<CnmtApp> cnmtCodeList = new ArrayList<CnmtApp>();
		try{
			session = this.sessionFactory.openSession();
			cnmtCodeList = session.createCriteria(CnmtApp.class)
					.add(Restrictions.eq(CnmtAppCNTS.CNMT_CODE, cnmtCode))
					.list();		
						
			if(! cnmtCodeList.isEmpty()){
				CnmtApp cnmtApp = (CnmtApp)cnmtCodeList.get(0);
				if(cnmtApp.getCnmtContCode().contains("dly")){
					resultMap.put("contract", getDailyContForCnmtPending(cnmtApp.getCnmtContCode()));
				}else if(cnmtApp.getCnmtContCode().contains("reg")){
					resultMap.put("contract", getRegContForCnmtPending(cnmtApp.getCnmtContCode()));
				}
				Criteria cr=session.createCriteria(BranchStockLeafDet.class);
				cr.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_STATUS, "cnmt"));
				cr.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_BRCODE, branchCode));
				cr.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_SNO, cnmtCode));
				List cnmtList = cr.list();
				if(! cnmtList.isEmpty())
					resultMap.put("cnmtCode", cnmtList.get(0));
				resultMap.put("cnmtPending", (CnmtApp)cnmtCodeList.get(0));
				resultMap.put(ConstantsValues.STATUS, ConstantsValues.SUCCESS);
			}else{			
				resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
				resultMap.put("msg", "No Such Record !");
			}
		}catch(Exception e){
			System.out.println("Error in getPendingCnmtDetail - WebServiceDaoImpl : "+e);
			resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
			resultMap.put("msg", "Database exception !");
		}
		session.flush();
		session.clear();
		session.close();
		return resultMap;
	}
	
	public Map<String, Object> getCnmtPendingCodeList(String branchCode){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try{
			session = this.sessionFactory.openSession();
					
			Criteria cr = session.createCriteria(CnmtApp.class);
			cr.add(Restrictions.eq(CnmtAppCNTS.CNMT_IS_PENDING, true));
			cr.add(Restrictions.eq(CnmtAppCNTS.CNMT_IS_CANCEL, false));
			cr.add(Restrictions.eq(CnmtAppCNTS.CNMT_BRANCH_CODE, branchCode));
			ProjectionList proList = Projections.projectionList();
			proList.add(Projections.property("cnmtCode"));
			cr.setProjection(proList);			
			List<String> list = cr.list();			
			List<Map<String, Object>> finalList = new ArrayList<Map<String, Object>>();
			Iterator<String> it = list.iterator();
			int i = 1;
			while(it.hasNext()){
				String cnmtCode = (String)it.next();
				Map<String, Object> map = new HashMap<String, Object>();
				List li = session.createQuery("SELECT brsLeafDetDlyRt AS brsLeafDetDlyRt FROM  BranchStockLeafDet WHERE brsLeafDetSNo = :brsLeafDetSNo")
						.setParameter("brsLeafDetSNo", cnmtCode)
						.list();				
				if(! li.isEmpty()){					
					map.put("brsLeafDetSNo", cnmtCode);
					map.put("brsLeafDetDlyRt", li.get(0));
					finalList.add(map);
				}
			}			
			resultMap.put("cnmtCodeList", finalList);
			resultMap.put(ConstantsValues.STATUS, ConstantsValues.SUCCESS);
		}catch(Exception e){
			System.out.println("Error in getCnmtPendingCodeList - WebServiceDAOImpl : "+e);
			resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
			resultMap.put("msg", "Database exception !");
		}
		session.flush();
		session.clear();
		session.close();
		return resultMap;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public Map<String, Object> checkAvaCnmtCode(String cnmtCode){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try{
			session = this.sessionFactory.openSession();
			List<CnmtApp> cnmtAppList = session.createCriteria(CnmtApp.class)
					.add(Restrictions.eq(CnmtAppCNTS.CNMT_CODE, cnmtCode))
					.list();			
			if(cnmtAppList.isEmpty())
				resultMap.put(ConstantsValues.STATUS, ConstantsValues.SUCCESS);
			else
				resultMap.put(ConstantsValues.STATUS, ConstantsValues.ERROR);
		}catch(Exception e){
			System.out.println("Error in checkAvaCnmtCode - WebServiceDAOImpl : "+e);
		}
		session.flush();
		session.clear();
		session.close();
		return resultMap;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public String updatePendingCnmt(String cnmtCode){
		String status = new String();
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			List<CnmtApp> cnmtAppList = session.createQuery("FROM CnmtApp WHERE cnmtCode = :cnmtCode")
					.setParameter("cnmtCode", cnmtCode)
					.list();
			if(! cnmtAppList.isEmpty()){
				CnmtApp cnmtApp = (CnmtApp)cnmtAppList.get(0);
				cnmtApp.setCnmtIsPending(false);
				session.update(cnmtApp);
				status = "success";
			}else{
				status = "error";
			}
		}catch(Exception e){
			System.out.println("Error in checkAvaCnmtCode - WebServiceDAOImpl : "+e);
		}
		transaction.commit();
		session.flush();
		session.clear();
		session.close();
		return status;
	}	
	
	@SuppressWarnings("unchecked")
	@Transactional
	public void updatePendingCnmtChange(Cnmt cnmt, User user){
		try{
			session = this.sessionFactory.openSession();			
			List<CnmtApp> cnmtAppList = session.createQuery("FROM CnmtApp WHERE cnmtCode = :cnmtCode")
					.setParameter("cnmtCode", cnmt.getCnmtCode())
					.list();
			session.close();
			if(! cnmtAppList.isEmpty()){
				CnmtApp cnmtApp = (CnmtApp)cnmtAppList.get(0);				
				cnmtApp.setCnmtCode(cnmtApp.getCnmtCode());
				cnmtApp.setCnmtCustCode(cnmt.getCustCode());
				cnmtApp.setCnmtVOG(cnmt.getCnmtVOG());
				cnmtApp.setCnmtDt(cnmt.getCnmtDt());							
				cnmtApp.setCnmtFromStCode(cnmt.getCnmtFromSt());
				cnmtApp.setCnmtToStCode(cnmt.getCnmtToSt());
				cnmtApp.setCnmtConsignorCode(cnmt.getCnmtConsignor());
				cnmtApp.setCnmtConsigneeCode(cnmt.getCnmtConsignee());
				cnmtApp.setCnmtVehicleTypeCode(cnmt.getCnmtVehicleType());
				cnmtApp.setCnmtProductType(cnmt.getCnmtProductType());
				cnmtApp.setCnmtActualWt(cnmt.getCnmtActualWt());
				cnmtApp.setCnmtGuaranteeWt(cnmt.getCnmtGuaranteeWt());
				if(! String.valueOf(cnmt.getCnmtKm()).equalsIgnoreCase(""))
					cnmtApp.setCnmtKm(cnmt.getCnmtKm());				
				if(! String.valueOf(cnmt.getCnmtState()).equalsIgnoreCase(""))
					cnmtApp.setCnmtStateCode(cnmt.getCnmtState());
				if(! String.valueOf(cnmt.getCnmtPayAt()).equalsIgnoreCase(""))
					cnmtApp.setCnmtPayAtCode(cnmt.getCnmtPayAt());
				if(! String.valueOf(cnmt.getCnmtBillAt()).equalsIgnoreCase(""))
					cnmtApp.setCnmtBillAtCode(cnmt.getCnmtBillAt());
				cnmtApp.setCnmtRate(cnmt.getCnmtRate());
				cnmtApp.setCnmtNoOfPkg(cnmt.getCnmtNoOfPkg());
				cnmtApp.setCnmtFreight(cnmt.getCnmtFreight());
				cnmtApp.setCnmtTOT(cnmt.getCnmtTOT());
				cnmtApp.setCnmtEmpCode(cnmt.getCnmtEmpCode());
				if(! String.valueOf(cnmt.getCnmtExtraExp()).equalsIgnoreCase(""))
					cnmtApp.setCnmtExtraExp(Double.parseDouble(String.valueOf(cnmt.getCnmtExtraExp())));
				cnmtApp.setCnmtContCode(cnmt.getContractCode());
				//cnmtApp.setCnmtDtdDly(cnmt.get);
				//cnmtApp.setCnmtDc(cnmt.getCnmtDC());
				//cnmtApp.setCnmtCostGrade(cnmt.getcnmt);
				
				//cnmtApp.setCnmtInvoiceNo(cnmt.getCnmtInvoiceNo());
				//cnmtApp.setCnmtInvoiceDt(cnmtInvoiceDate);
								
				cnmtApp.setCnmtUserCode(user.getUserCode());
				cnmtApp.setCnmtBCode(user.getUserBranchCode());
				cnmtApp.setCnmtBranchCode(user.getUserBranchCode());
						
				cnmtApp = fillFaCode(cnmtApp);
				session = this.sessionFactory.openSession();
				transaction = session.beginTransaction();
				session.merge(cnmtApp);				
			}
		}catch(Exception e){
			System.out.println("Error in updatePendingCnmtChange - WebServiceDAOImpl : "+e);
		}
		transaction.commit();
		session.flush();
		session.clear();
		session.close();
	}
	
	@Override
	public Boolean isCnmtAvailable(String cnmtCode){
		logger.info("Enter into isCnmtAvailable()...");
		Boolean available = false;		
		try{
			session = this.sessionFactory.openSession();
			List<Cnmt> cnmtList = session.createCriteria(Cnmt.class)
					.add(Restrictions.eq(CnmtCNTS.CNMT_CODE, cnmtCode))
					.list();
			if(cnmtList.isEmpty()){
				List<CnmtApp> cnmtAppList = session.createCriteria(CnmtApp.class)
						.add(Restrictions.eq(CnmtAppCNTS.CNMT_CODE, cnmtCode))
						.list();
				if(cnmtAppList.isEmpty()){
					available = true;
					logger.info("CNMT does not exist !");
				}else{
					available = false;
					logger.info("CNMT exists !");
				}
			}else{
				available = false;
				logger.info("CNMT exists !");
			}
		}catch(Exception e){
			logger.error("Exception : "+e);
		}
		session.flush();
		session.clear();
		session.close();
		logger.info("Exit from isCnmtAvailable()...");
		return available;
	}
	
	public Map<String, Object> getEntryCount(Map<String, Object> initParam){
		logger.info("Enter into getEntryCount()....");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try{
			String brhCode = String.valueOf(initParam.get("brhCode"));			
			String date = String.valueOf(initParam.get("date"));
			logger.info("Branch Code : "+brhCode +" :: Date : "+date);
			
			session = this.sessionFactory.openSession();			
			
			// CNMT Count
			Criteria cr = session.createCriteria(CnmtApp.class);
			if(! brhCode.equalsIgnoreCase(""))
				cr.add(Restrictions.eq(CnmtAppCNTS.CNMT_BRANCH_CODE, brhCode));
			cr.add(Restrictions.sqlRestriction("creationTS LIKE '%"+date+"%' "));			
			cr.setProjection(Projections.rowCount());
			Long cnmtCount = (Long)cr.uniqueResult();
			
			// Challan Count
			cr = session.createCriteria(ChallanApp.class);
			if(! brhCode.equalsIgnoreCase(""))
				cr.add(Restrictions.eq(ChallanAppCNTS.BRANCH_CODE, brhCode));
			cr.add(Restrictions.sqlRestriction("creationTS LIKE '%"+date+"%' "));
			cr.setProjection(Projections.rowCount());
			Long chlnCount = (Long)cr.uniqueResult();
			
			// Owner Count
			cr = session.createCriteria(OwnBrkApp.class);
			if(! brhCode.equalsIgnoreCase(""))
				cr.add(Restrictions.eq(OwnBrkAppCNTS.BRAN_CHCODE, brhCode));
			cr.add(Restrictions.eq(OwnBrkAppCNTS.TYPE, "owner"));			
			cr.add(Restrictions.sqlRestriction("creationTS LIKE '%"+date+"%' "));
			cr.setProjection(Projections.rowCount());
			Long ownCount = (Long)cr.uniqueResult();
			
			// Broker Count
			cr = session.createCriteria(OwnBrkApp.class);
			if(! brhCode.equalsIgnoreCase(""))
				cr.add(Restrictions.eq(OwnBrkAppCNTS.BRAN_CHCODE, brhCode));
			cr.add(Restrictions.eq(OwnBrkAppCNTS.TYPE, "broker"));			
			cr.add(Restrictions.sqlRestriction("creationTS LIKE '%"+date+"%' "));
			cr.setProjection(Projections.rowCount());
			Long brkCount = (Long)cr.uniqueResult();
			
			// Vehicle Count
			cr = session.createCriteria(VehicleApp.class);
			if(! brhCode.equalsIgnoreCase(""))
				cr.add(Restrictions.eq(VehicleAppCNTS.BRANCH_CODE, brhCode));						
			cr.add(Restrictions.sqlRestriction("creationTS LIKE '%"+date+"%' "));
			cr.setProjection(Projections.rowCount());
			Long vhCount = (Long)cr.uniqueResult();			
			
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			
			resultMap.put("cnmtCount", cnmtCount);
			resultMap.put("chlnCount", chlnCount);
			resultMap.put("ownCount", ownCount);
			resultMap.put("brkCount", brkCount);
			resultMap.put("vhCount", vhCount);
			
		}catch(Exception e){
			logger.error("Exception : "+e);
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", "Database Exception !");
		}
		session.flush();
		session.clear();
		session.close();
		logger.info("Exit from getEntryCount()....");
		return resultMap;
	}
	
	@Override
	public Map<String, Object> saveOrUpdateOTP(String phoneNo, String otp){
		Map<String, Object> otpMap=new HashMap<>();
		System.out.println("Phone no.="+phoneNo+" OTP No.="+otp);
		Session session =sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		try {
			/*List<AppUser> userList=session.createCriteria(AppUser.class)
					.add(Restrictions.eq("appUserPhoneNo", phoneNo)).list();*/
//					.add(Restrictions.eq("appUserStatus", "Active"))
			
			List<AppLoginOTP> otpList=session.createCriteria(AppLoginOTP.class)
					.add(Restrictions.eq("phoneNo", phoneNo)).list();
			
			if(otpList.isEmpty()) {
				AppLoginOTP au=new AppLoginOTP();
				au.setPhoneNo(phoneNo);
				au.setOtp(otp);
				session.save(au);
				otpMap.put("isSuccess", true);
				otpMap.put("message", "OTP has been sent successfully");
				//otpMap.put("otp", otp);"{
				

			}else {
				AppLoginOTP au=otpList.get(0);
				//au.setAppUserPhoneNo(phoneNo);
				//au.setAppUserStatus("InActive");
				au.setOtp(otp);
				session.update(au);
				otpMap.put("isSuccess", true);
				otpMap.put("message", "OTP has been sent successfully");
			}
			session.flush();
			session.clear();
			transaction.commit();
		}catch(Exception e) {
			logger.info("Exception in saveAndReturnOTP"+e);
			e.printStackTrace();
			transaction.rollback();
			otpMap.put("isSuccess", false);
			otpMap.put("message", "OTP not saved");
		}finally {
			session.close();
		}
		
		return otpMap;
	}
	
	
	/*@Override
	public Map<String, Object> verifyOTP(String phoneNo, String otp){
		Map<String, Object> otpMap=new HashMap<>();
		System.out.println("Phone no.="+phoneNo+" OTP No.="+otp);
		Session session =sessionFactory.openSession();
		try {
			List<AppLoginOTP> userList=session.createCriteria(AppLoginOTP.class)
					.add(Restrictions.eq("phoneNo", phoneNo))
					.add(Restrictions.eq("otp", otp)).list();
			
			if(!userList.isEmpty()) {
				
				Transaction transaction=session.beginTransaction();
				AppLoginOTP app=userList.get(0);
				app.setOtp(null);
				session.update(app);
				
				List<Supplier> apUsrList=session.createCriteria(Supplier.class)
						.add(Restrictions.eq("phoneNo", phoneNo)).list();
				if(apUsrList.isEmpty()) {
					
					List<Owner> ownList=session.createCriteria(Owner.class)
							.add(Restrictions.eq("ownPhone", phoneNo)).list();
					
					
					Supplier appUser=new Supplier();
					
					if(!ownList.isEmpty()) {
						Owner own=ownList.get(0);
						appUser.setAcntHldrName(own.getOwnAcntHldrName());
						appUser.setAcntNo(own.getOwnAccntNo());
						appUser.setBankName(own.getOwnBnkBranch());
						appUser.setIfsc(own.getOwnIfsc());
						appUser.setName(own.getOwnName());
						appUser.setPanImgUplded(own.isOwnIsPanImg());
					}
					
					
					appUser.setPhoneNo(phoneNo);
					appUser.setAcntStatus("pending");
					appUser.setAdresStatus("pending");
					appUser.setPanStatus("pending");
					appUser.setDecStatus("pending");
					Integer auId=(Integer)session.save(appUser);
					//session.update(appUser);
					
					SupplierApp supA=new SupplierApp();
					supA.setAcnt_hldr_name(appUser.getAcntHldrName());
					supA.setAcnt_no(appUser.getAcntNo());
					supA.setAcnt_status(appUser.getAcntStatus());
					supA.setAdres_status(appUser.getAdresStatus());
					supA.setBank_name(appUser.getBankName());
					supA.setBranch_name(appUser.getBranchName());
					supA.setCity(appUser.getCity());
					supA.setCreationTS(appUser.getCreationTS());
					supA.setDistrict(appUser.getDistrict());
					supA.setIfsc(appUser.getIfsc());
					supA.setIs_acnt_prf_uplded(appUser.isAcntPrfUplded());
					supA.setIs_kyc_verified(appUser.isKycVerified());
					supA.setIs_pan_img_uplded(appUser.isPanImgUplded());
					supA.setKyc_doc_id(appUser.getKycDocId());
					supA.setName(appUser.getName());
					supA.setPan_no(appUser.getPanNo());
					supA.setPan_status(appUser.getPanStatus());
					supA.setPhone_no(appUser.getPhoneNo());
					supA.setPhone_no2(appUser.getPhoneNo2());
					supA.setPincode(appUser.getPinCode());
					supA.setState(appUser.getState());
					supA.setStreet_plot_no(appUser.getStreetPlotNo());
					supA.setSup_id(appUser.getSupId());
					supA.setUser_status(appUser.getUserStatus());
					supA.setAdres_prf_type(appUser.getAdresPrfType());
					supA.setDec_status(appUser.getDecStatus());
					supA.setType(appUser.getType());
					supA.setIs_adrs_prf_uplded(appUser.isAdrsPrfImgUplded());
					supA.setIs_dec_img_uplded(appUser.isDecImgUplded());
					supA.setFirm_name(appUser.getFirmName());
					supA.setFirm_type(appUser.getFirmType());
					supA.setPan_name(appUser.getPanName());
					
					otpMap.put("data", supA);
				}else {
					Supplier appUser=apUsrList.get(0);
					
					SupplierApp supA=new SupplierApp();
					supA.setAcnt_hldr_name(appUser.getAcntHldrName());
					supA.setAcnt_no(appUser.getAcntNo());
					supA.setAcnt_status(appUser.getAcntStatus());
					supA.setAdres_status(appUser.getAdresStatus());
					supA.setBank_name(appUser.getBankName());
					supA.setBranch_name(appUser.getBranchName());
					supA.setCity(appUser.getCity());
					supA.setCreationTS(appUser.getCreationTS());
					supA.setDistrict(appUser.getDistrict());
					supA.setIfsc(appUser.getIfsc());
					supA.setIs_acnt_prf_uplded(appUser.isAcntPrfUplded());
					supA.setIs_kyc_verified(appUser.isKycVerified());
					supA.setIs_pan_img_uplded(appUser.isPanImgUplded());
					supA.setKyc_doc_id(appUser.getKycDocId());
					supA.setName(appUser.getName());
					supA.setPan_no(appUser.getPanNo());
					supA.setPan_status(appUser.getPanStatus());
					supA.setPhone_no(appUser.getPhoneNo());
					supA.setPhone_no2(appUser.getPhoneNo2());
					supA.setPincode(appUser.getPinCode());
					supA.setState(appUser.getState());
					supA.setStreet_plot_no(appUser.getStreetPlotNo());
					supA.setSup_id(appUser.getSupId());
					supA.setUser_status(appUser.getUserStatus());
					supA.setAdres_prf_type(appUser.getAdresPrfType());
					supA.setDec_status(appUser.getDecStatus());
					supA.setType(appUser.getType());
					supA.setIs_adrs_prf_uplded(appUser.isAdrsPrfImgUplded());
					supA.setIs_dec_img_uplded(appUser.isDecImgUplded());
					supA.setFirm_name(appUser.getFirmName());
					supA.setFirm_type(appUser.getFirmType());
					supA.setPan_name(appUser.getPanName());
					
					
					otpMap.put("data", supA);
				}
				
				
				
				session.flush();
				transaction.commit();
				otpMap.put("isSuccess", true);
				otpMap.put("message", "OTP verified successfully");
				
			
			}else {
				otpMap.put("isSuccess", false);
				otpMap.put("message", "OTP doesn't match");
				otpMap.put("data", null);
			}
		}catch(Exception e) {
			logger.info("Exception in verifyOTP"+e);
			otpMap.put("isSuccess", false);
			otpMap.put("message", "Something going wrong");
			otpMap.put("data", null);
			e.printStackTrace();
		}finally {
			session.clear();
			session.close();
		}
		
		return otpMap;
	}
	
	@Override
	public Map<String, Object> getSupKYC(int id){
		Map<String, Object> resultMap=new HashMap<>();
		Session session=sessionFactory.openSession();
		try {
			List<Supplier> apUsrList=session.createCriteria(Supplier.class)
					.add(Restrictions.eq("supId", id)).list();
			if(!apUsrList.isEmpty()) {

				Supplier appUser=apUsrList.get(0);
				
				SupplierApp supA=new SupplierApp();
				supA.setAcnt_hldr_name(appUser.getAcntHldrName());
				supA.setAcnt_no(appUser.getAcntNo());
				supA.setAcnt_status(appUser.getAcntStatus());
				supA.setAdres_status(appUser.getAdresStatus());
				supA.setBank_name(appUser.getBankName());
				supA.setBranch_name(appUser.getBranchName());
				supA.setCity(appUser.getCity());
				supA.setCreationTS(appUser.getCreationTS());
				supA.setDistrict(appUser.getDistrict());
				supA.setIfsc(appUser.getIfsc());
				supA.setIs_acnt_prf_uplded(appUser.isAcntPrfUplded());
				supA.setIs_kyc_verified(appUser.isKycVerified());
				supA.setIs_pan_img_uplded(appUser.isPanImgUplded());
				supA.setKyc_doc_id(appUser.getKycDocId());
				supA.setName(appUser.getName());
				supA.setPan_no(appUser.getPanNo());
				supA.setPan_status(appUser.getPanStatus());
				supA.setPhone_no(appUser.getPhoneNo());
				supA.setPhone_no2(appUser.getPhoneNo2());
				supA.setPincode(appUser.getPinCode());
				supA.setState(appUser.getState());
				supA.setStreet_plot_no(appUser.getStreetPlotNo());
				supA.setSup_id(appUser.getSupId());
				supA.setUser_status(appUser.getUserStatus());
				supA.setAdres_prf_type(appUser.getAdresPrfType());
				supA.setDec_status(appUser.getDecStatus());
				supA.setType(appUser.getType());
				supA.setIs_adrs_prf_uplded(appUser.isAdrsPrfImgUplded());
				supA.setIs_dec_img_uplded(appUser.isDecImgUplded());
				supA.setFirm_name(appUser.getFirmName());
				supA.setFirm_type(appUser.getFirmType());
				supA.setPan_name(appUser.getPanName());
						
				
				resultMap.put("data", supA);
				resultMap.put("isSuccess", true);
			
			}else {
				resultMap.put("isSuccess", false);
				resultMap.put("message", "Record not found");
				resultMap.put("data", null);
			}
		}catch(Exception e) {
			e.printStackTrace();
			resultMap.put("isSuccess", false);
			resultMap.put("message", "Something going wrong");
			resultMap.put("data", null);
		}finally {
			session.clear();
			session.close();
		}
		
		return resultMap;
	}
	
	
	
	@Override
	public void updateSupplier(Supplier supplier,Session session){
		session.update(supplier);
	}
	@Override
	public void updateSupplierDocs(SupKycDocs supplier,Session session){
		session.update(supplier);
	}
	@Override
	public int saveSupplierDocs(SupKycDocs supplier,Session session){
		return (int)session.save(supplier);
	}
	*/
	
	
}