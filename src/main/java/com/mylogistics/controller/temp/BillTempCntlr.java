package com.mylogistics.controller.temp;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.temp.BillTempDAO;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.temp.BillTempService;

@Controller
public class BillTempCntlr {

	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private BillTempDAO billTempDAO;
	
	@RequestMapping(value = "/saveBillTemp" , method = RequestMethod.POST)  
	public @ResponseBody Object saveBillTemp(@RequestBody BillTempService billTempService) {  
		System.out.println("Enter into submitAtmVouch---->");
		Map<String, Object> map = new HashMap<>();	
		
		int temp = billTempDAO.saveBillTemp(billTempService);
		
		if(temp > 0){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		
		return map;
	}
	
}
