package com.mylogistics.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cnmt_challan")
public class Cnmt_Challan {
	
	@Id
	@Column(name="cnmt_ChallanId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int cnmt_ChallanId;
	
	@Column(name="cnmtId")
	private int cnmtId;
	
	@Column(name="chlnId")
	private int chlnId;
	
	@Column(name="cnmtNoOfPkg", columnDefinition="int default 0")
	private int cnmtNoOfPkg;
	
	@Column(name="cnmtTotalWt", columnDefinition="double default 0.0")
	private double cnmtTotalWt;
	
	public int getCnmt_ChallanId() {
		return cnmt_ChallanId;
	}
	public void setCnmt_ChallanId(int cnmt_ChallanId) {
		this.cnmt_ChallanId = cnmt_ChallanId;
	}
	public int getCnmtId() {
		return cnmtId;
	}
	public void setCnmtId(int cnmtId) {
		this.cnmtId = cnmtId;
	}
	public int getChlnId() {
		return chlnId;
	}
	public void setChlnId(int chlnId) {
		this.chlnId = chlnId;
	}
	public int getCnmtNoOfPkg() {
		return cnmtNoOfPkg;
	}
	public void setCnmtNoOfPkg(int cnmtNoOfPkg) {
		this.cnmtNoOfPkg = cnmtNoOfPkg;
	}
	public double getCnmtTotalWt() {
		return cnmtTotalWt;
	}
	public void setCnmtTotalWt(double cnmtTotalWt) {
		this.cnmtTotalWt = cnmtTotalWt;
	}
	
}
