package com.mylogistics.DAOImpl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import com.mylogistics.DAO.IndentDAO;
import com.mylogistics.model.Indent;

public class IndentDAOImpl implements IndentDAO{

	//@Autowired
	private SessionFactory sessionFactory;
	
	//@Autowired
	private Session session;
	
	public IndentDAOImpl(SessionFactory sessionFactory) {
		super();
		this.sessionFactory = sessionFactory;
		
	}
	
	
	@Override
	public int saveNewIndent(Indent indent, Session session) {
		// TODO Auto-generated method stub
		return (int) session.save(indent);
	}

	@Override
	public void updateIndet(Indent indent, Session session) {
		// TODO Auto-generated method stub
		session.update(indent);
	}

	@Override
	public void deleteIndent(Indent indent, Session session) {
		// TODO Auto-generated method stub
		session.delete(indent);
	}

	@Override
	public boolean checkOrderIdExist(String orderId) {
		
		boolean flag=false;
		Session session = this.sessionFactory.openSession();
		try {
			Criteria cr=session.createCriteria(Indent.class)
					.add(Restrictions.eq("orderId", orderId));
			if(!cr.list().isEmpty())
				flag=true;
			
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		return flag;
	}
	
	@Override
	public Indent getIndentById(int id) {
		Session session = this.sessionFactory.openSession();
		Indent indent=(Indent) session.get(Indent.class, id);
		session.clear();
		  session.close();
		return indent;
		
	}
	
	@Override
	public List<Indent> getIndentByStatus(String status){
		Session session = this.sessionFactory.openSession();
		List<Indent> indentList= session.createCriteria(Indent.class)
				.add(Restrictions.eq("indentStatus", status))
				.add(Restrictions.isNull("indentStage")).list();
		session.clear();
		  session.close();
		return indentList;
	}
	
	@Override
	public List<Indent> getIndentByBranchAndStage(String stage, String branch) {
		Session session = this.sessionFactory.openSession();
		List<Indent> indentList = session.createCriteria(Indent.class)
				.add(Restrictions.eq("indentStage", stage))
				.add(Restrictions.eq("branchName", branch))
				.list();
		session.clear();
		session.close();
		return indentList;
	}
}
