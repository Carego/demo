<div ng-show="adminLogin || superAdminLogin">

	<div class="row" ng-show="contCodes">
		<div class="col s3 hide-on-med-and-down"> &nbsp; </div>
			<form class="col s6 card" style="align: center; margin-top: 40px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" ng-submit="contRightsView(contCode)">
				<div class="input-field col s6">
   					<input type="text" id="contcode" name="contCode" ng-model="contCode" ng-click="openContCodeDB()" readonly/>
   					<label>Enter Contract Code </label>
 				</div>
      			 		<div class="col s6 center">
	      			 		<input class="btn" type="submit" value="Submit">
      			 		</div>
			</form>
			<div class="col s3"> &nbsp; </div>
	</div>	
	
		<div id ="contractCodesDB" ng-hide="contractCodesDBFlag">
			
			<input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search by contract Code">
 	   	 	<table>
		 		<tr ng-repeat="codes in codeList | filter:filterTextbox">
			 	  	<td><input type="radio"  name="contractCode" value="{{ codes }}" ng-model="Code" ng-click="saveCode(codes)"></td>
              		<td>{{ codes }}</td>
          		</tr>
         	</table>       
		</div>
		
		<div class="row" ng-hide="contRightsDetails">
		
		<form name="ViewRightsForm" ng-submit="editContRights(ViewRightsForm,cr)">
		<h4>Here's the detail of <span class="teal-text text-lighten-2">{{cr.contCode}}</span>:</h4>
		<table class="table-hover table-bordered table-condensed">
			<tr>
				<td>Contract Code</td>
				<td>{{cr.contCode}}</td>
			</tr>
			
			<tr>
				<td>Bill Basis </td>
				<td><select name="crContBillBasis" id="crContBillBasis" ng-model="cr.crContBillBasis" ng-init="cr.crContBillBasis">
							<option value="chargeWt">Charge Weight</option>
							<option value="receiveWt">Receive Weight</option>
							<option value="actualWt">Actual Weight</option>
					</select>
				</td>
			</tr>
		</table>
				<input type="submit" value ="Submit">
		</form>
		</div>
		</div>