package com.mylogistics.model.bank;

import java.sql.Date;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.mylogistics.model.Branch;
import com.mylogistics.model.Employee;

//public class ChequeBook implements Cloneable {

@Entity
@Table(name="chequebook")
public class ChequeBook {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="chqBkId")
	private int chqBkId;
	
	@Column(name="chqBkFromChqNo")
	private String chqBkFromChqNo;
	
	@Column(name="chqBkToChqNo")
	private String chqBkToChqNo;
	
	@Column(name="chqBkNOChq")
	private int chqBkNOChq;
	
	@Column(name="chqBkIsIssued")
	private boolean chqBkIsIssued;
	
	@Column(name="chqBkIsRecByBr")
	private boolean chqBkIsRecByBr;
	
	@Column(name="chqBkIsCancelled")
	private boolean chqBkIsCancelled;
	
	@Column(name="chqBkIssueDt")
	private Date chqBkIssueDt;
	
	@Column(name="chqBkRecByBrDt")
	private Date chqBkReceByBrt;
	
	//@Column(name="chqBkPayType", nullable = false, columnDefinition = "char default 'A'")
	@Column(name="chqBkPayType")
	private char chqBkPayType; //A:ac pay and B:bearer
	
	@Column(name="chqBkMaxAmtLimit", nullable = false, columnDefinition = "int default -1")
	private int chqBkMaxAmtLimit;
	
	@ManyToOne
	@LazyCollection(LazyCollectionOption.TRUE)
	//@JsonBackReference(value = "bnkChqL")
	@JoinColumn(name = "authEmp1Id")
	private Employee authEmp1;
	
	@ManyToOne
	@LazyCollection(LazyCollectionOption.TRUE)
	//@JsonBackReference(value = "bnkChqL")
	@JoinColumn(name = "authEmp2Id")
	private Employee authEmp2;
	
	@ManyToOne
	@LazyCollection(LazyCollectionOption.TRUE)
	//@JsonBackReference(value = "bnkChqL")
	@JoinColumn(name = "branchId")
	private Branch branch;
	
	@ManyToOne
	@LazyCollection(LazyCollectionOption.TRUE)
	//@JsonBackReference(value = "bnkChqL")
	@JoinColumn(name = "bnkId")
	private BankMstr bankMstr;
	
	@ManyToOne
	@LazyCollection(LazyCollectionOption.TRUE)
	//@JsonBackReference(value = "bnkChqL")
	@JoinColumn(name = "cReqId")
	private ChequeRequest chequeRequest;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;

	public int getChqBkId() {
		return chqBkId;
	}

	public void setChqBkId(int chqBkId) {
		this.chqBkId = chqBkId;
	}

	public String getChqBkFromChqNo() {
		return chqBkFromChqNo;
	}

	public void setChqBkFromChqNo(String chqBkFromChqNo) {
		this.chqBkFromChqNo = chqBkFromChqNo;
	}

	public String getChqBkToChqNo() {
		return chqBkToChqNo;
	}

	public void setChqBkToChqNo(String chqBkToChqNo) {
		this.chqBkToChqNo = chqBkToChqNo;
	}

	public int getChqBkNOChq() {
		return chqBkNOChq;
	}

	public void setChqBkNOChq(int chqBkNOChq) {
		this.chqBkNOChq = chqBkNOChq;
	}

	public boolean isChqBkIsIssued() {
		return chqBkIsIssued;
	}

	public void setChqBkIsIssued(boolean chqBkIsIssued) {
		this.chqBkIsIssued = chqBkIsIssued;
	}

	public Date getChqBkIssueDt() {
		return chqBkIssueDt;
	}

	public void setChqBkIssueDt(Date chqBkIssueDt) {
		this.chqBkIssueDt = chqBkIssueDt;
	}

	public boolean isChqBkIsRecByBr() {
		return chqBkIsRecByBr;
	}

	public void setChqBkIsRecByBr(boolean chqBkIsRecByBr) {
		this.chqBkIsRecByBr = chqBkIsRecByBr;
	}

	public boolean isChqBkIsCancelled() {
		return chqBkIsCancelled;
	}

	public void setChqBkIsCancelled(boolean chqBkIsCancelled) {
		this.chqBkIsCancelled = chqBkIsCancelled;
	}

	public Date getChqBkReceByBrt() {
		return chqBkReceByBrt;
	}

	public void setChqBkReceByBrt(Date chqBkReceByBrt) {
		this.chqBkReceByBrt = chqBkReceByBrt;
	}

	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	public ChequeRequest getChequeRequest() {
		return chequeRequest;
	}

	public void setChequeRequest(ChequeRequest chequeRequest) {
		this.chequeRequest = chequeRequest;
	}

	public BankMstr getBankMstr() {
		return bankMstr;
	}

	public void setBankMstr(BankMstr bankMstr) {
		this.bankMstr = bankMstr;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public char getChqBkPayType() {
		return chqBkPayType;
	}

	public void setChqBkPayType(char chqBkPayType) {
		this.chqBkPayType = chqBkPayType;
	}

	public int getChqBkMaxAmtLimit() {
		return chqBkMaxAmtLimit;
	}

	public void setChqBkMaxAmtLimit(int chqBkMaxAmtLimit) {
		this.chqBkMaxAmtLimit = chqBkMaxAmtLimit;
	}

	public Employee getAuthEmp1() {
		return authEmp1;
	}

	public void setAuthEmp1(Employee authEmp1) {
		this.authEmp1 = authEmp1;
	}

	public Employee getAuthEmp2() {
		return authEmp2;
	}

	public void setAuthEmp2(Employee authEmp2) {
		this.authEmp2 = authEmp2;
	}

	//for cloning the object
	/*@Override
	public ChequeBook clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return (ChequeBook) super.clone();
	}*/
	
}
