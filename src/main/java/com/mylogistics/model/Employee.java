package com.mylogistics.model;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;



@Entity
@Table(name = "employee")
public class Employee {
	
	@Id
	@Column(name="empId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int empId;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="empCode")
	private String empCode;
	
	@Column(name="empName")
	private String empName;
	
	@Column(name="empAdd")
	private String empAdd;
	
	@Column(name="empCity")
	private String empCity;

    @Column(name="empState")
	private String empState;
    
    @Column(name="empPin")
	private String empPin;
    
    @Column(name="empDOB")
	private Date empDOB;
    
    @Column(name="empDOAppointment")
	private Date empDOAppointment;
    
    @Column(name="empDOResig")
	private Date empDOResig;

	@Column(name="empDesignation")
	private String empDesignation;
    
    @Column(name="empBasic")
	private double empBasic;
    
    @Column(name="empHRA")
	private double empHRA;
    
    @Column(name="empOtherAllowance")
	private double empOtherAllowance;
    
    @Column(name="empPFNo")
	private String empPFNo;
    
    @Column(name="empESINo")
	private String empESINo;
    
    @Column(name="empESIDispensary")
	private String empESIDispensary;
    
    @Column(name="empNominee")
	private String empNominee;
    
    @Column(name="empEduQuali")
	private String empEduQuali;
    
    @Column(name="empPF")
	private double empPF;
	
	@Column(name="empPFEmplr",columnDefinition="double default 0.0")
	private double empPFEmplr;
    
    @Column(name="empESI")
	private double empESI;
    
	@Column(name="empESIEmplr" , columnDefinition="double default 0.0")
	private double empESIEmplr;
	
	@Column(name="empTds" , columnDefinition="double default 0.0")
	private double empTds;
    
    @Column(name="empLoanBal")
	private double empLoanBal;
    
    @Column(name="empLoanPayment")
	private double empLoanPayment;
    
    @Column(name="empPresentAdd")
	private String empPresentAdd;
    
    @Column(name="empPresentCity")
	private String empPresentCity;
    
    @Column(name="empPresentState")
	private String empPresentState;
    
    @Column(name="empPresentPin")
	private String empPresentPin;
    
    @Column(name="empSex")
	private String empSex;
    
    @Column(name="empFatherName")
	private String empFatherName;
    
    @Column(name="empLicenseNo")
	private String empLicenseNo;
    
    @Column(name="empLicenseExp")
	private Date empLicenseExp;
    
    @Column(name="bCode")
	private String bCode;
    
    @Column(name="isView")
	private boolean isView;
    
    @Column(name="isTerminate")
	private String isTerminate;
   
	@Column(name="empTelephoneAmt")
	private double empTelephoneAmt;
    
    @Column(name="empMobAmt")
	private double empMobAmt;
    
    @Column(name="empBankAcNo")
   	private String empBankAcNo;
    
    @Column(name="empPanNo")
	private String empPanNo;
    
    @Column(name="empMailId")
	private String empMailId;
    
    @Column(name="empPhNo")
	private String empPhNo;
    
    @Column(name="empBankIFS")
	private String empBankIFS;
    
    @Column(name="empBankName")
	private String empBankName;
    
    @Column(name="empBankBranch")
	private String empBankBranch;

	@Column(name="branchCode")
	private String branchCode;
    
    @Column(name="empGross")
	private double empGross;
    
    @Column(name="netSalary")
	private double netSalary;
    
    @Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
    private Calendar creationTS;

    @Column(name="empCodeTemp")
	private String empCodeTemp;
    
    @Column(name="empFaCode")
	private String empFaCode;
    
    @Column
    private String empPassword;
    
    
    //Many To One
    @ManyToOne
	@LazyCollection(LazyCollectionOption.TRUE)
	@JoinColumn(name = "branchId") 
	private Branch prBranch;
    
    //One To Many
    @JsonIgnore
  	@JsonManagedReference(value = "eleSeBr")
  	@LazyCollection(LazyCollectionOption.TRUE)
  	@OneToMany(cascade = {CascadeType.ALL})
    @JoinTable(name="emp_br_access",
      		joinColumns=@JoinColumn(name="empId"),
              inverseJoinColumns=@JoinColumn(name="branchId"))
  	private List<Branch> seBranch = new ArrayList<Branch>();
    
    
    //One To Many
    @JsonIgnore
  	@JsonManagedReference(value = "eleEmp")
  	@LazyCollection(LazyCollectionOption.TRUE)
  	@OneToMany(cascade = {CascadeType.ALL})
    @JoinTable(name="employee_electricitymstr",
      		joinColumns=@JoinColumn(name="empId"),
              inverseJoinColumns=@JoinColumn(name="emId"))
  	private List<ElectricityMstr> electricityMstrList = new ArrayList<ElectricityMstr>();
    
    //One To Many
    @JsonIgnore
  	@JsonManagedReference(value = "telEmp")
  	@LazyCollection(LazyCollectionOption.TRUE)
  	@OneToMany(cascade = {CascadeType.ALL})
    @JoinTable(name="employee_telephonemstr",
    		   joinColumns=@JoinColumn(name="empId"),
              inverseJoinColumns=@JoinColumn(name="tmId"))
  	private List<TelephoneMstr> telephoneMstr = new ArrayList<TelephoneMstr>();
    
  //One To Many
    @JsonIgnore
  	@JsonManagedReference(value = "vehEmp")
  	@LazyCollection(LazyCollectionOption.TRUE)
  	@OneToMany(cascade = {CascadeType.ALL})
      @JoinTable(name="employee_vehiclemstr",
      		joinColumns=@JoinColumn(name="empId"),
              inverseJoinColumns=@JoinColumn(name="vehId"))
  	private List<VehicleMstr> vehicleMstrList = new ArrayList<VehicleMstr>();
  	
  //One To Many
    @JsonIgnore
  	@JsonManagedReference(value = "phnEmp")
  	@LazyCollection(LazyCollectionOption.TRUE)
  	@OneToMany(cascade = {CascadeType.ALL})
      @JoinTable(name="employee_phonemstr",
      		joinColumns=@JoinColumn(name="empId"),
              inverseJoinColumns=@JoinColumn(name="phId"))
  	private List<PhoneMstr> phoneMstrList = new ArrayList<PhoneMstr>();
    
    
  //One To Many
    @JsonIgnore
  	@JsonManagedReference(value = "ftdEmp")
  	@LazyCollection(LazyCollectionOption.TRUE)
  	@OneToMany(cascade = {CascadeType.ALL})
      @JoinTable(name="employee_fatravdivision",
      		joinColumns=@JoinColumn(name="empId"),
              inverseJoinColumns=@JoinColumn(name="ftdId"))
  	private List<FaTravDivision> ftdList = new ArrayList<FaTravDivision>();
  	
  	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToOne(cascade = CascadeType.ALL)
    private FAParticular fAParticular;
  	
  	 public List<TelephoneMstr> getTelephoneMstr() {
		return telephoneMstr;
	}

	public void setTelephoneMstr(List<TelephoneMstr> telephoneMstr) {
		this.telephoneMstr = telephoneMstr;
	}

	//getters and setters
	
	
	public int getEmpId() {
		return empId;
	}

	public List<FaTravDivision> getFtdList() {
		return ftdList;
	}

	public void setFtdList(List<FaTravDivision> ftdList) {
		this.ftdList = ftdList;
	}

	public List<ElectricityMstr> getElectricityMstrList() {
		return electricityMstrList;
	}

	public void setElectricityMstrList(List<ElectricityMstr> electricityMstrList) {
		this.electricityMstrList = electricityMstrList;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getEmpCode() {
		return empCode;
	}

	public void setEmpCode(String empCode) {
		this.empCode = empCode;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getEmpAdd() {
		return empAdd;
	}

	public void setEmpAdd(String empAdd) {
		this.empAdd = empAdd;
	}

	public String getEmpCity() {
		return empCity;
	}

	public void setEmpCity(String empCity) {
		this.empCity = empCity;
	}

	public String getEmpState() {
		return empState;
	}

	public void setEmpState(String empState) {
		this.empState = empState;
	}

	public String getEmpPin() {
		return empPin;
	}

	public void setEmpPin(String empPin) {
		this.empPin = empPin;
	}

	public Date getEmpDOB() {
		return empDOB;
	}

	public void setEmpDOB(Date empDOB) {
		this.empDOB = empDOB;
	}

	public Date getEmpDOAppointment() {
		return empDOAppointment;
	}

	public void setEmpDOAppointment(Date empDOAppointment) {
		this.empDOAppointment = empDOAppointment;
	}

	public Date getEmpDOResig() {
		return empDOResig;
	}

	public void setEmpDOResig(Date empDOResig) {
		this.empDOResig = empDOResig;
	}

	public String getEmpDesignation() {
		return empDesignation;
	}

	public void setEmpDesignation(String empDesignation) {
		this.empDesignation = empDesignation;
	}

	public double getEmpBasic() {
		return empBasic;
	}

	public void setEmpBasic(double empBasic) {
		this.empBasic = empBasic;
	}

	public double getEmpHRA() {
		return empHRA;
	}

	public void setEmpHRA(double empHRA) {
		this.empHRA = empHRA;
	}

	public double getEmpOtherAllowance() {
		return empOtherAllowance;
	}

	public void setEmpOtherAllowance(double empOtherAllowance) {
		this.empOtherAllowance = empOtherAllowance;
	}

	public String getEmpPFNo() {
		return empPFNo;
	}

	public void setEmpPFNo(String empPFNo) {
		this.empPFNo = empPFNo;
	}

	public String getEmpESINo() {
		return empESINo;
	}

	public void setEmpESINo(String empESINo) {
		this.empESINo = empESINo;
	}

	public String getEmpESIDispensary() {
		return empESIDispensary;
	}

	public void setEmpESIDispensary(String empESIDispensary) {
		this.empESIDispensary = empESIDispensary;
	}

	public String getEmpNominee() {
		return empNominee;
	}

	public void setEmpNominee(String empNominee) {
		this.empNominee = empNominee;
	}

	public String getEmpEduQuali() {
		return empEduQuali;
	}

	public void setEmpEduQuali(String empEduQuali) {
		this.empEduQuali = empEduQuali;
	}

	public double getEmpPF() {
		return empPF;
	}

	public void setEmpPF(double empPF) {
		this.empPF = empPF;
	}

	public double getEmpESI() {
		return empESI;
	}

	public void setEmpESI(double empESI) {
		this.empESI = empESI;
	}

	public double getEmpLoanBal() {
		return empLoanBal;
	}

	public void setEmpLoanBal(double empLoanBal) {
		this.empLoanBal = empLoanBal;
	}

	public double getEmpLoanPayment() {
		return empLoanPayment;
	}

	public void setEmpLoanPayment(double empLoanPayment) {
		this.empLoanPayment = empLoanPayment;
	}

	public String getEmpPresentAdd() {
		return empPresentAdd;
	}

	public void setEmpPresentAdd(String empPresentAdd) {
		this.empPresentAdd = empPresentAdd;
	}

	public String getEmpPresentCity() {
		return empPresentCity;
	}

	public void setEmpPresentCity(String empPresentCity) {
		this.empPresentCity = empPresentCity;
	}

	public String getEmpPresentState() {
		return empPresentState;
	}

	public void setEmpPresentState(String empPresentState) {
		this.empPresentState = empPresentState;
	}

	public String getEmpPresentPin() {
		return empPresentPin;
	}

	public void setEmpPresentPin(String empPresentPin) {
		this.empPresentPin = empPresentPin;
	}

	public String getEmpSex() {
		return empSex;
	}

	public void setEmpSex(String empSex) {
		this.empSex = empSex;
	}

	public String getEmpFatherName() {
		return empFatherName;
	}

	public void setEmpFatherName(String empFatherName) {
		this.empFatherName = empFatherName;
	}

	public String getEmpLicenseNo() {
		return empLicenseNo;
	}

	public void setEmpLicenseNo(String empLicenseNo) {
		this.empLicenseNo = empLicenseNo;
	}

	public Date getEmpLicenseExp() {
		return empLicenseExp;
	}

	public void setEmpLicenseExp(Date empLicenseExp) {
		this.empLicenseExp = empLicenseExp;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public boolean isView() {
		return isView;
	}

	public void setView(boolean isView) {
		this.isView = isView;
	}

	public String getIsTerminate() {
		return isTerminate;
	}

	public void setIsTerminate(String isTerminate) {
		this.isTerminate = isTerminate;
	}

	public double getEmpTelephoneAmt() {
		return empTelephoneAmt;
	}

	public void setEmpTelephoneAmt(double empTelephoneAmt) {
		this.empTelephoneAmt = empTelephoneAmt;
	}

	public double getEmpMobAmt() {
		return empMobAmt;
	}

	public void setEmpMobAmt(double empMobAmt) {
		this.empMobAmt = empMobAmt;
	}

	public String getEmpBankAcNo() {
		return empBankAcNo;
	}

	public void setEmpBankAcNo(String empBankAcNo) {
		this.empBankAcNo = empBankAcNo;
	}

	public String getEmpPanNo() {
		return empPanNo;
	}

	public void setEmpPanNo(String empPanNo) {
		this.empPanNo = empPanNo;
	}

	public String getEmpMailId() {
		return empMailId;
	}

	public void setEmpMailId(String empMailId) {
		this.empMailId = empMailId;
	}

	public String getEmpPhNo() {
		return empPhNo;
	}

	public void setEmpPhNo(String empPhNo) {
		this.empPhNo = empPhNo;
	}

	public String getEmpBankIFS() {
		return empBankIFS;
	}

	public void setEmpBankIFS(String empBankIFS) {
		this.empBankIFS = empBankIFS;
	}

	public String getEmpBankName() {
		return empBankName;
	}

	public void setEmpBankName(String empBankName) {
		this.empBankName = empBankName;
	}

	public String getEmpBankBranch() {
		return empBankBranch;
	}

	public void setEmpBankBranch(String empBankBranch) {
		this.empBankBranch = empBankBranch;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public double getEmpGross() {
		return empGross;
	}

	public void setEmpGross(double empGross) {
		this.empGross = empGross;
	}

	public double getNetSalary() {
		return netSalary;
	}

	public void setNetSalary(double netSalary) {
		this.netSalary = netSalary;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public String getEmpCodeTemp() {
		return empCodeTemp;
	}

	public void setEmpCodeTemp(String empCodeTemp) {
		this.empCodeTemp = empCodeTemp;
	}

	public List<VehicleMstr> getVehicleMstrList() {
		return vehicleMstrList;
	}

	public void setVehicleMstrList(List<VehicleMstr> vehicleMstrList) {
		this.vehicleMstrList = vehicleMstrList;
	}

	public List<PhoneMstr> getPhoneMstrList() {
		return phoneMstrList;
	}

	public void setPhoneMstrList(List<PhoneMstr> phoneMstrList) {
		this.phoneMstrList = phoneMstrList;
	}

	public String getEmpFaCode() {
		return empFaCode;
	}

	public void setEmpFaCode(String empFaCode) {
		this.empFaCode = empFaCode;
	}

	public FAParticular getfAParticular() {
		return fAParticular;
	}

	public void setfAParticular(FAParticular fAParticular) {
		this.fAParticular = fAParticular;
	}

	public Branch getPrBranch() {
		return prBranch;
	}

	public void setPrBranch(Branch prBranch) {
		this.prBranch = prBranch;
	}

	public List<Branch> getSeBranch() {
		return seBranch;
	}

	public void setSeBranch(List<Branch> seBranch) {
		this.seBranch = seBranch;
	}

	public double getEmpPFEmplr() {
		return empPFEmplr;
	}

	public void setEmpPFEmplr(double empPFEmplr) {
		this.empPFEmplr = empPFEmplr;
	}

	public double getEmpESIEmplr() {
		return empESIEmplr;
	}

	public void setEmpESIEmplr(double empESIEmplr) {
		this.empESIEmplr = empESIEmplr;
	}

	public double getEmpTds() {
		return empTds;
	}

	public void setEmpTds(double empTds) {
		this.empTds = empTds;
	}

	public String getEmpPassword() {
		return empPassword;
	}

	public void setEmpPassword(String empPassword) {
		this.empPassword = empPassword;
	}
  		
}
