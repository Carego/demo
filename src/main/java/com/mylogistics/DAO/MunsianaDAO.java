package com.mylogistics.DAO;

import org.springframework.stereotype.Repository;

@Repository
public interface MunsianaDAO {

	public double getMunsAmt(double adv);
	
}
