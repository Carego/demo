package com.mylogistics.DAOImpl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.FaCarDivisionDAO;
import com.mylogistics.model.FaCarDivision;

public class FaCarDivisionDAOImpl implements FaCarDivisionDAO{

	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	public FaCarDivisionDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	 @Transactional
	 @SuppressWarnings("unchecked")
	 public int saveFaCarD(FaCarDivision faCarDivision){
		 System.out.println("Enter into saveFaCarD function--");
		 int temp = 0;
		 try{
			 session = this.sessionFactory.openSession();
	         transaction = session.beginTransaction();
	         session.save(faCarDivision);
	         transaction.commit();
	         session.flush();
	         temp = 1;
		 }catch (Exception e) {
			 e.printStackTrace();
			 temp = -1;
		 }
		 session.clear();
		 session.close();
		 return temp;
	}
}
