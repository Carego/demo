<div ng-show="superAdminLogin">
	<div>
		<form ng-submit="unHoldDocs()" class="col s12 card" style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
<div class="row">

          <div class="col s3 input-field">
				<select name="docSer" ng-model="docSer" ng-change="changeDocs(docSer)" required>
					<option value='CN'>CNMT</option>
					<option value='CH'>CHALLAN</option>
					<option value='AR'>SEDR</option>
					<option value='LH'>LHPV</option>
					<option value='BL'>BILL</option>
					<option value='MR'>MR</option>
				</select> <label for="code">SELECT DOCUMENT TYPE:</label>
			</div>
			<div class="col s3 input-field">
				<input type="text" id="chlnCode" name="chlnCode" ng-model="chlnCode" ng-click="openChlnCodeDb()" readonly>
				<label for="code">Challan Code:</label>
			</div>


			<div class="input-field col s3">
			<input type="date" id="holdDt" name="holdDt"
					ng-model="obj.holdDt" required readonly> <label>Hold Date:</label>
			</div>
			
			</div>

			<div class="row">
				<div class="input-field col s12">
					<i class="mdi-editor-mode-edit prefix"></i>
					<textarea id="textarea" class="materialize-textarea" rows="3"
						cols="92" name="desc" ng-model="obj.remark"  required></textarea>
					<label>Description</label>
				</div>
			</div>

			<div class="row">
				<div class="input-field col s12 center">
					<input class="validate" type="submit" value="ALLOW">
				</div>
			</div>

		</form>
	</div>
	
	<div id ="chlnCodeDB" ng-hide="chlnCodeDBFlag">
		  <input type="text" name="filterArbox" ng-model="filterArbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Challan CODE</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="chlnCode in chlnCodeList | filter:filterArbox">
		 	  <td><input type="radio"  name="chlnCode"   value="{{ chlnCode }}" ng-model="chlnCodeM" ng-click="saveChln(chlnCode)"></td>
              <td>{{chlnCode}}</td>
          </tr>
      </table> 
	</div>
	

</div>