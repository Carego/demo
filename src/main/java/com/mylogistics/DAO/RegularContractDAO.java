package com.mylogistics.DAO;

import java.sql.Date;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.mylogistics.model.RegularContract;

@Repository
public interface RegularContractDAO {
	
		//Sanjana: saves the regular contract to database
		public int saveRegularContract(RegularContract regularContract);
	
		//Sanjana:retrieves regular contract from database on the basis of entered contract code
		public List<RegularContract> getRegularContract(String regContCode);
	
		//Sanjana: updates the contract
		public int updateRegularContract(RegularContract regularContract);
	
		//Sanjana: retrieves the list of regular contracts whose isVerify is 'no'
		public List<RegularContract> getRegContractisVerifyNo();
	
		//Sanjana: retrieves the last row of regular contract table
		public int getLastRegcontractId();
	 
		//Sanjana: returns the total number of rows in regular contract table
		public long totalCount();
		
		//Sanjana: retrieves the list of regular contracts whose isView is false
		public List<RegularContract> getRegContractisViewFalse(String branchCode);
		
		//Sanjana: updates the isView to true
		public int updateRegContisViewTrue(String code[]);
		
		//Sanjana:  get the list of regularContract Codes
		 public List<String> getRegularContractCode();
		 
		//Sanjana: get the list of regularContract Codes based on entered branchCode
		 public List<String> getRegularContractCodes(String branchCode);
		
		public List<RegularContract> getRegContractCode(String custCode);
		
		public List<RegularContract> getRegContForCnmt(String custCode,String cnmtFromSt);
		
		//Sanjana: get regularContract data for saving in oldRegularContract
		 public List<RegularContract> getRegularContractData(int id);
		 
		 public List<RegularContract> getRegContractData(String contCode);
		 
		 public List<String> getRegFaCode();
		 
		 public List<RegularContract> getRegContByFaCode(String faCode);
		 
		 public int extDtOfCont(String contFaCode, Date date);
		 
		 public int updateBillBase(String contCode , String billBase);
		 
		 public List<Map<String, Object>> getContList(String custId);
		 
		 public List<Map<String, Object>> getRateList(String contCode);

		public List<RegularContract> getContractByCustCodeAndDate(String custCode, Date date);

		public List<String> getContractByDate(Date date);

		public RegularContract getRegularContractByContCode(String regContCode);
}
