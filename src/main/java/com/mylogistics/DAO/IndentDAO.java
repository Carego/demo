package com.mylogistics.DAO;

import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.mylogistics.model.Indent;

@Repository
public interface IndentDAO {
	
	public int saveNewIndent(Indent indent,Session session);
	
	public void updateIndet(Indent indent,Session session);
	
	public void deleteIndent(Indent indent,Session session);
	
	public boolean checkOrderIdExist(String orderId);
	
	public Indent getIndentById(int id);
	
	public List<Indent> getIndentByStatus(String status);
	
	public List<Indent> getIndentByBranchAndStage(String stage,String branch);
	
}
