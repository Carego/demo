package com.mylogistics.services;

import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.exception.custom.BankMstrException;
import com.mylogistics.exception.custom.CashStmtStatusException;
import com.mylogistics.exception.custom.CustomerException;
import com.mylogistics.model.MoneyReceipt;

public interface MoneyReceiptService {

	@Transactional
	public String saveOAMR(MoneyReceipt moneyReceipt) 
			throws CashStmtStatusException, CustomerException, BankMstrException;
	
	@Transactional
	public String generateNewMrNo(int userBranchCode);
	
}