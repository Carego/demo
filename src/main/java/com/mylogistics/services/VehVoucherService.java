package com.mylogistics.services;

import java.util.List;

public interface VehVoucherService {

	public void addVmAndSVm(VM_SubVMService vm_SubVMService);
	
	public List<VM_SubVMService> getAllVmAndSVm();
	
	public void deleteAllVmAndSVM();
	
	public void remove(int index);
}
