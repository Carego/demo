<div ng-show="operatorLogin || superAdminLogin">
<title>CNMT Barbil</title>

	<div class="col s12 card" style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
		<form name="cnmtBblForm" ng-submit="submitCnmtBblForm(cnmtBblForm)">
			<div class="row">
				
				<div class="col s3 input-field">
		       		<input class="validate" type ="text" id="branchNameId" name ="branchName" ng-model="branch.branchName" readonly required="required">
		       	<label for="code">Branch</label>	
		       	</div>
		       	
		       	<div class="col s3 input-field">
		       		<input class="validate" type ="text" id="codeId" name ="codeName" ng-model="code" ng-keyup="openCodeDB()" required="required">
		       	<label for="code">cnmt/chln</label>	
		       	</div>
		       	
		       	<div class="col s3 input-field">
		       		<input class="validate" type ="text" id="custNameId" name ="custName" ng-model="cust.custName" ng-click="openCustDB()" readonly required="required">
		       	<label for="code">Customer</label>	
		       	</div>
		       	
		       	<div class="col s3 input-field">
		       		<input class="validate" type ="date" id="dtId" name ="dtName" ng-model="dt" ng-blur="veryFyDt()" required="required">
		       	<label for="code">Date</label>	
		       	</div>
		    </div>
		    
		    <div class="row">
				
				<div class="col s3 input-field">
		       		<input class="validate" type ="text" id="frmStnId" name ="frmStnName" ng-model="frmStn.stnName" ng-click="openFrmStnDB()" readonly required="required">
		       	<label for="code">From Stn</label>	
		       	</div>
		       	
		       	<div class="col s3 input-field">
		       		<input class="validate" type ="text" id="contCodeId" name ="contCodeName" ng-model="cont.contCode" ng-click="openContCodeDB()" readonly required="required" disabled="disabled">
		       	<label for="code">Cont Code</label>	
		       	</div>
		       	
		       	<div class="col s3 input-field">
		       		<input class="validate" type ="text" id="toStnId" name ="toStnName" ng-model="toStn.stnName" ng-click="openToStnDB()" readonly required="required">
		       	<label for="code">To Stn</label>	
		       	</div>
		       	
		       	<div class="col s3 input-field">
		       		<input class="validate" type ="text" id="empNameId" name ="empName" ng-model="emp.empName" ng-click="openEmpDB()" readonly required="required">
		       	<label for="code">Employee</label>	
		       	</div>
		    </div>
			
			<div class="row">
				
				<div class="col s3 input-field">
		       		<input class="validate" type ="number" id="cnmtRateId" name ="cnmtRateName" ng-model="cnmtRate" readonly required="required">
		       	<label for="code">Cnmt Rate</label>	
		       	</div>
		       	
		       	<div class="col s3 input-field">
		       		<input class="validate" type ="number" id="wtId" name ="wtName" ng-model="wt" step="0.00001" min="0.00000" ng-keyup="calCnmtFrt()" required="required">
		       	<label for="code">Weight</label>	
		       	</div>
		       	
		       	<div class="col s3 input-field">
		       		<input class="validate" type ="number" id="cnmtFrtId" name ="cnmtFrtName" ng-model="cnmtFrt" readonly required="required">
		       	<label for="code">Cnmt Freight</label>	
		       	</div>
		       	
		       	<div class="col s3 input-field">
		       		<input class="validate" type ="text" id="tpNoNameId" name ="tpNoName" ng-model="tpNo" >
		       	<label for="code">TP No</label>	
		       	</div>
		    </div>
		    
		    <div class="row">
				
				<div class="col s3 input-field">
		       		<input class="validate" type ="number" id="chlnRateId" name ="chlnRateName" ng-model="chlnRate" step="0.00001" min="0.00000" ng-keyup="calChlnFrt()" required="required">
		       	<label for="code">Challan Rate</label>	
		       	</div>
		       	
		       	<div class="col s3 input-field">
		       		<input class="validate" type ="number" id="chlnFrtId" name ="chlnFrtName" ng-model="chlnFrt" readonly required="required">
		       	<label for="code">Challan Freight</label>	
		       	</div>
		       	
		       	<div class="col s3 input-field">
		       		<input class="validate" type ="number" id="advId" name ="advName" ng-model="adv" step="0.00001" min="0.00000" ng-keyup="calBal()" required="required">
		       	<label for="code">Advance</label>	
		       	</div>
		       	
		       	<div class="col s3 input-field">
		       		<input class="validate" type ="number" id="balId" name ="balName" ng-model="bal" step="0.00001" min="0.00000" required="required">
		       	<label for="code">Balance</label>	
		       	</div>
		    </div>
			
			<div class="row">
				
				<div class="input-field col s3">
					<input class="validate" type ="text" id="vehId" name ="vehName" ng-model="rcNo" ng-keyup="getVehicleMstr()"  required>  
					<label>Vehicle</label>
				</div> 
				
				<div class="input-field col s3">
					<input class="validate" type="text" name="ownName" id="ownNameId" ng-model="own.ownName" ng-click="openOwnDB()" readonly required>
					<label>Owner</label>
				</div>
				
				<div class="input-field col s3">
					<input class="validate" type="text" name="brkName" id="brkNameId" ng-model="brk.brkName" ng-click="openBrkDB()" readonly required>
					<label>Broker</label>
				</div>
				
				<!-- <div class="input-field col s3">
					<select name="panHdrTypeName" id="panHdrTypeId"	ng-model="panHdrType" required>
						<option value="Owner">Owner</option>
						<option value="Broker">Broker</option>
					</select> <label>Pan HDR Type</label>
				</div> -->
		       	
		    </div>
			
			<div class="row">
   			 	<div class="col s12 center">
   			 		<input type="submit" id="submitId" value="Submit">
   			 	</div>
      		</div>
		</form>
	</div>
	
	<div id ="codeDB" ng-hide="codeDBFlag">
		<input type="text" name="filterTextbox1" ng-model="filterTextbox1" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>cnmt/chln</th>
 	  	  </tr>
		  <tr ng-repeat="code in codeList | filter:filterTextbox1">
		 	  <td><input type="radio" name="code" ng-click="saveCode(code, $index)"></td>
              <td>{{code}}</td>
          </tr>
      </table> 
	</div>
	
	<div id ="custDB" ng-hide="custDBFlag">
		<input type="text" name="filterTextbox2" ng-model="filterTextbox2" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Name</th>
 	  	  	  <th>Code</th>
 	  	  </tr>
		  <tr ng-repeat="cust in custList | filter:filterTextbox2">
		 	  <td><input type="radio" name="cust" ng-click="saveCust(cust)"></td>
              <td>{{cust.custName}}</td>
              <td>{{cust.custFaCode}}</td>
          </tr>
      </table> 
	</div>
	
	<div id ="frmStnDB" ng-hide="frmStnDBFlag">
		<input type="text" name="filterTextbox3" ng-model="filterTextbox3" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Code</th>
 	  	  	  <th>Name</th>
 	  	  	  <th>Pin</th>
 	  	  </tr>
		  <tr ng-repeat="frmStn in frmStnList | filter:filterTextbox3">
		 	  <td><input type="radio" name="frmStn" ng-click="saveFrmStn(frmStn)"></td>
              <td>{{frmStn.stnCode}}</td>
              <td>{{frmStn.stnName}}</td>
              <td>{{frmStn.stnPin}}</td>
          </tr>
      </table> 
	</div>
	
	<div id ="toStnDB" ng-hide="toStnDBFlag">
		<input type="text" name="filterTextbox4" ng-model="filterTextbox4" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Code</th>
 	  	  	  <th>Name</th>
 	  	  	  <th>Pin</th>
 	  	  </tr>
		  <tr ng-repeat="toStn in toStnList | filter:filterTextbox4">
		 	  <td><input type="radio" name="toStn" ng-click="saveToStn(toStn)"></td>
              <td>{{toStn.stnCode}}</td>
              <td>{{toStn.stnName}}</td>
              <td>{{toStn.stnPin}}</td>
          </tr>
      </table> 
	</div>
	
	<div id ="empDB" ng-hide="empDBFlag">
		<input type="text" name="filterTextbox5" ng-model="filterTextbox5" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Name</th>
 	  	  	  <th>Code</th>
 	  	  </tr>
		  <tr ng-repeat="emp in empList | filter:filterTextbox5">
		 	  <td><input type="radio" name="emp" ng-click="saveEmp(emp)"></td>
              <td>{{emp.empName}}</td>
              <td>{{emp.empCodeTemp}}</td>
              
          </tr>
      </table> 
	</div>
	
	<div id="vehDB" ng-hide="vehDBFlag">
		<input type="text" name="filterTextbox6" ng-model="filterTextbox6" placeholder="Search....">
		<table>
			<tr>
				<th></th>
				<th>Vehilce No</th>
			</tr>
			<tr ng-repeat="vehicle in vehList | filter:filterTextbox6">
				<td><input type="radio" name="vehicleName" id="vehicleId" ng-model="vehicleCode" ng-click="saveVehicle(vehicle)"></td>
				<td>{{ vehicle }}</td>
			</tr>
		</table>
	</div>
	
	<div id ="brkDB" ng-hide="brkDBFlag">
		<input type="text" name="filterTextbox7" ng-model="filterTextbox7" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Name </th>
 	  	  	  <th> Code </th>
 	  	  </tr>
		  <tr ng-repeat="brk in brkList | filter:filterTextbox7">
		 	  <td><input type="radio" name="brk" ng-click="saveBrk(brk)"></td>
              <td>{{brk.brkName}}</td>
              <td>{{brk.brkCode}}</td>
          </tr>
      </table> 
	</div>
	
	<div id ="ownDB" ng-hide="ownDBFlag">
		<input type="text" name="filterTextbox8" ng-model="filterTextbox8" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Name </th>
 	  	  	  <th> Code </th>
 	  	  </tr>
		  <tr ng-repeat="own in ownList | filter:filterTextbox8">
		 	  <td><input type="radio" name="own" ng-click="saveOwn(own)"></td>
              <td>{{own.ownName}}</td>
              <td>{{own.ownCode}}</td>
          </tr>
      </table> 
	</div>
	
	<div id="contractCodeDB" ng-hide="contCodeDBFlag">

		<input type="text" name="filterTextbox9" ng-model="filterTextbox9" placeholder="Search by Code">
	
		<table class="table" style="border-bottom: 1px solid white;">
			<tr>
				<th style="border-right: 1px solid grey"><br />
					<div class="hori-td">&nbsp;</div> <br />
					<div class="hori-td">Customer Name</div> <br />
					<div class="hori-td">Contract FACode</div> <br />
					<div class="hori-td">From Date</div> <br />
					<div class="hori-td">To Date</div> <br />
					<div class="hori-td">From Station</div> <br />
					<div class="hori-td">To Station</div> <br />
					<div class="hori-td">Door To Door Delivery</div> <br />
					<div class="hori-td">Cost Grade</div> <br />
					<div class="hori-td">Contract Type</div> <br />
					<div class="hori-td">Proportionate</div> <br /></th>
	
	
				<td ng-repeat="cont in contList | filter:filterTextbox9">
					<br />
					<div class="hori-td">
						<input type="radio" name="contCode" id="contId"	ng-click="saveContCode(cont)">
					</div> <br />
					<div class="hori-td">{{ cont.custName }}</div> <br />
					<div class="hori-td">{{ cont.FACode }}</div> <br />
					<div class="hori-td">{{ cont.fromDate }}</div> <br />
					<div class="hori-td">{{ cont.toDate }}</div> <br />
					<div class="hori-td">{{ cont.fromStnName }}</div> <br />
					<div class="hori-td">{{ cont.toStation }}</div> <br />
					<div class="hori-td">{{ cont.cnmtDDL }}</div> <br />
					<div class="hori-td">{{ cont.cnmtCostGrade }}</div> <br />
					<div class="hori-td">{{ cont.contType }}</div> <br />
					<div class="hori-td">{{ cont.proportionate }}</div>
				</td>
			</tr>
		</table>
	
	</div>

</div>