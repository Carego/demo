package com.mylogistics.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.CashStmtStatusDAO;
import com.mylogistics.DAO.ChallanDAO;
import com.mylogistics.DAO.LhpvTempDAO;
import com.mylogistics.model.Branch;
import com.mylogistics.model.CashStmtStatus;
import com.mylogistics.model.Challan;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.VoucherService;

@Controller
public class LhpvTempCntlr {

	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private BranchDAO branchDAO;
	
	@Autowired
	private CashStmtStatusDAO cashStmtStatusDAO;
	
	@Autowired
	private ChallanDAO challanDAO;
	
	@Autowired
	private LhpvTempDAO lhpvTempDAO;
	
	
	@RequestMapping(value = "/getLhpvTempDetails" , method = RequestMethod.POST)  
	public @ResponseBody Object getLhpvTempDetails() {  
		System.out.println("Enter into getLhpvTempDetails---->");
		Map<String, Object> map = new HashMap<>();	
		User user = (User) httpSession.getAttribute("currentUser");
		if(user != null){
			String branchCode = user.getUserBranchCode();
			List<Branch> branchList = branchDAO.retrieveBranch(branchCode);
			if(!branchList.isEmpty()){
				CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranch(branchCode);
				if(cashStmtStatus != null){
					map.put(ConstantsValues.BRANCH, branchList.get(0));
					map.put(ConstantsValues.CASH_STMT_STATUS, cashStmtStatus);
					map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				}else{
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}

		return map;
	}
	
	

	@RequestMapping(value = "/getChlnNoFrLT" , method = RequestMethod.POST)  
	public @ResponseBody Object getChlnNoFrLT() {  
		System.out.println("Enter into getChlnNoFrLT---->");
		Map<String, Object> map = new HashMap<>();
		User currentUser = (User) httpSession.getAttribute("currentUser");
		List<Challan> chlnList = challanDAO.getAllChallanCodes(currentUser.getUserBranchCode());
		if(!chlnList.isEmpty()){
			map.put("list",chlnList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}	
	
	

	@RequestMapping(value = "/saveLhpvTemp" , method = RequestMethod.POST)  
	public @ResponseBody Object saveLhpvTemp(@RequestBody VoucherService voucherService) {  
		System.out.println("Enter into saveLhpvTemp---->");
		Map<String, Object> map = new HashMap<>();
		int vchNo = lhpvTempDAO.saveLhpvTemp(voucherService);
		if(vchNo > 0){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
			
		return map;
	}	
	
}
