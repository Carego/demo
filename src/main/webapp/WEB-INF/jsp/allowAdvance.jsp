 <div ng-show="adminLogin || superAdminLogin"> 
<form name="AllowAdvanceForm">
<table>
	<tr>
		<td>Advance Type</td>
 		<td><select name="advncType"  ng-model="advncType"  ng-change="checkAdvanceType(advncType)" ng-required="true">
                    <option value="customer">Customer</option>
                    <option value="consignor">Consignor</option>  
                    <option value="consignee">Consignee</option>  
                    <option value="branch">Branch</option>  
                    <option value="fromStn">From station</option>
                    <option value="toStn">To station</option>   
         </select></td>
    </tr>  
    
    <tr>
    	<td>Valid Date</td>
    	<td><input type="date" name="validDate"  ng-model="validDate" ng-required ="true"></td>
   	</tr>
   	
   	<tr ng-hide="custConsignDivFlag">
   		<td >Select Customer</td>
   		<td> <input type ="text" name ="custName" ng-model="customerRgt.custName" readonly ng-click="OpenCustomerDB()"></td>
 			 <input type="hidden" ng-model="customerRgt.crhCustCode">
	</tr>
	
	<tr ng-hide="branchDivFlag">
		<td>Select Branch</td>
		<td><input type ="text" name ="branchName" ng-model="branchRgt.branchName"  readonly ng-click="OpenBranchDB()"></td>
 			<input type="hidden" ng-model="branchRgt.brhBranchCode">
 	</tr>	
 	
 	<tr ng-hide="stationDiv">
 		<td>Select Station</td>
 		<td><input type ="text" name ="stnName" ng-model="stationRgt.stnName"  readonly ng-click="OpenStationDB()">
 			<input type="hidden" ng-model="stationRgt.strhStCode">
 		</td>
 	</tr>	
 	
 	<tr ng-hide="hideSubmit">
 		<td><input type="button" value="Submit" ng-click="submit(AllowAdvanceForm)"></td>
 	</tr>		
 	
   	 <tr ng-hide="submitCustomerFlag">
   		<td><input type="button" value="Submit Customer Rights" ng-click="submitCustomerRights(AllowAdvanceForm,customerRgt,advncType,validDate)"></td>
   	</tr>
   	
   	<tr ng-hide="submitBranchFlag">
   		<td><input type="button" value="Submit Branch Rights" ng-click="submitBranchRights(AllowAdvanceForm,branchRgt,advncType,validDate)"></td>
   	</tr>
   	
   	<tr ng-hide="submitStationFlag">
   		<td><input type="button" value="Submit Station Rights" ng-click="submitStationRights(AllowAdvanceForm,stationRgt,advncType,validDate)"></td>
   	</tr>
   		 	
 </table> 
 </form>
 
 <div id="customerDB" ng-hide="customerDBFlag">
 	<input type="text" name="filterTextbox" ng-model="filterTextbox.custCode" placeholder="Search by customer code">
 	  
 	  		<table>
 	 			<tr>	
 	 		    	<th></th>
 	 		   
					<th>Customer Name</th>
			
					<th>Customer Code</th>
				
				</tr>
		  
		  		<tr ng-repeat="cust in customerList | filter:filterTextbox">
		 	  		<td><input type="radio"  name="custName" class="custCls"  value="{{ cust.custCode }}" ng-model="custCode" ng-click="saveCustomerCode(cust.custName,cust.custCode)"></td>
              		<td>{{ cust.custName }}</td>
             		 <td>{{ cust.custCode }}</td>
         		 </tr>
			</table>
</div>
        
<div id="branchDB" ng-hide="branchDBFlag">
 	<input type="text" name="filterTextbox" ng-model="filterTextbox.branchCode" placeholder="Search by branch code">
 	  
 	  <table>
 	 		<tr>	
 	 		    <th></th>
 	 		   
				<th>Branch Name</th>
			
				<th>Branch Code</th>
				
			</tr>
		  
		  <tr ng-repeat="branch in branchList | filter:filterTextbox">
		 	  <td><input type="radio"  name="branchName" class="branchCls"  value="{{ branch.branchCode }}" ng-model="branchCode" ng-click="saveBranchCode(branch.branchName,branch.branchCode)"></td>
              <td>{{ branch.branchName }}</td>
              <td>{{ branch.branchCode }}</td>
          </tr>
	</table>
</div>	

<div id="stationDB" ng-hide="stationDBFlag">
 	<input type="text" name="filterTextbox" ng-model="filterTextbox.stnCode" placeholder="Search by station code">
 	  
 	  <table>
 	 		<tr>	
 	 		    <th></th>
 	 		   
				<th>Station Name</th>
			
				<th>Station Code</th>
				
			</tr>
		  
		  <tr ng-repeat="stn in stationList | filter:filterTextbox">
		 	  <td><input type="radio"  name="stnName" class="stnCls"  value="{{  stn.stnCode }}" ng-model="stnCode" ng-click="saveStationCode(stn.stnName,stn.stnCode)"></td>
              <td>{{ stn.stnName }}</td>
              <td>{{ stn.stnCode }}</td>
          </tr>
	</table>
</div>	      			
</div>
