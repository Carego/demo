package com.mylogistics.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ownbrkkycbusinessdetails")
public class OwnBrkKycBusinessDetails {

	@Id
	@Column(name="businessDetId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int businessDetId;
	
	@Column
	private String ownBrkCode;
	
	@Column
	private String routes;
	
	@Column
	private String vehTypes;

	public int getBusinessDetId() {
		return businessDetId;
	}

	public void setBusinessDetId(int businessDetId) {
		this.businessDetId = businessDetId;
	}

	public String getOwnBrkCode() {
		return ownBrkCode;
	}

	public void setOwnBrkCode(String ownBrkCode) {
		this.ownBrkCode = ownBrkCode;
	}

	public String getRoutes() {
		return routes;
	}

	public void setRoutes(String routes) {
		this.routes = routes;
	}

	public String getVehTypes() {
		return vehTypes;
	}

	public void setVehTypes(String vehTypes) {
		this.vehTypes = vehTypes;
	}
	
	
}
