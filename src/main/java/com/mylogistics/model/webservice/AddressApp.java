package com.mylogistics.model.webservice;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "address_app")
public class AddressApp {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "addId")
	private Integer addId;
	@Column(name = "completeAdd")
	private String completeAdd;
	@Column(name = "addCity")
	private String addCity;
	@Column(name = "addState")
	private String addState;
	@Column(name = "addPin")
	private String addPin;
	@Column(name = "addType")
	private String addType;
	@Column(name = "ownBrkId")
	private Integer ownBrkId;
	
	@ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	private OwnBrkApp ownBrkApp;
	
	public Integer getAddId() {
		return addId;
	}
	public void setAddId(Integer addId) {
		this.addId = addId;
	}
	public String getCompleteAdd() {
		return completeAdd;
	}
	public void setCompleteAdd(String completeAdd) {
		this.completeAdd = completeAdd;
	}
	public String getAddCity() {
		return addCity;
	}
	public void setAddCity(String addCity) {
		this.addCity = addCity;
	}
	public String getAddState() {
		return addState;
	}
	public void setAddState(String addState) {
		this.addState = addState;
	}
	public String getAddPin() {
		return addPin;
	}
	public void setAddPin(String addPin) {
		this.addPin = addPin;
	}
	public String getAddType() {
		return addType;
	}
	public void setAddType(String addType) {
		this.addType = addType;
	}
	public Integer getOwnBrkId() {
		return ownBrkId;
	}
	public void setOwnBrkId(Integer ownBrkId) {
		this.ownBrkId = ownBrkId;
	}
	public OwnBrkApp getOwnBrkApp() {
		return ownBrkApp;
	}
	public void setOwnBrkApp(OwnBrkApp ownBrkApp) {
		this.ownBrkApp = ownBrkApp;
	}
	
	
}