<div ng-show="superAdminLogin">
	<div>
		<form ng-submit="save()" class="col s12 card" style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
<div class="row">
			<div class="col s3 input-field">
				<input type="text" id="chlnCode" name="chlnCode" ng-model="arChlnCode" ng-click="openChlnCodeDb()" readonly>
				<label for="code">Challan Code:</label>
			</div>

			<div class="col s3 input-field">
				<input type="number" id="arRemWtShrtg" name="arRemWtShrtg"
					ng-model="ar.arRemWtShrtg" step="0.00001" min="0.00000" ng-keyup="changeAmt(ar.arRemWtShrtg)" required> <label >Claim(Weight Shortage)</label>
			</div>

			<div class="input-field col s3">
			<input type="text" id="arSrtgDmg" name="arSrtgDmg"
					ng-model="ar.arSrtgDmg" required readonly> <label>Shortage/Damage Report:</label>
			</div>

			<div class="col s3 input-field">
				<select name="arSrtgDmgAlw" ng-model="ar.srtgDmgAlw" ng-change="allow(ar.srtgDmgAlw)" required>
					<option value='true'>Yes</option>
					<option value='false'>No</option>
				</select> <label for="code">Allow Shortage/Damage:</label>
			</div>
			</div>

			<div class="row">
				<div class="input-field col s12">
					<i class="mdi-editor-mode-edit prefix"></i>
					<textarea id="textarea" class="materialize-textarea" rows="3"
						cols="92" name="desc" ng-model="ar.arDesc"  required></textarea>
					<label>Description</label>
				</div>
			</div>

			<div class="row">
				<div class="input-field col s12 center">
					<input class="validate" type="submit" value="submit">
				</div>
			</div>

		</form>
	</div>
	
	<div id ="chlnCodeDB" ng-hide="chlnCodeDBFlag">
		  <input type="text" name="filterArbox" ng-model="filterArbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Challan CODE</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="arChlnCode in arChlnCodeList | filter:filterArbox">
		 	  <td><input type="radio"  name="arChlnCode"   value="{{ arChlnCode }}" ng-model="arChlnCodeM" ng-click="saveChln(arChlnCode)"></td>
              <td>{{arChlnCode}}</td>
          </tr>
      </table> 
	</div>
	

</div>