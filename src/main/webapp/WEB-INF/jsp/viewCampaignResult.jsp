

<!-- <div class="row" >
<div class="col s4">
	       				<input class="validate" type ="button" value="View Indents" ng-click="getIndentList()" >
	       		</div>
	       		
	       		
</div> -->
<div class="row" ng-show="allIndentShowDB">
	<table class="tblrow">

		<caption class="coltag tblrow">Campaign Result List</caption>

		<tr class="rowclr">
			<th class="colclr">Indent No.</th>
			<th class="colclr">Branch</th>
			<th class="colclr">Customer</th>
			<th class="colclr">From</th>
			<th class="colclr">To</th>
			<th class="colclr">Placement Date and time</th>
			<th class="colclr">Vehicle Type</th>
		</tr>

		<tr ng-repeat="indent in indentList">
			<td class="rowcel" ng-click="getCampaignByIndent(indent,$index)"><a>{{indent.indentId}}</a></td>
			<td class="rowcel">{{indent.branchName}}</td>
			<td class="rowcel">{{indent.cutomerName}}</td>
			<td class="rowcel">{{indent.fromStn}}</td>
			<td class="rowcel">{{indent.toStn}}</td>
			<td class="rowcel">{{indent.placementDateTime | date : 'M/d/yy
				h:mm a'}}</td>
			<td class="rowcel">{{indent.vehicleType}}</td>

		</tr>
	</table>

</div>



<div class="row" ng-show="singleIndentShowDB">
	<table class="tblrow">

		<caption class="coltag tblrow">Campaign Result List</caption>

		<tr class="rowclr">
			<th class="colclr">Indent No.</th>
			<th class="colclr">Branch</th>
			<th class="colclr">Customer</th>
			<th class="colclr">From</th>
			<th class="colclr">To</th>
			<th class="colclr">Placement Date and time</th>
			<th class="colclr">Vehicle Type</th>
		</tr>

		<tr>
			<td class="rowcel">{{indentList[tempIndex].indentId}}</td>
			<td class="rowcel">{{indentList[tempIndex].branchName}}</td>
			<td class="rowcel">{{indentList[tempIndex].cutomerName}}</td>
			<td class="rowcel">{{indentList[tempIndex].fromStn}}</td>
			<td class="rowcel">{{indentList[tempIndex].toStn}}</td>
			<td class="rowcel">{{indentList[tempIndex].placementDateTime |
				date : 'M/d/yy h:mm a'}}</td>
			<td class="rowcel">{{indentList[tempIndex].vehicleType}}</td>

		</tr>
	</table>

</div>



<div class="row" ng-show="ownerListDBFlag">
	<table class="tblrow">

		<caption class="coltag tblrow">Available Vendor List</caption>

		<tr class="rowclr">
			<th class="colclr">Company Name</th>
			<th class="colclr">Company Owner Name</th>
			<th class="colclr">Address</th>
			<th class="colclr">Contact No.</th>
			<th class="colclr">Vehicle Available</th>
			<th class="colclr">Rate</th>
			<th class="colclr">Remarks</th>
			<th class="colclr">Add Vehicle and Driver detail</th>
		</tr>

		<tr ng-repeat="ownMstr in ownerMstrList">
			<td class="rowcel">{{ownMstr.company_Name}}</td>
			<td class="rowcel">{{ownMstr.company_Owner_Name}}</td>
			<td class="rowcel">{{ownMstr.company_Office_Address}}</td>
			<td class="rowcel">{{ownMstr.phone_No}}</td>
			<td class="rowcel">{{campMap[ownMstr.master_Id].noOfVehAvail}}</td>
			<td class="rowcel"><div contenteditable>{{campMap[ownMstr.master_Id].rates}}</div></td>
			<td class="rowcel"><div contenteditable>{{campMap[ownMstr.master_Id].agentRemark}}</div></td>
			<td class="rowcel"
				ng-click="addVehAndDrvrDet(ownMstr,campMap[ownMstr.master_Id])"><a>Add
					Details</a></td>

		</tr>
	</table>

	<div class="row">
		<div class="col s4">
			<input class="validate" type="button" value="Save Placement"
				ng-click="savePlacement()">
		</div>
	</div>

</div>



<div id="addDrvrDetId" ng-hide="addDrvrDetFlag">
	<br> <br>
	<div class="input-field col s4">
		<input type="number" id="noOfVehicle" name="noOfVehicle"
			ng-model="noOfVehicle" ng-blur="make(noOfVehicle)" required>
		<label>No. of Vehicle</label>
	</div>
	<br> <br>
	<div ng-repeat="x in vehList">
		<div>
			<h4>Add detail of Vehicle {{x+1}}</h4>

		</div>
		<br>
		<div class="input-field col s4">
			<input type="text" id="vehicleNoId" name="vehicleNoName"
				ng-model="vehicleNo" required> <label>Vehicle No</label>
		</div>
		
		<div class="input-field col s4">
			<input type="text" id="driverNameId" name="driverName"
				ng-model="driverName" required> <label>Driver Name</label>
		</div>
		<div class="input-field col s4">
			<input type="text" id="driverContactNoId" name="driverContactNoName"
				ng-model="driverNo" required> <label>Driver
				Contact No.</label>
		</div >
		
	
	
	<div class="input-field col s12 center">
			<input type="button" value="Submit" ng-click="addDrvrDet(vehicleNo,driverName,driverNo)">
		</div>
	</div>
</div>

