package com.mylogistics.DAO;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mylogistics.model.PetroCard;

@Repository
public interface PetroCardDAO {
	
	public List<PetroCard> getAllPetroCardByBrhType(String cardType,String bCode);
	
	public List<PetroCard> getAllPetroCardByBrh(String bCode);

	public List<PetroCard> getAllPetroCardByType(String cardType);

}
