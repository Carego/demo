package com.mylogistics.constants;

public class MoneyReceiptCNTS {

	public static final String MR_ID = "mrId";
	
	public static final String MR_TYPE="mrType";
	
	public static final String MR_NO="mrNo";
	
	public static final String MR_IS_PENDING="mrIsPending";
	
	public static final String MR_IS_CANCEL="mrCancel";
	
	public static final String MR_CUST_ID="mrCustId";
	
	public static final String MR_BRH_ID="mrBrhId";
	
	public static final String MR_Date="mrDate";
	
	public static final String MR_NET_AMT = "mrNetAmt";  
	
	public static final String MR_REM_AMT = "mrRemAmt";
	
	public static final String MR_CANCEL = "mrCancel";
	
	public static final String MR_OTH_MR_AMT = "mrOthMrAmt";
	
	public static final String USER_BRANCH_CODE="bCode";
	
	public static final String USER_CODE="userCode";
	
	public static final String MR_Ded_Amt="mrDedAmt";
	
	public static final String MR_Exce_MR_AMT="mrAccessAmt";
	
	public static final String MR_Freight="mrFreight";
	
	public static final String MR_CUST_BANK_NAME="mrCustBankName";
	
	public static final String MR_CHQ_NO = "mrChqNo";

	public static final String MR_DESC = "mrDesc";
	
	public static final String MR_BNK_ID = "mrBnkId";
	
	public static final String MR_RTGS_REF_NO = "mrRtgsRefNo";
	
	public static final String MR_PAY_BY = "mrPayBy";
	
	public static final String MR_SRT_EXCESS_DISALLOW = "mrSrtExsDis";
	
}