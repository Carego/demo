package com.mylogistics.services.impl;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mylogistics.DAO.CashStmtStatusDAO;
import com.mylogistics.DAO.FaMasterDAO;
import com.mylogistics.DAO.bank.BankMstrDAO;
import com.mylogistics.DAO.bank.ChequeLeavesDAO;
import com.mylogistics.model.CashStmt;
import com.mylogistics.model.CashStmtStatus;
import com.mylogistics.model.FAMaster;
import com.mylogistics.model.User;
import com.mylogistics.model.bank.BankMstr;
import com.mylogistics.model.bank.ChequeLeaves;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.NewVoucherService;
import com.mylogistics.services.VoucherService;

@Service
public class NewVoucherServiceImpl implements NewVoucherService {
	
	public static final Logger logger = Logger.getLogger(NewVoucherServiceImpl.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private HttpSession httpSession;
	@Autowired
	private FaMasterDAO faMasterDAO;
	@Autowired
	private BankMstrDAO bankMstrDAO;
	@Autowired
	private ChequeLeavesDAO chequeLeavesDAO;
	@Autowired
	private CashStmtStatusDAO cashStmtStatusDAO;
	
	@Override
	public Map<String, Object> saveNewVoucher(VoucherService voucherService) {
		logger.info("Enter into saveNewVoucher----->>>");
		
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		User currentUser =	(User)httpSession.getAttribute("currentUser");
		Map<String,Object> map = new HashMap<>();
		
		try {
			List<FAMaster> faList = faMasterDAO.getFAMByFaNameN(session, ConstantsValues.CASH_IN_HAND);
			String cashFaCode = faList.get(0).getFaMfaCode();
			char chequeType = voucherService.getChequeType();
			String payMode = "Q";
			if(chequeType == 'C') {
				String bankCode = voucherService.getBankCode();
				List<BankMstr> bankMList = bankMstrDAO.getBankByBankCodeN(session, bankCode);
				if(!bankMList.isEmpty()){
					BankMstr bankMstr = bankMList.get(0);
					
					ChequeLeaves chequeLeaves = voucherService.getChequeLeaves();
					chequeLeaves.setBankMstr(bankMstr);
					chequeLeaves.setChqLChqAmt(voucherService.getAmount());
					chequeLeaves.setChqLUsedDt(new Date(new java.util.Date().getTime()));
					chequeLeaves.setChqLUsed(true);
					String tvNo = chequeLeaves.getChqLChqNo();
					int temp = chequeLeavesDAO.updateChqLeavesN(session, chequeLeaves);
					if(temp > 0){
						CashStmtStatus cashStmtStatus = voucherService.getCashStmtStatus();
						CashStmt cashStmt = new CashStmt();
						cashStmt.setbCode(currentUser.getUserBranchCode());
						cashStmt.setUserCode(currentUser.getUserCode());
						cashStmt.setCsDescription(voucherService.getDesc());
						cashStmt.setCsDrCr('C');
						cashStmt.setCsAmt(voucherService.getAmount());
						cashStmt.setCsType(voucherService.getVoucherType());
						cashStmt.setCsChequeType(voucherService.getChequeType());
						cashStmt.setCsPayTo(voucherService.getPayTo());
						cashStmt.setCsTvNo(tvNo);
						cashStmt.setCsVouchType("cash");
						cashStmt.setCsFaCode(bankCode);
						cashStmt.setPayMode(payMode);
						java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
						logger.info("sqlDate ====>>"+sqlDate);
						
						cashStmt.setCsDt(sqlDate);
						map = cashStmtStatusDAO.updateCSSByCSN(session, cashStmtStatus.getCssId() , cashStmt);
						CashStmt cashStmt1 = new CashStmt();
						cashStmt1.setbCode(currentUser.getUserBranchCode());
						cashStmt1.setUserCode(currentUser.getUserCode());
						cashStmt1.setCsDescription(voucherService.getDesc());
						cashStmt1.setCsDrCr('D');
						cashStmt1.setCsAmt(voucherService.getAmount());
						cashStmt1.setCsType(voucherService.getVoucherType());
						cashStmt1.setCsChequeType(voucherService.getChequeType());
						cashStmt1.setCsPayTo(voucherService.getPayTo());
						cashStmt1.setCsTvNo(tvNo);
						cashStmt1.setCsVouchType("bank");
						cashStmt1.setCsFaCode(cashFaCode);
						cashStmt1.setCsVouchNo((int)map.get("vhNo"));
						cashStmt1.setPayMode(payMode);
						logger.info("sqlDate ====>>>"+sqlDate);
						
						cashStmt1.setCsDt(sqlDate);
						map = cashStmtStatusDAO.updateCSSByCSN(session, cashStmtStatus.getCssId() , cashStmt1);
						
						map.put("bnkName",bankMstr.getBnkName());
						map.put("tvNo",tvNo);
					}
				}
			
			}else if(chequeType == 'M'){
				String bankCode = voucherService.getBankCode();
				List<BankMstr> bankMList = bankMstrDAO.getBankByBankCodeN(session, bankCode);
				if(!bankMList.isEmpty()){
					BankMstr bankMstr = bankMList.get(0);
					
					ChequeLeaves chequeLeaves = voucherService.getChequeLeaves();
					chequeLeaves.setBankMstr(bankMstr);
					chequeLeaves.setChqLChqAmt(voucherService.getAmount());
					chequeLeaves.setChqLUsedDt(new Date(new java.util.Date().getTime()));
					chequeLeaves.setChqLUsed(true);
					String tvNo = chequeLeaves.getChqLChqNo();
					logger.info("TV NO.... + "+tvNo);
					int temp = chequeLeavesDAO.updateChqLeavesN(session, chequeLeaves);
	
					if(temp > 0){
						CashStmtStatus cashStmtStatus = voucherService.getCashStmtStatus();
						CashStmt cashStmt = new CashStmt();
						cashStmt.setbCode(currentUser.getUserBranchCode());
						cashStmt.setUserCode(currentUser.getUserCode());
						cashStmt.setCsDescription(voucherService.getDesc());
						cashStmt.setCsDrCr('C');
						cashStmt.setCsAmt(voucherService.getAmount());
						cashStmt.setCsType(voucherService.getVoucherType());
						cashStmt.setCsChequeType(voucherService.getChequeType());
						cashStmt.setCsTvNo(tvNo);
						cashStmt.setCsVouchType("cash");
						cashStmt.setCsFaCode(bankCode);
						cashStmt.setPayMode(payMode);
						java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
						logger.info("sqlDate ====>>"+sqlDate);
						cashStmt.setCsDt(sqlDate);
						map = cashStmtStatusDAO.updateCSSByCSN(session, cashStmtStatus.getCssId() , cashStmt);
						CashStmt cashStmt1 = new CashStmt();
						cashStmt1.setbCode(currentUser.getUserBranchCode());
						cashStmt1.setUserCode(currentUser.getUserCode());
						cashStmt1.setCsDescription(voucherService.getDesc());
						cashStmt1.setCsDrCr('D');
						cashStmt1.setCsAmt(voucherService.getAmount());
						cashStmt1.setCsType(voucherService.getVoucherType());
						cashStmt1.setCsChequeType(voucherService.getChequeType());
						cashStmt1.setCsPayTo(voucherService.getPayTo());
						cashStmt1.setCsTvNo(tvNo);
						cashStmt1.setCsVouchType("bank");
						cashStmt1.setCsFaCode(cashFaCode);
						cashStmt1.setCsVouchNo((int)map.get("vhNo"));
						cashStmt1.setPayMode(payMode);
						logger.info("sqlDate ====>>>"+sqlDate);
						
						cashStmt1.setCsDt(sqlDate);
						map = cashStmtStatusDAO.updateCSSByCSN(session, cashStmtStatus.getCssId() , cashStmt1);
						map.put("bnkName",bankMstr.getBnkName());
						map.put("tvNo",tvNo);
					}
				}
			} else {
				logger.info("wrong check Type");
			}
			transaction.commit();
		} catch (Exception e) {
			logger.info(e.getMessage(), e);
			map.clear();
			map.put("vhNo", 0);
			transaction.rollback();
		} finally {
			session.close();
		}
		return map;
	}

}