package com.mylogistics.controller;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BranchMUStatsDAO;
import com.mylogistics.DAO.HBO_StnNotificationDAO;
import com.mylogistics.DAO.HOSLastDAO;
import com.mylogistics.model.HBO_StnNotification;
import com.mylogistics.model.HOSLast;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;


@Controller
public class SuperAdmin_DashboardCntlr {

	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private HOSLastDAO hOSLastDAO;
	
	@Autowired
	private BranchMUStatsDAO branchMUStatsDAO;
	
	@Autowired
	private HBO_StnNotificationDAO hBO_StnNotificationDAO;
	
	public static Logger logger = Logger.getLogger(SuperAdmin_DashboardCntlr.class);
	
	@RequestMapping(value = "/getHOSCnmtMonLast", method = RequestMethod.POST)
	public @ResponseBody Object getHOSCnmtMonLast(){
		System.out.println("enter into getHOSCnmtMonLast function");
		Map<String,Object> map = new HashMap<String,Object>();
		List<HOSLast> list = hOSLastDAO.getLastArrival();
		System.out.println("List<HOSLast> size = "+list.size());
		double cnmtDaysLast = 0;
		int monthsLast = 0;
		int daysLast = 0;
		if (!list.isEmpty()) {
			HOSLast hOSLast = list.get(0);
			System.out.println("hOSLast ----->id ="+hOSLast.gethOSLastId());
			int lastCnmt = hOSLast.gethOSCnmt();
		
			/*double actualDay = 0;*/
			System.out.println("***********lastCnmt = "+lastCnmt);
			
			if(lastCnmt >= 0){
				double monUsgCNMT = getAvgMonUsgCNMT();
				System.out.println("monUsgCNMT---->"+monUsgCNMT);
				if(monUsgCNMT > 0){
					cnmtDaysLast = (30/monUsgCNMT)*lastCnmt;
				}
			}
			System.out.println("cnmtDaysLast = "+cnmtDaysLast);
			
			
			if(cnmtDaysLast<30){
				daysLast = (int) cnmtDaysLast;
				monthsLast = 0;
				System.out.println("No. of days------"+daysLast);
				System.out.println("No. of months------"+monthsLast);
			}else if(cnmtDaysLast>=30){
				monthsLast = (int) cnmtDaysLast/30;
				daysLast = (int) cnmtDaysLast - (30*monthsLast);
				System.out.println("No. of days------"+daysLast);
				System.out.println("No. of months------"+monthsLast);
			}
		}
		
		map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		map.put("daysLast",daysLast);
		map.put("monthsLast",monthsLast);
		//map.put("daysLast", cnmtDaysLast);
		return map;	
	}
	
	public double getAvgMonUsgCNMT(){
		System.out.println("enter into getAvgMonUsgCNMT function");
		/*Date todayDate = new Date();
		Date firstEntryDate = branchMUStatsDAO.getFirstEntryDateOfCnmt();
		Calendar calendar =Calendar.getInstance();
			calendar.setTime(todayDate);
			calendar.add(Calendar.MONTH, -1);
			Date oneMonthPrevDate = calendar.getTime();
			
			calendar.add(Calendar.MONTH, -1);
			Date twoMonthPrevDate = calendar.getTime();
			
			calendar.add(Calendar.MONTH, -1);
			Date threeMonthPrevDate = calendar.getTime();
			
			double days = (double)( (todayDate.getTime() - firstEntryDate.getTime()) / (1000 * 60 * 60 * 24));
			*/
		//List<String> branchList = branchMUStatsDAO.getAllBranch();
		double avgMonUsgCNMT = 0;
		/*if(!branchList.isEmpty()){
			avgMonUsgCNMT = branchMUStatsDAO.getAvgMonUsgCNMT();
		}*/	
		avgMonUsgCNMT = branchMUStatsDAO.getAvgMonUsg("cnmt");
		System.out.println("avgMonUsgCNMT======>"+avgMonUsgCNMT);
		return avgMonUsgCNMT;
	}
	
	
	@RequestMapping(value = "/getHOSAvgMonUsgCNMT", method = RequestMethod.POST)
	public @ResponseBody Object getHOSAvgMonUsgCNMT(){
		System.out.println("enter into getHOSAvgMonUsgCNMT function");
		Map<String,Object> map = new HashMap<String,Object>();
		Date todayDate = new Date();
		//Date firstEntryDate = branchMUStatsDAO.getFirstEntryDateOfCnmt();
		Date firstEntryDate = hOSLastDAO.getFirstEntryDate();
		System.out.println("firstEntryDate --------->"+firstEntryDate);
		Calendar calendar =Calendar.getInstance();
			calendar.setTime(todayDate);
			calendar.add(Calendar.MONTH, -1);
			Date oneMonthPrevDate = calendar.getTime();
			
			calendar.add(Calendar.MONTH, -1);
			Date twoMonthPrevDate = calendar.getTime();
			
			calendar.add(Calendar.MONTH, -1);
			Date threeMonthPrevDate = calendar.getTime();
			
			System.out.println("todayDate==================>>>>"+todayDate);
			System.out.println("firstEntryDate==================>>>>"+firstEntryDate);
			Map<String,Object> stMap = new HashMap<String,Object>();
			
			if(firstEntryDate != null){
			
				double days = (double)( (todayDate.getTime() - firstEntryDate.getTime()) / (1000 * 60 * 60 * 24));
				System.out.println("days ========> "+days);
				stMap = hOSLastDAO.getNoOfStationary(todayDate, oneMonthPrevDate);
				double cnmtForOneMnth = (Double) stMap.get("noOfCnmt");
				System.out.println("************cnmtForOneMnth = "+cnmtForOneMnth);
				
	
				stMap = hOSLastDAO.getNoOfStationary(oneMonthPrevDate, twoMonthPrevDate);
				double cnmtForScndMnth = (Double)stMap.get("noOfCnmt");
				System.out.println("************cnmtForScndMnth = "+cnmtForScndMnth);
				
				stMap = hOSLastDAO.getNoOfStationary(twoMonthPrevDate, threeMonthPrevDate);
				double cnmtForThirdMnth = (Double)stMap.get("noOfCnmt");
				System.out.println("************cnmtForThirdMnth = "+cnmtForThirdMnth);
				
				 
				stMap = hOSLastDAO.getNoOfStationary(firstEntryDate, twoMonthPrevDate);
				double cnmtBwFrstAndScndMnth= (Double)stMap.get("noOfCnmt");
				System.out.println("************cnmtBwFrstAndScndMnth = "+cnmtBwFrstAndScndMnth);
				
				stMap= hOSLastDAO.getNoOfStationary(firstEntryDate, oneMonthPrevDate);
				double cnmtBwFrstAndOneMnth  = (Double)stMap.get("noOfCnmt");
				System.out.println("************cnmtBwFrstAndOneMnth = "+cnmtBwFrstAndOneMnth);
				
				
				stMap= hOSLastDAO.getNoOfStationary(todayDate, firstEntryDate);
				double cnmtBwTodayAndFrstDate = (Double)stMap.get("noOfCnmt");
				System.out.println("************cnmtBwTodayAndFrstDate = "+cnmtBwTodayAndFrstDate);
			
			
				double average=0.00;
				
				if(days>=90){
					System.out.println("************days are greater than 90***************");
					 average = ((cnmtForOneMnth*3) + (cnmtForScndMnth*2) + cnmtForThirdMnth)/6;
					 map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
					 map.put("average", average);
					 return map;
				}
				else if(days>=60 && days<90){
					System.out.println("************days are greater than 60 & less than 90***************");
					  int daysBwFrstAndScndMnth = (int)( (twoMonthPrevDate.getTime() - firstEntryDate.getTime()) / (1000 * 60 * 60 * 24));		
					  average = (((cnmtForOneMnth*3) + (cnmtForScndMnth*2) + (cnmtBwFrstAndScndMnth/daysBwFrstAndScndMnth))*30)/6;
					  map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
					  map.put("average", average);
					  return map;
				}
				else if(days>=30 && days<60){
					System.out.println("************days are greater than 30 & less than 60***************");
					System.out.println("************cnmtForOneMnth = "+cnmtForOneMnth);
					System.out.println("************cnmtBwFrstAndOneMnth = "+cnmtBwFrstAndOneMnth);
					int daysBwFrstAndOneMnth = (int)( (oneMonthPrevDate.getTime() - firstEntryDate.getTime()) / (1000 * 60 * 60 * 24));
					System.out.println("************daysBwFrstAndOneMnth = "+daysBwFrstAndOneMnth);
					average = (((cnmtForOneMnth*2) +  (cnmtBwFrstAndOneMnth/daysBwFrstAndOneMnth))*30)/3;
					map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
					map.put("average", average);
					return map;
				}
				else if(days<30){
					System.out.println("************days are less than 30***************");
					int daysBwFrstAndToday = (int)( (todayDate.getTime() - firstEntryDate.getTime()) / (1000 * 60 * 60 * 24));	
					average = ((cnmtBwTodayAndFrstDate/daysBwFrstAndToday)*30);
					map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
					map.put("average", average);
					return map; 
				}else{
					map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
					return map;
				}
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
				return map;
			}
		
	}
	
	
	@RequestMapping(value = "/getHOSChlnMonLast", method = RequestMethod.POST)
	public @ResponseBody Object getHOSChlnMonLast(){
		System.out.println("enter into getHOSChlnMonLast function");
		Map<String,Object> map = new HashMap<String,Object>();
		List<HOSLast> list = hOSLastDAO.getLastArrival();
		System.out.println("List<HOSLast> size = "+list.size());
		double chlnDaysLast = 0;
		int daysLast=0;
		int monthsLast=0;
		
		if (!list.isEmpty()) {
			HOSLast hOSLast = list.get(0);
			int lastChln = hOSLast.gethOSChln();
			
			System.out.println("***********lastChln = "+lastChln);
			
			if(lastChln >= 0){
				double monUsgCHLN = getAvgMonUsgCHLN();
				System.out.println("monUsgCHLN---->"+monUsgCHLN);
				if(monUsgCHLN > 0){
					chlnDaysLast = (30/monUsgCHLN)*lastChln;
				}
				//chlnDaysLast = (30/monUsgCHLN)*lastChln;
			}
			System.out.println("chlnDaysLast = "+chlnDaysLast);
			
			if(chlnDaysLast<30){
				daysLast = (int) chlnDaysLast;
				monthsLast = 0;
				System.out.println("No. of days------"+daysLast);
				System.out.println("No. of months------"+monthsLast);
			}else if(chlnDaysLast>=30){
				monthsLast = (int) chlnDaysLast/30;
				daysLast = (int) chlnDaysLast - (30*monthsLast);
				System.out.println("No. of days------"+daysLast);
				System.out.println("No. of months------"+monthsLast);
			}
		}
		map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		//map.put("daysLast", chlnDaysLast);
		map.put("daysLast",daysLast);
		map.put("monthsLast",monthsLast);
		return map;	
	}
	
	public double getAvgMonUsgCHLN(){
		System.out.println("enter into getAvgMonUsgCNMT function");
		double avgMonUsgCHLN = 0;
		avgMonUsgCHLN = branchMUStatsDAO.getAvgMonUsg("chln");
		return avgMonUsgCHLN;
	}
	
	@RequestMapping(value = "/getHOSAvgMonUsgCHLN", method = RequestMethod.POST)
	public @ResponseBody Object getHOSAvgMonUsgCHLN(){
		System.out.println("enter into getHOSAvgMonUsgCHLN function");
		Map<String,Object> map = new HashMap<String,Object>();
		Date todayDate = new Date();
		Date firstEntryDate = hOSLastDAO.getFirstEntryDate();
		Calendar calendar =Calendar.getInstance();
			calendar.setTime(todayDate);
			calendar.add(Calendar.MONTH, -1);
			Date oneMonthPrevDate = calendar.getTime();
			
			calendar.add(Calendar.MONTH, -1);
			Date twoMonthPrevDate = calendar.getTime();
			
			calendar.add(Calendar.MONTH, -1);
			Date threeMonthPrevDate = calendar.getTime();
			
			if(firstEntryDate != null){
				
				double days = (double)( (todayDate.getTime() - firstEntryDate.getTime()) / (1000 * 60 * 60 * 24));
				System.out.println("days ========> "+days);
				
				Map<String,Object> stMap = new HashMap<String,Object>();
				
				stMap = hOSLastDAO.getNoOfStationary(todayDate, oneMonthPrevDate);
				double chlnForOneMnth = (Double) stMap.get("noOfChln");
				System.out.println("************chlnForOneMnth = "+chlnForOneMnth);
				
				 stMap= hOSLastDAO.getNoOfStationary(oneMonthPrevDate, twoMonthPrevDate);
				 double chlnForScndMnth = (Double) stMap.get("noOfChln");
				System.out.println("************chlnForScndMnth = "+chlnForScndMnth);
				
				stMap = hOSLastDAO.getNoOfStationary(twoMonthPrevDate, threeMonthPrevDate);
				double chlnForThirdMnth = (Double) stMap.get("noOfChln");
				System.out.println("************chlnForThirdMnth = "+chlnForThirdMnth);
				
				stMap= hOSLastDAO.getNoOfStationary(firstEntryDate, twoMonthPrevDate);
				double chlnBwFrstAndScndMnth  = (Double) stMap.get("noOfChln");
				System.out.println("************chlnBwFrstAndScndMnth = "+chlnBwFrstAndScndMnth);
				
				stMap = hOSLastDAO.getNoOfStationary(firstEntryDate, oneMonthPrevDate);
				double chlnBwFrstAndOneMnth = (Double) stMap.get("noOfChln");
				System.out.println("************chlnBwFrstAndOneMnth = "+chlnBwFrstAndOneMnth);
				
				stMap = hOSLastDAO.getNoOfStationary(todayDate, firstEntryDate);
				double chlnBwTodayAndFrstDate = (Double) stMap.get("noOfChln");
				System.out.println("************chlnBwTodayAndFrstDate = "+chlnBwTodayAndFrstDate);
				
				
				double average=0.00;
				
				if(days>=90){
					System.out.println("************days are greater than 90***************");
					 average = ((chlnForOneMnth*3) + (chlnForScndMnth*2) + chlnForThirdMnth)/6;
					 map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
					 map.put("average", average);
					 return map;
				}
				else if(days>=60 && days<90){
					System.out.println("************days are greater than 60 & less than 90***************");
					  int daysBwFrstAndScndMnth = (int)( (twoMonthPrevDate.getTime() - firstEntryDate.getTime()) / (1000 * 60 * 60 * 24));		
					  average = (((chlnForOneMnth*3) + (chlnForScndMnth*2) + (chlnBwFrstAndScndMnth/daysBwFrstAndScndMnth))*30)/6;
					  map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
					  map.put("average", average);
					  return map;
				}
				else if(days>=30 && days<60){
					System.out.println("************days are greater than 30 & less than 60***************");
					System.out.println("************chlnForOneMnth = "+chlnForOneMnth);
					System.out.println("************chlnBwFrstAndOneMnth = "+chlnBwFrstAndOneMnth);
					int daysBwFrstAndOneMnth = (int)( (oneMonthPrevDate.getTime() - firstEntryDate.getTime()) / (1000 * 60 * 60 * 24));
					System.out.println("************daysBwFrstAndOneMnth = "+daysBwFrstAndOneMnth);
					average = (((chlnForOneMnth*2) +  (chlnBwFrstAndOneMnth/daysBwFrstAndOneMnth))*30)/3;
					map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
					map.put("average", average);
					return map;
				}
				else if(days<30){
					System.out.println("************days are less than 30***************");
					int daysBwFrstAndToday = (int)( (todayDate.getTime() - firstEntryDate.getTime()) / (1000 * 60 * 60 * 24));	
					average = ((chlnBwTodayAndFrstDate/daysBwFrstAndToday)*30);
					map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
					map.put("average", average);
					return map; 
				}else{
					map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
					return map;
				}
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
				return map;
			}
	}
	
	
	@RequestMapping(value = "/getHOSSedrMonLast", method = RequestMethod.POST)
	public @ResponseBody Object getHOSSedrMonLast(){
		System.out.println("enter into getHOSSedrMonLast function");
		Map<String,Object> map = new HashMap<String,Object>();
		List<HOSLast> list = hOSLastDAO.getLastArrival();
		System.out.println("List<HOSLast> size = "+list.size());
		double sedrDaysLast = 0;
		int daysLast=0;
		int monthsLast=0;
		
		if (!list.isEmpty()) {
			HOSLast hOSLast = list.get(0);
			int lastSedr = hOSLast.gethOSSedr();
			
			System.out.println("***********lastSedr = "+lastSedr);
			
			if(lastSedr >= 0){
				double monUsgSEDR = getAvgMonUsgSEDR();
				System.out.println("monUsgSEDR---->"+monUsgSEDR);
				if(monUsgSEDR > 0){
					sedrDaysLast = (30/monUsgSEDR)*lastSedr;
				}
				//sedrDaysLast = (30/monUsgSEDR)*lastSedr;
			}
			System.out.println("***********sedrDaysLast = "+sedrDaysLast);
			
			if(sedrDaysLast<30){
				daysLast = (int) sedrDaysLast;
				monthsLast = 0;
				System.out.println("No. of days------"+daysLast);
				System.out.println("No. of months------"+monthsLast);
			}else if(sedrDaysLast>=30){
				monthsLast = (int) sedrDaysLast/30;
				daysLast = (int) sedrDaysLast - (30*monthsLast);
				System.out.println("No. of days------"+daysLast);
				System.out.println("No. of months------"+monthsLast);
			}
		}	
		map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		//map.put("daysLast", sedrDaysLast);
		map.put("daysLast",daysLast);
		map.put("monthsLast",monthsLast);
		return map;	
	}
	
	public double getAvgMonUsgSEDR(){
		System.out.println("enter into getAvgMonUsgSEDR function");
		double avgMonUsgSEDR = 0;
		avgMonUsgSEDR = branchMUStatsDAO.getAvgMonUsg("sedr");
		return avgMonUsgSEDR;
	}
	
	@RequestMapping(value = "/getHOSAvgMonUsgSEDR", method = RequestMethod.POST)
	public @ResponseBody Object getHOSAvgMonUsgSEDR(){
		System.out.println("enter into getHOSAvgMonUsgSEDR function");
		Map<String,Object> map = new HashMap<String,Object>();
		Date todayDate = new Date();
		Date firstEntryDate = hOSLastDAO.getFirstEntryDate();
		Calendar calendar =Calendar.getInstance();
			calendar.setTime(todayDate);
			calendar.add(Calendar.MONTH, -1);
			Date oneMonthPrevDate = calendar.getTime();
			
			calendar.add(Calendar.MONTH, -1);
			Date twoMonthPrevDate = calendar.getTime();
			
			calendar.add(Calendar.MONTH, -1);
			Date threeMonthPrevDate = calendar.getTime();
			
			Map<String,Object> stMap = new HashMap<String,Object>();
			
			if(firstEntryDate !=null){
				
				double days = (double)( (todayDate.getTime() - firstEntryDate.getTime()) / (1000 * 60 * 60 * 24));
				System.out.println("days ========> "+days);
				
				stMap = hOSLastDAO.getNoOfStationary(todayDate, oneMonthPrevDate);
				double sedrForOneMnth = (Double) stMap.get("noOfSedr");
				System.out.println("************sedrForOneMnth = "+sedrForOneMnth);
				
				stMap= hOSLastDAO.getNoOfStationary(oneMonthPrevDate, twoMonthPrevDate);
				double sedrForScndMnth  = (Double) stMap.get("noOfSedr");
				System.out.println("************sedrForScndMnth = "+sedrForScndMnth);
				
				stMap = hOSLastDAO.getNoOfStationary(twoMonthPrevDate, threeMonthPrevDate);
				double sedrForThirdMnth = (Double) stMap.get("noOfSedr");
				System.out.println("************sedrForThirdMnth = "+sedrForThirdMnth);
				
				stMap = hOSLastDAO.getNoOfStationary(firstEntryDate, twoMonthPrevDate);
				double sedrBwFrstAndScndMnth = (Double) stMap.get("noOfSedr");
				System.out.println("************sedrBwFrstAndScndMnth = "+sedrBwFrstAndScndMnth);
				
				stMap= hOSLastDAO.getNoOfStationary(firstEntryDate, oneMonthPrevDate);
				double sedrBwFrstAndOneMnth   = (Double) stMap.get("noOfSedr");
				System.out.println("************sedrBwFrstAndOneMnth = "+sedrBwFrstAndOneMnth);
				
				stMap = hOSLastDAO.getNoOfStationary(todayDate, firstEntryDate);
				double sedrBwTodayAndFrstDate= (Double) stMap.get("noOfSedr");
				System.out.println("************sedrBwTodayAndFrstDate = "+sedrBwTodayAndFrstDate);
				
				
				double average=0.00;
				
				if(days>=90){
					System.out.println("************days are greater than 90***************");
					 average = ((sedrForOneMnth*3) + (sedrForScndMnth*2) + sedrForThirdMnth)/6;
					 map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
					 map.put("average", average);
					 return map;
				}
				else if(days>=60 && days<90){
					System.out.println("************days are greater than 60 & less than 90***************");
					  int daysBwFrstAndScndMnth = (int)( (twoMonthPrevDate.getTime() - firstEntryDate.getTime()) / (1000 * 60 * 60 * 24));		
					  average = (((sedrForOneMnth*3) + (sedrForScndMnth*2) + (sedrBwFrstAndScndMnth/daysBwFrstAndScndMnth))*30)/6;
					  map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
					  map.put("average", average);
					  return map;
				}
				else if(days>=30 && days<60){
					System.out.println("************days are greater than 30 & less than 60***************");
					System.out.println("************sedrForOneMnth = "+sedrForOneMnth);
					System.out.println("************sedrBwFrstAndOneMnth = "+sedrBwFrstAndOneMnth);
					int daysBwFrstAndOneMnth = (int)( (oneMonthPrevDate.getTime() - firstEntryDate.getTime()) / (1000 * 60 * 60 * 24));
					System.out.println("************daysBwFrstAndOneMnth = "+daysBwFrstAndOneMnth);
					average = (((sedrForOneMnth*2) +  (sedrBwFrstAndOneMnth/daysBwFrstAndOneMnth))*30)/3;
					map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
					map.put("average", average);
					return map;
				}
				else if(days<30){
					System.out.println("************days are less than 30***************");
					int daysBwFrstAndToday = (int)( (todayDate.getTime() - firstEntryDate.getTime()) / (1000 * 60 * 60 * 24));	
					average = ((sedrBwTodayAndFrstDate/daysBwFrstAndToday)*30);
					map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
					map.put("average", average);
					return map; 
				}else{
					map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
					return map;
				}
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
				return map;
			}
	}

	
	@RequestMapping(value = "/saveStnOrder", method = RequestMethod.POST)
	public @ResponseBody Object saveStnOrder(@RequestBody HBO_StnNotification hBOSN){
		logger.info("Enter into /saveStnOrder() : Cnmt = "+hBOSN.gethBOSN_cnmtNo()+" : Chln = "+hBOSN.gethBOSN_chlnNo()+" : Sedr = "+hBOSN.gethBOSN_sedrNo());
		Map<String,Object> map = new HashMap<String,Object>();
		User currentUser = (User) httpSession.getAttribute("currentUser");
		hBOSN.setbCode(currentUser.getUserCode());
		hBOSN.setUserCode(currentUser.getUserCode());
		hBOSN.sethBOSN_isView("no");
		hBOSN.sethBOSN_stnId(-1);
		int result = hBO_StnNotificationDAO.saveHBO_StnNot(hBOSN);
		if(result == 1){
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		logger.info("Exit from /saveStnOrder()");
		return map;
	}
	
	
	@RequestMapping(value = "/getHBOSNForSA", method = RequestMethod.POST)
	public @ResponseBody Object getHBOSNForSA() {
		System.out.println("enter into getHBOSNForSA function");
		List<HBO_StnNotification> HBOSNList = hBO_StnNotificationDAO.getHBOSNForSA();
		Map<String, Object> map = new HashMap<String, Object>();
		if(!HBOSNList.isEmpty()){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("HBOSNList",HBOSNList);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	@RequestMapping(value = "/confirmOrder", method = RequestMethod.POST)
	public @ResponseBody Object confirmOrder(@RequestBody HBO_StnNotification hBOSN) {
		System.out.println("enter into confirmOrder function");
		int  res = hBO_StnNotificationDAO.confirmOrder(hBOSN.gethBOSN_Id());
		Map<String, Object> map = new HashMap<String, Object>();
		if(res > 0){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
}