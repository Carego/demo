package com.mylogistics.constants;

public class OtherChargeCNTS {

	public static final String USER_CODE = "userCode";
	
	public static final String USER_BRANCH_CODE = "bCode";
	
	public static final String CREATION_TS = "creationTS";
	
	public static final String OTHER_CHG_ID = "otChgId";
	
	public static final String OTHER_CHG_BILL_NO = "otChgBillNo";
	
	public static final String OTHER_CHG_CONT_CODE = "otChgContCode";
	
	public static final String OTHER_CHG_CNMT_CODE = "otChgCnmtCode";
	
	public static final String OTHER_CHG_TYPE = "otChgType";
	
	public static final String OTHER_CHG_VALUE = "otChgValue";
	
}
