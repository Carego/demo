<style type="text/css">
.printable {display: none;}
            @media print {
                .noprint, .menuwrapper, .ui-dialog, footer, #toast-container {display: none !important;}
                .printable {display: block;
                		width: 10in !important;
                		position: fixed;
                		top: 0px;
                		left: 0px;
                		z-index: 999999;
                		}
                		
                		body {
						    width: 10in;
						    height: 210in;
						     
						  }
            }
            @PAGE {
				  size:A4 landscape;
				  margin:0.5cm 0.5cm 0.5cm 0.5cm;
			      }  
			      
			     			      
}
  .qlatterh{font-weight:bold;}
  .latterh{font-weight:bold;} 
   hr { 
    display: block;
    margin-top: 0.5em;
    margin-bottom: 0.5em;
    margin-left: auto;
    margin-right: auto;
    border-style: inset;
    border-width: 2px;
} 


</style>
<div ng-show="operatorLogin || superAdminLogin">
	<div class="noprint">
		<form name="blPrintForm" ng-submit=blPrintSubmit(blPrintForm) class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
					<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="brhId" name ="brhName" ng-model="branch" ng-required="true" ng-click="openBrhDB()" readonly>
		       			<label for="code">Billing Branch</label>	
		       		</div>
		       		
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="frBlId" name ="frBlName" ng-model="frBlNo" ng-required="true" >
		       			<label for="code">From Bill No</label>	
		       		</div>

		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="toBlId" name ="toBlName" ng-model="toBlNo" ng-required="true" >
		       			<label for="code">To Bill No</label>	
		       		</div>
		    </div>
		    
		    <div class="row">
	     		<div class="input-field col s12 center">
	       				<input class="validate" type ="submit" value="submit" >
	       		</div>
	      </div>	 
		</form>    
		
		<div class="row">
		     		<div class="input-field col s12 center">
		       				<input class="validate" type ="button" value="Download Annexture" ng-show="hmfBillFormat" ng-click="downloadAnx()">
		       		</div>

<!-- 		     		<div class="input-field col s12 center"> -->
<!-- 		       				<input class="validate" type ="button" value="Download Annexture" ng-show="relBillFormat" ng-click="downloadRelAnx()"> -->
<!-- 		       		</div> -->
		      </div>
		
	</div>
	
	
	<div id ="openBrhId" ng-hide="openBrhFlag">
		  <input type="text" name="filterBrh" ng-model="filterBrh" placeholder="Search....">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Branch Name</th>
 	  	  	  <th>FaCode</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="brh in brhList | filter:filterBrh">
		 	  <td><input type="radio"  name="brh"   value="{{ brh }}" ng-model="brhCode" ng-click="saveBranch(brh)"></td>
              <td>{{brh.branchName}}</td>
              <td>{{brh.branchFaCode}}</td>
          </tr>
      </table> 
	</div>
	
	
	<div id="printVsDB" ng-hide="printVsFlag" class="noprint">
		<div class="row">
			<div class="col s12 center"><h5 class="white-text">{{PRINT_STMT}}</h5></div>
		</div>
		<div>
			<div class="col s12 center">
				<a class="btn white-text" ng-click="cancelPrint()">Cancel</a>
				<a class="btn white-text" ng-click="printVs()">Yes</a>
			</div>
		</div>
	</div>
	
		
	
	<div class="printable"> 
			 <div class="divTableb" ng-repeat="bill in blList">
<!-- 			 format changed -->
		 	<div ng-show="bill.relBillFormat">
						 <div class="myheadRowlld">
			                <div class="mylatterhead">
			                <!-- <img src="resources/img/latterhead.png"" /> -->
			                </div>
			            </div> 
			        
			           <div class="divRowl spc">
			           <br><br><br><br><br><br><br><br><br><br>
			                <div class="mylatterhead1" ><h5>FREIGHT INVOICE</h5></div>
			                <div class="mylatterhead14" >(Original for Recpient)</div>
			             </div>
			          
			          <div class="divRowlld">
			                <div class="mylatterhead2">Invoice Srl No.{{bill.bill.blBillNo}}</span></div>
			                <div  class="mylatterhead2" style="text-align:right;" ng-if="blDate == undefined || blDate==''">Date: {{bill.bill.blBillDt | date:'dd.MM.yyyy'}}</span></div>
			           		 <div  class="mylatterhead2" style="text-align:right;" ng-if="blDate!= undefined && blDate!=''">Date: {{blDate | date:'dd.MM.yyyy'}}</span></div> 
			             </div>
			          
			            <div class="CSSTableGeneratorhd">
								<table>
									<tr> 
										<td>1.Name & Address of Goods Transport Agency</td>
										<td>M/s Care Go Logistics Pvt Ltd. <br>
										Regd. Office: S-405, Greater Kailash, Part-II,New Delhi-110048 <br>
										Email Id:  gurgaon@carego.co <br>
										Co CIN No. U6023DL2015PTC279265
										</td>
<!-- 										 {{selBrh.branchAdd}}</td> -->
									</tr>
			
			                         <tr> 
										<td>2. PAN of Goods Transport Agency</td>
										<td><span>AAGCC0032K</span></td>
									</tr>
									
									<tr> 
										<td>3. GST Number of GTA</td>
										<td>NOT APPLICABLE</td>
									</tr>
			
			                         <tr> 
										<td>4. Name & Address of Person Liable for payment of GST</td>
										<td>{{bill.cust.custName}}
										<hr>
											 {{bill.cust.cmpltAddress}}
										</td>
									</tr>
									
									<tr> 
										<td>5. GST of the Recipient of Service</td>
										<td>{{bill.bill.blGstNo}}</td>
									</tr>
			
			                         <tr> 
										<td>6. Description/Category of Service</td>
										<td>Transportation of goods by Road</td>
									</tr>
									
									<tr> 
										<td>7. Service Accounting Code(SAC) </td>
										<td>{{bill.cust.custSAC}}</td>
									</tr>
			
			                         <tr> 
										<td>8. Details of Transportation
										<br>
										i. Supply Invoice No. & Date
										<br>
										ii. Consignment Note No. & Date
										<br>
										iii. Weight of Consignment
										<br>
										iv. Description of Goods
										<br>
										v. Place of Origin
										<br>
										vi. Place of Destination
										<br>
										vii. Vehicle Number
										</td>
										<td>
										<br>
										As per Annexure attached
											<br>
										As per Annexure attached
											<br>
										As per Annexure attached
											<br>
										As per Annexure attached
											<br>
										As per Annexure attached
											<br>
										As per Annexure attached
											<br>
										As per Annexure attached
										</td>
									</tr>
									
									
			                          
			                    
									
								
									
			   <tr> 
				<td>viii. Value of Total Freight including loading/unloading Charges</td>
				<td><span>{{bill.bill.blFinalTot}}</span></td>
									</tr>
									
									 <tr> 
				<td>Taxable Service amount</td>
				<td><span>{{bill.bill.blFinalTot}}</span></td>
									</tr>
									
									 <tr> 
				<td>CGST &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.50%</td>
				<td><span ng-if="bill.cGST==0">Nill</span>
				<span ng-if="bill.cGST!=0">{{bill.cGST}}</span></td>
									</tr>
									
											 <tr> 
				<td>SGST &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.50%</td>
				<td><span ng-if="bill.sGST==0">Nill</span>
				<span ng-if="bill.sGST!=0">{{bill.sGST}}</td>
									</tr>
								
										 <tr> 
				<td>IGST &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 5.00%</td>
				<td><span ng-if="bill.iGST==0">Nill</span>
				<span ng-if="bill.iGST!=0">{{bill.iGST}}</td>
									</tr>	
									
											 <tr> 
				<td>Total GST Amount</td>
				<td><span>{{bill.cGST + bill.sGST + bill.iGST}}</span></td>
									</tr>
									
											 <tr> 
				<td>GST Amount in Words</td>
				<td><span>{{bill.gstAmt}}</span></td>
									</tr>
									
											 <tr> 
				<td>Total GST to be paid under Reverse Charge by the<br>
				Recipient of the Service
				</td>
				<td></td>
									</tr>
									
											 <tr> 
				<td>9. Total amount due from the Service Recipient</td>
				<td><span>{{bill.bill.blFinalTot}}</span></td>
									</tr>
									
											 <tr> 
				<td>10. Total amount due in words</td>
				<td><span>{{bill.bilAmt}}</span></td>
									</tr>
			
											
											 
											   <tr> 
										<td></td>
										<td>for M/s Care Go Logistics Pvt Ltd.
										<br>
										<br>
										<br>
										<br>
										<br>
										Authorised Signatory
										</td>
									</tr>
				
								</table>
							</div>
			       
			               
			               <div class="divRowlld">
			                <div class="mylatterhead3">Declaration</span></div>
			             </div>
			       
			             <div>
								<table>
									<tr  class="csstd"> 
										<td>1.  We hereby certify that we have not availed any input tax credit on goods & Services used for providing the service under this invoice.</td>
									</tr>
									<tr  class="csstd"> 
										<td>2. Certified that all the particulars given are true and correct</td>
									</tr>
								</table>
							</div>
			       
			            <div class="divRowlld">
			                <div class="mylatterhead4">Enclosure:</span></div>
			             </div> 
			             
			             <div class="divRowlld">
			                <div class="mylatterhead5">1. Annexure 2. Original LR's</span></div>
			             </div>      
			            
			             <!-- <div class="divRow1ld">
			                <div class="latterhead6">care go logistics pvt. ltd.</span></div>
			             </div> 
			             
			             <div class="divRowlld">
			                <div class="latterhead7">Regd. office - Flat no. 402, Baroda House Apartment, Sector-10, Dwarka, New Delhi-110075, Cin No.-U60231DL2015PTC279265</div>
			            <div class="latterhead7">Corporate Office - Sco-26, 1st Floor, Sec.-15, Part-II, Gurgaon-122001, Ph 0124-4205305</div>
			             </div> -->
			 	</div>

			 
				 <div ng-show="bill.hndlBillFormat">
						   <div class="headRow1">
                <div class="divCel1"><img src="resources/img/logo.png" width="357" height="106" /></div>
                <div  class="divCel1">
                          <div style="font-size: 16px; font-weight: bold; font-family: Arial;">Care Go Logistics Pvt. Ltd.</div>
                          <div style="font-size: 12px">Billing Office: S-405, Greater Kailash, Part-II,New Delhi-110048</div>
                         <div style="font-size: 12px">MSME Regd. No.: HR05E0000104</div>
                          <div style="font-size: 12px">Email: gurgaon@carego.co</div>
			 </div>
             </div>
            <div class="divRow1">
                  <div class="divCel01 boldbl">
                  	<div ng-show="bill.bill.blType == 'N'">FRIEGHT BILLS</div>
                  	<div ng-show="bill.bill.blType == 'S'">SUPPLEMENTARY BILLS</div>
                  </div>
                <div class="divCel02 boldbl">Co CIN No. U60231DL2015PTC279265</div>
                <div class="divCel03 boldbl">Service Tax Registration No.: AAGCC0032KSD001</div>
            </div>
            
            
            <div class="divRow1">
                  <div class="divCel04 boldbl">
                  <div class="divCel04 boldbl" style="border:0px; text-align:left;">To, M/s </div>
                
                  <div class="divCel04 boldbl" style="border:0px; margin-top:-15px; margin-left:100px; text-align:justify;">{{bill.cust.custName}}
                  <br>{{bill.cust.custAdd}},<br>{{bill.cust.custCity}} {{bill.cust.custPin}}</div>
                  
                  </div>
               <div class="divCel066 boldbl">
                   <div class="divCel06 boldbl">
                      <div style="margin-top:2px; text-align:center;">BILL NO.: {{bill.bill.blBillNo}}</div><br><div>DATE: {{bill.bill.blBillDt | date:'dd/MM/yyyy'}}</div>
                   </div>
               </div>
            </div>
            
            <div class="divRow1">
                    <div class="divCel25 boldbl">We hereby Submit our Bill for the Goods Transported as Under:</div>
	                <div class="divCel26 boldbl">OUR PAN NO: AAGCC0032K</div>
	                <div class="divCel26 boldbl">No TDS To Be Deducted w.e.f 01/10/2009</div>
            </div>
            
            
            <div class="divRow1">
               
			<div class="CSSTableGeneratorss">
				<table>
				 <tr>
				   <td class="latterh latterbold" width="450px"colspan="4">CONSIGNMENT DETAILS</td>
				   <td class="latterh latterbold" rowspan="2">Invoice</td>
				   <td class="latterh latterbold" rowspan="2">Truck No.</td>
				   <td class="latterh latterbold" rowspan="2">Product</td>
				   <td class="latterh latterbold" rowspan="2">Weight MT</td>
				   <td class="latterh latterbold" rowspan="2">Rate</td>
				   <td class="latterh latterbold" rowspan="2">Freight(A)</td>
				   <td class="latterh latterbold" rowspan="2">Detention(B)</td>
				   <td class="latterh latterbold"  rowspan="2">Bonus(C)</td>
				   <td class="latterh latterbold" rowspan="2">Ldng/Unldng(D)</td>
				   <td class="latterh latterbold" rowspan="2">Total(A+B+C+D)</td>
				 </tr>
				 <tr>
				   <td class="latterh latterbold">Date</td>
				   <td class="latterh latterbold">LR No.</td>
				   <td class="latterh latterbold">From Station</td>
				   <td class="latterh latterbold">To Station</td>
				 </tr>
				 <tr ng-repeat="bilDet in bill.blDetList">
				   <td class="latterh">{{bilDet.cnmt.cnmtDt | date:'dd/MM/yyyy'}}</td>
				   <td class="latterh">{{bilDet.cnmt.cnmtCode}}</td>
				   <td class="latterh">{{bilDet.frmStn}}</td>
				   <td class="latterh">{{bilDet.toStn}}</td>
				   <td class="latterh">{{bilDet.cnmt.cnmtInvoiceNo[0].invoiceNo}}</td>
				   <td class="latterh">{{bilDet.truckNo}}</td>
				   <td class="latterh">{{bilDet.cnmt.cnmtProductType}}</td>
				   <td class="latterh">
				   		<div ng-show="bilDet.billDet.bdBlBase == 'chargeWt'">{{bilDet.billDet.bdChgWt/1000 | number : 2}}</div>
		                <div ng-show="bilDet.billDet.bdBlBase == 'actualWt'">{{bilDet.billDet.bdActWt/1000 | number : 2}}</div>
		                <div ng-show="bilDet.billDet.bdBlBase == 'receiveWt'">{{bilDet.billDet.bdRecWt/1000 | number : 2}}</div>
				   </td>
				   <td class="latterh">{{bilDet.billDet.bdRate * 1000 | number : 2}}</td>
				   <td class="latterh">{{bilDet.billDet.bdFreight | number : 2}}</td>
				   <td class="latterh">{{bilDet.billDet.bdDetAmt | number : 2}}</td>
				   <td class="latterh">{{bilDet.billDet.bdBonusAmt | number : 2}}</td>
				   <td class="latterh">{{bilDet.billDet.bdLoadAmt | number : 2}}/{{bilDet.billDet.bdUnloadAmt | number : 2}}</td>
				   <td class="latterh">{{bilDet.billDet.bdTotAmt | number : 2}}</td>
				 </tr>
				
				</table>
				              
			</div>
      </div>
       
       
       
         <div class="divRow1">
                  <div class="divCel16">
                     <div class="divCel17 bold2bl">IT IS HEREBY DECLARE THAT WE ARE NOT AVAILING CENVAT CREDIT FACILITY IN RESPECT OF INPUTS/SERVICES /CAPITAL GOODS</div>
                     <div class="divCel17 bold2bl">WE HAVE NOT AVAILED ANY BENEFIT UNDER THE NOTIFICATION NO. 12/2003-SERVICE TAX DATED 20/06/2008</div>
                   </div>
               
               
             
                
                 <div class="divCel15 bold2bl" style="text-align:center;">
                     <div class="divCel14 bold1bl paddingbl" >SUB TOTAL</div>
                     <div class="divCel14 bold1bl paddingbl" >{{bill.bill.blSubTot}}</div> 
                  </div>
               
            </div>
       
        
         <div class="divRow1">
                     <div class="divCel18 bold1bl">Bank Name</div>
                     <div class="divCel00 bold1bl">A/c No.</div>
                     <div class="divCel18 bold1bl">IFSC CODE</div>
                     <div class="divCel05 bold1bl">Branch</div>
                     <div class="divCel19 bold2bl">Taxable Service tax= 30%</div>
                     <div class="divCel14 bold2bl" style="text-align:right; padding-right:5px;">{{bill.bill.blTaxableSerTax}}</div>
                     <div class="divCel14 bold2bl">&nbsp</div>
             </div>
             
             <div class="divRow1">
                     <div class="divCel18 bold1bl padding1bl">Kotak Mahindra Bank</div>
                     <div class="divCel00 bold1bl padding1bl">3011623336</div>
                     <div class="divCel18 bold1bl padding1bl">KKBK0000299</div>
                     <div class="divCel05 bold1bl paddingbl">61, Old Judicial Complex,<br> Sector-15, Gurgaon 122001</div>
                     <div class="divCel19 bold2bl pdng">Service Tax= 14%<br> Swachh Bharat Cess= 0.5%<br>Kisan kalyan Cess=0.5%</div>
                     <div class="divCel14 bold2bl pdng" style="text-align:right;"><span>{{bill.bill.blSerTax}}</span><br><span>{{bill.bill.blSwachBhCess}}</span><br><span>{{bill.bill.blKisanKalCess}}</span></div>
                     <div class="divCel14 bold2bl padding1bl">&nbsp</div>
             </div>
       
         <div class="divRow1">
                     <div class="divCel20 bold2bl paddingbl">
                        Only By A/c Payee Cheque / DD in Favour Of <span class="bold1">Care Go Logistics Pvt. Ltd.</span>, or Electronic Fund transfer to <span class="bold1">above Bank A/C</span>
                        <br>You are hereby requested to mail us Payment Detail on mail Id <span class="bold1">{{ selBrh.branchEmailId }}</span>
                     </div>
                     <div class="divCel20 bold1bl padding2bl">Pay {{bill.bilAmt}}</div>
                     <div class="divCel14 bold1bl padding2bl" style="text-align:right;">Total</div>
                     <div class="divCel14 bold1bl padding2bl">{{bill.bill.blFinalTot}}</div>
             </div>
           
             <div class="divRow1">
                <div class="divCel28 bold2bl" style="text-align:center;">
                     <div class="divCel31 bold1bl">Other Bank Details</div>
                     <div class="divCel27 bold1bl">Bank Name</div>
                     <div class="divCel27 bold1">A/c No.</div>
                     <div class="divCel27 bold1bl">IFSC CODE</div>
                     <div class="divCel29 bold1bl">Branch</div>
                     <div class="divCel27 bold1bl padding1bl">HDFC BANK</div>
                     <div class="divCel27 bold1bl padding1bl">13810330000037</div>
                     <div class="divCel27 bold1bl padding1bl">HDFC0001381</div>
                     <div class="divCel29 bold1bl paddingbl">472-11, OLD RAILWAY ROAD, DHOBI GHAT, NEAR KABIR BHAWAN, GURGAON</div>
                     <div class="divCel27 bold1bl padding1bl">ICICI BANK</div>
                     <div class="divCel27 bold1bl padding1bl">103105001831</div>
                     <div class="divCel27 bold1bl padding1bl">ICIC0001031</div>
                     <div class="divCel29 bold1bl paddingbl">SCO - 59, 60, OLD JUDICIAL COMPLEX, SECTOR - 15, GURGAON</div>
                     <div class="divCel27 bold1bl padding1bl">STATE BANK OF INDIA</div>
                     <div class="divCel27 bold1bl padding1bl">35334334071</div>
                     <div class="divCel27 bold1bl padding1bl">SBIN0060414</div>
                     <div class="divCel29 bold1bl paddingbl">SCO - 5,6,7 SECTOR - 15, <br>PART - 2, GURGAON</div>
                 </div>
                 <div class="divCel30" style="height:180px;">
                   <div class="divCel32 bold1bl" style="text-align:right; margin-top:10px; margin-left:-10px;">For Care Go Logistic Pvt. Ltd</div>
                    <div class="divCel32 bold1bl" style="text-align:left; margin-top:125px; margin-left:10px;">Cheked By</div>
                    <div class="divCel32 bold1bl" style="text-align:right; margin-top:-18px; margin-left:-10px;">Cheked By</div>
                 </div> 
             </div>
             
            <div class="divRow1">
                     <div class="divCel21 bold1bl padding3bl"><span>Corporate Office: SCO-26, 1st Floor, Sec. 15, Part-II, Gurgaon-122001,</span> &nbsp  <span>Ph.No.: 0124-4205305,</span> &nbsp <span>Email ID: corporate@tcgppl.com</span></div>
             </div> 
				 </div>
				 <!-- 			 format changed -->
				 <div ng-show="bill.simBillFormat">
							<div class="hRow1">
					<div class="dCel1">
						<img src="resources/img/logo.png" width="357" height="106" />
					</div>
					<div class="dCel1">
						<div
							style="font-size: 16px; font-weight: bold; font-family: Arial;">Care
							Go Logistics Pvt. Ltd.</div>
						<div style="font-size: 12px">Regd. Office: S-405, Greater Kailash, Part-II,New Delhi-110048</div>
						<div style="font-size: 12px">MSME Regd. No.: HR05E0000104</div>
						<div style="font-size: 12px">Email: gurgaon@carego.co</div>
					</div>
				</div>
				<div class="dRow1">
					<div class="mydCel01 bodbl">
						<div ng-show="bill.bill.blType == 'N'">Invoice</div>
						 <div ng-show="bill.bill.blType == 'S'">SUPPLEMENTARY BILLS</div> 
					</div>
					
					<div class="mydCel03 bodbl">SAC No.:
						{{bill.cust.custSAC}}</div>
				</div>


				<div class="dRow1">
					<div class="dCel04 bodbl">
						<div class="dCel04 bodbl" style="border: 0px; text-align: left;">To,
							M/s</div>

						<div class="dCel04 bodbl"
							style="border: 0px; margin-top: -15px; margin-left: 100px; text-align: justify;">
							{{bill.cust.custName}} <br>{{bill.cust.custAdd}},<br>{{bill.cust.custCity}}
							{{bill.cust.custPin}}
							<br>
                           
							
						</div>
						 <p style="text-align: left;margin-left: 5px;">GSTIN NO: {{bill.bill.blGstNo}}</p>

					</div>
					<div class="dCel066 bodbl">
						<div class="dCel06 bodbl">
							<div style="margin-top: 2px; text-align: center;">
							<br>
							Invoice NO.:
								{{bill.bill.blBillNo}}</div>
							<br>
							
							<div style="height: 27px;">DATE: {{bill.bill.blBillDt | date:'dd/MM/yyyy'}}
							<br>
							
							</div>
						</div>
					</div>
				</div>

				<div class="dRow1">
					<div class="mydCel25 bodbl">We Hereby Submit Our Invoice For The
						Goods Transported as Under:</div>
						
					<!-- <div class="mydCel26 bodbl"></div> -->
					
					<div class="mydCel03 bodbl">OUR PAN NO: AAGCC0032K</div>
					
					
					
				</div>

				<div class="dRow1">

					<div class="CSSTabGeneratorss">
						<table>
							<tr>
								<td width="300px" colspan="6">CONSIGNMENT DETAILS</td>								
							    <td rowspan="2"> Product</td>
				  				<td rowspan="2">RECEIVER PARTY</td>
							    <td rowspan="2"> Actual Weight</td>
				  				<td rowspan="2">Charge Weight</td>
							   	<td rowspan="2">Rate</td>
							   	<td rowspan="2">Freight</td>
							   	<td rowspan="2">Detention(B)</td>
							   	<td rowspan="2">EXPRESS DL(C)</td>
							   	<td rowspan="2">Ldng/Unldng(D)</td>
							   	<td rowspan="2">Two Point Col(E)</td>
							   	<td rowspan="2">Total(A+B+C+D+E)</td>								
							</tr>
							<tr>
								<td><strong class="bbl">Date</strong></td>
								<td><strong class="bbl">Invoice No.</strong></td>
								<td><strong class="bbl">LR No.</strong></td>
								<td><strong class="bbl">Lorry No.</strong></td>
								<td><strong class="bbl">From Station</strong></td>
								<td><strong class="bbl">To Station</strong></td>								   
							</tr>
							<tr ng-repeat="bilDet in bill.blDetList">
							
								<td>{{bilDet.cnmt.cnmtDt | date:'dd/MM/yyyy'}}</td>
							  	<td>{{bilDet.cnmt.cnmtInvoiceNo[0].invoiceNo}}<br>{{bilDet.cnmt.cnmtInvoiceNo[1].invoiceNo}}</td>
							   	<td>{{bilDet.cnmt.cnmtCode}}</td>
							   	<td>{{bilDet.truckNo}}</td>
							   	<td>{{bilDet.frmStn}}</td>
							   	<td> {{bilDet.toStn}}</td>
							   	<td> {{bilDet.cnmt.cnmtProductType}}</td>
							   	<td> {{bilDet.consignee.custName}}</td>
							   	<td>{{bilDet.billDet.bdActWt/1000 | number : 3}}</td>
							   	<td>{{bilDet.billDet.bdChgWt/1000 | number : 3}}</td>
							   	<td>{{bilDet.billDet.bdRate * 1000 | number : 2}}</td>
							    <td>{{bilDet.billDet.bdFreight | number : 2}}</td>
							    <td>{{bilDet.billDet.bdDetAmt | number : 2}}</td>
							   	<td>{{bilDet.billDet.bdBonusAmt | number : 2}}</td>
							   	<td>{{bilDet.billDet.bdLoadAmt | number : 2}}/{{bilDet.billDet.bdUnloadAmt | number : 2}}</td>
							   	<td>{{bilDet.billDet.bdOthChgAmt | number : 2}}</td> 
							   	<td>{{bilDet.billDet.bdTotAmt | number : 2}}</td>		   
							  
							</tr>

						</table>

					</div>
				</div>


			<!-- 	<div class="dRow1">
					<div class="del16">
						<div class="del17 bod2bl">IT IS HEREBY DECLARE THAT WE ARE
							NOT AVAILING CENVAT CREDIT FACILITY IN RESPECT OF INPUTS/SERVICES
							/CAPITAL GOODS</div>
						<div class="del17 bod2bl">WE HAVE NOT AVAILED ANY BENEFIT
							UNDER THE NOTIFICATION NO. 12/2003-SERVICE TAX DATED 20/06/2008</div>
					</div>




					<div class="dCel15 bod2bl" style="text-align: center;">
						<div class="dCel14 bod1bl pdingbl">SUB TOTAL</div>
						<div class="dCel14 bod1bl pdingbl">{{bill.bill.blSubTot}}</div>
					</div>

				</div> -->


				<!-- <div class="dRow1">
					<div class="dCel18 bod1bl">Bank Name</div>
					<div class="dCel00 bod1bl">A/c No.</div>
					<div class="dCel18 bod1bl">IFSC CODE</div>
					<div class="dCel05 bod1bl">Branch</div>
					<div class="dCel19 bod2bl">Taxable Service tax= 30%</div>
					<div class="dCel14 bod2bl"
						style="text-align: right; padding-right: 5px;">{{bill.bill.blTaxableSerTax}}</div>
					<div class="dCel14 bod2bl">&nbsp</div>
				</div> -->

				<!-- <div class="dRow1">
					<div class="dCel18 bod1bl pding1bl">Kotak Mahindra Bank</div>
					<div class="dCel00 bod1bl pding1bl">3011623336</div>
					<div class="dCel18 bod1bl pding1bl">KKBK0000299</div>
					<div class="dCel05 bod1bl pdingbl">
						61, Old Judicial Complex,<br> Sector-15, Gurgaon 122001
					</div>
					<div class="dCel19 bod2bl pdng">
						Service Tax= 14%<br>Swachh Bharat Cess= 0.5%<br>Kisan kalyan Cess=0.5%</div>
					<div class="dCel14 bod2bl pdng" style="text-align: right;">
						<span>{{bill.bill.blSerTax}}</span><br>
						<span>{{bill.bill.blSwachBhCess}}</span><br>
						<span>{{bill.bill.blKisanKalCess}}</span>
					</div>
					<div class="dCel14 bod2bl pding1bl">&nbsp</div>
				</div> -->

				<div class="dRow1">
					<div class="mydCel20 bod2bl pdingbl">
						 Amount in words: &nbsp;&nbsp;&nbsp;&nbsp; <span class="bbl">{{bill.bilAmt}}</span>
						 <hr>
						 	<br>
						 Only By A/c Payee Cheque / DD in Favour Of <span class="bbl">Care Go Logistics Pvt. Ltd.,</span><span> or Electronic Fund transfer to below Bank A/c </span>
		                 <br>
						
					</div>
					
					<div class="dRow1">
					
					<!-- <div class="dCel20 bod1bl pding2bl">Pay {{bill.bilAmt}}</div> -->
					<div class="mydCel14 bod1bl pding2bl" style="text-align: right;"><br>Total<br></div>
					<div class="mydCel14 bod1bl pding2bl"><br>{{bill.bill.blFinalTot}}<br></div>
				</div>

				<div class="dRow1">
					<div class="dCel28 bod2bl" style="text-align: center;">
						<div class="dCel31 bod1bl"> Bank Details</div>
						<div class="mydCel27 bod1bl">Bank Name</div>
						<div class="hedCel27 bod1">A/c No.</div>
						<div class="hedCel27 bod1bl">IFSC CODE</div>
						<div class="mydCel29 bod1bl">Branch</div>
						<!-- <div class="mydCel27 bod1bl pding1bl">HDFC BANK<br><br></div>
						<div class="hedCel27 bod1bl pding1bl">13810330000037<br><br></div>
						<div class="hedCel27 bod1bl pding1bl">HDFC0001381<br><br></div>
						<div class="mydCel29 bod1bl pdingbl">472-11, OLD RAILWAY ROAD,
							DHOBI GHAT, NEAR KABIR BHAWAN, GURGAON</div>
						<div class="mydCel27 bod1bl pding1bl">ICICI BANK</div>
						<div class="hedCel27 bod1bl pding1bl">103105001831</div>
						<div class="hedCel27 bod1bl pding1bl">ICIC0001031</div>
						<div class="mydCel29 bod1bl pdingbl">SCO - 59, 60, OLD
							JUDICIAL COMPLEX, SECTOR - 15, GURGAON</div> -->
							
							<div class="mydCel27 bod1bl pding1bl"><br><br><br><br><br><br>KOTAK MAHINDRA BANK <br><br><br><br><br><br></div>
						<div class="hedCel27 bod1bl pding1bl"><br><br><br><br><br><br>3011623336<br><br><br><br><br><br></div>
						<div class="hedCel27 bod1bl pding1bl"><br><br><br><br><br><br>KKBK0000299<br><br><br><br><br><br></div>
						<div class="mydCel29 bod1bl pdingbl"><br><br><br><br><br><br>61, Old Judicial Complex, Sector-15, Gurgaon 122001<br><br><br><br><br><br></div>
							
						<!-- <div class="mydCel27 bod1bl pding1bl">STATE BANK OF INDIA</div>
						<div class="hedCel27 bod1bl pding1bl">35334334071</div>
						<div class="hedCel27 bod1bl pding1bl">SBIN0060414</div>
						<div class="mydCel29 bod1bl pdingbl">
							SCO - 5,6,7 SECTOR - 15, <br>PART - 2, GURGAON
						</div> -->
					</div>
					<div class="dCel30" style="height: 240px;">
						<div class="dCel32 bod1bl"
							style="text-align: right; margin-top: 10px; margin-left: -10px;">For
							Care Go Logistic Pvt. Ltd</div>
						<div class="dCel32 bod1bl"
							style="text-align: left; margin-top: 125px; margin-left: 10px;">Cheked
							By</div>
						<div class="dCel32 bod1bl"
							style="text-align: right; margin-top: -18px; margin-left: -10px;">Cheked
							By</div>
					</div>
				</div>

				<div style="border:0px solid #000;width:100%;">
					<div class="dCel21 bod1bl pding3bl" style="text-align: left">
						As per notification No. 5/2017. Central Tax dated 19.6.2017, GTA are exempted under GST and it is payable by consignor/consignee under CM @ 5% Rupees &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{bill.cGST + bill.sGST + bill.iGST}}
						<br>
						We declare that no ITC availed by us on inputs and inputs services used for providing GTA Services.
					</div>
				</div>
				 </div>
	</div>			 
				 <!-- 			 format changed -->
				<div ng-show="bill.renBillFormat">
								 			
				            <!--  <div class="hdRow1">
				                <div class="dCel1"><img src="resources/img/logo.png" width="357" height="106" /></div>
				                <div  class="dCel1">
				                          <div style="font-size: 16px; font-weight: bold; font-family: Arial;">Care Go Logistics Pvt. Ltd.</div>
				                          <div style="font-size: 12px">Billing Office: {{ selBrh.branchAdd }}</div>
				                          <div style="font-size: 12px">Phone:{{ selBrh.branchPhone }}</div> 
				                          <div style="font-size: 12px">Email: {{ selBrh.branchEmailId }}</div>
							 </div>
				             </div> -->
				 <!--            <div class="dRow1">
				                  <div class="dCel01 bdbl">
				                  	<div ng-show="bill.bill.blType == 'N'">FRIEGHT BILLS</div>
				                  	<div ng-show="bill.bill.blType == 'S'">SUPPLEMENTARY BILLS</div>
				                  </div>
				                <div class="dCel02 bdbl">Co CIN No. U60231DL2015PTC279265</div>
				                <div class="dCel03 bdbl">Service Tax Registration No.: AAGCC0032KSD001</div>
				            </div>
				            
				            
				            <div class="dRow1">
				                  <div class="dCel04 bdbl">
				                  <div class="dCel04 bdbl" style="border:0px; text-align:left;">To, M/s </div>
				                
				                  <div class="dCel04 bdbl" style="border:0px; margin-top:-15px; margin-left:100px; text-align:justify;">{{bill.cust.custName}}
				                  <br>{{bill.cust.custAdd}},<br>{{bill.cust.custCity}} {{bill.cust.custPin}}</div>
				                  
				                  </div>
				               <div class="dCel066 bdbl">
				                   <div class="dCel06 bdbl">
				                      <div style="margin-top:2px; text-align:center;">BILL NO.: {{bill.bill.blBillNo}}</div><br><div>DATE: {{bill.bill.blBillDt | date:'dd/MM/yyyy'}}</div>
				                   </div>
				               </div>
				            </div>
				            
				            <div class="dRow1">
				                    <div class="dCel25 bdbl">We hereby Submit our Bill for the Goods Transported as Under:</div>
					                <div class="dCel26 bdbl">OUR PAN NO: AAGCC0032K</div>
					                <div class="dCel26 bdbl">No TDS To Be Deducted w.e.f 01/10/2009</div>
				            </div>
				            
				             <div class="dRow1">
				                
				                 <div class="CSSTleGeneratorss">
				<table>
				<tr>
				   <td width="450px"colspan="4">CONSIGNMENT DETAILS</td>
				   <td rowspan="2">Invoice</td>
				   <td rowspan="2">Truck No.</td>
				   <td rowspan="2">Product</td>
				   <td rowspan="2">Weight MT</td>
				   <td rowspan="2">Rate</td>
				   <td rowspan="2">Freight(A)</td>
				   <td rowspan="2">Detention(B)</td>
				   <td rowspan="2">Bonus(C)</td>
				   <td rowspan="2">Ldng/Unldng(D)</td>
				   <td rowspan="2">Total(A+B+C+D)</td>
				 </tr>
				 <tr>
				   <td>Date</td>
				   <td>LR No.</td>
				   <td>From Station</td>
				   <td>To Station</td>
				 </tr>
				 
				  <tr ng-repeat="bilDet in bill.blDetList">
				   <td>{{bilDet.cnmt.cnmtDt | date:'dd/MM/yyyy'}}</td>
				   <td>{{bilDet.cnmt.cnmtCode}}</td>
				   <td>{{bilDet.frmStn}}</td>
				   <td>{{bilDet.toStn}}</td>
				   <td>{{bilDet.cnmt.cnmtInvoiceNo[0].invoiceNo}}</td>
				   <td>{{bilDet.truckNo}}</td>
				   <td>{{bilDet.cnmt.cnmtProductType}}</td>
				   <td>
				   		<div ng-show="bilDet.billDet.bdBlBase == 'chargeWt'">{{bilDet.billDet.bdChgWt/1000 | number : 3}}</div>
		                <div ng-show="bilDet.billDet.bdBlBase == 'actualWt'">{{bilDet.billDet.bdActWt/1000 | number : 3}}</div>
		                <div ng-show="bilDet.billDet.bdBlBase == 'receiveWt'">{{bilDet.billDet.bdRecWt/1000 | number : 3}}</div>
				   </td>
				   <td>{{bilDet.billDet.bdRate * 1000 | number : 2}}</td>
				   <td>{{bilDet.billDet.bdFreight | number : 2}}</td>
				   <td>{{bilDet.billDet.bdDetAmt | number : 2}}</td>
				   <td>{{bilDet.billDet.bdBonusAmt | number : 2}}</td>
				   <td>{{bilDet.billDet.bdLoadAmt | number : 2}}/{{bilDet.billDet.bdUnloadAmt | number : 2}}</td>
				   <td>{{bilDet.billDet.bdTotAmt | number : 2}}</td>
				 </tr>
				 
				</table>
				               
				            </div>
				       </div>
				       
				      
				        
				         <div class="dRow1">
				                 <div class="nextrow">
				                     <div class="dCel18 bd1bl">Bank Name</div>
				                     <div class="dCel00 bd1bl">A/c No.</div>
				                     <div class="dCel18 bd1bl">IFSC CODE</div>
				                     <div class="dCel05 bd1bl">Branch</div>
				                             
				                     <div class="dCel18 bd1bl pdding1bl">Kotak Mahindra Bank</div>
				                     <div class="dCel00 bd1bl pdding1bl">3011623336</div>
				                     <div class="dCel18 bd1bl pdding1bl">KKBK0000299</div>
				                     <div class="dCel05 bd1bl pddingbl">61, Old Judicial Complex,<br> Sector-15, Gurgaon 122001</div>
				                 </div>
				                 
				               <div class="nextrow1">
				                 <div class="dCel141 bd1bl pdding2bl">SUB TOTAL</div>
				                     <div class="dCel141 bd1bl pdding2bl">{{bill.bill.blSubTot}}</div>
				               </div>
				                 
				             </div>
				      
				       
				         <div class="dRow1">
				                     <div class="dCel20 bd2bl pddingbl">
				                        Only By A/c Payee Cheque / DD in Favour Of <span class="bold1">Care Go Logistics Pvt. Ltd.</span>, or Electronic Fund transfer to <span class="bold1">above Bank A/C</span>
				                        <br>You are hereby requested to mail us Payment Detail on mail Id <span class="bold1">{{ selBrh.branchEmailId }}</span>
				                     </div>
				                     <div class="dCel20 bd1bl pdding2bl">Pay {{bill.bilAmt}}</div>
				                     <div class="dCel14 bd1bl pdding2bl" style="text-align:right;">Total</div>
				                     <div class="dCel14 bd1bl pdding2bl">{{bill.bill.blFinalTot}}</div>
				             </div>
				           
				             <div class="dRow1">
				                <div class="dCel28 bd2bl">
				                    <div class="dCel17 bd2bl pddingbl"><p style="margin-top:10px;">IT IS HEREBY DECLARE THAT WE ARE NOT AVAILING CENVAT CREDIT FACILITY IN RESPECT OF INPUTS/SERVICES /CAPITAL GOODS</p></div>
				                     <div class="dCel17 bd2bl pddingbl"><p style="margin-top:10px;">WE HAVE NOT AVAILED ANY BENEFIT UNDER THE NOTIFICATION NO. 12/2003-SERVICE TAX DATED 20/06/2008</p></div>
				                 </div>
				                 <div class="dCel30" style="height:100px;">
				                   <div class="dCel32 bd1bl" style="text-align:right; margin-top:10px; margin-left:-10px;">For Care Go Logistic Pvt. Ltd</div>
				                    <div class="dCel32 bd1bl" style="text-align:left; margin-top:50px; margin-left:10px;">Cheked By</div>
				                    <div class="dCel32 bd1bl" style="text-align:right; margin-top:-18px; margin-left:-10px;">Cheked By</div>
				                 </div> 
				             </div>
				             
				            <div class="dRow1">
				                     <div class="dCel21 bd1bl pdding3bl"><span>Corporate Office: SCO-26, 1st Floor, Sec. 15, Part-II, Gurgaon-122001,</span> &nbsp  <span>Ph.No.: 0124-4205305,</span> &nbsp <span>Email ID: corporate@tcgppl.com</span></div>
				             </div>  -->
				               <div class="reqmhRow1">
                <div class="reqmCel1"><img src="resources/img/logo.png" width="357" height="106" /></div>
                <div  class="reqmCel1">
                          <div style="font-size: 16px; font-weight: bold; font-family: Arial;">Care Go Logistics Pvt. Ltd.</div>
                          <div style="font-size: 11px">Billing Office: S-405, Greater Kailash, Part-II,New Delhi-110048</div>
                          <div style="font-size: 12px">MSME Regd. No.: HR05E0000104</div>
                          <div style="font-size: 11px">Email: gurgaon@carego.co
                          <br>
                          Web Site: Care.go People, CIN: U60231DL2015PTC279265
                          </div> 
                         
			 </div>
             </div>
             <div class="reqmRow1">
                    <div class="reqmCel25 reqmboldbl" style="width: 975px; text-align: center;">Tax Invoice</div>
	             
            </div>
				             
				                    <div class="reqmRow1">
                  <div class="reqmCel04 reqmboldbl" style="height: 140px;width: 975px;">
                  <div class="reqmCel04 reqmboldbl" style="border:0px; text-align:left;"><br><br>&nbsp;&nbsp;To,M/s. </div>
                
                  <div class="reqmCel04 reqmboldbl" style="border:0px; margin-top:-15px;margin-left:90px;  text-align:justify;">{{bill.cust.custName}}
                  <br>{{bill.cust.custAdd}},<br>{{bill.cust.custCity}} {{bill.cust.custPin}}<br><p style="text-align: left"> GSTIN-{{bill.bill.blGstNo}}</p></div>
                  <br>
                   <div style="border: 1px solid black;position: absolute;    top: 125px;right: 250px;width: 315px;height:140px; text-align: left;">
                   <strong>&nbsp;&nbsp;Name of Consignee</strong><br>&nbsp;&nbsp;{{bill.blDetList[0].consignee.custName}}<br>&nbsp;&nbsp;{{bill.blDetList[0].toStn}}
                   </div> 
                <div style="position: absolute;    top: 125px;right: -15px;width: 315px;height: 140px;">
                 <table style="border: 1px solid black;">
                <tr><td style="border: 1px solid black;text-align: center" colspan="2">Tax Invoice No.</td><td style="border: 1px solid black;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td style="border: 1px solid black;text-align: center" colspan="2">{{bill.bill.blBillNo}}</td></tr>
                <tr><td style="border: 1px solid black;text-align: center" colspan="2">Date</td><td style="border: 1px solid black;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td style="border: 1px solid black;text-align: center" colspan="2">{{bill.bill.blBillDt}}</td></tr>
                <tr><td rowspan="2" colspan="2" style="text-align: center;border: 1px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>PAN NO.</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td style="border: 1px solid black;text-align: center" colspan="2" rowspan="2"><br>AAGCC0032K</td></tr>
                <tr><td style="border: 1px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                
               </tr>
            
                </table>
                
                </div>
                 
     
                </div>
            </div>
            
            <div class="reqmRow1">
                    <div class="reqmCel25 reqmboldbl" style="width: 975px; text-align: center;"> GTA(Goods Transport Service)</div>
	             
            </div>
            
            
            <div class="reqmRow1">
               
			<div class="reqmCSTableGeneratorss">
				<table>
				 <tr>
			   <td class="reqmltterh reqmltterbold" colspan="4">CONSIGNMENT DETAILS</td> 
				  <td class="reqmltterh reqmltterbold" rowspan="2" >DLY DATE</td>

				   <td class="reqmltterh reqmltterbold" rowspan="2">TRUCK NO</td>
				   <td class="reqmltterh reqmltterbold" rowspan="2">PRODUCT</td>
				   <td class="reqmltterh reqmltterbold" rowspan="2">WEIGHT<br>(Ton)</td>
				 <td class="reqmltterh reqmltterbold" rowspan="2">Rate<br>(Rs)</td>

				   <td class="reqmltterh reqmltterbold" rowspan="2">FRIEGHT<br>(Rs)</td>
				   <td class="reqmltterh reqmltterbold" rowspan="2">UNLOADIN<br>G</td>
				   <td class="reqmltterh reqmltterbold" rowspan="2">DETENTION</td>
				   <td class="reqmltterh reqmltterbold" rowspan="2">DELIVERY <br>CHARGE</td> 
				   <td class="reqmltterh reqmltterbold" rowspan="2">TOTAL(Rs.)</td>
				  
				 </tr>
				 <tr>
				 <td class="reqmltterh reqmltterbold" >Date</td>
				   <td class="reqmltterh reqmltterbold" >LR. No.</td>
				   <td class="reqmltterh reqmltterbold" >From Station</td>
				  <!--  <td class="reqmltterh reqmltterbold" >Gate out <br> DT</td> -->
				   <td class="reqmltterh reqmltterbold" >To Station</td>
				 <!--   <td class="reqmltterh reqmltterbold" >STATE</td>
				   <td class="reqmltterh reqmltterbold" >FRIEGHT<br>RATE</td>
				   <td class="reqmltterh reqmltterbold" >AMOUNT</td>
				   <td class="reqmltterh reqmltterbold" >REMARKS</td>  -->
				 </tr>
			
				 <tr ng-repeat="bilDet in bill.blDetList">
				   <!-- <td class="reqmltterh">{{bilDet.cnmt.cnmtCode}}</td> -->
<!-- 				    <td class="reqmltterh">{{bilDet.cnmt.cnmtInvoiceNo[0].invoiceNo}}<br>{{bilDet.cnmt.cnmtInvoiceNo[1].invoiceNo}}</td> -->
				   <td class="reqmltterh reqmltterbold">{{bilDet.cnmt.cnmtDt | date:'dd/MM/yyyy'}}</td>
				   <td class="reqmltterh">{{bilDet.cnmt.cnmtCode}}</td>
				   <td class="reqmltterh">{{bilDet.frmStn}}</td>
				   <td class="reqmltterh">{{bilDet.toStn}}</td>
				   
				   <td class="reqmltterh">{{bilDet.cnmt.cnmtDtOfDly}}</td>
				     <td class="reqmltterh">{{bilDet.truckNo}}</td>
				      <td class="reqmltterh">{{bilDet.cnmt.cnmtProductType}}</td>
				   <td class="reqmltterh">
				   		<div ng-show="bilDet.billDet.bdBlBase == 'chargeWt'">{{bilDet.billDet.bdChgWt/1000 | number : 3}}</div>
		                <div ng-show="bilDet.billDet.bdBlBase == 'actualWt'">{{bilDet.billDet.bdActWt/1000 | number : 3}}</div>
		                <div ng-show="bilDet.billDet.bdBlBase == 'receiveWt'">{{bilDet.billDet.bdRecWt/1000 | number : 3}}</div>
				   </td>
				   <td class="reqmltterh">{{bilDet.billDet.bdRate * 1000 | number : 3}}</td>
				   <td class="reqmltterh">{{bilDet.billDet.bdFreight | number : 3}}</td>
				    <td class="reqmltterh">{{bilDet.billDet.bdUnloadAmt | number : 3}}</td>
				    <td class="reqmltterh">{{bilDet.billDet.bdDetAmt | number : 3}}</td>
				    <td class="reqmltterh">{{bilDet.billDet.bdOthChgAmt}}</td>
				    <td class="reqmltterh">{{bilDet.billDet.bdFreight | number : 3}}</td>
				      <!--  <td class="reqmltterh">{{bilDet.billDet.bdFreight | number : 3}}</td>
				        <td class="reqmltterh">{{bilDet.billDet.bdFreight | number : 3}}</td> -->
				   
				 </tr>
				
			
				
				<tr>
				   <td class="reqmltterh reqmltterbold" colspan="10">&nbsp;</td>
				  
				   <td class="reqmltterh reqmltterbold">&nbsp;</td>
				   <td class="reqmltterh reqmltterbold">&nbsp;</td>
				   <td class="reqmltterh reqmltterbold">&nbsp;</td>
				   <td class="reqmltterh reqmltterbold">&nbsp;{{bill.bill.blFinalTot}}</td>
				<!--    <td class="reqmltterh reqmltterbold">&nbsp;TOTAL</td>
				   <td class="reqmltterh reqmltterbold">&nbsp;</td> -->
				</tr>
					<tr>
				   <td colspan="2" class="reqmltterh reqmltterbold" style="text-align: center;">In words-:</td>
				   <td colspan="13" class="reqmltterh reqmltterbold" style="text-align: left;">&nbsp; {{bill.bilAmt}}</td>
				  
				</tr>
				
				</table>
				              
			</div>
      </div>
       
       
       
     
        
     
           
             <div class="reqmRow1">
            
                 <div class="reqmCel30" style="height:105px;">
               <p style="text-align: left;font-size: 70%; font-weight: bold;">&nbsp;&nbsp;Note:"GST tax to be paid by Consignor"</p>
              <p style="text-align: left;font-size: 80%; font-weight: bold;">&nbsp;&nbsp;Service Accounting Code (SAC) : {{bill.cust.custSAC}}<br>
             &nbsp;&nbsp; Notification Code. NO. 13/2017- Central Tax(Rate) dt 28<sup>th</sup> Jun 2017<br>
             &nbsp;&nbsp; Place of Origin- UP - State Code- 09<br>
             
              <!--   </p>
                <p style="text-align: left;font-size: 70%;">
             Certified that the Credit of input tax charged on goods and services used in supplying of GTA service has not been taken in view of notification issued under Goods & Service Tax.
                </p> -->
                
                
                 <!--   <div class="reqmCel32 reqmbold1bl" style="text-align:right; margin-top:10px; font-weight: bold;">For, Care Go Logistic Pvt. Ltd</div>
                    <div class="reqmCel32 reqmbold1bl" style="text-align:right; margin-top:50px; margin-left:-250px;"></div>
                    <div class="reqmCel32 reqmbold1bl" style="text-align:right; margin-top:-14px; font-weight: bold;">Auth. Signatory</div> -->
                 </div> 
             </div>
             
            <!-- <div class="reqmRow1">
                     <div class="reqmCel21 reqmbold1bl reqmpadding3bl"><span>Corporate Office: SCO-26, 1st Floor, Sec. 15, Part-II, Gurgaon-122001,</span> &nbsp  <span>Ph.No.: 0124-4205305,</span> &nbsp <span>Email ID: corporate@tcgppl.com</span></div>
             </div>  -->
				    
				      <div class="reqmRow1">
                    <div class="reqmCel25 reqmboldbl" style="width: 975px;">
                    <div style="position: absolute; width: 300px; height: 90px;">
                    <table style="margin-left: 10px;">
                    
                    
                    <tr><td>Bank:<br>
                    Branch:<br>
                    Account No<br>
                    RTGS/NEFT
                    </td>
                  
                    <td style="width: 500px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Kotak Mahindra Bank<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Gurgaon<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3011623336<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;KKBK0000299</td>
                  <!--   <td style="border: 1px solid black; width: 200px;" ></td> -->
                    </tr>

                    </table>
                    
                    </div>
                      <div style="border: 1px solid black;position: absolute;    top: 570px;right: 400px;width: 150px;height:90px; text-align: left;">
                  
                   </div> 
                   
                    <div class="reqmCel32 reqmbold1bl" style="text-align:right; margin-top:10px; font-weight: bold;margin-left:-160px;">For:- Care Go Logistic Pvt. Ltd</div>
                    <div class="reqmCel32 reqmbold1bl" style="text-align:right; margin-top:50px; margin-left:-250px;">Checked By</div>
                    <div class="reqmCel32 reqmbold1bl" style="text-align:right; margin-top:-14px; font-weight: bold;">Bill Incharge</div>
</div>
	             
            </div>    
								 
				</div> 

<!-- Bhu bill format start -->

		<div ng-show="bill.bhuBillFormat">
				<!-- <!-- <div class="hRow1">
					<div class="dCel1">
						<img src="resources/img/logo.png" width="357" height="106" />
					</div>
					<div class="dCel1">
						<div
							style="font-size: 16px; font-weight: bold; font-family: Arial;">Care
							Go Logistics Pvt. Ltd.</div>
						<div style="font-size: 12px">Billing Office: {{
							selBrh.branchAdd }}</div>
						<div style="font-size: 12px">Phone:{{ selBrh.branchPhone }}</div>
						<div style="font-size: 12px">Email: {{ selBrh.branchEmailId
							}}</div>
					</div>
				</div>
				<div class="dRow1">
					<div class="dCel01 bodbl">
						<div ng-show="bill.bill.blType == 'N'">FRIEGHT BILLS</div>
						<div ng-show="bill.bill.blType == 'S'">SUPPLEMENTARY BILLS</div>
					</div>
					<div class="dCel02 bodbl">Co CIN No. U60231DL2015PTC279265</div>
					<div class="dCel03 bodbl">Service Tax Regidtration No.:
						AAGCC0032KSD001</div>
				</div>


				<div class="dRow1">
					<div class="dCel04 bodbl">
						<div class="dCel04 bodbl" style="border: 0px; text-align: left;">To,
							M/s</div>

						<div class="dCel04 bodbl"
							style="border: 0px; margin-top: -15px; margin-left: 100px; text-align: justify;">
							{{bill.cust.custName}} <br>{{bill.cust.custAdd}},<br>{{bill.cust.custCity}}
							{{bill.cust.custPin}}
						</div>

					</div>
					<div class="dCel066 bodbl">
						<div class="dCel06 bodbl">
							<div style="margin-top: 2px; text-align: center;">BILL NO.:
								{{bill.bill.blBillNo}}</div>
							<br>
							<div>DATE: {{bill.bill.blBillDt | date:'dd/MM/yyyy'}}</div>
						</div>
					</div>
				</div>

				<div class="dRow1">
					<div class="dCel25 bodbl">We hereby Submit our Bill for the
						Goods Transported as Under:</div>
					<div class="dCel26 bodbl">OUR PAN NO: AAGCC0032K</div>
					<div class="dCel26 bodbl">No TDS To Be Deducted w.e.f
						01/10/2009</div>
				</div>

				<div class="dRow1">

					<div class="CSSTabGeneratorss">
						<table>
							<tr>
								<td width="300px" colspan="4">CONSIGNMENT DETAILS</td>
								<td rowspan="2">Invoice</td>
								<td rowspan="2">Truck No.</td>
								<td rowspan="2">Del DT</td>
								<td rowspan="2">Product</td>
								<td rowspan="2">Weight MT</td>
								<td rowspan="2">Rate</td>
								<td rowspan="2">Freight(A)</td>
								<td rowspan="2">Detention(B)</td>
								<td rowspan="2">Bonus(C)</td>
								<td rowspan="2">Ldng/Unldng(D)</td>
								<td rowspan="2">Total(A+B+C+D)</td>

							</tr>
							<tr>
								<td><strong class="bbl">Date</strong></td>
								<td><strong class="bbl">LR No.</strong></td>
								<td><strong class="bbl">From Station</strong></td>
								<td><strong class="bbl">To Station</strong></td>
							</tr>
							<tr ng-repeat="bilDet in bill.blDetList">
								<td>{{bilDet.cnmt.cnmtDt | date:'dd/MM/yyyy'}}</td>
								<td>{{bilDet.cnmt.cnmtCode}}</td>
								<td>{{bilDet.frmStn}}</td>
								<td>{{bilDet.toStn}}</td>
								<td>{{bilDet.cnmt.cnmtInvoiceNo[0].invoiceNo}}</td>
								<td>{{bilDet.truckNo}}</td>
								<td>{{bilDet.delDt | date:'dd/MM/yyyy'}}</td>
								<td>{{bilDet.cnmt.cnmtProductType}}</td>
								<td>
									<div ng-show="bilDet.billDet.bdBlBase == 'chargeWt'">{{bilDet.billDet.bdChgWt/1000
										| number : 3}}</div>
									<div ng-show="bilDet.billDet.bdBlBase == 'actualWt'">{{bilDet.billDet.bdActWt/1000
										| number : 3}}</div>
									<div ng-show="bilDet.billDet.bdBlBase == 'receiveWt'">{{bilDet.billDet.bdRecWt/1000
										| number : 3}}</div>
								</td>
								<td>{{bilDet.billDet.bdRate * 1000 | number : 2}}</td>
								<td>{{bilDet.billDet.bdFreight | number : 2}}</td>
								<td>{{bilDet.billDet.bdDetAmt | number : 2}}</td>
								<td>{{bilDet.billDet.bdBonusAmt | number : 2}}</td>
								<td>{{bilDet.billDet.bdLoadAmt | number :
									2}}/{{bilDet.billDet.bdUnloadAmt | number : 2}}</td>
								<td>{{bilDet.billDet.bdTotAmt | number : 2}}</td>
							</tr>

						</table>

					</div>
				</div>


				<div class="dRow1">
					<div class="del16">
						<div class="del17 bod2bl">IT IS HEREBY DECLARE THAT WE ARE
							NOT AVAILING CENVAT CREDIT FACILITY IN RESPECT OF INPUTS/SERVICES
							/CAPITAL GOODS</div>
						<div class="del17 bod2bl">WE HAVE NOT AVAILED ANY BENEFIT
							UNDER THE NOTIFICATION NO. 12/2003-SERVICE TAX DATED 20/06/2008</div>
					</div>




					<div class="dCel15 bod2bl" style="text-align: center;">
						<div class="dCel14 bod1bl pdingbl">SUB TOTAL</div>
						<div class="dCel14 bod1bl pdingbl">{{bill.bill.blSubTot}}</div>
					</div>

				</div>


				<div class="dRow1">
					<div class="dCel18 bod1bl">Bank Name</div>
					<div class="dCel00 bod1bl">A/c No.</div>
					<div class="dCel18 bod1bl">IFSC CODE</div>
					<div class="dCel05 bod1bl">Branch</div>
					<div class="dCel19 bod2bl">Taxable Service tax= 30%</div>
					<div class="dCel14 bod2bl"
						style="text-align: right; padding-right: 5px;">{{bill.bill.blTaxableSerTax}}</div>
					<div class="dCel14 bod2bl">&nbsp</div>
				</div>

				<div class="dRow1">
					<div class="dCel18 bod1bl pding1bl">Kotak Mahindra Bank</div>
					<div class="dCel00 bod1bl pding1bl">3011623336</div>
					<div class="dCel18 bod1bl pding1bl">KKBK0000299</div>
					<div class="dCel05 bod1bl pdingbl">
						61, Old Judicial Complex,<br> Sector-15, Gurgaon 122001
					</div>
					<div class="dCel19 bod2bl pdng">
						Service Tax= 14%<br>Swachh Bharat Cess= 0.5%<br>Kisan
						kalyan Cess=0.5%
					</div>
					<div class="dCel14 bod2bl pdng" style="text-align: right;">
						<span>{{bill.bill.blSerTax}}</span><br> <span>{{bill.bill.blSwachBhCess}}</span><br>
						<span>{{bill.bill.blKisanKalCess}}</span>
					</div>
					<div class="dCel14 bod2bl pding1bl">&nbsp</div>
				</div>

				<div class="dRow1">
					<div class="dCel20 bod2bl pdingbl">
						Only By A/c Payee Cheque / DD in Favour Of <span class="bbl">Care
							Go Logistics Pvt. Ltd.</span>, or Electronic Fund transfer to <span
							class="bold1">above Bank A/C</span> <br>You are hereby
						requested to mail us Payment Detail on mail Id <span class="bbl">{{
							selBrh.branchEmailId }}</span>
					</div>
					<div class="dCel20 bod1bl pding2bl">Pay {{bill.bilAmt}}</div>
					<div class="dCel14 bod1bl pding2bl" style="text-align: right;">Total</div>
					<div class="dCel14 bod1bl pding2bl">{{bill.bill.blFinalTot}}</div>
				</div>

				<div class="dRow1">
					<div class="dCel28 bod2bl" style="text-align: center;">
						<div class="dCel31 bod1bl">Other Bank Details</div>
						<div class="dCel27 bod1bl">Bank Name</div>
						<div class="dCel27 bod1">A/c No.</div>
						<div class="dCel27 bod1bl">IFSC CODE</div>
						<div class="dCel29 bod1bl">Branch</div>
						<div class="dCel27 bod1bl pding1bl">HDFC BANK</div>
						<div class="dCel27 bod1bl pding1bl">13810330000037</div>
						<div class="dCel27 bod1bl pding1bl">HDFC0001381</div>
						<div class="dCel29 bod1bl pdingbl">472-11, OLD RAILWAY ROAD,
							DHOBI GHAT, NEAR KABIR BHAWAN, GURGAON</div>
						<div class="dCel27 bod1bl pding1bl">ICICI BANK</div>
						<div class="dCel27 bod1bl pding1bl">103105001831</div>
						<div class="dCel27 bod1bl pding1bl">ICIC0001031</div>
						<div class="dCel29 bod1bl pdingbl">SCO - 59, 60, OLD
							JUDICIAL COMPLEX, SECTOR - 15, GURGAON</div>
						<div class="dCel27 bod1bl pding1bl">STATE BANK OF INDIA</div>
						<div class="dCel27 bod1bl pding1bl">35334334071</div>
						<div class="dCel27 bod1bl pding1bl">SBIN0060414</div>
						<div class="dCel29 bod1bl pdingbl">
							SCO - 5,6,7 SECTOR - 15, <br>PART - 2, GURGAON
						</div>
					</div>
					<div class="dCel30" style="height: 180px;">
						<div class="dCel32 bod1bl"
							style="text-align: right; margin-top: 10px; margin-left: -10px;">For
							Care Go Logistic Pvt. Ltd</div>
						<div class="dCel32 bod1bl"
							style="text-align: left; margin-top: 125px; margin-left: 10px;">Cheked
							By</div>
						<div class="dCel32 bod1bl"
							style="text-align: right; margin-top: -18px; margin-left: -10px;">Cheked
							By</div>
					</div>
				</div>

				<div class="dRow1">
					<div class="dCel21 bod1bl pding3bl">
						<span>Corporate Office: SCO-26, 1st Floor, Sec. 15,
							Part-II, Gurgaon-122001,</span> &nbsp <span>Ph.No.: 0124-4205305,</span>
						&nbsp <span>Email ID: corporate@tcgppl.com</span>
					</div>
				</div>
 --> 
 
 

				<div class="hRow1">
					<div class="dCel1">
						<img src="resources/img/logo.png" width="357" height="106" />
					</div>
					<div class="dCel1">
						<div
							style="font-size: 16px; font-weight: bold; font-family: Arial;">Care
							Go Logistics Pvt. Ltd.</div>
						<div style="font-size: 12px">Regd. Office: S-405, Greater
							Kailash, Part-II,New Delhi-110048</div>
 							<div style="font-size: 12px">MSME Regd. No.: HR05E0000104</div>
						<div style="font-size: 12px">Email: gurgaon@carego.co</div>
					</div>
				</div>
				<div class="dRow1">
					<div class="mydCel01 bodbl" style="width: 518px;">
						<div ng-show="bill.bill.blType == 'N'">TAX INVOICE</div>
						<div ng-show="bill.bill.blType == 'S'">SUPPLEMENTARY BILLS</div>
					</div>
					<div class="mydCel03 bodbl" style="width: 250px;">Co CIN No.: U60231DL2015PTC279265</div>
					<div class="mydCel03 bodbl" style="width: 250px;">SAC No.: {{bill.cust.custSAC}}</div>
				</div>


				<div class="dRow1">
					<div class="dCel04 bodbl">
						<div class="dCel04 bodbl" style="border: 0px; text-align: left;">To,
							M/s</div>

						<div class="dCel04 bodbl"
							style="border: 0px; margin-top: -15px; text-align: left;">
							<br>{{bill.cust.custName}} <br>{{bill.cust.custAdd}},<br>{{bill.cust.custCity}}
							{{bill.cust.custPin}} <br>


						</div>
						<p style="text-align: left; margin-left: 5px;">GST NO:
							{{bill.bill.blGstNo}}</p>

					</div>
					<div class="dCel066 bodbl">
						<div class="dCel06 bodbl">
							<div style="margin-top: 2px; text-align: center;">
								<br> Invoice NO.: {{bill.bill.blBillNo}}
							</div>
							<br>
                             <br>
							<div style="height: 27px;">
								DATE: {{bill.bill.blBillDt | date:'dd/MM/yyyy'}} <br>

							</div>
						</div>
					</div>
				</div>

				<div class="dRow1">
					<div class="mydCel25 bodbl" style="width: 518px;">We Hereby Submit Our Invoice For
						The Goods Transported as Under:</div>
					<div class="mydCel26 bodbl" style="width: 250px;">OUR PAN NO: AAGCC0032K</div>
					<div class="mydCel26 bodbl" style="width: 250px;">&nbsp;</div>
				</div>

				<div class="dRow1">

					<div class="CSSTabGeneratorss">
						<table>
							<tr>
								<td width="300px" colspan="7">CONSIGNMENT DETAILS</td>
								<td rowspan="2">Weight<br> MT</td>
					<!-- 			<td rowspan="2">Charge Weight</td> -->
								<td rowspan="2">Rate</td>
								<td rowspan="2">Freight(A)</td>
								<td rowspan="2">Detention(B)</td>
								<td rowspan="2">Bonus(C)</td>
								<td rowspan="2">Ldng/Unldng<br> AMT.</td>
								<td rowspan="2">Others(E)</td>
								<td rowspan="2">Total(A+B+C+D+E)</td>
							</tr>
							<tr>
								<td><strong class="bbl">Date</strong></td>
								
								<td><strong class="bbl">LR No.</strong></td>
							
								<td><strong class="bbl">From Station</strong></td>
								<td><strong class="bbl">To Station</strong></td>
									<td><strong class="bbl">Vehicle No.</strong></td>
								<td><strong class="bbl">UNLOADING<br>DATE</strong></td>
								<td><strong class="bbl">Invoice No.</strong></td>
							</tr>
							<tr ng-repeat="bilDet in bill.blDetList">

								<td>{{bilDet.cnmt.cnmtDt | date:'dd/MM/yyyy'}}</td>
								
								<td>{{bilDet.cnmt.cnmtCode}}</td>
							
								<td>{{bilDet.frmStn}}</td>
								<td>{{bilDet.toStn}}</td>
									<td>{{bilDet.truckNo}}</td>
									<td> {{bilDet.arDt | date:'dd.MM.yyyy'}}</td>
								<td>{{bilDet.cnmt.cnmtInvoiceNo[0].invoiceNo}}<br>
								{{bilDet.cnmt.cnmtInvoiceNo[1].invoiceNo}}<br>
								{{bilDet.cnmt.cnmtInvoiceNo[2].invoiceNo}}
								</td>
								<td>
									<div ng-show="bilDet.billDet.bdBlBase == 'chargeWt'">{{bilDet.billDet.bdChgWt/1000
										| number : 3}}</div>
									<div ng-show="bilDet.billDet.bdBlBase == 'actualWt'">{{bilDet.billDet.bdActWt/1000
										| number : 3}}</div>
									<div ng-show="bilDet.billDet.bdBlBase == 'receiveWt'">{{bilDet.billDet.bdRecWt/1000
										| number : 3}}</div>
								</td>
								<td>{{bilDet.billDet.bdRate * 1000 | number : 2}}</td>
								<td>{{bilDet.billDet.bdFreight | number : 2}}</td>
								<td>{{bilDet.billDet.bdDetAmt | number : 2}}</td>
								<td>{{bilDet.billDet.bdBonusAmt | number : 2}}</td>
								<td>{{bilDet.billDet.bdLoadAmt | number :
									2}}/{{bilDet.billDet.bdUnloadAmt | number : 2}}</td>
								<td>{{bilDet.billDet.bdOthChgAmt | number : 2}}</td>
								<td>{{bilDet.billDet.bdTotAmt | number : 2}}</td>

							</tr>
							<tr>
							<td>Bank Name</td>
							<td>A/c No.</td>
							<td>IFSC CODE</td>
							<td colspan="10">Amount In Words:</td>
						<td rowspan="2"><br>Total</td>
						<td rowspan="2"><br>{{bill.bill.blFinalTot}}</td>
							</tr>
							<tr>
							<td >Kotak Mahindra Bank<br><br></td>
							<td >3011623336<br><br></td>
							<td >KKBK0000299<br><br></td>
							<td  colspan="10" style="font-size: 15px;">{{bill.bilAmt}}</td>
							</tr>
                      <tr><td colspan="15" style="text-align: left;">Payment to be made Only By Cheque/ DD in Favour Of Care Go Logistics Pvt. Ltd., or Electronic Fund transfer to above Kotak Bank A/C</td></tr>
						</table>

					</div>
				</div>


				<!-- 	<div class="dRow1">
					<div class="del16">
						<div class="del17 bod2bl">IT IS HEREBY DECLARE THAT WE ARE
							NOT AVAILING CENVAT CREDIT FACILITY IN RESPECT OF INPUTS/SERVICES
							/CAPITAL GOODS</div>
						<div class="del17 bod2bl">WE HAVE NOT AVAILED ANY BENEFIT
							UNDER THE NOTIFICATION NO. 12/2003-SERVICE TAX DATED 20/06/2008</div>
					</div>




					<div class="dCel15 bod2bl" style="text-align: center;">
						<div class="dCel14 bod1bl pdingbl">SUB TOTAL</div>
						<div class="dCel14 bod1bl pdingbl">{{bill.bill.blSubTot}}</div>
					</div>

				</div> -->


				<!-- <div class="dRow1">
					<div class="dCel18 bod1bl">Bank Name</div>
					<div class="dCel00 bod1bl">A/c No.</div>
					<div class="dCel18 bod1bl">IFSC CODE</div>
					<div class="dCel05 bod1bl">Branch</div>
					<div class="dCel19 bod2bl">Taxable Service tax= 30%</div>
					<div class="dCel14 bod2bl"
						style="text-align: right; padding-right: 5px;">{{bill.bill.blTaxableSerTax}}</div>
					<div class="dCel14 bod2bl">&nbsp</div>
				</div> -->

				<!-- <div class="dRow1">
					<div class="dCel18 bod1bl pding1bl">Kotak Mahindra Bank</div>
					<div class="dCel00 bod1bl pding1bl">3011623336</div>
					<div class="dCel18 bod1bl pding1bl">KKBK0000299</div>
					<div class="dCel05 bod1bl pdingbl">
						61, Old Judicial Complex,<br> Sector-15, Gurgaon 122001
					</div>
					<div class="dCel19 bod2bl pdng">
						Service Tax= 14%<br>Swachh Bharat Cess= 0.5%<br>Kisan kalyan Cess=0.5%</div>
					<div class="dCel14 bod2bl pdng" style="text-align: right;">
						<span>{{bill.bill.blSerTax}}</span><br>
						<span>{{bill.bill.blSwachBhCess}}</span><br>
						<span>{{bill.bill.blKisanKalCess}}</span>
					</div>
					<div class="dCel14 bod2bl pding1bl">&nbsp</div>
				</div> -->

			<!-- 	<div class="dRow1">
					<div class="mydCel20 bod2bl pdingbl">
						Amount in words: &nbsp;&nbsp;&nbsp;&nbsp; <span class="bbl">{{bill.bilAmt}}</span>
						<hr>
						<br> Only By A/c Payee Cheque / DD in Favour Of <span
							class="bbl">Care Go Logistics Pvt. Ltd.,</span><span> or
							Electronic Fund transfer to below Bank A/c </span> <br>

					</div>

					<div class="dRow1">

						<div class="dCel20 bod1bl pding2bl">Pay {{bill.bilAmt}}</div>
						<div class="mydCel14 bod1bl pding2bl" style="text-align: right;">
							<br>Total<br>
						</div>
						<div class="mydCel14 bod1bl pding2bl">
							<br>{{bill.bill.blFinalTot}}<br>
						</div>
					</div>

					<div class="dRow1">
						<div class="dCel28 bod2bl" style="text-align: center;">
							<div class="dCel31 bod1bl">Bank Details</div>
							<div class="dCel27 bod1bl">Bank Name</div>
							<div class="dCel27 bod1">A/c No.</div>
							<div class="dCel27 bod1bl">IFSC CODE</div>
							<div class="dCel29 bod1bl">Branch</div>
							<div class="dCel27 bod1bl pding1bl">HDFC BANK</div>
							<div class="dCel27 bod1bl pding1bl">13810330000037</div>
							<div class="dCel27 bod1bl pding1bl">HDFC0001381</div>
							<div class="dCel29 bod1bl pdingbl">472-11, OLD RAILWAY
								ROAD, DHOBI GHAT, NEAR KABIR BHAWAN, GURGAON</div>
							<div class="dCel27 bod1bl pding1bl">ICICI BANK</div>
							<div class="dCel27 bod1bl pding1bl">103105001831</div>
							<div class="dCel27 bod1bl pding1bl">ICIC0001031</div>
							<div class="dCel29 bod1bl pdingbl">SCO - 59, 60, OLD
								JUDICIAL COMPLEX, SECTOR - 15, GURGAON</div>
							<div class="dCel27 bod1bl pding1bl">STATE BANK OF INDIA</div>
							<div class="dCel27 bod1bl pding1bl">35334334071</div>
							<div class="dCel27 bod1bl pding1bl">SBIN0060414</div>
							<div class="dCel29 bod1bl pdingbl">
								SCO - 5,6,7 SECTOR - 15, <br>PART - 2, GURGAON
							</div>
						</div>
						<div class="dCel30" style="height: 180px;">
							<div class="dCel32 bod1bl"
								style="text-align: right; margin-top: 10px; margin-left: -10px;">For
								Care Go Logistic Pvt. Ltd</div>
							<div class="dCel32 bod1bl"
								style="text-align: left; margin-top: 125px; margin-left: 10px;">Cheked
								By</div>
							<div class="dCel32 bod1bl"
								style="text-align: right; margin-top: -18px; margin-left: -10px;">Cheked
								By</div>
						</div>
					</div>

					<div class="dRow1">
						<div class="dCel21 bod1bl pding3bl" style="text-align: left">
							As per notification No. 5/2017. Central Tax dated 19.6.2017, GTA
							are exempted under GST and it is payable by consignor/consignee
							under CM @ 5% Rupees &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{bill.cGST +
							bill.sGST + bill.iGST}} <br> We declare that no ITC availed
							by us on inputs and inputs services used for providing GTA
							Services.
						</div>
					</div>
				</div> -->
					<div class="remRow1">
					<div class="remCel30" style="height: 100px;">
						<div class="remCel32 rembold1bl"
							style="text-align: right; margin-top: 10px;">For Care Go
							Logistic Pvt. Ltd</div>
						<div class="remCel32 rembold1bl"
							style="text-align: right; margin-top: 50px; margin-left: -250px;">Checked
							By</div>
						<div class="remCel32 rembold1bl"
							style="text-align: right; margin-top: -14px;">Bill Incharge</div>
					</div>
				</div>
			
			</div>			
		
			<div ng-show="bill.hilBillFormat">
					<div class="headRowb">
		                <div class="divCell1b"><img src="resources/img/logo.png" width="357" height="106" /></div>
		                <div  class="divCell1b">
		                          <div style="font-size: 16px; font-weight: bold; font-family: Arial;">Care Go Logistics Pvt. Ltd.</div>
		                          <div style="font-size: 12px">Billing Office: S-405, Greater Kailash, Part-II,New Delhi-110048</div>
		                          <div style="font-size: 12px">MSME Regd. No.: HR05E0000104</div>
		                          <div style="font-size: 12px">Email: gurgaon@carego.co</div>
					 </div>
		            </div>
		            <div class="divRowb">
		                  <div class="divCell01b boldb">
		                  	<div ng-show="bill.bill.blType == 'N'">FRIEGHT BILLS</div>
		                  	<div ng-show="bill.bill.blType == 'S'">SUPPLEMENTARY BILLS</div>
		                  </div>
		                <div class="divCell02b boldb">Co CIN No. U60231DL2015PTC279265</div>
		                <div class="divCell03b boldb">Service Tax Registration No.: AAGCC0032KSD001</div>
		            </div>
		            
		            
		            <div class="divRowb">
		                  <div class="divCell04b boldb">
		                  <div class="divCell04b boldb" style="border:0px; text-align:left;">To, M/s </div>
		                
		                  <div class="divCell04b boldb" style="border:0px; margin-top:-15px; margin-left:100px; text-align:justify;">{{bill.cust.custName}}
		                  <br>{{bill.cust.custAdd}},<br>{{bill.cust.custCity}} {{bill.cust.custPin}}
		                 <br> 
		                 </div>
		                  <p style="text-align: left;margin-left: 5px;">GSTIN - {{bill.bill.blGstNo}}
		                  </div>
		               <div class="divCell066b boldb">
		                   <div class="divCell06b boldb">
		                      <div style="margin-top:0px; text-align:center;">BILL NO.: {{bill.bill.blBillNo}}</div><br><br><br><div>DATE: {{bill.bill.blBillDt | date:'dd/MM/yyyy'}}</div>
		                   </div>
		               </div>
		            </div>
		            
		            <div class="divRowb">
		                    <div class="divCell25b boldb">We hereby Submit our Bill for the Goods Transported as Under:</div>
			                <div class="divCell26b boldb">OUR PAN NO: AAGCC0032K</div>
			                <div class="divCell26b boldb">No TDS To Be Deducted w.e.f 01/10/2009</div>
		            </div>
		            
		             <div class="divRowb">
		                  <div class="divCell10b bold2b" style="text-align:center;">
		                     <div class="divCell11b bold2b">CONSIGNMENT DETAILS</div>
		                     <div class="divCell12b bold1b">Date</div>
		                     <div class="divCell12b bold1b">LR No.</div>
		                     <div class="divCell12b bold1b">From Station</div>
		                     <div class="divCell12b bold1b">To Station</div>
		                     <div ng-repeat="bilDet in bill.blDetList">
		                     	<div class="divCell12b bold2b">{{bilDet.cnmt.cnmtDt | date:'dd/MM/yyyy'}}</div>
			                     <div class="divCell12b bold2b">{{bilDet.cnmt.cnmtCode}}</div>
			                     <div class="divCell12b bold2b">{{bilDet.frmStn}}</div>
			                     <div class="divCell12b bold2b">{{bilDet.toStn}}</div> 
		                     </div>
		                  </div>
		               
		               
		               <div class="divCell14b_2 bold1b paddingb_2">Invoice No</div>
		                <div class="divCell14b_2 bold1b paddingb_2">Weight MT</div>
		                <div class="divCell14b_2 bold1b paddingb_2">Rate</div>
		                <div class="divCell14b_2 bold1b paddingb_2">Freight(A)</div>
		                <div class="divCell14b_2 bold1b paddingb_2">otherCharge</div>
		                 <div class="divCell14b_2 bold1b paddingb_2">po</div>
		                <div class="divCell14b_2 bold1b paddingb_2">Detention(B)</div>
		                <div class="divCell14b bold1b paddingb">Ldng/Unldng(D)</div>
		                <div class="divCell14b bold1b paddingb">Total(A+B+C+D)</div>
		                
		                 <div class="divCell15b bold2b" style="text-align:center;" ng-repeat="bilDet in bill.blDetList">
		                     <div class="divCell14b_2 bold2b" >{{bilDet.cnmt.cnmtInvoiceNo[0].invoiceNo}}</div>
		                     <div class="divCell14b_2 bold2b" >
		                     	<div ng-show="bilDet.billDet.bdBlBase == 'chargeWt'">{{bilDet.billDet.bdChgWt/1000 | number : 3}}</div>
		                     	<div ng-show="bilDet.billDet.bdBlBase == 'actualWt'">{{bilDet.billDet.bdActWt/1000 | number : 3}}</div>
		                     	<div ng-show="bilDet.billDet.bdBlBase == 'receiveWt'">{{bilDet.billDet.bdRecWt/1000 | number : 3}}</div>
		                     </div>
		                     <div class="divCell14b_2 bold2b" >{{bilDet.billDet.bdRate * 1000 | number : 3}}</div>
		                     <div class="divCell14b_2 bold2b">{{bilDet.billDet.bdFreight | number : 2}}</div>
		                  <div class="divCell14b_2 bold2b">{{bilDet.billDet.bdOthChgAmt}}</div>
		                     <div class="divCell14b_2 bold2b">{{bilDet.billDet.bdPoNum}}</div>
		                     <div class="divCell14b_2 bold2b" >{{bilDet.billDet.bdDetAmt | number : 2}}</div>
		                     <div class="divCell14b bold2b" >{{bilDet.billDet.bdLoadAmt | number : 2}}/{{bilDet.billDet.bdUnloadAmt | number : 2}}</div>
		                     <div class="divCell14b bold2b" >{{bilDet.billDet.bdTotAmt | number : 2}}</div> 
		                  </div>
		               
		            </div>
		       
		       
		       
		         <div class="divRowb">
		                  <div class="divCell16b">
		                     <div class="divCell17b bold2b">IT IS HEREBY DECLARE THAT WE ARE NOT AVAILING CENVAT CREDIT FACILITY IN RESPECT OF INPUTS/SERVICES /CAPITAL GOODS</div>
		                     <div class="divCell17b bold2b">WE HAVE NOT AVAILED ANY BENEFIT UNDER THE NOTIFICATION NO. 12/2003-SERVICE TAX DATED 20/06/2008</div>
		                   </div>
		               
		               
		             
		                
		                 <div class="divCell15b bold2b" style="text-align:center;">
		                     <div class="divCell14b bold1b paddingb" >SUB TOTAL</div>
		                     <div class="divCell14b bold1b paddingb" >{{bill.bill.blSubTot}}</div> 
		                  </div>
		               
		            </div>
		       
		        
		         <div class="divRowb">
		                     <div class="divCell18b bold1b">Bank Name</div>
		                     <div class="divCell12b bold1b">A/c No.</div>
		                     <div class="divCell18b bold1b">IFSC CODE</div>
		                     <div class="divCell05b bold1b">Branch</div>
		                     <div class="divCell19b bold2b">Taxable Service tax= 30%</div>
		                     <div class="divCell14b bold2b" style="text-align:right; padding-right:5px;">{{bill.bill.blTaxableSerTax}}</div>
		                     <div class="divCell14b bold2b">&nbsp</div>
		             </div>
		             
		             <div class="divRowb">
		                     <div class="divCell18b bold1b padding1b">Kotak Mahindra Bank</div>
		                     <div class="divCell12b bold1b padding1b">3011623336</div>
		                     <div class="divCell18b bold1b padding1b">KKBK0000299</div>
		                     <div class="divCell05b bold1b paddingb">61, Old Judicial Complex,<br> Sector-15, Gurgaon 122001</div>
		                     <div class="divCell19b bold2b pdng">Service Tax= 14%<br>Swachh Bharat Cess= 0.5%<br>Kisan kalyan Cess=0.5%</div>
		                     <div class="divCell14b bold2b pdng" style="text-align:right;"><span>{{bill.bill.blSerTax}}</span><br><span>{{bill.bill.blSwachBhCess}}</span><br><span>{{bill.bill.blKisanKalCess}}</span></div>
		                     <div class="divCell14b bold2b padding1b">&nbsp</div>
		             </div>
		       
		         <div class="divRowb">
		                     <div class="divCell20b bold2b paddingb">
		                        Only By A/c Payee Cheque / DD in Favour Of <span class="bold1">Care Go Logistics Pvt. Ltd.</span>, or Electronic Fund transfer to <span class="bold1">above Bank A/C</span>
		                        <br>You are hereby requested to mail us Payment Detail on mail Id <span class="bold1">{{ selBrh.branchEmailId }}</span>
		                     </div>
		                     <div class="divCell20b bold1b padding2b">Pay {{bill.bilAmt}}</div>
		                     <div class="divCell14b bold1b padding2b" style="text-align:right;">Total</div>
		                     <div class="divCell14b bold1b padding2b">{{bill.bill.blFinalTot}}</div>
		             </div>
		           
		             <div class="divRowb">
		                <div class="divCell28b bold2b" style="text-align:center;">
		                      <div class="divCell31b bold1b"><br><br><br><br><br><br><br><br><br><br><br><br></div>
		                    <!-- <div class="divCell27b bold1b">Bank Name</div>
		                     <div class="divCell27b bold1b">A/c No.</div>
		                     <div class="divCell27b bold1b">IFSC CODE</div>
		                     <div class="divCell29b bold1b">Branch</div>
		                     <div class="divCell27b bold1b padding1b">HDFC BANK</div>
		                     <div class="divCell27b bold1b padding1b">13810330000037</div>
		                     <div class="divCell27b bold1b padding1b">HDFC0001381</div>
		                     <div class="divCell29b bold1b paddingb">472-11, OLD RAILWAY ROAD, DHOBI GHAT, NEAR KABIR BHAWAN, GURGAON</div>
		                     <div class="divCell27b bold1b padding1b">ICICI BANK</div>
		                     <div class="divCell27b bold1b padding1b">103105001831</div>
		                     <div class="divCell27b bold1b padding1b">ICIC0001031</div>
		                     <div class="divCell29b bold1b paddingb">SCO - 59, 60, OLD JUDICIAL COMPLEX, SECTOR - 15, GURGAON</div>
		                     <div class="divCell27b bold1b padding1b">STATE BANK OF INDIA</div>
		                     <div class="divCell27b bold1b padding1b">35334334071</div>
		                     <div class="divCell27b bold1b padding1b">SBIN0060414</div>
		                     <div class="divCell29b bold1b paddingb">SCO - 5,6,7 SECTOR - 15, <br>PART - 2, GURGAON</div>
		                -->  </div>
		                 <div class="divCell30b" style="height:180px;">
		                   <div class="divCell32b bold1" style="text-align:right; margin-top:10px; margin-left:-10px;">For Care Go Logistic Pvt. Ltd</div>
		                    <div class="divCell32b bold1" style="text-align:left; margin-top:125px; margin-left:10px;">Cheked By</div>
		                    <div class="divCell32b bold1b" style="text-align:right; margin-top:-18px; margin-left:-10px;">Cheked By</div>
		                 </div> 
		             </div>
		             
		            <div class="divRowb">
		                     <div class="divCell21b bold1b padding3b"><span>Corporate Office: SCO-26, 1st Floor, Sec. 15, Part-II, Gurgaon-122001,</span> &nbsp  <span>Ph.No.: 0124-4205305,</span> &nbsp <span>Email ID: corporate@tcgppl.com</span></div>
		             </div> 
				 </div>

	 <!-- end hil bil format -->
			 <!-- ******************************start haldia customer************************************* -->
<!-- 	 format changed -->
				<div ng-show="bill.adaniBillFormat">
				 
				 		    <div class="rheadRow1">
                <div class="rdivCel1"><img src="resources/img/logo.png" width="357" height="106" /></div>
                <div  class="rdivCel1">
                          <div style="font-size: 16px; font-weight: bold; font-family: Arial;">Care Go Logistics Pvt. Ltd.</div>
                          <div style="font-size: 12px">Regd. Office: S-405, Greater Kailash, Part-II,New Delhi-110048</div>
                          <div style="font-size: 12px">MSME Regd. No.: HR05E0000104</div>
                          <div style="font-size: 12px">Email: gurgaon@carego.co</div>
                          <div style="font-size: 12px">Corporate Office: SCO-44, 4th Floor, Old Judicial Complex, Civil Lines Gurugram - 122001</div>
			 </div>
             </div>
          	<div class="dRow1">
					<div class="mydCel01 bodbl">
						<div ng-show="bill.bill.blType == 'N'">TAX INVOICE</div>
						 <div ng-show="bill.bill.blType == 'S'">SUPPLEMENTARY BILLS</div> 
					</div>
					
					<div class="mydCel03 bodbl">SAC Code.:
						{{bill.cust.custSAC}}</div>
				</div>
            
            
       		<div class="dRow1">
					<div class="mdCel04 bodbl">
						<div class="dCel04 bodbl" style="border: 0px; text-align: left;">&nbsp;&nbsp;To,
							M/s</div>

						<div class="dCel04 bodbl"
							style="border: 0px; margin-top: -15px; margin-left: 100px; text-align: justify;">
							{{bill.cust.custName}} <br>{{bill.cust.custAdd}},<br>{{bill.cust.custCity}}
							{{bill.cust.custPin}}
							<br>
                          	
						</div>
						 <p style="text-align: left;margin-left: 5px;">GSTIN - {{bill.bill.blGstNo}} 
						 
<!-- 						  <span style="margin-left: 100px;">  PAN NO. -&nbsp;&nbsp;{{bill.cust.custPanNo}} </span> -->
						 </p>

					</div>
				 	<div class="mdCel066 bodbl">
						<div class="dCel06 bodbl">
							<div style="margin-top: 2px; text-align: center;">
							<br>
							Invoice NO.:
								{{bill.bill.blBillNo}}</div>
							<br>
							<div style="height: 27px;">DATE: {{bill.bill.blBillDt | date:'dd/MM/yyyy'}}
							<br>
							</div>
						</div>
					</div> 
				</div>
            
         	<div class="dRow1">
					<div class="mydCel25 bodbl">We hereby Submit our Frieght Bill for the
						Goods Transported as Under:</div>
					<div class="mmydCel26 bodbl">OUR PAN NO: AAGCC0032K <!-- <span style="margin-left: 25px;">GST NO:</span> --></div>
					
				</div>
            
		 <div class="dRow1">

					<div class="CSSTabGeneratorss">
						<table>
					<!-- 		<tr>
								<td width="300px" colspan="6">CONSIGNMENT DETAILS</td>								
							    <td rowspan="2"> Actual Weight</td>
				  				<td rowspan="2">Charge Weight</td>
							   	<td rowspan="2">Rate</td>
							   	<td rowspan="2">Freight</td>
							   	<td rowspan="2">Detention(B)</td>
							   	<td rowspan="2">EXPRESS DL(C)</td>
							   	<td rowspan="2">Ldng/Unldng(D)</td>
							   	<td rowspan="2">Two Point Col(E)</td>
							   	<td rowspan="2">Total(A+B+C+D+E)</td>								
							</tr>
							<tr>
							<td><strong class="bbl">Entry Sh. No</strong></td>
								<td><strong class="bbl">Date</strong></td>
								
								<td><strong class="bbl">LR No.</strong></td>
								
								<td><strong class="bbl">From Station</strong></td>
								<td><strong class="bbl">To Station</strong></td>
								<td><strong class="bbl">Lorry No.</strong></td>								   
							</tr> -->
								 <tr>
				   <td  width="450px"colspan="5">CONSIGNMENT DETAILS</td>
				    <td style="font-size: 10px;" rowspan="2">Shipment No.</td>
				   <td style="font-size: 10px;" rowspan="2">Charge<br> Weight</td>
				   <td style="font-size: 10px;" rowspan="2">Rate</td>
				   <td style="font-size: 10px;" rowspan="2">Frieght(A)</td>
				   <td style="font-size: 10px;" rowspan="2">Bonus(C)</td>
				   <td style="font-size: 10px;" rowspan="2">Ldng/Unldng(D)</td>
				   <td style="font-size: 10px;" rowspan="2">Total(A+B+C+D)</td>
				 </tr>
				 <tr>
				   <td style="font-size: 10px;"><strong>Entry Sh. No</strong></td>
				   <td style="font-size: 10px;"><strong>Date</strong></td>
				   <td style="font-size: 10px;"><strong>LR No.</strong></td>
				   <td style="font-size: 10px;"><strong>To Station</strong></td>
				    <td style="font-size: 10px;"><strong>Lorry No.</strong></td>
				  
				   
				 </tr>
							<tr ng-repeat="bilDet in bill.blDetList">
							
							<td style="font-size: 10px;">{{bilDet.billDet.bdEntryShNum}}</td>
				   <td style="font-size: 10px;">{{bilDet.cnmt.cnmtDt | date:'dd/MM/yyyy'}}</td>
				   <td style="font-size: 10px;">{{bilDet.cnmt.cnmtCode}}</td>
				    <td style="font-size: 10px;">{{bilDet.toStn}}</td>
				    <td style="font-size: 10px;">{{bilDet.truckNo}}</td>
				    <td style="font-size: 10px;">{{bilDet.billDet.bdShipmentNum}}</td>
				   <td style="font-size: 10px;">
				   	 <div ng-show="bilDet.billDet.bdBlBase == 'chargeWt'">{{bilDet.billDet.bdChgWt/1000 | number : 2}}</div>
		                <div ng-show="bilDet.billDet.bdBlBase == 'actualWt'">{{bilDet.billDet.bdActWt/1000 | number : 2}}</div>
		                <div ng-show="bilDet.billDet.bdBlBase == 'receiveWt'">{{bilDet.billDet.bdRecWt/1000 | number : 2}}</div>
				   </td>
				   <td style="font-size: 10px;">{{bilDet.billDet.bdRate | number : 3}}</td>
				   <td style="font-size: 10px;">{{bilDet.billDet.bdFreight | number : 3}}</td>
				   <td style="font-size: 10px;">{{bilDet.billDet.bdBonusAmt | number : 3}}</td>
				   <td style="font-size: 10px;">{{bilDet.billDet.bdLoadAmt | number : 3}}/{{bilDet.billDet.bdUnloadAmt | number : 2}}</td>
				   
				   <td style="font-size: 10px;">{{bilDet.billDet.bdTotAmt | number : 3}}</td>
							
							  
							</tr>

						</table>

					</div>
				</div> 
       
        	<div class="dRow1">
					<div class="mydCel20 bod2bl pdingbl">
						 Amount in words: &nbsp;&nbsp;&nbsp;&nbsp; <span class="bbl">{{bill.bilAmt}}</span>
						 <hr>
						 	<br>
						 Only By A/c Payee Cheque / DD in Favour Of <span class="bbl">Care Go Logistics Pvt. Ltd.,</span><span> or Electronic Fund transfer to below Bank A/c </span>
		                 <br>
						
					</div></div>
					
					<div class="dRow1">
					
					<!-- <div class="dCel20 bod1bl pding2bl">Pay {{bill.bilAmt}}</div> -->
					<div class="mydCel14 bod1bl pding2bl" style="text-align: right;"><br>Total<br></div>
					<div class="mydCel14 bod1bl pding2bl"><br>{{bill.bill.blFinalTot}}<br></div>
				</div>
				
       
        
<!--          <div class="rdivRow1">
         <div class="dCel31 bod1bl"> Bank Details</div>
         <div class="dCel31 bod1bl">
         <div style="position: absolute; width: 155px; border: 1px solid black;">Bank Name</div><div style="position: absolute;width: 135px; border: 1px solid black;margin-left: 155px;">A/c NO.</div>
         
         </div>
       
                     <div class="rdivCel18 rbold1bl" style="position: absolute; margin-top: 100px;width: 155px;">Bank Name</div>
                     <div class="rdivCel00 rbold1bl" style="position: absolute; margin-top: 100px;margin-left: 155px; width: 135px;">A/c No.</div>
                     <div class="rdivCel18 rbold1bl" style="position: absolute; margin-top: 100px;margin-left: 280px; width: 135px;">IFSC CODE</div>
                     <div class="rdivCel05 rbold1bl" style="position: absolute; margin-top: 100px;margin-left: 420px;width: 192px;">Branch</div>
                     <div class="rdivCel19 rbold2bl">Taxable Service tax= 30%</div>
                     <div class="rdivCel14 rbold2bl" style="text-align:right; padding-right:5px;">{{bill.bill.blTaxableSerTax}}</div>
                     <div class="rdivCel14 rbold2bl">&nbsp</div>
                     <div class="rdivCel14 rbold2bl">&nbsp</div>
                     <div class="rdivCel14 rbold2bl">&nbsp</div>
             </div> -->
             
         <!--     <div class="rdivRow1">
                     <div class="rdivCel18 rbold1bl rpadding1bl">Kotak Mahindra Bank</div>
                     <div class="rdivCel00 rbold1bl rpadding1bl">3011623336</div>
                     <div class="rdivCel18 rbold1bl rpadding1bl">KKBK0000299</div>
                     <div class="rdivCel05 rbold1bl rpaddingbl">61, Old Judicial Complex,<br> Sector-15, Gurgaon 122001</div>
                   
             </div> -->
             		<div class="dRow1">
					<div class="dCel28 bod2bl" style="text-align: center;">
						<div class="dCel31 bod1bl"> Bank Details</div>
						<div class="mydCel27 bod1bl">Bank Name</div>
						<div class="hedCel27 bod1bl">A/c No.</div>
						<div class="hedCel27 bod1bl">IFSC CODE</div>
						<div class="mydCel29 bod1bl">Branch</div>
						<div class="mydCel27 bod1bl pding1bl"><br><br><br><br><br> KOTAK MAHINDRA BANK <br><br><br><br><br><br></div>
						<div class="hedCel27 bod1bl pding1bl"><br><br><br><br><br> 3011623336 <br><br><br><br><br><br></div>
						<div class="hedCel27 bod1bl pding1bl"><br><br><br><br><br> KKBK0000299 <br><br><br><br><br><br></div>
						<div class="mydCel29 bod1bl pdingbl"><br><br><br><br><br><br>122001 <br><br><br><br><br><br></div>
						<br><br><br><br><br><br><br><br><br><br><br>
						<!-- <div class="mydCel27 bod1bl pding1bl">HDFC BANK</div>
						<div class="hedCel27 bod1bl pding1bl">13810330000037</div>
						<div class="hedCel27 bod1bl pding1bl">HDFC0001381</div>
						<div class="mydCel29 bod1bl pdingbl"><br> BHAWAN, GURGAON<br></div>
						<div class="mydCel27 bod1bl pding1bl">ICICI BANK</div>
						<div class="hedCel27 bod1bl pding1bl">103105001831</div>
						<div class="hedCel27 bod1bl pding1bl">ICIC0001031</div>
						<div class="mydCel29 bod1bl pdingbl"><br> GURGAON<br></div>
						<div class="mydCel27 bod1bl pding1bl">STATE BANK OF INDIA</div>
						<div class="hedCel27 bod1bl pding1bl">35334334071</div>
						<div class="hedCel27 bod1bl pding1bl">SBIN0060414</div>
						<div class="mydCel29 bod1bl pdingbl">
							SCO - 5,6,7 SECTOR - 15, <br>PART - 2, GURGAON
						</div> -->
						<div class="mydCel2911 bod1bl pdingbl">
							As per notification No. 5/2017. Central Tax dated 19.6.2017, GTA are exempted under GST and it is payable by consignor/consignee<br> under RCM @ 5%
						</div>
						<div class="mydCel2911 bod1bl pdingbl">
							GST Amount in Words: {{bill.gstAmt}}
						</div>
					</div>
					
								<div class="dCel30" style="height: 304px;">
						<div class="dCel32 bod1bl">
						<p style="text-align: center; margin-top: 10px; margin-left: -10px;"> GST CALCUTION:</p>
						<p style="text-align: left; margin-top: 11px; margin-left: 10px;"> CGST.AMOUNT @ 2.50%:<span style="text-align: right; margin-top: -18px; margin-left: 180px;">{{bill.cGST}}</span><br><br>
						SGST.AMOUNT @ 2.50%:<span style="text-align: right; margin-top: -18px; margin-left: 180px;">{{bill.sGST}}</span><br><br>
						IGST.AMOUNT @ 5%:<span style="text-align: right; margin-top: -18px; margin-left: 180px;">{{bill.iGST}}</span>
						<hr>
					   
						<p style="text-align: left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TOTAL TAX =<span style="text-align: right; margin-top: -18px; margin-left: 255px;">{{bill.cGST+bill.sGST+bill.iGST}}</span></p>
						<hr>
						<p style="font-weight: bolder;"><b>For Care Go Logistics Pvt. Ltd.</b></p>
		
							</div>
				     <div class="dCel32 bod1bl"
							style="text-align: left; margin-top: 100px; margin-left: 10px;">Cheked
							By</div>
						<div class="dCel32 bod1bl"
							style="text-align: right; margin-top: -18px; margin-left: -10px;">Cheked
							By</div>
					</div>
					</div>
       
    
				 
				 </div>				 
		<!-- ------------------------start raigardh customer jindal Steel-------- -->	

		<div ng-show="bill.jindalSteelBillFormat">
			    <div class="sheadRow1">
                <div class="sdivCel1"><img src="resources/img/logo.png" width="357" height="106" /></div>
                <div  class="sdivCel1">
                          <div style="font-size: 16px; font-weight: bold; font-family: Arial;">Care Go Logistics Pvt. Ltd.</div>
                          <div style="font-size: 12px">Billing Office: S-405, Greater Kailash, Part-II,New Delhi-110048</div>
<!--                           <div style="font-size: 12px">Phone:{{ selBrh.branchPhone }}</div>  -->
                         <div style="font-size: 12px">MSME Regd. No.: HR05E0000104</div>
                          <div style="font-size: 12px">Email: gurgaon@carego.co</div>
			 </div>
             </div>
            <div class="sdivRow1">
                  <div class="sdivCel01 sboldbl">
                  	<div ng-show="bill.bill.blType == 'N'">FRIEGHT BILLS</div>
                  	<div ng-show="bill.bill.blType == 'S'">SUPPLEMENTARY BILLS</div>
                  </div>
                <div class="sdivCel02 sboldbl">Co CIN No. U60231DL2015PTC279265</div>
                <div class="sdivCel03 sboldbl">Service Tax Registration No.: AAGCC0032KSD001</div>
            </div>
            
            
            <div class="sdivRow1">
                  <div class="sdivCel04 sboldbl">
                  <div class="sdivCel04 sboldbl" style="border:0px; text-align:left;">To, M/s </div>
                
                  <div class="sdivCel04 sboldbl" style="border:0px; margin-top:-15px; margin-left:100px; text-align:justify;">{{bill.cust.custName}}
                  <br>{{bill.cust.custAdd}},<br>{{bill.cust.custCity}} {{bill.cust.custPin}}</div>
                  
                  </div>
               <div class="sdivCel066 sboldbl">
                   <div class="sdivCel06 sboldbl">
                      <div style="margin-top:2px; text-align:center;">BILL NO.: {{bill.bill.blBillNo}}</div><br><div>DATE: {{bill.bill.blBillDt | date:'dd/MM/yyyy'}}</div>
                   </div>
               </div>
            </div>
            
            <div class="sdivRow1">
                    <div class="sdivCel25 sboldbl">We hereby Submit our Bill for the Goods Transported as Under:</div>
	                <div class="sdivCel26 sboldbl">OUR PAN NO: AAGCC0032K</div>
	                <div class="sdivCel26 sboldbl">No TDS To Be Deducted w.e.f 01/10/2009</div>
            </div>
            
            
            <div class="sdivRow1">
               
			<div class="sCSSTableGeneratorss">
				<table>
				 <tr>
				   <td class="slatterh slatterbold" width="450px"colspan="4">CONSIGNMENT DETAILS</td>
				   <td class="slatterh slatterbold" rowspan="2">From Station</td>
				   <td class="slatterh slatterbold" rowspan="2">To Station</td>
				   <td class="slatterh slatterbold" rowspan="2">Material</td>
				   <td class="slatterh slatterbold" rowspan="2">Vehicle No.</td>
				   <td class="slatterh slatterbold" rowspan="2">Delivery Date</td>
				   <td class="slatterh slatterbold" rowspan="2">Net Wt in M.T</td>
				   <td class="slatterh slatterbold" rowspan="2">Charge Wt in M.T</td>
				   <td class="slatterh slatterbold"  rowspan="2">Rate PMT</td>
				   <td class="slatterh slatterbold" rowspan="2">Total Freight</td>
				 </tr>
				 <tr>
				
				   <td class="slatterh slatterbold">LR No.</td>
				   <td class="slatterh slatterbold">DATE</td>
				   <td class="slatterh slatterbold">INVOICE NO.</td>
				   <td class="slatterh slatterbold">OBD NO.</td>
				 </tr>
				 <tr ng-repeat="bilDet in bill.blDetList">
				   
				   <td class="slatterh">{{bilDet.cnmt.cnmtCode}}</td>
				   <td class="slatterh">{{bilDet.cnmt.cnmtDt | date:'dd/MM/yyyy'}}</td>
				   <td class="slatterh">{{bilDet.cnmt.cnmtInvoiceNo[0].invoiceNo}}</td>
				   <td class="slatterh">{{bilDet.billDet.bdObdNum}}</td>
				   <td class="slatterh">{{bilDet.frmStn}}</td>
				   <td class="slatterh">{{bilDet.toStn}}</td>
				   <td class="slatterh">{{bilDet.cnmt.cnmtProductType}}</td>
				   <td class="slatterh">{{bilDet.truckNo}}</td>
				   <td class="slatterh">{{bilDet.delDt | date:'dd/MM/yyyy'}}</td>
				 <!--<td class="slatterh">
				   		<div ng-show="bilDet.billDet.bdBlBase == 'chargeWt'">{{bilDet.billDet.bdChgWt/1000 | number : 3}}</div>
		                <div ng-show="bilDet.billDet.bdBlBase == 'actualWt'">{{bilDet.billDet.bdActWt/1000 | number : 3}}</div>
		                <div ng-show="bilDet.billDet.bdBlBase == 'receiveWt'">{{bilDet.billDet.bdRecWt/1000 | number : 3}}</div>
				   </td>-->
				   <td class="slatterh">{{bilDet.billDet.bdActWt/1000 | number : 3}}</td>
				   <!--<td class="slatterh">{{bilDet.billDet.bdRate * 1000 | number : 2}}</td>-->
				   <td class="slatterh">{{bilDet.billDet.bdChgWt/1000 | number : 3}}</td>
				   <td class="slatterh">{{bilDet.billDet.bdRate | number : 5}}</td>
				   <!--<td class="slatterh">{{bilDet.billDet.bdFreight | number : 2}}</td>-->
				   <!--<td class="slatterh">{{bilDet.billDet.bdDetAmt | number : 2}}</td>-->
				   <!-- <td class="slatterh">{{bilDet.billDet.bdBonusAmt | number : 2}}</td>-->
				  <!--<td class="slatterh">{{bilDet.billDet.bdLoadAmt | number : 2}}/{{bilDet.billDet.bdUnloadAmt | number : 2}}</td>-->
				   <td class="slatterh">{{bilDet.billDet.bdTotAmt | number : 2}}</td>
				 </tr>
				
				</table>
				              
			</div>
      </div>
       
       
       
         <div class="sdivRow1">
                  <div class="sdivCel16">
                     <div class="sdivCel17 sbold2bl">IT IS HEREBY DECLARE THAT WE ARE NOT AVAILING CENVAT CREDIT FACILITY IN RESPECT OF INPUTS/SERVICES /CAPITAL GOODS</div>
                     <div class="sdivCel17 sbold2bl">WE HAVE NOT AVAILED ANY BENEFIT UNDER THE NOTIFICATION NO. 12/2003-SERVICE TAX DATED 20/06/2008</div>
                   </div>
               
               
             
                
                 <div class="sdivCel15 sbold2bl" style="text-align:center;">
                     <div class="sdivCel14 sbold1bl spaddingbl" >SUB TOTAL</div>
                     <div class="sdivCel14 sbold1bl spaddingbl" >{{bill.bill.blSubTot}}</div> 
                  </div>
               
            </div>
       
        
         <div class="sdivRow1">
                     <div class="sdivCel18 sbold1bl">Bank Name</div>
                     <div class="sdivCel00 sbold1bl">A/c No.</div>
                     <div class="sdivCel18 sbold1bl">IFSC CODE</div>
                     <div class="sdivCel05 sbold1bl">Branch</div>
                     <div class="sdivCel19 sbold2bl">Taxable Service tax= 30%</div>
                     <div class="sdivCel14 sbold2bl" style="text-align:right; padding-right:5px;">{{bill.bill.blTaxableSerTax}}</div>
                     <div class="sdivCel14 sbold2bl">&nbsp</div>
             </div>
             
             <div class="sdivRow1">
                     <div class="sdivCel18 sbold1bl spadding1bl">Kotak Mahindra Bank</div>
                     <div class="sdivCel00 sbold1bl spadding1bl">3011623336</div>
                     <div class="sdivCel18 sbold1bl spadding1bl">KKBK0000299</div>
                     <div class="sdivCel05 sbold1bl spaddingbl">61, Old Judicial Complex,<br> Sector-15, Gurgaon 122001</div>
                     <div class="sdivCel19 sbold2bl spdng">Service Tax= 14%<br> Swachh Bharat Cess= 0.5%<br>Kisan kalyan Cess=0.5%</div>
                     <div class="sdivCel14 sbold2bl spdng" style="text-align:right;"><span>{{bill.bill.blSerTax}}</span><br><span>{{bill.bill.blSwachBhCess}}</span><br><span>{{bill.bill.blKisanKalCess}}</span></div>
                     <div class="sdivCel14 sbold2bl spadding1bl">&nbsp</div>
             </div>
       
         <div class="sdivRow1">
                     <div class="sdivCel20 sbold2bl spaddingbl">
                        By Cheque / DD in Favour Of<span class="bold1">Care Go Logistics Pvt. Ltd.</span>, or Electronic Fund transfer to <span class="bold1">above Bank A/C</span>
                        <br>You are hereby requested to mail us Payment Detail on mail Id <span class="bold1">{{ selBrh.branchEmailId }}</span>
                     </div>
                     <div class="sdivCel20 sbold1bl spadding00bl">Amount in Words: {{bill.bilAmt}}</div>
                     <div class="sdivCel14 sbold1bl spadding2bl" style="text-align:right;">Total</div>
                     <div class="sdivCel14 sbold1bl spadding2bl">{{bill.bill.blFinalTot}}</div>
             </div>
           
             <div class="sdivRow1">
                 <div class="sdivCel30" style="height:100px;">
                   <div class="sdivCel32 sbold1bl" style="text-align:right; margin-top:10px;">For Care Go Logistic Pvt. Ltd</div>
                    <div class="sdivCel32 sbold1bl" style="text-align:right; margin-top:40px;">Authorised Signatory</div>
                 </div> 
             </div>
             
            <div class="sdivRow1">
                     <div class="sdivCel21 sbold1bl spadding3bl"><span>Corporate Office: SCO-26, 1st Floor, Sec. 15, Part-II, Gurgaon-122001,</span> &nbsp  <span>Ph.No.: 0124-4205305,</span> &nbsp <span>Email ID: corporate@tcgppl.com</span></div>
             </div> 
		
		
		</div>
		
		<!-- ------------------------ start  raigardh customer MSP STEEL & POWER -------- -->
				 <div ng-show="bill.mspSteelBillFormat">
				 <div class="mheadRow1">
                <div class="mdivCel1"><img src="resources/img/logo.png" width="357" height="106" /></div>
                <div  class="mdivCel1">
                          <div style="font-size: 16px; font-weight: bold; font-family: Arial;">Care Go Logistics Pvt. Ltd.</div>
                          <div style="font-size: 12px">Billing Office: S-405, Greater Kailash, Part-II,New Delhi-110048</div>
<!--                           <div style="font-size: 12px">Phone:{{ selBrh.branchPhone }}</div>  -->
                         <div style="font-size: 12px">MSME Regd. No.: HR05E0000104</div>
                          <div style="font-size: 12px">Email: gurgaon@carego.co</div>
			 </div>
             </div>
            <div class="mdivRow1">
                  <div class="mdivCel01 mboldbl">
                  	<div ng-show="bill.bill.blType == 'N'">FRIEGHT BILLS</div>
                  	<div ng-show="bill.bill.blType == 'S'">SUPPLEMENTARY BILLS</div>
                  </div>
                <div class="mdivCel02 mboldbl">Co CIN No. U60231DL2015PTC279265</div>
                <div class="mdivCel03 mboldbl">Service Tax Registration No.: AAGCC0032KSD001</div>
            </div>
            
            
            <div class="mdivRow1">
                  <div class="mdivCel04 mboldbl">
                  <div class="mdivCel04 mboldbl" style="border:0px; text-align:left;">To, M/s </div>
                
                  <div class="mdivCel04 mboldbl" style="border:0px; margin-top:-15px; margin-left:100px; text-align:justify;">{{bill.cust.custName}}
                  <br>{{bill.cust.custAdd}},<br>{{bill.cust.custCity}} {{bill.cust.custPin}}</div>
                  
                  </div>
               <div class="mdivCel066 mboldbl">
                   <div class="mdivCel06 mboldbl">
                      <div style="margin-top:2px; text-align:center;">BILL NO.: {{bill.bill.blBillNo}}</div><br><div>DATE: {{bill.bill.blBillDt | date:'dd/MM/yyyy'}}</div>
                   </div>
               </div>
            </div>
            
            <div class="mdivRow1">
                    <div class="mdivCel25 mboldbl">We hereby Submit our Bill for the Goods Transported as Under:</div>
	                <div class="mdivCel26 mboldbl">OUR PAN NO: AAGCC0032K</div>
	                <div class="mdivCel26 mboldbl">No TDS To Be Deducted w.e.f 01/10/2009</div>
            </div>
            
            
            <div class="mdivRow1">
               
			<div class="mCSSTableGeneratorss">
				<table>
				 <tr>
				   <td class="mlatterh mlatterbold" width="450px"colspan="5">CONSIGNMENT DETAILS</td>
				   <td class="mlatterh mlatterbold" rowspan="2">To Station</td>
				   <td class="mlatterh mlatterbold" rowspan="2">Weight MT</td>
				   <td class="mlatterh mlatterbold" rowspan="2">Rate</td>
				   <td class="mlatterh mlatterbold" rowspan="2">Freight (A)</td>
				   <td class="mlatterh mlatterbold" rowspan="2">Material</td>
				   <td class="mlatterh mlatterbold" rowspan="2">Lorry No.</td>
				   <td class="mlatterh mlatterbold" rowspan="2">Total</td>
				 </tr>
				 <tr>
				   <td class="mlatterh mlatterbold">Sales Order No.</td>
				   <td class="mlatterh mlatterbold">Invoice NO.</td>
				   <td class="mlatterh mlatterbold">Date</td>
				   <td class="mlatterh mlatterbold">LR NO.</td>
				   <td class="mlatterh mlatterbold">From Station</td>
				 </tr>
				 <tr ng-repeat="bilDet in bill.blDetList">
				   <td class="mlatterh">&nbsp;</td>
				   <td class="mlatterh">{{bilDet.cnmt.cnmtInvoiceNo[0].invoiceNo}}<br>{{bilDet.cnmt.cnmtInvoiceNo[1].invoiceNo}}<br>{{bilDet.cnmt.cnmtInvoiceNo[2].invoiceNo}}</td>
				   <td class="mlatterh">{{bilDet.cnmt.cnmtDt | date:'dd/MM/yyyy'}}</td>
				   <td class="mlatterh">{{bilDet.cnmt.cnmtCode}}</td>
				   <td class="mlatterh">{{bilDet.frmStn}}</td>
				   <td class="mlatterh">{{bilDet.toStn}}</td>
				   <td class="mlatterh">
				   		<div ng-show="bilDet.billDet.bdBlBase == 'chargeWt'">{{bilDet.billDet.bdChgWt/1000 | number : 2}}</div>
		                <div ng-show="bilDet.billDet.bdBlBase == 'actualWt'">{{bilDet.billDet.bdActWt/1000 | number : 2}}</div>
		                <div ng-show="bilDet.billDet.bdBlBase == 'receiveWt'">{{bilDet.billDet.bdRecWt/1000 | number : 2}}</div>
				   </td>
				   <td class="mlatterh">{{bilDet.billDet.bdRate * 1000 | number : 3}}</td>
				   <td class="mlatterh">{{bilDet.billDet.bdFreight | number : 3}}</td>
				   <td class="mlatterh">{{bilDet.cnmt.cnmtProductType}}</td>
				   <td class="mlatterh">{{bilDet.truckNo}}</td>
				   <td class="mlatterh">{{bilDet.billDet.bdTotAmt | number : 3}}</td>
				 </tr>
				
				</table>
				              
			</div>
      </div>
       
       
       
         <div class="mdivRow1">
                  <div class="mdivCel16">
                     <div class="mdivCel17 mbold2bl">IT IS HEREBY DECLARE THAT WE ARE NOT AVAILING CENVAT CREDIT FACILITY IN RESPECT OF INPUTS/SERVICES /CAPITAL GOODS</div>
                     <div class="mdivCel17 mbold2bl">WE HAVE NOT AVAILED ANY BENEFIT UNDER THE NOTIFICATION NO. 12/2003-SERVICE TAX DATED 20/06/2008</div>
                   </div>
               
               
             
                
                 <div class="mdivCel15 mbold2bl" style="text-align:center;">
                     <div class="mdivCel14 mbold1bl mpaddingbl" >SUB TOTAL</div>
                     <div class="mdivCel14 mbold1bl mpaddingbl" >{{bill.bill.blSubTot}}</div> 
                  </div>
               
            </div>
       
        
         <div class="mdivRow1">
                     <div class="mdivCel18 mbold1bl">Bank Name</div>
                     <div class="mdivCel00 mbold1bl">A/c No.</div>
                     <div class="mdivCel18 mbold1bl">IFSC CODE</div>
                     <div class="mdivCel05 mbold1bl">Branch</div>
                     <div class="mdivCel19 mbold2bl">Taxable Service tax= 30%</div>
                     <div class="mdivCel14 mbold2bl" style="text-align:right; padding-right:5px;">{{bill.bill.blTaxableSerTax}}</div>
                     <div class="mdivCel14 mbold2bl">&nbsp</div>
             </div>
             
             <div class="mdivRow1">
                     <div class="mdivCel18 mbold1bl mpadding1bl">Kotak Mahindra Bank</div>
                     <div class="mdivCel00 mbold1bl mpadding1bl">3011623336</div>
                     <div class="mdivCel18 mbold1bl mpadding1bl">KKBK0000299</div>
                     <div class="mdivCel05 mbold1bl mpaddingbl">61, Old Judicial Complex,<br> Sector-15, Gurgaon 122001</div>
                     <div class="mdivCel19 mbold2bl mpdng">Service Tax= 14%<br> Swachh Bharat Cess= 0.5%<br>Kisan kalyan Cess=0.5%</div>
                     <div class="mdivCel14 mbold2bl mpdng" style="text-align:right;"><span>{{bill.bill.blSerTax}}</span><br><span>{{bill.bill.blSwachBhCess}}</span><br><span>{{bill.bill.blKisanKalCess}}</span></div>
                     <div class="mdivCel14 mbold2bl mpadding1bl">&nbsp</div>
             </div>
       
         <div class="mdivRow1">
                     <div class="mdivCel20 mbold2bl mpaddingbl">
                        By Cheque / DD in Favour Of<span class="bold1">Care Go Logistics Pvt. Ltd.</span>, or Electronic Fund transfer to <span class="bold1">above Bank A/C</span>
                        <br>You are hereby requested to mail us Payment Detail on mail Id <span class="bold1">{{ selBrh.branchEmailId }}</span>
                     </div>
                     <div class="mdivCel20 mbold1bl mpadding00bl">Amount in Words: {{bill.bilAmt}}</div>
                     <div class="mdivCel14 mbold1bl mpadding2bl" style="text-align:right;">Total</div>
                     <div class="mdivCel14 mbold1bl mpadding2bl">{{bill.bill.blFinalTot}}</div>
             </div>
           
             <div class="mdivRow1">
                 <div class="mdivCel30" style="height:100px;">
                   <div class="mdivCel32 mbold1bl" style="text-align:right; margin-top:10px;">For Care Go Logistic Pvt. Ltd</div>
                    <div class="mdivCel32 mbold1bl" style="text-align:right; margin-top:50px; margin-left:-250px;">Cheque</div>
                    <div class="mdivCel32 mbold1bl" style="text-align:right; margin-top:-14px;">Authorised Signatory</div>
                 </div> 
             </div>
             
            <div class="mdivRow1">
                     <div class="mdivCel21 mbold1bl mpadding3bl"><span>Corporate Office: SCO-26, 1st Floor, Sec. 15, Part-II, Gurgaon-122001,</span> &nbsp  <span>Ph.No.: 0124-4205305,</span> &nbsp <span>Email ID: corporate@tcgppl.com</span></div>
             </div> 

			

		</div>
		
		
		<!-- ------------------------ start raigardh customer SKS POWER GENERATION (C.G) LTD -------- -->
		
		 	 <div ng-show="bill.sksPowerGenBillFormat">
		    <div class="mrhRow1">
                <div class="mrCel1"><img src="resources/img/logo.png" width="357" height="106" /></div>
                <div  class="mrCel1">
                          <div style="font-size: 16px; font-weight: bold; font-family: Arial;">Care Go Logistics Pvt. Ltd.</div>
                          <div style="font-size: 12px">Billing Office: S-405, Greater Kailash, Part-II,New Delhi-110048</div>
<!--                           <div style="font-size: 12px">Phone:{{ selBrh.branchPhone }}</div>  -->
                          <div style="font-size: 12px">MSME Regd. No.: HR05E0000104</div>
                          <div style="font-size: 12px">Email: gurgaon@carego.co</div>
			 </div>
             </div>
            <div class="mrRow1">
                  <div class="mrCel01 mrboldbl">
                  	<div ng-show="bill.bill.blType == 'N'">FRIEGHT BILLS</div>
                  	<div ng-show="bill.bill.blType == 'S'">SUPPLEMENTARY BILLS</div>
                  </div>
                <div class="mrCel02 mrboldbl">Co CIN No. U60231DL2015PTC279265</div>
                <div class="mrCel03 mrboldbl">Service Tax Registration No.: AAGCC0032KSD001</div>
            </div>
            
            
            <div class="mrRow1">
                  <div class="mrCel04 mrboldbl">
                  <div class="mrCel04 mrboldbl" style="border:0px; text-align:left;">To, M/s </div>
                
                  <div class="mrCel04 mrboldbl" style="border:0px; margin-top:-15px; margin-left:100px; text-align:justify;">{{bill.cust.custName}}
                  <br>{{bill.cust.custAdd}},<br>{{bill.cust.custCity}} {{bill.cust.custPin}}</div>
                  
                  </div>
               <div class="mrCel066 mrboldbl">
                   <div class="mrCel06 mrboldbl">
                      <div style="margin-top:2px; text-align:center;">BILL NO.: {{bill.bill.blBillNo}}</div><br><div>DATE: {{bill.bill.blBillDt | date:'dd/MM/yyyy'}}</div>
                   </div>
               </div>
            </div>
            
            <div class="mrRow1">
                    <div class="mrCel25 mrboldbl">We hereby Submit our Bill for the Goods Transported as Under:</div>
	                <div class="mrCel26 mrboldbl">OUR PAN NO: AAGCC0032K</div>
	                <div class="mrCel26 mrboldbl">No TDS To Be Deducted w.e.f 01/10/2009</div>
            </div>
            
            
            <div class="mrRow1">
               
			<div class="CTableGeneratorss">
				<table>
				 <tr>
				   <td class="mtterh mtterbold" width="450px"colspan="5">CONSIGNMENT DETAILS</td>
				   <td class="mtterh mtterbold" rowspan="2">Weight MT</td>
				   <td class="mtterh mtterbold" rowspan="2">Rate</td>
				   <td class="mtterh mtterbold" rowspan="2">Freight (A)</td>
				   <td class="mtterh mtterbold" rowspan="2">Material</td>
				   <td class="mtterh mtterbold" rowspan="2">Lorry No.</td>
				   <td class="mtterh mtterbold" rowspan="2">Total</td>
				 </tr>
				 <tr>
				    <!-- <td class="mtterh mltterbold">Sr. No.</td>-->
				   <td class="mtterh mltterbold">Invoice NO.</td>
				   <td class="mtterh mtterbold">Date</td>
				   <td class="mtterh mtterbold">LR NO.</td>
				   <td class="mtterh mtterbold">From Station</td>
				   <td class="mtterh mtterbold">To Station</td>
				 </tr>
				 <tr ng-repeat="bilDet in bill.blDetList">
				  <!-- <td class="mtterh">1</td>-->
				   <td class="mtterh">{{bilDet.cnmt.cnmtInvoiceNo[0].invoiceNo}}<br>{{bilDet.cnmt.cnmtInvoiceNo[1].invoiceNo}}<br>{{bilDet.cnmt.cnmtInvoiceNo[2].invoiceNo}}</td>
				   <td class="mtterh">{{bilDet.cnmt.cnmtDt | date:'dd/MM/yyyy'}}</td>
				   <td class="mtterh">{{bilDet.cnmt.cnmtCode}}</td>
				   <td class="mtterh">{{bilDet.frmStn}}</td>
				   <td class="mtterh">{{bilDet.toStn}}</td>
				   <td class="mtterh">
				   		<div ng-show="bilDet.billDet.bdBlBase == 'chargeWt'">{{bilDet.billDet.bdChgWt/1000 | number : 3}}</div>
		                <div ng-show="bilDet.billDet.bdBlBase == 'actualWt'">{{bilDet.billDet.bdActWt/1000 | number : 3}}</div>
		                <div ng-show="bilDet.billDet.bdBlBase == 'receiveWt'">{{bilDet.billDet.bdRecWt/1000 | number : 3}}</div>
				   </td>
				   <td class="mtterh">{{bilDet.billDet.bdRate * 1000 | number : 3}}</td>
				   <td class="mtterh">{{bilDet.billDet.bdFreight | number : 3}}</td>
				   <td class="mtterh">{{bilDet.cnmt.cnmtProductType}}</td>
				   <td class="mtterh">{{bilDet.truckNo}}</td>
				   <td class="mtterh">{{bilDet.billDet.bdTotAmt | number : 3}}</td>
				 </tr>
				
				</table>
				              
			</div>
      </div>
       
       
       
         <div class="mrRow1">
                  <div class="mrCel16">
                     <div class="mrCel17 mrbold2bl">IT IS HEREBY DECLARE THAT WE ARE NOT AVAILING CENVAT CREDIT FACILITY IN RESPECT OF INPUTS/SERVICES /CAPITAL GOODS</div>
                     <div class="mrCel17 mrbold2bl">WE HAVE NOT AVAILED ANY BENEFIT UNDER THE NOTIFICATION NO. 12/2003-SERVICE TAX DATED 20/06/2008</div>
                   </div>
               
               
             
                
                 <div class="mrCel15 mrbold2bl" style="text-align:center;">
                     <div class="mrCel14 mrbold1bl mrpaddingbl" >SUB TOTAL</div>
                     <div class="mrCel14 mrbold1bl mrpaddingbl" >{{bill.bill.blSubTot}}</div> 
                  </div>
               
            </div>
       
        
         <div class="mrRow1">
                     <div class="mrCel18 mrbold1bl">Bank Name</div>
                     <div class="mrCel00 mrbold1bl">A/c No.</div>
                     <div class="mrCel18 mrbold1bl">IFSC CODE</div>
                     <div class="mrCel05 mrbold1bl">Branch</div>
                     <div class="mrCel19 mrbold2bl">Taxable Service tax= 30%</div>
                     <div class="mrCel14 mrbold2bl" style="text-align:right; padding-right:5px;">{{bill.bill.blTaxableSerTax}}</div>
                     <div class="mrCel14 mrbold2bl">&nbsp</div>
             </div>
             
             <div class="mrRow1">
                     <div class="mrCel18 mrbold1bl mrpadding1bl">Kotak Mahindra Bank</div>
                     <div class="mrCel00 mrbold1bl mrpadding1bl">3011623336</div>
                     <div class="mrCel18 mrbold1bl mrpadding1bl">KKBK0000299</div>
                     <div class="mrCel05 mrbold1bl mrpaddingbl">61, Old Judicial Complex,<br> Sector-15, Gurgaon 122001</div>
                     <div class="mrCel19 mrbold2bl mrpdng">Service Tax= 14%<br> Swachh Bharat Cess= 0.5%<br>Kisan kalyan Cess=0.5%</div>
                     <div class="mrCel14 mrbold2bl mrpdng" style="text-align:right;"><span>{{bill.bill.blSerTax}}</span><br><span>{{bill.bill.blSwachBhCess}}</span><br><span>{{bill.bill.blKisanKalCess}}</span></div>
                     <div class="mrCel14 mrbold2bl mrpadding1bl">&nbsp</div>
             </div>
       
         <div class="mrRow1">
                     <div class="mrCel20 mrbold2bl mrpaddingbl">
                        By Cheque / DD in Favour Of<span class="bold1">Care Go Logistics Pvt. Ltd.</span>, or Electronic Fund transfer to <span class="bold1">above Bank A/C</span>
                        <br>You are hereby requested to mail us Payment Detail on mail Id <span class="bold1">{{ selBrh.branchEmailId }}</span>
                     </div>
                     <div class="mrCel20 mrbold1bl mrpadding00bl">Amount in Words: {{bill.bilAmt}}</div>
                     <div class="mrCel14 mrbold1bl mrpadding2bl" style="text-align:right;">Total</div>
                     <div class="mrCel14 mrbold1bl mrpadding2bl">{{bill.bill.blFinalTot}}</div>
             </div>
           
             <div class="mrRow1">
                 <div class="mrCel30" style="height:100px;">
                   <div class="mrCel32 mrbold1bl" style="text-align:right; margin-top:10px;">For Care Go Logistic Pvt. Ltd</div>
                    <div class="mrCel32 mrbold1bl" style="text-align:right; margin-top:50px; margin-left:-250px;">Checked By</div>
                    <div class="mrCel32 mrbold1bl" style="text-align:right; margin-top:-14px;">Bill Incharge</div>
                 </div> 
             </div>
             
            <div class="mrRow1">
                     <div class="mrCel21 mrbold1bl mrpadding3bl"><span>Corporate Office: SCO-26, 1st Floor, Sec. 15, Part-II, Gurgaon-122001,</span> &nbsp  <span>Ph.No.: 0124-4205305,</span> &nbsp <span>Email ID: corporate@tcgppl.com</span></div>
             </div> 

			

		</div>
		
		
		<!-- ------------------------ start Singrauli customer HINDALCO INDUSTRIES LTD.(MAHAN) -------- -->
<!-- 	format changed -->
			 <div ng-show="bill.hidalcoMahanBillFormat">

			               <div class="reqmhRow1">
                <div class="reqmCel1"><img src="resources/img/logo.png" width="357" height="106" /></div>
                <div  class="reqmCel1">
                          <div style="font-size: 16px; font-weight: bold; font-family: Arial;">Care Go Logistics Pvt. Ltd.</div>
                          <div style="font-size: 11px">Billing Office: S-405, Greater Kailash, Part-II,New Delhi-110048</div>
                          <div style="font-size: 12px">MSME Regd. No.: HR05E0000104</div>
                          <div style="font-size: 11px">Email: gurgaon@carego.co
                          <br>
                          Web Site: Care.go People, CIN: U60231DL2015PTC279265
                          <br>
                          <strong style="font-weight: bold; margin-left: 95px;">PAN No.:AAGCC0032K</strong>
                          </div> 
                         
			 </div>
             </div>
             <div class="reqmRow1">
                    <div class="reqmCel25 reqmboldbl" style="width: 975px; text-align: center;">Tax Invoice</div>
	             
            </div>
				             
				                    <div class="reqmRow1">
                  <div class="reqmCel04 reqmboldbl" style="height: 125px;width: 975px;">
                  <div class="reqmCel04 reqmboldbl" style="border:0px; text-align:left;"><br><br>&nbsp;&nbsp;To,M/s. </div>
                
                  <div class="reqmCel04 reqmboldbl" style="border:0px; margin-top:-15px;margin-left:90px;  text-align:justify;">{{bill.cust.custName}}
                  <br>{{bill.cust.custAdd}},<br>{{bill.cust.custCity}} {{bill.cust.custPin}}<br><p style="text-align: left"> GSTIN-{{bill.bill.blGstNo}}</p></div>
                  <br>
                   <div style="border: 1px solid black;position: absolute;    top: 125px;right: 170px;width: 275px;height:125px; text-align: left;">
                   <strong>&nbsp;&nbsp;Name of Consignee</strong><br>&nbsp;&nbsp;{{bill.blDetList[0].consignee.custName}}<br>&nbsp;&nbsp;{{bill.blDetList[0].toStn}}
                   </div> 
                <div style="position: absolute;    top: 125px;right: -15px;width: 275px;height: 125px;">
                 <table style="border: 1px solid black;">
                <tr><td style="border: 1px solid black;text-align: center" colspan="3">Tax Invoice No.</td><td style="border: 1px solid black;text-align: center" colspan="2"> {{bill.bill.blBillNo}}</td></tr>
                <tr><td style="border: 1px solid black;text-align: center" colspan="3">Date</td><td style="border: 1px solid black;text-align: center" colspan="2">{{bill.bill.blBillDt | date:'dd/MM/yyyy'}} </td></tr>
<!--                 <tr><td style="border: 1px solid black;text-align: center" colspan="3">Tax Invoice No.</td><td style="border: 1px solid black;text-align: center"   colspan="2">REN/17-18/568 </td></tr> -->
               <!--  <tr><td style="border: 1px solid black;text-align: center" colspan="2">Date</td><td style="border: 1px solid black;text-align: center" colspan="2">04-01-2018</td></tr>-->
                <tr><td rowspan="2" colspan="2" style="text-align: center;border: 1px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>PAN NO.</td><td style="border: 1px solid black;text-align: center" colspan="2" rowspan="2"><br>AAGCC0032K</td></tr>
              
            
                </table>
                
                </div>
                 
     
                </div>
            </div>
            
            <div class="reqmRow1">
                    <div class="reqmCel25 reqmboldbl" style="width: 975px; text-align: center;"> &nbsp;</div>
	             
            </div>
            
            
            <div class="reqmRow1">
               
			<div class="reqmCSTableGeneratorss">
				<table>
				 <tr>
			   <td class="reqmltterh reqmltterbold" colspan="4">CONSIGNMENT DETAILS</td> 
				  <td class="reqmltterh reqmltterbold" rowspan="2" >INVOICE NO</td>
				   <td class="reqmltterh reqmltterbold" rowspan="2">TRUCK NO</td>
				   <td class="reqmltterh reqmltterbold" rowspan="2">PRODUCT</td>
				   <td class="reqmltterh reqmltterbold" rowspan="2">WEIGHT<br>(Ton)</td>
 				   <td class="reqmltterh reqmltterbold" rowspan="2">Rate<br>(Rs)</td>
				   <td class="reqmltterh reqmltterbold" rowspan="2">FRIEGHT<br>(Rs)</td>
				   <td class="reqmltterh reqmltterbold" rowspan="2">UNLOADIN<br>G</td>
				   <td class="reqmltterh reqmltterbold" rowspan="2">OTHERS</td> 
				   <td class="reqmltterh reqmltterbold" rowspan="2">TOTAL<br>(Rs.)</td>
				 </tr>
				 <tr>
				 <td class="reqmltterh reqmltterbold" >Date</td>
				   <td class="reqmltterh reqmltterbold" >LR. No.</td>
				   <td class="reqmltterh reqmltterbold" >From Station</td>
				  <!--  <td class="reqmltterh reqmltterbold" >Gate out <br> DT</td> -->
				   <td class="reqmltterh reqmltterbold" >To Station</td>

				 </tr>
			
				 <tr ng-repeat="bilDet in bill.blDetList">
				   <!-- <td class="reqmltterh">{{bilDet.cnmt.cnmtCode}}</td> -->
				    <td class="reqmltterh">{{bilDet.cnmt.cnmtDt | date:'dd/MM/yyyy'}}</td>
				   <td class="reqmltterh">{{bilDet.cnmt.cnmtCode}}</td>
				   <td class="reqmltterh reqmltterbold">{{bilDet.frmStn}}</td>
				   <td class="reqmltterh">{{bilDet.toStn}}</td>
				   <td class="reqmltterh">{{bilDet.cnmt.cnmtInvoiceNo[0].invoiceNo}}<br>{{bilDet.cnmt.cnmtInvoiceNo[1].invoiceNo}}</td>
				   <td class="reqmltterh">{{bilDet.truckNo}}</td>
				   
				   <td class="reqmltterh">{{bilDet.cnmt.cnmtProductType}}</td>
				   <td class="reqmltterh">
				   		<div ng-show="bilDet.billDet.bdBlBase == 'chargeWt'">{{bilDet.billDet.bdChgWt/1000 | number : 3}}</div>
		                <div ng-show="bilDet.billDet.bdBlBase == 'actualWt'">{{bilDet.billDet.bdActWt/1000 | number : 3}}</div>
		                <div ng-show="bilDet.billDet.bdBlBase == 'receiveWt'">{{bilDet.billDet.bdRecWt/1000 | number : 3}}</div>
				   </td>
				   <td class="reqmltterh">{{bilDet.billDet.bdRate * 1000 | number : 0}}</td>
				   <td class="reqmltterh">{{bilDet.billDet.bdFreight | number : 0}}</td>
				    <td class="reqmltterh">{{bilDet.billDet.bdUnloadAmt}}</td>
				     <td class="reqmltterh">{{bilDet.billDet.bdOthChgAmt}}</td>
				      <td class="reqmltterh">{{bilDet.billDet.bdTotAmt | number : 0}}</td>
				      
				 </tr>
				
			
				
				<tr>
				   <td class="reqmltterh reqmltterbold" colspan="9">&nbsp;</td>
				  
				   <td class="reqmltterh reqmltterbold">&nbsp;</td>
				   <td class="reqmltterh reqmltterbold">&nbsp;</td>
				   <td class="reqmltterh reqmltterbold">&nbsp;</td>
				   <td class="reqmltterh reqmltterbold">&nbsp;{{bill.bill.blFinalTot}}</td>

				</tr>
					<tr>
				   <td colspan="1" class="reqmltterh reqmltterbold" style="text-align: center;">In words-:</td>
				   <td colspan="14" class="reqmltterh reqmltterbold" style="text-align: left;">&nbsp;{{bill.bilAmt}}</td>
				  
				</tr>
				
				</table>
				              
			</div>
      </div>
       
       
       
           
             <div class="reqmRow1">
            
                 <div class="reqmCel30" style="height:160px;">
               <p style="text-align: left;font-size: 70%; font-weight: bold;">&nbsp;&nbsp;Note:"GST tax to be paid by Consignor"</p>
              <p style="text-align: left;font-size: 80%; font-weight: bold;">&nbsp;&nbsp;Service Accounting Code (SAC) : {{bill.cust.custSAC}}<br>
             &nbsp;&nbsp; Notification Code. NO. 13/2017- Central Tax(Rate) dt 28<sup>th</sup> Jun 2017<br>
             &nbsp;&nbsp; Place of Origin- MP - State Code-23<br>
             <br>
             <br>
                </p>
                <p style="text-align: left;font-size: 70%;">&nbsp;Terms And Conditions:-<br>
                &nbsp;PLEASE PAY BY A/C. PAYEE CHEQUE/D.D ONLY IN FAVOUR OF "Care Go Logistics Private Limited." OR RTGS BANK DETAILS ARE BELOW MENTIONED
                </p>
                 </div> 
             </div>
             
				    
				      <div class="reqmRow1">
                    <div class="reqmCel25 reqmboldbl" style="width: 975px;">
                    <div style="position: absolute; width: 300px; height: 90px;">
                    <table style="margin-left: 10px;">
                    
                    
                    <tr><td>Bank:<br>
                    Branch:<br>
                    Account No<br>
                    RTGS/NEFT
                    </td>
                  
                    <td style="width: 500px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-weight: bold;">Kotak Mahindra Bank</span><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Gurgaon<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3011623336<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;KKBK0000299</td>
                  <!--   <td style="border: 1px solid black; width: 200px;" ></td> -->
                    </tr>

                    </table>
                    
                    </div>
                      <div style="border: 1px solid black;position: absolute;    top: 595px;right: 450px;width: 0px;height:90px; text-align: left;">
                  
                   </div> 
                   
                    <div class="reqmCel32 reqmbold1bl" style="text-align:right; margin-top:10px; font-weight: bold;margin-left:-160px;">For:- Care Go Logistic Pvt. Ltd</div>
                    <div class="reqmCel32 reqmbold1bl" style="text-align:right; margin-top:50px; margin-left:-250px;">Checked By</div>
                    <div class="reqmCel32 reqmbold1bl" style="text-align:right; margin-top:-14px; font-weight: bold;">Bill Incharge</div>
</div>
	             
            </div> 
			
			

		</div>
	<!-- ------------------------ end Singrauli customer HINDALCO INDUSTRIES LTD.(MAHAN) -------- -->
	
			<!--  start Renukoot SKI Carbon Black pvt ltd. by manoj -->
<!-- 		format changed  -->
					<div ng-show="bill.skiCarbonBillFormat">
								 			
				               <div class="reqmhRow1">
                <div class="reqmCel1"><img src="resources/img/logo.png" width="357" height="106" /></div>
                <div  class="reqmCel1">
                          <div style="font-size: 16px; font-weight: bold; font-family: Arial;">Care Go Logistics Pvt. Ltd.</div>
<!--                           <div style="font-size: 11px">Billing Office: S-405, Greater Kailash, Part-II,New Delhi-110048</div> -->
                          <div style="font-size: 11px">Billing Office: Near Govind Automobiles, Murdhawa,Renukoot Distt-Sonbhadra U.P. Pin-231217</div>
                          <hr>
 							<div style="font-size: 12px">MSME Regd. No.: HR05E0000104</div>
                          <div style="font-size: 11px">Email: gurgaon@carego.co
                          <br>
                          Web Site: Care.go People, CIN: U60231DL2015PTC279265
                          </div> 
                         
			 </div>
             </div>
             <div class="reqmRow1">
                    <div class="reqmCel25 reqmboldbl" style="width: 960px; text-align: center;">Tax Invoice</div>
	             
            </div>
				             
				                    <div class="reqmRow1">
                  <div class="reqmCel04 reqmboldbl" style="height: 140px;width: 960px;">
                  <div class="reqmCel04 reqmboldbl" style="border:0px; text-align:left;"><br><br>&nbsp;&nbsp;To,M/s. </div>
                
                  <div class="reqmCel04 reqmboldbl" style="border:0px; margin-top:-15px;margin-left:90px;  text-align:justify;">{{bill.cust.custName}}
                  <br>{{bill.cust.custAdd}},<br>{{bill.cust.custCity}} {{bill.cust.custPin}}<br><p style="text-align: left"> GSTIN-09AASCS9916L1ZA</p></div>
                  <br>
                <div style="position: absolute;    top: 125px;right: 0;width: 400px;border-right: 1px solid black;">
                 <table style="border: 1px solid black;">
                <tr><td style="border: 1px solid black;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td style="border: 1px solid black;text-align: center" colspan="2">Tax Invoice No.</td><td style="border: 1px solid black;text-align: center" colspan="2">{{bill.bill.blBillNo}}</td></tr>
                <tr><td style="border: 1px solid black;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td style="border: 1px solid black;text-align: center" colspan="2">Date</td><td style="border: 1px solid black;text-align: center" colspan="2">{{bill.bill.blBillDt | date:'dd/MM/yyyy'}}</td></tr>
                <tr><td colspan="3" style="text-align: center;border: 1px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PAN NO.</td><td style="border: 1px solid black;text-align: center" colspan="2">AAGCC0032K</td></tr>
                <tr><td style="border: 1px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td style="border: 1px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td style="border: 1px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td style="border: 1px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td style="border: 1px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>
            
                </table>
                
                </div>
                 
     
                </div>
            </div>
            
            <div class="reqmRow1">
                    <div class="reqmCel25 reqmboldbl" style="width: 960px; text-align: center;"> GTA(Goods Transport Service)</div>
	             
            </div>
            
            
            <div class="reqmRow1">
               
			<div class="reqmCSTableGeneratorss">
				<table>
				 <tr>
			   <td class="reqmltterh reqmltterbold" colspan="5">CONSIGNMENT DETAILS</td> 
				  <td class="reqmltterh reqmltterbold" rowspan="2" >INVOICE NO.</td>
				  <td class="reqmltterh reqmltterbold" rowspan="2">Party Name</td>
				   <td class="reqmltterh reqmltterbold" rowspan="2">TRUCK NO</td>
				   <td class="reqmltterh reqmltterbold" rowspan="2">ACT WEIGHT</td>
				   <td class="reqmltterh reqmltterbold" rowspan="2">WEIGHT<br>(Ton)</td>
				 <td class="reqmltterh reqmltterbold" rowspan="2">Report<br>Dt</td>
				   <td class="reqmltterh reqmltterbold" rowspan="2">UN.Date</td>
				   <td class="reqmltterh reqmltterbold" rowspan="2">FRIEGHT<br>(Rs)</td>
				   <td class="reqmltterh reqmltterbold" rowspan="2">OTHER<br>CHRGES</td>
				   <td class="reqmltterh reqmltterbold" rowspan="2">TOTAL(Rs.)</td>
				  
				 </tr>
				 <tr>
				 <td class="reqmltterh reqmltterbold" >Sr. No.</td>
				   <td class="reqmltterh reqmltterbold" >LR. No.</td>
				   <td class="reqmltterh reqmltterbold" >LR Date</td>
				   <td class="reqmltterh reqmltterbold" >Gate out <br> DT</td>
				   <td class="reqmltterh reqmltterbold" >To Station</td>
				 <!--   <td class="reqmltterh reqmltterbold" >STATE</td>
				   <td class="reqmltterh reqmltterbold" >FRIEGHT<br>RATE</td>
				   <td class="reqmltterh reqmltterbold" >AMOUNT</td>
				   <td class="reqmltterh reqmltterbold" >REMARKS</td>  -->
				 </tr>
			
				 <tr ng-repeat="bilDet in bill.blDetList">
				   <td class="reqmltterh">{{$index+1}}</td>
				   <td class="reqmltterh">{{bilDet.cnmt.cnmtCode}}</td>
				   <td class="reqmltterh reqmltterbold">{{bilDet.cnmt.cnmtDt | date:'dd/MM/yyyy'}}</td>
				   <td class="reqmltterh">{{bilDet.cnmt.cnmtGtOutDt | date:'dd/MM/yyyy'}}</td>
<!-- 				   <td class="reqmltterh">{{bilDet.frmStn}}</td> -->
				   <td class="reqmltterh">{{bilDet.toStn}}</td>
				    <td class="reqmltterh">{{bilDet.cnmt.cnmtInvoiceNo[0].invoiceNo}}<br>{{bilDet.cnmt.cnmtInvoiceNo[1].invoiceNo}}</td>
				   <td class="reqmltterh">{{bilDet.consignee.custName}}</td>
				   <td class="reqmltterh">{{bilDet.truckNo}}</td>
				   <td class="reqmltterh">{{bilDet.billDet.bdActWt/1000 | number : 3}}</td>
				   <td class="reqmltterh">
				   		<div ng-show="bilDet.billDet.bdBlBase == 'chargeWt'">{{bilDet.billDet.bdChgWt/1000 | number : 3}}</div>
		                <div ng-show="bilDet.billDet.bdBlBase == 'actualWt'">{{bilDet.billDet.bdActWt/1000 | number : 3}}</div>
		                <div ng-show="bilDet.billDet.bdBlBase == 'receiveWt'">{{bilDet.billDet.bdRecWt/1000 | number : 3}}</div>
				   </td>
				   <td class="reqmltterh">{{bilDet.delDt | date:'dd/MM/yyyy'}}</td>
				   <td class="reqmltterh">{{bilDet.arDt | date:'dd.MM.yyyy' }}</td>
				    <td class="reqmltterh">{{bilDet.billDet.bdFreight | number : 3}}</td>
				     <td class="reqmltterh">{{bilDet.billDet.bdUnloadAmt + bilDet.billDet.bdDetAmt + bilDet.billDet.bdOthChgAmt +  bilDet.billDet.bdUnloadDetAmt + bilDet.billDet.bdTollTax| number : 3}}</td>
				        <td class="reqmltterh">{{bilDet.billDet.bdTotAmt | number : 3}}</td>
				   
				 </tr>
				
			
				
				<tr>
				   <td class="reqmltterh reqmltterbold">&nbsp;</td>
				   <td class="reqmltterh reqmltterbold">&nbsp;</td>
				   <td class="reqmltterh reqmltterbold">&nbsp;</td>
				   <td class="reqmltterh reqmltterbold">&nbsp;</td>
				   <td class="reqmltterh reqmltterbold">&nbsp;</td>
				   <td class="reqmltterh reqmltterbold">&nbsp;</td>
				   <td class="reqmltterh reqmltterbold">&nbsp;</td>
				   <td class="reqmltterh reqmltterbold">&nbsp;</td>
				   <td class="reqmltterh reqmltterbold">&nbsp;</td>
				   <td class="reqmltterh reqmltterbold">&nbsp;</td>
				   <td class="reqmltterh reqmltterbold">&nbsp;</td>
				   <td class="reqmltterh reqmltterbold">&nbsp;</td>
				   <td class="reqmltterh reqmltterbold">&nbsp;</td>
				   <td class="reqmltterh reqmltterbold">&nbsp;TOTAL</td>
				   <td class="reqmltterh reqmltterbold">&nbsp;{{bill.bill.blFinalTot}}</td>
				</tr>
					<tr>
				   <td colspan="2" class="reqmltterh reqmltterbold" style="text-align: center;">In words-:</td>
				   <td colspan="13" class="reqmltterh reqmltterbold" style="text-align: left;">&nbsp;{{bill.bilAmt}}</td>
				  
				</tr>
				
				</table>
				              
			</div>
      </div>
       
       
       
     
        
     
           
             <div class="reqmRow1">
            
                 <div class="reqmCel30" style="height:120px;">
                <br>
              
             
              <p style="text-align: left;font-size: 80%; font-weight: bold;">Service Accounting Code (SAC) : 996511<br>
              Notification Code. NO. 13/2017- Central Tax(Rate) dt 28<sup>th</sup> Jun 2017<br>
              Place of Origin- UP - State Code- 09<br>
             
                </p>
                <p style="text-align: left;font-size: 70%;">
             Certified that the Credit of input tax charged on goods and services used in supplying of GTA service has not been taken in view of notification issued under Goods & Service Tax.
                </p>
                
                
                 <!--   <div class="reqmCel32 reqmbold1bl" style="text-align:right; margin-top:10px; font-weight: bold;">For, Care Go Logistic Pvt. Ltd</div>
                    <div class="reqmCel32 reqmbold1bl" style="text-align:right; margin-top:50px; margin-left:-250px;"></div>
                    <div class="reqmCel32 reqmbold1bl" style="text-align:right; margin-top:-14px; font-weight: bold;">Auth. Signatory</div> -->
                 </div> 
             </div>
             
            <!-- <div class="reqmRow1">
                     <div class="reqmCel21 reqmbold1bl reqmpadding3bl"><span>Corporate Office: SCO-26, 1st Floor, Sec. 15, Part-II, Gurgaon-122001,</span> &nbsp  <span>Ph.No.: 0124-4205305,</span> &nbsp <span>Email ID: corporate@tcgppl.com</span></div>
             </div>  -->
				    
				      <div class="reqmRow1">
                    <div class="reqmCel25 reqmboldbl" style="width: 960px;">
                    <div style="position: absolute; width: 300px; height: 90px;">
                    <table>
                    <tr><td>Bank:<br>
                    Branch:<br>
                    Account No<br>
                    RTGS/NEFT
                    </td>
                    <td>Kotak Mahindra Bank<br>Gurgaon<br>3011623336<br>KKBK0000299</td></tr>

                    </table>
                    </div>
                    <div class="reqmCel32 reqmbold1bl" style="text-align:right; margin-top:10px; font-weight: bold;margin-left:-160px;">For:- Care Go Logistic Pvt. Ltd</div>
                    <div class="reqmCel32 reqmbold1bl" style="text-align:right; margin-top:50px; margin-left:-250px;">Checked By</div>
                    <div class="reqmCel32 reqmbold1bl" style="text-align:right; margin-top:-14px; font-weight: bold;">Bill Incharge</div>
</div>
	             
            </div>    
								 
				</div> 
		
		<!--  end renukoot SKI Carbon Black pvt ltd. by manoj-->
			 
 					<!-- ------------------------ start Singrauli Hindalco Industries Ltd -------- -->
			 <div ng-show="bill.hindalcoBillFormat">
	    <div class="emhRow1">
                <div class="emCel1"><img src="resources/img/logo.png" width="357" height="106" /></div>
                <div  class="emCel1">
                          <div style="font-size: 16px; font-weight: bold; font-family: Arial;">Care Go Logistics Pvt. Ltd.</div>
                          <div style="font-size: 12px">Billing Office: S-405, Greater Kailash, Part-II,New Delhi-110048 </div>
<!--                           <div style="font-size: 12px">Phone:{{ selBrh.branchPhone }}</div>  -->
                          <div style="font-size: 12px">MSME Regd. No.: HR05E0000104</div>
                          <div style="font-size: 12px">Email: gurgaon@carego.co</div>
			 </div>
             </div>
            <div class="emRow1">
                  <div class="emCel01 emboldbl">
                  	<div ng-show="bill.bill.blType == 'N'">FRIEGHT BILLS</div>
                  	<div ng-show="bill.bill.blType == 'S'">SUPPLEMENTARY BILLS</div>
                  </div>
                <div class="emCel02 emboldbl">Co CIN No. U60231DL2015PTC279265</div>
                <div class="emCel03 emboldbl">Service Tax Registration No.: AAGCC0032KSD001</div>
            </div>
            
            
            <div class="emRow1">
                  <div class="emCel04 emboldbl">
                  <div class="emCel04 emboldbl" style="border:0px; text-align:left;">To, M/s </div>
                
                  <div class="emCel04 emboldbl" style="border:0px; margin-top:-15px; margin-left:100px; text-align:justify;">{{bill.cust.custName}}
                  <br>{{bill.cust.custAdd}},<br>{{bill.cust.custCity}} {{bill.cust.custPin}}</div>
                  
                  </div>
               <div class="emCel066 emboldbl">
                   <div class="emCel06 emboldbl">
                      <div style="margin-top:2px; text-align:center;">BILL NO.: {{bill.bill.blBillNo}}</div><br><div>DATE: {{bill.bill.blBillDt | date:'dd/MM/yyyy'}}</div>
                   </div>
               </div>
            </div>
            
            <div class="emRow1">
                    <div class="emCel25 emboldbl">We hereby Submit our Bill for the Goods Transported as Under:</div>
	                <div class="emCel26 emboldbl">OUR PAN NO: AAGCC0032K</div>
	                <div class="emCel26 emboldbl">No TDS To Be Deducted w.e.f 01/10/2009</div>
            </div>
            
            
            <div class="emRow1">
               
			<div class="emCSTableGeneratorss">
				<table>
				 <tr>
				   <td class="emltterh emltterbold" width="450px"colspan="5">CONSIGNMENT DETAILS</td>
				   <td class="emltterh emltterbold" rowspan="2">Invoice NO.</td>
				   <td class="emltterh emltterbold" rowspan="2">Truck No.</td>
				   <td class="emltterh emltterbold" rowspan="2">Product</td>
				   <td class="emltterh emltterbold" rowspan="2">Weight MT</td>
				   <td class="emltterh emltterbold" rowspan="2">Rate</td>
				   <td class="emltterh emltterbold" rowspan="2">Freight (A)</td>
				   <td class="emltterh emltterbold" rowspan="2">Other Charges</td>
				   <td class="emltterh emltterbold" rowspan="2">Total</td>
				 </tr>
				 <tr>
				   <!-- <td class="emltterh mltterbold">Sales Order No.</td> -->
				   <td class="emltterh emltterbold">Date</td>
				   <td class="emltterh emltterbold">LR NO.</td>
				   <td class="emltterh emltterbold">From Station</td>
				   <td class="emltterh emltterbold">To Station</td>
				   <td class="emltterh emltterbold">Dly Date</td>
				 </tr>
				 <tr ng-repeat="bilDet in bill.blDetList">
				   <!-- <td class="emltterh">{{bilDet.cnmt.cnmtCode}}</td> -->
				   <td class="emltterh">{{bilDet.cnmt.cnmtDt | date:'dd/MM/yyyy'}}</td>
				   <td class="emltterh">{{bilDet.cnmt.cnmtCode}}</td>
				   <td class="emltterh">{{bilDet.frmStn}}</td>
				   <td class="emltterh">{{bilDet.toStn}}</td>
				   <td class="emltterh emltterbold">{{bilDet.delDt}}</td>
				   <td class="emltterh">{{bilDet.cnmt.cnmtInvoiceNo[0].invoiceNo}}<br>{{bilDet.cnmt.cnmtInvoiceNo[1].invoiceNo}}</td>
				   <td class="emltterh">{{bilDet.truckNo}}</td>
				   <td class="emltterh">{{bilDet.cnmt.cnmtProductType}}</td>
				   <td class="emltterh">
				   		<div ng-show="bilDet.billDet.bdBlBase == 'chargeWt'">{{bilDet.billDet.bdChgWt/1000 | number : 3}}</div>
		                <div ng-show="bilDet.billDet.bdBlBase == 'actualWt'">{{bilDet.billDet.bdActWt/1000 | number : 3}}</div>
		                <div ng-show="bilDet.billDet.bdBlBase == 'receiveWt'">{{bilDet.billDet.bdRecWt/1000 | number : 3}}</div>
				   </td>
				   <td class="emltterh">{{bilDet.billDet.bdRate * 1000 | number : 0}}</td>
				   <td class="emltterh">{{bilDet.billDet.bdFreight | number : 0}}</td>
				   <td class="emltterh">{{bilDet.billDet.bdOthChgAmt | number :0}}</td>
				   <td class="emltterh">{{bilDet.billDet.bdTotAmt | number : 0}}</td>
				 </tr>
				
				</table>
				              
			</div>
      </div>
       
       
       
         <div class="emRow1">
                  <div class="emCel16">
                     <div class="emCel17 embold2bl">&nbsp;</div>
                     <div class="emCel17 embold2bl">IT IS HEREBY DECLARE THAT WE ARE NOT AVAILING CENVAT CREDIT FACILITY IN RESPECT OF INPUTS/SERVICES /CAPITAL GOODS</div>
                     <div class="emCel17 embold2bl">WE HAVE NOT AVAILED ANY BENEFIT UNDER THE NOTIFICATION NO. 12/2003-SERVICE TAX DATED 20/06/2008</div>
                   </div>
               
               
             
                
                 <div class="emCel15 embold2bl" style="text-align:center;">
                     <div class="emCel14 embold1bl empaddingbl em1" >SUB TOTAL</div>
                     <div class="emCel14 embold1bl empaddingbl em1" >{{bill.bill.blSubTot}}</div> 
                  </div>
               
            </div>
       
        
         <div class="emRow1">
                     <div class="emCel18 embold1bl">Bank Name</div>
                     <div class="emCel00 embold1bl">A/c No.</div>
                     <div class="emCel18 embold1bl">IFSC CODE</div>
                     <div class="emCel05 embold1bl">Branch</div>
                     <div class="emCel14 embold2bl" style="text-align:right; padding-right:5px;">&nbsp;</div>
                     <div class="emCel14 embold2bl">&nbsp</div>
             </div>
             
             <div class="emRow1">
                     <div class="emCel18 embold1bl empadding1bl">Kotak Mahindra Bank</div>
                     <div class="emCel00 embold1bl empadding1bl">3011623336</div>
                     <div class="emCel18 embold1bl empadding1bl">KKBK0000299</div>
                     <div class="emCel05 embold1bl empaddingbl">61, Old Judicial Complex,<br> Sector-15, Gurgaon 122001</div>
                     <div class="emCel14 embold2bl empdng" style="text-align:right;"><span>&nbsp;</span><br><span>&nbsp;</span><br><span>&nbsp;</span></div>
                     <div class="emCel14 embold2bl empadding1bl">&nbsp</div>
             </div>
       
         <div class="emRow1">
                     <div class="emCel20 embold2bl empaddingbl">
                        By Cheque / DD in Favour Of<span class="bold1">Care Go Logistics Pvt. Ltd.</span>, or Electronic Fund transfer to <span class="bold1">above Bank A/C</span>
                        <br>You are hereby requested to mail us Payment Detail on mail Id <span class="bold1">{{ selBrh.branchEmailId }}</span>
                     </div>
                     <div class="emCel20 embold1bl empadding00bl">Amount in Words: {{bill.bilAmt}}</div>
                     <div class="emCel14 embold1bl empadding2bl" style="text-align:right;">Total</div>
                     <div class="emCel14 embold1bl empadding2bl">{{bill.bill.blFinalTot}}</div>
             </div>
           
             <div class="emRow1">
                 <div class="emCel30" style="height:100px;">
                   <div class="emCel32 embold1bl" style="text-align:right; margin-top:10px;">For Care Go Logistic Pvt. Ltd</div>
                    <div class="emCel32 embold1bl" style="text-align:right; margin-top:50px; margin-left:-250px;">Checked By</div>
                    <div class="emCel32 embold1bl" style="text-align:right; margin-top:-14px;">Bill Incharge</div>
                 </div> 
             </div>
             
            <div class="emRow1">
                     <div class="emCel21 embold1bl empadding3bl"><span>Corporate Office: SCO-26, 1st Floor, Sec. 15, Part-II, Gurgaon-122001,</span> &nbsp  <span>Ph.No.: 0124-4205305,</span> &nbsp <span>Email ID: corporate@tcgppl.com</span></div>
             </div> 

			

		</div>
			
<!--  					------------------------- start Renkoot's Grasim Industries Ltd ---------------------  -->
 					
<!--  					format changed -->
 					<div ng-show="bill.grasimIndusBillFormat"> 
		    <div class="reqmhRow1">
                <div class="reqmCel1"><img src="resources/img/logo.png" width="357" height="106" /></div>
                <div  class="reqmCel1">
                          <div style="font-size: 16px; font-weight: bold; font-family: Arial;">Care Go Logistics Pvt. Ltd.</div>
                          <div style="font-size: 12px">Billing Office: S-405, Greater Kailash, Part-II,New Delhi-110048 </div>
<!--                           <div style="font-size: 12px">Phone:{{ selBrh.branchPhone }}</div>  -->
                          <div style="font-size: 12px">MSME Regd. No.: HR05E0000104</div>
                          <div style="font-size: 12px">Email: gurgaon@carego.co</div>
			 </div>
             </div>
             <!-- <div class="reqmRow1">
                  <div class="reqmCel01 reqmboldbl">
                  	<div ng-show="bill.bill.blType == 'N'">FRIEGHT BILLS</div>
                  	<div ng-show="bill.bill.blType == 'S'">SUPPLEMENTARY BILLS</div>
                  </div>
                <div class="reqmCel02 reqmboldbl">Co CIN No. U60231DL2015PTC279265</div>
                <div class="reqmCel03 reqmboldbl">Service Tax Registration No.: AAGCC0032KSD001</div>
            </div> --> 
            
            
            <div class="reqmRow1">
                  <div class="reqmCel04 reqmboldbl" style="height: 260px;width: 960px;">
                  <div class="reqmCel04 reqmboldbl" style="border:0px; text-align:left;"><br><br>To, </div>
                <br><br><br>
                  <div class="reqmCel04 reqmboldbl" style="border:0px; margin-top:-15px;  text-align:justify;"><br><br>{{bill.cust.custName}}
                  <br><br>{{bill.cust.custAdd}},<br><br>{{bill.cust.custCity}} {{bill.cust.custPin}}<br><br><p style="text-align: left"> GSTIN-09AAACG4464B7ZQ</p></div>
                  <br>
                <div style="position: absolute;    top: 120px;right: 0;width: 400px;border-right: 1px solid black;">
                 <table>
                <tr><td>Name of Service</td><td>GTA(Goods Transport Service)</td></tr>
                <tr><td> Service Accounting Cc(SAC)</td><td>996511(FIXED)</td></tr>
                <tr><td>PAN No.</td><td>AAGCC0032K</td></tr>
                <tr><td> GSTIN</td><td>07AAGCC0032K1ZZ</td></tr>
                <tr><td>BILL NO.:</td><td>{{bill.bill.blBillNo}}</td></tr>
                <tr><td>DATE:</td><td>{{bill.bill.blBillDt | date:'dd/MM/yyyy'}}</td></tr>
                <tr><td>VENDOR CODE</td><td>2100014138</td></tr>
                </table>
                
                </div>
                 
          <!--    <div class="reqmCel066 reqmboldbl">
                   <div class="reqmCel06 reqmboldbl">
                     <div style="margin-top:2px; text-align:center;">Name of Service &nbsp;&nbsp;&nbsp;&nbsp;GTA(Goods Transport Service)<br>
                      Service Accounting Cc(SAC)996511(FIXED)<br>
                      PAN No. AAGCC0032K1ZZ<br>
                      GSTIN 07AAGCC0032K1ZZ
               
                      </div><br>
                      
               <div>BILL NO.: {{bill.bill.blBillNo}}<br>
                      DATE: {{bill.bill.blBillDt | date:'dd/MM/yyyy'}}
                      <br>
                      VENDOR CODE 2100014138
                      </div>
               
                   </div>
               </div> -->  
                </div>
            </div>
            
            <div class="reqmRow1">
                    <div class="reqmCel25 reqmboldbl" style="width: 960px; text-align: left;">&nbsp;&nbsp;SUBJECT : SBP,FLAKES,ALCP & CPW transportation Freight Bill from Renukoot to various Destination </div>
	              <!--   <div class="reqmCel26 reqmboldbl">OUR PAN NO: AAGCC0032K</div>
	                <div class="reqmCel26 reqmboldbl">No TDS To Be Deducted w.e.f 01/10/2009</div> -->
            </div>
            
            
            <div class="reqmRow1">
               
			<div class="reqmCSTableGeneratorss">
				<table>
				 <tr>
			<!-- 	   <td class="reqmltterh reqmltterbold" width="450px"colspan="6">CONSIGNMENT DETAILS</td> -->
				  <td class="reqmltterh reqmltterbold" >Sr. NO.</td>
				  <td class="reqmltterh reqmltterbold" >INVOICE NO</td>
				   <td class="reqmltterh reqmltterbold" >DATE</td>
				   <td class="reqmltterh reqmltterbold" >LR. NO.</td>
				   <td class="reqmltterh reqmltterbold" >VEHICLE<br>NUMBER</td>
				   <td class="reqmltterh reqmltterbold" >VEHICLE<br>TYPE</td>
				   <td class="reqmltterh reqmltterbold" >WEIGHT</td>
				   <td class="reqmltterh reqmltterbold" >PARTY NAME</td>
				   <td class="reqmltterh reqmltterbold" >PRODUCT</td>
				   <td class="reqmltterh reqmltterbold" >Destination</td>
				   <td class="reqmltterh reqmltterbold" >STATE</td>
				   <td class="reqmltterh reqmltterbold" >FRIEGHT<br>RATE</td>
				   <td class="reqmltterh reqmltterbold" >AMOUNT</td>
				   <td class="reqmltterh reqmltterbold" >REMARKS</td>
				 </tr>
				<!--  <tr>
				   <td class="reqmltterh mltterbold">Sales Order No.</td>
				   <td class="reqmltterh reqmltterbold">Invoice NO.</td>
				   <td class="reqmltterh reqmltterbold">CNMT NO.</td>
				   <td class="reqmltterh reqmltterbold">Date</td>
				   <td class="reqmltterh reqmltterbold">Truck NO.</td>
				   <td class="reqmltterh reqmltterbold">From Station</td>
				   <td class="reqmltterh reqmltterbold">To Station</td>
				   
				 </tr> -->
				 <tr ng-repeat="bilDet in bill.blDetList">
				   <!-- <td class="reqmltterh">{{bilDet.cnmt.cnmtCode}}</td> -->
				   <td class="reqmltterh">{{$index+1}}</td>
				    <td class="reqmltterh">{{bilDet.cnmt.cnmtInvoiceNo[0].invoiceNo}}<br>{{bilDet.cnmt.cnmtInvoiceNo[1].invoiceNo}}</td>
				   <td class="reqmltterh reqmltterbold">{{bilDet.cnmt.cnmtDt | date:'dd/MM/yyyy'}}</td>
				   <td class="reqmltterh">{{bilDet.cnmt.cnmtCode}}</td>
				   <td class="reqmltterh">{{bilDet.truckNo}}</td>
				     <td class="reqmltterh">{{bilDet.cnmt.cnmtVehicleType}}</td>
				   <td class="reqmltterh">
				   		<div ng-show="bilDet.billDet.bdBlBase == 'chargeWt'">{{bilDet.billDet.bdChgWt/1000 | number : 3}}</div>
		                <div ng-show="bilDet.billDet.bdBlBase == 'actualWt'">{{bilDet.billDet.bdActWt/1000 | number : 3}}</div>
		                <div ng-show="bilDet.billDet.bdBlBase == 'receiveWt'">{{bilDet.billDet.bdRecWt/1000 | number : 3}}</div>
				   </td>
				   <td class="reqmltterh">{{bilDet.consignee.custName}}</td>
				   <td class="reqmltterh">{{bilDet.cnmt.cnmtProductType}}</td>
				   <td class="reqmltterh">{{bilDet.toStn}}</td>
				   <td class="reqmltterh">{{bilDet.toStnStatePre}}</td>
				   <td class="reqmltterh">{{bilDet.billDet.bdRate * 1000 | number : 3}}</td>
				    <td class="reqmltterh">{{bilDet.billDet.bdFreight | number : 3}}</td>
				      <td class="reqmltterh">{{bilDet.cnmt.arRemark}}</td>
				   
				 </tr>
				
				<!--  <tr>
				   <td class="reqmltterh reqmltterbold">&nbsp;</td>
				   <td class="reqmltterh reqmltterbold">&nbsp;</td>
				   <td class="reqmltterh reqmltterbold">&nbsp;</td>
				   <td class="reqmltterh reqmltterbold">&nbsp;</td>
				   <td class="reqmltterh reqmltterbold">&nbsp;</td>
				   <td class="reqmltterh reqmltterbold">&nbsp;</td>
				   <td class="reqmltterh reqmltterbold">&nbsp;</td>
				   <td class="reqmltterh reqmltterbold">&nbsp;</td>
				   <td class="reqmltterh reqmltterbold">&nbsp;</td>
				   <td class="reqmltterh reqmltterbold">&nbsp;</td>
				</tr> -->
				
				<!-- <tr>
				   <td class="reqmltterh reqmltterbold">&nbsp;</td>
				   <td class="reqmltterh reqmltterbold">&nbsp;</td>
				   <td class="reqmltterh reqmltterbold">&nbsp;</td>
				   <td class="reqmltterh reqmltterbold">&nbsp;</td>
				   <td class="reqmltterh reqmltterbold">&nbsp;</td>
				   <td class="reqmltterh reqmltterbold">&nbsp;</td>
				   <td class="reqmltterh reqmltterbold">&nbsp;</td>
				   <td class="reqmltterh reqmltterbold">&nbsp;</td>
				   <td class="reqmltterh reqmltterbold">&nbsp;</td>
				   <td class="reqmltterh reqmltterbold">&nbsp;</td>
				</tr> -->
				
				<tr>
				   <td class="reqmltterh reqmltterbold">&nbsp;</td>
				   <td class="reqmltterh reqmltterbold">&nbsp;</td>
				   <td class="reqmltterh reqmltterbold">&nbsp;</td>
				   <td class="reqmltterh reqmltterbold">&nbsp;</td>
				   <td class="reqmltterh reqmltterbold">&nbsp;</td>
				   <td class="reqmltterh reqmltterbold">&nbsp;TOTAL</td>
				   <td class="reqmltterh reqmltterbold">&nbsp;{{bill.totWt}}</td>
				   <td class="reqmltterh reqmltterbold">&nbsp;</td>
				   <td class="reqmltterh reqmltterbold">&nbsp;</td>
				   <td class="reqmltterh reqmltterbold">&nbsp;</td>
				   <td class="reqmltterh reqmltterbold">&nbsp;</td>
				   <td class="reqmltterh reqmltterbold">&nbsp;</td>
				   <td class="reqmltterh reqmltterbold">&nbsp;{{bill.bill.blFinalTot}}</td>
				   <td class="reqmltterh reqmltterbold">&nbsp;</td>
				</tr>
				
				</table>
				              
			</div>
      </div>
       
       
       
      <!--    <div class="reqmRow1">
                  <div class="reqmCel16">
                     <div class="reqmCel17 reqmbold2bl">&nbsp;</div>
                     <div class="reqmCel17 reqmbold2bl">IT IS HEREBY DECLARE THAT WE ARE NOT AVAILING CENVAT CREDIT FACILITY IN RESPECT OF INPUTS/SERVICES /CAPITAL GOODS</div>
                     <div class="reqmCel17 reqmbold2bl">WE HAVE NOT AVAILED ANY BENEFIT UNDER THE NOTIFICATION NO. 12/2003-SERVICE TAX DATED 20/06/2008</div>
                   </div>
               
               
             
                
                 <div class="reqmCel15 reqmbold2bl" style="text-align:center;">
                     <div class="reqmCel14 reqmbold1bl reqmpaddingbl reqm1" >SUB TOTAL</div>
                     <div class="reqmCel14 reqmbold1bl reqmpaddingbl reqm1" >{{bill.bill.blSubTot}}</div> 
                  </div>
               
            </div>
        -->
        
        
       <!--   <div class="reqmRow1">
                     <div class="reqmCel20 reqmbold2bl reqmpaddingbl">
                        By Cheque / DD in Favour Of<span class="bold1">Care Go Logistics Pvt. Ltd.</span>, or Electronic Fund transfer to <span class="bold1">above Bank A/C</span>
                        <br>You are hereby requested to mail us Payment Detail on mail Id <span class="bold1">{{ selBrh.branchEmailId }}</span>
                     </div>
                     <div class="reqmCel20 reqmbold1bl reqmpadding00bl">Amount in Words: {{bill.bilAmt}}</div>
                     <div class="reqmCel14 reqmbold1bl reqmpadding2bl" style="text-align:right;">Total</div>
                     <div class="reqmCel14 reqmbold1bl reqmpadding2bl">{{bill.bill.blFinalTot}}</div>
             </div> -->
           
             <div class="reqmRow1"">
            
                 <div class="reqmCel30" style="height:200px;">
                 <p style="text-align: left;font-weight: bold;">Rupees in words:- &nbsp;&nbsp;&nbsp;{{bill.bilAmt}}</p>
                 <br>
             
              <p style="text-align: left;">Declaration: "We hereby declare that GST Credit on Inputs,Capital Goods and Input services,<br>
              used for providing the taxable services has not been taken under the provisions of the CGST Act-2017
             </p>
                
                   <div class="reqmCel32 reqmbold1bl" style="text-align:right; margin-top:10px; font-weight: bold;">For, Care Go Logistic Pvt. Ltd</div>
                    <div class="reqmCel32 reqmbold1bl" style="text-align:right; margin-top:50px; margin-left:-250px;"></div>
                    <div class="reqmCel32 reqmbold1bl" style="text-align:right; margin-top:-14px; font-weight: bold;">Auth. Signatory</div>
                 </div> 
             </div>
             
            <div class="reqmRow1">
                     <div class="reqmCel21 reqmbold1bl reqmpadding3bl"><span>Corporate Office: SCO-26, 1st Floor, Sec. 15, Part-II, Gurgaon-122001,</span> &nbsp  <span>Ph.No.: 0124-4205305,</span> &nbsp <span>Email ID: corporate@tcgppl.com</span></div>
             </div> 

			

		</div>
 					
 					
 			<!-- ------------------------- end Renkoot's Grasim Industries Ltd ---------------------  -->


				 <!--ruchi soya  new format  bill no HAL000723   starts here -->
	
				 <div ng-show="bill.ruchiSoyaBillFormat">
				 
					
				 
				 
				 
							<div class="hRow1">
					<div class="dCel1">
						<img src="resources/img/logo.png" width="357" height="106" />
					</div>
					<div class="dCel1">
						<div
							style="font-size: 25px; font-weight: bold; font-family: Arial;">Care
							Go Logistics Pvt. Ltd.</div>
						<div style="font-size: 14px">Billing Office: S-405, Greater Kailash, Part-II,New Delhi-110048</div>
						<!--  <div style="font-size: 12px">Phone:{{ selBrh.branchPhone }}</div> 
						<div style="font-size: 12px">gurgaon@carego.co</div> -->
					<div style="font-size: 12px">MSME Regd. No.: HR05E0000104</div>
					</div>
				</div>
				<div class="dRow1">
					<div class="mymydCel01 bodbl">
						<div ng-show="bill.bill.blType == 'N'"> TAX INVOICE</div>
						 <div ng-show="bill.bill.blType == 'S'">SUPPLEMENTARY BILLS</div> 
					</div>
					<div class="mymydCel03 bodbl">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					CIN No.- U60231DL2015PTC279265</div>
					 <div class="mymydCel03 bodbl">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					 SAC No.
						{{bill.cust.custSAC}}</div>
				</div>
				
				<div class="dRow1">
					<div class="dCel04 bodbl">
						<div class="dCel04 bodbl" style="border: 0px; text-align: left;">To,
							M/s</div>

						<div class="dCel04 bodbl"
							style="border: 0px; margin-top: -15px; margin-left: 100px; text-align: justify;">
							{{bill.cust.custName}} <br>{{bill.cust.custAdd}},<br>{{bill.cust.custCity}}
							{{bill.cust.custPin}}
							<br>
                          	
						</div>
						 <p style="text-align: left;margin-left: 5px;">GST NO: {{bill.bill.blGstNo}}
						  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						    SAC COD-&nbsp;&nbsp;{{bill.cust.custSAC}} 
						 </p>

					</div>
					<div class="dCel066 bodbl">
						<div class="dCel06 bodbl">
							<div style="margin-top: 2px; text-align: center;">
							<br>
							Invoice NO.:
								{{bill.bill.blBillNo}}</div>
							<br>
							<div style="height: 27px;">DATE: {{bill.bill.blBillDt | date:'dd/MM/yyyy'}}
							<br>
							</div>
						</div>
					</div>
				</div>

				<div class="dRow1">
					<div class="mymydCel25 bodbl">We Hereby Submit Our Frieght Bill For The
						Goods Transported as Under:</div>
					<div class="mymydCel26 bodbl">OUR PAN NO: AAGCC0032K</div>
			 <div class="mymydCel26 bodbl">GST NO:</div>
					
				</div>

				<div class="dRow1">

					<div class="CSSTabGeneratorss">
						<table>
							<tr>
								<td width="300px" colspan="6">CONSIGNMENT DETAILS</td>								
							    <td rowspan="2"> Actual Weight</td>
				  				<td rowspan="2">Charge Weight</td>
							   	<td rowspan="2">Rate</td>
							   	<td rowspan="2">Freight(A)</td>
							   	<td rowspan="2">Detention(B)</td>
							   	<td rowspan="2">Bonus(C)</td>
							   	<td rowspan="2">Ldng/Unldng(D)</td>
							   	<td rowspan="2">Others(E)</td>
							   	<td rowspan="2">Total(A+B+C+D+E)</td>								
							</tr>
							<tr>
								<td><strong class="bbl">Date</strong></td>
								<td><strong class="bbl">Invoice No.</strong></td>
								<td><strong class="bbl">LR No.</strong></td>
								<td><strong class="bbl">Lorry No.</strong></td>
								<td><strong class="bbl">From Station</strong></td>
								<td><strong class="bbl">To Station</strong></td>								   
							</tr>
							<tr ng-repeat="bilDet in bill.blDetList">
							
								<td><strong class="bod2bl">{{bilDet.cnmt.cnmtDt | date:'dd/MM/yyyy'}}</td>
							  	<td><strong class="bod2bl">{{bilDet.cnmt.cnmtInvoiceNo[0].invoiceNo}}<br>{{bilDet.cnmt.cnmtInvoiceNo[1].invoiceNo}}</td>
							   	<td><strong class="bod2bl">{{bilDet.cnmt.cnmtCode}}</td>
							   	<td><strong class="bod2bl">{{bilDet.truckNo}}</td>
							   	<td><strong class="bod2bl">{{bilDet.frmStn}}</td>
							   	<td><strong class="bod2bl"> {{bilDet.toStn}}</td>
							   	<td><strong class="bod2bl">{{bilDet.billDet.bdActWt/1000 | number : 3}}</td>
							   	<td><strong class="bod2bl">{{bilDet.billDet.bdChgWt/1000 | number : 3}}</td>
							   	<td><strong class="bod2bl">{{bilDet.billDet.bdRate * 1000 | number : 2}}</td>
							    <td><strong class="bod2bl">{{bilDet.billDet.bdFreight | number : 2}}</td>
							    <td><strong class="bod2bl">{{bilDet.billDet.bdDetAmt | number : 2}}</td>
							   	<td><strong class="bod2bl">{{bilDet.billDet.bdBonusAmt | number : 2}}</td>
							   	<td><strong class="bod2bl">{{bilDet.billDet.bdLoadAmt | number : 2}}/{{bilDet.billDet.bdUnloadAmt | number : 2}}</td>
							   	<td><strong class="bod2bl">{{bilDet.billDet.bdOthChgAmt | number : 2}}</td> 
							   	<td><strong class="bod2bl">{{bilDet.billDet.bdTotAmt | number : 2}}</td>		   
							  
							</tr>

						</table>

					</div>
				</div>


			

				<div class="dRow1">
					<div class="mymydCel20 bod2bl pdingbl">
					IT IS HEREBY DECLARE THAT WE ARE NOT AVAILING CENVAT CREDIT FACILITY IN RESPECT OF INPUTS/SERVICES/CAPITAL GOODS
					<hr>
					WE HAVE NOT AVAILED ANY BENEFIT UNDER THE NOTIFICATION NO. 12/2003-SERVICE TAX DATED 20/06/2008
					<hr>
					
						 Amount in words: &nbsp;&nbsp;&nbsp;&nbsp; <span class="bbl">{{bill.bilAmt}}</span>
						
					
						
					</div>
					
					<div class="dRow1">
					
					<!-- <div class="dCel20 bod1bl pding2bl">Pay {{bill.bilAmt}}</div> -->
					<div class="mymydCel14 bod1bl pding2bl" style="text-align: right;"><br>Total<br><br></div>
					<div class="mymydCel14 bod1bl pding2bl"><br>{{bill.bill.blFinalTot}}<br><br></div>
				</div>

				<div class="dRow1">
					<div class="hedCel28 bod2bl" style="text-align: center;">
						<!-- <div class="dCel31 bod1bl"> Bank Details</div> -->
						<div class="hemdCel27 bod1bl">Bank Name</div>
						<div class="hemdCel27 bod1">A/c No.</div>
						<div class="hemdCel27 bod1bl">IFSC CODE</div>
						<div class="hedCel29 bod1bl">Branch
						</div>
						<div class="hemdCel27 bod1bl pding1bl">KOTAK MAHINDRA BANK</div>
						<div class="hemdCel27 bod1bl pding1bl">3011623336</div>
						<div class="hemdCel27 bod1bl pding1bl">KKBK0000299</div>
						<div class="hedCel29 bod1bl pdingbl">
						<br>
						61, Old Judicial Complex, Sector-15,
							Gurgaon 122001
							<br>
							</div>
							
							<div class="mymydCel27 bod1bl pding1bl">
         Only by A/C Payee Cheque / DD in favour of 
         <span><strong>Care Go Logistics Pvt. Ltd., of Electronic Fund Transfer to above Bank A/c
         <br>
         You are hereby requested to mail us Payment Detail on mail Id corp.carego@gmail.com
         </strong></span>

</div>
							
								 <div class="mymydCel31 bod1bl"> <strong>For Care Go Logistics Pvt. Ltd.</strong></div> 
							
						
					</div>
			 	<div class="hedCel30" style="height: 122px;">
					<br><br>
		<u></u>
			
					</div>
				</div> 

				<div class="dRow1">
					<div class="hedCel21 bod1bl pding3bl" style="text-align: right">
					<br>
					<br>
					Bill Incharge
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	
					</div>
				</div>
				 </div>
				 
				 				 </div>
				 
				 
				<!--  bill no HAL000723 ends here --> 
	
	<!--  chennai ski carbon start -->
		<div ng-show="bill.SkiCArbonnChennaiBillFormat">
		<!-- 		<div class="hRow1">
					<div class="dCel1">
						<img src="resources/img/logo.png" width="357" height="106" />
					</div>
					<div class="dCel1">
						<div
							style="font-size: 16px; font-weight: bold; font-family: Arial;">Care
							Go Logistics Pvt. Ltd.</div>
						<div style="font-size: 12px">Regd. Office: S-405, Greater
							Kailash, Part-II,New Delhi-110048</div>
						<div style="font-size: 12px">Phone:{{ selBrh.branchPhone }}</div>
						<div style="font-size: 12px">Email: gurgaon@carego.co</div>
					</div>
				</div> -->
				<div class="dRow1">
					<div class="mydCel01 bodbl" style="width: 1018px;">
						<div ng-show="bill.bill.blType == 'N'">TAX INVOICE</div>
						<div ng-show="bill.bill.blType == 'S'">SUPPLEMENTARY BILLS</div>
					</div>
					<!-- <div class="mydCel03 bodbl" style="width: 250px;">Co CIN No.: U60231DL2015PTC279265</div>
					<div class="mydCel03 bodbl" style="width: 250px;">SAC No.: {{bill.cust.custSAC}}</div> -->
				</div>
					<div class="dRow1">
					<div class="mydCel01 bodbl" style="width: 1018px;">
					<!-- 	<div ng-show="bill.bill.blType == 'N'">TAX INVOICE</div>
						<div ng-show="bill.bill.blType == 'S'">SUPPLEMENTARY BILLS</div> -->
						<u>All Disputes Subject to Delhi Jurisdiction</u>
					</div>
					<!-- <div class="mydCel03 bodbl" style="width: 250px;">Co CIN No.: U60231DL2015PTC279265</div>
					<div class="mydCel03 bodbl" style="width: 250px;">SAC No.: {{bill.cust.custSAC}}</div> -->
				</div>
				
						<div class="remRow1" style="width: 1027px;">
					<div class="remCel30" style="height: 100px;">
						<div class="remCel32 rembold1bl"
							style="text-align:center; ">
							Care Go Logistics Pvt. Ltd.
							<br>
							Regd. Office: S-405, Greater Kailash, Part-II,New Delhi-110048 <br>
							Email Id:  gurgaon@carego.co <br>
							MSME Regd. No.: HR05E0000104<br>
				
							Co CIN No. U6023DL2015PTC279265 <br> 
							SAC Code: {{bill.cust.custSAC}}, PAN No: AAGCC0032K 
</div>
<!-- 						<div class="remCel32 rembold1bl"
							style="text-align: right; margin-top: 50px; margin-left: -250px;">Checked
							By</div>
						<div class="remCel32 rembold1bl"
							style="text-align: right; margin-top: -14px;">Bill Incharge</div> -->
					</div>
				</div>


				<div class="dRow1">
					<div class="dCel04 bodbl" style="border-right: 0px;">
						<div class="dCel04 bodbl" style="border: 0px; text-align: left;">To,
							</div>

						<div class="dCel04 bodbl"
							style="border: 0px; margin-top: -15px; text-align: left;">
							<br>{{bill.cust.custName}} <br>{{bill.cust.custAdd}},<br>{{bill.cust.custCity}}
							{{bill.cust.custPin}} <br>


						</div>
						<p style="text-align: left; margin-left: 5px;">GST NO:
							{{bill.bill.blGstNo}}</p>

					</div>
					<div class="dCel066 bodbl" style="border-left: 0px;">
						<div class="dCel06 bodbl">
							<div style="margin-top: 2px; text-align: center;">
								<br> BILL NO:- {{bill.bill.blBillNo}}
							</div>
							<br>
                             <br>
							<div style="height: 27px;">
								BILL DATE:-{{bill.bill.blBillDt | date:'dd/MM/yyyy'}} <br>

							</div>
						</div>
					</div>
				</div>

				<div class="dRow1">
					<div class="mydCel25 bodbl" style="width: 1018px;"><!-- We Hereby Submit Our Invoice For
						The Goods Transported as Under: --> &nbsp;</div>
<!-- 					<div class="mydCel26 bodbl" style="width: 250px;">OUR PAN NO: AAGCC0032K</div>
					<div class="mydCel26 bodbl" style="width: 250px;">&nbsp;</div> -->
				</div>

				<div class="dRow1" style="width: 1027px;">

					<div class="CSSTabGeneratorss">
						<table>
							<tr>
							
								<td >SL<br>NO<br></td>
								<td >BILL NO<br></td>
							    <td >BILL DATE<br></td>
								<td >LR No.<br></td>
								<td >LR Date<br></td>
								<td >Weight<br></td>
								<td >TRUCK No.<br></td>
								<td >Invoice No.<br></td>
								<td >From<br></td>
								<td >To<br></td>
					<!-- 			<td rowspan="2">Charge Weight</td> -->
							<!-- 	<td rowspan="2">Rate</td> -->
								<td >Freight<br></td>
								<td>Unl. Chg.<br></td>
								<td >Factory Halt<br></td>
								<td >Unload Point<br>Halt<br></td>
							<!-- 	<td rowspan="2">Bonus(C)</td>
								<td rowspan="2">Ldng/Unldng<br> AMT.</td> -->
							<!-- 	<td rowspan="2">Two Point Col(E)</td> -->
								<td >Total Amount<br></td>
							</tr>
					 
							<tr ng-repeat="bilDet in bill.blDetList">
								<td>{{$index+1}}</td>
								<td>{{bill.bill.blBillNo | date:'dd/MM/yyyy'}}</td>
                             	<td>{{bill.bill.blBillDt | date:'dd/MM/yyyy'}}</td>
								<td>{{bilDet.cnmt.cnmtCode}}</td>
								<td>{{bilDet.cnmt.cnmtDt | date:'dd/MM/yyyy'}}</td>
								<td>{{bilDet.billDet.bdChgWt/1000 | number : 3}}
								<td>{{bilDet.truckNo}}</td>
								<td>{{bilDet.cnmt.cnmtInvoiceNo[0].invoiceNo}}<br>{{bilDet.cnmt.cnmtInvoiceNo[1].invoiceNo}}
								</td><td>{{bilDet.frmStn}}</td>
								<td>{{bilDet.toStn}}</td>
								<td>{{bilDet.billDet.bdFreight | number : 0}}</td>
								<td>{{bilDet.billDet.bdUnloadAmt | number : 2}}</td>
								<td>{{bilDet.billDet.bdDetAmt | number : 2}}</td>
                              <td>{{bilDet.billDet.bdUnloadDetAmt | number : 2}}</td>
                              <td>{{bilDet.billDet.bdTotAmt | number : 2}}</td>
							</tr>
							<tr>
						<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
						<td></td><td></td><td></td><td></td><td></td>
						<td><br>Total</td>
						<td><br>{{bill.bill.blFinalTot}}</td>
							</tr>
							<tr>
							<td  colspan="15" style="font-size: 15px; text-align: left;">Amount:- {{bill.bilAmt}}</td>
							</tr>
                      <tr>
<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
<td></td><td></td><td></td><td></td><td></td>
</tr>
						</table>

					</div>
				</div>


					<div class="remRow1" style="width: 1027px;">
					<div class="remCel30" style="height: 200px;">
						<div class="remCel32 rembold1bl"
							style="text-align: left;">
							"WE HEREBY CONFIRM AND DECLARE THAT:<br>
							1. The credit of Duty paid on inputs or capital goods used for providing this services has not been taken and<br>
							shall not be taken under the provision of Cevent Credit Rules 2004 and<br>
							2. We don't have available the benefit under the Notification No: 12/2003 dated 05/12/2006.<br>
							The above declaration is true and correct and in the event of lapes on our part, in honouring this declaration, we agreed to<br>
							 compensate M/s SKI CARBON BLACK(INDIA) PRIVATE LTD for all financial liablity that may arise because of working declaration if any" <br>
							 3. GST TO BE PAID BY CUSTOMER.
							</div>
						<div class="remCel32 rembold1bl"
							style="text-align: right; margin-top: 10px;">For Care Go
							Logistic Pvt. Ltd</div>
						<div class="remCel32 rembold1bl"
							style="text-align: right; margin-top: 50px; margin-left: -750px;">CHECK
							BY</div>
						<div class="remCel32 rembold1bl"
							style="text-align: right; margin-top: -14px;">Authorised Signatory</div>
					</div>
				</div>
			</div>
			
			<!--  chennai ski end -->


<!-- SKI Carbon mumbai start -->

<div ng-show="bill.SkiMUMBAIBillFormat">

	<div class="hRow1">
					<div class="dCel1">
						<img src="resources/img/logo.png" width="357" height="106" />
					</div>
					<div class="dCel1">
						<div
							style="font-size: 16px; font-weight: bold; font-family: Arial;">Care
							Go Logistics Pvt. Ltd.</div>
						<div style="font-size: 12px">Regd. Office: S-405, Greater
							Kailash, Part-II,New Delhi-110048</div>
<!-- 						<div style="font-size: 12px">Phone:{{ selBrh.branchPhone }}</div> -->
						<div style="font-size: 12px">MSME Regd. No.: HR05E0000104</div>
						<div style="font-size: 12px">Email: gurgaon@carego.co</div>
					</div>
				</div>
				<div class="dRow1">
					<div class="mydCel01 bodbl" style="width: 418px;">
						<div ng-show="bill.bill.blType == 'N'">FRIEGHT BILLS</div>
						<div ng-show="bill.bill.blType == 'S'">SUPPLEMENTARY BILLS</div>
					</div>
					<div class="mydCel03 bodbl" style="width: 300px;">Co CIN No.: U60231DL2015PTC279265</div>
					<div class="mydCel03 bodbl" style="width: 300px;">Service Tax Registration No.: AAGCC0032KSD001</div>
				</div>


				<div class="dRow1">
					<div class="dCel04 bodbl">
						<div class="dCel04 bodbl" style="border: 0px; text-align: left;">To,
							M/s</div>

						<div class="dCel04 bodbl"
							style="border: 0px; margin-top: -15px; text-align: left;">
							<br>{{bill.cust.custName}} <br>{{bill.cust.custAdd}},<br>{{bill.cust.custCity}}
							{{bill.cust.custPin}} <br>


						</div>
						<p style="text-align: left; margin-left: 5px;">GST NO:
							{{bill.bill.blGstNo}}</p>

					</div>
					<div class="dCel066 bodbl">
						<div class="dCel06 bodbl">
							<div style="margin-top: 2px; text-align: center;">
								<br> BILL NO.: {{bill.bill.blBillNo}}
							</div>
							<br>
                             <br>
							<div style="height: 27px;">
								DATED: {{bill.bill.blBillDt | date:'dd/MM/yyyy'}} <br>

							</div>
						</div>
					</div>
				</div>

				<div class="dRow1">
					<div class="mydCel25 bodbl" style="width: 418px;">We Hereby Submit Our Fright Bill For
						The Goods Transported as Under:</div>
					<div class="mydCel26 bodbl" style="width: 300px;">OUR PAN NO: AAGCC0032K</div>
					<div class="mydCel26 bodbl" style="width: 300px;">NO TDS To Be Deducted w.e.f 01/10/2009</div>
				</div>

				<div class="dRow1">

					<div class="CSSTabGeneratorss">
						<table>
							<tr>
								<td width="300px" colspan="4">CONSIGNMENT DETAILS</td>
								<td rowspan="2">MATERIAL</td>
					            <td rowspan="2">BILLED<br> QTY</td>
								<td rowspan="2">LOCATION</td>
								<td rowspan="2">TRUCK NO</td>
								<td rowspan="2">TRANSPORTER NAME</td>
								<td rowspan="2">Freight AMT</td>
								<td rowspan="2">REPORTING<br>DATE</td>
								<td rowspan="2">UNLOADING<br>DATE</td>
								<td rowspan="2">DETENTION</td>
								<td rowspan="2">WARAI</td>
								<td rowspan="2">OTHERS</td>
								<td rowspan="2">Total</td>
							</tr>
							<tr>
								<td><strong class="bbl">BILLING DATE</strong></td>
								
								<td><strong class="bbl">LR No.</strong></td>
								<td><strong class="bbl">INVOICE NO.</strong></td>
									<td><strong class="bbl">CUSTOMER NAME</strong></td>
						<!-- 		<td><strong class="bbl">From Station</strong></td>
								<td><strong class="bbl">To Station</strong></td>
									<td><strong class="bbl">Lorry No.</strong></td> -->
							
							</tr>
							<tr ng-repeat="bilDet in bill.blDetList">

								<td>{{bilDet.cnmt.cnmtDt | date:'dd/MM/yyyy'}}</td>
								
								<td>{{bilDet.cnmt.cnmtCode}}</td>
							<td>{{bilDet.cnmt.cnmtInvoiceNo[0].invoiceNo}}<br>{{bilDet.cnmt.cnmtInvoiceNo[1].invoiceNo}}
								<td>{{bilDet.consignee.custName}}</td>
								<td>{{bilDet.cnmt.cnmtProductType}}</td>
								<td>{{bilDet.cnmt.cnmtNoOfPkg}}</td>
								<td>{{bilDet.toStn}}</td>
									<td>{{bilDet.truckNo}}</td>
								
								</td>
								<td>CARE GO LOGISTICS</td>
								<!-- <td>{{bilDet.billDet.bdChgWt/1000 | number : 3}}</td> -->
							<!-- 	<td>{{bilDet.billDet.bdRate * 1000 | number : 2}}</td> -->
								<td>{{bilDet.billDet.bdFreight | number : 2}}</td>
								<td>{{ bilDet.delDt | date:'dd.MM.yyyy' }}</td>
								<td>{{bilDet.arDt | date:'dd.MM.yyyy' }}</td>
								<td>{{bilDet.billDet.bdDetAmt | number : 2}}</td>
								 <td>{{bilDet.billDet.bdUnloadAmt | number : 2}}</td> 
								<td>{{bilDet.billDet.bdOthChgAmt | number : 2}}</td>
								<td>{{bilDet.billDet.bdTotAmt | number : 2}}</td>

							</tr>
							<tr>
						<!-- 	<td>Bank Name</td>
							<td>A/c No.</td>
							<td>IFSC CODE</td> -->
							<td colspan="14" style="text-align: left;">In Words: {{bill.bilAmt}} </td>
						<td rowspan="2"><br>SUB TOTAL</td>
						<td rowspan="2"><br>{{bill.bill.blFinalTot}}</td>
							</tr>
							<tr>
							<td colspan="14" style="text-align: left;">WE HAVE NOT AVAILED ANY BENEFIT UNDER THE NOTIFICATION NO. 12/2003- SERVICE TAX DATED 20/06/2008<br><br></td>
							
							</tr>
                      <tr>
                      <td style="width: 100px;">Bank Name</td>
                      <td style="width: 100px;">A/c No.</td>
                      <td style="width: 100px;">IFSC CODE</td>
                      <td colspan="11">Branch</td>
                      <td></td><td></td>
                      </tr>
                         <tr>
                      <td style="width: 100px;"><br>Kotak Mahindra Bank<br><br></td>
                      <td style="width: 100px;"><br>3011623336<br><br></td>
                      <td style="width: 100px;"><br>KKBK0000299<br><br></td>
                      <td colspan="11"><br>61, Old Judicial Complex, Sector-15, Gurgaon 122001<br><br></td>
                      <td></td><td></td>
                      </tr>
						</table>

					</div>
				</div>


				<!-- 	<div class="dRow1">
					<div class="del16">
						<div class="del17 bod2bl">IT IS HEREBY DECLARE THAT WE ARE
							NOT AVAILING CENVAT CREDIT FACILITY IN RESPECT OF INPUTS/SERVICES
							/CAPITAL GOODS</div>
						<div class="del17 bod2bl">WE HAVE NOT AVAILED ANY BENEFIT
							UNDER THE NOTIFICATION NO. 12/2003-SERVICE TAX DATED 20/06/2008</div>
					</div>




					<div class="dCel15 bod2bl" style="text-align: center;">
						<div class="dCel14 bod1bl pdingbl">SUB TOTAL</div>
						<div class="dCel14 bod1bl pdingbl">{{bill.bill.blSubTot}}</div>
					</div>

				</div> -->


				<!-- <div class="dRow1">
					<div class="dCel18 bod1bl">Bank Name</div>
					<div class="dCel00 bod1bl">A/c No.</div>
					<div class="dCel18 bod1bl">IFSC CODE</div>
					<div class="dCel05 bod1bl">Branch</div>
					<div class="dCel19 bod2bl">Taxable Service tax= 30%</div>
					<div class="dCel14 bod2bl"
						style="text-align: right; padding-right: 5px;">{{bill.bill.blTaxableSerTax}}</div>
					<div class="dCel14 bod2bl">&nbsp</div>
				</div> -->

				<!-- <div class="dRow1">
					<div class="dCel18 bod1bl pding1bl">Kotak Mahindra Bank</div>
					<div class="dCel00 bod1bl pding1bl">3011623336</div>
					<div class="dCel18 bod1bl pding1bl">KKBK0000299</div>
					<div class="dCel05 bod1bl pdingbl">
						61, Old Judicial Complex,<br> Sector-15, Gurgaon 122001
					</div>
					<div class="dCel19 bod2bl pdng">
						Service Tax= 14%<br>Swachh Bharat Cess= 0.5%<br>Kisan kalyan Cess=0.5%</div>
					<div class="dCel14 bod2bl pdng" style="text-align: right;">
						<span>{{bill.bill.blSerTax}}</span><br>
						<span>{{bill.bill.blSwachBhCess}}</span><br>
						<span>{{bill.bill.blKisanKalCess}}</span>
					</div>
					<div class="dCel14 bod2bl pding1bl">&nbsp</div>
				</div> -->

			<!-- 	<div class="dRow1">
					<div class="mydCel20 bod2bl pdingbl">
						Amount in words: &nbsp;&nbsp;&nbsp;&nbsp; <span class="bbl">{{bill.bilAmt}}</span>
						<hr>
						<br> Only By A/c Payee Cheque / DD in Favour Of <span
							class="bbl">Care Go Logistics Pvt. Ltd.,</span><span> or
							Electronic Fund transfer to below Bank A/c </span> <br>

					</div>

					<div class="dRow1">

						<div class="dCel20 bod1bl pding2bl">Pay {{bill.bilAmt}}</div>
						<div class="mydCel14 bod1bl pding2bl" style="text-align: right;">
							<br>Total<br>
						</div>
						<div class="mydCel14 bod1bl pding2bl">
							<br>{{bill.bill.blFinalTot}}<br>
						</div>
					</div>

					<div class="dRow1">
						<div class="dCel28 bod2bl" style="text-align: center;">
							<div class="dCel31 bod1bl">Bank Details</div>
							<div class="dCel27 bod1bl">Bank Name</div>
							<div class="dCel27 bod1">A/c No.</div>
							<div class="dCel27 bod1bl">IFSC CODE</div>
							<div class="dCel29 bod1bl">Branch</div>
							<div class="dCel27 bod1bl pding1bl">HDFC BANK</div>
							<div class="dCel27 bod1bl pding1bl">13810330000037</div>
							<div class="dCel27 bod1bl pding1bl">HDFC0001381</div>
							<div class="dCel29 bod1bl pdingbl">472-11, OLD RAILWAY
								ROAD, DHOBI GHAT, NEAR KABIR BHAWAN, GURGAON</div>
							<div class="dCel27 bod1bl pding1bl">ICICI BANK</div>
							<div class="dCel27 bod1bl pding1bl">103105001831</div>
							<div class="dCel27 bod1bl pding1bl">ICIC0001031</div>
							<div class="dCel29 bod1bl pdingbl">SCO - 59, 60, OLD
								JUDICIAL COMPLEX, SECTOR - 15, GURGAON</div>
							<div class="dCel27 bod1bl pding1bl">STATE BANK OF INDIA</div>
							<div class="dCel27 bod1bl pding1bl">35334334071</div>
							<div class="dCel27 bod1bl pding1bl">SBIN0060414</div>
							<div class="dCel29 bod1bl pdingbl">
								SCO - 5,6,7 SECTOR - 15, <br>PART - 2, GURGAON
							</div>
						</div>
						<div class="dCel30" style="height: 180px;">
							<div class="dCel32 bod1bl"
								style="text-align: right; margin-top: 10px; margin-left: -10px;">For
								Care Go Logistic Pvt. Ltd</div>
							<div class="dCel32 bod1bl"
								style="text-align: left; margin-top: 125px; margin-left: 10px;">Cheked
								By</div>
							<div class="dCel32 bod1bl"
								style="text-align: right; margin-top: -18px; margin-left: -10px;">Cheked
								By</div>
						</div>
					</div>

					<div class="dRow1">
						<div class="dCel21 bod1bl pding3bl" style="text-align: left">
							As per notification No. 5/2017. Central Tax dated 19.6.2017, GTA
							are exempted under GST and it is payable by consignor/consignee
							under CM @ 5% Rupees &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{bill.cGST +
							bill.sGST + bill.iGST}} <br> We declare that no ITC availed
							by us on inputs and inputs services used for providing GTA
							Services.
						</div>
					</div>
				</div> -->
					<div class="remRow1">
					<div class="remCel30" style="height: 100px;">
						<div class="remCel32 rembold1bl"
							style="text-align: right; margin-top: 10px;">
							&nbsp;</div>
						<div class="remCel32 rembold1bl"
							style="text-align: right; margin-top: 50px; margin-left: -250px;">Checked
							By</div>
						<div class="remCel32 rembold1bl"
							style="text-align: right; margin-top: -14px;">Bill Incharge</div>
					</div>
				</div>

			</div>

<!-- End SKI carbon mumbai -->

<!--  start kwality  -->
<div ng-show="bill.kwalityBillFormat">
				<div class="hRow1">
					<div class="dCel1">
						<img src="resources/img/logo.png" width="357" height="106" />
					</div>
					<div class="dCel1">
						<div
							style="font-size: 16px; font-weight: bold; font-family: Arial;">Care
							Go Logistics Pvt. Ltd.</div>
						<div style="font-size: 12px">Regd. Office: S-405, Greater
							Kailash, Part-II,New Delhi-110048</div>
<!-- 						<div style="font-size: 12px">Phone:{{ selBrh.branchPhone }}</div> -->
						<div style="font-size: 12px">MSME Regd. No.: HR05E0000104</div>
						<div style="font-size: 12px">Email: gurgaon@carego.co</div>
					</div>
				</div>
				<div class="dRow1">
					<div class="mydCel01 bodbl" style="width: 518px;">
						<div ng-show="bill.bill.blType == 'N'">Invoice</div>
						<div ng-show="bill.bill.blType == 'S'">SUPPLEMENTARY BILLS</div>
					</div>

					<div class="mydCel03 bodbl" style="width: 250px;">Co CIN No.: U6023DL2015PTC279265
						</div>
					<div class="mydCel03 bodbl" style="width: 250px;">SAC No.:
						{{bill.cust.custSAC}}</div>
				</div>


				<div class="dRow1">
					<div class="dCel04 bodbl" style="width: 768px;">
						<div class="dCel04 bodbl" style="border: 0px; text-align: left;">To,
							M/s</div>

						<div class="dCel04 bodbl"
							style="border: 0px; margin-top: -15px; margin-left: 100px; text-align: justify;">
							{{bill.cust.custName}} <br>{{bill.cust.custAdd}},<br>{{bill.cust.custCity}}
							{{bill.cust.custPin}} <br>
							<p style="text-align: left; margin-left: 5px;">GST NO:
								{{bill.bill.blGstNo}}</p>

						</div>


					</div>
					<div class="dCel066 bodbl" style="width: 250px;">
						<div class="dCel06 bodbl"
							style="width: 250px; border: 1px solid black; height: 80px;">
							<!-- 	<div style="margin-top: 2px; text-align: center; border: 1px solid black;">
							<br>
							Invoice NO.:
								{{bill.bill.blBillNo}}</div>
							<br>
							
							<div style="height: 27px;">DATE: {{bill.bill.blBillDt | date:'dd/MM/yyyy'}}
							<br>
							
							</div>  -->
							<div
								style="width: 70px; height: 80px; text-align: center; border: 1px solid black;">
								<br> <br> BILL NO. <br> DATED

							</div>
							<div
								style="position: absolute; height: 55px; margin-top: -80px; margin-left: 70px; border: 1px solid black; width: 110px; text-align: center; float: left; display: table-column;">
								<br> <br> {{bill.bill.blBillNo}}

							</div>
							<div
								style="position: absolute; height: 25px; margin-top: -28px; margin-left: 70px; border: 1px solid black; width: 110px; text-align: center; float: left; display: table-column;">

								{{bill.bill.blBillDt | date:'dd/MM/yyyy'}}</div>
							<div
								style="position: absolute; height: 27px; margin-top: -80px; margin-left: 180px; border: 1px solid black; width: 70px; text-align: center; float: left; display: table-column;">

								&nbsp;</div>
							<div
								style="position: absolute; height: 40px; margin-top: -55px; margin-left: 180px; border: 1px solid black; width: 70px; text-align: center; float: left; display: table-column;">

								&nbsp;</div>
							<div
								style="position: absolute; height: 37px; margin-top: -35px; margin-left: 180px; border: 1px solid black; width: 70px; text-align: center; float: left; display: table-column;">

								&nbsp;</div>

							<!-- <table style="border: 1px solid black; border-collapse: collapse; width: 100%">
							<tr><td rowspan="2"><br>BILL NO.</td><td rowspan="2"><br>{{bill.bill.blBillNo}}</td><td>&nbsp;</td></tr>
							<tr><td>&nbsp;</td></tr>
							<tr><td>Dated</td><td>{{bill.bill.blBillDt | date:'dd/MM/yyyy'}}</td><td></td></tr>
							</table> -->
						</div>
					</div>
				</div>

				<!-- 			<div class="dRow1">
					<div class="mydCel25 bodbl">We Hereby Submit Our Invoice For The
						Goods Transported as Under:</div>
					<div class="mydCel26 bodbl">OUR PAN NO: AAGCC0032K</div>
					
				</div> -->

				<div class="dRow1">
					<div class="mydCel01 bodbl" style="width: 518px;">We hereby
						our Frieght Bill for the goods Transported as Under</div>

					<div class="mydCel03 bodbl" style="width: 250px;">OUR PAN NO.
						AAGCC0032K</div>
					<div class="mydCel03 bodbl" style="width: 250px;">&nbsp;</div>
				</div>

				<div class="dRow1">

					<div class="CSSTabGeneratorss">
						<table>
							<tr>
								<td width="300px" colspan="5">CONSIGNMENT DETAILS</td>
								<td rowspan="2">Weight<br> MT
								</td>
								<!-- <td rowspan="2">Charge Weight</td> -->
								<td rowspan="2">Rate</td>
								<td rowspan="2">Freight<br>(A)
								</td>
								<td rowspan="2">Detention(B)</td>
								<td rowspan="2">EXTRA<br>(C)
								</td>
								<td rowspan="2">UNLOADING</td>
								<!--   	<td rowspan="2">Two Point Col(E)</td> -->
								<td rowspan="2">Total<br>(A+B+C+D+E)
								</td>
							</tr>
							<tr>
								<td><strong class="bbl">Date</strong></td>
								<td><strong class="bbl">INVOICE NO.</strong></td>
								<td><strong class="bbl">LR No.</strong></td>
								<!-- <td><strong class="bbl">Lorry No.</strong></td> -->
								<td><strong class="bbl">From Station</strong></td>
								<td><strong class="bbl">To Station</strong></td>
							</tr>
							<tr ng-repeat="bilDet in bill.blDetList">

								<td>{{bilDet.cnmt.cnmtDt | date:'dd/MM/yyyy'}}</td>
								<td>{{bilDet.cnmt.cnmtInvoiceNo[0].invoiceNo}}<br>{{bilDet.cnmt.cnmtInvoiceNo[1].invoiceNo}}
								</td>
								<td>{{bilDet.cnmt.cnmtCode}}</td>
								<!--    	<td>{{bilDet.truckNo}}</td> -->
								<td>{{bilDet.frmStn}}</td>
								<td>{{bilDet.toStn}}</td>
<!-- 								<td>{{bilDet.billDet.bdActWt/1000 | number : 3}}</td> -->
								<td>{{bilDet.billDet.bdChgWt/1000 | number : 3}}</td>
								<td>{{bilDet.billDet.bdRate * 1000 | number : 2}}</td>
								<td>{{bilDet.billDet.bdFreight | number : 2}}</td>
								<td>{{bilDet.billDet.bdDetAmt | number : 2}}</td>
								<td>{{bilDet.billDet.bdOthChgAmt + bilDet.billDet.bdBonusAmt + bilDet.billDet.bdLoadAmt| number : 2}}</td>
								<td>{{bilDet.billDet.bdUnloadAmt | number : 2}}</td>
								<td>{{bilDet.billDet.bdTotAmt | number : 2}}</td>

							</tr>

						</table>

					</div>
				</div>


				<!-- 	<div class="dRow1">
					<div class="del16">
						<div class="del17 bod2bl">IT IS HEREBY DECLARE THAT WE ARE
							NOT AVAILING CENVAT CREDIT FACILITY IN RESPECT OF INPUTS/SERVICES
							/CAPITAL GOODS</div>
						<div class="del17 bod2bl">WE HAVE NOT AVAILED ANY BENEFIT
							UNDER THE NOTIFICATION NO. 12/2003-SERVICE TAX DATED 20/06/2008</div>
					</div>




					<div class="dCel15 bod2bl" style="text-align: center;">
						<div class="dCel14 bod1bl pdingbl">SUB TOTAL</div>
						<div class="dCel14 bod1bl pdingbl">{{bill.bill.blSubTot}}</div>
					</div>

				</div> -->


				<!-- <div class="dRow1">
					<div class="dCel18 bod1bl">Bank Name</div>
					<div class="dCel00 bod1bl">A/c No.</div>
					<div class="dCel18 bod1bl">IFSC CODE</div>
					<div class="dCel05 bod1bl">Branch</div>
					<div class="dCel19 bod2bl">Taxable Service tax= 30%</div>
					<div class="dCel14 bod2bl"
						style="text-align: right; padding-right: 5px;">{{bill.bill.blTaxableSerTax}}</div>
					<div class="dCel14 bod2bl">&nbsp</div>
				</div> -->

				<!-- <div class="dRow1">
					<div class="dCel18 bod1bl pding1bl">Kotak Mahindra Bank</div>
					<div class="dCel00 bod1bl pding1bl">3011623336</div>
					<div class="dCel18 bod1bl pding1bl">KKBK0000299</div>
					<div class="dCel05 bod1bl pdingbl">
						61, Old Judicial Complex,<br> Sector-15, Gurgaon 122001
					</div>
					<div class="dCel19 bod2bl pdng">
						Service Tax= 14%<br>Swachh Bharat Cess= 0.5%<br>Kisan kalyan Cess=0.5%</div>
					<div class="dCel14 bod2bl pdng" style="text-align: right;">
						<span>{{bill.bill.blSerTax}}</span><br>
						<span>{{bill.bill.blSwachBhCess}}</span><br>
						<span>{{bill.bill.blKisanKalCess}}</span>
					</div>
					<div class="dCel14 bod2bl pding1bl">&nbsp</div>
				</div> -->

				<div class="dRow1">
					<div class="mydCel20 bod2bl pdingbl">
						<!-- 						 Amount in words: &nbsp;&nbsp;&nbsp;&nbsp; <span class="bbl">{{bill.bilAmt}}</span> -->
						IT IS HEREBY DECLARE THAT WE ARE NOT AVAILING CENVAT FACILITY IN
						RESPECT OF INPUTS SERVICES CAPITAL GOODS
						<hr>
						<br>
						<!--  Only By A/c Payee Cheque / DD in Favour Of <span class="bbl">Care Go Logistics Pvt. Ltd.,</span><span> or Electronic Fund transfer to below Bank A/c </span> -->
						&nbsp; <br>

					</div>

					<div class="dRow1">

						<!-- <div class="dCel20 bod1bl pding2bl">Pay {{bill.bilAmt}}</div> -->
						<div class="mydCel14 bod1bl pding2bl" style="text-align: right;">
							<br>SUB Total<br>
						</div>
						<div class="mydCel14 bod1bl pding2bl">
							<br>{{bill.bill.blFinalTot}}<br>
						</div>
					</div>
				</div>


				<div class="dRow1">

					<div class="CSSTabGeneratorss">
						<table>
							<tr>
								<!-- <td width="300px" colspan="5">CONSIGNMENT DETAILS</td> -->
								<td>Bank Name</td>
								<!-- <td rowspan="2">Charge Weight</td> -->
								<td style="width: 70px;">&nbsp;</td>
								<td>A/c NO.</td>
								<td>IFSC CODE</td>
								<td>Branch</td>
								<td>Taxable Service tax=</td>
								<!--   	<td rowspan="2">Two Point Col(E)</td> -->
								<td style="width: 70px;">&nbsp;</td>
								<td style="width: 70px;">&nbsp;</td>
							</tr>
							<!-- 	<tr>
								<td><strong class="bbl">Date</strong></td>
								<td><strong class="bbl">INVOICE NO.</strong></td>
								<td><strong class="bbl">LR No.</strong></td>
								<td><strong class="bbl">Lorry No.</strong></td>
								<td><strong class="bbl">From Station</strong></td>
								<td><strong class="bbl">To Station</strong></td>								   
							</tr> -->
							<tr>

								<td>Kotak Mahindra Bank</td>
								<td></td>
								<td>3011623336</td>
								<!--    	<td>{{bilDet.truckNo}}</td> -->
								<td>KKBK0000299</td>
								<td>61, Old Judicial Complex</td>
								<td rowspan="2">Service Tax= <br> Swachh Bharat Cess=
								</td>
								<!-- <td>{{bilDet.billDet.bdChgWt/1000 | number : 3}}</td> -->
								<td></td>
								<td rowspan="3"></td>

							</tr>
							<tr>

								<td></td>
								<td></td>
								<td></td>
								<!--    	<td>{{bilDet.truckNo}}</td> -->
								<td></td>
								<td>Gurgaon</td>

								<!-- <td>{{bilDet.billDet.bdChgWt/1000 | number : 3}}</td> -->
								<td></td>


							</tr>
							<tr>

								<td colspan="5"></td>

								<td>Krishi Kalyan Cess=</td>

								<td></td>


							</tr>

							<tr>
								<td rowspan="2" colspan="4" style="text-align: left;">Only
									By Cheque / DD in Favour of Care Go Logistics Pvt. Ltd. or<br>
									Electronic Fund transfer to above Kotak Bank A/C
								</td>
								<td rowspan="2" colspan="2"><u>Pay {{bill.bilAmt}} </u></td>
								<td></td>
								<td rowspan="2"><br> {{bill.bill.blFinalTot}}</td>
							</tr>
							<tr>
								<td></td>
							</tr>

						</table>

					</div>
				</div>

				<!-- <div class="dRow1">
					<div class="dCel28 bod2bl" style="text-align: center;">
						<div class="dCel31 bod1bl"> Bank Details</div>
						<div class="dCel27 bod1bl">Bank Name</div>
						<div class="dCel27 bod1">A/c No.</div>
						<div class="dCel27 bod1bl">IFSC CODE</div>
						<div class="dCel29 bod1bl">Branch</div>
						<div class="dCel27 bod1bl pding1bl">HDFC BANK</div>
						<div class="dCel27 bod1bl pding1bl">13810330000037</div>
						<div class="dCel27 bod1bl pding1bl">HDFC0001381</div>
						<div class="dCel29 bod1bl pdingbl">472-11, OLD RAILWAY ROAD,
							DHOBI GHAT, NEAR KABIR BHAWAN, GURGAON</div>
						<div class="dCel27 bod1bl pding1bl">ICICI BANK</div>
						<div class="dCel27 bod1bl pding1bl">103105001831</div>
						<div class="dCel27 bod1bl pding1bl">ICIC0001031</div>
						<div class="dCel29 bod1bl pdingbl">SCO - 59, 60, OLD
							JUDICIAL COMPLEX, SECTOR - 15, GURGAON</div>
						<div class="dCel27 bod1bl pding1bl">STATE BANK OF INDIA</div>
						<div class="dCel27 bod1bl pding1bl">35334334071</div>
						<div class="dCel27 bod1bl pding1bl">SBIN0060414</div>
						<div class="dCel29 bod1bl pdingbl">
							SCO - 5,6,7 SECTOR - 15, <br>PART - 2, GURGAON
						</div>
					</div>
					<div class="dCel30" style="height: 180px;">
						<div class="dCel32 bod1bl"
							style="text-align: right; margin-top: 10px; margin-left: -10px;">For
							Care Go Logistic Pvt. Ltd</div>
						<div class="dCel32 bod1bl"
							style="text-align: left; margin-top: 125px; margin-left: 10px;">Cheked
							By</div>
						<div class="dCel32 bod1bl"
							style="text-align: right; margin-top: -18px; margin-left: -10px;">Cheked
							By</div>
					</div>
				</div> -->

				<div class="dRow1">

					<div class="dCel21 bod1bl pding3bl"
						style="text-align: left; border-bottom-width: 0px;">
						<br> <br>
						<div
							style="border: 1px solid black; margin-left: 600px; width: 6%">&nbsp;</div>
						<br>


					</div>
					<div style="float: left; width: 60px; border: 1px solid black;">&nbsp;</div>
					<div
						style="float: left; font-size: 15px; width: 65%; border: 1px solid black;">We
						declare that no ITC availed by us on inputs and inputs services
						used for providing GTA Services</div>
					<div
						style="float: left; width: 290px; border-right: 1px solid black;">&nbsp;</div>
					<div
						style="position: absolute; width: 69%; margin-top: 101px; border: 1px solid black;">
						<span style="margin-left: 62px;">GST IS PAYABLE BY
							CONSIGNOR/CONSIGNEE UNDER RCV @ 5% RUPEES {{bill.cGST + bill.sGST + bill.iGST}}</span>
					</div>
					<div
						style="float: left; position: absolute; margin-top: 101px; margin-left: 662px; width: 356px; border: 1px solid black; border-top-width: 0px;">
						<span style="margin-left: 80px;">Checked By</span><span
							style="margin-left: 100px;">Bill Incharge</span>
					</div>
				</div>


				<!-- 	<div class="dRow1">
				<div style="float:left; width:60px; border:1px solid black;">&nbsp;</div>
<div style="font-size:15px; width:65%; border:1px solid black;">We declare that no ITC availed by us on inputs and inputs services used for providing GTA Services</div>
				<div style="float:left; width:290px; border-right:1px solid black;">&nbsp;</div>
				</div> -->

				<!--  </div> -->



			</div>

<!--  end kwality -->

<!-- damani bill format satart -->

<div ng-show="bill.damanishipBillFormat">
				<div class="hRow1">
					<div class="dCel1">
						<img src="resources/img/logo.png" width="357" height="106" />
					</div>
					<div class="dCel1">
						<div
							style="font-size: 16px; font-weight: bold; font-family: Arial;">Care
							Go Logistics Pvt. Ltd.</div>
						<div style="font-size: 12px">Regd. Office: S-405, Greater
							Kailash, Part-II,New Delhi-110048</div>
<!-- 						<div style="font-size: 12px">Phone:{{ selBrh.branchPhone }}</div> -->
						<div style="font-size: 12px">MSME Regd. No.: HR05E0000104</div>
						<div style="font-size: 12px">Email: gurgaon@carego.co</div>
						<div style="font-size: 12px">Co CIN No.
							U60231DL2015PTC279265</div>
					</div>
				</div>
				<div class="dRow1">
					<div class="mydCel01 bodbl">
						<div ng-show="bill.bill.blType == 'N'">Invoice</div>
						<div ng-show="bill.bill.blType == 'S'">SUPPLEMENTARY BILLS</div>
					</div>

					<div class="mydCel03 bodbl">SAC No.: {{bill.cust.custSAC}}</div>
				</div>


				<div class="dRow1">
					<div class="dCel04 bodbl">
						<div class="dCel04 bodbl" style="border: 0px; text-align: left;">To,
							M/s</div>

						<div class="dCel04 bodbl"
							style="border: 0px; margin-top: -15px; margin-left: 100px; text-align: justify;">
							{{bill.cust.custName}} <br>{{bill.cust.custAdd}},<br>{{bill.cust.custCity}}
							{{bill.cust.custPin}} <br>


						</div>
						<p style="text-align: left; margin-left: 5px;">GSTIN NO:
							{{bill.bill.blGstNo}}</p>

					</div>
					<div class="dCel066 bodbl">
						<div class="dCel06 bodbl">
							<div style="margin-top: 2px; text-align: center;">
								<br> Invoice NO.: {{bill.bill.blBillNo}}
							</div>
							<br>

							<div style="height: 27px;">
								DATE: {{bill.bill.blBillDt | date:'dd/MM/yyyy'}} <br>

							</div>
						</div>
					</div>
				</div>

				<div class="dRow1">
					<div class="mydCel25 bodbl">We Hereby Submit Our Invoice For
						The Goods Transported as Under:</div>
					<div class="mydCel03 bodbl">OUR PAN NO: AAGCC0032K</div>

				</div>

				<div class="dRow1">

					<div class="CSSTabGeneratorss">
						<table>
							<tr>
								<td width="300px" colspan="6">CONSIGNMENT DETAILS</td>
								<td rowspan="2">Actual Weight</td>
								<td rowspan="2">Charge Weight</td>
								<td rowspan="2">Rate</td>
								<td rowspan="2">Freight</td>
								<td rowspan="2">Detention(B)</td>
								<td rowspan="2">Bonus(C)</td>
								<td rowspan="2">Ldng/Unldng(D)</td>
								<td rowspan="2">Two Point Col(E)</td>
								<td rowspan="2">Total(A+B+C+D+E)</td>
							</tr>
							<tr>
								<td><strong class="bbl">Date</strong></td>
								<td><strong class="bbl">Invoice No.</strong></td>
								<td><strong class="bbl">LR No.</strong></td>
								<td><strong class="bbl">Lorry No.</strong></td>
								<td><strong class="bbl">From Station</strong></td>
								<td><strong class="bbl">To Station</strong></td>
							</tr>
							<tr ng-repeat="bilDet in bill.blDetList">

								<td>{{bilDet.cnmt.cnmtDt | date:'dd/MM/yyyy'}}</td>
								<td>{{bilDet.cnmt.cnmtInvoiceNo[0].invoiceNo}}<br>{{bilDet.cnmt.cnmtInvoiceNo[1].invoiceNo}}
								</td>
								<td>{{bilDet.cnmt.cnmtCode}}</td>
								<td>{{bilDet.truckNo}}</td>
								<td>{{bilDet.frmStn}}</td>
								<td>{{bilDet.toStn}}</td>
								<td>{{bilDet.billDet.bdActWt/1000 | number : 3}}</td>
								<td>{{bilDet.billDet.bdChgWt/1000 | number : 3}}</td>
								<td>{{bilDet.billDet.bdRate * 1000 | number : 2}}</td>
								<td>{{bilDet.billDet.bdFreight | number : 2}}</td>
								<td>{{bilDet.billDet.bdDetAmt | number : 2}}</td>
								<td>{{bilDet.billDet.bdBonusAmt | number : 2}}</td>
								<td>{{bilDet.billDet.bdLoadAmt | number :
									2}}/{{bilDet.billDet.bdUnloadAmt | number : 2}}</td>
								<td>{{bilDet.billDet.bdOthChgAmt | number : 2}}</td>
								<td>{{bilDet.billDet.bdTotAmt | number : 2}}</td>

							</tr>

						</table>

					</div>
				</div>


				<!-- 	<div class="dRow1">
					<div class="del16">
						<div class="del17 bod2bl">IT IS HEREBY DECLARE THAT WE ARE
							NOT AVAILING CENVAT CREDIT FACILITY IN RESPECT OF INPUTS/SERVICES
							/CAPITAL GOODS</div>
						<div class="del17 bod2bl">WE HAVE NOT AVAILED ANY BENEFIT
							UNDER THE NOTIFICATION NO. 12/2003-SERVICE TAX DATED 20/06/2008</div>
					</div>




					<div class="dCel15 bod2bl" style="text-align: center;">
						<div class="dCel14 bod1bl pdingbl">SUB TOTAL</div>
						<div class="dCel14 bod1bl pdingbl">{{bill.bill.blSubTot}}</div>
					</div>

				</div> -->


				<!-- <div class="dRow1">
					<div class="dCel18 bod1bl">Bank Name</div>
					<div class="dCel00 bod1bl">A/c No.</div>
					<div class="dCel18 bod1bl">IFSC CODE</div>
					<div class="dCel05 bod1bl">Branch</div>
					<div class="dCel19 bod2bl">Taxable Service tax= 30%</div>
					<div class="dCel14 bod2bl"
						style="text-align: right; padding-right: 5px;">{{bill.bill.blTaxableSerTax}}</div>
					<div class="dCel14 bod2bl">&nbsp</div>
				</div> -->

				<!-- <div class="dRow1">
					<div class="dCel18 bod1bl pding1bl">Kotak Mahindra Bank</div>
					<div class="dCel00 bod1bl pding1bl">3011623336</div>
					<div class="dCel18 bod1bl pding1bl">KKBK0000299</div>
					<div class="dCel05 bod1bl pdingbl">
						61, Old Judicial Complex,<br> Sector-15, Gurgaon 122001
					</div>
					<div class="dCel19 bod2bl pdng">
						Service Tax= 14%<br>Swachh Bharat Cess= 0.5%<br>Kisan kalyan Cess=0.5%</div>
					<div class="dCel14 bod2bl pdng" style="text-align: right;">
						<span>{{bill.bill.blSerTax}}</span><br>
						<span>{{bill.bill.blSwachBhCess}}</span><br>
						<span>{{bill.bill.blKisanKalCess}}</span>
					</div>
					<div class="dCel14 bod2bl pding1bl">&nbsp</div>
				</div> -->

				<div class="dRow1">
					<div class="mydCel20 bod2bl pdingbl">
						Amount in words: &nbsp;&nbsp;&nbsp;&nbsp; <span class="bbl">{{bill.bilAmt}}</span>
						<hr>
						<br> Only By A/c Payee Cheque / DD in Favour Of <span
							class="bbl">Care Go Logistics Pvt. Ltd.,</span><span> or
							Electronic Fund transfer to below Bank A/c </span> <br>

					</div>

					<div class="dRow1">

						<!-- <div class="dCel20 bod1bl pding2bl">Pay {{bill.bilAmt}}</div> -->
						<div class="mydCel14 bod1bl pding2bl" style="text-align: right;">
							<br>Total<br>
						</div>
						<div class="mydCel14 bod1bl pding2bl">
							<br>{{bill.bill.blFinalTot}}<br>
						</div>
					</div>

					<div class="dRow1">
						<div class="dCel28 bod2bl"
							style="text-align: center; width: 647px;">
							<div class="dCel31 bod1bl" style="width: 647px;">Bank
								Details</div>
							<div class="dCel27 bod1bl" style="width: 165px;">Bank Name</div>
							<div class="dCel27 bod1" style="width: 100px;">A/c No.</div>
							<div class="dCel27 bod1bl" style="width: 100px;">IFSC CODE</div>
							<div class="dCel29 bod1bl" style="width: 282px;">Branch</div>
							<div class="dCel27 bod1bl pding1bl" style="width: 165px;"><br><br><br><br><br>KOTAK
								MAHINDRA BANK<br><br><br><br><br><br></div>
							<div class="dCel27 bod1bl pding1bl" style="width: 100px;"><br><br><br><br><br>3011623336<br><br><br><br><br><br></div>
							<div class="dCel27 bod1bl pding1bl" style="width: 100px;"><br><br><br><br><br>KKBK0000299<br><br><br><br><br><br></div>
							<div class="dCel29 bod1bl pdingbl" style="width: 282px;"><br><br><br><br><br>61,Old Judicial
								Complex,Sector-15,Gurgaon 122001<br><br><br><br><br><br><br></div>
							<!-- <div class="dCel27 bod1bl pding1bl" style="width: 165px;">HDFC
								BANK</div>
							<div class="dCel27 bod1bl pding1bl" style="width: 100px;">13810330000037</div>
							<div class="dCel27 bod1bl pding1bl" style="width: 100px;">HDFC0001381</div>
							<div class="dCel29 bod1bl pdingbl" style="width: 282px;">472-11, OLD RAILWAY
								ROAD, DHOBI GHAT, NEAR KABIR BHAWAN, GURGAON</div>
							<div class="dCel27 bod1bl pding1bl" style="width: 165px;">ICICI
								BANK</div>
							<div class="dCel27 bod1bl pding1bl" style="width: 100px;">103105001831</div>
							<div class="dCel27 bod1bl pding1bl" style="width: 100px;">ICIC0001031</div>
							<div class="dCel29 bod1bl pdingbl" style="width: 282px;">SCO - 59, 60, OLD
								JUDICIAL COMPLEX, SECTOR - 15, GURGAON</div>
							<div class="dCel27 bod1bl pding1bl" style="width: 165px;">STATE
								BANK OF INDIA</div>
							<div class="dCel27 bod1bl pding1bl" style="width: 100px;">35334334071</div>
							<div class="dCel27 bod1bl pding1bl" style="width: 100px;">SBIN0060414</div>
							<div class="dCel29 bod1bl pdingbl" style="width: 282px;">
								SCO - 5,6,7 SECTOR - 15, <br>PART - 2, GURGAON
							</div> -->
						</div>
						<div class="dCel30" style="height: 225px; width: 371px;">
							<div class="dCel32 bod1bl"
								style="text-align: right; margin-top: 20px; margin-left: -40px; width: 361px;">For
								Care Go Logistic Pvt. Ltd</div>
							<div class="dCel32 bod1bl"
								style="text-align: left; margin-top: 170px; margin-left: 10px; width: 361px;">Cheked
								By</div>
							<div class="dCel32 bod1bl"
								style="text-align: right; margin-top: -12px; margin-left: -10px; width: 361px;">Cheked
								By</div>
						</div>
					</div>

					<div class="dRow1">
						<div class="dCel21 bod1bl pding3bl" style="text-align: left">
							As per notification No. 5/2017. Central Tax dated 19.6.2017, GTA
							are exempted under GST and it is payable by consignor/consignee
							under CM @ 5% Rupees &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{bill.cGST +
							bill.sGST + bill.iGST}} <br> We declare that no ITC availed
							by us on inputs and inputs services used for providing GTA
							Services.
						</div>
					</div>
				</div>
			</div>

<!-- damani bill format end -->

<!-- hill ltd mumbai start  -->
<div ng-show="bill.HillLTDMUMBAIBillFormat">
				<div class="hRow1">
					<div class="dCel1">
						<img src="resources/img/logo.png" width="357" height="106" />
					</div>
					<div class="dCel1">
						<div
							style="font-size: 16px; font-weight: bold; font-family: Arial;">Care
							Go Logistics Pvt. Ltd.</div>
						<div style="font-size: 12px">Regd. Office: S-405, Greater
							Kailash, Part-II,New Delhi-110048</div>
<!-- 						<div style="font-size: 12px">Phone:{{ selBrh.branchPhone }}</div> -->
						<div style="font-size: 12px">MSME Regd. No.: HR05E0000104</div>
						<div style="font-size: 12px">Email: gurgaon@carego.co</div>
					</div>
				</div>
				<div class="dRow1">
					<div class="mydCel01 bodbl" style="width: 518px;">
						<div ng-show="bill.bill.blType == 'N'">TAX INVOICE</div>
						<div ng-show="bill.bill.blType == 'S'">SUPPLEMENTARY BILLS</div>
					</div>
					<div class="mydCel03 bodbl" style="width: 250px;">Co CIN No.: U60231DL2015PTC279265</div>
					<div class="mydCel03 bodbl" style="width: 250px;">SAC No.: {{bill.cust.custSAC}}</div>
				</div>


				<div class="dRow1">
					<div class="dCel04 bodbl">
						<div class="dCel04 bodbl" style="border: 0px; text-align: left;">To,
							M/s</div>

						<div class="dCel04 bodbl"
							style="border: 0px; margin-top: -15px; text-align: left;">
							<br>{{bill.cust.custName}} <br>{{bill.cust.custAdd}},<br>{{bill.cust.custCity}}
							{{bill.cust.custPin}} <br>


						</div>
						<p style="text-align: left; margin-left: 5px;">GSTIN NO:
							{{bill.bill.blGstNo}}</p>

					</div>
					<div class="dCel066 bodbl">
						<div class="dCel06 bodbl">
							<div style="margin-top: 2px; text-align: center;">
								<br> Invoice NO.: {{bill.bill.blBillNo}}
							</div>
							<br>
                             <br>
							<div style="height: 27px;">
								DATE: {{bill.bill.blBillDt | date:'dd/MM/yyyy'}} <br>

							</div>
						</div>
					</div>
				</div>

				<div class="dRow1">
					<div class="mydCel25 bodbl" style="width: 518px;">We Hereby Submit Our Invoice For
						The Goods Transported as Under:</div>
					<div class="mydCel26 bodbl" style="width: 250px;">OUR PAN NO: AAGCC0032K</div>
					<div class="mydCel26 bodbl" style="width: 250px;">&nbsp;</div>
				</div>

				<div class="dRow1">

					<div class="CSSTabGeneratorss">
						<table>
							<tr>
								<td width="300px" colspan="6">CONSIGNMENT DETAILS</td>
								<td rowspan="2">Weight<br> MT</td>
					<!-- 			<td rowspan="2">Charge Weight</td> -->
								<td rowspan="2">Rate</td>
								<td rowspan="2">Freight(A)</td>
								<td rowspan="2">Detention(B)</td>
								<td rowspan="2">Bonus(C)</td>
								<td rowspan="2">Ldng/Unldng<br> AMT.</td>
								<td rowspan="2">Others(E)</td>
								<td rowspan="2">Total(A+B+C+D+E)</td>
							</tr>
							<tr>
								<td><strong class="bbl">Date</strong></td>
								
								<td><strong class="bbl">LR No.</strong></td>
							
								<td><strong class="bbl">From Station</strong></td>
								<td><strong class="bbl">To Station</strong></td>
									<td><strong class="bbl">Lorry No.</strong></td>
								<td><strong class="bbl">Invoice No.</strong></td>
							</tr>
							<tr ng-repeat="bilDet in bill.blDetList">

								<td>{{bilDet.cnmt.cnmtDt | date:'dd/MM/yyyy'}}</td>
								
								<td>{{bilDet.cnmt.cnmtCode}}</td>
							
								<td>{{bilDet.frmStn}}</td>
								<td>{{bilDet.toStn}}</td>
									<td>{{bilDet.truckNo}}</td>
								<td>{{bilDet.cnmt.cnmtInvoiceNo[0].invoiceNo}}<br>{{bilDet.cnmt.cnmtInvoiceNo[1].invoiceNo}}
								</td>
<!-- 								<td>{{bilDet.billDet.bdActWt/1000 | number : 3}}</td> -->
								<td>{{bilDet.billDet.bdChgWt/1000 | number : 3}}</td>
								<td>{{bilDet.billDet.bdRate * 1000 | number : 2}}</td>
								<td>{{bilDet.billDet.bdFreight | number : 2}}</td>
								<td>{{bilDet.billDet.bdDetAmt | number : 2}}</td>
								<td>{{bilDet.billDet.bdBonusAmt | number : 2}}</td>
								<td>{{bilDet.billDet.bdLoadAmt | number :
									2}}/{{bilDet.billDet.bdUnloadAmt | number : 2}}</td>
								<td>{{bilDet.billDet.bdOthChgAmt | number : 2}}</td>
								<td>{{bilDet.billDet.bdTotAmt | number : 2}}</td>

							</tr>
							<tr>
							<td>Bank Name</td>
							<td>A/c No.</td>
							<td>IFSC CODE</td>
							<td colspan="9">Amount In Words:</td>
						<td rowspan="2"><br>Total</td>
						<td rowspan="2"><br>{{bill.bill.blFinalTot}}</td>
							</tr>
							<tr>
							<td >Kotak Mahindra Bank<br><br></td>
							<td >3011623336<br><br></td>
							<td >KKBK0000299<br><br></td>
							<td  colspan="9" style="font-size: 15px;">{{bill.bilAmt}}</td>
							</tr>
                      <tr><td colspan="14" style="text-align: left;">Payment to be made Only By Cheque/ DD in Favour Of Care Go Logistics Pvt. Ltd., or Electronic Fund transfer to above Kotak Bank A/C</td></tr>
						</table>

					</div>
				</div>


				<!-- 	<div class="dRow1">
					<div class="del16">
						<div class="del17 bod2bl">IT IS HEREBY DECLARE THAT WE ARE
							NOT AVAILING CENVAT CREDIT FACILITY IN RESPECT OF INPUTS/SERVICES
							/CAPITAL GOODS</div>
						<div class="del17 bod2bl">WE HAVE NOT AVAILED ANY BENEFIT
							UNDER THE NOTIFICATION NO. 12/2003-SERVICE TAX DATED 20/06/2008</div>
					</div>




					<div class="dCel15 bod2bl" style="text-align: center;">
						<div class="dCel14 bod1bl pdingbl">SUB TOTAL</div>
						<div class="dCel14 bod1bl pdingbl">{{bill.bill.blSubTot}}</div>
					</div>

				</div> -->


				<!-- <div class="dRow1">
					<div class="dCel18 bod1bl">Bank Name</div>
					<div class="dCel00 bod1bl">A/c No.</div>
					<div class="dCel18 bod1bl">IFSC CODE</div>
					<div class="dCel05 bod1bl">Branch</div>
					<div class="dCel19 bod2bl">Taxable Service tax= 30%</div>
					<div class="dCel14 bod2bl"
						style="text-align: right; padding-right: 5px;">{{bill.bill.blTaxableSerTax}}</div>
					<div class="dCel14 bod2bl">&nbsp</div>
				</div> -->

				<!-- <div class="dRow1">
					<div class="dCel18 bod1bl pding1bl">Kotak Mahindra Bank</div>
					<div class="dCel00 bod1bl pding1bl">3011623336</div>
					<div class="dCel18 bod1bl pding1bl">KKBK0000299</div>
					<div class="dCel05 bod1bl pdingbl">
						61, Old Judicial Complex,<br> Sector-15, Gurgaon 122001
					</div>
					<div class="dCel19 bod2bl pdng">
						Service Tax= 14%<br>Swachh Bharat Cess= 0.5%<br>Kisan kalyan Cess=0.5%</div>
					<div class="dCel14 bod2bl pdng" style="text-align: right;">
						<span>{{bill.bill.blSerTax}}</span><br>
						<span>{{bill.bill.blSwachBhCess}}</span><br>
						<span>{{bill.bill.blKisanKalCess}}</span>
					</div>
					<div class="dCel14 bod2bl pding1bl">&nbsp</div>
				</div> -->

			<!-- 	<div class="dRow1">
					<div class="mydCel20 bod2bl pdingbl">
						Amount in words: &nbsp;&nbsp;&nbsp;&nbsp; <span class="bbl">{{bill.bilAmt}}</span>
						<hr>
						<br> Only By A/c Payee Cheque / DD in Favour Of <span
							class="bbl">Care Go Logistics Pvt. Ltd.,</span><span> or
							Electronic Fund transfer to below Bank A/c </span> <br>

					</div>

					<div class="dRow1">

						<div class="dCel20 bod1bl pding2bl">Pay {{bill.bilAmt}}</div>
						<div class="mydCel14 bod1bl pding2bl" style="text-align: right;">
							<br>Total<br>
						</div>
						<div class="mydCel14 bod1bl pding2bl">
							<br>{{bill.bill.blFinalTot}}<br>
						</div>
					</div>

					<div class="dRow1">
						<div class="dCel28 bod2bl" style="text-align: center;">
							<div class="dCel31 bod1bl">Bank Details</div>
							<div class="dCel27 bod1bl">Bank Name</div>
							<div class="dCel27 bod1">A/c No.</div>
							<div class="dCel27 bod1bl">IFSC CODE</div>
							<div class="dCel29 bod1bl">Branch</div>
							<div class="dCel27 bod1bl pding1bl">HDFC BANK</div>
							<div class="dCel27 bod1bl pding1bl">13810330000037</div>
							<div class="dCel27 bod1bl pding1bl">HDFC0001381</div>
							<div class="dCel29 bod1bl pdingbl">472-11, OLD RAILWAY
								ROAD, DHOBI GHAT, NEAR KABIR BHAWAN, GURGAON</div>
							<div class="dCel27 bod1bl pding1bl">ICICI BANK</div>
							<div class="dCel27 bod1bl pding1bl">103105001831</div>
							<div class="dCel27 bod1bl pding1bl">ICIC0001031</div>
							<div class="dCel29 bod1bl pdingbl">SCO - 59, 60, OLD
								JUDICIAL COMPLEX, SECTOR - 15, GURGAON</div>
							<div class="dCel27 bod1bl pding1bl">STATE BANK OF INDIA</div>
							<div class="dCel27 bod1bl pding1bl">35334334071</div>
							<div class="dCel27 bod1bl pding1bl">SBIN0060414</div>
							<div class="dCel29 bod1bl pdingbl">
								SCO - 5,6,7 SECTOR - 15, <br>PART - 2, GURGAON
							</div>
						</div>
						<div class="dCel30" style="height: 180px;">
							<div class="dCel32 bod1bl"
								style="text-align: right; margin-top: 10px; margin-left: -10px;">For
								Care Go Logistic Pvt. Ltd</div>
							<div class="dCel32 bod1bl"
								style="text-align: left; margin-top: 125px; margin-left: 10px;">Cheked
								By</div>
							<div class="dCel32 bod1bl"
								style="text-align: right; margin-top: -18px; margin-left: -10px;">Cheked
								By</div>
						</div>
					</div>

					<div class="dRow1">
						<div class="dCel21 bod1bl pding3bl" style="text-align: left">
							As per notification No. 5/2017. Central Tax dated 19.6.2017, GTA
							are exempted under GST and it is payable by consignor/consignee
							under CM @ 5% Rupees &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{bill.cGST +
							bill.sGST + bill.iGST}} <br> We declare that no ITC availed
							by us on inputs and inputs services used for providing GTA
							Services.
						</div>
					</div>
				</div> -->
					<div class="remRow1">
					<div class="remCel30" style="height: 100px;">
						<div class="remCel32 rembold1bl"
							style="text-align: right; margin-top: 10px;">For Care Go
							Logistic Pvt. Ltd</div>
						<div class="remCel32 rembold1bl"
							style="text-align: right; margin-top: 50px; margin-left: -250px;">Checked
							By</div>
						<div class="remCel32 rembold1bl"
							style="text-align: right; margin-top: -14px;">Bill Incharge</div>
					</div>
				</div>
			</div>
<!--  end hill ltd  -->



<!-- end  -->

			<div ng-show="bill.bhilaiBillFormat">
				<div class="hRow1" style="width: 1018px;">
					<div class="dCel1">
						<img src="resources/img/logo.png" width="357" height="106" />
					</div>
					<div class="dCel1">
						<div
							style="font-size: 16px; font-weight: bold; font-family: Arial;">Care
							Go Logistics Pvt. Ltd.</div>
						<div style="font-size: 12px">Regd. Office: S-405, Greater
							Kailash, Part-II,New Delhi-110048</div>
						<div style="font-size: 12px">MSME Regd. No.: HR05E0000104</div>
						<div style="font-size: 12px">Email: gurgaon@carego.co</div>
					</div>
				</div>
				<div class="dRow1">
					<div class="mydCel01 bodbl" style="width: 1018px;">

						<div ng-show="bill.bill.blType == 'N'"
							style="margin-left: 60px; text-align: left; font-size: 20px;">FRIEGHT
							BILLS</div>
						<div ng-show="bill.bill.blType == 'S'">SUPPLEMENTARY BILLS</div>
					</div>

				</div>


				<div class="dRow1">
					<div class="dCel04 bodbl">
						<div class="dCel04 bodbl" style="border: 0px; text-align: left;">To,
							M/s</div>

						<div class="dCel04 bodbl"
							style="border: 0px; margin-top: -15px; margin-left: 100px; text-align: justify;">
							{{bill.cust.custName}} <br>{{bill.cust.custAdd}},<br>{{bill.cust.custCity}}
							{{bill.cust.custPin}} <br>


						</div>
						<p style="text-align: left; margin-left: 5px;">GSTIN NO:
							{{bill.bill.blGstNo}}</p>
						<p
							style="font-style: italic; margin-left: 50px; margin-top: -50px;">PAN
							NO.- AAGCC0032K</p>
					</div>
					<div
						style="position: absolute; font-size: 13px; top: 135px; right: 111px; width: 171px; border: 1px solid black; text-align: center;">
						CONTRACT REF. NO.</div>
					<div
						style="position: absolute; height: 39px; font-size: 13px; top: 135px; right: -58px; width: 170px; border: 1px solid black; text-align: left;">
						&nbsp;REF No <br> &nbsp; 18-01-18
					</div>
					<div
						style="position: absolute; font-size: 12px; top: 155px; right: 110px; width: 171px; border: 1px solid black; text-align: center;">
						Dated</div>
					<div
						style="position: absolute; font-size: 16px; top: 174px; right: 110px; width: 171px; border: 1px solid black; text-align: right;">
						BILL NO.<br> DATED
					</div>
					<div
						style="position: absolute; font-size: 16px; height: 46px; top: 173px; right: -58px; width: 170px; border: 1px solid black; text-align: left;">
						&nbsp; &nbsp;{{bill.bill.blBillNo}}<br> &nbsp;&nbsp;
						{{bill.bill.blBillDt | date:'dd/MM/yyyy'}}
					</div>
				</div>

				<div class="dRow1">
					<div class="mydCel25 bodbl" style="width: 1018px;">
						We Hereby Submit Our Frieght Bill For The Goods Transported as
						Under:<span style="margin-left: 200px;">NO TDS TO BE
							DEDUCTED w.e.f 01/10/2009</span>
					</div>

				</div>

				<div class="dRow1" style="width: 1026px;">

					<div class="CSSTabGeneratorss">
						<table>
							<tr>
								<td width="300px" colspan="4">CONSIGNMENT DETAILS</td>
								<td rowspan="2">Weight</td>
								<!-- <td rowspan="2">Charge Weight</td> -->
								<td rowspan="2">Rate</td>
								<td rowspan="2">Freight</td>
								<td rowspan="2">LORRY NO</td>
								<td rowspan="2">SIZE <br> L X W X H
								</td>
								<td rowspan="2">FINE</td>
								<td rowspan="2">DETANTION</td>
								<td rowspan="2">Total<br>(A+B+C+D+E)
								</td>
							</tr>
							<tr>
								<td><strong class="bbl">Date</strong></td>
								<td><strong class="bbl">CNMT No.</strong></td>
								<!-- <td><strong class="bbl">LR No.</strong></td> 
								<td><strong class="bbl">Lorry No.</strong></td>-->
								<td><strong class="bbl">From Station</strong></td>
								<td><strong class="bbl">To Station</strong></td>
							</tr>
							<tr ng-repeat="bilDet in bill.blDetList">

								<td>{{bilDet.cnmt.cnmtDt | date:'dd/MM/yyyy'}}</td>
								<!-- <td>{{bilDet.cnmt.cnmtInvoiceNo[0].invoiceNo}}<br>{{bilDet.cnmt.cnmtInvoiceNo[1].invoiceNo}}
								</td> -->
								 <td>{{bilDet.cnmt.cnmtCode}}</td>
							   	<!--<td>{{bilDet.truckNo}}</td> -->
								<td>{{bilDet.frmStn}}</td>
								<td>{{bilDet.toStn}}</td>
<!-- 								<td>{{bilDet.billDet.bdActWt/1000 | number : 3}}</td> -->
								<td>{{bilDet.billDet.bdChgWt/1000 | number : 3}}</td>
								<td>{{bilDet.billDet.bdRate * 1000 | number : 2}}</td>
								<td>{{bilDet.billDet.bdFreight | number : 2}}</td>
								<td>{{bilDet.truckNo}}</td>
								<td>{{bilDet.billDet.bdBonusAmt | number : 2}}</td>
								<td></td>
								<td></td>
								<td>{{bilDet.billDet.bdTotAmt | number : 2}}</td>

							</tr>

						</table>

					</div>
				</div>


				

				<div class="dRow1">
					<div class="mydCel20 bod2bl pdingbl">
						<!--  Amount in words: &nbsp;&nbsp;&nbsp;&nbsp; <span class="bbl">{{bill.bilAmt}}</span> -->
						IT IS HEREBY DECLARE THAT WE ARE NOT AVAILING CENVAT CREDIT
						FACILITY IN RESPECT OF INPUTS/SERVICES/ CAPITAL GOODS WE HAVE NOT
						<br> AVAILED ANY BENEFIT UNDER THE NOTIFICATION NO.
						12/2003-SERVICE TAX DATED 20/06/2008 <br> <br>
					</div>

					<div class="dRow1">

						<!-- <div class="dCel20 bod1bl pding2bl">Pay {{bill.bilAmt}}</div> -->
						<div class="mydCel14 bod1bl pding2bl" style="text-align: right;">SUB
							Total</div>
						<div class="mydCel14 bod1bl pding2bl">{{bill.bill.blFinalTot}}</div>
					</div>

					<div class="dRow1">
						<div class="dCel28 bod2bl"
							style="text-align: center; width: 920px;">
							<div class="dCel31 bod1bl"
								style="width: 1020px; text-align: left;">
								Rs; - &nbsp;&nbsp; {{bill.bilAmt}}
								<hr>
								<span style="text-align: left;">Only By A/c Payee Cheque
									/ DD in Favour of Care Go Logistics Pvt. Ltd. or Electronic
									fund Transfer to below Bank A/C</span>
							</div>
							<div class="dCel27 bod1bl">Bank Name</div>
							<div class="dCel27 bod1">A/c No.</div>
							<div class="dCel27 bod1bl">IFSC CODE</div>
							<div class="dCel29 bod1bl" style="width: 315px;">Branch</div>

							<div class="dCel27 bod1bl pding1bl">KOTAK MAHINDRA BANK <br><br></div>
							<div class="dCel27 bod1bl pding1bl">
								3011623336<br> <br>
							</div>
							<div class="dCel27 bod1bl pding1bl">
								KKBK0000299<br> <br>
							</div>
							<div class="dCel29 bod1bl pdingbl" style="width: 315px;">
								61,Old JUDICIAL COMPLEX,SECTOR-15,GURGAON 122001<br> <br>
								<br>
							</div>

							
						</div>
						<div class="dCel31 bod1bl"
							style="width: 1020px; text-align: left; border-right-width: 0px;">
							As per notification No. 5/2017 - Central Tax dated 19.06.2017, <br>
							GTA are exempted under GST and it is payable by consignor/
							consignee under RCM @5% <br> <span
								style="margin-left: 770px;">For Care Go Logistics Pvt
								Ltd.</span> <br> <br> <br> <span
								style="margin-left: 820px;">Signature</span> <br> <span
								style="margin-left: 780px;">Checked by Bill Incharge</span> <br>
							<span style="font-size: 16px;">LOADING POINT :- METALL
								STRUCTURES PVT LTD MURMUNDA BHILAI</span>
						</div>
						
					</div>

					
				</div>
			</div>


<!-- himf statr -->

<div ng-show="bill.hmfBillFormat">
		<!-- 		<div class="hRow1">
					<div class="dCel1">
						<img src="resources/img/logo.png" width="357" height="106" />
					</div>
					<div class="dCel1">
						<div
							style="font-size: 16px; font-weight: bold; font-family: Arial;">Care
							Go Logistics Pvt. Ltd.</div>
						<div style="font-size: 12px">Regd. Office: S-405, Greater
							Kailash, Part-II,New Delhi-110048</div>
						<div style="font-size: 12px">Phone:{{ selBrh.branchPhone }}</div>
						<div style="font-size: 12px">Email: gurgaon@carego.co</div>
					</div>
				</div> -->
				<div class="dRow1">
					<div class="mydCel01 bodbl" style="width: 1018px;">
						<div ng-show="bill.bill.blType == 'N'">TAX INVOICE</div>
						<div ng-show="bill.bill.blType == 'S'">SUPPLEMENTARY BILLS</div>
					</div>
					<!-- <div class="mydCel03 bodbl" style="width: 250px;">Co CIN No.: U60231DL2015PTC279265</div>
					<div class="mydCel03 bodbl" style="width: 250px;">SAC No.: {{bill.cust.custSAC}}</div> -->
				</div>
				<!-- 	<div class="dRow1">
					<div class="mydCel01 bodbl" style="width: 1018px;">
						<div ng-show="bill.bill.blType == 'N'">TAX INVOICE</div>
						<div ng-show="bill.bill.blType == 'S'">SUPPLEMENTARY BILLS</div>
						<u>All Disputes Subject to Chennai Jurisdiction</u>
					</div>
					<div class="mydCel03 bodbl" style="width: 250px;">Co CIN No.: U60231DL2015PTC279265</div>
					<div class="mydCel03 bodbl" style="width: 250px;">SAC No.: {{bill.cust.custSAC}}</div>
				</div> -->
				
			<!-- <!-- 			<div class="remRow1" style="width: 1027px;">
					<div class="remCel30" style="height: 100px;">
						<div class="remCel32 rembold1bl"
							style="text-align:center; ">
							ICM LOGISTICS PVT LTD
							<br>
							W-121, FIRM TOWERS, 3RD FLOOR,3RD AVENUE,<br>
							ANNA NAGAR, CHENNAI- 600040
							<br>Ph:26208376/77, e-mail: HeadOffice@icmlogistics.com
							<br>
							GSTIN: {{bill.bill.blGstNo}}, SAC Code: {{bill.cust.custSAC}}, PAN No: AABC18695P 
</div>
						<div class="remCel32 rembold1bl"
							style="text-align: right; margin-top: 50px; margin-left: -250px;">Checked
							By</div>
						<div class="remCel32 rembold1bl"
							style="text-align: right; margin-top: -14px;">Bill Incharge</div>
					</div>
				</div> --> 


				<div class="dRow1">
					<div class="dCel04 bodbl" ">
						<div class="dCel04 bodbl" style="border: 0px; text-align: left;"><br>&nbsp;&nbsp;<u>Customer Detail</u>
							</div>

						<div class="dCel04 bodbl"
							style="border: 0px; margin-top: -15px; text-align: left;">
							<br>&nbsp;&nbsp;{{bill.cust.custName}} <br>&nbsp;&nbsp;{{bill.cust.custAdd}},<br>&nbsp;&nbsp;{{bill.cust.custCity}}
							&nbsp;&nbsp;{{bill.cust.custPin}} <br>
							&nbsp;&nbsp;PAN NO- 07AAACH4041D1Z6


						</div>
						<p style="text-align: left; margin-left: 5px;">GST NO:
							{{bill.bill.blGstNo}}</p>
							<br>
							<hr>
									<div class="dCel04 bodbl" style="border: 0px; text-align: left;"><br>&nbsp;&nbsp;<u>Vendor's Detail</u>
							</div>

						<div class="dCel04 bodbl"
							style="border: 0px; margin-top: -15px; text-align: left;">
						<br>
						&nbsp;&nbsp; Vendor Name &nbsp;&nbsp;<span style="margin-left: 20px;"><strong>Care Go Logistics Pvt Ltd</strong></span>
						<br>
						&nbsp;&nbsp; Address &nbsp;&nbsp;<span style="margin-left: 50px;"><strong>S-405, Greater Kailash Part-II, New Delhi-110048, India</strong></span>
                        <br>
                       &nbsp;&nbsp; GSTIN &nbsp;&nbsp;<span style="margin-left: 60px;"><strong>As Per Notification No 5/2017 Central Tax Dated 19-06-2017</strong></span>
                        <br>
                       &nbsp;&nbsp; PAN No. &nbsp;&nbsp;<span style="margin-left: 50px;"><strong>AAGCC0032K</strong></span>
						<br>
						&nbsp;&nbsp; MSME Regd. No.: HR05E0000104<br>
						
						&nbsp;&nbsp; State Name &nbsp;&nbsp;<span style="margin-left: 32px;"><strong>Expempted</strong></span>
						<br>
						&nbsp;&nbsp; State Code &nbsp;&nbsp;<span style="margin-left: 35px;"><strong>Expempted</strong></span> 
						</div>

					</div>
					
						<div class="dCel066 bodbl" style="border-left: 0px; height:321px; ">
						<div class="dCel06 bodbl">
							<div style="margin-top: 2px; text-align: left;">
								<br>&nbsp;&nbsp; Invoice NO. <span style="margin-left: 160px;">{{bill.bill.blBillNo}}</span>
						
							<br> &nbsp;&nbsp;Date : <span style="margin-left: 190px;">{{bill.bill.blBillDt | date:'dd/MM/yyyy'}}</span>
							<br> &nbsp;&nbsp;Service Order No.: <span style="margin-left: 130px;">NILL</span>
							<br> &nbsp;&nbsp;Service Order Date.: <span style="margin-left: 120px;">NILL</span>
							<br>&nbsp;&nbsp;Place of Service(State) <span style="margin-left: 50px;">NA</span>
							<br>&nbsp;&nbsp;Place of Service(Code) <span style="margin-left: 50px;">NA</span>
							
							</div>
							
				<!-- 			<div style="height: 27px;">
								BILL DATE:-{{bill.bill.blBillDt | date:'dd/MM/yyyy'}} <br>

							</div> -->
						</div>
					</div>
					
				
				</div>

				<div class="dRow1">
					<div class="mydCel25 bodbl" style="width: 1018px; text-align: left;"><!-- We Hereby Submit Our Invoice For
						The Goods Transported as Under: -->&nbsp; PARTICULARS</div>
<!-- 					<div class="mydCel26 bodbl" style="width: 250px;">OUR PAN NO: AAGCC0032K</div>
					<div class="mydCel26 bodbl" style="width: 250px;">&nbsp;</div> -->
				</div>

				<div class="dRow1" style="width: 1027px;">

					<div class="CSSTabGeneratorss">
						<table>
						<tr>
						<td colspan="7">Being the billing for Transportation Charges</td>
						<td rowspan="2"></td>
						</tr>
							<tr>
							
								<td >Description of Service</td>
								<td>SAC<br>(Services Code)</td>
							    <td>Name of Services</td>
								<td >Unit<br></td>
								<td >UOM<br></td>
								<td >Rate(Rs)<br></td>
								<td >Over Hieght<br></td>
					
							</tr>
					 
					<!-- 		<tr ng-repeat="bilDet in bill.blDetList">
                                
                              
							</tr> -->
							<tr>
							<td>Transportation of Goods</td>
							<td rowspan="6">{{bill.cust.custSAC}}</td>
							<td rowspan="6" style="text-align: left;">&nbsp;Transportation of Goods By Road</td>
							<td>{{bill.totCnmtCount}}</td>
							<td>Fixed</td>
							<td>{{bill.totFrt | number : 2}}</td>
							<td>{{bill.totOvrHgt}}</td>
							<td>{{bill.bill.blFinalTot}}</td>
							</tr>
							<tr>
							<td></td><td></td><td></td><td></td><td></td><td></td>
							</tr>
								<tr>
							<td></td><td></td><td></td><td></td><td></td><td></td>
							</tr>
								<tr>
							<td></td><td></td><td></td><td></td><td></td><td></td>
							</tr>
								<tr>
							<td></td><td></td><td></td><td></td><td></td><td></td>
							</tr>
								<tr>
							<td></td><td></td><td></td><td></td><td></td><td></td>
							</tr>
							<tr>
							<td colspan="7" style="text-align: left;">TOTAL</td><td>{{bill.bill.blFinalTot}}</td>
							</tr>
							<tr> <td colspan="7"></td> <td></td></tr>
							<tr> <td colspan="7" style="text-align: left;font-size:13px; font-weight: bold;">GST IS PAYABLE BY CONSIGNOR/ CONSIGNEE UNDER RCM @ 5% RUPEES</td> <td></td></tr>
						<tr> <td colspan="7" style="text-align: left;font-size:13px; font-weight: bold;">Gross Total</td> <td>{{bill.bill.blFinalTot}}</td></tr>
						<tr> <td colspan="8" style="text-align: left;font-size:13px; font-weight: bold;">Amount in words:-{{bill.bilAmt}}</td></tr>
						</table>

					</div>
				</div>


					<div class="remRow1" style="width: 1027px;">
					<div class="remCel30" >
						<div class="remCel32 rembold1bl" style="font-size: 13px; font-weight: bold;">
							<br><span style="margin-left: -350px;"><u>Declaration</u></span>
							
							</div>
							<div class="remCel32 rembold1bl" style="text-align: left;">
							- Certified that all the given above are true and correct. The amount indicated represents the price actually charged and there is no flow of additional consideration directly or indirectly from the Service.
							
							</div>
						<div class="remCel32 rembold1bl"
							style="text-align: right; margin-top: 10px;">For Care Go
							Logistic Pvt. Ltd</div>
						<div class="remCel32 rembold1bl"
							style="text-align: right; margin-top: 50px; margin-left: -750px;">
							</div>
						<div class="remCel32 rembold1bl"
							style="text-align: right; margin-top: -14px;">Authorised Signatory</div>
					</div>
				</div>
			</div>

<!-- himf end -->



<!-- Adani Wilmar Ltd Jaipur lucknow alwar start  -->
<div ng-show="bill.AdaniWilmarJaiLucAlwBillFormat">
				<div class="hRow1">
					<div class="dCel1">
						<img src="resources/img/logo.png" width="357" height="106" />
					</div>
					<div class="dCel1">
						<div
							style="font-size: 16px; font-weight: bold; font-family: Arial;">Care
							Go Logistics Pvt. Ltd.</div>
						<div style="font-size: 12px">Regd. Office: S-405, Greater
							Kailash, Part-II,New Delhi-110048</div>
<!-- 						<div style="font-size: 12px">Phone:{{ selBrh.branchPhone }}</div> -->
						<div style="font-size: 12px">MSME Regd. No.: HR05E0000104</div>
						<div style="font-size: 12px">Email: gurgaon@carego.co</div>
					</div>
				</div>
				<div class="dRow1">
					<div class="mydCel01 bodbl" style="width: 518px;">
						<div ng-show="bill.bill.blType == 'N'">TAX INVOICE</div>
						<div ng-show="bill.bill.blType == 'S'">SUPPLEMENTARY BILLS</div>
					</div>
					<div class="mydCel03 bodbl" style="width: 250px;">Co CIN No.: U60231DL2015PTC279265</div>
					<div class="mydCel03 bodbl" style="width: 250px;">SAC No.: {{bill.cust.custSAC}}</div>
				</div>


				<div class="dRow1">
					<div class="dCel04 bodbl">
						<div class="dCel04 bodbl" style="border: 0px; text-align: left;">
							M/s {{bill.cust.custName}}</div>

						<div class="dCel04 bodbl"
							style="border: 0px; margin-top: -15px; text-align: left;">
							<br>{{bill.adaniStn}} <br><br>
							 <br>


						</div>
						<p style="text-align: left; margin-left: 5px;">GST NO:
							{{bill.bill.blGstNo}}</p>

					</div>
					<div class="dCel066 bodbl">
						<div class="dCel06 bodbl">
							<div style="margin-top: 2px; text-align: center;">
								<br> Invoice NO.: {{bill.bill.blBillNo}}
							</div>
							<br>
                             <br>
							<div style="height: 27px;">
								DATE: {{bill.bill.blBillDt | date:'dd/MM/yyyy'}} <br>

							</div>
						</div>
					</div>
				</div>

				<div class="dRow1">
					<div class="mydCel25 bodbl" style="width: 518px;">We Hereby Submit Our Frieght Bill For
						The Goods Transported as Under:</div>
					<div class="mydCel26 bodbl" style="width: 250px;">OUR PAN NO: AAGCC0032K</div>
					<div class="mydCel26 bodbl" style="width: 250px;">&nbsp;</div>
				</div>

				<div class="dRow1">

					<div class="CSSTabGeneratorss">
						<table>
							<tr>
								<td width="300px" colspan="6">CONSIGNMENT DETAILS</td>
								<td rowspan="2">Weight<br> MT</td>
					<!-- 			<td rowspan="2">Charge Weight</td> -->
								<td rowspan="2">Rate</td>
								<td rowspan="2">Freight(A)</td>
								<td rowspan="2">Detention(B)</td>
								<td rowspan="2">Bonus(C)</td>
								<td rowspan="2">Ldng/Unldng<br> AMT.</td>
								<td rowspan="2">Others(E)</td>
								<td rowspan="2">Total(A+B+C+D+E)</td>
							</tr>
							<tr>
								<td><strong class="bbl">Date</strong></td>
								
								<td><strong class="bbl">LR No.</strong></td>
								<td><strong class="bbl">From Station</strong></td>
								<td><strong class="bbl">To Station</strong></td>
									<td><strong class="bbl">Lorry No.</strong></td>
								<td><strong class="bbl">Invoice No.</strong></td>
							</tr>
							<tr ng-repeat="bilDet in bill.blDetList">

								<td>{{bilDet.cnmt.cnmtDt | date:'dd/MM/yyyy'}}</td>
								
								<td>{{bilDet.cnmt.cnmtCode}}</td>
								<td>{{bilDet.frmStn}}</td>
								<td>{{bilDet.toStn}}</td>
								<td>{{bilDet.truckNo}}</td>
								<td>{{bilDet.cnmt.cnmtInvoiceNo[0].invoiceNo}}<br>{{bilDet.cnmt.cnmtInvoiceNo[1].invoiceNo}}
								</td>
<!-- 								<td>{{bilDet.billDet.bdActWt/1000 | number : 3}}</td> -->
								<td>{{bilDet.billDet.bdChgWt/1000 | number : 3}}</td>
								<td>{{bilDet.billDet.bdRate * 1000 | number : 2}}</td>
								<td>{{bilDet.billDet.bdFreight | number : 2}}</td>
								<td>{{bilDet.billDet.bdDetAmt | number : 2}}</td>
								<td>{{bilDet.billDet.bdBonusAmt | number : 2}}</td>
								<td>{{bilDet.billDet.bdLoadAmt | number :
									2}}/{{bilDet.billDet.bdUnloadAmt | number : 2}}</td>
								<td>{{bilDet.billDet.bdOthChgAmt | number : 2}}</td>
								<td>{{bilDet.billDet.bdTotAmt | number : 2}}</td>

							</tr>
							<tr>
							<td>Bank Name</td>
							<td>A/c No.</td>
							<td>IFSC CODE</td>
							<td colspan="9" style="font-size: 15px;">Amount In Words: Rs. {{bill.bilAmt}}</td>
						<td rowspan="2"><br>Total</td>
						<td rowspan="2"><br>{{bill.bill.blFinalTot}}</td>
							</tr>
							<tr>
							<td >Kotak Mahindra Bank<br><br></td>
							<td >3011623336<br><br></td>
							<td >KKBK0000299<br><br></td>
							<td  colspan="9" style="font-size: 15px;">&nbsp;</td>
							</tr>
                      <tr><td colspan="14" style="text-align: left;">Payment to be made Only By Cheque/ DD in Favour Of Care Go Logistics Pvt. Ltd., or Electronic Fund transfer to above Kotak Bank A/C</td></tr>
						</table>

					</div>
				</div>


			
					<div class="remRow1">
					<div class="remCel30" style="height: 100px;">
						<div class="remCel32 rembold1bl"
							style="text-align: right; margin-top: 10px;">For Care Go
							Logistic Pvt. Ltd</div>
						<div class="remCel32 rembold1bl"
							style="text-align: right; margin-top: 50px; margin-left: -250px;">Checked
							By</div>
						<div class="remCel32 rembold1bl"
							style="text-align: right; margin-top: -14px;">Bill Incharge</div>
					</div>
				</div>
			<!-- 	<div >
					<div class="mydCel25 bodbl" style="width: 960px;">GST IS PAYABLE BY CONSIGNOR?CONSIGNEE UNDER RCM 5% RUPEES</div>
					<div class="mydCel26 bodbl" style="width: 250px;">OUR PAN NO: AAGCC0032K</div>
					<div class="mydCel26 bodbl" style="width: 250px;">&nbsp;</div>
				</div> -->
				<div class="dRow1">

					<div class="CSSTabGeneratorss">
						<table>
				
                      <tr><td colspan="13" style="text-align: left;font-size: 15px; font-weight: bold;">GST IS PAYABLE BY CONSIGNOR/CONSIGNEE UNDER RCM 5% RUPEES {{bill.cGST + bill.sGST + bill.iGST}}</td></tr>
						</table>

					</div>
				</div>
			</div>
<!--  end Adani Wilmar Jaipur lucknow alwar ltd  -->

<!-- Passive Infra Project PVT LTD. start -->

				 <div ng-show="bill.PassiveInfraBillFormat">
							<div class="hRow1">
					<div class="dCel1">
						<img src="resources/img/logo.png" width="357" height="106" />
					</div>
					<div class="dCel1">
						<div
							style="font-size: 16px; font-weight: bold; font-family: Arial;">Care
							Go Logistics Pvt. Ltd.</div>
						<div style="font-size: 12px">Regd. Office: S-405, Greater Kailash, Part-II,New Delhi-110048</div>
<!-- 						<div style="font-size: 12px">Phone:{{ selBrh.branchPhone }}</div> -->
						<div style="font-size: 12px">MSME Regd. No.: HR05E0000104</div>
						<div style="font-size: 12px">Email: gurgaon@carego.co</div>
					</div>
				</div>
				<div class="dRow1">
					<div class="mydCel01 bodbl">
						<div ng-show="bill.bill.blType == 'N'">Invoice</div>
						 <div ng-show="bill.bill.blType == 'S'">SUPPLEMENTARY BILLS</div> 
					</div>
					
					<div class="mydCel03 bodbl">SAC No.:
						{{bill.cust.custSAC}}</div>
				</div>


				<div class="dRow1">
					<div class="dCel04 bodbl">
						<div class="dCel04 bodbl" style="border: 0px; text-align: left;">To,
							M/s</div>

						<div class="dCel04 bodbl"
							style="border: 0px; margin-top: -15px; margin-left: 100px; text-align: justify;">
							{{bill.cust.custName}} <br>{{bill.cust.custAdd}},<br>{{bill.cust.custCity}}
							{{bill.cust.custPin}}
							<br>
                           
							
						</div>
						 <p style="text-align: left;margin-left: 5px;">GSTIN NO: {{bill.bill.blGstNo}}</p>

					</div>
					<div class="dCel066 bodbl">
						<div class="dCel06 bodbl">
							<div style="margin-top: 2px; text-align: center;">
							<br>
							Invoice NO.:
								{{bill.bill.blBillNo}}</div>
							<br>
							
							<div style="height: 27px;">DATE: {{bill.bill.blBillDt | date:'dd/MM/yyyy'}}
							<br>
							
							</div>
						</div>
					</div>
				</div>

				<div class="dRow1">
					<div class="mydCel25 bodbl">We Hereby Submit Our Invoice For The
						Goods Transported as Under:</div>
						
					<!-- <div class="mydCel26 bodbl"></div> -->
					
					<div class="mydCel03 bodbl">OUR PAN NO: AAGCC0032K</div>
					
					
					
				</div>

				<div class="dRow1">

					<div class="CSSTabGeneratorss">
						<table>
							<tr>
								<td width="300px" colspan="5">CONSIGNMENT DETAILS</td>								
				  				<td rowspan="2">CHARGE WEIGHT</td>
							   	<td rowspan="2">RATE</td>
							   	<td rowspan="2">FREIGHT(A)</td>
							   	<td rowspan="2">DETENTION(B)</td>
							   	<td rowspan="2">R.T.O CHALLAN(C)</td>
							   	<td rowspan="2">TOLL TAX(D)</td>
							   	<td rowspan="2">TOTAL FREIGHT</td>
							   	<td rowspan="2"> ADVANCE RECVD (E)</td>
							   	<td rowspan="2">TOTAL(A+B+C+D-E)</td>								
							</tr>
							<tr>
								<td><strong class="bbl">DATE</strong></td>
<!-- 								<td><strong class="bbl">Invoice No.</strong></td> -->
								<td><strong class="bbl">LR NO.</strong></td>
								<td><strong class="bbl">LORRY NO.</strong></td>
								<td><strong class="bbl">FROM</strong></td>
								<td><strong class="bbl">TO</strong></td>								   
							</tr>
							<tr ng-repeat="bilDet in bill.blDetList">
							
								<td>{{bilDet.cnmt.cnmtDt | date:'dd.MM.yyyy'}}</td>
<!-- 							  	<td>{{bilDet.cnmt.cnmtInvoiceNo[0].invoiceNo}}<br>{{bilDet.cnmt.cnmtInvoiceNo[1].invoiceNo}}</td> -->
							   	<td>{{bilDet.cnmt.cnmtCode}}</td>
							   	<td>{{bilDet.truckNo}}</td>
							   	<td>{{bilDet.frmStn}}</td>
							   	<td> {{bilDet.toStn}}</td>
<!-- 							   	<td>{{bilDet.billDet.bdActWt/1000 | number : 3}}</td> -->
							   	<td>{{bilDet.billDet.bdChgWt/1000 | number : 3}}</td>
							   	<td>{{bilDet.billDet.bdRate * 1000 | number : 2}}</td>
							    <td>{{bilDet.billDet.bdFreight | number : 2}}</td>
							    <td>{{bilDet.billDet.bdDetAmt | number : 2}}</td>
							    <td>{{bilDet.billDet.bdRtoChallan | number : 2}}</td>
							   	<td>{{bilDet.billDet.bdTollTax | number : 2}}</td>
							   	<td>{{(bilDet.billDet.bdTotAmt) | number : 2}}</td>		   
							  	<td>{{bilDet.billDet.bdCnmtAdv | number : 2}}</td>
							   	<td>{{bilDet.billDet.bdTotAmt - bilDet.billDet.bdCnmtAdv | number : 2}}</td>		   
							  	
							</tr>

						</table>

					</div>
				</div>




				<div class="dRow1">
					<div class="mydCel20 bod2bl pdingbl">
						 Amount in words: &nbsp;&nbsp;&nbsp;&nbsp; <span class="bbl">{{bill.bilAmt}}</span>
						 <hr>
						 	<br>
						 Only By A/c Payee Cheque / DD in Favour Of <span class="bbl">Care Go Logistics Pvt. Ltd.,</span><span> or Electronic Fund transfer to below Bank A/c </span>
		                 <br>
						
					</div>
					
					<div class="dRow1">
					
					<!-- <div class="dCel20 bod1bl pding2bl">Pay {{bill.bilAmt}}</div> -->
					<div class="mydCel14 bod1bl pding2bl" style="text-align: right;"><br>Total<br></div>
					<div class="mydCel14 bod1bl pding2bl"><br>{{bill.bill.blFinalTot - bill.totAdvance}}<br></div>
				</div>

				<div class="dRow1">
					<div class="dCel28 bod2bl" style="text-align: center;">
						<div class="dCel31 bod1bl"> Bank Details</div>
						<div class="mydCel27 bod1bl">Bank Name</div>
						<div class="hedCel27 bod1">A/c No.</div>
						<div class="hedCel27 bod1bl">IFSC CODE</div>
						<div class="mydCel29 bod1bl">Branch</div>
						<!-- <div class="mydCel27 bod1bl pding1bl">HDFC BANK<br><br></div>
						<div class="hedCel27 bod1bl pding1bl">13810330000037<br><br></div>
						<div class="hedCel27 bod1bl pding1bl">HDFC0001381<br><br></div>
						<div class="mydCel29 bod1bl pdingbl">472-11, OLD RAILWAY ROAD,
							DHOBI GHAT, NEAR KABIR BHAWAN, GURGAON</div>
						<div class="mydCel27 bod1bl pding1bl">ICICI BANK</div>
						<div class="hedCel27 bod1bl pding1bl">103105001831</div>
						<div class="hedCel27 bod1bl pding1bl">ICIC0001031</div>
						<div class="mydCel29 bod1bl pdingbl">SCO - 59, 60, OLD
							JUDICIAL COMPLEX, SECTOR - 15, GURGAON</div>
							 -->
							<div class="mydCel27 bod1bl pding1bl"><br><br><br><br><br>KOTAK MAHINDRA BANK<br><br><br><br><br><br><br></div>
						<div class="hedCel27 bod1bl pding1bl"><br><br><br><br><br>3011623336<br><br><br><br><br><br><br></div>
						<div class="hedCel27 bod1bl pding1bl"><br><br><br><br><br>KKBK0000299<br><br><br><br><br><br><br></div>
						<div class="mydCel29 bod1bl pdingbl"><br><br><br><br><br>61, Old Judicial Complex, Sector-15, Gurgaon 122001<br><br><br><br><br><br><br></div>
							
						<!-- <div class="mydCel27 bod1bl pding1bl">STATE BANK OF INDIA</div>
						<div class="hedCel27 bod1bl pding1bl">35334334071</div>
						<div class="hedCel27 bod1bl pding1bl">SBIN0060414</div>
						<div class="mydCel29 bod1bl pdingbl">
							SCO - 5,6,7 SECTOR - 15, <br>PART - 2, GURGAON
						</div> -->
					</div>
					<div class="dCel30" style="height: 240px;">
						<div class="dCel32 bod1bl"
							style="text-align: right; margin-top: 10px; margin-left: -10px;">For
							Care Go Logistic Pvt. Ltd</div>
						<div class="dCel32 bod1bl"
							style="text-align: left; margin-top: 125px; margin-left: 10px;">Cheked
							By</div>
						<div class="dCel32 bod1bl"
							style="text-align: right; margin-top: -18px; margin-left: -10px;">Cheked
							By</div>
					</div>
				</div>

				<div class="dRow1">
					<div class="dCel21 bod1bl pding3bl" style="text-align: left">
						As per notification No. 5/2017. Central Tax dated 19.6.2017, GTA are exempted under GST and it is payable by consignor/consignee under CM @ 5% Rupees &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{bill.cGST + bill.sGST + bill.iGST}}
						<br>
						We declare that no ITC availed by us on inputs and inputs services used for providing GTA Services.
					</div>
				</div>
				 </div>
	</div>			 

<!-- passive infra project pvt Ltd end -->

</div>


	</div>

	

	<div id="exportablerel" class="noprint" >
	 
	<table ng-repeat="bill in blList" ng-show="bill.relBillFormat">
		<!-- <caption class="coltag tblrow">Annexure For Bill No. {{bill.bill.blBillNo}}</caption>-->
		<caption class="coltag tblrow"><b style="font-weight: bold;">Care Go Logistics PVT Ltd<br></b>
		  <b style="border:1px solid black;">RELIANCE CORPORATE PARK BUILDING NO.5B IST FLOOR 5TTC INDUSTRAIL AREA GHANSOLI</b></caption>	
		<tr style="text-align:center;">
			<th style="border:1px solid #000000;">FILE NO</th>
		    <th style="border:1px solid #000000;">FILE DATE</th>
		    <th style="border:1px solid #000000;">BILL NO</th>
		    <th style="border:1px solid #000000;">BILL DATE</th>
		    <th style="border:1px solid #000000;">BILL AMT</th>
		    <th style="border:1px solid #000000;">BILL From</th>
		    <th style="border:1px solid #000000;">BILL To</th>
		    <th style="border:1px solid #000000;">DCPI NO</th>
		    <th style="border:1px solid #000000;">DCPI DATE</th>
		    <th style="border:1px solid #000000;">TRUCK NO</th>
		    <th style="border:1px solid #000000;">LR NO</th>
		    <th style="border:1px solid #000000;">LR DATE</th>  
		    <th style="border:1px solid #000000;">DELIVERED</th>
		    <th style="border:1px solid #000000;">DELIVERY</th>
		    <th style="border:1px solid #000000;">RATE</th>
		    <th style="border:1px solid #000000;">AMOUNT</th>
		    <th style="border:1px solid #000000;">VENDOR</th>
		    <th style="border:1px solid #000000;">COMPANY</th>
		    <th style="border:1px solid #000000;">OVER HEIGHT</th>
		    <th style="border:1px solid #000000;">TOTAL AMOUNT</th>
		    <th style="border:1px solid #000000;">FROM</th>
		    <th style="border:1px solid #000000;">TO</th>
		 <!--    <th style="border:1px solid #000000;">WEIGHT</th>
		    <th style="border:1px solid #000000;">MATERIAL</th> -->
		    <th style="border:1px solid #000000;">TRUCK</th>
		    <!-- <th style="border:1px solid #000000;">REMARKS</th> -->
		</tr>
		
		<tr ng-repeat="bilDet in bill.blDetList" style="text-align:center;">
			<!--<td style="border:1px solid #000000;">{{ bill.bfNo }}</td>-->
			<td style="border:1px solid #000000;">{{ bill.bill.blBillNo }}</td>
			<td style="border:1px solid #000000;" ng-if="blDate == undefined || blDate==''">{{ bill.bill.blBillDt | date:'dd.MM.yyyy' }}</td>
			<td style="border:1px solid #000000;" ng-if="blDate != undefined && blDate!=''">{{ blDate | date:'dd.MM.yyyy'}}</td>
			<td style="border:1px solid #000000;">{{ bill.bill.blBillNo }}</td>
			<td style="border:1px solid #000000;" ng-if="blDate == undefined || blDate==''">{{ bill.bill.blBillDt | date:'dd.MM.yyyy' }}</td>
			<td style="border:1px solid #000000;" ng-if="blDate != undefined && blDate!=''">{{ blDate | date:'dd.MM.yyyy'}}</td>
			<td style="border:1px solid #000000;">{{bill.bill.blSubTot}}</td> <!--  bilDet.billDet.bdTotAmt | number : 2 -->
			<td style="border:1px solid #000000;">DL</td>
			<td style="border:1px solid #000000;">{{bill.cust.stateRelPreFix}}</td>
			<td style="border:1px solid #000000;">{{ bilDet.cnmt.cnmtInvoiceNo[0].invoiceNo }}</td>
			<td style="border:1px solid #000000;">{{ bilDet.cnmt.cnmtInvoiceNo[0].date | date:'dd.MM.yyyy'}}</td>
			<td style="border:1px solid #000000;">{{ bilDet.truckNo }}</td>
			<td style="border:1px solid #000000;">0{{ bilDet.cnmt.cnmtCode }}</td>
			<td style="border:1px solid #000000;">{{ bilDet.cnmt.cnmtDt | date:'dd.MM.yyyy' }}</td>
			<td style="border:1px solid #000000;">{{ bilDet.delQty }}</td>
			<td style="border:1px solid #000000;">{{ bilDet.delDt | date:'dd.MM.yyyy' }}</td>
			<td style="border:1px solid #000000;">{{changeCommaNumber((bilDet.billDet.bdFreight | number : 0))}}</td> <!--{{bilDet.billDet.bdRate * 1000 | number : 2}}  -->
			<td style="border:1px solid #000000;">{{changeCommaNumber((bilDet.billDet.bdFreight | number : 0))}}</td> 
			<td style="border:1px solid #000000;">3234040</td>
			<td style="border:1px solid #000000;">5075</td>
			<td style="border:1px solid #000000;">{{ changeCommaNumber(((bilDet.billDet.bdTotAmt-bilDet.billDet.bdFreight)| number : 0)) }}</td> <!-- bilDet.ovrHgt | number : 2 --> <!-- bilDet.ovrHgt | number : 0 -->
			<td style="border:1px solid #000000;">{{changeCommaNumber((bilDet.billDet.bdTotAmt | number : 0))}}</td> <!--{{ bilDet.billDet.bdTotAmt | number : 2 }}  -->
			<td style="border:1px solid #000000;">{{ bilDet.frmStn }}</td>
			<td style="border:1px solid #000000;">{{ bilDet.toStn }}</td>
			<!-- <td style="border:1px solid #000000;">
				<div ng-if="bilDet.billDet.bdBlBase == 'chargeWt'">{{bilDet.billDet.bdChgWt/1000 | number : 2}}</div>
                <div ng-if="bilDet.billDet.bdBlBase == 'actualWt'">{{bilDet.billDet.bdActWt/1000 | number : 2}}</div>
                <div ng-if="bilDet.billDet.bdBlBase == 'receiveWt'">{{bilDet.billDet.bdRecWt/1000 | number : 2}}</div>
			</td> 
			<td style="border:1px solid #000000;">{{ bilDet.cnmt.cnmtProductType }}</td>-->
			<td style="border:1px solid #000000;">{{ bilDet.vehType }}</td>
			<!-- <td style="border:1px solid #000000;">{{ bill.bill.blDesc }}</td> -->
		</tr>
	</table>
	</div>
	
	<div id="exportable" class="noprint" >
	<table ng-repeat="bill in blList" ng-show="bill.hmfBillFormat">
		<!-- <caption class="coltag tblrow">Annexure For Bill No. {{bill.bill.blBillNo}}</caption>-->
		<caption class="coltag tblrow"><b style="font-weight: bold;">Care Go Logistics PVT Ltd<br></b>
		  <b style="border:1px solid black;">Himachal Futuristic Communications Ltd. 8, Commercial Complex, Masjid Moth, Grater Kailash, Part - II, New Delhi - 110048, India</b></caption>	
		<tr style="text-align:center;">
			<th style="border:1px solid #000000;">FILE NO</th>
		    <th style="border:1px solid #000000;">FILE DATE</th>
		    <th style="border:1px solid #000000;">BILL NO</th>
		    <th style="border:1px solid #000000;">BILL DATE</th>
		    <th style="border:1px solid #000000;">BILL AMT</th>
		    <th style="border:1px solid #000000;">BILL From</th>
		    <th style="border:1px solid #000000;">BILL To</th>
		    <th style="border:1px solid #000000;">DCPI NO</th>
		    <th style="border:1px solid #000000;">DCPI DATE</th>
		    <th style="border:1px solid #000000;">TRUCK NO</th>
		    <th style="border:1px solid #000000;">LR NO</th>
		    <th style="border:1px solid #000000;">LR DATE</th>  
		    <th style="border:1px solid #000000;">DELIVERED QTY</th>
		    <th style="border:1px solid #000000;">DELIVERY DATE</th>
		    <th style="border:1px solid #000000;">RATE</th>
		    <th style="border:1px solid #000000;">AMOUNT</th>
		    <th style="border:1px solid #000000;">VENDOR CODE</th>
		    <th style="border:1px solid #000000;">COMPANY CODE</th>
		    <th style="border:1px solid #000000;">OVER HEIGHT</th>
		    <th style="border:1px solid #000000;">TOTAL</th>
		    <th style="border:1px solid #000000;">FROM</th>
		    <th style="border:1px solid #000000;">TO</th>
		 <!--    <th style="border:1px solid #000000;">WEIGHT</th>
		    <th style="border:1px solid #000000;">MATERIAL</th> -->
		    <th style="border:1px solid #000000;">TRUCK TYPE</th>
		    <!-- <th style="border:1px solid #000000;">REMARKS</th> -->
		</tr>
		
		<tr ng-repeat="bilDet in bill.blDetList" style="text-align:center;">
			<!--<td style="border:1px solid #000000;">{{ bill.bfNo }}</td>-->
			<td style="border:1px solid #000000;">{{ bill.bill.blBillNo }}</td>
			<td style="border:1px solid #000000;" ng-if="blDate == undefined || blDate==''">{{ bill.bill.blBillDt | date:'dd.MM.yyyy' }}</td>
			<td style="border:1px solid #000000;" ng-if="blDate != undefined && blDate!=''">{{ blDate | date:'dd.MM.yyyy'}}</td>
			<td style="border:1px solid #000000;">{{ bill.bill.blBillNo }}</td>
			<td style="border:1px solid #000000;" ng-if="blDate == undefined || blDate==''">{{ bill.bill.blBillDt | date:'dd.MM.yyyy' }}</td>
			<td style="border:1px solid #000000;" ng-if="blDate != undefined && blDate!=''">{{ blDate | date:'dd.MM.yyyy'}}</td>
			<td style="border:1px solid #000000;">{{bill.bill.blSubTot}}</td> <!--  bilDet.billDet.bdTotAmt | number : 2 -->
			<td style="border:1px solid #000000;">GJ</td>
			<td style="border:1px solid #000000;">{{bilDet.toStnStatePre}}</td>
			<td style="border:1px solid #000000;">'{{ bilDet.cnmt.cnmtInvoiceNo[0].invoiceNo }}</td>
			<td style="border:1px solid #000000;">{{ bilDet.cnmt.cnmtInvoiceNo[0].date | date:'dd.MM.yyyy'}}</td>
			<td style="border:1px solid #000000;">{{ bilDet.truckNo }}</td>
			<td style="border:1px solid #000000;">0{{ bilDet.cnmt.cnmtCode }}</td>
			<td style="border:1px solid #000000;">{{ bilDet.cnmt.cnmtDt | date:'dd.MM.yyyy' }}</td>
			<td style="border:1px solid #000000;">{{ bilDet.delQty }}</td>
			<td style="border:1px solid #000000;">{{ bilDet.delDt | date:'dd.MM.yyyy' }}</td>
			<td style="border:1px solid #000000;">{{changeCommaNumber((bilDet.billDet.bdFreight | number : 0))}}</td> <!--{{bilDet.billDet.bdRate * 1000 | number : 2}}  -->
			<td style="border:1px solid #000000;">{{changeCommaNumber((bilDet.billDet.bdFreight | number : 0))}}</td> 
			<td style="border:1px solid #000000;">3234040</td>
			<td style="border:1px solid #000000;">5075</td>
			<td style="border:1px solid #000000;">{{ changeCommaNumber(((bilDet.billDet.bdTotAmt-bilDet.billDet.bdFreight)| number : 0)) }}</td> <!-- bilDet.ovrHgt | number : 2 --> <!-- bilDet.ovrHgt | number : 0 -->
			<td style="border:1px solid #000000;">{{changeCommaNumber((bilDet.billDet.bdTotAmt | number : 0))}}</td> <!--{{ bilDet.billDet.bdTotAmt | number : 2 }}  -->
			<td style="border:1px solid #000000;">Hazira</td>
			<td style="border:1px solid #000000;">{{ bilDet.toStn }}</td>
			<!-- <td style="border:1px solid #000000;">
				<div ng-if="bilDet.billDet.bdBlBase == 'chargeWt'">{{bilDet.billDet.bdChgWt/1000 | number : 2}}</div>
                <div ng-if="bilDet.billDet.bdBlBase == 'actualWt'">{{bilDet.billDet.bdActWt/1000 | number : 2}}</div>
                <div ng-if="bilDet.billDet.bdBlBase == 'receiveWt'">{{bilDet.billDet.bdRecWt/1000 | number : 2}}</div>
			</td> 
			<td style="border:1px solid #000000;">{{ bilDet.cnmt.cnmtProductType }}</td>-->
			<td style="border:1px solid #000000;">{{ bilDet.vehType }}</td>
			<!-- <td style="border:1px solid #000000;">{{ bill.bill.blDesc }}</td> -->
		</tr>
	</table>
	</div>
	
 </div>    
           