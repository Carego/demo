package com.mylogistics.mobileApi.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
//import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.mylogistics.DAO.OwnerDAO;
import com.mylogistics.DAO.VehicleVendorDAO;
import com.mylogistics.mobileApi.controller.model.VehicleDataFrmAPI;
import com.mylogistics.mobileApi.controller.model.VehicleVendorMstr;
import com.mylogistics.model.Owner;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class VehicleVendorMstrCntlr {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private VehicleVendorDAO vehicleVendorDAO;
	
	@Autowired
	private OwnerDAO ownerDAO;
	
	private static final String POST_URL = "https://preprod.aadhaarapi.com/verify-rc";

	
	/*private String vehRcImgPath = "/var/www/html/Erp_Image/Vehicle/Test/RC";
	private String vehPolicyPath = "/var/www/html/Erp_Image/Vehicle/Test/Policy";
	private String vehPSImgPath = "/var/www/html/Erp_Image/Vehicle/Test/PermitSlip";
	private String vehPollutionImgPath = "/var/www/html/Erp_Image/Vehicle/Test/Pollution";
	*/private String vehDocsImgPath = "/var/www/html/Erp_Image/Vehicle/Test/Docs";

	
	
	@RequestMapping(value = "/submitVehilceDetails", method = RequestMethod.POST)
	public  Object submitVehilceDetails(@RequestBody VehicleVendorMstr vehicleVendorMstr) {
		Map<String, Object> resultMap=new HashMap<>();
		System.out.println("hi");
		
		com.mylogistics.model.VehicleVendorMstr vvm=vehicleVendorDAO.getVehByVehNo(vehicleVendorMstr.getVvRcNo());
		
		
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		try {
			Owner owner=ownerDAO.getOwnByCode(vehicleVendorMstr.getOwnCode(),session);
			
			if(vvm != null) {
				resultMap.put("isSuccess", false);
				resultMap.put("message", "Vehicle already exist");
			}else {
				
				com.mylogistics.model.VehicleVendorMstr vv=new com.mylogistics.model.VehicleVendorMstr();
				vv.setVvChassisNo(vehicleVendorMstr.getVvChassisNo());
				vv.setVvEngineNo(vehicleVendorMstr.getVvEngineNo());
				vv.setVvFitValidDt(vehicleVendorMstr.getVvFitValidDt());
				vv.setVvHypoTo(vehicleVendorMstr.getVvHypoTo());
				vv.setVvModel(vehicleVendorMstr.getVvModel());
				vv.setVvModelNo(vehicleVendorMstr.getVvModelNo());
				vv.setVvPerIssueDt(vehicleVendorMstr.getVvPerIssueDt());
				vv.setVvPermitType(vehicleVendorMstr.getVvPermitType());
				vv.setVvPerNo(vehicleVendorMstr.getVvPerNo());
				vv.setVvPerValidDt(vehicleVendorMstr.getVvPerValidDt());
				vv.setVvPerState(vehicleVendorMstr.getVvPerState());
				vv.setVvPolicyComp(vehicleVendorMstr.getVvPolicyComp());
				vv.setVvPolicyValidDt(vehicleVendorMstr.getVvPolicyValidDt());
				vv.setVvPolicyNo(vehicleVendorMstr.getVvPolicyNo());
				vv.setVvPollutionNorms(vehicleVendorMstr.getVvPollutionNorms());
				vv.setVvRcIssueDt(vehicleVendorMstr.getVvRcIssueDt());
				vv.setVvRcNo(vehicleVendorMstr.getVvRcNo());
				vv.setVvRcValidDt(vehicleVendorMstr.getVvRcValidDt());
				vv.setVvType(vehicleVendorMstr.getVvType());
				vv.setVvPollutionUptoDate(vehicleVendorMstr.getVvPollutionUptoDate());
				vv.setVvPerSlpImgPath(vehicleVendorMstr.getVvPerSlpImgPath());
				vv.setVvPolicyImgPath(vehicleVendorMstr.getVvPolicyImgPath());
				vv.setVvPollutionImgPath(vehicleVendorMstr.getVvPollutionImgPath());
				vv.setVvRcImgPath(vehicleVendorMstr.getVvRcImgPath());
				vv.setOwner(owner);
				
				int vvId=vehicleVendorDAO.saveVehicleVendorMstr(session, vv);
				
				
				/*if(owner.getVehicleVendorMstrList()==null) {
					
					List<com.mylogistics.model.VehicleVendorMstr>vvList=new ArrayList<>();
					vvList.add(vv);
					owner.setVehicleVendorMstrList(vvList);
				}else {*/
					owner.getVehicleVendorMstrList().add(vv);
				//}
				session.update(owner);
				
				
				session.flush();
				session.clear();
				transaction.commit();
				resultMap.put("isSuccess", true);
				resultMap.put("vvId", vvId);
				resultMap.put("message", "Vehicle detail successfully saved!");
			}
			
		}catch(Exception e) {
			e.printStackTrace();
			transaction.rollback();
			resultMap.put("isSuccess", false);
			resultMap.put("message", "Retry!");
		}finally {
			session.close();
		}
		
		return resultMap;
	}
	
	
	/*@RequestMapping(value = "/uploadVehilceDocs", method = RequestMethod.POST)
	public  Object uploadVehilceDocs(@RequestParam("file") MultipartFile file,@RequestParam("vvId") int vvId, @RequestParam("doc_name") String docName) {
		
		System.out.println("file name = " + file.getContentType());
		System.out.println("original file name = " + file.getOriginalFilename());
		String fileType = file.getOriginalFilename().split("\\.")[file.getOriginalFilename().split("\\.").length - 1];
		System.out.println("file type = " + fileType);
		
		Map<String, Object> resultMap=new HashMap<>();
		
		com.TestServer.model.VehicleVendorMstr vehicleVendorMstr=vehicleVendorDAO.getVehicleVendorMstrById(vvId);
		
		if (vehicleVendorMstr != null) {
			
			Session session=sessionFactory.openSession();
			Transaction transaction=session.beginTransaction();

			try {

				if (docName.equalsIgnoreCase("RC")) {

					Path rcPath = Paths.get(vehRcImgPath);
					if (!Files.exists(rcPath))
						Files.createDirectories(rcPath);

					byte[] fileInBytes = file.getBytes();
					File rcFile = new File(vehRcImgPath + "/RC" + vehicleVendorMstr.getVvId() + "." + fileType);
					if (rcFile.exists())
						rcFile.delete();

					FileOutputStream out = new FileOutputStream(rcFile);
					out.write(fileInBytes);
					out.close();

					vehicleVendorMstr
							.setVvRcImgPath(vehRcImgPath + "/RC" + vehicleVendorMstr.getVvId() + "." + fileType);

				} else if (docName.equalsIgnoreCase("INSURANCE")) {

					Path policyPath = Paths.get(vehPolicyPath);
					if (!Files.exists(policyPath))
						Files.createDirectories(policyPath);

					byte[] fileInBytes = file.getBytes();
					File insuranceFile = new File(
							vehPolicyPath + "/INSUR" + vehicleVendorMstr.getVvId() + "." + fileType);
					if (insuranceFile.exists())
						insuranceFile.delete();

					FileOutputStream out = new FileOutputStream(insuranceFile);
					out.write(fileInBytes);
					out.close();

					vehicleVendorMstr.setVvPolicyImgPath(
							vehPolicyPath + "/INSUR" + vehicleVendorMstr.getVvId() + "." + fileType);

				} else if (docName.equalsIgnoreCase("PERMIT")) {

					Path psPath = Paths.get(vehPSImgPath);
					if (!Files.exists(psPath))
						Files.createDirectories(psPath);

					byte[] fileInBytes = file.getBytes();
					File psFile = new File(vehPSImgPath + "/PS" + vehicleVendorMstr.getVvId() + "." + fileType);
					if (psFile.exists())
						psFile.delete();

					FileOutputStream out = new FileOutputStream(psFile);
					out.write(fileInBytes);
					out.close();

					vehicleVendorMstr
							.setVvPerSlpImgPath(vehPSImgPath + "/PS" + vehicleVendorMstr.getVvId() + "." + fileType);

				} else if (docName.equalsIgnoreCase("POLLUTION")) {

					Path pollutionPath = Paths.get(vehPollutionImgPath);
					if (!Files.exists(pollutionPath))
						Files.createDirectories(pollutionPath);

					byte[] fileInBytes = file.getBytes();
					File polFile = new File(
							vehPollutionImgPath + "/POLLUTION" + vehicleVendorMstr.getVvId() + "." + fileType);
					if (polFile.exists())
						polFile.delete();

					FileOutputStream out = new FileOutputStream(polFile);
					out.write(fileInBytes);
					out.close();

					vehicleVendorMstr.setVvPollutionImgPath(
							vehPollutionImgPath + "/POLLUTION" + vehicleVendorMstr.getVvId() + "." + fileType);

				} else {
					resultMap.put("isSuccess", false);
					resultMap.put("message", "Please select valid document!");
					return resultMap;
				}
				
				vehicleVendorDAO.updateVehicleVendorMstr(session, vehicleVendorMstr);
				
				session.flush();
				session.clear();
				transaction.commit();
				resultMap.put("isSuccess", true);
				resultMap.put("message", "Image successfully saved!");
				
			} catch (Exception e) {
				System.out.println("Error in creating direcotry : " + e);
				transaction.rollback();
				resultMap.put("isSuccess", false);
				resultMap.put("message", "Retry!");
			}finally {
				session.close();
			}

		}else {
			resultMap.put("isSuccess", false);
			resultMap.put("message", "Vehicle not found!");
		}
		
		return resultMap;
	}
	*/
	
	
	@RequestMapping(value = "/uploadVehilceDocs", method = RequestMethod.POST)
	public  Object uploadVehilceDocs(@RequestParam("file") MultipartFile file) {
		
		System.out.println("file name = " + file.getContentType());
		System.out.println("original file name = " + file.getOriginalFilename());
		String fileType = file.getOriginalFilename().split("\\.")[file.getOriginalFilename().split("\\.").length - 1];
		System.out.println("file type = " + fileType);
		
		Map<String, Object> resultMap=new HashMap<>();
		
			try {


					Path rcPath = Paths.get(vehDocsImgPath);
					if (!Files.exists(rcPath))
						Files.createDirectories(rcPath);

					byte[] fileInBytes = file.getBytes();
					File rcFile = new File(vehDocsImgPath + "/" +file.getOriginalFilename() );
					if (rcFile.exists())
						rcFile.delete();

					FileOutputStream out = new FileOutputStream(rcFile);
					out.write(fileInBytes);
					out.close();

					resultMap.put("imagePath", vehDocsImgPath + "/"+file.getOriginalFilename());
					resultMap.put("isSuccess", true);
					resultMap.put("message", "Image successfully uploaded");

				
			} catch (Exception e) {
				System.out.println("Error in creating direcotry : " + e);
				resultMap.put("isSuccess", false);
				resultMap.put("message", "Retry!");
			}

		
		
		return resultMap;
	}
	
	
	
	@RequestMapping(value = "/checkVehicleExistByRC", method=RequestMethod.POST)
	public @ResponseBody Object checkVehicleExistByRC(@RequestBody Map<String,String> vehicleMap){
		System.out.println("Entered into checkVehicleExist");
		Map<String, Object> map = new HashMap<>();
		String rcNo=vehicleMap.get("rcNo");
		boolean flag;
		flag=vehicleVendorDAO.checkVehicleExistByRc(rcNo);
		
		if(flag) {
			map.put("isSuccess", false);
			map.put("message", "Vehicle number already exist");
		}else {
			map.put("isSuccess", true);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/getVehicleDetailByRCNoFromAPI", method=RequestMethod.POST)
	public @ResponseBody Object getVehicleDetailByRCNoFromAPI(@RequestBody Map<String,String> vehicleMap){
		System.out.println("Entered into getVehicleDetailByRCNoFromAPI");
		Map<String, Object> map = new HashMap<>();
		try {
			String lorryNo=vehicleMap.get("rcNo");
			
			com.mylogistics.model.VehicleVendorMstr vehicleVendorMstr=vehicleVendorDAO.getVehByVehNo(lorryNo);
			
			if(vehicleVendorMstr == null) {
				
				
				
				/*String sb="{\"id\":\"d1d3e622-0fec-4137-bee3-4763f9a244d8\",\"env\":1,\r\n" + 
						"\"request_timestamp\":\"2019-02-02 15:05:27:872 +05:30\",\r\n" + 
						"\"response_timestamp\":\"2019-02-02 15:05:34:845 +05:30\",\r\n" + 
						"\"transaction_status\":1,\r\n" + 
						"\"result\":{\"rc_body_type_desc\":\"TRUCK (CLOSED BODY)\",\r\n" + 
						"\"rc_chasi_no\":\"MAT448202F5F08431\",\r\n" + 
						"\"rc_color\":\"NA\",\r\n" + 
						"\"rc_cubic_cap\":\"0.00\",\r\n" + 
						"\"rc_eng_no\":\"51F84234689\",\r\n" + 
						"\"rc_f_name\":\"NA\",\r\n" + 
						"\"rc_financer\":\"KOTAK MAHINDRA BANK LTD\",\r\n" + 
						"\"rc_fit_upto\":\"17-Aug-2019\",\r\n" + 
						"\"rc_fuel_desc\":\"DIESEL\",\r\n" + 
						"\"rc_gvw\":\"25000\",\r\n" + 
						"\"rc_insurance_comp\":\"Universal Sompo General Insurance  Co. Ltd.\",\r\n" + 
						"\"rc_insurance_policy_no\":\"2315/58660241/00/000\",\r\n" + 
						"\"rc_insurance_upto\":\"05-Jul-2019\",\r\n" + 
						"\"rc_maker_desc\":\"TATA MOTORS LTD\",\r\n" + 
						"\"rc_maker_model\":\"TATA MOTORS LTD, LPT 2518\",\r\n" + 
						"\"rc_manu_month_yr\":\"6/2015\",\r\n" + 
						"\"rc_mobile_no\":\"9798965423\",\r\n" + 
						"\"rc_no_cyl\":\"6\",\r\n" + 
						"\"rc_norms_desc\":\"BHARAT STAGE II\",\r\n" + 
						"\"rc_np_issued_by\":\"Secretary RTA, GURGAON\",\r\n" + 
						"\"rc_np_no\":\"NP/HR/55/082018/25845\",\r\n" + 
						"\"rc_np_upto\":\"19-Aug-2019\",\r\n" + 
						"\"rc_owner_name\":\"M/S CARE GO LOGISTICS PVT LTD\",\r\n" + 
						"\"rc_owner_sr\":\"1\",\r\n" + 
						"\"rc_permanent_address\":\"A 115 PH 1 SUSHANT LOK, GURGAON, GURGAON, -122001\",\r\n" + 
						"\"rc_present_address\":\"A 115 PH 1 SUSHANT LOK, GURGAON, GURGAON, -122001\",\r\n" + 
						"\"rc_registered_at\":\"RTA, GURGAON, Haryana\",\r\n" + 
						"\"rc_regn_dt\":\"20-Aug-2015\",\r\n" + 
						"\"rc_regn_no\":\"HR55V6542\",\r\n" + 
						"\"rc_seat_cap\":\"3\",\r\n" + 
						"\"rc_sleeper_cap\":\"0\",\r\n" + 
						"\"rc_stand_cap\":\"\",\r\n" + 
						"\"rc_status_as_on\":\"02-Feb-2019\",\r\n" + 
						"\"rc_tax_upto\":\"31-Mar-2019\",\r\n" + 
						"\"rc_unld_wt\":\"9000\",\r\n" + 
						"\"rc_vch_catg\":\"HGV\",\r\n" + 
						"\"rc_vh_class_desc\":\"Goods Carrier\",\r\n" + 
						"\"rc_wheelbase\":\"0\",\r\n" + 
						"\"stautsMessage\":\"OK\"},\r\n" + 
						"\"response_msg\":\"Valid Authentication\",\r\n" + 
						"\"response_code\":\"101\"}";*/
				//ObjectMapper mapper = new ObjectMapper();


				// convert JSON string to Map
				//Map<String, Object> newMap=new HashMap<>();
				//newMap = mapper.readValue(sb, new TypeReference<Map<String, Object>>(){});

				
/*				ObjectMapper mapper = new ObjectMapper();

				Map<String, Object> newMap = new HashMap<String, Object>();
				Map<String, Object> apiMap = new HashMap<String, Object>();
				// convert JSON string to Map
				apiMap = mapper.readValue(sb, new TypeReference<Map<String, Object>>(){});
				
				
				newMap.putAll((Map<? extends String, ? extends Object>) apiMap.get("result"));
				
				
				com.TestServer.model.VehicleDataFrmAPI apiData=new com.TestServer.model.VehicleDataFrmAPI();
					
					apiData.setEnv((int) apiMap.get("env"));
					apiData.setId((String) apiMap.get("id"));
					apiData.setRequest_timestamp((String) apiMap.get("request_timestamp"));
					apiData.setResponse_code((String) apiMap.get("response_code"));
					apiData.setResponse_msg((String) apiMap.get("response_msg"));
					apiData.setResponse_timestamp((String) apiMap.get("response_timestamp"));
					apiData.setStautsMessage((String) apiMap.get("stautsMessage"));
					apiData.setTransaction_status((int) apiMap.get("transaction_status"));
					apiData.setRc_body_type_desc((String) newMap.get("rc_body_type_desc"));
					apiData.setRc_chasi_no((String) newMap.get("rc_chasi_no"));
					apiData.setRc_color((String) newMap.get("rc_color"));
					apiData.setRc_cubic_cap((String) newMap.get("rc_cubic_cap"));
					apiData.setRc_eng_no((String) newMap.get("rc_eng_no"));
					apiData.setRc_f_name((String) newMap.get("rc_f_name"));
					apiData.setRc_financer((String) newMap.get("rc_financer"));
					apiData.setRc_fit_upto((String) newMap.get("rc_fit_upto"));
					apiData.setRc_fuel_desc((String) newMap.get("rc_fuel_desc"));
					apiData.setRc_gvw((String) newMap.get("rc_gvw"));
					apiData.setRc_insurance_comp((String) newMap.get("rc_insurance_comp"));
					apiData.setRc_insurance_policy_no((String) newMap.get("rc_insurance_policy_no"));
					apiData.setRc_insurance_upto((String) newMap.get("rc_insurance_upto"));
					apiData.setRc_maker_desc((String) newMap.get("rc_maker_desc"));
					apiData.setRc_maker_model((String) newMap.get("rc_maker_model"));
					apiData.setRc_manu_month_yr((String) newMap.get("rc_manu_month_yr"));
					apiData.setRc_mobile_no((String) newMap.get("rc_mobile_no"));
					apiData.setRc_no_cyl((String) newMap.get("rc_no_cyl"));
					apiData.setRc_norms_desc((String) newMap.get("rc_norms_desc"));
					apiData.setRc_np_issued_by((String) newMap.get("rc_np_issued_by"));
					apiData.setRc_np_no((String) newMap.get("rc_np_no"));
					apiData.setRc_np_upto((String) newMap.get("rc_np_upto"));
					apiData.setRc_owner_name((String) newMap.get("rc_owner_name"));
					apiData.setRc_owner_sr((String) newMap.get("rc_owner_sr"));
					apiData.setRc_permanent_address((String) newMap.get("rc_permanent_address"));
					apiData.setRc_present_address((String) newMap.get("rc_present_address"));
					apiData.setRc_registered_at((String) newMap.get("rc_registered_at"));
					apiData.setRc_regn_dt((String) newMap.get("rc_regn_dt"));
					apiData.setRc_regn_no((String) newMap.get("rc_regn_no"));
					apiData.setRc_seat_cap((String) newMap.get("rc_seat_cap"));
					apiData.setRc_sleeper_cap((String) newMap.get("rc_sleeper_cap"));
					apiData.setRc_stand_cap((String) newMap.get("rc_stand_cap"));
					apiData.setRc_status_as_on((String) newMap.get("rc_status_as_on"));
					apiData.setRc_tax_upto((String) newMap.get("rc_tax_upto"));
					apiData.setRc_unld_wt((String) newMap.get("rc_unld_wt"));
					apiData.setRc_vch_catg((String) newMap.get("rc_vch_catg"));
					apiData.setRc_vh_class_desc((String) newMap.get("rc_vh_class_desc"));
					apiData.setRc_wheelbase((String) newMap.get("rc_wheelbase"));
					
					int vid=vehicleVendorDAO.saveVehicleDataFrmAPI(apiData);
					
					
					SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MMM-yyyy");
					
					
					VehicleVendorMstr vv=new VehicleVendorMstr();
					//vv.setOwnCode(vehicleVendorMstr.getOwner().getOwnCode());
					vv.setVvChassisNo((String) newMap.get("rc_chasi_no"));
					vv.setVvEngineNo((String) newMap.get("rc_eng_no"));
					
					if(newMap.get("rc_fit_upto")!=null) {
					
					java.util.Date date = sdf1.parse((String)newMap.get("rc_fit_upto"));
					java.sql.Date fitUpto = new java.sql.Date(date.getTime());
					vv.setVvFitValidDt(fitUpto);
					}
					
					
					vv.setVvHypoTo((String) newMap.get("rc_financer"));
					//vv.setVvId(vehicleVendorMstr.getVvId());
					if(newMap.get("rc_manu_month_yr") !=null) {
					String model=newMap.get("rc_manu_month_yr").toString();
					
					
						String ar[]=model.split("/");
						System.out.println(ar[1]);
					
					vv.setVvModel( Integer.parseInt(ar[1]));
					}
					vv.setVvModelNo((String) newMap.get("rc_maker_model"));
					//vv.setVvPerIssueDt(vehicleVendorMstr.getVvPerIssueDt());
					//vv.setVvPermitType(vehicleVendorMstr.getVvPermitType());
					//vv.setVvPerNo(vehicleVendorMstr.getVvPerNo());
					//vv.setVvPerSlpImgPath(vehicleVendorMstr.getVvPerSlpImgPath());
					//vv.setVvPerState(vehicleVendorMstr.getVvPerState());
					//vv.setVvPerValidDt(vehicleVendorMstr.getVvPerValidDt());
					vv.setVvPolicyComp((String) newMap.get("rc_insurance_comp"));
					//vv.setVvPolicyImgPath(vehicleVendorMstr.getVvPolicyImgPath());
					vv.setVvPolicyNo((String) newMap.get("rc_insurance_policy_no"));
					
					if(newMap.get("rc_insurance_upto")!=null) {
						java.util.Date ins = sdf1.parse((String)newMap.get("rc_insurance_upto"));
						java.sql.Date insUpto = new java.sql.Date(ins.getTime());
						vv.setVvPolicyValidDt(insUpto);
					}
					
					
					
					//vv.setVvPollutionImgPath(vehicleVendorMstr.getVvPollutionImgPath());
					vv.setVvPollutionNorms((String) newMap.get("rc_norms_desc"));
					//vv.setVvPollutionUptoDate(vehicleVendorMstr.getVvPollutionUptoDate());
					//vv.setVvRcImgPath(vehicleVendorMstr.getVvRcImgPath());
					if(newMap.get("rc_regn_dt")!=null) {
						java.util.Date reg = sdf1.parse((String)newMap.get("rc_regn_dt"));
						java.sql.Date regDt = new java.sql.Date(reg.getTime());
						
						vv.setVvRcIssueDt(regDt);
					}
					
					vv.setVvRcNo((String) newMap.get("rc_regn_no"));
					//vv.setVvRcValidDt(vehicleVendorMstr.getVvRcValidDt());
					//vv.setVvTransferDt(vehicleVendorMstr.getVvTransferDt());
					//vv.setVvTransferId(vehicleVendorMstr.getVvTransferId());
					//vv.setVvTransitPassNo(vehicleVendorMstr.getVvTransitPassNo());
					vv.setVvType((String) newMap.get("rc_vch_catg"));
					
					
					
					System.out.println("vid="+vid);
					map.put("isSuccess", true);
					map.put("data", vv);
				return map;
				
				
				*/
				
				
			
				
				
								
				
				String content="Y";
				String contentMsg="CAREGO LOGISTIC PRIVATE LIMITED";
				
				String POST_PARAMS = "{\r\n" + 
						"    \"reg_no\":\""+lorryNo+"\",\r\n" + 
						"    \"consent\":\""+content+"\",\r\n" + 
						"    \"consent_text\":\""+contentMsg+"\"\r\n" + 
						"}";
				
				
				URL obj = new URL(POST_URL);
				HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
				con.setRequestMethod("POST");
				con.setRequestProperty("Content-Type", "application/json");
				con.setRequestProperty("qt_api_key", "1a645d15-2756-4617-8872-6ad99bc942b5");
				con.setRequestProperty("qt_agency_id", "bfeb3573-3829-4415-afe6-f687b7c0cac2");
				

				// For POST only - START
				con.setDoOutput(true);
				OutputStream os = con.getOutputStream();
				os.write(POST_PARAMS.getBytes());
				os.flush();
				os.close();
				// For POST only - END

				int responseCode = con.getResponseCode();
				System.out.println("POST Response Code :: " + responseCode);
				System.out.println("POST Response Code :: " + con.getResponseMessage());

				
				
				if (responseCode == HttpURLConnection.HTTP_OK) { //success
					BufferedReader in = new BufferedReader(new InputStreamReader(
							con.getInputStream()));
					String inputLine;
					StringBuffer response = new StringBuffer();

					while ((inputLine = in.readLine()) != null) {
						response.append(inputLine);
					}
					in.close();

					// print result
					System.out.println(response.toString());
					
					
					ObjectMapper mapper = new ObjectMapper();

					Map<String, Object> newMap = new HashMap<String, Object>();
					Map<String, Object> apiMap = new HashMap<String, Object>();
					// convert JSON string to Map
					apiMap = mapper.readValue(response.toString(), new TypeReference<Map<String, Object>>(){});
					
					System.out.println("apiMap"+apiMap);
					
					if(apiMap.get("result")==null) {
						map.put("isSuccess", false);
						map.put("data", null);
						map.put("message", "Please enter valid vehicle No.");
						return map;
					}
					
					newMap.putAll((Map<? extends String, ? extends Object>) apiMap.get("result"));
					System.out.println("newMap"+newMap);
					
					
					com.mylogistics.model.VehicleDataFrmAPI apiData=new com.mylogistics.model.VehicleDataFrmAPI();
					
					apiData.setEnv((int) apiMap.get("env"));
					apiData.setId((String) apiMap.get("id"));
					apiData.setRequest_timestamp((String) apiMap.get("request_timestamp"));
					apiData.setResponse_code((String) apiMap.get("response_code"));
					apiData.setResponse_msg((String) apiMap.get("response_msg"));
					apiData.setResponse_timestamp((String) apiMap.get("response_timestamp"));
					apiData.setStautsMessage((String) apiMap.get("stautsMessage"));
					apiData.setTransaction_status((int) apiMap.get("transaction_status"));
					apiData.setRc_body_type_desc((String) newMap.get("rc_body_type_desc"));
					apiData.setRc_chasi_no((String) newMap.get("rc_chasi_no"));
					apiData.setRc_color((String) newMap.get("rc_color"));
					apiData.setRc_cubic_cap((String) newMap.get("rc_cubic_cap"));
					apiData.setRc_eng_no((String) newMap.get("rc_eng_no"));
					apiData.setRc_f_name((String) newMap.get("rc_f_name"));
					apiData.setRc_financer((String) newMap.get("rc_financer"));
					apiData.setRc_fit_upto((String) newMap.get("rc_fit_upto"));
					apiData.setRc_fuel_desc((String) newMap.get("rc_fuel_desc"));
					apiData.setRc_gvw((String) newMap.get("rc_gvw"));
					apiData.setRc_insurance_comp((String) newMap.get("rc_insurance_comp"));
					apiData.setRc_insurance_policy_no((String) newMap.get("rc_insurance_policy_no"));
					apiData.setRc_insurance_upto((String) newMap.get("rc_insurance_upto"));
					apiData.setRc_maker_desc((String) newMap.get("rc_maker_desc"));
					apiData.setRc_maker_model((String) newMap.get("rc_maker_model"));
					apiData.setRc_manu_month_yr((String) newMap.get("rc_manu_month_yr"));
					apiData.setRc_mobile_no((String) newMap.get("rc_mobile_no"));
					apiData.setRc_no_cyl((String) newMap.get("rc_no_cyl"));
					apiData.setRc_norms_desc((String) newMap.get("rc_norms_desc"));
					apiData.setRc_np_issued_by((String) newMap.get("rc_np_issued_by"));
					apiData.setRc_np_no((String) newMap.get("rc_np_no"));
					apiData.setRc_np_upto((String) newMap.get("rc_np_upto"));
					apiData.setRc_owner_name((String) newMap.get("rc_owner_name"));
					apiData.setRc_owner_sr((String) newMap.get("rc_owner_sr"));
					apiData.setRc_permanent_address((String) newMap.get("rc_permanent_address"));
					apiData.setRc_present_address((String) newMap.get("rc_present_address"));
					apiData.setRc_registered_at((String) newMap.get("rc_registered_at"));
					apiData.setRc_regn_dt((String) newMap.get("rc_regn_dt"));
					apiData.setRc_regn_no((String) newMap.get("rc_regn_no"));
					apiData.setRc_seat_cap((String) newMap.get("rc_seat_cap"));
					apiData.setRc_sleeper_cap((String) newMap.get("rc_sleeper_cap"));
					apiData.setRc_stand_cap((String) newMap.get("rc_stand_cap"));
					apiData.setRc_status_as_on((String) newMap.get("rc_status_as_on"));
					apiData.setRc_tax_upto((String) newMap.get("rc_tax_upto"));
					apiData.setRc_unld_wt((String) newMap.get("rc_unld_wt"));
					apiData.setRc_vch_catg((String) newMap.get("rc_vch_catg"));
					apiData.setRc_vh_class_desc((String) newMap.get("rc_vh_class_desc"));
					apiData.setRc_wheelbase((String) newMap.get("rc_wheelbase"));
					
					int vid=vehicleVendorDAO.saveVehicleDataFrmAPI(apiData);
					
					System.out.println("vid="+vid);
					
					SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MMM-yyyy");
					
					
					VehicleVendorMstr vv=new VehicleVendorMstr();
					//vv.setOwnCode(vehicleVendorMstr.getOwner().getOwnCode());
					vv.setVvChassisNo((String) newMap.get("rc_chasi_no"));
					vv.setVvEngineNo((String) newMap.get("rc_eng_no"));
					
					java.util.Date date = sdf1.parse((String)newMap.get("rc_fit_upto"));
					java.sql.Date fitUpto = new java.sql.Date(date.getTime());
					
					
					vv.setVvFitValidDt(fitUpto);
					vv.setVvHypoTo((String) newMap.get("rc_financer"));
					//vv.setVvId(vehicleVendorMstr.getVvId());
					
					if(newMap.get("rc_manu_month_yr") !=null) {
					String model=newMap.get("rc_manu_month_yr").toString();
					
						String ar[]=model.split("/");
						System.out.println(ar[1]);
					
					vv.setVvModel( Integer.parseInt(ar[1]));
					}
					vv.setVvModelNo((String) newMap.get("rc_maker_model"));
					vv.setVvPolicyComp((String) newMap.get("rc_insurance_comp"));
					vv.setVvPolicyNo((String) newMap.get("rc_insurance_policy_no"));
					
					java.util.Date ins = sdf1.parse((String)newMap.get("rc_insurance_upto"));
					java.sql.Date insUpto = new java.sql.Date(ins.getTime());
					
					vv.setVvPolicyValidDt(insUpto);
					vv.setVvPollutionNorms((String) newMap.get("rc_norms_desc"));
					java.util.Date reg = sdf1.parse((String)newMap.get("rc_regn_dt"));
					java.sql.Date regDt = new java.sql.Date(reg.getTime());
					
					vv.setVvRcIssueDt(regDt);
					vv.setVvRcNo((String) newMap.get("rc_regn_no"));
					//vv.setVvRcValidDt(vehicleVendorMstr.getVvRcValidDt());
					//vv.setVvTransitPassNo(vehicleVendorMstr.getVvTransitPassNo());
					vv.setVvType((String) newMap.get("rc_vch_catg"));
					
					
					map.put("isSuccess", true);
					map.put("data", vv);
					map.put("message", con.getResponseMessage());
					
					
				} else {
					System.out.println("POST request not worked");
					map.put("isSuccess", false);
					map.put("data", null);
					map.put("message", con.getResponseMessage());
				}

				
				
			}else {
				
				
				VehicleVendorMstr vv=new VehicleVendorMstr();
				vv.setOwnCode(vehicleVendorMstr.getOwner().getOwnCode());
				vv.setVvChassisNo(vehicleVendorMstr.getVvChassisNo());
				vv.setVvEngineNo(vehicleVendorMstr.getVvEngineNo());
				vv.setVvFitValidDt(vehicleVendorMstr.getVvFitValidDt());
				vv.setVvHypoTo(vehicleVendorMstr.getVvHypoTo());
				vv.setVvId(vehicleVendorMstr.getVvId());
				vv.setVvModel(vehicleVendorMstr.getVvModel());
				vv.setVvModelNo(vehicleVendorMstr.getVvModelNo());
				vv.setVvPerIssueDt(vehicleVendorMstr.getVvPerIssueDt());
				vv.setVvPermitType(vehicleVendorMstr.getVvPermitType());
				vv.setVvPerNo(vehicleVendorMstr.getVvPerNo());
				vv.setVvPerSlpImgPath(vehicleVendorMstr.getVvPerSlpImgPath());
				vv.setVvPerState(vehicleVendorMstr.getVvPerState());
				vv.setVvPerValidDt(vehicleVendorMstr.getVvPerValidDt());
				vv.setVvPolicyComp(vehicleVendorMstr.getVvPolicyComp());
				vv.setVvPolicyImgPath(vehicleVendorMstr.getVvPolicyImgPath());
				vv.setVvPolicyNo(vehicleVendorMstr.getVvPolicyNo());
				vv.setVvPolicyValidDt(vehicleVendorMstr.getVvPolicyValidDt());
				vv.setVvPollutionImgPath(vehicleVendorMstr.getVvPollutionImgPath());
				vv.setVvPollutionNorms(vehicleVendorMstr.getVvPollutionNorms());
				vv.setVvPollutionUptoDate(vehicleVendorMstr.getVvPollutionUptoDate());
				vv.setVvRcImgPath(vehicleVendorMstr.getVvRcImgPath());
				vv.setVvRcIssueDt(vehicleVendorMstr.getVvRcIssueDt());
				vv.setVvRcNo(vehicleVendorMstr.getVvRcNo());
				vv.setVvRcValidDt(vehicleVendorMstr.getVvRcValidDt());
				vv.setVvTransferDt(vehicleVendorMstr.getVvTransferDt());
				vv.setVvTransferId(vehicleVendorMstr.getVvTransferId());
				vv.setVvTransitPassNo(vehicleVendorMstr.getVvTransitPassNo());
				vv.setVvType(vehicleVendorMstr.getVvType());
				
				map.put("isSuccess", false);
				map.put("data", null);
				map.put("message", "Vehicle already registered");
				
				
			}
			
			
		}catch(Exception e) {
			e.printStackTrace();
			map.put("isSuccess", false);
			map.put("data", null);
			map.put("message", "Retry");
		}
		
				return map;
	}
	
	

}
