'use strict';

var app = angular.module('application');

app.controller('IndentCntlr',['$scope','$location','$http','$filter','$sce','$window',
                              function($scope,$location,$http,$filter,$sce,$window){

	console.log("hi");
	
	$scope.selCustFlag = true;
	$scope.selBranchFlag = true;
	$scope.fromStationDBFlag = true;
	$scope.toStationDBFlag = true;
	$scope.conNameReadOnlyFlag=false;
	$scope.consignDBFlag=true;
	$scope.excelButtonFlag=false;
	 $scope.toVehicleTypeDBFlag = true;
	 $scope.toProductTypeDBFlag = true;
	 $scope.toOwnBrkDBFlag=true;
	 $scope.branchList=[];
	 $scope.custList=[];
	 $scope.ownBrkList=[];
	 $scope.ownBrk={};
	 
	 $scope.newIndentId=0;
	 $scope.ctsId=0;
	 
	 $scope.getBranch = function(){
		 console.log("enter into getBranch function");
		 
		 if($scope.branchList.length>0){
			 $scope.selBranchFlag = false;
				$('div#selBrhId').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					title: "Select Branch",
					show: UDShow,
					hide: UDHide,
					position: UDPos,
					draggable: true,
					close: function(event, ui) { 
				        $(this).dialog('destroy');
				        $(this).hide();
				        $scope.selCustFlag = true;
				    }
					});
				
				$('div#selBrhId').dialog('open');	
		 }else{
			 var response = $http.post($scope.projectName+'/getBranchFrIndent');
			  response.success(function(data, status, headers, config){
				  if(data.result === "success"){
					  $scope.branchList = data.list;
					  $scope.selBranchFlag = false;
						$('div#selBrhId').dialog({
							autoOpen: false,
							modal:true,
							resizable: false,
							title: "Select Branch",
							show: UDShow,
							hide: UDHide,
							position: UDPos,
							draggable: true,
							close: function(event, ui) { 
						        $(this).dialog('destroy');
						        $(this).hide();
						        $scope.selCustFlag = true;
						    }
							});
						
						$('div#selBrhId').dialog('open');	
				  }else{
					  $scope.alertToast("Server Error");
				  }
			   });
			   response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
			   });
		 }
		 
		
	 }
	 
	 $scope.saveBranch=function(brh){
		 $scope.branchName=brh.branchName;
		 $('div#selBrhId').dialog('close');
	 }
	 
	
	 $scope.getCustomer = function(){
		 console.log("enter into getCustomer function");
		 /*if($scope.custList.length>0){
			 $scope.selCustFlag = false;
				$('div#selCustId').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					title: "Select Customer",
					show: UDShow,
					hide: UDHide,
					position: UDPos,
					draggable: true,
					close: function(event, ui) { 
				        $(this).dialog('destroy');
				        $(this).hide();
				        $scope.selCustFlag = true;
				    }
					});
				
				$('div#selCustId').dialog('open');	
		 }*/
		 
		 if(angular.isUndefined($scope.placeDateTime) || $scope.placeDateTime==null){
			 $scope.alertToast("Placement date required")
		 }else{
			 
			 
			 var data={
					 "indentPlacementDtTime":$scope.placeDateTime
			 }
		 //else{
			 var response = $http.post($scope.projectName+'/getCustFrIndent',data);
			  response.success(function(data, status, headers, config){
				  if(data.result === "success"){
					  $scope.custList = data.list;
					  $scope.selCustFlag = false;
						$('div#selCustId').dialog({
							autoOpen: false,
							modal:true,
							resizable: false,
							title: "Select Customer",
							show: UDShow,
							hide: UDHide,
							position: UDPos,
							draggable: true,
							close: function(event, ui) { 
						        $(this).dialog('destroy');
						        $(this).hide();
						        $scope.selCustFlag = true;
						    }
							});
						
						$('div#selCustId').dialog('open');	
				  }else{
					  $scope.alertToast("Server Error");
				  }
			   });
			   response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
			   });
		 }
		 
		
	 }
	 
	 
	 $scope.saveCustomer=function(cust){
		 
		 $('div#selCustId').dialog('close');
		 
		 $scope.conName="";
		 $scope.consignCode="";
		 $scope.conNameReadOnlyFlag=false;

		 console.log($scope.placeDateTime);
		 if(angular.isUndefined($scope.placeDateTime)){
			 $scope.alertToast("Placement date required")
		 }else{
			 
			 $scope.custName=cust.custName;
			 $scope.custCode=cust.custCode;
			 
			 var data={
					 "indentPlacementDtTime":$scope.placeDateTime,
					 "custCode":$scope.custCode
			 }
			 var response = $http.post($scope.projectName+'/getContractFrIndent',data);
			 response.success(function(data, status, headers, config){
				  if(data.result === "success"){
					  $scope.contractList=data.contractList;
					  console.log("$scope.contractList ="+$scope.contractList[0].contCode);
					  
				  }else{
					  $scope.alertToast(data.msg);
				  }
			   });
			   response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
			   });
		 }
		 
	 }
	 
	
	 $scope.selectCustType=function(){
		 if($scope.custType=="Both"){
			 $scope.conName=$scope.custName;
			 $scope.consignCode=$scope.custCode;
			 $scope.conNameReadOnlyFlag=true;
		 }else{
			 $scope.conName="";
			 $scope.consignCode="";
			 $scope.conNameReadOnlyFlag=false;
		 }
	 }
	 
	 $scope.getFromLocation=function(){
		 console.log("hii");
		 console.log($scope.placeDateTime);
		 if(angular.isUndefined($scope.placeDateTime)){
			 $scope.alertToast("Placement date required")
		 }else{
			 
			 var data={
					 "indentPlacementDtTime":$scope.placeDateTime,
					 "custCode":$scope.custCode
			 }
			 var response = $http.post($scope.projectName+'/getFrmStnFrIndent',data);
			 response.success(function(data, status, headers, config){
				  if(data.result === "success"){
					  $scope.frmStnList=data.frmStnList;
					  
					  $scope.fromStationDBFlag = false;
						$('div#fromStationDBId').dialog({
							autoOpen: false,
							modal:true,
							resizable: false,
							title: "Select From Station",
							show: UDShow,
							hide: UDHide,
							position: UDPos,
							draggable: true,
							close: function(event, ui) { 
						        $(this).dialog('destroy');
						        $(this).hide();
						        $scope.fromStationDBFlag = true;
						    }
							});
						
						$('div#fromStationDBId').dialog('open');
					  
				  }else{
					  $scope.alertToast("Server Error");
				  }
			   });
			   response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
			   });
		 }
		 
	 }
	 
	 $scope.saveFromStn=function(frmStn){
		 $scope.fromStn=frmStn.stnName;
		 $scope.frmStnCode=frmStn.stnCode;
		 $('div#fromStationDBId').dialog('close');
		 $scope.getToLocation();
	 }
	 
	 
	 $scope.getToLocation=function(){
		 console.log("hii");
		 console.log($scope.placeDateTime);
		 if(angular.isUndefined($scope.placeDateTime)){
			 $scope.alertToast("Placement date required")
		 }else{
			 
			 //$scope.contractCode='';
			 for(var i=0;i<$scope.contractList.length;i++){
				 if($scope.contractList[i].stnCode==$scope.frmStnCode){
					 $scope.contractCode=$scope.contractList[i].contCode;
					 console.log("contractCode="+$scope.contractCode);
				 }else{
					 console.log("Blank");
				 }
					 
			 }
			 
			 var data={
					 "indentPlacementDtTime":$scope.placeDateTime,
					 "custCode":$scope.custCode,
					 "frmStnCode":$scope.frmStnCode,
					 "contCode":$scope.contractCode
			 }
			 var response = $http.post($scope.projectName+'/getToStnFrIndent',data);
			 response.success(function(data, status, headers, config){
				  if(data.result === "success"){
					  $scope.toStnList=data.toStnList;
					  
					  $scope.toStationDBFlag = false;
						$('div#toStationDBId').dialog({
							autoOpen: false,
							modal:true,
							resizable: false,
							title: "Select To Station",
							show: UDShow,
							hide: UDHide,
							position: UDPos,
							draggable: true,
							close: function(event, ui) { 
						        $(this).dialog('destroy');
						        $(this).hide();
						        $scope.fromStationDBFlag = true;
						    }
							});
						
						$('div#toStationDBId').dialog('open');
					  
				  }else{
					  $scope.alertToast("Server Error");
				  }
			   });
			   response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
			   });
		 }
		 
	 }
	 
	 $scope.saveToStn=function(toStn){
		
		 $scope.toStn=toStn.stnName;
		 $scope.toStnCode=toStn.stnCode;
		 $('div#toStationDBId').dialog('close');
		 
		 
		 
		 
		 var data={
				 "indentPlacementDtTime":$scope.placeDateTime,
				 "custCode":$scope.custCode,
				 "frmStnCode":$scope.frmStnCode,
				 "toStnCode":$scope.toStnCode,
				 "contCode":$scope.contractCode
		 }
		 
		 var response = $http.post($scope.projectName+'/getVehTypeFrIndent',data);
		 response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				  $scope.vehicleTypeList=data.vtList;
				  
				  $scope.toVehicleTypeDBFlag = false;
					$('div#toVehicleTypeDBId').dialog({
						autoOpen: false,
						modal:true,
						resizable: false,
						title: "Select To Station",
						show: UDShow,
						hide: UDHide,
						position: UDPos,
						draggable: true,
						close: function(event, ui) { 
					        $(this).dialog('destroy');
					        $(this).hide();
					        $scope.fromStationDBFlag = true;
					    }
						});
					
					$('div#toVehicleTypeDBId').dialog('open');
				  
			  }else{
				  $scope.alertToast("Server Error");
			  }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
		   });
		 
		 
		 
	 }
	 
	 
	 $scope.saveVehicleType = function(veh){
		 $('div#toVehicleTypeDBId').dialog('close');
		 $scope.vehicleType=veh.vtVehicleType;
		 $scope.vehicleTypeCode=veh.vtCode;
		 
		 
		 var data={
				 "indentPlacementDtTime":$scope.placeDateTime,
				 "custCode":$scope.custCode,
				 "frmStnCode":$scope.frmStnCode,
				 "toStnCode":$scope.toStnCode,
				 "contCode":$scope.contractCode,
				 "vehicleType":$scope.vehicleTypeCode
		 }
		 
		 var response = $http.post($scope.projectName+'/getPrdctTypeFrIndent',data);
		 response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				  $scope.ctsList=data.ctsList;
				  
				  $scope.toProductTypeDBFlag = false;
					$('div#toProductTypeDBId').dialog({
						autoOpen: false,
						modal:true,
						resizable: false,
						title: "Select To Station",
						show: UDShow,
						hide: UDHide,
						position: UDPos,
						draggable: true,
						close: function(event, ui) { 
					        $(this).dialog('destroy');
					        $(this).hide();
					        $scope.fromStationDBFlag = true;
					    }
						});
					
					$('div#toProductTypeDBId').dialog('open');
				  
			  }else{
				  $scope.alertToast("Server Error");
			  }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
		   });
		 
		 
		 
	 }
	 
	 
	 $scope.saveProductType = function(cts){
		 $('div#toProductTypeDBId').dialog('close');
		 $scope.materialType=cts.ctsProductType;
		 $scope.ctsId=cts.ctsId;
		 console.log("ctsId="+$scope.ctsId);
	 }
	 
	 
	 $scope.saveIndent=function(){
		 console.log("hii");
		 console.log($scope.indentDateTime);
		 
		 var indent={
				 	"ctsId":$scope.ctsId,
				 	"orderId":$scope.orderId,
				 	"branchName":$scope.branchName,
					"indentDateTime":$scope.indentDateTime,
					"placementDateTime":$scope.placeDateTime,
					"custCode":$scope.custCode,
					"custName":$scope.custName,
					"cutomerType":$scope.custType,
					"cnsneCnsnrName":$scope.conName,
					"cnsneCnsnrCode":$scope.consignCode,
					"fromStn":$scope.frmStnCode,
					"toStn":$scope.toStnCode,
					//"kilometer":$scope.km,
					"transitTime":$scope.transitTime,
					"vehicleType":$scope.vehicleType,
					"material":$scope.materialType,
					"totalWeight":$scope.totalWeight,
					"vehicleRqr":$scope.totalVehicle,
					"tools":$scope.tools,
					//"gps":$scope.gps,
					"prTonRate":$scope.ratePrTn,
					"trgtrate":$scope.targetRate,
					"minGuaranteeWeight":$scope.minWeight,
					"cnmtDimensionType":$scope.cnmtDimensionType,
					"dimension":$scope.dimension,
					"advancePercent":$scope.advancePercent,
					"advPayMode":$scope.advPayMode,
					"balPayMode":$scope.balPayMode,
					"balTerms":$scope.balTerms
		 }
		 
		 var response = $http.post($scope.projectName+'/saveIndent',indent );
		 response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				  $scope.alertToast("success");
				  
				  console.log("indentId="+data.indentId);
				  $scope.newIndentId=data.indentId;
				  var res = $http.post($scope.projectName+'/getIndentOwnerMaster',data.indentId );
					 res.success(function(data, status, headers, config){
						  if(data.result === "success"){
							//  $scope.ownBrkList=angular.toJson(data.ownerMasterList);
							  $scope.ownBrkList=data.ownerMasterList;
							  console.log("data="+$scope.ownBrkList);
							  
							  $scope.toOwnBrkDBFlag = false;
								$('div#toOwnBrkDBId').dialog({
									autoOpen: false,
									modal:true,
									resizable: false,
									title: "Select To Broker",
									show: UDShow,
									hide: UDHide,
									position: UDPos,
									draggable: true,
									close: function(event, ui) { 
								        $(this).dialog('destroy');
								        $(this).hide();
								        $scope.fromStationDBFlag = true;
								    }
									});
								
								$('div#toOwnBrkDBId').dialog('open');
					
						  }
					   });
					   res.error(function(data, status, headers, config) {
							$scope.errorToast(data.result);
					   });
				  /*$scope.orderId='';
				  	$scope.branchName='';
					$scope.indentDateTime=null;
					$scope.placeDateTime=null;
					$scope.custCode="";
					$scope.custName="";
					$scope.conName="";
					$scope.frmStnCode="";
					$scope.toStnCode="";
					$scope.fromStn="";
					$scope.toStn="";
					//$scope.km=0;
					//$scope.transitTime=0;
					$scope.vehicleType="";
					//$scope.materialType="";
					$scope.totalWeight='';
					$scope.totalVehicle='';
					$scope.tools="";
					$scope.ratePrTn='';
					$scope.targetRate='';
					$scope.minWeight='';
					$scope.dimension='';
					$scope.advancePercent='';
					$scope.balTerms='';*/
			  
			  }else{
				  $scope.alertToast(data.msg);
			  }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
		   });
		 
	 }
	 
	 
	 $scope.verifyOrderid=function(){
		 
		 var response = $http.post($scope.projectName+'/checkOrderIdExist',$scope.orderId );
		 response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				  $scope.orderId="";
				  $scope.alertToast("Order ID already exist");
			  }else{
				 // $scope.alertToast("success");
			  }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
		   });
		
		 
	 }
	 
 $scope.newList=[];
 $scope.submitList = function () {
     var message = "";
     $scope.newList=[];
     for (var i = 0; i < $scope.ownBrkList.length; i++) {
         if ($scope.ownBrkList[i].Selected) {
        	 console.log("hii");
        	 $scope.newList.push($scope.ownBrkList[i]);
         }
     }
	 
	 if($scope.newList.length>0){
    	 console.log("Sending to dialer software API");
    	 
    	 /*var clientMap={
    			 "indentId":$scope.newIndentId,
    			 "brkList" :$scope.newList
    	 }*/
    	 
    	 var clientMap={};
    	 clientMap.indentId=$scope.newIndentId;
    	 clientMap.brkList=$scope.newList;
    	 
    	 var response = $http.post($scope.projectName+'/sendDetailToDialer',clientMap);
		 response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				  $scope.orderId="";
				  $scope.alertToast("success");
				  $scope.excelButtonFlag=true;
			  }else{
				  $scope.alertToast("Error");
				  console.log(data);
				  $scope.excelButtonFlag=false;
			  }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
				$scope.excelButtonFlag=false;
		   });
    	 
     }else{
    	 $scope.alertToast("Please select brokers");
    	 $scope.excelButtonFlag=false;
     }
 } 
 
 
 
 
 
 $scope.getConsignorList = function(keyCode){
		console.log("Enter into getConsignorList()");
		if(keyCode === 8 || parseInt($scope.conName.length) < 2)
			return;
		var response = $http.post($scope.projectName + '/getCustomerListByNameFa', $scope.conName);
		response.success(function(data, status, headers, config){
			console.log(data);
			if(data.result === "success"){
				$scope.customerList = data.customerList;
				$scope.OpenCnmtConsignorDB();				
			}else{
				$scope.alertToast(data.msg);
				$scope.conName = "";
				$scope.consignCode="";
			}			
		});
		response.error(function(data, status, headers, config){
			console.log("Error in hitting /getCustomerListByNaFa");
		});
		console.log("Exit from getConsignorList()");
	}
	
	$scope.OpenCnmtConsignorDB = function(){
		$scope.consignDBFlag=false;
		$('div#consigDB').dialog({
			autoOpen: false,
			modal:true,
			title: "CNMT Consignor Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#consigDB').dialog('open');
	}
 
 $scope.saveConsignorCustomerCode=function(consignor){
	 
	 $scope.conName=consignor.custName;
	 $scope.consignCode=consignor.custCode;
	 $('div#consigDB').dialog('close');
 }
 
 

	$scope.calculatePrTnRt=function(){
		$scope.ratePrTn=$scope.targetRate/$scope.minWeight;
	}
	
	$scope.calculateTrgtRt=function(){
		$scope.targetRate=$scope.ratePrTn*$scope.minWeight;
	}
 
	
}]);