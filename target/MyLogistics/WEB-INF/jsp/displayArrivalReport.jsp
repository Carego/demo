<div ng-show="adminLogin || superAdminLogin">
<title>Display Arrival Report</title>
<div class="container">
	<div class="row">
		<div class="col s2">
			<input type="radio" name="arList" ng-model="arByModal"
				ng-click="enableModalTextBox()">
		</div>
		<div class="col s10">
			<input type="text" id="arByMId" name="arByM" ng-model="arByM"
				disabled="disabled" ng-click="OpenARDB()">
		</div>
	</div>

	<div class="row">
		<div class="col s2">
			<input type="radio" name="arList" ng-model="arByAuto"
				ng-click="enableAutoTextBox()">
		</div>
		<div class="col s8">
			<input type="text" id="arByAId" name="arByA" ng-model="arByA"
				disabled="disabled" ng-keyup="getARList()">
		</div>
		<div class="col s2">
			<input class="col s12" type="button" id="ok" value="OK"
				disabled="disabled" ng-click="getArriRepList()">
		</div>
	</div>

	<div class="row">
		<div class="input-field col s2 m2 center">
			<input id="bkToList" type="button" value="Back To list"
				ng-click="backToList()">
		</div>
	</div>
</div>

<div id="arDB" ng-hide="arDBFlag">
	<input type="text" name="filterArDB" ng-model="filterArDB"
		placeholder="Search by Arrival Report Code">
	<table>
		<tr ng-repeat="arCodes in arCodeList | filter:filterArDB">
			<td><input type="radio" name="arCode" value="{{ arCodes }}"
				ng-model="arCodes1" ng-click="saveARCode(arCodes)"></td>
			<td>{{ arCodes }}</td>
		</tr>
	</table>
</div>

<div class="container" id="ArTableDB" ng-hide="isViewNo">
	<input type="text" name="filterAr" ng-model="filterAr.arCode"
		placeholder="Search by Arrival Report Code">

	<table class="table-bordered">
		<tr>
			<th></th>

			<th>AR Code</th>

			<th>Challan Code</th>

			<th>User Code</th>

			<th>Time Stamp</th>

		</tr>


		<tr ng-repeat="ar in arList | filter:filterAr">
			<td><input id="{{ ar.arCode }}" type="checkbox"
				value="{{ ar.arCode }}"
				ng-checked="selection.indexOf(ar.arCode) > -1"
				ng-click="toggleSelection(ar.arCode)" /></td>
			<td>{{ ar.arCode }}</td>
			<td>{{ ar.archlnCode }}</td>
			<td>{{ ar.userCode }}</td>
			<td>{{ ar.creationTS }}</td>
		</tr>
	</table>
	<table>
		<tr>
			<td class="center"><input type="submit" value="Submit"
				ng-click="verifyAR()"></td>
		</tr>
	</table>
</div>

<div ng-hide="showAR">
	<div class="container">
	<div class="row">
	<div class="col s3 hidden-xs hidden-sm"> &nbsp; </div>
	<div ng-hide = "show" class="col s8 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >	
				<h4>Here's the details of Arrival Report <span class="teal-text text-lighten-2">{{arrivalReport.arCode}}</span>:</h4>
		<table class="table-bordered table-hover table-condensed">
	            <tr>
	                <td>Arrival Report Type:</td>
	                <td><input type="text" id="arRepType" name="arRepType" ng-model="arrivalReport.arRepType" readonly></td>
	            </tr>
	            
	            <tr>
	                <td>Arrival Report Code:</td>
	                <td><input type="text" id="arCode" name="arCode" ng-model="arrivalReport.arCode" readonly></td>
	            </tr>
	            
	             <tr>
	                <td>Challan Code:</td>
	                <td><input type="text" id="archlnCodeId" name="archlnCodeName" ng-model="arrivalReport.archlnCode" readonly></td>
	            </tr>
	            
	            <tr>
	                <td>Arrival Report Date:</td>
	                <td><input type="date" id="arDt" name="arDt" ng-model="arrivalReport.arDt" readonly></td>
	            </tr>
	            
	            <tr>
	                <td>Received Weight:</td>
	                <td><input type="number" id="arRcvWt" name="arRcvWt" ng-model="arrivalReport.arRcvWt" readonly></td>
	            </tr>
	            
	            <tr>
	                <td>Package:</td>
	                <td><input type="number" id="arPkg" name="arPkg" ng-model="arrivalReport.arPkg" readonly></td>
	            </tr>
	            
	            <tr>
	                <td>Extra Km:</td>
	                <td><input type="number" id="arExtKmId" name="arExtKmName" ng-model="arrivalReport.arExtKm" step="0.00001" min="0.00000" readonly></td>
	            </tr>
	            
	            <tr>
	                <td>Over Height:</td>
	                <td><input type="number" id="arOvrHgtId" name="arOvtHgtName" ng-model="arrivalReport.arOvrHgt" step="0.00001" min="0.00000" readonly></td>
	            </tr>
	            
	            <tr>
	                <td>Weight Shortage:</td>
	                <td><input type="number" id="arWtShrtgId" name="arWtShrtgName" ng-model="arrivalReport.arWtShrtg" step="0.00001" min="0.00000" readonly></td>
	            </tr>
	            
	            <tr>
	                <td>Penalty:</td>
	                <td><input type="number" id="arPenaltyId" name="arPenaltyName" ng-model="arrivalReport.arPenalty" step="0.00001" min="0.00000" readonly></td>
	            </tr>
	            
	            <tr>
	                <td>Others:</td>
	                <td><input type="number" id="arOther" name="arOther" ng-model="arrivalReport.arOther" step="0.00001" min="0.00000" readonly></td>
	            </tr>
	            
	            <tr>
	                <td>Claim:</td>
	                <td><input type="number" id="arClaim" name="arClaim" ng-model="arrivalReport.arClaim" step="0.00001" min="0.00000" readonly></td>
	            </tr> 
	            
	            <tr>
	                <td>Unloading:</td>
	                <td><input type="number" id="arUnloading" name="arUnloading" ng-model="arrivalReport.arUnloading" step="0.00001" min="0.00000" readonly></td>
	            </tr>
	            
	             <tr>
	                <td>Detention:</td>
	                <td><input type="number" id="arDetention" name="arDetention" ng-model="arrivalReport.arDetention" step="0.00001" min="0.00000" readonly></td>
	            </tr>
	            
	            <tr>
	                <td>Late Delivery Charge:</td>
	                <td><input type="number" id="arLateDelivery" name="arLateDelivery" ng-model="arrivalReport.arLateDelivery" step="0.00001" min="0.00000" readonly></td>
	            </tr>
	            
	            <tr>
	                <td>Late Ack. Charge:</td>
	                <td><input type="number" id="arLateAck" name="arLateAck" ng-model="arrivalReport.arLateAck" step="0.00001" min="0.00000" readonly></td>
	            </tr>
	            
	            <tr>
	                <td>Reporting Date:</td>
	                <td><input type="date" id="arRepDt" name="arRepDt" ng-model="arrivalReport.arRepDt" readonly></td>
	            </tr>
	            
	            <tr>
	                <td>Reporting Time:</td>
	                <td><input type="time" id="arRepTime" name="arRepTime" ng-model="arrivalReport.arRepTime" readonly></td>
	            </tr>
	            
	            <!-- <tr>
	                <td>HO Allowance:</td>
	                <td>
	                <select name="arIsHOAlw" ng-model="arrivalReport.arIsHOAlw" required>
						<option value='true'>Yes</option>
						<option value='false'>No</option>
					</select> 
				<input type="text" id="" name="" ng-model="arrivalReport.arIsHDAlw" ></td>
	            </tr> -->
	         
		</table>
		
		<!-- <input type="button" value="UPDATE" ng-click="editARSubmit(arrivalReport)"/> -->
</div>
</div>
	
	
	
		<!-- <div class="row">
			
			<div class="input-field col s12 m4 l3">
				<input type="text" id="arOther" name="arOther"
					ng-model="arrivalReport.arOther" value={{arrivalReport.arOther}}>
				<label>Other</label>
			</div>
			<div class="input-field col s12 m4 l3">
				<input type="text" id="arLateDelivery" name="arLateDelivery"
					ng-model="arrivalReport.arLateDelivery"
					value={{arrivalReport.arLateDelivery}}> <label>Late
					Delivery</label>
			</div>
			<div class="input-field col s12 m4 l3">
				<input type="text" id="arLateAck" name="arLateAck"
					ng-model="arrivalReport.arLateAck"
					value={{arrivalReport.arLateAck}}> <label>Late
					Acknowledgement</label>
			</div>
			<div class="input-field col s12 m4 l3">
				<input type="text" id="arRepDt" name="arRepDt"
					ng-model="arrivalReport.arRepDt" value={{arrivalReport.arRepDt}}>
				<label>Reporting Date</label>
			</div>
			<div class="input-field col s12 m4 l3">
				<input type="text" id="arRepTime" name="arRepTime"
					ng-model="arrivalReport.arRepTime"
					value={{arrivalReport.arRepTime}}> <label>Reporting
					Time</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12 center">
				<input type="button" value="Submit!"
					ng-click="editARSubmit(arrivalReport)">
			</div>
		</div>
	</div>
	<form method="post" action="downloadAr" enctype="multipart/form-data">
		<div class="container">
			<div class="row">
				<div class="input-field col s12 center">
					<input type="hidden" id="submitImageId" name="submitImageName"
						value="{{ arrivalReport.arCode }}" size="50" /> <input
						type="submit" id="submitImage" value="download" />
				</div>
			</div>
		</div> -->
	</form>

</div>
</div>
</div>