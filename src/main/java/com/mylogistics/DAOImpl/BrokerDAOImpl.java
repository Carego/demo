package com.mylogistics.DAOImpl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.sql.rowset.serial.SerialBlob;

import org.apache.commons.lang.NumberUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.BrokerDAO;
import com.mylogistics.constants.BrokerCNTS;
import com.mylogistics.constants.BrokerImgCNTS;
import com.mylogistics.constants.OwnerCNTS;
import com.mylogistics.constants.OwnerImgCNTS;
import com.mylogistics.model.Broker;
import com.mylogistics.model.BrokerImg;
import com.mylogistics.model.Owner;
import com.mylogistics.model.OwnerImg;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.PanService;

public class BrokerDAOImpl implements BrokerDAO{

	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;

	@Autowired
	private HttpSession httpSession;
	
	public static Logger logger = Logger.getLogger(BrokerDAOImpl.class);
	
	private String brokerPanImgPath = "/var/www/html/Erp_Image/Broker/Pan";
	private String brokerDecImgPath = "/var/www/html/Erp_Image/Broker/Dec";
	private String brokerBnkImgPath = "/var/www/html/Erp_Image/Broker/Bnk";
	
	@Autowired
	public BrokerDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional
	public int saveBroker(Broker broker){
		int temp;
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			int id = (Integer) session.save(broker);
			transaction.commit();
			temp =  id;
			System.out.println("broker Data is inserted successfully");
			session.flush();
		}

		catch(Exception e){
			e.printStackTrace();
			temp = -1;
		}
		session.clear();
		session.close();
		return temp;
	}

	@Transactional
	public long totalCount(){
		long tempCount = -1;
		try {
			session = this.sessionFactory.openSession();
			tempCount = (Long) session.createCriteria(Broker.class).setProjection(Projections.rowCount()).uniqueResult();
			session.flush();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return tempCount;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public int getLastBrokerId(){
		List<Broker> list = new ArrayList<Broker>();
		int id=0;
		try{
			session = this.sessionFactory.openSession();
			list = session.createQuery("from Broker order by brkId DESC").setMaxResults(1).list();
			id = list.get(0).getBrkId();
			session.flush();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return id;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<Broker>  getBroker(){
		System.out.println("enter into getBroker function");
		List<Broker> list = new ArrayList<Broker>();
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			list = session.createCriteria(Broker.class)
					.add(Restrictions.isNotNull("brkName")).list();
			transaction.commit();
			session.flush();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return list;
	}

	@Override
	public Broker  findBrokerByBrh_Name(String branchCode, String brkName){
		logger.info("Enter into findBrokerByBrh_Name() : BranchCode = "+branchCode+" : BrkName = "+brkName);
		Broker broker = null;
		try{
			session = this.sessionFactory.openSession();
			List<Broker> brkList = session.createCriteria(Broker.class)
					.add(Restrictions.eq(BrokerCNTS.BRANCH_CODE, branchCode))
					.add(Restrictions.eq(BrokerCNTS.BRK_NAME, brkName))
					.list();
			
			if(! brkList.isEmpty()){
				broker = brkList.get(0);
				brkList = null;
			}
		}
		catch (Exception e) {
			logger.error("Exception : "+e);
		}finally{
			session.clear();
			session.close();
		}
		return broker;
	}

	@Transactional
	@SuppressWarnings("unchecked")
	public List<Broker> getBrokerisViewFalse(String branchCode){
		List<Broker> result = new ArrayList<Broker>();

		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr=session.createCriteria(Broker.class);
			cr.add(Restrictions.eq(BrokerCNTS.IS_VIEW,false));
			cr.add(Restrictions.eq(BrokerCNTS.BRANCH_CODE,branchCode));
			result =cr.list();
			transaction.commit();
			session.flush();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return  result; 
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public int updateBrokerisViewTrue(String code[]){
		int temp;
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		try{
			session = this.sessionFactory.openSession();
			for(int i=0;i<code.length;i++){
				transaction = session.beginTransaction();
				Broker broker = new Broker();
				List<Broker> list = new ArrayList<Broker>();
				Criteria cr=session.createCriteria(Broker.class);
				cr.add(Restrictions.eq(BrokerCNTS.BRK_CODE,code[i]));
				list =cr.list();
				broker = list.get(0);
				broker.setView(true);
				broker.setCreationTS(calendar);
				session.update(broker);
				transaction.commit();
			}
			session.flush();

			temp = 1;
		}

		catch(Exception e){
			e.printStackTrace();
			temp = -1;
		}
		session.clear();
		session.close();
		return temp;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<Broker> getBrokerList(String brkCode){

		List<Broker> result = new ArrayList<Broker>();

		try{
			session = this.sessionFactory.openSession();
			Criteria cr=session.createCriteria(Broker.class);
			cr.add(Restrictions.eq(BrokerCNTS.BRK_CODE,brkCode));
			result =cr.setMaxResults(1).list();
			session.flush();
		}

		catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return  result; 
	}

	@Transactional
	public int updateBroker(Broker broker){
		logger.info("Enter into updateBroker()");
		int temp;
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);		
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			
			Criteria cr=session.createCriteria(Broker.class);
			cr.add(Restrictions.eq(BrokerCNTS.BRK_CODE, broker.getBrkCode()));
			
			broker.setCreationTS(calendar);
			session.saveOrUpdate(broker);
			
			transaction.commit();			
			temp= 1;
		}
		catch(Exception e){
			logger.error("Exception : "+e);
			temp= -1;
		}		
		session.clear();
		session.close();
		logger.info("Exit from updateBroker()");
		return temp;
	}

	@Transactional
	@SuppressWarnings("unchecked")
	public List<String> getBrokerCode(){
		List<String> oList = new ArrayList<String>();
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr=session.createCriteria(Broker.class);
			ProjectionList projList = Projections.projectionList();
			projList.add(Projections.property("brkCode"));
			cr.setProjection(projList);
			oList = cr.list();
			transaction.commit();
			session.flush();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return oList;
	}

	@Transactional
	@SuppressWarnings("unchecked")
	public List<String> getBrokerCodes(String branchCode){
		List<String> result = new ArrayList<String>();

		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr=session.createCriteria(Broker.class);
			cr.add(Restrictions.eq(BrokerCNTS.BRANCH_CODE,branchCode));
			ProjectionList projList = Projections.projectionList();
			projList.add(Projections.property("brkCode"));
			cr.setProjection(projList);
			result =cr.list();
			transaction.commit();
			session.flush();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return  result; 
	}

	@SuppressWarnings("unchecked")
	public Broker getBrokerData(int id){
		Broker brk = null;
		Session session = this.sessionFactory.openSession();;
		try{
			brk=(Broker) session.get(Broker.class, id);
		}
		catch (Exception e) {
			e.printStackTrace();
		}finally { 
			session.close();
		}
		return brk;
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, String>> getBrkCodesIsBnkD(){
		List<Map<String, String>> brokerCodeList = new ArrayList<>();
		Session session = this.sessionFactory.openSession();
		try{
			
			Criteria cr=session.createCriteria(Broker.class);
			cr.add(Restrictions.eq(BrokerCNTS.IS_BRK_BNK_DET,false));
			ProjectionList projectionList = Projections.projectionList();
			projectionList.add(Projections.property(BrokerCNTS.BRK_CODE),"brkCode");
			projectionList.add(Projections.property(BrokerCNTS.BRK_NAME),"brkName");
			cr.setProjection(projectionList);
			cr.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			brokerCodeList =cr.list();
		}
		catch (Exception e) {
			e.printStackTrace();
		}finally {
			session.clear();
			session.close();
		}
		return brokerCodeList;

	}
	
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Map<String,String>> getBrkNCF(){
		System.out.println("enter into getBrkNCF function");
		//Map<String,String> map = new HashMap<String,String>();
		List<Map<String,String>> brkList = new ArrayList<>();
		try{
			session = this.sessionFactory.openSession();
			Criteria cr=session.createCriteria(Broker.class)
					.add(Restrictions.isNotNull("brkName"));
			
			List<Broker> brkCodeList =cr.list();
			
			if(!brkCodeList.isEmpty()){
				for(int i=0;i<brkCodeList.size();i++){
					//if(brkCodeList.get(i).getBrkImgId() > 0){
						Map<String,String> map = new HashMap<String,String>();
						
						map.put("id", String.valueOf(brkCodeList.get(i).getBrkId()));
						map.put("code",brkCodeList.get(i).getBrkCode());
						map.put("faCode",brkCodeList.get(i).getBrkFaCode());
						map.put("name",brkCodeList.get(i).getBrkName());
						map.put("panNo",brkCodeList.get(i).getBrkPanNo());
						map.put("panImgId",String.valueOf(brkCodeList.get(i).getBrkImgId()));
						
						brkList.add(map);
					//}
				}
			}
			
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return brkList;
	}
	
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Map<String,String>> getBrkNCFByName(String brkName){
		System.out.println("enter into getBrkNCF function");		
		List<Map<String,String>> brkList = new ArrayList<>();
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr=session.createCriteria(Broker.class);
			cr.add(Restrictions.like(BrokerCNTS.BRK_NAME, brkName+"%"));
			
			List<Broker> brkCodeList =cr.list();
			
			if(!brkCodeList.isEmpty()){
				for(int i=0;i<brkCodeList.size();i++){
					//if(brkCodeList.get(i).getBrkImgId() > 0){
						Map<String,String> map = new HashMap<String,String>();
						
						map.put("id", String.valueOf(brkCodeList.get(i).getBrkId()));
						map.put("code",brkCodeList.get(i).getBrkCode());
						map.put("faCode",brkCodeList.get(i).getBrkFaCode());
						map.put("name",brkCodeList.get(i).getBrkName());
						map.put("panNo",brkCodeList.get(i).getBrkPanNo());
						map.put("panImgId",String.valueOf(brkCodeList.get(i).getBrkImgId()));
						
						brkList.add(map);
					//}
				}
			}
			
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return brkList;
	}
	
	
	@SuppressWarnings("unchecked")
	@Transactional
	public int saveBrkAndImg(Broker broker , BrokerImg brokerImg, Blob brokerPanImg, Blob brokerDecImg){
		System.out.println("enter into saveBrkAndImg function");
		logger.info("Enter into saveBrkAndImg()");
		User currentUser = (User) httpSession.getAttribute("currentUser");
		int res = 0;
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			
			int brkId = (Integer) session.save(broker);			
			broker.setBrkCode("brk"+String.valueOf(brkId));
			String brkCode = "brk"+String.valueOf(brkId);
			if(brkId > 0){
				logger.info("Broker Saved ID = "+brkId);
				
				if(brokerPanImg != null || brokerDecImg != null){
					try{								
						if(brokerPanImg != null){
							logger.info("Going to write broker pan image : PanImgPath = "+brokerPanImgPath+"/"+brkCode+".pdf");
							brokerImg.setBrkPanImgPath(brokerPanImgPath+"/"+brkCode+".pdf");
							byte brokerPanImgInBytes[] = brokerPanImg.getBytes(1, (int)brokerPanImg.length());
							File panFile = new File(brokerImg.getBrkPanImgPath());
							if(panFile.exists())
								panFile.delete();
							if(panFile.createNewFile()){											    
								OutputStream targetFile=  new FileOutputStream(panFile);
					            targetFile.write(brokerPanImgInBytes);			            
					            targetFile.close();
					            logger.info("Writing pan image is done !");
							}
						}
						if(brokerDecImg != null){
							logger.info("Going to write broker dec image : DecImgPath = "+brokerDecImgPath+"/"+brkCode+".pdf");
							brokerImg.setBrkDecImgPath(brokerDecImgPath+"/"+brkCode+".pdf");
							byte brokerDecImgInBytes[] = brokerDecImg.getBytes(1, (int)brokerDecImg.length());					
							File decFile = new File(brokerImg.getBrkDecImgPath());										
							if(decFile.exists())
								decFile.delete();
							if(decFile.createNewFile()){											    
								OutputStream targetFile=  new FileOutputStream(decFile);
					            targetFile.write(brokerDecImgInBytes);			            
					            targetFile.close();
					            logger.info("Writing dec image is done !");
							}
						}						
					}catch(Exception e){						
						logger.error("Error in writing file ! Pan Path = "+brokerPanImgPath+"/"+brkCode+".pdf"+" : DecPath = "+brokerImg.getBrkDecImgPath()+"/"+brkCode+".pdf"+" : "+e);
						res = 0;
						return res;
					}				
					
					brokerImg.setBrkId(brkId);
					brokerImg.setbCode(currentUser.getUserBranchCode());
					brokerImg.setUserCode(currentUser.getUserCode());
					
					int brkImgId = (Integer) session.save(brokerImg);
					if(brkImgId > 0){
						broker.setBrkImgId(brkImgId);
						if(brokerPanImg != null){
							broker.setBrkIsPanImg(true);
						}
						if(brokerDecImg != null){
							broker.setBrkIsDecImg(true);
						}
					}
				}
			}else{
				logger.info("Broker is not saved !");
			}
			
			session.merge(broker);
			transaction.commit();
			res = brkId;
		}catch(Exception e){
			logger.error("Exception in saving broker : "+e);			
			e.printStackTrace();
		}
		session.clear();
		session.close();
		logger.info("Exit from saveBrkAndImg()");
		return res;
	}

	@Override
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Map<String, Object>> getBrkNameCodeId() {
		
		System.out.println("enter into getBrkNameCodeId function");
		User currentUser = (User) httpSession.getAttribute("currentUser");
		List<Map<String, Object>> brokerListMap = new ArrayList<>();
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr=session.createCriteria(Broker.class)
					.add(Restrictions.isNotNull("brkName"))
					.addOrder(Order.asc("brkName"));
			//cr.add(Restrictions.eq(BrokerCNTS.BRANCH_CODE, currentUser.getUserBranchCode()));
			List<Broker> brkList = cr.list();
			
			if(!brkList.isEmpty()){
				for (Broker broker : brkList) {
					Map<String, Object> brkMap = new HashMap<>();
					brkMap.put(BrokerCNTS.BRK_ID, broker.getBrkId());
					brkMap.put(BrokerCNTS.BRK_NAME, broker.getBrkName());
					brkMap.put(BrokerCNTS.BRK_CODE, broker.getBrkCode());
					brokerListMap.add(brkMap);
				}
			}
			
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		
		return brokerListMap;
		
	}
	
	
	
public List<Map<String, Object>> getBrkNameCodeIdByName(String brkName) {
		
		System.out.println("enter into getBrkNameCodeId function");
		User currentUser = (User) httpSession.getAttribute("currentUser");
		List<Map<String, Object>> brokerListMap = new ArrayList<>();
		try{
			session = this.sessionFactory.openSession();
			Criteria cr=session.createCriteria(Broker.class);
			cr.add(Restrictions.like(BrokerCNTS.BRK_NAME, "%"+brkName+"%"));
			List<Broker> brkList = cr.list();
			
			if(!brkList.isEmpty()){
				for (Broker broker : brkList) {
					Map<String, Object> brkMap = new HashMap<>();
					brkMap.put(BrokerCNTS.BRK_ID, broker.getBrkId());
					brkMap.put(BrokerCNTS.BRK_NAME, broker.getBrkName());
					brkMap.put(BrokerCNTS.BRK_CODE, broker.getBrkCode());
					brkMap.put(BrokerCNTS.BRK_PAN_No, broker.getBrkPanNo());
					brokerListMap.add(brkMap);
				}
			}
			
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		
		return brokerListMap;
		
	}
	
	
	@SuppressWarnings("unchecked")
	@Transactional
	public Broker getBrkByFaCode(String faCode){
		System.out.println("enter into getBrkByFaCode function");
		Broker broker = null;
		List<Broker> brkList = new ArrayList<>();
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr=session.createCriteria(Broker.class);
			cr.add(Restrictions.eq(BrokerCNTS.BRK_FA_CODE,faCode));
			brkList = cr.list();
			if(!brkList.isEmpty()){		
				broker = brkList.get(0);
			}
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return broker;
	}
	
	
	
	@SuppressWarnings("unchecked")
	@Transactional
	public int checkBrkPan(String brkCode){
		logger.info("Enter into checkBrkPan : BrkCode = "+brkCode);
		List<Broker> brkList = new ArrayList<>();
		int res = 0;
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr=session.createCriteria(Broker.class);
			cr.add(Restrictions.eq(BrokerCNTS.BRK_CODE,brkCode));
			brkList = cr.list();
			if(!brkList.isEmpty()){
				logger.info("Broker found : Broker ID = "+brkList.get(0).getBrkId());
				List<BrokerImg> brkImgList = new ArrayList<>();
				cr=session.createCriteria(BrokerImg.class);
				cr.add(Restrictions.eq(BrokerImgCNTS.BRK_ID,brkList.get(0).getBrkId()));
				brkImgList = cr.list();
				if(!brkImgList.isEmpty()){
					logger.info("BrokerImg found : BrokerImg ID = "+brkImgList.get(0).getBrkImgId());
					res = 1;
				}else{
					logger.info("BrokerImg not found");
					res = 0;
				}
			}else{
				logger.info("Broker not found !");
			}
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return res;
	}

	@Override
	public Map<String, Object> saveBrkPan(PanService panService, BrokerImg brokerImg,
			float brkIntRate, boolean isPanImg, boolean isDecImg, Blob brokerPanImg, Blob brokerDecImg) {
		Map<String, Object> resultMap = new HashMap<String, Object>();		
		System.out.println("enter into saveBrkPan function");	
		logger.info("Enter into saveBrkPan()...");
		int temp = 0;
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			int brkImgId = -1;
			
			Broker broker = (Broker) session.get(Broker.class, panService.getBrkId());
			String brkCode = broker.getBrkCode();
			logger.info("Broker found !");	
			try{
				if(brokerPanImg != null){
					logger.info("Going to write broker pan image !");
					brokerImg.setBrkPanImgPath(brokerImg.getBrkPanImgPath()+"/"+brkCode+".pdf");
					byte brokerPanImgInBytes[] = brokerPanImg.getBytes(1, (int)brokerPanImg.length());
					File panFile = new File(brokerImg.getBrkPanImgPath());
					if(panFile.exists())
						panFile.delete();
					if(panFile.createNewFile()){
						OutputStream targetFile=  new FileOutputStream(panFile);
			            targetFile.write(brokerPanImgInBytes);
			            targetFile.close();
			            logger.info("Writing pan image is done !");
					}
				}
				if(brokerDecImg != null){
					logger.info("Going to write broker dec image !");
					brokerImg.setBrkDecImgPath(brokerImg.getBrkDecImgPath()+"/"+brkCode+".pdf");
					byte brokerDecImgInBytes[] = brokerDecImg.getBytes(1, (int)brokerDecImg.length());
					File decFile = new File(brokerImg.getBrkDecImgPath());
					if(decFile.exists())
						decFile.delete();
					if(decFile.createNewFile()){
						OutputStream targetFile=  new FileOutputStream(decFile);
			            targetFile.write(brokerDecImgInBytes);
			            targetFile.close();
			            logger.info("Writing dec image is done !");
					}
				}
			}catch(Exception e){
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				resultMap.put("msg", "Error in uploading image !");
				logger.error("Error in writing file ! : "+e);
				return resultMap;
			}		
			Integer id = broker.getBrkImgId();
			logger.info("BrokerImg ID : "+id);
			if (id != null && id > 0) {
				BrokerImg brkImg = (BrokerImg)session.get(BrokerImg.class, broker.getBrkImgId());
				brokerImg.setBrkImgId(broker.getBrkImgId());
				session.merge(brokerImg);
				brkImgId = broker.getBrkImgId();
				logger.info("BrokerImg is updated !");
			} else {
				brkImgId = (Integer) session.save(brokerImg);
				logger.info("BrokerImg is saved !");
			}
			
			broker.setBrkImgId(brkImgId);
			broker.setBrkIsPanImg(isPanImg);
			broker.setBrkIsDecImg(isDecImg);
			broker.setBrkPanIntRt(brkIntRate);
			broker.setBrkPanDOB(panService.getPanDOB());
			broker.setBrkPanDt(panService.getPanDt());
			broker.setBrkPanName(panService.getPanName());
			broker.setBrkPanNo(panService.getPanNo());
			
			session.merge(broker);
			transaction.commit();
			session.flush();
			
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			resultMap.put("msg", "success");
			logger.info("Broker is updated !");
		}catch(Exception e){
			logger.error("Error in saveBrkPan() : "+e);			
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", "Something is going wrong !");		
		}
		session.clear();
		session.close();
		logger.info("Exit from saveBrkPan()...");
		return resultMap;
	}

	@Override
	@Transactional
	public Blob getBrkPanImg(int brkImgId) {
		logger.info("Enter into getBrkPanImg() : BrkImgID = "+brkImgId);
		System.out.println("enter into getBrkPanImg function");
		Blob blob = null;
		try{
			session = this.sessionFactory.openSession();
			BrokerImg brokerImg = (BrokerImg) session.get(BrokerImg.class, brkImgId);			
			String panImgPath = brokerImg.getBrkPanImgPath();
			if(! panImgPath.equalsIgnoreCase("")){
				File file = new File(panImgPath);
				if(file.exists()){
					Path path = Paths.get(panImgPath);
					byte imgInBytes[] = Files.readAllBytes(path);
					blob = new SerialBlob(imgInBytes);
				}					
			}			
		}catch(Exception e){
			e.printStackTrace();
			logger.error("Exception : "+e);
		}
		session.clear();
		session.close();
		logger.info("Exit from getBrkPanImg()");
		return blob;
	}

	@Override
	@Transactional
	public List<Map<String, Object>> getBrkNameCodeIdFa() {
		System.out.println("entered into getBrkNameCodeIdFa");
		List<Map<String, Object>> brkNameCodeIdFaList = new ArrayList<>();
		
		try {
			session = this.sessionFactory.openSession();
			Query query = session.createQuery("select new map(brkId as brkId, brkCode as brkCode, brkName as brkName, brkFaCode as brkFaCode) from Broker"); 
			brkNameCodeIdFaList = query.list();
			session.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		session.clear();
		session.flush();
		return brkNameCodeIdFaList;
	}
	
	@Override
	public List<Broker> getBrokerByNameCode(Session session, User user, String brkNameCode, List<String> proList){
		logger.info("UserID = "+user.getUserId()+" : Enter into getBrokerByNameCode() : BrkNameCode = "+brkNameCode);
		List<Broker> brkList = null;	
		try{
			Criteria cr = session.createCriteria(Broker.class);
			if(proList != null){
				ProjectionList projectionList = Projections.projectionList();
				for(int i=0; i<proList.size(); i++)
					projectionList.add(Projections.property(proList.get(i)), proList.get(i));
				cr.setProjection(projectionList);
				cr.setResultTransformer(Transformers.aliasToBean(Broker.class));
			}
			if(NumberUtils.isNumber(brkNameCode))
				cr.add(Restrictions.like(BrokerCNTS.BRK_FA_CODE, "%"+brkNameCode));
			else
				cr.add(Restrictions.like(BrokerCNTS.BRK_NAME, brkNameCode+"%"));
			
			brkList = cr.list();
			logger.info("UserID = "+user.getUserId()+" : Broker Size = "+brkList.size());
		}catch(Exception e){
			logger.error("UserID = "+user.getUserId()+" : Exception = "+e);
		}
		logger.info("UserID = "+user.getUserId()+" : Exit from getBrokerByNameCode() : BrkNameCode = "+brkNameCode);
		return brkList;
	}
	
	
	
	@Override
	public List<Map<String, Object>> getBrkNamePhCodeIdFa() {
		
		List<Map<String, Object>> brkNameCodeIdFaList = new ArrayList<>();
		
		try {
			session = this.sessionFactory.openSession();
			Query query = session.createQuery("select new map(brkId as brkId, brkCode as brkCode, brkName as brkName,brkPhNoList as brkPhNoList,brkFaCode as brkFaCode) from Broker"); 
			brkNameCodeIdFaList = query.list();
			session.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return brkNameCodeIdFaList;
	}
	
	
	
	@Override
	public Boolean isBrokerExist(Session session,String panNo) {
		
		logger.info("Enter into isOwnerAvailable() : OwnerName = "+panNo);
		Boolean isAvailable = false;		
			List<String> brkList = session.createCriteria(Broker.class)
					.add(Restrictions.eq(BrokerCNTS.BRK_PAN_No, panNo))
					.setProjection(Projections.property(BrokerCNTS.BRK_PAN_NAME))
					.list();
			if(brkList.isEmpty())
				isAvailable = true;
			else
				isAvailable = false;
		return isAvailable;
	}
	
	
	
	@Override
	public List<Map<String, Object>> getBrkNameCodeByPanNo(String panNo) {
		
		List<Map<String, Object>> brkNameCodeList = new ArrayList<>();
		Session session=null;
		try {
			session = this.sessionFactory.openSession();
			Query query = session.createQuery("select new map(brkId as brkId, brkCode as brkCode, brkName as brkName,brkPanName as brkPanName,brkFaCode as brkFaCode) from Broker where brkPanNo ='"+panNo+"' "); 
			brkNameCodeList = query.list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return brkNameCodeList;
	}
	
	
	@Override
	public void updateBroker(Session session,Broker broker){
		logger.info("Enter into updateBroker()");
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);		
			
			broker.setCreationTS(calendar);
			session.saveOrUpdate(broker);
			
		logger.info("Exit from updateBroker()");
	}
	
	@Override
	public void updateBrkImg(Session session,BrokerImg brkImg){
		logger.info("Enter into updateBrokerImg()");
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);		
			
			brkImg.setCreationTS(calendar);
			session.saveOrUpdate(brkImg);
			
		logger.info("Exit from updateBrokerImg()");
	}
	
	
	@Override
	public int saveBrkImg(Session session,BrokerImg brkImg){
		logger.info("Enter into updateBrokerImg()");
		int temp=0;
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);		
			
			brkImg.setCreationTS(calendar);
			temp=(Integer) session.save(brkImg);
			
		logger.info("Exit from updateBrokerImg()");
		return temp;
	}
	
	
	@Override
	public int saveBrkAndImgN(Broker broker , BrokerImg brokerImg, Blob brokerPanImg, Blob brokerDecImg,Blob chqImg,Session session){
		System.out.println("enter into saveBrkAndImgN function");
		logger.info("Enter into saveBrkAndImgN()");
		User currentUser = (User) httpSession.getAttribute("currentUser");
			int brkId = (Integer) session.save(broker);			
			broker.setBrkCode("brk"+String.valueOf(brkId));
			String brkCode = "brk"+String.valueOf(brkId);
			if(brkId > 0){
				logger.info("Broker Saved ID = "+brkId);
				try {
					
					
					Path panPath = Paths.get(brokerPanImgPath);
					if(! Files.exists(panPath))
						Files.createDirectories(panPath);//make new directory if not exist
					
					Path decPath = Paths.get(brokerDecImgPath);
					if(! Files.exists(decPath))
						Files.createDirectories(decPath);//make new directory if not exist
					
					Path path = Paths.get(brokerBnkImgPath);
					if(! Files.exists(path))
						Files.createDirectories(path);//make new directory if not exist
					
					
					
						if(brokerPanImg != null){
							logger.info("Going to write broker pan image : PanImgPath = "+brokerPanImgPath+"/"+brkCode+".pdf");
							brokerImg.setBrkPanImgPath(brokerPanImgPath+"/"+brkCode+".pdf");
							byte brokerPanImgInBytes[] = brokerPanImg.getBytes(1, (int)brokerPanImg.length());
							File panFile = new File(brokerImg.getBrkPanImgPath());
							if(panFile.exists())
								panFile.delete();
							if(panFile.createNewFile()){											    
								OutputStream targetFile=  new FileOutputStream(panFile);
					            targetFile.write(brokerPanImgInBytes);			            
					            targetFile.close();
					            logger.info("Writing pan image is done !");
							}
						}
						if(brokerDecImg != null){
							logger.info("Going to write broker dec image : DecImgPath = "+brokerDecImgPath+"/"+brkCode+".pdf");
							brokerImg.setBrkDecImgPath(brokerDecImgPath+"/"+brkCode+".pdf");
							byte brokerDecImgInBytes[] = brokerDecImg.getBytes(1, (int)brokerDecImg.length());					
							File decFile = new File(brokerImg.getBrkDecImgPath());										
							if(decFile.exists())
								decFile.delete();
							if(decFile.createNewFile()){											    
								OutputStream targetFile=  new FileOutputStream(decFile);
					            targetFile.write(brokerDecImgInBytes);			            
					            targetFile.close();
					            logger.info("Writing dec image is done !");
							}
						}
						
						if(chqImg != null){
							logger.info("Going to write broker chq image : DecImgPath = "+brokerBnkImgPath+"/"+brkCode+".pdf");
							brokerImg.setBrkBnkDetImgPath(brokerBnkImgPath+"/"+brkCode+".pdf");
							byte brokerChqImgInBytes[] = chqImg.getBytes(1, (int)chqImg.length());					
							File chqFile = new File(brokerImg.getBrkBnkDetImgPath());										
							if(chqFile.exists())
								chqFile.delete();
							if(chqFile.createNewFile()){											    
								OutputStream targetFile=  new FileOutputStream(chqFile);
					            targetFile.write(brokerChqImgInBytes);			            
					            targetFile.close();
					            logger.info("Writing chq image is done !");
							}
							broker.setBrkBnkDet(true);
						}
					
					brokerImg.setBrkId(brkId);
					brokerImg.setbCode(currentUser.getUserBranchCode());
					brokerImg.setUserCode(currentUser.getUserCode());
					
					int brkImgId = (Integer) session.save(brokerImg);
					if(brkImgId > 0){
						broker.setBrkImgId(brkImgId);
						if(brokerPanImg != null){
							broker.setBrkIsPanImg(true);
						}
						if(brokerDecImg != null){
							broker.setBrkIsDecImg(true);
						}
					}
					
					session.update(broker);
					
				}catch(Exception e){
					e.printStackTrace();
					logger.info("Exception in saveing broker images"+e);
				}
					
			}else{
				logger.info("Broker is not saved !");
			}
			
		logger.info("Exit from saveBrkAndImg()");
		return brkId;
	}
	
	
	
	@Override
	public List<Broker> getBrkFrBnkVerify() {
		
		List<Broker> brkList = new ArrayList<>();
		Session session=null;
		try {
			session = this.sessionFactory.openSession();
			brkList = session.createCriteria(Broker.class)
					.add(Restrictions.eq(BrokerCNTS.IS_BRK_BNK_DET, true))
					.add(Restrictions.eq(BrokerCNTS.BRK_VALID_BNK_DET,false))
					.add(Restrictions.eq(BrokerCNTS.BRK_INVALID_BNK_DET,false)).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return brkList;
	}
	
	
	@Override
	public List<Broker> getBrokerInvalidAcc() {
		
		List<Broker> brkList = new ArrayList<>();
		Session session=null;
		try {
			session = this.sessionFactory.openSession();
			brkList = session.createCriteria(Broker.class)
					//.add(Restrictions.eq(BrokerCNTS.IS_BRK_BNK_DET, true))
					.add(Restrictions.eq(BrokerCNTS.BRK_INVALID_BNK_DET,true)).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return brkList;
	}
	
	
	@Override
	public Map<String,String> bnkInfoValid(Integer brkId){
		Map<String,String> map=new HashMap<>();
		User user=(User) httpSession.getAttribute("currentUser");
		Session session=this.sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		try {
			Broker brk=(Broker) session.get(Broker.class, brkId);
			brk.setBrkBnkDetValid(true);
			brk.setBrkBnkDetInValid(false);
			brk.setBrkBnkDetVerifyBy(user.getUserCode());
			session.update(brk);
			session.flush();
			session.clear();
			transaction.commit();
			
			map.put("result", "success");
		}catch (Exception e) {
			transaction.rollback();
			e.printStackTrace();
			map.put("result", "error");
			// TODO: handle exception
		}finally {
			session.close();
		}
		
		
		return map;
	}
	
	
	
	@Override
	public Map<String,String> bnkInfoInValid(int brkId){
		Map<String,String> map=new HashMap<>();
		User user=(User) httpSession.getAttribute("currentUser");
		Session session=this.sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		try {
			Broker brk=(Broker) session.get(Broker.class, brkId);
			brk.setBrkBnkDetValid(false);
			brk.setBrkBnkDetInValid(true);
			brk.setBrkBnkDetVerifyBy(user.getUserCode());
			session.update(brk);
			session.flush();
			session.clear();
			transaction.commit();
			
			map.put("result", "success");
		}catch (Exception e) {
			transaction.rollback();
			e.printStackTrace();
			map.put("result", "error");
			// TODO: handle exception
		}finally {
			session.close();
		}
		
		
		return map;
	}
	
	
	
	@Override
	public Map<String,String> bnkInfoUpdate(Broker newBrk){
		Map<String,String> map=new HashMap<>();
		User user=(User) httpSession.getAttribute("currentUser");
		Session session=this.sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		try {
			Broker brk=(Broker) session.get(Broker.class, newBrk.getBrkId());
			brk.setBrkAccntNo(newBrk.getBrkAccntNo());
			brk.setBrkAcntHldrName(newBrk.getBrkAcntHldrName());
			brk.setBrkBnkBranch(newBrk.getBrkBnkBranch());
			brk.setBrkIfsc(newBrk.getBrkIfsc());
			brk.setBrkBnkDetValid(true);
			brk.setBrkBnkDet(true);
			brk.setBrkBnkDetInValid(false);
			brk.setBrkBnkDetVerifyBy(user.getUserCode());
			session.update(brk);
			session.flush();
			session.clear();
			transaction.commit();
			
			map.put("result", "success");
		}catch (Exception e) {
			transaction.rollback();
			e.printStackTrace();
			map.put("result", "error");
			// TODO: handle exception
		}finally {
			session.close();
		}
		
		
		return map;
	}
	
	
	
	@Override
	public Map<String,String> uploadChqImg(byte fileInBytes[],int brkId){
		Map<String, String> map=new HashMap<>();
		User crnUser=(User) httpSession.getAttribute("currentUser");
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		try {
			
			
			Path path = Paths.get(brokerBnkImgPath);
			if(! Files.exists(path))
				Files.createDirectories(path);//make new directory if not exist
			
			
			Broker brk=(Broker) session.get(Broker.class, brkId);
			if(brk != null) {
				BrokerImg brkImg=(BrokerImg) session.get(BrokerImg.class, brk.getBrkImgId());
				
				if(brkImg!=null) {
					if(brkImg.getBrkBnkDetImgPath()!=null) {
						File chqFile = new File(brkImg.getBrkBnkDetImgPath());										
						if(chqFile.exists())
							chqFile.delete();
						
						FileOutputStream out = new FileOutputStream(chqFile);
						out.write(fileInBytes);
						out.close();
						//brkImg.setBrkBnkDetImgPath(brokerBnkImgPath+"/brk"+brkId+".pdf");
						brkImg.setUserCode(crnUser.getUserCode());
						session.update(brkImg);
					}else {
						File chqFile = new File(brokerBnkImgPath+"/brk"+brkId+".pdf");										
						if(chqFile.exists())
							chqFile.delete();
						
						FileOutputStream out = new FileOutputStream(chqFile);
						out.write(fileInBytes);
						out.close();
						
						brkImg.setBrkBnkDetImgPath(brokerBnkImgPath+"/brk"+brkId+".pdf");
						brkImg.setUserCode(crnUser.getUserCode());
						session.update(brkImg);
					}
					
					
				}else {
					
					BrokerImg brkImgN=new BrokerImg();
					File chqFile = new File(brokerBnkImgPath+"/brk"+brkId+".pdf");										
					if(chqFile.exists())
						chqFile.delete();
					
					FileOutputStream out = new FileOutputStream(chqFile);
					out.write(fileInBytes);
					out.close();
					brkImgN.setbCode(crnUser.getUserBranchCode());
					brkImgN.setBrkId(brkId);
					brkImgN.setUserCode(crnUser.getUserCode());
					
					brkImgN.setBrkBnkDetImgPath(brokerBnkImgPath+"/brk"+brkId+".pdf");
					int brkImgId=(int) session.save(brkImgN);
					
					brk.setBrkImgId(brkImgId);
					session.update(brk);
				}
				
			}
			
			session.flush();
			session.clear();
			transaction.commit();
			
			map.put("result", "success");
			map.put("msg", "success");
		}catch(Exception e) {
			e.printStackTrace();
			transaction.rollback();
			map.put("result", "error");
			map.put("msg", "There is some problem to upload file");
			map.put("exp", "Exception="+e);
		}finally {
			session.close();
		}
		
		
		return map;
	}
	
	
	@Override
	public Map<String,String> uploadPanImg(byte fileInBytes[],int brkId){
		Map<String, String> map=new HashMap<>();
		User crnUser=(User) httpSession.getAttribute("currentUser");
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		try {
			
			
			Path path = Paths.get(brokerPanImgPath);
			if(! Files.exists(path))
				Files.createDirectories(path);//make new directory if not exist
			
			
			Broker brk=(Broker) session.get(Broker.class, brkId);
			if(brk != null) {
				BrokerImg brkImg=(BrokerImg) session.get(BrokerImg.class, brk.getBrkImgId());
				
				if(brkImg!=null) {
					if(brkImg.getBrkPanImgPath()!=null) {
						File panFile = new File(brkImg.getBrkPanImgPath());										
						if(panFile.exists())
							panFile.delete();
						
						FileOutputStream out = new FileOutputStream(panFile);
						out.write(fileInBytes);
						out.close();
						//ownImg.setOwnBnkDetImgPath(ownerBankDetImgPath+"/own"+ownId+".pdf");
						brkImg.setUserCode(crnUser.getUserCode());
						session.update(brkImg);
					}else {
						File panFile = new File(brokerPanImgPath+"/brk"+brkId+".pdf");										
						if(panFile.exists())
							panFile.delete();
						
						FileOutputStream out = new FileOutputStream(panFile);
						out.write(fileInBytes);
						out.close();
						
						brkImg.setBrkPanImgPath(brokerPanImgPath+"/brk"+brkId+".pdf");
						brkImg.setUserCode(crnUser.getUserCode());
						session.update(brkImg);
					}
					
					
				}else {
					
					BrokerImg brkImgN=new BrokerImg();
					File panFile = new File(brokerPanImgPath+"/brk"+brkId+".pdf");										
					if(panFile.exists())
						panFile.delete();
					
					FileOutputStream out = new FileOutputStream(panFile);
					out.write(fileInBytes);
					out.close();
					brkImgN.setbCode(crnUser.getUserBranchCode());
					brkImgN.setBrkId(brkId);
					brkImgN.setUserCode(crnUser.getUserCode());
					
					brkImgN.setBrkPanImgPath(brokerPanImgPath+"/brk"+brkId+".pdf");
					int brkImgId=(int) session.save(brkImgN);
					
					brk.setBrkImgId(brkImgId);
					
				}
				
			}
			brk.setBrkIsPanImg(true);
			session.update(brk);
			session.flush();
			session.clear();
			transaction.commit();
			
			map.put("result", "success");
			map.put("msg", "success");
		}catch(Exception e) {
			e.printStackTrace();
			transaction.rollback();
			map.put("result", "error");
			map.put("msg", "There is some problem to upload file");
			map.put("exp", "Exception="+e);
		}finally {
			session.close();
		}
		
		return map;
	}
	
	
	@Override
	public Map<String,String> uploadDecImg(byte fileInBytes[],int brkId){
		Map<String, String> map=new HashMap<>();
		User crnUser=(User) httpSession.getAttribute("currentUser");
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		try {
			
			
			Path path = Paths.get(brokerDecImgPath);
			if(! Files.exists(path))
				Files.createDirectories(path);//make new directory if not exist
			
			
			Broker brk=(Broker) session.get(Broker.class, brkId);
			if(brk != null) {
				BrokerImg brkImg=(BrokerImg) session.get(BrokerImg.class, brk.getBrkImgId());
				
				if(brkImg!=null) {
					if(brkImg.getBrkDecImgPath()!=null) {
						File decFile = new File(brkImg.getBrkDecImgPath());										
						if(decFile.exists())
							decFile.delete();
						
						FileOutputStream out = new FileOutputStream(decFile);
						out.write(fileInBytes);
						out.close();
						//ownImg.setOwnBnkDetImgPath(ownerBankDetImgPath+"/own"+ownId+".pdf");
						brkImg.setUserCode(crnUser.getUserCode());
						session.update(brkImg);
					}else {
						File decFile = new File(brokerDecImgPath+"/brk"+brkId+".pdf");										
						if(decFile.exists())
							decFile.delete();
						
						FileOutputStream out = new FileOutputStream(decFile);
						out.write(fileInBytes);
						out.close();
						
						brkImg.setBrkDecImgPath(brokerDecImgPath+"/brk"+brkId+".pdf");
						brkImg.setUserCode(crnUser.getUserCode());
						session.update(brkImg);
					}
					
					
				}else {
					
					BrokerImg brkImgN=new BrokerImg();
					File decFile = new File(brokerDecImgPath+"/brk"+brkId+".pdf");										
					if(decFile.exists())
						decFile.delete();
					
					FileOutputStream out = new FileOutputStream(decFile);
					out.write(fileInBytes);
					out.close();
					brkImgN.setbCode(crnUser.getUserBranchCode());
					brkImgN.setBrkId(brkId);
					brkImgN.setUserCode(crnUser.getUserCode());
					
					brkImgN.setBrkDecImgPath(brokerDecImgPath+"/brk"+brkId+".pdf");
					int brkImgId=(int) session.save(brkImgN);
					
					brk.setBrkImgId(brkImgId);
					
				}
				
			}
			brk.setBrkIsDecImg(true);
			brk.setBrkPanIntRt(0);
			session.update(brk);
			session.flush();
			session.clear();
			transaction.commit();
			
			map.put("result", "success");
			map.put("msg", "success");
		}catch(Exception e) {
			e.printStackTrace();
			transaction.rollback();
			map.put("result", "error");
			map.put("msg", "There is some problem to upload file");
			map.put("exp", "Exception="+e);
		}finally {
			session.close();
		}
		
		return map;
	}


	
	
	
	@SuppressWarnings("unchecked")
	public List<Broker> getBrkFrAdvByName(String brkName){
		System.out.println("enter into getBrkFrAdvByName function");		
		List<Broker> brkList = new ArrayList<>();
		Session session=this.sessionFactory.openSession();
		try{
			Criteria cr=session.createCriteria(Broker.class);
			cr.add(Restrictions.like(BrokerCNTS.BRK_NAME, brkName+"%"));
			
			brkList =cr.list();
			
			
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			session.clear();
			session.close();
		}
		return brkList;
	}
	
	@Override
	public Broker getBrkByCode(String brkCode,Session session) {
		Broker broker=null;
		
		List<Broker>brkList=session.createCriteria(Broker.class)
				.add(Restrictions.eq(BrokerCNTS.BRK_CODE, brkCode)).list();
		
		if(!brkList.isEmpty()) {
			broker=brkList.get(0);
		}
				
		return broker;
	}
	
	
	@Override
	public int saveBroker(Broker broker,Session session){
			return (Integer) session.save(broker);
	}
	
	@Override
	public List<Broker> getPhoneNo(){
		
		List<Broker> bList=new ArrayList<>();
		Session session = this.sessionFactory.openSession();
		try {
			
			Criteria cr=session.createCriteria(Broker.class);
			ProjectionList projectionList=Projections.projectionList();
			projectionList.add(Projections.property("brkCode"),"brkCode");
			projectionList.add(Projections.property("brkPhNoList"),"brkPhNoList");
			
			cr.setProjection(projectionList);
			cr.setResultTransformer(Transformers.aliasToBean(Broker.class));
			
			bList=cr.list();
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.close();
		
		return bList;
	}
	
	
	
	@SuppressWarnings("unchecked")
	@Override
	public Broker getBrkByCode(String brkCode){
		System.out.println("enter into getBrkByCode function");
		Broker broker = null;
		try{
			List<Broker> brkList = new ArrayList<>();
			session = this.sessionFactory.openSession();
			Criteria cr=session.createCriteria(Broker.class);
			cr.add(Restrictions.eq(BrokerCNTS.BRK_CODE,brkCode));
			brkList = cr.list();
			if(!brkList.isEmpty()){
				broker = brkList.get(0);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return broker;
	}
	
}
