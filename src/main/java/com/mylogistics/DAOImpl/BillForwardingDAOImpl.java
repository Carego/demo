package com.mylogistics.DAOImpl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.BillForwardingDAO;
import com.mylogistics.constants.BillForwardingCNTS;
import com.mylogistics.constants.CashStmtCNTS;
import com.mylogistics.constants.CashStmtStatusCNTS;
import com.mylogistics.controller.BillCntlr;
import com.mylogistics.model.Bill;
import com.mylogistics.model.BillForwarding;
import com.mylogistics.model.Branch;
import com.mylogistics.model.CashStmt;
import com.mylogistics.model.CashStmtStatus;
import com.mylogistics.model.Customer;
import com.mylogistics.model.User;

public class BillForwardingDAOImpl implements BillForwardingDAO{
		
		private SessionFactory sessionFactory;
		private Session session;
		private Transaction transaction;
		
		@Autowired
		private HttpSession httpSession;
		
		public static Logger logger = Logger.getLogger(BillForwardingDAOImpl.class);
		
		
		@Autowired
		public BillForwardingDAOImpl(SessionFactory sessionFactory) {
			this.sessionFactory = sessionFactory;
		}
		
		//@Transactional
		@SuppressWarnings("unchecked")
		public String saveBF(BillForwarding billForwarding){
			System.out.println("enter into saveBF function");
			User currentUser = (User) httpSession.getAttribute("currentUser");
			String res = null;
			Session session = this.sessionFactory.openSession();
			Transaction transaction = session.beginTransaction();
			
			try{
				
				List<Bill> billList = new ArrayList<>();
				billList = billForwarding.getBillList();
				
				
				List<BillForwarding> bfList = new ArrayList<>();
				Query query = session.createQuery("from BillForwarding where bfBrhCode = :type order by bfId DESC");
				query.setParameter("type", currentUser.getUserBranchCode());
				bfList = query.setMaxResults(1).list();
				
				String brh = "";
				String preBf = "/BF";
				String bfNo = "";
				Branch branch = (Branch) session.get(Branch.class,Integer.parseInt(currentUser.getUserBranchCode()));
				if(branch != null){
					if(branch.getBranchId()==6) {
						brh="RIP";
					}else
						brh = branch.getBranchName().substring(0,3).toUpperCase();
				}
				 
				
				if(brh != null){
					
					if(!billList.isEmpty()){
						BillForwarding actBF = new BillForwarding();
						actBF.setBfBrhCode(currentUser.getUserBranchCode());
						actBF.setUserCode(currentUser.getUserCode());
						actBF.setbCode(currentUser.getUserBranchCode());
						actBF.setBfTotAmt(billForwarding.getBfTotAmt());
						
						if(!bfList.isEmpty()){
							String prevBfNo = bfList.get(0).getBfNo();
							bfNo = brh + preBf + String.valueOf(Integer.parseInt(prevBfNo.substring(6)) + 1 + 100000).substring(1);
						}else{
							bfNo = brh+preBf+"00001";
						}
						
						
						actBF.setBfNo(bfNo);
						actBF.setBfCustId(billForwarding.getBfCustId());
						int bfId = (Integer) session.save(actBF);
						
						for(int i=0;i<billList.size();i++){
							Bill bill = billList.get(i);
							Bill actBill = (Bill) session.get(Bill.class,bill.getBlId());
							
							BillForwarding freshBF = (BillForwarding) session.get(BillForwarding.class,bfId);
							actBill.setBillForwarding(freshBF);
							freshBF.getBillList().add(actBill);
							
							session.merge(actBill);
							session.merge(freshBF);
							
							
							
							
							
							//TODO
							
							Customer customer= (Customer) session.get(Customer.class, actBill.getBlCustId());
							
							if(customer.getCustName().equalsIgnoreCase("RELIANCE JIO INFOCOMM LTD")){
								
								List<CashStmt> csList=new ArrayList<>();
								
								csList = session.createCriteria(CashStmt.class)
										.add(Restrictions.eq(CashStmtCNTS.CS_TV_NO, actBill.getBlBillNo())).list();
								
								System.out.println("Size "+csList.size());
								if(csList.isEmpty()){

									List<CashStmtStatus> cashStmtStatusList = new ArrayList<CashStmtStatus>();
									Query queryS = session
											.createQuery("from CashStmtStatus where bCode= :type and cssDt >= '2017-11-01' order by cssId DESC");
									queryS.setParameter("type", currentUser.getUserBranchCode());
									cashStmtStatusList = queryS.setMaxResults(1).list();
									
									Date csDt=cashStmtStatusList.get(0).getCssDt();

									System.out.println("date "+csDt);
									actBill.setBlBillDt(csDt);
									session.update(actBill);
									
									
									 int voucherNo = 0;
									 
									 Criteria cr = session.createCriteria(CashStmt.class);
									 cr.add(Restrictions.eq(CashStmtCNTS.CS_CDT,actBill.getBlBillDt()));
									 cr.add(Restrictions.eq(CashStmtCNTS.USER_BRANCH_CODE,currentUser.getUserBranchCode()));
									 ProjectionList proList = Projections.projectionList();
									 proList.add(Projections.property("csVouchNo"));
									 cr.setProjection(proList);
										
									 List<Integer>  voucherNoList = cr.list();
									 if(!voucherNoList.isEmpty()){
											int vNo = voucherNoList.get(voucherNoList.size() - 1);
											voucherNo = vNo + 1;
									 }else{
											voucherNo = 1;
									 }
									 
									 
									 List<CashStmtStatus> cssList = new ArrayList<>();
									 cr = session.createCriteria(CashStmtStatus.class);
									 cr.add(Restrictions.eq(CashStmtStatusCNTS.B_Code,currentUser.getUserBranchCode()));
									 cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_DT,actBill.getBlBillDt()));
									 cssList = cr.list();
									 
									 if(!cssList.isEmpty()){
										 CashStmtStatus css = cssList.get(0);
										 
										 CashStmt cs1 = new CashStmt();
										 cs1.setUserCode(currentUser.getUserCode());
										 cs1.setbCode(currentUser.getUserBranchCode());
										 //cs1.setCsDescription(bill.getBlDesc());
										 cs1.setCsDescription("As Per Bill File");
										 cs1.setCsDrCr('C');
										 cs1.setCsAmt(actBill.getBlSubTot());
										 cs1.setCsType("BILL");
										 cs1.setCsTvNo(actBill.getBlBillNo());
										 cs1.setCsVouchType("CONTRA");
										 cs1.setCsFaCode("1200001");
										 cs1.setCsVouchNo(voucherNo);
										 cs1.setCsDt(css.getCssDt());
										 
										 cs1.setCsNo(css);
										 css.getCashStmtList().add(cs1);
										 session.merge(css);
										 
										 
										 CashStmt cs2 = new CashStmt();
										 cs2.setUserCode(currentUser.getUserCode());
										 cs2.setbCode(currentUser.getUserBranchCode());
										 //cs2.setCsDescription(bill.getBlDesc());
										 cs2.setCsDescription("As Per Bill File");
										 cs2.setCsDrCr('D');
										 cs2.setCsAmt(actBill.getBlFinalTot());
										 cs2.setCsType("BILL");
										 cs2.setCsTvNo(actBill.getBlBillNo());
										 cs2.setCsVouchType("CONTRA");
										 cs2.setCsFaCode(customer.getCustFaCode());
										 cs2.setCsVouchNo(voucherNo);
										 cs2.setCsDt(css.getCssDt());
										 
										 CashStmtStatus actCss = (CashStmtStatus) session.get(CashStmtStatus.class,css.getCssId());
										 cs2.setCsNo(actCss);
										 actCss.getCashStmtList().add(cs2);
										 session.merge(actCss);

									 }

								}
								
							}
							

														
						}
						
						res = bfNo;
					}
					transaction.commit();
					
				}else{
					System.out.println("User doesn't have any branch");
					res = null;
				}
			}catch(Exception e){
				e.printStackTrace();
				transaction.rollback();
				logger.info("Exception in bill forwarding saveBF()");
				res = null;
			}finally{
				session.clear();
				session.close();
			}
			return res;
		}
		
		
		@Transactional
		@SuppressWarnings("unchecked")
		public List<BillForwarding> getBfFrBillSbmsn(int custId){
			System.out.println("enter into getBfFrBillSbmsn function");
			List<BillForwarding> bfList = new ArrayList<>();
			try{
				session = this.sessionFactory.openSession();
				transaction = session.beginTransaction();
				
				Criteria cr = session.createCriteria(BillForwarding.class);
				cr.add(Restrictions.eq(BillForwardingCNTS.BF_CUST_ID,custId));
				cr.add(Restrictions.isNull(BillForwardingCNTS.BF_REC_DT));
				
				bfList = cr.list();
				session.flush();
			}catch(Exception e){
				e.printStackTrace();
			}
			session.clear();
			session.close();
			return bfList;
		}
		
		
		@Transactional
		@SuppressWarnings("unchecked")
		public List<Bill> getBillListById(int bfId){
			System.out.println("enter into getBillListById function = "+bfId);
			List<Bill> blList = new ArrayList<>();
			try{
				session = this.sessionFactory.openSession();
				transaction = session.beginTransaction();
				
				BillForwarding billForwarding = (BillForwarding) session.get(BillForwarding.class,bfId);
				if(billForwarding != null){
					Hibernate.initialize(billForwarding.getBillList());
					blList = billForwarding.getBillList();
				}
				
				session.flush();
			}catch(Exception e){
				e.printStackTrace();
			}
			session.clear();
			session.close();
			return blList;
		}
		
		
		@SuppressWarnings("unchecked")
		@Override
		public int saveBlSbmsnDt(BillForwarding billForwarding,Session session){
			System.out.println("enter into saveBlSbmsnDt function");
			int res = 0;
				BillForwarding actBF = (BillForwarding) session.get(BillForwarding.class,billForwarding.getBfId());
				actBF.setBfRecDt(billForwarding.getBfRecDt());
				actBF.setBfImagePath(billForwarding.getBfImagePath());
				session.merge(actBF);
				List<Bill> blList = new ArrayList<>();
				Hibernate.initialize(actBF.getBillList());
				blList = actBF.getBillList();
				if(!blList.isEmpty()){
					for(int i=0;i<blList.size();i++){
						Bill bill = blList.get(i);
						bill.setBlBillSubDt(billForwarding.getBfRecDt());
						session.update(bill);
					}
				}
				res = 1;
			return res;
		}
		
		
		@Transactional
		@SuppressWarnings("unchecked")
		public Map<String,Object> getBillFrwdPrint(String bfNo){
			System.out.println("enter into getBillFrwdPrint function");
			Map<String,Object> bfMap = new HashMap<String, Object>();
			try{
				session = this.sessionFactory.openSession();
				transaction = session.beginTransaction();
				
				List<BillForwarding> bfList = new ArrayList<BillForwarding>();
				Criteria cr = session.createCriteria(BillForwarding.class);
				cr.add(Restrictions.eq(BillForwardingCNTS.BF_NO,bfNo));
				bfList = cr.list();
				
				if(!bfList.isEmpty()){
					BillForwarding billForwarding = bfList.get(0);
					Branch branch = (Branch) session.get(Branch.class,Integer.parseInt(billForwarding.getBfBrhCode()));
					Customer customer = (Customer) session.get(Customer.class,billForwarding.getBfCustId());
					
					bfMap.put("bf", billForwarding);
					bfMap.put("brhAdd",branch.getBranchAdd());
					bfMap.put("brhPhNo",branch.getBranchPhone());
					bfMap.put("brhEmail",branch.getBranchEmailId());
					bfMap.put("custName",customer.getCustName());
					bfMap.put("custAdd",customer.getCustAdd());
					bfMap.put("custCity",customer.getCustCity());
					bfMap.put("custPin",customer.getCustPin());
					
					List<Map<String,Object>> blList = new ArrayList<Map<String,Object>>();
					
					if(!billForwarding.getBillList().isEmpty()){
						for(int i=0;i<billForwarding.getBillList().size();i++){
							Map<String,Object> billMap = new HashMap<String, Object>();
							billMap.put("blNo",billForwarding.getBillList().get(i).getBlBillNo());
							billMap.put("blDt",billForwarding.getBillList().get(i).getBlBillDt());
							billMap.put("blAmt",billForwarding.getBillList().get(i).getBlFinalTot());
							
							blList.add(billMap);
						}
					}
					
					bfMap.put("blList",blList);
				}
				
			}catch(Exception e){
				e.printStackTrace();
			}
			session.clear();
			session.close();
			return bfMap;
		}
		
		
		
		@Override
		public BillForwarding getPreviousBFbyCustBr(Integer custId,String branchCode) {
			Session session=this.sessionFactory.openSession();
			BillForwarding billForwarding=null;
			List<BillForwarding> list=new ArrayList<>();
			try {
				//billForwarding=(BillForwarding);
				list=session.createCriteria(BillForwarding.class)
						.add(Restrictions.eq(BillForwardingCNTS.BF_CUST_ID, custId))
						.add(Restrictions.eq(BillForwardingCNTS.BF_BRH_CODE, branchCode))
						.addOrder(Order.desc(BillForwardingCNTS.BF_NO))
						.setMaxResults(1).list();
				if(!list.isEmpty())
					billForwarding=list.get(0);
				System.out.println("listSize="+list.size());
				
			}catch(Exception e) {
				e.printStackTrace();
			}
			session.clear();
			session.close();
			return billForwarding;
		}
		
		
}
