<div ng-show="operatorLogin || superAdminLogin">
<title>View State</title>
<style>
th{color:#26A69A;}
</style>
<!-- <div ng-controller = "StateCntlr" > -->

<div class="row">
	<div class="col s3 hidden-xs hidden-sm"> &nbsp; </div>
		<form class="col s6 card" style="align: center; margin-top: 40px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" ng-submit="submitCode(stateName,stateCode)" >
	 <div id ="stateCode" >
			<div class="input-field col s6">
				<input type="text" id="sName" name="stateName" ng-model="stateName" required readonly ng-click="openStateCodeDB()"/>	
   				<input type="hidden" name="stateCode" ng-model="stateCode"/>
   				<label>Enter State Code</label>
 			</div>
      		<div class="input-field col s6 center">
      			<input class="btn waves-effect waves-light" type="submit" value="Submit">
      		</div>
	 </div> 
		</form>
		<div class="col s3"> &nbsp; </div>
</div>


<div id="stateDetails" ng-hide="stateDetailsFlag"> 
<div class="row">
	<div class="col s3 hidden-xs hidden-sm"> &nbsp; </div>
	<div ng-hide = "show" class="col s6 card" style="align: center; margin-top: 40px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >	
		<h4>View State</h4>	
		<table class="table-bordered table-hover table-condensed">
	         <tr>
								
				<th>State Name</th>
			
				<th>State Lorry Prefix</th>
							
				<!-- <th>State Code</th>
				
				<th>State STD</th>
				
				<th>State CreationTS</th> -->
				
			</tr>
 	 		  
		  <tr ng-repeat="state in stateList">
		 	  <td>{{ state.stateName }}</td>
              <td>{{ state.stateLryPrefix }}</td>
              <!-- <td>{{ state.stateCode }}</td>
              <td>{{ state.stateSTD }}</td>
              <td>{{ state.creationTS }}</td> -->
          </tr>
		</table>
	</div>
</div>	
</div>

<div id ="stateCodeDB" ng-hide="stateCodeDBFlag"> 
 	   	<input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
		<table>
		 
		<tr>
			<th></th>					
			<th>State Name</th>
			<th>State Lorry Prefix</th>
		</tr>
		 
		  <tr ng-repeat="state in stateList  | filter:filterTextbox">
		 	  <td><input type="radio"  name="stateName" class="statecls"  value="{{ state }}" ng-model="stateName" ng-click="saveStateCode(state)"></td>
              <td>{{ state.stateName }}</td>
              <td>{{ state.stateLryPrefix }}</td>
          </tr>
         </table>
</div>

<div id="stateCodeDetails" ng-hide="stateCodeDetailsFlag">
	<div class="row">
		<div class="col s3 hidden-xs hidden-sm"> &nbsp; </div>
		<div class="col s6 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >	
			<h4>Here's the details of State <span class="teal-text text-lighten-2">{{state.stateName}}</span>:</h4>
			
			<form name="StateForm" ng-submit="updateState(StateForm)">	
			<table class="table-bordered table-hover table-condensed">
	            
	            <tr>
	                <td>State Name:</td>
	                <td><input type ="text" name ="stateName" ng-model="state.stateName" value={{state.stateName}}  ng-required="true" ng-minlength="3" ng-maxlength="40"></td>
	            </tr>
	            
	            <tr>
	                <td>State Code:</td>
	                 <td><input type ="text" name ="stateCode" ng-model="state.stateCode" value={{state.stateCode}} readonly ></td>
	            </tr>
	            
	             <tr>
	                <td>State Lorry Prefix:</td>
	                <td><input type ="text" name ="stateLryPrefix" ng-model="state.stateLryPrefix" value={{state.stateLryPrefix}} ng-required="true" ng-minlength="2"></td>
	            </tr>
	            
	            <tr>
	                <td>State STD:</td>
	                <td><input type ="text" id="std" name ="stateSTD" ng-model="state.stateSTD" value={{state.stateSTD}} ng-required="true" ng-minlength="2" ng-maxlength="5"></td>
	            </tr>
	            
	            <tr>
					<td>CreationTS</td>
					<td><input type ="text" name ="creationTS" ng-model="time" value={{time}} required readonly></td>
				</tr>
		</table>
				<input type="submit" value="Submit!">
	</form>
	      
</div>		
</div>
</div>
		
</div>
<!--  <table>
 	 		<tr>
								
				<th>State Name</th>
			
				<th>State Lorry Prefix</th>
							
				<th>State Code</th>
				
				<th>State STD</th>
				
			</tr>
 	 		  
		  <tr ng-repeat="state in stateList">
		 	  <td>{{ state.stateName }}</td>
              <td>{{ state.stateLryPrefix }}</td>
              <td>{{ state.stateCode }}</td>
              <td>{{ state.stateSTD }}</td>
          </tr>
         </table> -->

<!-- </div> -->

