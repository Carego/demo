package com.mylogistics.services;

import java.sql.Date;

import org.springframework.stereotype.Service;

@Service
public class ReconRptService {
	
	private int frmBrId;
	
	private int toBrId;
	
	private Date frmDt;
	
	private Date toDt;

	public int getFrmBrId() {
		return frmBrId;
	}

	public void setFrmBrId(int frmBrId) {
		this.frmBrId = frmBrId;
	}

	public int getToBrId() {
		return toBrId;
	}

	public void setToBrId(int toBrId) {
		this.toBrId = toBrId;
	}

	public Date getFrmDt() {
		return frmDt;
	}

	public void setFrmDt(Date frmDt) {
		this.frmDt = frmDt;
	}

	public Date getToDt() {
		return toDt;
	}

	public void setToDt(Date toDt) {
		this.toDt = toDt;
	}
	
}
