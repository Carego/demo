package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.mylogistics.DAO.EditBillDAO;
import com.mylogistics.constants.BillCNTS;
import com.mylogistics.constants.CashStmtCNTS;
import com.mylogistics.constants.CnmtCNTS;
import com.mylogistics.model.Bill;
import com.mylogistics.model.BillDetail;
import com.mylogistics.model.Branch;
import com.mylogistics.model.CashStmt;
import com.mylogistics.model.CashStmtTemp;
import com.mylogistics.model.Cnmt;
import com.mylogistics.model.Customer;
import com.mylogistics.model.ServiceTax;
import com.mylogistics.model.Station;
import com.mylogistics.services.BillDetService;
import com.mylogistics.services.BillService;
import com.mylogistics.services.ConstantsValues;

public class EditBillDAOImpl implements EditBillDAO{

	public static final Logger logger = Logger.getLogger(EditBillDAOImpl.class);
	
	@Override
	public List<Branch> getAllBranch(Session session) {
		// TODO Auto-generated method stub
		
		Transaction transaction=session.beginTransaction();
		Criteria criteria=session.createCriteria(Branch.class);
		 List<Branch> listBranch=criteria.list();
		transaction.commit();
		return listBranch;
	}
	
	@Override
	public List<Bill> getAllBill(Session session, Integer brhId) {
		// TODO Auto-generated method stub
		
		Transaction transaction=session.beginTransaction();
		  Criteria criteria=session.createCriteria(Bill.class);
		    criteria.add(Restrictions.eq(BillCNTS.BILL_BRH_ID,brhId));
		    criteria.add(Restrictions.ge(BillCNTS.BILL_REM_AMT,0.0));
		    criteria.add(Restrictions.eq(BillCNTS.BILL_CANCEL,false));
		    List<Bill> billList=criteria.list();
		    transaction.commit();
		    System.out.println(billList.size());
		   return billList;
	}
	
	@Override
	public Object getBillFromBranch(Session session,String blBillNO, Integer brhId,ServiceTax serviceTax) {
		// TODO Auto-generated method stub
		List <Map<String,Object>> billList=new ArrayList<Map<String,Object>>();
		Map<String,Object> map=new HashMap<String, Object>();
		List<Bill> blList=new ArrayList<Bill>();
		List<String> cnmtList=new ArrayList<String>();
	
		List<Map<String,Object>> billDetList=new ArrayList<Map<String,Object>>();
		
		Transaction transaction=session.beginTransaction();
		   Criteria blCriteria=session.createCriteria(Bill.class);
		    blCriteria.add(Restrictions.eq(BillCNTS.BILL_BRH_ID,brhId));
		    blCriteria.add(Restrictions.eq(BillCNTS.Bill_NO,blBillNO));
		     blList=blCriteria.list();
		   map.put("blList",blList);
		    if(!blList.isEmpty()){
		       Integer custCode=blList.get(0).getBlCustId();
		       Criteria cnmtCriteria=session.createCriteria(Cnmt.class);
   			cnmtCriteria.add(Restrictions.eq(CnmtCNTS.CUST_CODE,String.valueOf(custCode)));
   			 cnmtCriteria.add(Restrictions.isNull(CnmtCNTS.CNMT_BILL_NO));
   			 cnmtCriteria.add(Restrictions.eq(CnmtCNTS.CNMT_BILL_AT,String.valueOf(brhId)));
   			  cnmtCriteria.add(Restrictions.eq(CnmtCNTS.CNMT_CANCEL,false));
   			  ProjectionList proj=Projections.projectionList();
   			    proj.add(Projections.property(CnmtCNTS.CNMT_CODE));
   			     cnmtCriteria.setProjection(proj);
   			  cnmtList=cnmtCriteria.list();
		    }
		   
		    	    
		   
		         for(Bill bill:blList){
		        	 Customer customer = (Customer) session.get(Customer.class,bill.getBlCustId());
					 map.put("cust",customer);
		        	List<BillDetail> billDetailList=bill.getBillDetList();
		        	 for(BillDetail bd:billDetailList){
		        		 Map<String,Object> detailMap=new HashMap<String,Object>();
		        		 detailMap.put("billDetail",bd);
		        		 Cnmt cnmt=(Cnmt)session.get(Cnmt.class,bd.getBdCnmtId());
		        		 detailMap.put("cnmt",cnmt);
		        		 Station frStn = (Station) session.get(Station.class,Integer.parseInt(cnmt.getCnmtFromSt()));
						 Station toStn = (Station) session.get(Station.class,Integer.parseInt(cnmt.getCnmtToSt()));
						 detailMap.put("frStn",frStn.getStnName());
						 detailMap.put("toStn",toStn.getStnName());
		        		  billDetList.add(detailMap);
		        	 }
		        	     
		         }
		     
		    map.put("serTax",serviceTax);
		    map.put("blDetList",billDetList);
		    map.put("cnmtList",cnmtList);
		    billList.add(map);
		   transaction.commit();
		   
		   
		
		return billList;
	}
	
	@Override
	public Object modifyBill(Session session, BillService billService) {
		// TODO Auto-generated method stub
	 Map<String,Object> map=new HashMap<String,Object>();
	   List<String> cnmtCodeList=new ArrayList<String>();
	   List<BillDetService> bdservList=new ArrayList<BillDetService>();
	     List<BillDetail> billDetailList=new ArrayList<BillDetail>();     // database billDetailList......
	     List<BillDetail> clientAddBlDetailList=new ArrayList<BillDetail>(); // clientSide billDetailList......
	     List<Integer> blCnmtListId=new ArrayList<Integer>();
	     List<Integer> oldCnmtId=new ArrayList<Integer>();
	     
		Bill bl=billService.getBill();
		Double blFinalTot=bl.getBlFinalTot();
		String blNo=bl.getBlBillNo();
		System.out.println("blFinalTot="+blFinalTot);
		System.out.println("blNumber="+blNo);
		
		 bdservList= billService.getBdSerList();
		 List<String> servCnmtList=new ArrayList<String>();   
		   for(BillDetService bds:bdservList){
			   servCnmtList.add(bds.getCnmt().getCnmtCode());
		   }
		System.out.println("servCnmtList="+servCnmtList.size());
		 Criteria criteria=session.createCriteria(CashStmt.class);
		          criteria.add(Restrictions.eq(CashStmtCNTS.CS_TV_NO,blNo));
		  List<CashStmt> csList=criteria.list();
		   if(!csList.isEmpty()){
			   
						  for(CashStmt cs:csList){
							  if(cs.getCsFaCode()!=null&&!cs.getCsFaCode().equals("1110200"))
							  cs.setCsAmt(blFinalTot);			//cs update......
						  }
						  
				 
				          Bill bill=(Bill)session.get(Bill.class,bl.getBlId());
						  billDetailList=bill.getBillDetList();
						    for(int i=0;i<billDetailList.size();i++){
						    	for(int j=0;j<bdservList.size();j++){
						    		BillDetail databaseBlDet=billDetailList.get(i);
						    		BillDetail editBlDet=bdservList.get(j).getBillDetail();
						    		 if(databaseBlDet.getBdId()==editBlDet.getBdId()){
						    			 databaseBlDet.setBdActWt(editBlDet.getBdActWt());
						    			 databaseBlDet.setBdChgWt(editBlDet.getBdChgWt());
						    			 databaseBlDet.setBdRecWt(editBlDet.getBdRecWt());
						    			 databaseBlDet.setBdRate(editBlDet.getBdRate());
						    			 databaseBlDet.setBdFreight(editBlDet.getBdFreight());
						    			 databaseBlDet.setBdLoadAmt(editBlDet.getBdLoadAmt());
						    			 databaseBlDet.setBdUnloadAmt(editBlDet.getBdUnloadAmt());
						    			 databaseBlDet.setBdBonusAmt(editBlDet.getBdBonusAmt());
						    			 databaseBlDet.setBdTotAmt(editBlDet.getBdTotAmt());
						    			 databaseBlDet.setBdDetAmt(editBlDet.getBdDetAmt());
						    			ArrayList<Map<String,Object>> editMapList=editBlDet.getBdOthChgList();
						    			ArrayList<Map<String,Object>> otherDatabaseMapList=databaseBlDet.getBdOthChgList();
						    			  for(int k=0;k<editMapList.size();k++){
						    				 Map<String,Object> editMap=(Map<String,Object>)editMapList.get(k);
						    				   if(otherDatabaseMapList.isEmpty()){
						    				    databaseBlDet.setBdOthChgList(editBlDet.getBdOthChgList());
						    				    break;
						    				   }else{
						    					    if(otherDatabaseMapList.contains(editMap)){
						    					    	;
						    					    }else{
						    					    	otherDatabaseMapList.add(editMap);
						    					       }
						    					   }
						    				   }
						    			         Iterator<Map<String,Object>> iterator=otherDatabaseMapList.iterator();
						    			         	while(iterator.hasNext()){						    			         	
			    					    		if(!editMapList.contains(iterator.next())){
			    					    			 iterator.remove();
			    					    		}
			    					       	}
						    			  }
						    			 
						    			 
						    		 }
						    	}  
						    
						  bill.setBlFinalTot(bl.getBlFinalTot());
						  bill.setBlSubTot(bl.getBlSubTot());
						  bill.setBlTaxableSerTax(bl.getBlTaxableSerTax());
						  bill.setBlSerTax(bl.getBlSerTax());
						  bill.setBlSwachBhCess(bl.getBlSwachBhCess());
						  bill.setBlKisanKalCess(bl.getBlKisanKalCess());
						  bill.setBlRemAmt(bl.getBlRemAmt());
						 						    
						  Criteria cnmtCri=session.createCriteria(Cnmt.class);
						  cnmtCri.add(Restrictions.eq(CnmtCNTS.CNMT_BILL_NO,blNo));
						    cnmtCri.setProjection(Projections.property(CnmtCNTS.CNMT_CODE));
						    cnmtCodeList=cnmtCri.list();
						   System.out.println("cnmtCodeList="+cnmtCodeList.size());
						       for(String cnmtCode:cnmtCodeList){
						    	   if(servCnmtList.contains(cnmtCode)){
						    		   ;
						    	   }else{
						    		   Criteria crit=session.createCriteria(Cnmt.class);
									List<Cnmt> cnmtList= crit.add(Restrictions.eq(CnmtCNTS.CNMT_CODE,cnmtCode)).list();
									   Cnmt cnmt=cnmtList.get(0);
									    System.out.println("cnmt="+cnmt.getCnmtCode());
									    System.out.println("cnmtblId="+cnmt.getCnmtBillDetId());
									    Iterator<BillDetail> bdIterator=billDetailList.iterator();
									    while(bdIterator.hasNext()){
									    	 BillDetail bd=bdIterator.next();
									    	 if(bd.getBdId()==cnmt.getCnmtBillDetId()){
									    		 System.out.println("billDetailId="+bd.getBdId());
									    		  bdIterator.remove();
									    		 BillDetail bDetail=(BillDetail)session.get(BillDetail.class,bd.getBdId());
									    		    session.delete(bDetail);
									    		
									    	 }
									     }
									     cnmt.setCnmtBillNo(null);
									     cnmt.setCnmtBillDetId(-1); 
									    
						    	   }
						       }
						       
						   //save bill detail..
						        for(int i=0;i<bdservList.size();i++){
						        	BillDetService bds=bdservList.get(i);
						        	BillDetail bd=bds.getBillDetail();
						              System.out.println("bdId="+bd.getBdId());
						               if(bd.getBdId()==0){
						            	   clientAddBlDetailList.add(bd);
						               }
						        	 }
						        System.out.println("size="+clientAddBlDetailList.size());
						        for(int i=0;i<clientAddBlDetailList.size();i++){
						        	BillDetail bld=clientAddBlDetailList.get(i);
						        	  bld.setbCode(bill.getbCode());
						        	  bld.setUserCode(bill.getUserCode());
						        	  bld.setBill(bill);
						        	 Integer id=(Integer)session.save(bld);
						        	 BillDetail bd=(BillDetail)session.get(BillDetail.class, id);
						        	 billDetailList.add(bd);
						           	 Cnmt cnmt=(Cnmt)session.get(Cnmt.class,bd.getBdCnmtId());
						        	   cnmt.setCnmtBillDetId(id);
						        	   cnmt.setCnmtBillNo(blNo); 
						        	}
						      
						       
						    map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
				 
		   }else{
			   map.put("csMessage","please Cs closed");
			  return map;
		   }
		return map;
		
	}
	
	@Override
	public Object modifyBillN(Session session, BillService billService) {
		Map<String,Object> resultMap = new HashMap<String,Object>();
		List<BillDetail> clientAddBlDetailList = new ArrayList<BillDetail>(); // clientSide billDetailList......
	     
		Bill bl = billService.getBill();
		Double blFinalTot = bl.getBlFinalTot();
		String blNo = bl.getBlBillNo();
		
		logger.info("blFinalTot = "+blFinalTot);
		logger.info("blNumber = "+blNo);
		
		List<BillDetService> bdservList = billService.getBdSerList();
		List<String> servCnmtList = new ArrayList<String>();   
		for(BillDetService bds : bdservList){
			servCnmtList.add(bds.getCnmt().getCnmtCode());
		}
		logger.info("servCnmtList = "+servCnmtList.size());
		
		Criteria criteria = session.createCriteria(CashStmt.class);
		criteria.add(Restrictions.eq(CashStmtCNTS.CS_TV_NO, blNo));
		List<CashStmt> csList = criteria.list();
		if(!csList.isEmpty()) {
			for(CashStmt cs : csList){
				if(cs.getCsFaCode() !=null && !cs.getCsFaCode().equals("1110200"))
					cs.setCsAmt(blFinalTot);			//cs update......
			}
			Bill bill = (Bill)session.get(Bill.class, bl.getBlId());
			List<BillDetail> billDetailList = bill.getBillDetList(); // From database
			for(int i = 0; i < billDetailList.size(); i++) {
				for(int j = 0; j < bdservList.size(); j++) {
					BillDetail databaseBlDet = billDetailList.get(i);
					BillDetail editBlDet = bdservList.get(j).getBillDetail();
					if(databaseBlDet.getBdId().equals(editBlDet.getBdId())) {
						databaseBlDet.setBdActWt(editBlDet.getBdActWt());
						databaseBlDet.setBdChgWt(editBlDet.getBdChgWt());
						databaseBlDet.setBdRecWt(editBlDet.getBdRecWt());
						databaseBlDet.setBdRate(editBlDet.getBdRate());
						databaseBlDet.setBdFreight(editBlDet.getBdFreight());
						databaseBlDet.setBdLoadAmt(editBlDet.getBdLoadAmt());
						databaseBlDet.setBdUnloadAmt(editBlDet.getBdUnloadAmt());
						databaseBlDet.setBdBonusAmt(editBlDet.getBdBonusAmt());
						databaseBlDet.setBdTotAmt(editBlDet.getBdTotAmt());
						databaseBlDet.setBdDetAmt(editBlDet.getBdDetAmt());
						databaseBlDet.setBdOthChgAmt(editBlDet.getBdOthChgAmt());
						
						ArrayList<Map<String, Object>> editMapList = editBlDet.getBdOthChgList();
						ArrayList<Map<String, Object>> otherDatabaseMapList = databaseBlDet.getBdOthChgList();
						
						for(int k = 0; k < editMapList.size(); k++){
							Map<String,Object> editMap =(Map<String,Object>)editMapList.get(k);
							if(otherDatabaseMapList.isEmpty()){
								databaseBlDet.setBdOthChgList(editBlDet.getBdOthChgList());
								break;
							} else {
								if(otherDatabaseMapList.contains(editMap)){
								}else{
									otherDatabaseMapList.add(editMap);
								}
							}
						}
						session.update(databaseBlDet);
						Iterator<Map<String,Object>> iterator=otherDatabaseMapList.iterator();
						while(iterator.hasNext()) {
							if(!editMapList.contains(iterator.next())){
								iterator.remove();
							}
						}
					}
				}
			}  
			bill.setBlFinalTot(bl.getBlFinalTot());
			bill.setBlSubTot(bl.getBlSubTot());
			bill.setBlTaxableSerTax(bl.getBlTaxableSerTax());
			bill.setBlSerTax(bl.getBlSerTax());
			bill.setBlSwachBhCess(bl.getBlSwachBhCess());
			bill.setBlKisanKalCess(bl.getBlKisanKalCess());
			bill.setBlRemAmt(bl.getBlRemAmt());

			Criteria cnmtCri = session.createCriteria(Cnmt.class);
			cnmtCri.add(Restrictions.eq(CnmtCNTS.CNMT_BILL_NO, blNo));
			cnmtCri.setProjection(Projections.property(CnmtCNTS.CNMT_CODE));
			List<String> cnmtCodeList = cnmtCri.list();
			logger.info("cnmtCodeList = "+cnmtCodeList.size());
			
			// if CNMT is deleted from Bill
			for(String cnmtCode:cnmtCodeList){
				if(servCnmtList.contains(cnmtCode)){
				}else{
					Criteria crit = session.createCriteria(Cnmt.class);
					List<Cnmt> cnmtList= crit.add(Restrictions.eq(CnmtCNTS.CNMT_CODE, cnmtCode)).list();
					Cnmt cnmt = cnmtList.get(0);
					logger.info("cnmt = "+cnmt.getCnmtCode());
					logger.info("cnmtblId = "+cnmt.getCnmtBillDetId());
					Iterator<BillDetail> bdIterator = billDetailList.iterator();
					while(bdIterator.hasNext()) {
						BillDetail bd = bdIterator.next();
						if(bd.getBdId() == cnmt.getCnmtBillDetId()) {
							logger.info("billDetailId = "+bd.getBdId());
							bdIterator.remove();
							BillDetail bDetail=(BillDetail)session.get(BillDetail.class,bd.getBdId());
							session.delete(bDetail);
						}
					}
					cnmt.setCnmtBillNo(null);
					cnmt.setCnmtBillDetId(-1); 
				}
			}
			//save new bill detail..
			for(int i=0;i<bdservList.size();i++){
				BillDetService bds=bdservList.get(i);
				BillDetail bd=bds.getBillDetail();
				logger.info("bdId="+bd.getBdId());
				if(bd.getBdId()== null || bd.getBdId() == 0){
					clientAddBlDetailList.add(bd);
				}
			}
			logger.info("size="+clientAddBlDetailList.size());
			for(int i=0;i<clientAddBlDetailList.size();i++){
				BillDetail bld=clientAddBlDetailList.get(i);
				bld.setbCode(bill.getbCode());
				bld.setUserCode(bill.getUserCode());
				bld.setBill(bill);
				Integer id=(Integer)session.save(bld);
				BillDetail bd=(BillDetail)session.get(BillDetail.class, id);
				billDetailList.add(bd);
				Cnmt cnmt=(Cnmt)session.get(Cnmt.class,bd.getBdCnmtId());
				cnmt.setCnmtBillDetId(id);
				cnmt.setCnmtBillNo(blNo); 
			}
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", "Cashstmt not found for this bill("+blNo+")");
		}
		return resultMap;
	}
	
	@Override
	public Object modifyBillGST(Session session, BillService billService) {
		Map<String,Object> resultMap = new HashMap<String,Object>();
		List<BillDetail> clientAddBlDetailList = new ArrayList<BillDetail>(); // clientSide billDetailList......
	     
		Bill bl = billService.getBill();
		Double blFinalTot = bl.getBlFinalTot();
		String blNo = bl.getBlBillNo();
		
		logger.info("blFinalTot = "+blFinalTot);
		logger.info("blNumber = "+blNo);
		
		List<BillDetService> bdservList = billService.getBdSerList();
		List<String> servCnmtList = new ArrayList<String>();   
		for(BillDetService bds : bdservList){
			servCnmtList.add(bds.getCnmt().getCnmtCode());
		}
		logger.info("servCnmtList = "+servCnmtList.size());
		
		Criteria criteria = session.createCriteria(CashStmt.class);
		criteria.add(Restrictions.eq(CashStmtCNTS.CS_TV_NO, blNo));
		List<CashStmt> csList = criteria.list();
		if(!csList.isEmpty()) {
			for(CashStmt cs : csList){
				if(cs.getCsFaCode() !=null && !cs.getCsFaCode().equals("1110200"))
					cs.setCsAmt(blFinalTot);			//cs update......
			}
			Bill bill = (Bill)session.get(Bill.class, bl.getBlId());
			List<BillDetail> billDetailList = bill.getBillDetList(); // From database
			for(int i = 0; i < billDetailList.size(); i++) {
				for(int j = 0; j < bdservList.size(); j++) {
					BillDetail databaseBlDet = billDetailList.get(i);
					BillDetail editBlDet = bdservList.get(j).getBillDetail();
					if(databaseBlDet.getBdId().equals(editBlDet.getBdId())) {
						databaseBlDet.setBdActWt(editBlDet.getBdActWt());
						databaseBlDet.setBdChgWt(editBlDet.getBdChgWt());
						databaseBlDet.setBdRecWt(editBlDet.getBdRecWt());
						databaseBlDet.setBdRate(editBlDet.getBdRate());
						databaseBlDet.setBdFreight(editBlDet.getBdFreight());
						databaseBlDet.setBdLoadAmt(editBlDet.getBdLoadAmt());
						databaseBlDet.setBdUnloadAmt(editBlDet.getBdUnloadAmt());
						databaseBlDet.setBdBonusAmt(editBlDet.getBdBonusAmt());
						databaseBlDet.setBdTotAmt(editBlDet.getBdTotAmt());
						databaseBlDet.setBdDetAmt(editBlDet.getBdDetAmt());
						databaseBlDet.setBdOthChgAmt(editBlDet.getBdOthChgAmt());
						databaseBlDet.setBdCnmtAdv(editBlDet.getBdCnmtAdv());
						databaseBlDet.setBdTollTax(editBlDet.getBdTollTax());
						
						ArrayList<Map<String, Object>> editMapList = editBlDet.getBdOthChgList();
						ArrayList<Map<String, Object>> otherDatabaseMapList = databaseBlDet.getBdOthChgList();
						
						for(int k = 0; k < editMapList.size(); k++){
							Map<String,Object> editMap =(Map<String,Object>)editMapList.get(k);
							if(otherDatabaseMapList.isEmpty()){
								databaseBlDet.setBdOthChgList(editBlDet.getBdOthChgList());
								break;
							} else {
								if(otherDatabaseMapList.contains(editMap)){
								}else{
									otherDatabaseMapList.add(editMap);
								}
							}
						}
						Iterator<Map<String,Object>> iterator=otherDatabaseMapList.iterator();
						while(iterator.hasNext()) {
							if(!editMapList.contains(iterator.next())){
								iterator.remove();
							}
						}
					}
				}
			}  
			bill.setBlFinalTot(bl.getBlFinalTot());
			bill.setBlSubTot(bl.getBlSubTot());
			//bill.setBlTaxableSerTax(bl.getBlTaxableSerTax());
			//bill.setBlSerTax(bl.getBlSerTax());
			//bill.setBlSwachBhCess(bl.getBlSwachBhCess());
			//bill.setBlKisanKalCess(bl.getBlKisanKalCess());
			bill.setBlRemAmt(bl.getBlRemAmt());
			bill.setBlStateGST(bl.getBlStateGST());
			bill.setBlGstNo(bl.getBlGstNo());
			bill.setBlSgst(bl.getBlSgst());
			bill.setBlCgst(bl.getBlCgst());
			bill.setBlIgst(bl.getBlIgst());

			Criteria cnmtCri = session.createCriteria(Cnmt.class);
			cnmtCri.add(Restrictions.eq(CnmtCNTS.CNMT_BILL_NO, blNo));
			cnmtCri.setProjection(Projections.property(CnmtCNTS.CNMT_CODE));
			List<String> cnmtCodeList = cnmtCri.list();
			logger.info("cnmtCodeList = "+cnmtCodeList.size());
			
			// if CNMT is deleted from Bill
			for(String cnmtCode:cnmtCodeList){
				if(servCnmtList.contains(cnmtCode)){
				}else{
					Criteria crit = session.createCriteria(Cnmt.class);
					List<Cnmt> cnmtList= crit.add(Restrictions.eq(CnmtCNTS.CNMT_CODE, cnmtCode)).list();
					Cnmt cnmt = cnmtList.get(0);
					logger.info("cnmt = "+cnmt.getCnmtCode());
					logger.info("cnmtblId = "+cnmt.getCnmtBillDetId());
					Iterator<BillDetail> bdIterator = billDetailList.iterator();
					while(bdIterator.hasNext()) {
						BillDetail bd = bdIterator.next();
						if(bd.getBdId() == cnmt.getCnmtBillDetId()) {
							logger.info("billDetailId = "+bd.getBdId());
							bdIterator.remove();
							BillDetail bDetail=(BillDetail)session.get(BillDetail.class,bd.getBdId());
							session.delete(bDetail);
						}
					}
					cnmt.setCnmtBillNo(null);
					cnmt.setCnmtBillDetId(-1);
										
				}
			}
			//save new bill detail..
			for(int i=0;i<bdservList.size();i++){
				BillDetService bds=bdservList.get(i);
				BillDetail bd=bds.getBillDetail();
				logger.info("bdId="+bd.getBdId());
				if(bd.getBdId()== null || bd.getBdId() == 0){
					clientAddBlDetailList.add(bd);
				}
			}
			logger.info("size="+clientAddBlDetailList.size());
			for(int i=0;i<clientAddBlDetailList.size();i++){
				BillDetail bld=clientAddBlDetailList.get(i);
				bld.setbCode(bill.getbCode());
				bld.setUserCode(bill.getUserCode());
				bld.setBill(bill);
				Integer id=(Integer)session.save(bld);
				BillDetail bd=(BillDetail)session.get(BillDetail.class, id);
				billDetailList.add(bd);
				Cnmt cnmt=(Cnmt)session.get(Cnmt.class,bd.getBdCnmtId());
				cnmt.setCnmtBillDetId(id);
				cnmt.setCnmtBillNo(blNo); 
			}
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			//TODO
			Criteria cr = session.createCriteria(CashStmtTemp.class)
			.add(Restrictions.eq(CashStmtCNTS.CS_TV_NO, blNo));
			List<CashStmtTemp> csTempList = cr.list();
			
			if(!csTempList.isEmpty()) {
				for(CashStmtTemp cs : csTempList){
					if(cs.getCsFaCode() !=null && !cs.getCsFaCode().equals("1110200"))
						cs.setCsAmt(blFinalTot);			//cs update......
				}
				Bill bill = (Bill)session.get(Bill.class, bl.getBlId());
				List<BillDetail> billDetailList = bill.getBillDetList(); // From database
				for(int i = 0; i < billDetailList.size(); i++) {
					for(int j = 0; j < bdservList.size(); j++) {
						BillDetail databaseBlDet = billDetailList.get(i);
						BillDetail editBlDet = bdservList.get(j).getBillDetail();
						if(databaseBlDet.getBdId().equals(editBlDet.getBdId())) {
							databaseBlDet.setBdActWt(editBlDet.getBdActWt());
							databaseBlDet.setBdChgWt(editBlDet.getBdChgWt());
							databaseBlDet.setBdRecWt(editBlDet.getBdRecWt());
							databaseBlDet.setBdRate(editBlDet.getBdRate());
							databaseBlDet.setBdFreight(editBlDet.getBdFreight());
							databaseBlDet.setBdLoadAmt(editBlDet.getBdLoadAmt());
							databaseBlDet.setBdUnloadAmt(editBlDet.getBdUnloadAmt());
							databaseBlDet.setBdBonusAmt(editBlDet.getBdBonusAmt());
							databaseBlDet.setBdTotAmt(editBlDet.getBdTotAmt());
							databaseBlDet.setBdDetAmt(editBlDet.getBdDetAmt());
							databaseBlDet.setBdOthChgAmt(editBlDet.getBdOthChgAmt());
							databaseBlDet.setBdCnmtAdv(editBlDet.getBdCnmtAdv());
							databaseBlDet.setBdTollTax(editBlDet.getBdTollTax());
							
							ArrayList<Map<String, Object>> editMapList = editBlDet.getBdOthChgList();
							ArrayList<Map<String, Object>> otherDatabaseMapList = databaseBlDet.getBdOthChgList();
							
							for(int k = 0; k < editMapList.size(); k++){
								Map<String,Object> editMap =(Map<String,Object>)editMapList.get(k);
								if(otherDatabaseMapList.isEmpty()){
									databaseBlDet.setBdOthChgList(editBlDet.getBdOthChgList());
									break;
								} else {
									if(otherDatabaseMapList.contains(editMap)){
									}else{
										otherDatabaseMapList.add(editMap);
									}
								}
							}
							Iterator<Map<String,Object>> iterator=otherDatabaseMapList.iterator();
							while(iterator.hasNext()) {
								if(!editMapList.contains(iterator.next())){
									iterator.remove();
								}
							}
						}
					}
				}  
				bill.setBlFinalTot(bl.getBlFinalTot());
				bill.setBlSubTot(bl.getBlSubTot());
				//bill.setBlTaxableSerTax(bl.getBlTaxableSerTax());
				//bill.setBlSerTax(bl.getBlSerTax());
				//bill.setBlSwachBhCess(bl.getBlSwachBhCess());
				//bill.setBlKisanKalCess(bl.getBlKisanKalCess());
				bill.setBlRemAmt(bl.getBlRemAmt());
				bill.setBlStateGST(bl.getBlStateGST());
				bill.setBlGstNo(bl.getBlGstNo());
				bill.setBlSgst(bl.getBlSgst());
				bill.setBlCgst(bl.getBlCgst());
				bill.setBlIgst(bl.getBlIgst());

				Criteria cnmtCri = session.createCriteria(Cnmt.class);
				cnmtCri.add(Restrictions.eq(CnmtCNTS.CNMT_BILL_NO, blNo));
				cnmtCri.setProjection(Projections.property(CnmtCNTS.CNMT_CODE));
				List<String> cnmtCodeList = cnmtCri.list();
				logger.info("cnmtCodeList = "+cnmtCodeList.size());
				
				// if CNMT is deleted from Bill
				for(String cnmtCode:cnmtCodeList){
					if(servCnmtList.contains(cnmtCode)){
					}else{
						Criteria crit = session.createCriteria(Cnmt.class);
						List<Cnmt> cnmtList= crit.add(Restrictions.eq(CnmtCNTS.CNMT_CODE, cnmtCode)).list();
						Cnmt cnmt = cnmtList.get(0);
						logger.info("cnmt = "+cnmt.getCnmtCode());
						logger.info("cnmtblId = "+cnmt.getCnmtBillDetId());
						Iterator<BillDetail> bdIterator = billDetailList.iterator();
						while(bdIterator.hasNext()) {
							BillDetail bd = bdIterator.next();
							if(bd.getBdId() == cnmt.getCnmtBillDetId()) {
								logger.info("billDetailId = "+bd.getBdId());
								bdIterator.remove();
								BillDetail bDetail=(BillDetail)session.get(BillDetail.class,bd.getBdId());
								session.delete(bDetail);
							}
						}
						cnmt.setCnmtBillNo(null);
						cnmt.setCnmtBillDetId(-1);
											
					}
				}
				//save new bill detail..
				for(int i=0;i<bdservList.size();i++){
					BillDetService bds=bdservList.get(i);
					BillDetail bd=bds.getBillDetail();
					logger.info("bdId="+bd.getBdId());
					if(bd.getBdId()== null || bd.getBdId() == 0){
						clientAddBlDetailList.add(bd);
					}
				}
				logger.info("size="+clientAddBlDetailList.size());
				for(int i=0;i<clientAddBlDetailList.size();i++){
					BillDetail bld=clientAddBlDetailList.get(i);
					bld.setbCode(bill.getbCode());
					bld.setUserCode(bill.getUserCode());
					bld.setBill(bill);
					Integer id=(Integer)session.save(bld);
					BillDetail bd=(BillDetail)session.get(BillDetail.class, id);
					billDetailList.add(bd);
					Cnmt cnmt=(Cnmt)session.get(Cnmt.class,bd.getBdCnmtId());
					cnmt.setCnmtBillDetId(id);
					cnmt.setCnmtBillNo(blNo); 
				}
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			
				
			}else {
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				resultMap.put("msg", "Cashstmt not found for this bill("+blNo+")");
			}
			
		}
		return resultMap;
	}
}
