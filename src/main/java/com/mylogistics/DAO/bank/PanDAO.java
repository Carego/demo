package com.mylogistics.DAO.bank;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

@Repository
public interface PanDAO {

	public List<Map<String, Object>> getChdOwnBrkValidList(String branchCode, String dateFrom, String dateTo,String panHldr);
	
	public int validatedPanNo(Map<String, Object> panValidatedMap);
	
}
