package com.mylogistics.DAOImpl.bank;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.itextpdf.text.log.SysoCounter;
import com.mylogistics.DAO.bank.PanDAO;
import com.mylogistics.constants.BrokerCNTS;
import com.mylogistics.constants.ChallanCNTS;
import com.mylogistics.constants.ChallanDetailCNTS;
import com.mylogistics.constants.OwnerCNTS;
import com.mylogistics.constants.bank.BankMstrCNTS;
import com.mylogistics.controller.ChqCancelVoucherCntlr;
import com.mylogistics.model.Broker;
import com.mylogistics.model.Challan;
import com.mylogistics.model.ChallanDetail;
import com.mylogistics.model.Owner;
import com.mylogistics.model.bank.BankMstr;
import com.mylogistics.services.Chln_ChlnDetService;
import com.mylogistics.services.CodePatternService;
import com.sun.org.apache.xalan.internal.xsltc.compiler.sym;

public class PanDAOImpl implements PanDAO{

	private Session session;
	private SessionFactory sessionFactory;
	private Transaction transaction;
	
	private static Logger logger = Logger.getLogger(PanDAOImpl.class);
	
	@Autowired
	public PanDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public List<Map<String, Object>> getChdOwnBrkValidList(String branchCode, String dateFrom, String dateTo,String panHldr) {
		
		logger.info("Enter into getChdOwnBrkValidList() : BranchCode = "+branchCode+" : DateFrom = "+dateFrom+" : DateTo = "+dateTo);
		java.util.Date dtFr = null;
		java.util.Date dtTo = null;
		Date from = null;
		Date to = null;
		
		List<ChallanDetail> myList = new ArrayList<>();	
		List<Map<String, Object>> finalList = new ArrayList<Map<String, Object>>();
		
		SimpleDateFormat dtFormat = new SimpleDateFormat("yyyy-MM-dd");
		try{		
			// Converting date to java.sql.Date			
			dtFr = dtFormat.parse(dateFrom);			
			from = new Date(dtFr.getTime());			
			if(dateTo != null && !dateTo.equalsIgnoreCase("")){				
				dtTo = dtFormat.parse(dateTo);				
				to = new Date(dtTo.getTime());				
			}
		}catch(Exception e){
			logger.error("Exception : "+e);
		}	
		
		try {			
			session = this.sessionFactory.openSession();		
			
			String query = "SELECT chd,chln.chlnDt FROM ChallanDetail chd, Challan chln " +
					"WHERE chd.chdChlnCode = chln.chlnCode AND " +
					"chd.bCode = :bCode AND " /*+					
					"chd.chdValidPan = :chdValidPan AND "*/;			
			if(to != null)
				query = query +"chln.chlnDt >= :from AND chln.chlnDt <= :to ORDER BY chln.chlnDt ASC";
			else
				query = query +"chln.chlnDt >= :from ORDER BY chln.chlnDt ASC";
			
			System.err.println("Query = "+query);
			
			Query chdQuery = session.createQuery(query);
			chdQuery.setString("bCode", branchCode);
			//chdQuery.setBoolean("chdValidPan", false);	
			if(to != null){
				chdQuery.setDate("from", from);
				chdQuery.setDate("to", to);
			}else
				chdQuery.setDate("from", from);
			
			myList = chdQuery.list();
			if(! myList.isEmpty()){					
				logger.info("ChallanDetail found list size = "+myList.size());			
				List<ChallanDetail> chdList = new ArrayList<ChallanDetail>();
				List<Date> chlnDtList = new ArrayList<Date>();
				Set<String> ownCodeList = new HashSet<String>();
				Set<String> brkCodeList = new HashSet<String>();
			
				Iterator chlnChdIterator = myList.iterator();
				while(chlnChdIterator.hasNext()){
					Object obj[] =  (Object[]) chlnChdIterator.next();				
					ChallanDetail chlnDet = (ChallanDetail) obj[0];
					Date chlnDt = (Date) obj[1];
					chdList.add(chlnDet);
					chlnDtList.add(chlnDt);
					ownCodeList.add(chlnDet.getChdOwnCode());
					brkCodeList.add(chlnDet.getChdBrCode());					
				}			
				List<Owner> ownList = session.createCriteria(Owner.class)
						.add(Restrictions.in(OwnerCNTS.OWN_CODE, ownCodeList))					
						.list();				
				List<Broker> brkList = session.createCriteria(Broker.class)
						.add(Restrictions.in(BrokerCNTS.BRK_CODE, brkCodeList))
						.list();				
				for(int i=0; i<chdList.size(); i++){
					ChallanDetail chlnDet = (ChallanDetail) chdList.get(i);
					Owner owner = (Owner) getObjectFromList(ownList, chlnDet.getChdOwnCode());
					Broker broker = (Broker) getObjectFromList(brkList, chlnDet.getChdBrCode());
					chlnDet.setChdChlnDt(chlnDtList.get(i));
					Map finalMap = createMap(chlnDet, owner, broker,panHldr);
					if(finalMap != null)
						finalList.add(finalMap);
				}
			}else{
				logger.info("No such record found !");
			}			
			session.flush();			
		} catch (Exception e) {
			logger.error("Exception : "+e);
			e.printStackTrace();
		}		
		session.clear();
		session.close();
		logger.info("Final ChallanDetail list size = "+finalList.size());
		return finalList;
	}
	public Map createMap(ChallanDetail chlnDet, Owner owner, Broker broker,String panHldr){		
		/*if(owner.isOwnValidPan() || broker.isBrkValidPan()){			
			return null;
		}*/
		if(panHldr.equalsIgnoreCase("Owner") && owner.isOwnValidPan()){
			return null;
		}else if(panHldr.equalsIgnoreCase("Broker") && broker.isBrkValidPan()){
			return null;
		}
		
		Map<String, Object> map = new HashMap<String, Object>();			
		if(panHldr.equalsIgnoreCase("Owner")){
			map.put("id", owner.getOwnId());
			map.put("code", owner.getOwnCode());				
			map.put("name", owner.getOwnName());
			map.put("panNo", owner.getOwnPanNo());
			map.put("imgId", owner.getOwnImgId());
			
			map.put("isDecImg", owner.isOwnIsDecImg());
			map.put("isPanImg", owner.isOwnIsPanImg());		
			map.put("isValidPan", owner.isOwnValidPan());
			map.put("isInvalidPan", owner.isOwnInvalidPan());
			
			map.put("panHolder", panHldr);
		}else {
			map.put("id", broker.getBrkId());
			map.put("code", broker.getBrkCode());
			map.put("name", broker.getBrkName());
			map.put("panNo", broker.getBrkPanNo());
			map.put("imgId", broker.getBrkImgId());
			
			map.put("isDecImg", broker.isBrkIsDecImg());
			map.put("isPanImg", broker.isBrkIsPanImg());		
			map.put("isValidPan", broker.isBrkValidPan());
			map.put("isInvalidPan", broker.isBrkInvalidPan());			
			
			map.put("panHolder", panHldr);
		}
		map.put("chdId", chlnDet.getChdId());
		map.put("brnchCode",chlnDet.getbCode());
		map.put("chdChlnCode", chlnDet.getChdChlnCode());
		map.put("chdChlnDt", chlnDet.getChdChlnDt());
		map.put("chdOwnCode", chlnDet.getChdOwnCode());
		map.put("chdBrCode", chlnDet.getChdBrCode());
		map.put("chdRcNo", chlnDet.getChdRcNo());
		return map;
	}
	public Object getObjectFromList(List list, String ownBrkCode){
		Object obj = null;
		if(ownBrkCode.contains("own")){
			for(int i=0; i<list.size(); i++){
				Owner owner = (Owner)list.get(i);
				if(ownBrkCode.equalsIgnoreCase(owner.getOwnCode())){
					obj = owner;
					break;
				}					
			}
		}else if(ownBrkCode.contains("brk")){
			for(int i=0; i<list.size(); i++){
				Broker broker = (Broker)list.get(i);
				if(ownBrkCode.equalsIgnoreCase(broker.getBrkCode())){
					obj = broker;
					break;
				}					
			}
		}
		return obj;
	}

	@Override
	public int validatedPanNo(Map<String, Object> panValidatedMap) {
		System.out.println("PanDAOImpl.validatedPanNo()");
		int temp = 0;
		Session session= this.sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		try {
			
			if (String.valueOf(panValidatedMap.get("panHolder")).equalsIgnoreCase("owner")) {
				//update challan detail
				Criteria cr=session.createCriteria(ChallanDetail.class);
				cr.add(Restrictions.eq(ChallanDetailCNTS.CHD_OWN_CODE, String.valueOf(panValidatedMap.get("code"))));
				cr.add(Restrictions.eq("chdPanHdrType", "Owner"));
				List<ChallanDetail> chlnDetList = cr.list();
				
				for (ChallanDetail challanDetail : chlnDetList) {
					challanDetail.setChdValidPan((boolean) panValidatedMap.get("isValidPan"));
					challanDetail.setChdInvalidPan((boolean) panValidatedMap.get("isInvalidPan"));
					session.update(challanDetail);
				}
				
				//update owner
				Owner owner = (Owner) session.get(Owner.class, Integer.parseInt(String.valueOf(panValidatedMap.get("id")))); 
				
				owner.setOwnValidPan((boolean) panValidatedMap.get("isValidPan"));
				owner.setOwnInvalidPan((boolean) panValidatedMap.get("isInvalidPan"));
				owner.setOwnPanNo(String.valueOf(panValidatedMap.get("panNo")));
				session.update(owner);
				
			} else {
				//update challan detail
				Criteria cr=session.createCriteria(ChallanDetail.class);
				cr.add(Restrictions.eq(ChallanDetailCNTS.CHD_BR_CODE, String.valueOf(panValidatedMap.get("code"))));
				cr.add(Restrictions.eq("chdPanHdrType", "Broker"));
				List<ChallanDetail> chlnDetList = cr.list();
				
				for (ChallanDetail challanDetail : chlnDetList) {
					challanDetail.setChdValidPan((boolean) panValidatedMap.get("isValidPan"));
					challanDetail.setChdInvalidPan((boolean) panValidatedMap.get("isInvalidPan"));
					session.update(challanDetail);
				}
				
				//update broker
				Broker broker = (Broker) session.get(Broker.class, Integer.parseInt(String.valueOf(panValidatedMap.get("id"))));
				
				broker.setBrkValidPan((boolean) panValidatedMap.get("isValidPan"));
				broker.setBrkInvalidPan((boolean) panValidatedMap.get("isInvalidPan"));
				broker.setBrkPanNo(String.valueOf(panValidatedMap.get("panNo")));
				session.update(broker);
			}
			session.flush();
			session.clear();
			transaction.commit();
			
			temp = 1;
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();
			temp = -1;
		}finally {
			session.close();
		}
		
		return temp;
	}
	
	
	
}
