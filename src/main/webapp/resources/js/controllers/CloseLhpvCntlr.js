'use strict';

var app = angular.module('application');

app.controller('CloseLhpvCntlr',['$scope','$location','$http','$filter','$window','$log',
                                  function($scope,$location,$http,$filter,$window,$log){
	$scope.vs = {};
	
	$scope.printLhpvFlag = true;
	
	$scope.getLhpvDet = function(){
		console.log("enter into getLhpvDet function");
		var response = $http.post($scope.projectName+'/getLhpvCloseDet');
		  response.success(function(data, status, headers, config){
			   if(data.result === "success"){
				   $scope.vs.branch = data.branch;
				   $scope.vs.lhpvStatus = data.lhpvStatus;
				   $scope.vs.voucherType = $scope.LHPV_CLOSE_VOUCHER;
				   $scope.dateTemp = $filter('date')(data.lhpvStatus.lsDt, "yyyy-MM-dd'");
				   
				   if($scope.vs.lhpvStatus.lsClose === false){
					   $scope.getLhpvInfo();
					   $('#saveId').removeAttr("disabled");
				   }else{
					   $('#saveId').attr("disabled","disabled");
					   $scope.alertToast("You already close the LHPV of "+$scope.dateTemp);
				   }
			   }else{
				   console.log("error in fetching getLhpvDet data");
				   $scope.alertToast("Server Error");
			   }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
		   });
	}
	
	
	$scope.getLhpvInfo = function(){
		console.log("enter into getLhpvInfo function");
		var response = $http.post($scope.projectName+'/getLhpvInfo',$scope.vs.lhpvStatus);
		  response.success(function(data, status, headers, config){
			  $log.info(data);
			   if(data.result === "success"){
				   $scope.lhpvAdvList = data.lhpvAdvList;
				   $scope.lhpvBalList = data.lhpvBalList;
				   $scope.lhpvSupList = data.lhpvSupList;
				   $scope.totAdv = 0;
				   $scope.totBal = 0;
				   $scope.totSup = 0;
				   if($scope.lhpvAdvList.length > 0){
					   for(var i=0;i<$scope.lhpvAdvList.length;i++){
						   $scope.totAdv = $scope.totAdv + $scope.lhpvAdvList[i].laFinalTot; 
					   }
				   }
				   if($scope.lhpvBalList.length > 0){
					   for(var i=0;i<$scope.lhpvBalList.length;i++){
						   $scope.totBal = $scope.totBal + $scope.lhpvBalList[i].lbFinalTot; 
					   }
				   }
				   if($scope.lhpvSupList.length > 0){
					   for(var i=0;i<$scope.lhpvSupList.length;i++){
						   $scope.totSup = $scope.totSup + $scope.lhpvSupList[i].lspFinalTot; 
					   }
				   }
				   $scope.chlnAdvList = data.chlnAdvList;
				   $scope.chlnBalList = data.chlnBalList;
				   $scope.chlnSupList = data.chlnSupList;
				   
				   $scope.lhpvAdv = data.single;
			   }else{
				   $scope.alertToast("There is no LHPV Transaction for "+$scope.dateTemp);
			   }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
		   });
	}
	
	
	$scope.voucherSubmit = function(voucherForm){
		console.log("enter into voucherSubmit funciton = "+voucherForm.$invalid);
		if(voucherForm.$invalid){
			$scope.alertToast(" Error ");
		}else{
			$('#saveId').attr("disabled","disabled");
			
			var response = $http.post($scope.projectName+'/closeLhpv',$scope.vs.lhpvStatus);
			  response.success(function(data, status, headers, config){
				   if(data.result === "success"){
					   $scope.alertToast("Successfully Close the Lhpv of "+$scope.dateTemp);
					   
					   $scope.printLhpvFlag = false;
				    	$('div#printLhpvDB').dialog({
							autoOpen: false,
							modal:true,
							resizable: false,
							show: UDShow,
							hide: UDHide,
							position: UDPos,
							draggable: true,
							close: function(event, ui) { 
						        $(this).dialog('destroy');
						        $(this).hide();
						        $scope.printLhpvFlag = true;
						    }
							});
						$('div#printLhpvDB').dialog('open');
						
				   }else{
					   $scope.alertToast("Server Error");
				   }
			   });
			   response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
			   });
		}
	}
	
	
	
	$scope.cancelPrint = function(){
		console.log("enter into cancelPrint function");
		$('div#printLhpvDB').dialog('close');
	}
	
	$scope.printVs = function(){
		console.log("enter into printVs funciton");
		$window.location.href = $scope.projectName+'/printLhpv';
	}
	
	
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		$('#saveId').attr("disabled","disabled"); 
		$scope.getLhpvDet();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 } 
}]);