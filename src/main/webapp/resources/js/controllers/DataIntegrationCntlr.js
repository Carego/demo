'use strict';

var app = angular.module('application');

app.controller('DataIntegrationCntlr',['$scope','$location','$http','$filter','$q',
                                 function($scope,$location,$http,$filter,$q){
	
	console.log("DataIntegrationCntlr started");
	
	$scope.ownOpenFlag=true;
	$scope.brkOpenFlag=true;
	
	
	$scope.getOwnerView = function() {
		console.log("Enter into getCnmtView");
		var response = $http.post($scope.projectName+'/getOwnerView');
		response.success(function(data, status, headers, config){
			console.log("getCnmtView--> "+data.result);
			if (data.result === "success") {
				console.log(data.result);
				$scope.alertToast(data.result);
				$scope.ownerList=data.list;
				$scope.ownOpenFlag=false;
			} else {
				console.log(data.result);
				$scope.alertToast(data.result);
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}
	
	
	$scope.getBrokerView = function() {
		console.log("Enter into getBrokerView");
		var response = $http.post($scope.projectName+'/getBrokerView');
		response.success(function(data, status, headers, config){
			console.log("getBrokerView--> "+data.result);
			if (data.result === "success") {
				console.log(data.result);
				$scope.alertToast(data.result);
				$scope.brokerList=data.list;
				$scope.brkOpenFlag=false;
			} else {
				console.log(data.result);
				$scope.alertToast(data.result);
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}
	
	$scope.downloadOwn = function(){
		console.log("enter into downloadOwn function");
		var blob = new Blob([document.getElementById('exportable').innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
        saveAs(blob, "Owner.xls");
	};
	
	
	$scope.downloadBrk = function(){
		console.log("enter into downloadOwn function");
		var blob = new Blob([document.getElementById('exportableB').innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
        saveAs(blob, "Broker.xls");
	};
	
	

	$scope.stateDBFlag=true;
	$scope.distDBFlag=true;
	$scope.cityDBFlag=true;
	$scope.stnDBFlag=true;
	
	
	$('#pinCodeId').keypress(function(key) {
		if(key.charCode < 48 || key.charCode > 57)
			return false;
	});
	
	$scope.getState=function(){
		console.log("getState()");
		 var response= $http.post($scope.projectName+'/getStateDetails');
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					console.log(data.list);
					$scope.stateList=data.list;
					$scope.OpenStateDB();
				}else{
					$scope.alertToast("state not found in database");
				}
			});
			response.error(function(data, status, headers, config) {
				$scope.alertToast("There is some problem in fetching state");
			});
		
	}
	
	
	
	$scope.OpenStateDB = function(){
		$scope.stateDBFlag=false;
		$('div#stateDB').dialog({
			autoOpen: false,
			modal:true,
			title: "State",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#stateDB').dialog('open');
	}

	$scope.saveStateCode = function(state){
		$scope.stateCode = state.stateCode;
		$scope.stateName = state.stateName;
		$scope.stateGST=state.stateGST;
		$('div#stateDB').dialog('close');
		$scope.stateDBFlag=true;
		
		
		 var response= $http.post($scope.projectName+'/getDistByStateCode',state.stateCode);
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					console.log(data.list);
					$scope.distList=data.list;
					$scope.OpenDistDB();
				}else{
					$scope.alertToast("District not found in database");
				}
			});
			response.error(function(data, status, headers, config) {
				$scope.alertToast("There is some problem in fetching District");
			});
		
		
	}
	
	
	$scope.OpenDistDB = function(){
		$scope.distDBFlag=false;
		$('div#distDB').dialog({
			autoOpen: false,
			modal:true,
			title: "District",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#distDB').dialog('open');
	}
	
	
	
	
	$scope.saveDist = function(dist){
		$scope.distName = dist;
		$('div#distDB').dialog('close');
		$scope.distDBFlag=true;
		var map={
				"dist":$scope.distName,
				"state":$scope.stateCode
		};
		
		 var response= $http.post($scope.projectName+'/getCityByDistName',map);
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					console.log(data.list);
					$scope.cityList=data.list;
					$scope.OpenCityDB();
				}else{
					$scope.alertToast("city not found in database");
				}
			});
			response.error(function(data, status, headers, config) {
				$scope.alertToast("There is some problem in fetching city");
			});
		
		
	}
	
	
	
	$scope.OpenCityDB = function(){
		$scope.cityDBFlag=false;
		$('div#cityDB').dialog({
			autoOpen: false,
			modal:true,
			title: "City",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#cityDB').dialog('open');
	}
	
	
	$scope.saveCity = function(city){
		$scope.cityName = city;
		$('div#cityDB').dialog('close');
		$scope.cityDBFlag=true;
		var map={
				"city":$scope.cityName,
				"dist":$scope.distName,
				"state":$scope.stateCode
		};
		
		 var response= $http.post($scope.projectName+'/getStnByCityDistName',map);
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					console.log(data.list);
					$scope.stnList=data.list;
					$scope.OpenStnDB();
				}else{
					$scope.alertToast("station not found in database");
				}
			});
			response.error(function(data, status, headers, config) {
				$scope.alertToast("There is some problem in fetching station");
			});
		
		
	}
	
	
	$scope.OpenStnDB = function(){
		$scope.stnDBFlag=false;
		$('div#stnDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Station",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#stnDB').dialog('open');
	}
	
	
	
	
	$scope.saveStn = function(stn){
		$scope.stateCode=stn.stateCode;
		$scope.stnName = stn.stnName;
		$scope.stnCode = stn.stnCode;
		$scope.distName=stn.stnDistrict;
		$scope.cityName=stn.stnCity;
		$scope.pinCode=stn.stnPin;
		$('div#stnDB').dialog('close');
		console.log("Station"+stn.stnName);
		$scope.stnDBFlag=true;
	}
	
	
	
	
	$scope.getStnByPin=function(){
		
		if($scope.pinCode.length<6)
			return;
		
		var response= $http.post($scope.projectName+'/getStnByPin',$scope.pinCode);
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				console.log(data.list +""+data.stateName);
				$scope.stnList=data.list;
				$scope.stateName=data.stateName;
				$scope.stateGST=data.stateGST;
				console.log("$scope.stateGST"+$scope.stateGST);
				$scope.OpenStnDB();
			}else{
				$scope.alertToast("station not found in database");
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.alertToast("There is some problem in fetching station");
		});
		
		
	}
	

	console.log("DataIntegrationCntlr ended");
	
}]);