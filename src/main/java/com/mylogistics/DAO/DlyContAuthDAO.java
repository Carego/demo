package com.mylogistics.DAO;

import java.util.List;

import com.mylogistics.model.DlyContAuth;

public interface DlyContAuthDAO {

	public int saveDCA(DlyContAuth dlyContAuth);
	
	public List<DlyContAuth> getDCAbyCustCode(String custCode);
	
	public List<String> getDCACustCode();
	
	public int againAlwFrCont(String custCode);
}
