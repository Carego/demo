package com.mylogistics.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.CustomerDAO;
import com.mylogistics.model.Customer;
import com.mylogistics.services.CustomerService;

@Service
public class CustomerServiceImpl implements CustomerService {
	
	@Autowired
	private CustomerDAO customerDAO;
	
	@Transactional
	public Customer getCustomer(Integer custId) {
		return customerDAO.getCustomer(custId);
	}

}