<div ng-show="operatorLogin || superAdminLogin">
	<div class="row">
		<form name="pdmrForm" ng-submit=pdmrSubmit(pdmrForm) class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
			
					<div class="col s4 input-field">
		       				<input class="validate" type ="date" id="csDtId" name ="csDtName" ng-model="csDt" ng-blur="chngDt()" readonly ng-required="true" >
		       			<label for="code">CS Date</label>	
		       		</div>
		       		
					<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="custId" name ="custName" ng-model="customer.custName" ng-click="selectCust()" ng-required="true" readonly>
		       			<label for="code">Customer Name</label>	
		       		</div>
					
					<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="pdMRId" name ="pdMRName" ng-model="selOAMR.mrNo" ng-click="selectPDMR()" ng-required="true" readonly>
		       			<label for="code">Pending MR No.</label>	
		       		</div>
		     		
		    </div>
		    
		    <div class="row">
		    	<div class="input-field col s12">
				 <div ng-show="pdmr.mrFrPDList.length > 0">
					<table class="tblrow">
					<caption class="coltag tblrow">Payment Detail Of ON Accounting MR</caption>
						<tr class="rowclr">
							<th class="colclr">OA MR NO.</th>
							<th class="colclr">Detail Amt</th>
							<th class="colclr">Action</th>
						</tr>
						<tr class="tbl" ng-repeat="item in pdmr.mrFrPDList">
							<td class="rowcel">{{item.onAccMr}}</td>
							<td class="rowcel">{{item.detAmt}}</td>
							<td class="rowcel"><input type="button" value="remove" ng-click="removeDetMR($index)" /></td>
						</tr>
						<tr>
							<td></td>
							<td>TOTAL</td>
							<td class="rowcel">{{totOAAmt}}</td>
						</tr>
					</table>
				</div>
				</div>
    		 </div>	
		    
		    
		  
		    <div class="row">
		   			
		       	<!-- 	<div class="col s4 input-field">
		       				<input class="validate" type ="number" id="netPayId" name ="netPayName" ng-model="selOAMR.mrNetAmt" readonly>
		       			<label for="code">Payment Detail Amount</label>	
		       		</div> -->
		       			  
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="billId" name ="billName" ng-model="bill.blBillNo" ng-click="selectBill()" readonly>
		       			<label for="code">Select Bill No.</label>	
		       		</div>
		       			       		
		    </div>
		    
		   
		    <div class="row">
		    	<div class="input-field col s12">
				 <div ng-show="subPdmrList.length > 0">
					<table class="tblrow">
					<caption class="coltag tblrow">Payment Detail Money Receipt</caption>
						<tr class="rowclr">
							<th class="colclr">Bill No.</th>
							<th class="colclr">Bill Date</th>
							<th class="colclr">Freight</th>
							<th class="colclr">Service Tax</th>
							<th class="colclr">TDS</th>
							<th class="colclr">Excess</th>
							<th class="colclr">Deduction</th>
							<th class="colclr">Ded Reason</th>
							<th class="colclr">Net Payment</th>
							<th class="colclr">Action</th>
						</tr>
						<tr class="tbl" ng-repeat="subPmr in subPdmrList">
							<td class="rowcel">{{subPmr.bill.blBillNo}}</td>
							<td class="rowcel">{{subPmr.bill.blBillDt}}</td>
							<td class="rowcel">{{subPmr.mrFreight}}</td>
							<td class="rowcel">{{subPmr.mrSrvTaxAmt}}</td>
							<td class="rowcel">{{subPmr.mrTdsAmt}}</td>
							<td class="rowcel">{{subPmr.mrAccessAmt}}</td>
							<td class="rowcel">{{subPmr.mrDedAmt}}</td>
							<td class="rowcel">{{subPmr.mrDedRsn}}</td>
							<td class="rowcel">{{subPmr.mrNetAmt}}</td>
							<td class="rowcel"><input type="button" value="remove" ng-click="removePDMR($index)" /></td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td>TOTAL</td>
							<td class="rowcel">{{totRecAmt}}</td>
						</tr>
					</table>
				</div>
				</div>
    		 </div>	
		    
		     <div class="row">
				<div class="input-field col s12">
					<i class="mdi-editor-mode-edit prefix"></i>
 					<textarea id="textarea" class="materialize-textarea"  rows="3" cols="92"  name ="desc" ng-model="pdmr.mrDesc"></textarea>
 					<label>Description</label>
				</div>
         	</div>	
         
	      <div class="row">
	     		<div class="input-field col s12 center">
	       				<input class="validate" type ="submit" id="saveId" value="submit" >
	       		</div>
	      </div>		
	     
	</form>
	</div> 
	
	
	<div id ="selCustId" ng-hide="selCustFlag">
		  <input type="text" name="filterCust" ng-model="filterCust" placeholder="Search....">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Customer Name</th>
 	  	  	  <th>FaCode</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="cust in custList | filter:filterCust">
		 	  <td><input type="radio"  name="cust"   value="{{ cust }}" ng-model="custCode" ng-click="saveCustomer(cust)"></td>
              <td>{{cust.custName}}</td>
              <td>{{cust.custFaCode}}</td>
          </tr>
      </table> 
	</div>
	
	<div id ="selPDMRId" ng-hide="selPDMRFlag">
		  <input type="text" name="filterMR" ng-model="filterMR" placeholder="Search....">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>MR NO</th>
 	  	  	  <th>Date</th>
 	  	  	  <th>Net Payment</th>
 	  	  	  <th>Rem Amt</th>
 	  	  	  <th>Detail Amt</th>
 	  	  	  <th>Action</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="mr in mrList | filter:filterMR">
		 	  <td><input type="checkbox"  name="mr"   value="{{ mr }}" ></td>
              <td>{{mr.mrNo}}</td>
               <td>{{mr.mrDate | date:'dd/MM/yyyy'}}</td>
              <td>{{mr.mrNetAmt}}</td>
              <td>{{mr.mrRemAmt}}</td>
              <td><input type="number" name="mrAmtName" id="maAmtId[$index]" ng-model="mrAmt[$index]" ng-blur="checkAmt($index,mr.mrRemAmt,mrAmt[$index])"></td>
              <td><input type="button" name="saveMrAmtName" id="saveMrAmtId[$index]" value="Submit" ng-click="saveMR(mrAmt[$index],mr,$index)"></td>
          </tr>
      </table> 
	</div>
	
	
	<div id ="selBillId" ng-hide="selBillFlag">
		  <input type="text" name="filterBill" ng-model="filterBill" placeholder="Search....">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Bill No</th>
 	  	  	  <th>Bill Date</th>
 	  	  	  <th>Final Amount</th>
 	  	  	  <th>Pending Amount</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="bill in blList | filter:filterBill">
		 	  <td><input type="radio"  name="bill"   value="{{ bill }}" ng-model="billCode" ng-click="saveBill(bill)"></td>
              <td>{{bill.blBillNo}}</td>
              <td>{{bill.blBillDt}}</td>
              <td>{{bill.blFinalTot | number : 2}}</td>
              <td>{{bill.blRemAmt | number : 2}}</td>
          </tr>
      </table> 
	</div>
	
	
	<div id ="dedDetId" ng-hide="dedDetFlag">
 	  <form class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" name="dedDetForm" ng-submit="submitDedDet(dedDetForm)">
		 <div class="row">
		 		
		 		<div class="col s4 input-field">
		       		<input class="validate" type ="text" id="cnmtId" name ="cnmtName" ng-model="cnmtCode" ng-click="selectCnmt()" readonly>
		       			<label for="code">Select Cnmt No</label>	
		       	</div>
		       	
		 		<div class="col s4 input-field">
					<select name="dedTypeName" id="dedTypeId" ng-model="dedType" ng-init="dedType = 'FREIGHT RATE'" ng-required="true">
							<option value='FREIGHT RATE'>FREIGHT RATE</option>
							<option value='FREIGHT WEIGHT'>FREIGHT WEIGHT</option>
							<option value='LATE DELIVERY'>LATE DELIVERY</option>
							<option value='COOLIE LOADING'>COOLIE LOADING</option>
							<option value='COOLIE UNLOADING'>COOLIE UNLOADING</option>			
							<option value='DETENTION'>DETENTION</option>
							<option value='CLAIM SHORTAGE'>CLAIM SHORTAGE</option>
							<option value='CLAIM DAMAGE'>CLAIM DAMAGE</option>
							<option value='NON PLACEMENT'>NON PLACEMENT</option>
							<option value='OTHERS'>OTHERS</option>
					</select>
					<label>Deduction Type</label>
				</div>
				
				<div class="col s4 input-field">
					<input type ="number" name="dedName" id="dedId" ng-model="dedAmt" step="0.001" min="0.000" ng-required="true"/>
					<label>Deduction Amount</label>
				</div>
				
	     </div>
	  	     
	     <div class="row">
	     		<div class="input-field col s12 center">
	       				<input class="validate" type ="submit" value="submit" >
	       		</div>
	      </div>		
	    </form> 
	</div>
	
	
	<div id ="selCnmtId" ng-hide="selCnmtFlag">
		  <input type="text" name="filterCnmt" ng-model="filterCnmt" placeholder="Search....">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>CNMT NO</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="cnmt in cnmtList | filter:filterCnmt">
		 	  <td><input type="radio"  name="cnmt"   value="{{ cnmt }}" ng-model="cnmtCode" ng-click="saveCnmt(cnmt)"></td>
              <td>{{cnmt}}</td>
          </tr>
       </table>
	</div>
	
	
	<div id="billInfoId" ng-hide="billInfoFlag">
	<form class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" name="billInfoForm" ng-submit="submitBillInfo(billInfoForm)">
		 <div class="row">
		 		<div class="col s4 input-field">
					<input class="validate" type ="number" name="frtAmtName" id="frtAmtId" ng-model="subPdmr.mrFreight" step="0.001" min="0.000" ng-required="true" ng-readonly='customer.custEditBillAmt'>
					<label>Freight Amount</label>
				</div>
				
				<div class="col s4 input-field">
					<input type ="number" name="tdsName" id="tdsId" ng-model="subPdmr.mrTdsAmt" step="0.001" min="0.000"/>
					<label>TDS</label>
				</div>
				
			    <div class="col s4 input-field">
					<input class="validate" type ="number" name="recAmtName" id="recAmtId" ng-model="subPdmr.mrNetAmt" step="0.001" min="0.000" ng-required="true">
					<label>Received Amount</label>
				</div>
			
	     </div>
	     
	     <div class="row">
	     		<div class="col s4 input-field">
					<input type ="number" name="dedName" id="dedId" ng-model="subPdmr.mrDedAmt" step="0.001" min="0.000">
					<label>Deduction Amount</label>
				</div>
				
				 <div class="col s4 input-field">
					<input type ="button" name="dedBtn" id="dedBtnId" value="Deduction Detail" ng-click="addDedDetail()">
				</div>
				
	     		<div class="col s4 input-field">
					<input type ="number" name="excName" id="excId" ng-model="subPdmr.mrAccessAmt" step="0.001" min="0.000"/>
					<label>Excess</label>
				</div>
	     </div>
	    
	     
	     <!-- <div class="row">
		      <div class="col s4 input-field">
					<input type ="number" name="stName" id="stId" ng-model="subPdmr.mrSrvTaxAmt" step="0.001" min="0.000" ng-requierd="true">
					<label>Service Tax</label>
			  </div>
	     </div> -->
	     
	    <div class="row">
	    	<div class="input-field col s12">
			 <div ng-show="dedDetailList.length > 0">
				<table class="tblrow">
				<caption class="coltag tblrow">Deduction Detail</caption>
					<tr class="rowclr">
						<th class="colclr">Cnmt No.</th>
						<th class="colclr">Dedution Type</th>
						<th class="colclr">Deduction Amount</th>
						<th class="colclr">Action</th>
					</tr>
					<tr class="tbl" ng-repeat="dedDetail in dedDetailList">
						<td class="rowcel">{{dedDetail.cnmtNo}}</td>
						<td class="rowcel">{{dedDetail.dedType}}</td>
						<td class="rowcel">{{dedDetail.dedAmt}}</td>
						<td class="rowcel"><input type="button" value="remove" ng-click="removeDed($index)" /></td>
					</tr>
				</table>
			</div>
			</div>
   		</div>	 
	     
	    
	     
	    <!-- <div class="row">
				<div class="input-field col s12">
					<i class="mdi-editor-mode-edit prefix"></i>
 					<textarea id="textarea" class="materialize-textarea"  rows="3" cols="92"  name ="dedRsn" ng-model="subPdmr.mrDedRsn"></textarea>
 					<label>Deduction Reason</label>
				</div>
         </div>	 -->
	     
	     
	      <div class="row">
	     		<div class="input-field col s12 center">
	       				<input class="validate" type ="submit" value="submit" >
	       		</div>
	       </div>		
	     
	</form>
	</div>
	
	
	
</div>	