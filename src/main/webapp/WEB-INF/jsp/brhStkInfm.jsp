<div ng-show="operatorLogin || superAdminLogin" title="Branch Stock Information">
	<style>
	
	.tbl{text-align:center; 
		  color: #B5DAD4;
		
		border:1px solid rgba(255, 255, 255, 0.47);
		-moz-border-right: 1px solid rgba(255, 255, 255, 0.47);
		-webkit-border-right: 1px solid rgba(255, 255, 255, 0.47);}
		
		.rowcelwhite
		{
		text-align: center;
		color: #fff;
		border: 1px solid rgba(255, 255, 255, 0.47);
	    font-size:14px;
	    padding: 0px;
	    
	    }
	    .rowcelheader
	    {
		text-align: center;
	    color: #B5DAD4;
	    border: 1px solid rgba(255, 255, 255, 0.47);
	    font-size:14px;
	    font-weight:bold;
	    padding:3px;
	    
	    }
	    
		.align{
	    text-align:center;
	    font-size:20px;
	    }
	    .cursor
	    {
	    cursor:pointer;
	    }
		:hover.cursor
		{
		background-color:rgba(73, 156, 145, 0.25);
		}
		
	</style>
	<div class="row">
		
		<form name="StkEnqForm" ng-submit="submitStkEnq(StkEnqForm)" class="col s12 card" style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
			<!-- Find By Branch -->		
			<div class="row">
		       	<div class="col s3 input-field">
		       		<input class="validate" type ="text" id="branchFaCode" name="branchFaCode" ng-model="branchFaCode" ng-click="OpenBranchDB()" required="required" readonly="readonly">
		       		<input type ="hidden" id="branchCode" name="branchCode" ng-model="branchCode" required="required"/>
					<label>Branch</label>
				</div>       	
		       	<div class="col s3 input-field">
	       			<input class="col s12 btn teal white-text" type ="submit" ng-click="submitStkEnq(StkEnqForm, 'findByBranch')" id="stkEnqFormSubmit" name ="stkEnqFormSubmit" value="Find By Branch" >
	       		</div>
			</div>
			
			<!-- Find By Start No. And End No. -->
			<div class="row">
		       	<div class="col s3 input-field">
		       		<input class="validate" type ="text" id="startNo" name="startNo" ng-model="stock.startNo" ng-required="required">
					<label>Start No.</label>
				</div> 
				<div class="col s3 input-field">
		       		<input class="validate" type ="text" id="endNo" name="endNo" ng-model="stock.endNo" ng-required="required">
					<label>End No.</label>
				</div>
				<div class="col s3 input-field">
		    		<select name="typeName" id="typeName" ng-model="stock.typeName" ng-required="required" ng-init="stock.typeName ='cnmt'">
						<option value='cnmt'>CNMT</option>
						<option value='chln'>CHALLAN</option>
						<option value='sedr'>SEDR</option>
					</select>
					<label>Type</label>
				</div>      	
		       	<div class="col s3 input-field">
	       			<input class="col s12 btn teal white-text" type ="submit" id="findByStk" name ="findByStk" ng-click="findByStk(StkEnqForm)" value="Find By Stock" >
	       		</div>
			</div>
			
			<!-- Find Missing CNMT/Challan/SEDR -->
			<div class="row">
				<div class="col s3 input-field">
		       		<input class="validate" type ="text" id="branchFaCode" name="branchFaCode" ng-model="branchFaCode" ng-click="OpenBranchDB()" required="required" readonly="readonly">
		       		<input type ="hidden" id="stockBranchCode" name="stockBranchCode" ng-model="stock.branchCode"/>
					<label>Branch</label>
				</div>
		       	<div class="col s3 input-field">
		       		<input class="validate" type ="date" id="dateFrom" name="dateFrom" ng-model="stock.dateFrom" ng-required="required">
					<label>From Date</label>
				</div> 
				<div class="col s3 input-field">
		       		<input class="validate" type ="date" id="dateTo" name="dateTo" ng-model="stock.dateTo">
					<label>To Date</label>
				</div>
				<div class="col s3 input-field">
		    		<select name="typeName" id="typeName" ng-model="stock.stnType">
		    			<option value=''></option>
						<option value='cnmt'>CNMT</option>
						<option value='chln'>CHALLAN</option>
						<option value='sedr'>SEDR</option>
					</select>
					<label>Type</label>
				</div>  
		       	<div class="col s3 input-field">
	       			<input class="col s12 btn teal white-text" type ="button" id="findMissingStn" name ="findMissingStn" ng-click="findMissingStn(StkEnqForm)" value="Find Missing Stn." >
	       		</div>
	       		
	       		<div class="col s3 input-field">
	       			<input class="col s12 btn teal white-text" type ="button" id="downloadMissing" name ="downloadMissing" ng-click="downLoadMissingStn(StkEnqForm)" value="Download" >
	       		</div>
			</div>
			
			<!-- Find By Master Stock -->
			<div class="row">
		       	<div class="col s3 input-field">
	       			<input class="col s12 btn teal white-text" type ="submit" ng-click="findByMstrStock()" id="findByMstrStock" name ="findByMstrStock" value="Find By Master Stock" >
	       		</div>
			</div>
		</form>
		
	</div>
	
	<div id ="branchDB" ng-hide="branchDBFlag">
		<input type="text" name="filterTextbox1" ng-model="filterTextbox1" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Branch Name </th>
 	  	  	  <th> Branch FaCode </th>
 	  	  </tr>
		  <tr ng-repeat="branch in branchList | filter:filterTextbox1">
		 	  <td><input type="radio" name="branch" value="{{ branch }}" ng-click="saveBranch(branch)"></td>
              <td>{{branch.branchName}}</td>
              <td>{{branch.branchFaCode}}</td>
          </tr>
      </table> 
	</div>
	
	<div id ="stkEnqDB" ng-hide="stkEnqDBFlag">
		<input type="text" name="filterTextbox1" ng-model="filterTextbox1" placeholder="Search">
 	    <table>
 	  	  <tr> 	  	  	  
 	  	  	  <th>S.No. </th>
 	  	  	  <th>Start No.</th> 	  	  	    
 	  	  </tr>
		  <tr ng-repeat="enq in enqList | filter:filterTextbox1">		 	 
              <td>{{$index + 1}}</td>
              <td>{{enq}}</td>                            
          </tr>
      </table> 
	</div>
	
	<div id ="stkEnqUsedUnusedDB" ng-hide="stkEnqUsedUnusedDBFlag">
		<input type="text" name="filterTextbox1" ng-model="filterTextbox1" placeholder="Search">
 	    <table>
 	  	  <tr> 	  	  	  
 	  	  	  <th>S.No. </th>
 	  	  	  <th>Start No.</th> 	  	  	    
 	  	  </tr>
		  <tr ng-repeat="enq in usedUnusedList | filter:filterTextbox1">		 	 
              <td>{{$index + 1}}</td>
              <td>{{enq}}</td>                            
          </tr>
      </table> 
	</div>
	
	<div id ="stkEnqUsedDB" ng-hide="stkEnqDBUsedFlag">
		<input type="text" name="filterTextbox1" ng-model="filterTextbox1" placeholder="Search">
 	    <table>
 	  	  <tr> 	  	  	  
 	  	  	  <th>S.No. </th>
 	  	  	  <th>Start No.</th>
 	  	  	  <th>Used</th>
 	  	  	  <th>Unused</th> 	  	  	    
 	  	  </tr>
		  <tr ng-repeat="enq in enqList | filter:filterTextbox1">		 	 
              <td>{{$index + 1}}</td>
              <td>{{enq.startNo}}</td>   
              <td ng-click="fetchDetail(enq.startNo, 'used')" style="cursor:pointer;">{{enq.used}}</td>
              <td ng-click="fetchDetail(enq.startNo, 'unused')" style="cursor:pointer;">{{enq.unused}}</td>  
          </tr>
      </table> 
	</div>
		
	<div ng-hide="stkEnqTable">
		<table class="table">
 	    	<caption class="coltag tblrow">Stationary By Branch</caption> 
	 	    <tr>
 	  	  		<th class="rowcelheader" colspan="5">CNMT</th>	  	
 	  	  		<th class="rowcelheader" colspan="5">CHALLAN</th>
 	  	  		<th class="rowcelheader" colspan="5">SEDR</th>
 	  	  </tr>
 	  	  <tr>
 	  	  		<th class="rowcelheader">Total</th> 	  	  	  
 	  	  		<th class="rowcelheader">Rec.</th>
 	  	  		<th class="rowcelheader">UnRec.</th>
 	  	  		<th class="rowcelheader">Used</th>
 	  	  		<th class="rowcelheader">Unused</th>
 	  	  		
 	  	  		<th class="rowcelheader">Total</th> 	  	  	  
 	  	  		<th class="rowcelheader">Rec.</th>
 	  	  		<th class="rowcelheader">UnRec.</th>
 	  	  		<th class="rowcelheader">Used</th>
 	  	  		<th class="rowcelheader">Unused</th>
 	  	  		
 	  	  		<th class="rowcelheader">Total</th> 	  	  	  
 	  	  		<th class="rowcelheader">Rec.</th>
 	  	  		<th class="rowcelheader">UnRec.</th>
 	  	  		<th class="rowcelheader">Used</th>
 	  	  		<th class="rowcelheader">Unused</th>
 	  	  		
 	  	  </tr>
 	  	  <tr>
 	  	  
 	  	  	<!-- CNMT -->
				<td ng-if="totalCnmt.length > 0 " ng-click="showDetail(totalCnmt,'Total CNMT')" class="rowcelwhite cursor">{{totalCnmt.length}}</td>
			 	<td ng-if="totalCnmt.length < 1 " class="rowcelwhite">{{totalCnmt.length}}</td>
			 	<td ng-if="recCnmt.length > 0" ng-click="showDetail(recCnmt,'Receive CNMT')" class="rowcelwhite cursor">{{recCnmt.length}}</td>		 	  
			 	<td ng-if="recCnmt.length < 1" class="rowcelwhite">{{recCnmt.length}}</td>			 	
			 	<td ng-if="unRecCnmt.length > 0" ng-click="showDetail(unRecCnmt,'UnReceive CNMT')" class="rowcelwhite cursor">{{unRecCnmt.length}}</td>		 	  
			 	<td ng-if="unRecCnmt.length < 1" class="rowcelwhite">{{unRecCnmt.length}}</td>			 	
			 	<td ng-if="usedCnmt.length > 0" ng-click="showDetail(usedCnmt,'Used CNMT')" class="rowcelwhite cursor">{{usedCnmt.length}}</td>
			 	<td ng-if="usedCnmt.length < 1" class="rowcelwhiter">{{usedCnmt.length}}</td>
			 	<td ng-if="unUsedCnmt.length > 0" ng-click="showDetail(unUsedCnmt,'Unused CNMT')" class="rowcelwhite cursor">{{unUsedCnmt.length}}</td>	
			 	<td ng-if="unUsedCnmt.length < 1" class="rowcelwhite">{{unUsedCnmt.length}}</td>
			 	
			 <!-- Challan -->
		 	  	<td ng-if="totalChln.length > 0 " ng-click="showDetail(totalChln,'Total Chln')" class="rowcelwhite cursor">{{totalChln.length}}</td>
			 	<td ng-if="totalChln.length < 1 " class="rowcelwhite">{{totalChln.length}}</td>
			 	<td ng-if="recChln.length > 0" ng-click="showDetail(recChln,'Receive Chln')" class="rowcelwhite cursor">{{recChln.length}}</td>		 	  
			 	<td ng-if="recChln.length < 1" class="rowcelwhite">{{recChln.length}}</td>			 	
			 	<td ng-if="unRecChln.length > 0" ng-click="showDetail(unRecChln,'Unreceive Chln')" class="rowcelwhite cursor">{{unRecChln.length}}</td>		 	  
			 	<td ng-if="unRecChln.length < 1" class="rowcelwhite">{{unRecChln.length}}</td>			 	
			 	<td ng-if="usedChln.length > 0" ng-click="showDetail(usedChln,'Used Challan')" class="rowcelwhite cursor">{{usedChln.length}}</td>
			 	<td ng-if="usedChln.length < 1" class="rowcelwhiter">{{usedChln.length}}</td>
			 	<td ng-if="unUsedChln.length > 0" ng-click="showDetail(unUsedChln,'Unused Chln')" class="rowcelwhite cursor">{{unUsedChln.length}}</td>	
			 	<td ng-if="unUsedChln.length < 1" class="rowcelwhite">{{unUsedChln.length}}</td>
			 	
			<!-- SEDR -->
				<td ng-if="totalSedr.length > 0 " ng-click="showDetail(totalSedr,'Total Sedr')" class="rowcelwhite cursor">{{totalSedr.length}}</td>
			 	<td ng-if="totalSedr.length < 1 " class="rowcelwhite">{{totalSedr.length}}</td>
			 	<td ng-if="recSedr.length > 0" ng-click="showDetail(recSedr,'Receive Sedr')" class="rowcelwhite cursor">{{recSedr.length}}</td>		 	  
			 	<td ng-if="recSedr.length < 1" class="rowcelwhite">{{recSedr.length}}</td>			 	
			 	<td ng-if="unRecSedr.length > 0" ng-click="showDetail(unRecSedr,'Unreceive Sedr')" class="rowcelwhite cursor">{{unRecSedr.length}}</td>		 	  
			 	<td ng-if="unRecSedr.length < 1" class="rowcelwhite">{{unRecSedr.length}}</td>			 	
			 	<td ng-if="usedSedr.length > 0" ng-click="showDetail(usedSedr,'Used SEDR')" class="rowcelwhite cursor">{{usedSedr.length}}</td>
			 	<td ng-if="usedSedr.length < 1" class="rowcelwhiter">{{usedSedr.length}}</td>
			 	<td ng-if="unUsedSedr.length > 0" ng-click="showDetail(unUsedSedr,'Unused Sedr')" class="rowcelwhite cursor">{{unUsedSedr.length}}</td>	
			 	<td ng-if="unUsedSedr.length < 1" class="rowcelwhite">{{unUsedSedr.length}}</td>
		 	  
          </tr>
      </table> 
	</div>	
	
	<!-- Find By Stock Table -->
	<div ng-hide="findByStockTable">
		<table class="table">
 	    	<caption class="coltag tblrow">Stationary Enquiry</caption> 
	 	    <tr>
 	  	  		<th class="rowcelheader" colspan="8">Find by Stock</th>	  	
 	  	  </tr>
 	  	  <tr>
 	  	  		<th class="rowcelheader">S.No</th>
 	  	  		<th class="rowcelheader">Start No.</th>
 	  	  		<th class="rowcelheader">Dirty</th>
 	  	  		<th class="rowcelheader">Is Issued</th>
 	  	  		<th class="rowcelheader">Is Receive</th>
 	  	  		<th class="rowcelheader">Branch Code</th>
 	  	  		<th class="rowcelheader">Used</th>
 	  	  		<th class="rowcelheader">Unused</th>
 	  	  </tr>
 	  	  <tr ng-repeat="obj in byStkList">
				<td class="rowcelwhite">{{$index +1}}</td>
				<td class="rowcelwhite">{{obj.startNo}}</td>
				<td class="rowcelwhite">{{obj.isDirty}}</td>
				<td class="rowcelwhite">{{obj.isIssued}}</td>
				<td class="rowcelwhite">{{obj.isRec}}</td>
				<td class="rowcelwhite">{{obj.branchCode}}</td>
				<td ng-if="obj.usedCount.length > 0" ng-click="showDt(obj.usedCount)" class="rowcelwhite cursor">{{obj.usedCount.length}}</td>
				<td ng-if="obj.usedCount.length < 1" class="rowcelwhite">{{obj.usedCount.length}}</td>
				<td ng-if="obj.unusedCount.length > 0" ng-click="showDt(obj.unusedCount)" class="rowcelwhite cursor">{{obj.unusedCount.length}}</td>
				<td ng-if="obj.unusedCount.length < 1" class="rowcelwhite">{{obj.unusedCount.length}}</td>
          </tr>
      </table> 
	</div>	
		
	<div ng-hide="mstrStkTable">
		<table class="table">
 	    	<caption class="coltag tblrow">Master Stock</caption> 
	 	    <tr>
 	  	  		<th class="rowcelheader" colspan="4">CNMT</th>	  	
 	  	  		<th class="rowcelheader" colspan="4">CHALLAN</th>
 	  	  		<th class="rowcelheader" colspan="4">SEDR</th>
 	  	  </tr>
 	  	  <tr>
 	  	  		<th class="rowcelheader">Available</th> 	  	  	  
 	  	  		<th class="rowcelheader">Sent To Branch</th>
 	  	  		<th class="rowcelheader">Receive By Branch</th>
 	  	  		<th class="rowcelheader">Unreceive By Branch</th>
 	  	  		
 	  	  		<th class="rowcelheader">Available</th> 	  	  	  
 	  	  		<th class="rowcelheader">Sent To Branch</th>
 	  	  		<th class="rowcelheader">Receive By Branch</th>
 	  	  		<th class="rowcelheader">Unreceive By Branch</th>
 	  	  		
 	  	  		<th class="rowcelheader">Available</th> 	  	  	  
 	  	  		<th class="rowcelheader">Sent To Branch</th>
 	  	  		<th class="rowcelheader">Receive By Branch</th>
 	  	  		<th class="rowcelheader">Unreceive By Branch</th>
 	  	  		
 	  	  </tr>
 	  	  <tr ng-repeat="mstr in masterStk">
 	  	  
 	  	  	<!-- CNMT -->
				<td ng-if="mstr.cnmtAval.length > 0 " ng-click="showMaster(mstr.cnmtAval)" class="rowcelwhite cursor">{{mstr.cnmtAval.length}}</td>
			 	<td ng-if="mstr.cnmtAval.length < 1 " class="rowcelwhite">{{mstr.cnmtAval.length}}</td>
			 	<td ng-if="mstr.cnmtSentToBranch.length > 0 " ng-click="showMaster(mstr.cnmtSentToBranch)" class="rowcelwhite cursor">{{mstr.cnmtSentToBranch.length}}</td>
			 	<td ng-if="mstr.cnmtSentToBranch.length < 1 " class="rowcelwhite">{{mstr.cnmtSentToBranch.length}}</td>		 	
			 	<td ng-if="mstr.cnmtRecByBranch.length > 0 " ng-click="showMaster(mstr.cnmtRecByBranch)" class="rowcelwhite cursor">{{mstr.cnmtRecByBranch.length}}</td>
			 	<td ng-if="mstr.cnmtRecByBranch.length < 1 " class="rowcelwhite">{{mstr.cnmtRecByBranch.length}}</td>		 	
			 	<td ng-if="mstr.cnmtUnrecByBranch.length > 0 " ng-click="showMaster(mstr.cnmtUnrecByBranch)" class="rowcelwhite cursor">{{mstr.cnmtUnrecByBranch.length}}</td>
			 	<td ng-if="mstr.cnmtUnrecByBranch.length < 1 " class="rowcelwhite">{{mstr.cnmtUnrecByBranch.length}}</td>		 			 	
			 	
			 <!-- Challan -->
				<td ng-if="mstr.chlnAval.length > 0 " ng-click="showMaster(mstr.chlnAval)" class="rowcelwhite cursor">{{mstr.chlnAval.length}}</td>
			 	<td ng-if="mstr.chlnAval.length < 1 " class="rowcelwhite">{{mstr.chlnAval.length}}</td>
			 	<td ng-if="mstr.chlnSentToBranch.length > 0 " ng-click="showMaster(mstr.chlnSentToBranch)" class="rowcelwhite cursor">{{mstr.chlnSentToBranch.length}}</td>
			 	<td ng-if="mstr.chlnSentToBranch.length < 1 " class="rowcelwhite">{{mstr.chlnSentToBranch.length}}</td>		 	
			 	<td ng-if="mstr.chlnRecByBranch.length > 0 " ng-click="showMaster(mstr.chlnRecByBranch)" class="rowcelwhite cursor">{{mstr.chlnRecByBranch.length}}</td>
			 	<td ng-if="mstr.chlnRecByBranch.length < 1 " class="rowcelwhite">{{mstr.chlnRecByBranch.length}}</td>		 	
			 	<td ng-if="mstr.chlnUnrecByBranch.length > 0 " ng-click="showMaster(mstr.chlnUnrecByBranch)" class="rowcelwhite cursor">{{mstr.chlnUnrecByBranch.length}}</td>
			 	<td ng-if="mstr.chlnUnrecByBranch.length < 1 " class="rowcelwhite">{{mstr.chlnUnrecByBranch.length}}</td>
			 	
			 <!-- SEDR -->
				<td ng-if="mstr.sedrAval.length > 0 " ng-click="showMaster(mstr.sedrAval)" class="rowcelwhite cursor">{{mstr.sedrAval.length}}</td>
			 	<td ng-if="mstr.sedrAval.length < 1 " class="rowcelwhite">{{mstr.sedrAval.length}}</td>
			 	<td ng-if="mstr.sedrSentToBranch.length > 0 " ng-click="showMaster(mstr.sedrSentToBranch)" class="rowcelwhite cursor">{{mstr.sedrSentToBranch.length}}</td>
			 	<td ng-if="mstr.sedrSentToBranch.length < 1 " class="rowcelwhite">{{mstr.sedrSentToBranch.length}}</td>		 	
			 	<td ng-if="mstr.sedrRecByBranch.length > 0 " ng-click="showMaster(mstr.sedrRecByBranch)" class="rowcelwhite cursor">{{mstr.sedrRecByBranch.length}}</td>
			 	<td ng-if="mstr.sedrRecByBranch.length < 1 " class="rowcelwhite">{{mstr.sedrRecByBranch.length}}</td>		 	
			 	<td ng-if="mstr.sedrUnrecByBranch.length > 0 " ng-click="showMaster(mstr.sedrUnrecByBranch)" class="rowcelwhite cursor">{{mstr.sedrUnrecByBranch.length}}</td>
			 	<td ng-if="mstr.sedrUnrecByBranch.length < 1 " class="rowcelwhite">{{mstr.sedrUnrecByBranch.length}}</td>
			 	
          </tr>
      </table> 
	</div>
	
	<div id ="masterStkDB" ng-hide="masterStkDBFlag">
		<input type="text" name="filterTextbox1" ng-model="filterTextbox1" placeholder="Search">
 	    <table>
 	  	  <tr> 	  	  	  
 	  	  	  <th>S.No. </th>
 	  	  	  <th>Start No.</th> 	  	  	    	  	    
 	  	  </tr>
		  <tr ng-repeat="enq in masterDt | filter:filterTextbox1">		 	 
              <td>{{$index + 1}}</td>
              <td>{{enq.mstrStnStkStartNo}}</td>   
          </tr>
      </table> 
	</div>
	
	<div id="missingTable">
	
	<div id ="missCnmt" ng-show="cnmtMissFlag" style="width: 30%;float: left;">		
 	   <table class="table">
 	    	<caption class="coltag tblrow"><b>Missing CNMT</b></caption> 
	 	    <tr>
 	  	  		<th class="rowcelheader" style="width: 15%;">S.No.</th>
 	  	  		<th class="rowcelheader">Cnmt</th>
 	  	  		<th class="rowcelheader">BCode</th>  
 	  	  	</tr>
		  	<tr ng-repeat="cm in missStn.cnmt">		 	 
            	<td class="rowcelwhite" style="width: 15%;">{{$index + 1}}</td>
              	<td class="rowcelwhite">{{cm.split(':')[0]}}</td>   
              	<td class="rowcelwhite">{{cm.split(':')[1]}}</td>
          </tr>
      </table> 
	</div>
	
	<div id ="missChln" ng-show="chlnMissFlag" style="width: 30%;float: left;">		
 	   <table class="table">
 	    	<caption class="coltag tblrow"><b>Missing CHLN</b></caption> 
	 	    <tr>
 	  	  		<th class="rowcelheader" style="width: 15%;">S.No.</th>
 	  	  		<th class="rowcelheader">Chln</th> 	  	   	  	  	    	  	    
 	  	  		<th class="rowcelheader">BCode</th>
 	  	  	</tr>
		  	<tr ng-repeat="ch in missStn.chln">		 	 
            	<td class="rowcelwhite" style="width: 15%;">{{$index + 1}}</td>
              	<td class="rowcelwhite">{{ch.split(':')[0]}}</td>   
              	<td class="rowcelwhite">{{ch.split(':')[1]}}</td>
          </tr>
      </table> 
	</div>
	
	<div id ="missSedr" ng-show="sedrMissFlag" style="width: 30%;float: left;">		
 	   <table class="table">
 	    	<caption class="coltag tblrow"><b>Missing SEDR</b></caption> 
	 	    <tr>
 	  	  		<th class="rowcelheader" style="width: 15%;">S.No.</th>
 	  	  		<th class="rowcelheader">SEDR</th>
 	  	  		<th class="rowcelheader">BCode</th>  
 	  	  	</tr>
		  	<tr ng-repeat="se in missStn.sedr">		 	 
            	<td class="rowcelwhite" style="width: 15%;">{{$index + 1}}</td>
              	<td class="rowcelwhite">{{se.split(':')[0]}}</td>   
              	<td class="rowcelwhite">{{se.split(':')[1]}}</td>
          </tr>
      </table> 
	</div>
	
	</div>
	

</div>