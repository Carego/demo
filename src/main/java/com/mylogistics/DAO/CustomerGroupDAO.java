package com.mylogistics.DAO;

import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import com.mylogistics.model.CustomerGroup;
import com.mylogistics.model.User;


@Repository
public interface CustomerGroupDAO {

	public Map<String,Object> getCustAndCustGroup(Session session);
	public void updateCustomerGroupId(Session session,String custGroupId,List<Integer> custList);
	public List<CustomerGroup> getAllCustGroupList(SessionFactory sessionFactory);
	public List<CustomerGroup> getAllCustGroupList(Session session, User user);
	public List<String> getCustCodesByGroup(Session session, User user, String groupId);
	
}
