'use strict';

var app = angular.module('application');

app.controller('PayDetMRNCntlr',['$scope','$location','$http','$filter','$sce','$log','$document',
                              function($scope,$location,$http,$filter,$sce,$log, $document){
	
	$scope.mrList = [];
	$scope.pdmr = {};
	$scope.pdmr.mrFrPDList = [];
	$scope.subPdmr = {};
	$scope.subPdmrList = [];
	$scope.selOAMR = {}
	$scope.blList = [];
	$scope.selBill = {};
	$scope.totRecAmt = 0;
	//$scope.selBlList = [];
	
	$scope.selCustFlag = true;
	$scope.selPDMRFlag = true;
	$scope.selBillFlag = true;
	$scope.billInfoFlag = true;
	$scope.selCnmtFlag = true;
	$scope.dedDetFlag = true;
	$scope.isFormSubmitted = false;
	$scope.isdedDetFormSubmitted = false;
	$scope.isbillInfoFormSubmitted = false;
	
	$scope.cnmtCode = "";
	$scope.dedType = "";
	
	$scope.dedDetailList = [];
	
	$('#recAmtId').keypress(function(key) {
		if(key.charCode < 46 || key.charCode > 57)
			return false;
	});


	$('#recAmtId').keypress(function(e) {
		if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 7)){
			e.preventDefault();
		}
	});
	
	
	$('#frtAmtId').keypress(function(key) {
		if(key.charCode < 46 || key.charCode > 57)
			return false;
	});


	$('#frtAmtId').keypress(function(e) {
		if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 7)){
			e.preventDefault();
		}
	});
	
	$('#tdsId').keypress(function(key) {
		if(key.charCode < 46 || key.charCode > 57)
			return false;
	});


	$('#tdsId').keypress(function(e) {
		if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 7)){
			e.preventDefault();
		}
	});
	
	
	$('#dedId').keypress(function(key) {
		if(key.charCode < 46 || key.charCode > 57)
			return false;
	});


	$('#dedId').keypress(function(e) {
		if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 7)){
			e.preventDefault();
		}
	});
	
	$('#excId').keypress(function(key) {
		if(key.charCode < 46 || key.charCode > 57)
			return false;
	});


	$('#excId').keypress(function(e) {
		if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 7)){
			e.preventDefault();
		}
	});
	
	
	$('#stId').keypress(function(key) {
		if(key.charCode < 46 || key.charCode > 57)
			return false;
	});


	$('#stId').keypress(function(e) {
		if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 7)){
			e.preventDefault();
		}
	});
	
	
	
	$scope.getPDMrDetail = function(){
		console.log("enter into getMrDetail function");
		var response = $http.post($scope.projectName+'/getPDMrDetail');
		  response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				  $scope.csDt = data.cssDt;
				  $scope.cssDt = data.cssDt;
				  $scope.pdmr.mrDate = $scope.csDt;
				  if($scope.currentBranch === "CRPO"){
					  $("#csDtId").attr("readonly", false);
				  }else{
					  $("#csDtId").attr("readonly", true);
				  }
				  $scope.custList = data.list;
			  }else{
				  $scope.errorToast("SERVER ERROR");
			  }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
		   });	
	}
	
	
	$scope.chngDt = function(){
		console.log("enter into chngDt function");
		if($scope.csDt > $scope.cssDt){
			$scope.alertToast("You can't enter greater than "+$scope.pdmr.mrDate+" date");
			$scope.csDt = $scope.pdmr.mrDate; 
		}else{
			$scope.pdmr.mrDate = $scope.csDt;
		}
	}
	
	$scope.selectCust = function(){
		console.log("enter into selectCust function");
		$scope.selCustFlag = false;
		$('div#selCustId').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Select Customer",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.selCustFlag = true;
		    }
			});
		
		$('div#selCustId').dialog('open');	
	}
	
	
	$scope.saveCustomer = function(cust){
		console.log("enter into saveCustomer function");
		$('div#selCustId').dialog('close');	
		$scope.customer = cust;
		
		$scope.subPdmr = {};
		$scope.subPdmrList = [];
		$scope.selOAMR = {}
		$scope.blList = [];
		$scope.mrList = [];
		$scope.selBill = {};
		$scope.totRecAmt = 0;
		$scope.pdmr.mrFrPDList = [];;
		
		$scope.pdmr.mrCustId = cust.custId;
		
		var response = $http.post($scope.projectName+'/getPendingMR',$scope.pdmr.mrCustId);
		  response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				 $scope.mrList = data.list;
				 $scope.blList = data.blList;
				 
				 if($scope.blList.length > 0){
					 console.log("size of blList = "+$scope.blList.length);
				 }else{
					 $scope.alertToast("There is no pending bill for "+$scope.customer.custName);
				 }
			  }else{
				  $scope.mrList = [];
				  $scope.errorToast($scope.customer.custName+" doesn't have any pending MR");
			  }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
		   });	
	}
	
	
	$scope.selectPDMR = function(){
		console.log("enter into selectPDMR function");
		$scope.selPDMRFlag = false;
		$('div#selPDMRId').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "On Accounting MR",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.selPDMRFlag = true;
		    }
			});
		
		$('div#selPDMRId').dialog('open');	
		$scope.mrAmt = [];
		for(var i=0;i<$scope.mrList.length;i++){
			$scope.mrAmt[i] = $scope.mrList[i].mrRemAmt;
		}
	}
	
	
	$scope.checkAmt = function(index,remAmt,entrAmt){
		console.log("enter into checkAmt function");
		if(parseFloat(entrAmt) > parseFloat(remAmt)){
			$scope.mrAmt[index] = 0;
			$scope.alertToast("You can't enter more than "+remAmt);
		}
	}
	
	$scope.saveMR = function(amt,mr,index){
		console.log("enter into saveMR function = "+mr.mrBrhId);
		//$('div#selPDMRId').dialog('close');
		$scope.selOAMR = mr;		
		//$scope.pdmr.mrFrPD = mr.mrNo;		
		/*$scope.subPdmr = {};
		$scope.subPdmrList = [];
		$scope.selBill = {};
		$scope.totRecAmt = 0;*/
		if (amt == null) {
			$scope.alertToast("Please enter amount");
		}else if(parseFloat(amt) < 0){
			$scope.alertToast("you can't enter a negative amount");
		}else if(parseFloat(amt) === 0){
			$scope.alertToast("please enter greater than 0 amount");
		}else{
			if(mr.mrBrhId > 0){
				var mrDet = {
						"brhId"   : mr.mrBrhId,
						"onAccMr" : mr.mrNo,
						"detAmt"  : amt
				};				
				
				if($scope.pdmr.mrFrPDList.length > 0){
					console.log("**********");
					for(var i=0;i<$scope.pdmr.mrFrPDList.length;i++){
						if($scope.pdmr.mrFrPDList[i].onAccMr === mr.mrNo){
							$scope.pdmr.mrFrPDList.splice(i,1);
							break;
						}
					}
					
					$scope.pdmr.mrFrPDList.push(mrDet);
					$scope.totOAAmt = 0;
					for(var i=0;i<$scope.pdmr.mrFrPDList.length;i++){
						$scope.totOAAmt = $scope.totOAAmt + parseFloat($scope.pdmr.mrFrPDList[i].detAmt);
					}
					
				}else{
					console.log("#############");
					$scope.pdmr.mrFrPDList.push(mrDet);
					/*$('#maAmtId[index]').attr("disabled","disabled");
					$('#saveMrAmtId[index]').attr("disabled","disabled");
					*/
					$scope.totOAAmt = 0;
					for(var i=0;i<$scope.pdmr.mrFrPDList.length;i++){
						$scope.totOAAmt = $scope.totOAAmt + parseFloat($scope.pdmr.mrFrPDList[i].detAmt);
					}
				}	
				console.log("size of $scope.pdmr.mrFrPDList = "+$scope.pdmr.mrFrPDList.length);
			}
		}
	}
	
	
	$scope.removeDetMR = function(index){
		console.log("enter inot removeDetMR function = "+index)
		$scope.pdmr.mrFrPDList.splice(index,1);
		$scope.totOAAmt = 0;
		for(var i=0;i<$scope.pdmr.mrFrPDList.length;i++){
			$scope.totOAAmt = $scope.totOAAmt + parseFloat($scope.pdmr.mrFrPDList[i].detAmt);
		}
	}
	
	
	
	$scope.selectBill = function(){
		console.log("enter into selectBill funciton");
		$scope.selBillFlag = false;
		$('div#selBillId').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Select Bill No",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.selBillFlag = true;
		    }
			});
		
		$('div#selBillId').dialog('open');
	}
	
	
	
	$scope.saveBill = function(bill){
		console.log("enter into saveBill function");
		var duplicate = false;
		$scope.selBill = bill;
		$scope.cnmtList = [];
		$scope.subPdmr.mrFreight=parseFloat(bill.blRemAmt.toFixed(2));
		
		var response = $http.post($scope.projectName+'/getCnmtFrPDMR',$scope.selBill.blBillNo);
		  response.success(function(data, status, headers, config){
			  $log.info(data);
			  if(data.result === "success"){
				$scope.cnmtList = data.list
				
				if($scope.subPdmrList.length > 0){
					for(var i=0;i<$scope.subPdmrList.length;i++){
						if($scope.subPdmrList[i].bill.blBillNo === bill.blBillNo){
							duplicate = true;
							break;
						}
					}
					
					if(duplicate === true){
						$scope.alertToast("You already enter the detail of "+bill.blBillNo);
					}else{
						
						$('div#selBillId').dialog('close');
						$scope.billInfoFlag = false;
						$('div#billInfoId').dialog({
							autoOpen: false,
							modal:true,
							resizable: false,
							title: "Payment Detail For Bill "+bill.blBillNo+" ("+bill.blRemAmt.toFixed(2)+")",
							show: UDShow,
							hide: UDHide,
							position: UDPos,
							draggable: true,
							close: function(event, ui) { 
						        $(this).dialog('destroy');
						        $(this).hide();
						        $scope.billInfoFlag = true;
						    }
							});
						
						$('div#billInfoId').dialog('open');
					}
					
				}else{
					$('div#selBillId').dialog('close');
					$scope.billInfoFlag = false;
					$('div#billInfoId').dialog({
						autoOpen: false,
						modal:true,
						resizable: false,
						title: "Payment Detail For Bill "+bill.blBillNo+" ("+bill.blRemAmt.toFixed(2)+")",
						show: UDShow,
						hide: UDHide,
						position: UDPos,
						draggable: true,
						close: function(event, ui) { 
					        $(this).dialog('destroy');
					        $(this).hide();
					        $scope.billInfoFlag = true;
					    }
						});
					
					$('div#billInfoId').dialog('open');
				}
				
			  }else{
				  $scope.alertToast($scope.selBill.blBillNo+" doesn't have any cnmt")
			  }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
		   });	
		
	}
	
	
	
	$scope.submitBillInfo = function(billInfoForm){
		console.log("enter into submitBillInfo function");
		if(billInfoForm.$invalid){
			$scope.isbillInfoFormSubmitted =true;
		}else{
			$scope.isbillInfoFormSubmitted =false;
			if(angular.isUndefined($scope.subPdmr.mrNetAmt)){
				$scope.subPdmr.mrNetAmt = 0;
			}
			if(angular.isUndefined($scope.subPdmr.mrFreight)){
				$scope.subPdmr.mrFreight = 0;
			}
			if(angular.isUndefined($scope.subPdmr.mrTdsAmt)){
				$scope.subPdmr.mrTdsAmt = 0;
			}
			if(angular.isUndefined($scope.subPdmr.mrDedAmt)){
				$scope.subPdmr.mrDedAmt = 0;
			}
			if(angular.isUndefined($scope.subPdmr.mrAccessAmt)){
				$scope.subPdmr.mrAccessAmt = 0;
			}
			if(angular.isUndefined($scope.subPdmr.mrSrvTaxAmt)){
				$scope.subPdmr.mrSrvTaxAmt = 0;
			}
				
			
			var ded = 0;
			for(var i=0;i<$scope.dedDetailList.length;i++){
				ded = ded + $scope.dedDetailList[i].dedAmt;
			}
			
			if(ded === $scope.subPdmr.mrDedAmt){
				/*var finalAmt = 0.0;
				finalAmt = parseFloat(($scope.subPdmr.mrFreight + $scope.subPdmr.mrSrvTaxAmt + $scope.subPdmr.mrAccessAmt + $scope.subPdmr.mrDedAmt) - $scope.subPdmr.mrTdsAmt);
				
				console.log("finalAmt = "+finalAmt);*/
				
				if(parseFloat($scope.subPdmr.mrFreight + $scope.subPdmr.mrSrvTaxAmt + $scope.subPdmr.mrAccessAmt) === parseFloat($scope.subPdmr.mrTdsAmt + $scope.subPdmr.mrNetAmt + ded)){
		
					 /*if($scope.selBill.blRemAmt < finalAmt){
					$scope.alertToast("Payment detail must be <= "+$scope.selBill.blRemAmt);
				}else{*/
					//$scope.selBill.blRemAmt = finalAmt - $scope.selBill.blRemAmt;
					console.log("Remaining amount of "+$scope.selBill.blBillNo+" = "+$scope.selBill.blRemAmt);
					$scope.subPdmr.bill = $scope.selBill;
					$scope.subPdmr.mrDate = $scope.pdmr.mrDate;
					$scope.subPdmr.mrCustId = $scope.pdmr.mrCustId;
					$scope.subPdmr.mrFrPD = $scope.pdmr.mrFrPD;
					$scope.subPdmr.mrDedDetList = $scope.dedDetailList;
					
					/*console.log("$scope.selOAMR.mrNetAmt = "+$scope.selOAMR.mrNetAmt);
					console.log("$scope.totRecAmt = "+$scope.totRecAmt);
					console.log("$scope.subPdmr.mrNetAmt = "+$scope.subPdmr.mrNetAmt);*/
					
					if(parseFloat($scope.totOAAmt) < parseFloat($scope.totRecAmt + $scope.subPdmr.mrNetAmt)){
						$scope.alertToast("please enter bill detail less than equal to MR amount "+$scope.totOAAmt);
					}else{
						$('div#billInfoId').dialog('close');
						$scope.subPdmrList.push($scope.subPdmr);
						console.log("size of subPdmrList = "+$scope.subPdmrList.length);
						
						$scope.selBill = {};
						$scope.subPdmr = {};
						
						$scope.totRecAmt = 0;
						if($scope.subPdmrList.length > 0){
							for(var i=0;i<$scope.subPdmrList.length;i++){
								$scope.totRecAmt = $scope.totRecAmt + $scope.subPdmrList[i].mrNetAmt;
							}
						} 
					//}
						
					$scope.dedDetailList = [];	
					}
					
					
				}else{
					$scope.alertToast("Please enter complete detail of "+parseFloat($scope.subPdmr.mrFreight + $scope.subPdmr.mrSrvTaxAmt).toFixed(2));
				} 
		
			}else{
				$scope.alertToast("Please enter complete deduction detail of "+$scope.subPdmr.mrDedAmt);
			}
		}
	}
	
	
	$scope.removePDMR = function(index){
		console.log("enter into removePDMR function");
		$scope.subPdmrList.splice(index,1);
		
		$scope.totRecAmt = 0;
		if($scope.subPdmrList.length > 0){
			for(var i=0;i<$scope.subPdmrList.length;i++){
				$scope.totRecAmt = $scope.totRecAmt + $scope.subPdmrList[i].mrNetAmt;
			}
		} 
	}
	
	
	$scope.pdmrSubmit = function(pdmrForm){
		console.log("enter into pdmrSubmit function "+pdmrForm.$invalid);
		if(pdmrForm.$invalid){
			$scope.isFormSubmitted = true;
		}else{
			if($scope.totOAAmt !== $scope.totRecAmt){
				$scope.alertToast("Please enter the complete payment detail of "+$scope.totOAAmt);
				$scope.isFormSubmitted = true;
			}else{
				if($scope.subPdmrList.length > 0){
					$scope.isFormSubmitted = false;
					for(var i=0;i<$scope.subPdmrList.length;i++){
						$scope.subPdmrList[i].mrDesc = $scope.pdmr.mrDesc;
						$scope.subPdmrList[i].mrFrPDList = $scope.pdmr.mrFrPDList;
					}
					$('#saveId').attr("disabled","disabled");
					
					var response = $http.post($scope.projectName+'/submitPDMR',$scope.subPdmrList);
					  response.success(function(data, status, headers, config){
						  if(data.result === "success"){
							  $('#saveId').removeAttr("disabled");
							  $scope.alertToast("Successfully generat MR NO. = "+data.mrNo);
							    $scope.mrList = [];
								$scope.pdmr = {};
								$scope.subPdmr = {};
								$scope.subPdmrList = [];
								$scope.selOAMR = {}
								$scope.blList = [];
								$scope.selBill = {};
								$scope.totRecAmt = 0;
								$scope.totOAAmt = 0;
								$scope.customer = {};
								
								$scope.csDt = $scope.cssDt;
						  }else{
							  $scope.alertToast("SERVER ERROR");
							  $('#saveId').removeAttr("disabled");
						  }
					   });
					   response.error(function(data, status, headers, config) {
							$scope.errorToast(data.result);
					   });	
				}else{
					$scope.isFormSubmitted = true;
					$scope.alertToast("Please enter the payment detail of "+$scope.totOAAmt);
				}
			}
		}
	}
	
	
	$scope.selectCnmt = function(){
		console.log("enter into selectCnmt function");
		$scope.selCnmtFlag = false;
		$('div#selCnmtId').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Select Cnmt",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.selCnmtFlag = true;
		    }
			});
		
		$('div#selCnmtId').dialog('open');
	}
	
	
	$scope.saveCnmt = function(cnmt){
		console.log("enter into saveCnmt function");
		$scope.cnmtCode = cnmt;
		$('div#selCnmtId').dialog('close');
	}
	
	
	$scope.addDedDetail = function(){
		console.log("enter into addDedDetail function");
		if($scope.subPdmr.mrDedAmt > 0){
			$scope.dedDetFlag = false;
			$('div#dedDetId').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				title: "Deduction Detail",
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			        $scope.dedDetFlag = true;
			    }
				});
			
			$('div#dedDetId').dialog('open');
		}else{
			$scope.alertToast("Please enter deduction amount");
		}
	}
	
	
	$scope.submitDedDet = function(dedDetForm){
		console.log("enter into submitDedDet funciton = "+dedDetForm.$invalid);
		if(dedDetForm.$invalid){
			$scope.alertToast("Please fill the correct deduction detail");
			$scope.isdedDetFormSubmitted = true;
		}else{
			$scope.isdedDetFormSubmitted = false;
			if($scope.dedDetailList.length == 0){
				
				if($scope.dedAmt > $scope.subPdmr.mrDedAmt){
					$scope.alertToast("Deduction detail amount can't be greater than "+$scope.subPdmr.mrDedAmt);
				}else{
					var map = {
							"cnmtNo"  : $scope.cnmtCode,
							"dedType" : $scope.dedType,
							"dedAmt"  : $scope.dedAmt
					};
					
					$scope.dedDetailList.push(map);
					$('div#dedDetId').dialog('close');
				}
				
			}else{
				var ded = 0;
				var duplicate = false;
				for(var i=0;i<$scope.dedDetailList.length;i++){
					ded = ded + $scope.dedDetailList[i].dedAmt;
					if($scope.dedDetailList[i].cnmtNo === $scope.cnmtCode && $scope.dedDetailList[i].dedType === $scope.dedType){
						duplicate = true;
						break;
					}
				}
				
				if(duplicate === false){
					ded = ded + $scope.dedAmt;
					
					if(ded > $scope.subPdmr.mrDedAmt){
						$scope.alertToast("Deduction detail amount can't be greater than "+$scope.subPdmr.mrDedAmt);
					}else{
						var map = {
								"cnmtNo"  : $scope.cnmtCode,
								"dedType" : $scope.dedType,
								"dedAmt"  : $scope.dedAmt
						};
						
						$scope.dedDetailList.push(map);
						$('div#dedDetId').dialog('close');
					}
				}else{
					$scope.alertToast("You can't repeat the same deduction detail");
				}
			}
			
			$scope.cnmtCode = "";
			$scope.dedType = "FREIGHT RATE";
			$scope.dedAmt = 0;
		}
	}
	
	
	$scope.removeDed = function(index){
		console.log("enter into removeDed function = "+index);
		$scope.dedDetailList.splice(index,1);
	}
	
	
	
	 if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		 $scope.getPDMrDetail();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 } 
}]);