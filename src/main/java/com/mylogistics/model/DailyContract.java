package com.mylogistics.model;

import java.sql.Date;
import java.util.Calendar;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name = "dailycontract")
public class DailyContract {
	
	@Id
	@Column(name="dlyContId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int dlyContId;
	
	@Column(name="dlyContCode")
	private String dlyContCode;
	
	@Column(name="branchCode")
	private String branchCode;
	
	@Column(name="dlyContBLPMCode")//custcode
	private String dlyContBLPMCode;
	
	@Column(name="dlyContCngrCode")
	private String dlyContCngrCode;
	
	@Column(name="dlyContCrName")
	private String dlyContCrName;
	
	@Column(name="dlyContStartDt")
	private Date dlyContStartDt;
	
	@Column(name="dlyContEndDt")
	private Date dlyContEndDt;
	
	@Column(name="dlyContFromStation")
	private String dlyContFromStation;
	
	@Column(name="dlyContToStation")
	private String dlyContToStation;
	
	@Column(name="dlyContRate")
	private double dlyContRate;
	
	/*@Column(name="dlyContFromWt")
	private double dlyContFromWt;
	
	@Column(name="dlyContToWt")
	private double dlyContToWt;*/
	
	@Column(name="dlyContType")
	private String dlyContType;
	
	/*@Column(name="dlyContProductType")
	private String dlyContProductType;*/
	
	@Column(name="dlyContDc")
	private String dlyContDc;
	
	@Column(name="dlyContCostGrade")
	private String dlyContCostGrade;
	
	/*@Column(name="dlyContTransitDay")
	private int dlyContTransitDay;
	
	@Column(name="dlyContStatisticalCharge")
	private double dlyContStatisticalCharge;
	
	@Column(name="dlyContLoad")
	private double dlyContLoad;
	
	@Column(name="dlyContUnLoad")
	private double dlyContUnLoad;*/
	
	@Column(name="dlyContHourOrDay")
	private String dlyContHourOrDay;
	
	@Column(name="dlyContProportionate")
	private String dlyContProportionate;
	
	@Column(name="dlyContBillBasis")
	private String dlyContBillBasis;
	
	/*@Column(name="dlyContAdditionalRate")
	private double dlyContAdditionalRate;*/
	
	@Column(name="dlyContDdl")
	private String dlyContDdl;
	
	@Column(name="dlyContRemark")
	private String dlyContRemark;
	
	@Column(name="dlyContIsVerify")
	private String dlyContIsVerify;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="dlyContFaCode")
	private String dlyContFaCode;
	
	@Column(name="dlyTransExpBearer")
	private String dlyTransExpBearer;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="isView")
	private boolean isView;
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToOne(cascade = CascadeType.ALL)
    private FAParticular fAParticular;

	public boolean isView() {
		return isView;
	}

	public void setView(boolean isView) {
		this.isView = isView;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public int getDlyContId() {
		return dlyContId;
	}

	public void setDlyContId(int dlyContId) {
		this.dlyContId = dlyContId;
	}

	public String getDlyContCode() {
		return dlyContCode;
	}

	public void setDlyContCode(String dlyContCode) {
		this.dlyContCode = dlyContCode;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getDlyContBLPMCode() {
		return dlyContBLPMCode;
	}

	public void setDlyContBLPMCode(String dlyContBLPMCode) {
		this.dlyContBLPMCode = dlyContBLPMCode;
	}

	public String getDlyContCngrCode() {
		return dlyContCngrCode;
	}

	public void setDlyContCngrCode(String dlyContCngrCode) {
		this.dlyContCngrCode = dlyContCngrCode;
	}

	public String getDlyContCrName() {
		return dlyContCrName;
	}

	public void setDlyContCrName(String dlyContCrName) {
		this.dlyContCrName = dlyContCrName;
	}
	
	public Date getDlyContStartDt() {
		return dlyContStartDt;
	}

	public void setDlyContStartDt(Date dlyContStartDt) {
		this.dlyContStartDt = dlyContStartDt;
	}

	public Date getDlyContEndDt() {
		return dlyContEndDt;
	}

	public void setDlyContEndDt(Date dlyContEndDt) {
		this.dlyContEndDt = dlyContEndDt;
	}

	public String getDlyContFromStation() {
		return dlyContFromStation;
	}

	public void setDlyContFromStation(String dlyContFromStation) {
		this.dlyContFromStation = dlyContFromStation;
	}

	public String getDlyContToStation() {
		return dlyContToStation;
	}

	public void setDlyContToStation(String dlyContToStation) {
		this.dlyContToStation = dlyContToStation;
	}

	public double getDlyContRate() {
		return dlyContRate;
	}

	public void setDlyContRate(double dlyContRate) {
		this.dlyContRate = dlyContRate;
	}

	public String getDlyContType() {
		return dlyContType;
	}

	public void setDlyContType(String dlyContType) {
		this.dlyContType = dlyContType;
	}

	public String getDlyContDc() {
		return dlyContDc;
	}

	public void setDlyContDc(String dlyContDc) {
		this.dlyContDc = dlyContDc;
	}

	public String getDlyContCostGrade() {
		return dlyContCostGrade;
	}

	public void setDlyContCostGrade(String dlyContCostGrade) {
		this.dlyContCostGrade = dlyContCostGrade;
	}

	/*public int getDlyContTransitDay() {
		return dlyContTransitDay;
	}

	public void setDlyContTransitDay(int dlyContTransitDay) {
		this.dlyContTransitDay = dlyContTransitDay;
	}

	public double getDlyContStatisticalCharge() {
		return dlyContStatisticalCharge;
	}

	public void setDlyContStatisticalCharge(double dlyContStatisticalCharge) {
		this.dlyContStatisticalCharge = dlyContStatisticalCharge;
	}

	public double getDlyContLoad() {
		return dlyContLoad;
	}

	public void setDlyContLoad(double dlyContLoad) {
		this.dlyContLoad = dlyContLoad;
	}

	public double getDlyContUnLoad() {
		return dlyContUnLoad;
	}

	public void setDlyContUnLoad(double dlyContUnLoad) {
		this.dlyContUnLoad = dlyContUnLoad;
	}*/

	public String getDlyContHourOrDay() {
		return dlyContHourOrDay;
	}

	public void setDlyContHourOrDay(String dlyContHourOrDay) {
		this.dlyContHourOrDay = dlyContHourOrDay;
	}

	public String getDlyContProportionate() {
		return dlyContProportionate;
	}

	public void setDlyContProportionate(String dlyContProportionate) {
		this.dlyContProportionate = dlyContProportionate;
	}

	public String getDlyContDdl() {
		return dlyContDdl;
	}

	public void setDlyContDdl(String dlyContDdl) {
		this.dlyContDdl = dlyContDdl;
	}

	public String getDlyContRemark() {
		return dlyContRemark;
	}

	public void setDlyContRemark(String dlyContRemark) {
		this.dlyContRemark = dlyContRemark;
	}
	
	public String getDlyContIsVerify() {
		return dlyContIsVerify;
	}

	public void setDlyContIsVerify(String dlyContIsVerify) {
		this.dlyContIsVerify = dlyContIsVerify;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	
	public String getDlyContFaCode() {
		return dlyContFaCode;
	}

	public void setDlyContFaCode(String dlyContFaCode) {
		this.dlyContFaCode = dlyContFaCode;
	}

	public FAParticular getfAParticular() {
		return fAParticular;
	}

	public void setfAParticular(FAParticular fAParticular) {
		this.fAParticular = fAParticular;
	}

	public String getDlyContBillBasis() {
		return dlyContBillBasis;
	}

	public void setDlyContBillBasis(String dlyContBillBasis) {
		this.dlyContBillBasis = dlyContBillBasis;
	}

	public String getDlyTransExpBearer() {
		return dlyTransExpBearer;
	}

	public void setDlyTransExpBearer(String dlyTransExpBearer) {
		this.dlyTransExpBearer = dlyTransExpBearer;
	}
	
	
}
