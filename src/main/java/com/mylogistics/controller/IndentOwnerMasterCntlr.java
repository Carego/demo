package com.mylogistics.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.IndentDAO;
import com.mylogistics.DAO.IndentOwnerMasterDAO;
import com.mylogistics.DAO.StateDAO;
import com.mylogistics.DAO.StationDAO;
import com.mylogistics.model.OwnerMaster;
import com.mylogistics.model.Indent;
import com.mylogistics.model.State;
import com.mylogistics.model.Station;


@Controller
public class IndentOwnerMasterCntlr {
	
	@Autowired
	private IndentOwnerMasterDAO indentOwnerMasterDAO;
	
	@Autowired
	private IndentDAO indentDAO;
	
	@Autowired
	private StateDAO stateDAO;
	
	@Autowired
	private StationDAO stationDAO;
	
	@RequestMapping(value = "/getIndentOwnerMaster", method = RequestMethod.POST)
	public @ResponseBody Object getIndentOwnerMaster(@RequestBody int indentId) {
		System.out.println("enter into getIndentOwnerMaster function"+indentId);
		Map<String,Object> map = new HashMap<>();
		try {
			Indent indent=indentDAO.getIndentById(indentId);
			
			Station station=stationDAO.getStationByStnCode(indent.getToStn());
			
			State state=stateDAO.getStateById(Integer.parseInt(station.getStateCode()));
			
			List<OwnerMaster> ownerMasterList=indentOwnerMasterDAO.getIndentOwnerMaster(indent.getBranchName(), state.getStateName(),state.getStateZone(), indent.getVehicleType());
			map.put("ownerMasterList", ownerMasterList);
			map.put("result", "success");
			
		}catch(Exception e) {
			e.printStackTrace();
			map.put("result", "error");
		}
		
		return map;
	}
	
	

}
