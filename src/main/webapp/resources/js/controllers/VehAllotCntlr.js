'use strict';

var app = angular.module('application');

app.controller('VehAllotCntlr',['$scope','$location','$http','$filter','$window',
                                  function($scope,$location,$http,$filter,$window){
	
	$scope.branchList = [];
	$scope.empList = [];
	$scope.ownAddList = [];
	
	$scope.vms = {};
	
	$scope.branchCodeDBFlag = true;
	$scope.empCodeDBFlag = true;
	$scope.rtoAddDBFlag = true;
	$scope.newRtoAddDBFlag = true;
	$scope.submitVhFlag = true;
	$scope.ownAddDBFlag = true;
	$scope.newOwnAddDBFlag = true;
	
	$scope.showRTOAdd = false;
	$scope.showOwnAddFlag = false;
	
	var max = 15;
	
	$('#vehModelId').keypress(function(key) {
        if(key.charCode < 46 || key.charCode > 57)
        	return false;
    });
	
	$('#vehModelId').keypress(function(e) {
     	if((this.value.length == 4)){
             e.preventDefault();
         } 
    });
	
	$('#vehInsPremId').keypress(function(key) {
        if(key.charCode < 46 || key.charCode > 57)
        	return false;
    });
	
	$('#vehInsPremId').keypress(function(e) {
     	if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == max)){
             e.preventDefault();
         } 
    });
	
	$('#vehTaxAmtId').keypress(function(key) {
        if(key.charCode < 46 || key.charCode > 57)
        	return false;
    });
	
	$('#vehTaxAmtId').keypress(function(e) {
     	if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == max)){
             e.preventDefault();
         } 
    });
	
	
	
	
	$scope.getBranchList = function(){
		console.log("enter into getBranchList function");
		var response = $http.post($scope.projectName+'/getBrListFrVehA');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.branchList = data.brList;
				$scope.getOwnAdd();
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config){
			$scope.alertToast(data);
		});
	}
	
	
	$scope.getOwnAdd = function(){
		console.log("enter into getOwnAdd function");
		var response = $http.post($scope.projectName+'/getOwnAddFrVehA');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.ownAddList = data.list;
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config){
			$scope.alertToast(data);
		});
	}
	
	
	$scope.openBranchDB = function(){
		console.log("enter into openBranchDB function");
		$scope.branchCodeDBFlag = false;
    	$('div#branchCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Branch List",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		$('div#branchCodeDB').dialog('open');
	}
	
	
	$scope.saveBrCode = function(branch){
		console.log("enter into saveBrCode function-->"+branch.branchFaCode);
		$scope.vms.branch = branch;
		$scope.branchCodeDBFlag = true;
		$('div#branchCodeDB').dialog('close');
		
		var response = $http.post($scope.projectName+'/getEmpListFrVehA',branch);
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.empList = data.empList;
				$scope.getRtoAddList();
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config){
			$scope.alertToast(data);
		});	
	}
	
	
	$scope.getRtoAddList = function(){
		console.log("enter into getRtoAdd function");
		var response = $http.post($scope.projectName+'/getRtoAddList');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.rtoAddList = data.list;
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config){
			$scope.alertToast(data);
		});	
	}
	
	
	$scope.openEmpDB = function(){
		if(angular.isUndefined($scope.vms.branch)){
			$scope.alertToast("Please select any branch");
		}else{
			console.log("enter into openEmpDB function");
			$scope.empCodeDBFlag = false;
	    	$('div#empCodeDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				title: "Employee List",
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			    }
				});
			$('div#empCodeDB').dialog('open');
		}
	}
	
	
	$scope.saveEmpCode = function(emp){
		console.log("enter into saveEmpCode function");
		$scope.vms.employee = emp;
		$scope.empCodeDBFlag = true;
		$('div#empCodeDB').dialog('close');
	}
	
	
	$scope.selectRTO = function(){
		console.log("enter into selectRTO function");
		
		$scope.rtoAddDBFlag = false;
    	$('div#rtoAddDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Employee List",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		$('div#rtoAddDB').dialog('open');
	}
	
	
	$scope.saveRTO = function(rtoAdd){
		console.log("enter into saveRTO function");
		$scope.vms.address = rtoAdd;
		$scope.showRTOAdd = true;
		$scope.rtoAddDBFlag = true;
		$('div#rtoAddDB').dialog('close');
	}
	
	
	$scope.newRTOAdd = function(){
		console.log("enter into newRTOAdd function");
		$scope.newRtoAddDBFlag = false;
    	$('div#newRtoAddDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Employee List",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		$('div#newRtoAddDB').dialog('open');
	}
	
	
	$scope.submitNewRto = function(newRTOForm,add){
		console.log("enter into submitNewRto function = "+newRTOForm.$invalid);
		if(newRTOForm.$invalid){
			$scope.alertToast("please fill correct address");
		}else{
			$('div#newRtoAddDB').dialog('close');
			$scope.newRtoAddDBFlag = true;
			$scope.alertToast("success");
			$scope.vms.address = add;
			$scope.vms.address.addId = 0;
			$scope.showRTOAdd = true;
		}
	}
	
	
	$scope.submitVehAllot = function(newVehAllotForm){
		console.log("enter into submitVehicle function = "+newVehAllotForm.$invalid);
		if(newVehAllotForm.$invalid){
			$scope.alertToast("please fill correct form");
		}else{
			$scope.submitVhFlag = false;
	    	$('div#submitVhDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				title: "Employee List",
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			    }
				});
			$('div#submitVhDB').dialog('open');	
		}
	}
	
	
	$scope.back = function(){
		console.log("enter into back function");
		$scope.submitVhFlag = true;
		$('div#submitVhDB').dialog('close');	
	}
	
	
	$scope.saveVms = function(){
		console.log("enter into saveVms function");
		var response = $http.post($scope.projectName+'/saveVms',$scope.vms);
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.submitVhFlag = true;
				$('div#submitVhDB').dialog('close');
				$scope.alertToast(data.result);
				$scope.vms = {};
				$scope.branchList = [];
				$scope.empList = [];
				$scope.showRTOAdd = false;
				$scope.showOwnAddFlag = false;
				$scope.getBranchList();
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config){
			$scope.alertToast(data);
		});	
	}
	
	
	
	$scope.selectOwnAdd = function(){
		console.log("enter into selectOwnAdd function");
		$scope.ownAddDBFlag = false;
    	$('div#ownAddDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Employee List",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		$('div#ownAddDB').dialog('open');	
	}
	
	
	$scope.saveOwnAdd = function(ownAdd){
		console.log("enter into saveOwnAdd function");
		$scope.ownAddDBFlag = true;
		$('div#ownAddDB').dialog('close');
		$scope.vms.ownAdd = ownAdd; 
	}
	
	
	$scope.addNewOwnAdd = function(){
		console.log("enter into addNewOwnAdd function");
		$scope.newOwnAddDBFlag = false;
    	$('div#newOwnAddDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Employee List",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		$('div#newOwnAddDB').dialog('open');	
	}
	
	
	
	$scope.submitNewOwnAdd = function(newOwnAddForm,add1){
		console.log("enter into submitNewOwnAdd function = "+newOwnAddForm.$invalid);
		if(newOwnAddForm.$invalid){
			$scope.alertToast("please fill correct address");
		}else{
			$scope.newOwnAddDBFlag = true;
			$('div#newOwnAddDB').dialog('close');
			$scope.vms.ownAdd = add1; 
			$scope.vms.ownAdd.addId = 0;
			$scope.showOwnAddFlag = true;
		}
	}
	
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		 $scope.getBranchList();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 }
}]);