package com.mylogistics.exception.custom;

public class CustomerException extends Exception {

	private static final long serialVersionUID = 3739710791191712648L;

	public CustomerException (String exceptionMsg) {
		super(exceptionMsg);
	}
	
}