package com.mylogistics.controller.report;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.mylogistics.DAO.report.ReportDAO;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;

@Controller
public class TrialCntlr {
	
	@Autowired
	private ReportDAO reportDAO;
	
	private List<Map<String, Object>> trialMapList = new ArrayList<>();
	private List<Map<String, Object>> faCodeNameList = new ArrayList<>();
	private Map<String, Object> trialRepMap = new HashMap<>();
	private double cashInClosing = 0.0;
	
	private static final String TAG = "DirectUrlCntlr: ";
	private static final String AUTH = "authorized";
	private static final String UN_AUTH = "unAuthorized";
	
	@Autowired
	HttpSession httpSession;
	
	public String authentication(){
		System.out.println("---------enter into authentication function");
		User currentUser = (User)httpSession.getAttribute("currentUser");

		if(currentUser == null){
			System.out.println("current user is null");
			return UN_AUTH;
		}else{
			System.out.println("current user authentication token ="+currentUser.getUserAuthToken());
			System.out.println("current session id ="+httpSession.getId());
			if(currentUser.getUserAuthToken().equalsIgnoreCase(httpSession.getId())){
				System.out.println("authorized user set in session");
				return AUTH;
			}else{
				System.out.println("unAuthorized user set in session");
				return UN_AUTH;
			}
		}
	}
	
	@RequestMapping(value = "/getFinYears", method = RequestMethod.POST)
	public @ResponseBody Object getFinYears() {
		System.out.println("enter into getFinYears funciton");
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		List<String> finYearList = reportDAO.getFinYears();
		
		if (!finYearList.isEmpty()) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("finYearList", finYearList);
		}else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/getTrialReport", method = RequestMethod.POST)
	public @ResponseBody Object getLedgerReportHtml(@RequestBody Map<String, Object> trialReportMap) {
		System.out.println("enter into getTrialReport funciton");
		
		trialRepMap = trialReportMap;
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		Map<String, Object> trialMapNCloseBalMap = new HashMap<>();
		
		trialMapList = new ArrayList<>();
		
		if ((boolean) trialReportMap.get("isBranch")) {
			trialMapNCloseBalMap = reportDAO.getBrTrial(trialReportMap);
			trialMapList = (List<Map<String, Object>>) trialMapNCloseBalMap.get("trialMapList");
			cashInClosing = (double) trialMapNCloseBalMap.get("cashInClosing");
		}else if ((boolean) trialReportMap.get("isConsolidate")) {
			trialMapNCloseBalMap = reportDAO.getConsolidateTrial(trialReportMap);
			trialMapList = (List<Map<String, Object>>) trialMapNCloseBalMap.get("trialMapList");
			cashInClosing = (double) trialMapNCloseBalMap.get("cashInClosing");
		}
		
		if(!trialMapList.isEmpty()){
			Map<Object, Object> faCodeNameMap = new HashMap<>();
			
			List<String> faCode = new ArrayList<>();
			for (Map<String, Object> trialMap : trialMapList) {
				faCode.add(String.valueOf(trialMap.get("csFaCode")));
				if (!trialMap.containsKey(trialMap.get("csFaCode"))) {
					faCodeNameMap.put(trialMap.get("csFaCode"), trialMap.get("csFaName"));
				}
			}
					
			Set<String> hs = new HashSet<>();
			hs.addAll(faCode);
			faCode.clear();
			faCode.addAll(hs);
			
			Collections.sort(faCode);
			
			System.err.println("****");
			System.out.println("FaCodeNameMap = "+faCodeNameMap);
			System.out.println("FaCode = "+faCode);
			System.err.println("****");
			
			
			faCodeNameList = new ArrayList<>();
			for (int i = 0; i < faCode.size(); i++) {
				Map<String, Object> tempMap = new HashMap<>();
				
				double totalDr = 0.00;
				double totalCr = 0.00;
				double total = 0.00;
				double balance = 0.00;
				String balanceType = null;
				
				for (Map<String, Object> trialMap : trialMapList) {
					 if (faCode.get(i).equalsIgnoreCase(String.valueOf(trialMap.get("csFaCode")))) {
						if (String.valueOf(trialMap.get("csDrCr")).equalsIgnoreCase("D")) {
							totalDr = totalDr + (double)trialMap.get("csAmt");
						} else if (String.valueOf(trialMap.get("csDrCr")).equalsIgnoreCase("C")) {
							totalCr = totalCr + (double)trialMap.get("csAmt");
						}
						if (String.valueOf(trialMap.get("csFaCode")).equalsIgnoreCase("0300126")) {
							System.out.print(trialMap.get("csDrCr")+"\t");
							System.out.println("amt: "+trialMap.get("csAmt"));
						}
					} 
				}
				
				if (totalDr > totalCr) {
					balance = totalDr - totalCr;
					balanceType = "D";
					total = totalDr;
				} else if (totalDr < totalCr) {
					balance = totalCr - totalDr;
					balanceType = "C";
					total = totalCr;
				} else if (totalDr == totalCr) {
					balance = totalCr - totalDr;
					balanceType = "equal";
					total = totalDr;//or can be credit
				}
				
//				sumDr = sumDr+totalDr;
//				sumCr = sumCr+totalCr;
				
				tempMap.put("csFaCode", faCode.get(i));
				tempMap.put("csFaName", String.valueOf(faCodeNameMap.get(faCode.get(i))));
				
				System.err.println("CsFaName = "+tempMap.get("csFaName"));
				
				
				balance = Math.round(balance*100.0)/100.0;
				
				tempMap.put("csAmtBalance", balance);
				tempMap.put("csAmtTotal", total);
				tempMap.put("csDrCrBalance", balanceType);
				if (!balanceType.equalsIgnoreCase("equal")) {
					faCodeNameList.add(tempMap);
				} else if (String.valueOf(tempMap.get("csFaCode")).equalsIgnoreCase("1110057")){
					faCodeNameList.add(tempMap);
				}
				
			}		
			
			
			//add sundry debtors
			if (!faCodeNameList.isEmpty()) {
				for (int i = 0; i < faCodeNameList.size(); i++) {
					if (String.valueOf(faCodeNameList.get(i).get("csFaCode")).substring(0, 2).equalsIgnoreCase("03")) {
						//add customer to sundry debtors fa code
						//System.out.println("==========>> customerFa: "+faCodeNameList.get(i).get("csFaCode")+"\tTYPE: "+faCodeNameList.get(i).get("csDrCrBalance")+"\tAmt:"+faCodeNameList.get(i).get("csAmtBalance"));
						for (int j = 0; j < faCodeNameList.size(); j++) {
							if (String.valueOf(faCodeNameList.get(j).get("csFaCode")).equalsIgnoreCase("1110057")) {
								System.out.println("I am in If");
								if (String.valueOf(faCodeNameList.get(i).get("csDrCrBalance")).equalsIgnoreCase("D")) {//add
									faCodeNameList.get(j).put("csAmtBalance", Double.parseDouble(String.valueOf(faCodeNameList.get(j).get("csAmtBalance"))) +
											Double.parseDouble(String.valueOf(faCodeNameList.get(i).get("csAmtBalance"))));
								} else if (String.valueOf(faCodeNameList.get(i).get("csDrCrBalance")).equalsIgnoreCase("C")){
									faCodeNameList.get(j).put("csAmtBalance", Double.parseDouble(String.valueOf(faCodeNameList.get(j).get("csAmtBalance"))) -
											Double.parseDouble(String.valueOf(faCodeNameList.get(i).get("csAmtBalance"))));
								}
								
								if (Double.parseDouble(String.valueOf(faCodeNameList.get(j).get("csAmtBalance")))>0) {
									//nature is dr
									faCodeNameList.get(j).put("csDrCrBalance", "D");
								} else {
									//nature is cr
									faCodeNameList.get(j).put("csDrCrBalance", "C");
								}
								//System.out.print("before: "+new BigDecimal(String.valueOf(faCodeNameList.get(j).get("csAmtBalance"))).toPlainString()+"balance Amt: "+ new BigDecimal(String.valueOf(faCodeNameList.get(i).get("csAmtBalance"))).toPlainString());
								//System.out.println("after: "+ new BigDecimal(String.valueOf(faCodeNameList.get(j).get("csAmtBalance"))).toPlainString());
								break;
							} else {
								//System.out.println("I am in else");
							}
						}
						faCodeNameList.remove(i);
						i=i-1;
					}
				}
			}
			
			//System.out.println("add all cust as sundry debtors");
			
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("faCodeNameList", faCodeNameList);
			//map.put("trialReportMap", trialRepMap);
			map.put("cashInClosing", cashInClosing);
			
			//uncomment to send data at client 
			/*map.put("ledgerMapList",trialMapList);
			map.put("faCodeNameList", faCodeNameList);
			map.put("cashInClosing", cashInClosing);*/
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		
		/*System.out.println("Dr sum: "+sumDr);
		System.out.println("Cr sum: "+sumCr);*/
		
		return map;
	}
	
	@RequestMapping(value = "/printTrialReportPdf")
	public ModelAndView getTrialReportPdf() {
		System.out.println("enter into printTrialReportPdf funciton");
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		/*map.put("trialMapList",trialMapList);*/
		map.put("faCodeNameList", faCodeNameList);
		map.put("trialReportMap", trialRepMap);
		map.put("cashInClosing", cashInClosing);
		
		String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return new ModelAndView("sessionExpired", null);
		else	
			return new ModelAndView("trialReportPdfView", map);
	}
}
