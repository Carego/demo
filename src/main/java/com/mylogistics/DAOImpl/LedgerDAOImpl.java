package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.NumberUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mylogistics.DAO.LedgerDAO;
import com.mylogistics.constants.CashStmtCNTS;
import com.mylogistics.constants.FAMasterCNTS;
import com.mylogistics.model.Branch;
import com.mylogistics.model.CashStmt;
import com.mylogistics.model.FAMaster;
import com.mylogistics.model.MoneyReceipt;
import com.mylogistics.services.CodePatternService;

@Repository
public class LedgerDAOImpl implements LedgerDAO {

	public static final Logger logger = Logger.getLogger(LedgerDAOImpl.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public List<Map<String, Object>> getBrLedger(Map<String, Object> ledgerReportMap) {
		
		List<Map<String, Object>> ledgerMapList = new ArrayList<>();
		Map<String, String> faCodeNameMap = new HashMap<>();
		Session session = this.sessionFactory.openSession();
		try {
			Criteria cr = session.createCriteria(FAMaster.class)
					.add(Restrictions.ge(FAMasterCNTS.FA_MFA_CODE, ledgerReportMap.get("fromFaCode")))
					.add(Restrictions.le(FAMasterCNTS.FA_MFA_CODE, ledgerReportMap.get("toFaCode")));
			List<FAMaster> faMasterList = cr.list();
			
			for (FAMaster faMaster : faMasterList) {
				faCodeNameMap.put(faMaster.getFaMfaCode(), faMaster.getFaMfaName());
			}
			
			
			cr = session.createCriteria(CashStmt.class)
					.add(Restrictions.ne(CashStmtCNTS.CS_FA_CODE, "1110131"))
					.add(Restrictions.ge(CashStmtCNTS.CS_FA_CODE,ledgerReportMap.get("fromFaCode")))
					.add(Restrictions.le(CashStmtCNTS.CS_FA_CODE,ledgerReportMap.get("toFaCode")))
					.createAlias("csNo", "csNo")
					.add(Restrictions.eq("csNo.bCode", String.valueOf(ledgerReportMap.get("branchId"))))
					.add(Restrictions.ge("csNo.cssDt", CodePatternService.getSqlDate(String.valueOf(ledgerReportMap.get("fromDt")))))
					.add(Restrictions.le("csNo.cssDt", CodePatternService.getSqlDate(String.valueOf(ledgerReportMap.get("toDt")))))
					.addOrder(Order.asc("csNo.cssDt"));
			
			/*cr = session.createCriteria(CashStmt.class)
					.add(Restrictions.ne(CashStmtCNTS.CS_FA_CODE, "1110131"))
					.add(Restrictions.sqlRestriction("CAST(csFaCode AS UNSIGNED) >= "
						+Integer.parseInt(String.valueOf(ledgerReportMap.get("fromFaCode")))+" AND CAST(csFaCode AS UNSIGNED) "
						+ "<= "+Integer.parseInt(String.valueOf(ledgerReportMap.get("toFaCode")))+""))
					.createAlias("csNo", "csNo")
					.add(Restrictions.eq("csNo.bCode", String.valueOf(ledgerReportMap.get("branchId"))))
					.add(Restrictions.ge("csNo.cssDt", CodePatternService.getSqlDate(String.valueOf(ledgerReportMap.get("fromDt")))))
					.add(Restrictions.le("csNo.cssDt", CodePatternService.getSqlDate(String.valueOf(ledgerReportMap.get("toDt")))))
					.addOrder(Order.asc("csNo.cssDt"));*/
			
			List<CashStmt> cashStmtList = cr.list();
			logger.info("Cashstmt size = "+cashStmtList.size());
			
			for (CashStmt cashStmt : cashStmtList) {
				Map<String, Object> map = new HashMap<>();
				map.put(CashStmtCNTS.CS_FA_CODE, cashStmt.getCsFaCode());
				map.put("csFaName", faCodeNameMap.get(cashStmt.getCsFaCode()));
				map.put(CashStmtCNTS.CS_DT, cashStmt.getCsDt());
				map.put(CashStmtCNTS.CS_VOUCH_NO, cashStmt.getCsVouchNo());				
				map.put(CashStmtCNTS.CS_DESCRIPTION, cashStmt.getCsDescription());				
				map.put(CashStmtCNTS.CS_DR_CR, cashStmt.getCsDrCr());				
				map.put(CashStmtCNTS.CS_AMT, Math.round(cashStmt.getCsAmt()*100.0)/100.0);
				map.put(CashStmtCNTS.CS_VOUCH_TYPE, cashStmt.getCsVouchType());
				map.put(CashStmtCNTS.CS_TYPE, cashStmt.getCsType());				
				map.put(CashStmtCNTS.CS_TV_NO, cashStmt.getCsTvNo());
				map.put(CashStmtCNTS.PAY_MODE, cashStmt.getPayMode());				
				
				map.put("branchId", String.valueOf(ledgerReportMap.get("branchId")));
				map.put("branchName", String.valueOf(ledgerReportMap.get("branchName")));	
				
				ledgerMapList.add(map);
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		} finally {
			session.close();
		}		
		return ledgerMapList;
	}
	
	public Map<String, MoneyReceipt> getMoneyReceiptByBill(List<Map<String, Object>> ledgerMapListTemp) {
		Map<String, MoneyReceipt> mrMap = new HashMap<String, MoneyReceipt>();
		List<String> billNoList = new ArrayList<String>();
		Iterator it = ledgerMapListTemp.iterator();
		while(it.hasNext()){
			Map map = (Map)it.next();
			String csType = String.valueOf(map.get("csType"));
			String csVouchType = String.valueOf(map.get("csVouchType"));
			String csTvNo = String.valueOf(map.get("csTvNo"));
			if(csType.equalsIgnoreCase("Bill") && csVouchType.equalsIgnoreCase("CONTRA")){
				if(! NumberUtils.isNumber(csTvNo)){
					billNoList.add(csTvNo);
				}
			}
		}
		if (! billNoList.isEmpty()) {
			Session session = sessionFactory.openSession();
			Criteria mrCriteria = session.createCriteria(MoneyReceipt.class);
			mrCriteria.createAlias("bill", "bl");
			mrCriteria.add(Restrictions.in("bl.blBillNo", billNoList));
			mrCriteria.setFetchMode("bill", FetchMode.EAGER);
			List<MoneyReceipt> mrList = mrCriteria.list();
			if (! mrList.isEmpty()) {
				for(MoneyReceipt mr : mrList)
					mrMap.put(mr.getBill().getBlBillNo(), mr);
			}
			session.close();
		}
		return mrMap;
	}
	
	public List<Map<String, Object>> getBrLedgerFinalList(List<Map<String, Object>> ledgerMapListTemp) {

		List<Map<String, Object>> finalList = new ArrayList();
		Map<String, MoneyReceipt> mrMap = getMoneyReceiptByBill(ledgerMapListTemp);
		Session session = this.sessionFactory.openSession();
		try{
			Iterator it = ledgerMapListTemp.iterator();
			while(it.hasNext()){
				Map<String, Object> map = (Map)it.next();
				
				String csType = String.valueOf(map.get("csType"));
				String csVouchType = String.valueOf(map.get("csVouchType"));
				String csTvNo = String.valueOf(map.get("csTvNo"));
				String payMode = null;
				Object payObj = map.get("payMode");
				if(payObj != null)
					payMode = String.valueOf(payObj);				
				if(csType.equalsIgnoreCase("Bill") && csVouchType.equalsIgnoreCase("CONTRA")){					
					if(! NumberUtils.isNumber(csTvNo)){
						MoneyReceipt realMr = mrMap.get(csTvNo);
						if(realMr != null){				
							if(realMr.getMrPayBy() == 'Q'){
								map.put("chq", realMr.getMrChqNo());
								map.put("csPayMode", "CHEQUE");
							}else if(realMr.getMrPayBy() == 'C'){
								map.put("chq", "");
								map.put("csPayMode", "CASH");
							}else if(realMr.getMrPayBy() == 'R'){								
								map.put("chq", realMr.getMrRtgsRefNo());								
								map.put("csPayMode", "RTGS");
							}else{
								map.put("chq", "");
								map.put("csPayMode", "");
							}
						}else{
							map.put("chq", "");
							map.put("csPayMode", "");							
						}
					}else{
						map.put("chq", "");
						map.put("csPayMode", "");					
					}
				}else{
					if(payMode != null){					
						if(payMode.equalsIgnoreCase("C")){						
							map.put("chq", "");
							map.put("csPayMode", "CASH");
						}else if(payMode.equalsIgnoreCase("Q")){							
							map.put("csPayMode", "CHEQUE");
							if(csTvNo.contains("(")){						
								map.put(CashStmtCNTS.CS_TV_NO, csTvNo.substring(0, csTvNo.indexOf("(")));
								map.put("chq",csTvNo.substring(csTvNo.indexOf("(")+1, csTvNo.length()-1));							
							}else{
								map.put(CashStmtCNTS.CS_TV_NO, csTvNo);
								map.put("chq", csTvNo);
							}
						}else if(payMode.equalsIgnoreCase("R")){							
							map.put("csPayMode", "RTGS");							
							String cstvNo = String.valueOf(map.get("csTvNo"));
							if(csTvNo.contains("(")){
								map.put("chq", cstvNo.substring(cstvNo.indexOf("(")+1, cstvNo.length()-1));
								map.put(CashStmtCNTS.CS_TV_NO, cstvNo.substring(0, csTvNo.indexOf("(")));
							}else{
								map.put("chq", cstvNo);
							}
								
						}else if(payMode.equalsIgnoreCase("O")){
							map.put("csPayMode", "O");							
							map.put("chq", "");
						}
					}else{					
						map.put("chq", "");
						map.put("csPayMode", "");
					}
				}
				finalList.add(map);
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.info("Exception in getBrLedgerFinalList "+e);
		}finally{
			session.clear();
			session.close();
		}
		return finalList;
	}
	
	public List<Map<String, Object>> getConsolidateLedgerFinalList(List<Map<String, Object>> ledgerMapListTemp) {

		List<Map<String, Object>> finalList = new ArrayList();
		Map<String, MoneyReceipt> mrMap = getMoneyReceiptByBill(ledgerMapListTemp);
		
		Session session = this.sessionFactory.openSession();
		try{
			Iterator it = ledgerMapListTemp.iterator();
			while(it.hasNext()){
				Map<String, Object> map = (Map)it.next();
				
				String csType = String.valueOf(map.get("csType"));
				String csVouchType = String.valueOf(map.get("csVouchType"));
				String csTvNo = String.valueOf(map.get("csTvNo"));
				String payMode = null;
				Object payObj = map.get("payMode");
				if(payObj != null)
					payMode = String.valueOf(payObj);				
				if(csType.equalsIgnoreCase("Bill") && csVouchType.equalsIgnoreCase("CONTRA")){					
					if(! NumberUtils.isNumber(csTvNo)){
						MoneyReceipt realMr = mrMap.get(csTvNo);				
						if(realMr != null){							
							if(realMr.getMrPayBy() == 'Q'){
								map.put("chq", realMr.getMrChqNo());
								map.put("csPayMode", "CHEQUE");
							}else if(realMr.getMrPayBy() == 'C'){
								map.put("chq", "");
								map.put("csPayMode", "CASH");
							}else if(realMr.getMrPayBy() == 'R'){
								map.put("chq", realMr.getMrRtgsRefNo());								
								map.put("csPayMode", "RTGS");
							}else{
								map.put("chq", "");
								map.put("csPayMode", "");
							}
						}else{
							map.put("chq", "");
							map.put("csPayMode", "");							
						}
					}else{
						map.put("chq", "");
						map.put("csPayMode", "");					
					}
				}else{
					if(payMode != null){					
						if(payMode.equalsIgnoreCase("C")){						
							map.put("chq", "");
							map.put("csPayMode", "CASH");
						}else if(payMode.equalsIgnoreCase("Q")){							
							map.put("csPayMode", "CHEQUE");
							if(csTvNo.contains("(")){						
								map.put(CashStmtCNTS.CS_TV_NO, csTvNo.substring(0, csTvNo.indexOf("(")));
								map.put("chq",csTvNo.substring(csTvNo.indexOf("(")+1, csTvNo.length()-1));							
							}else{
								map.put(CashStmtCNTS.CS_TV_NO, csTvNo);
								map.put("chq", csTvNo);
							}
						}else if(payMode.equalsIgnoreCase("R")){							
							map.put("csPayMode", "RTGS");							
							String cstvNo = String.valueOf(map.get("csTvNo"));
							if(csTvNo.contains("(")){
								map.put("chq", cstvNo.substring(cstvNo.indexOf("(")+1, cstvNo.length()-1));
								map.put(CashStmtCNTS.CS_TV_NO, cstvNo.substring(0, csTvNo.indexOf("(")));
							}else{
								map.put("chq", "");
							}
						}
					}else{					
						map.put("chq", "");
						map.put("csPayMode", "");
					}
				}
				finalList.add(map);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			session.clear();
			session.close();
		}
		return finalList;
	
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getConsolidateLedger(Map<String, Object> ledgerReportMap) {
		
		List<Map<String, Object>> ledgerMapList = new ArrayList<>();
		Map<String, String> faCodeNameMap = new HashMap<>();
		Map<Integer, String> brIdNameMap = new HashMap<>();
		Session session = this.sessionFactory.openSession();
		
		try {
			Criteria cr = session.createCriteria(FAMaster.class)
					.add(Restrictions.ge(FAMasterCNTS.FA_MFA_CODE, ledgerReportMap.get("fromFaCode")))
					.add(Restrictions.le(FAMasterCNTS.FA_MFA_CODE, ledgerReportMap.get("toFaCode")));
			List<FAMaster> faMasterList = cr.list();
			
			for (FAMaster faMaster : faMasterList) {
				faCodeNameMap.put(faMaster.getFaMfaCode(), faMaster.getFaMfaName());
			}
			
			cr = session.createCriteria(Branch.class);
			List<Branch> branchList = cr.list();
			
			for (Branch branch : branchList) {
				brIdNameMap.put(branch.getBranchId(), branch.getBranchName());
			}
			
			
			cr = session.createCriteria(CashStmt.class)
					.add(Restrictions.ne(CashStmtCNTS.CS_FA_CODE, "1110131"))
					.add(Restrictions.ge(CashStmtCNTS.CS_FA_CODE,ledgerReportMap.get("fromFaCode")))
					.add(Restrictions.le(CashStmtCNTS.CS_FA_CODE,ledgerReportMap.get("toFaCode")))
					.createAlias("csNo", "csNo")
					.add(Restrictions.ge("csNo.cssDt", CodePatternService.getSqlDate(String.valueOf(ledgerReportMap.get("fromDt")))))
					.add(Restrictions.le("csNo.cssDt", CodePatternService.getSqlDate(String.valueOf(ledgerReportMap.get("toDt")))))
					.addOrder(Order.asc("csNo.cssDt"));
			
			
			/*cr = session.createCriteria(CashStmt.class)
					.add(Restrictions.ne(CashStmtCNTS.CS_FA_CODE, "1110131"))
					.add(Restrictions.sqlRestriction("CAST(csFaCode AS UNSIGNED) >= "
						+Integer.parseInt(String.valueOf(ledgerReportMap.get("fromFaCode")))+" AND CAST(csFaCode AS UNSIGNED) "
						+ "<= "+Integer.parseInt(String.valueOf(ledgerReportMap.get("toFaCode")))+""))
					.createAlias("csNo", "csNo")
					.add(Restrictions.ge("csNo.cssDt", CodePatternService.getSqlDate(String.valueOf(ledgerReportMap.get("fromDt")))))
					.add(Restrictions.le("csNo.cssDt", CodePatternService.getSqlDate(String.valueOf(ledgerReportMap.get("toDt")))))
					.addOrder(Order.asc("csNo.cssDt"));*/
			
			List<CashStmt> cashStmtList =  cr.list();
			
			System.out.println("CashStmtList Size : "+cashStmtList.size());
			
			for (CashStmt cashStmt : cashStmtList) {				
				Map<String, Object> map = new HashMap<>();
				map.put(CashStmtCNTS.CS_FA_CODE, cashStmt.getCsFaCode());
				map.put("csFaName", faCodeNameMap.get(cashStmt.getCsFaCode()));
				map.put(CashStmtCNTS.CS_DT, cashStmt.getCsDt());
				map.put(CashStmtCNTS.CS_VOUCH_NO, cashStmt.getCsVouchNo());
				map.put(CashStmtCNTS.CS_VOUCH_TYPE, cashStmt.getCsVouchType());
				map.put(CashStmtCNTS.CS_TYPE, cashStmt.getCsType());
				map.put(CashStmtCNTS.CS_DESCRIPTION, cashStmt.getCsDescription());	
				map.put(CashStmtCNTS.CS_DR_CR, cashStmt.getCsDrCr());		
				map.put(CashStmtCNTS.CS_AMT, Math.round(cashStmt.getCsAmt()*100.0)/100.0);
				map.put(CashStmtCNTS.CS_TV_NO, cashStmt.getCsTvNo());
				map.put(CashStmtCNTS.PAY_MODE, cashStmt.getPayMode());
				
				map.put("branchId", cashStmt.getbCode());
				map.put("branchName", brIdNameMap.get(cashStmt.getbCode()));		
				
				ledgerMapList.add(map);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Exception in consolidate ledger "+e);
		} finally{
			session.close();				
		}
		return ledgerMapList;
	}
	
}