package com.mylogistics.constants.webservice;

public class VehicleAppCNTS {

	public static final String V_ID = "vId";
	public static final String V_OWN_CODE = "vOwnCode";
	public static final String V_OWN_NAME = "vOwnName";
	public static final String V_BRK_CODE = "vBrkCode";
	public static final String V_BRK_NAME = "vBrkName";
	public static final String V_TYPE_CODE = "vTypeCode";
	public static final String V_TYPE = "vType";
	public static final String V_RC_NO = "vRcNo";
	public static final String V_RC_ISSUE_DT = "vRcIssueDt";
	public static final String V_RC_VALID_DT = "vRcValidDt";
	public static final String V_DRIVER_NAME = "vDriverName";
	public static final String V_DRIVER_DL_NO = "vDriverDlNo";
	public static final String V_DRIVER_DL_ISSUE_DT = "vDriverDlIssueDt";
	public static final String V_DRIVER_DL_VALID_DT = "vDriverDlValidDt";
	public static final String V_DRIVER_MOB_NO = "vDriverMobNo";
	public static final String V_PERMIT_NO = "vPermitNo";
	public static final String V_PERMIT_ISSUE_DT = "vPermitIssueDt";
	public static final String V_PERMIT_VALID_DT = "vPermitValidDt";
	public static final String V_PERMIT_STATE_CODE = "vPermitStateCode";
	public static final String V_PERMIT_STATE_NAME = "vPermitStateName";
	public static final String V_FIT_DOC_NO = "vFitDocNo";
	public static final String V_FIT_DOC_ISSUE_DT = "vFitDocIssueDt";
	public static final String V_FIT_DOC_VALID_DT = "vFitDocValidDt";
	public static final String V_FIT_DOC_STATE_CODE = "vFitDocStateCode";
	public static final String V_FIT_DOC_STATE_NAME = "vFitDocStateName";
	public static final String V_FINANCE_BANK_NAME = "vFinanceBankName";
	public static final String V_POLICY_NO = "vPolicyNo";
	public static final String V_POLICY_COMPANY = "vPolicyCompany";
	public static final String V_TRANSIT_PASS_NO = "vTransitPassNo";
	public static final String IS_PENDING = "isPending";
	public static final String IS_CANCEL = "isCancel";
	public static final String USER_CODE = "userCode";
	public static final String BRANCH_CODE = "branchCode";	
	
}