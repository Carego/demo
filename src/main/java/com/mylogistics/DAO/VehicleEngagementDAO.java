package com.mylogistics.DAO;

import java.util.List;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.mylogistics.model.CampaignResults;
import com.mylogistics.model.VehicleEngagement;

@Repository
public interface VehicleEngagementDAO {
	
	public int saveVehicleEngagement(VehicleEngagement engagement,Session session );

	public List<VehicleEngagement> getVehicleEngagementByCampainResult(List<CampaignResults> campList );

	public boolean checkVehicleEngagementByVehNoAndCampainResult(CampaignResults camp,String vehNo,Session session);

	public VehicleEngagement getVehicleEngagementByVeId(int vEId);

	public void updateVehicleEngagement(VehicleEngagement engagement, Session session);

}
