package com.mylogistics.DAO;

import java.util.List;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.mylogistics.model.OwnBrkKycBusinessDetails;

@Repository
public interface OwnBrkKycBusinessDetailsDAO {
	
	public int saveOwnBrkBusinessDetail(Session session,OwnBrkKycBusinessDetails businessDetails);

	public List<OwnBrkKycBusinessDetails> getOwnBrkBusinessDetailByCode(String ownBrkCode);
}
