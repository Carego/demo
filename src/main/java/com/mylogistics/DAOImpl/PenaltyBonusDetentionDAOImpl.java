package com.mylogistics.DAOImpl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.PenaltyBonusDetentionDAO;
import com.mylogistics.constants.PenaltyBonusDetentonCNTS;
import com.mylogistics.model.PenaltyBonusDetention;

public class PenaltyBonusDetentionDAOImpl implements PenaltyBonusDetentionDAO {
	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	public PenaltyBonusDetentionDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Transactional
	public int savePBD(PenaltyBonusDetention penaltyBonusDetention){
		   int temp;
		   try{
				 session = this.sessionFactory.openSession();
				 transaction = session.beginTransaction();
				 session.save(penaltyBonusDetention);
				 transaction.commit();
				 session.flush();
				 System.out.println("penaltyBonusDetention Data is inserted successfully");
				 temp = 1;
			}
			catch(Exception e){
				e.printStackTrace();
				temp = -1;
			}
		   session.clear();
		   session.close();
		   return temp;
		 }
	
	
	@Transactional
	 @SuppressWarnings("unchecked")
	public List<PenaltyBonusDetention> getPenaltyBonusDetention(String code){
		 List<PenaltyBonusDetention> penaltyBonusDetentions = new ArrayList<PenaltyBonusDetention>();
		 System.out.println("Enter into getPenaltyBonusDetention function--- "+code);
		 try{
			 session = this.sessionFactory.openSession();
	         transaction = session.beginTransaction();
	         Criteria cr = session.createCriteria(PenaltyBonusDetention.class);   
	         cr.add(Restrictions.eq(PenaltyBonusDetentonCNTS.PBD_CONT_CODE,code));
	         penaltyBonusDetentions =cr.list();
	         transaction.commit();
	         session.flush();
		 }catch (Exception e) {
			 e.printStackTrace();
		 }
		 session.clear();
		 session.close();
		 return penaltyBonusDetentions;
	}
	
	@Transactional
	 public PenaltyBonusDetention deletePBD(String code){
			System.out.println("code in dao is-----------------------"+code);
			
			PenaltyBonusDetention penaltyBonusDetention = new PenaltyBonusDetention();
			
		    try{
		    	session = this.sessionFactory.openSession();
				 transaction = session.beginTransaction();
				 Criteria cr=session.createCriteria(PenaltyBonusDetention.class);
				 cr.add(Restrictions.eq(PenaltyBonusDetentonCNTS.PBD_CONT_CODE,code));
				 
				 penaltyBonusDetention = (PenaltyBonusDetention) cr.list().get(0);
				 session.delete(penaltyBonusDetention);
				 transaction.commit();
				 session.flush();
				
				 System.out.println("penaltyBonusDetention Data is deleted successfully");	 
		    }catch (Exception e){
				e.printStackTrace();
			}
		    session.clear();
		    session.close();
			 return penaltyBonusDetention;
	   }
	
	
	@Override
	public List<PenaltyBonusDetention> getPenaltyBonusDetention(String code,Date date,String to ){
		 List<PenaltyBonusDetention> penaltyBonusDetentions = new ArrayList<PenaltyBonusDetention>();
		 System.out.println("Enter into getPenaltyBonusDetention function--- "+code);
		 try{
			 session = this.sessionFactory.openSession();
	         Criteria cr = session.createCriteria(PenaltyBonusDetention.class);   
	         cr.add(Restrictions.eq(PenaltyBonusDetentonCNTS.PBD_CONT_CODE,code));
	         cr.add(Restrictions.le(PenaltyBonusDetentonCNTS.PBD_START_DT,date));
	         cr.add(Restrictions.ge(PenaltyBonusDetentonCNTS.PBD_END_DT,date));
	         cr.add(Restrictions.eq(PenaltyBonusDetentonCNTS.PBD_TO_STATION,to));
	         penaltyBonusDetentions =cr.list();
		 }catch (Exception e) {
			 e.printStackTrace();
		 }
		 session.clear();
		 session.close();
		 return penaltyBonusDetentions;
	}
	
}
