<div ng-show="operatorLogin || superAdminLogin">
	
	<div ng-show="lodingFlag" style=" position: absolute; z-index:1; height:100%; background-color: rgba(73, 84, 82, 0.54); width:100%;">
		<div style="margin-left:500px; margin-top:100px;">
			<img src="resources/img/loading.gif"  id="img" width="388px" height="104px"/>
		</div>
	</div>
	
	<div class="row">
		<form ng-submit="submitPanValidForm(PanValidForm)" name="PanValidForm" class="col s12 card" style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
			
				<div class="col s2 input-field">
					<input type ="text" id="branchFaCode" name="branchFaCode" ng-model="branchFaCode" ng-click="OpenBranchCodeDB()" readonly="readonly" required="required"/>
					<label>Select Branch</label>
				</div>
		   		
		   		<div class="col s3 input-field">
					<input type ="date" id="dateFrom" name="dateFrom" ng-model="dateFrom" required="required"/>
					<label>Date From</label>
				</div>
				
		   		<div class="col s3 input-field">
					<input type ="date" id="dateTo" name="dateTo" ng-model="dateTo"/>
					<label>Date To</label>
				</div>
				<div  class="col s2 input-field">
				<select name="panHldrName" id="panHldrId" ng-model="panHldr"  ng-init="panHldr = 'Owner'" ng-required="true">
								<option value='Owner'>Owner</option>
								<option value='Broker'>Broker</option>
							</select>
							<label>PAN Holder Type</label>
				</div>
				<div class="col s2 input-field">
	       			<input class="col s12 btn teal white-text" type ="submit" value="Submit" >
	       		</div>
				
			
					<div class="col s4 input-field">
			   		  <input type="checkbox" id="notValidId" name="panValid" ng-model="notValid" ng-init="notValid=false" ng-click="getNotValidList()"/>	
			   			<label>Not Validate</label>
			   		</div>
			   		
			   		<div class="col s4 input-field">
			   		  <input type="checkbox" id="invalidPanId" name="panValid" ng-model="invalidPan" ng-init="invalidPan=false" ng-click="getInvalidPanList()"/>	
			   			<label>Invalid Pan</label>
			   		</div>
				   		
		   		
			</div>
			
		</form>
	</div>
	
	<div id="branchCodeDB" ng-hide="BranchCodeDBFlag">

	<input type="text" name="filterTextbox"
		ng-model="filterTextbox" placeholder="Search">
	<table>
		<tr>
			<th></th>

			<th>Branch Code</th>

			<th>Branch Name</th>

			<th>Branch Pin</th>
			
			<th>Branch FACode</th>
		</tr>
		<tr ng-repeat="branch in branchList | filter:filterTextbox"">
			<td><input type="radio" name="branchCode" id="branchId"
				value="{{ branch.branchCode }}" ng-model="branchFaCode"
				ng-click="saveBranchCode(branch)"></td>
			<td>{{ branch.branchCode }}</td>
			<td>{{ branch.branchName }}</td>
			<td>{{ branch.branchPin }}</td>
			<td>{{ branch.branchFaCode }}</td>
		</tr>
	</table>

</div>

	<!-- notValidPanDecList -->
	<div class="row" ng-if="notValidPanDecList.length>0">
		<table class="tblrow" >
       		
       		<caption class="coltag tblrow">Not Validate Pan</caption> 
       		
            <tr class="rowclr">
            	<th class="colclr">S.No</th>
            	<!-- <th class="colclr">Brh Code</th> -->
	            <th class="colclr">Chln No.</th>
	            <th class="colclr">Chln Dt.</th>
	            <th class="colclr">Own Code</th>
	            <th class="colclr">Brk Code</th>
	            <!-- <th class="colclr">Vehicle No</th> -->
	            <th class="colclr">Pan Holder Name</th>
	            <th class="colclr">Pan No.</th>
	            <th class="colclr">Valid Pan</th>
	            <th class="colclr">Invalid Pan</th>
	            <!-- <th class="colclr">Valid Dec</th>
	            <th class="colclr">Invalid Dec</th> -->
	            <th class="colclr">Pan Img</th>
	            <!-- <th class="colclr">Dec Img</th> -->
	            <th class="colclr">Save</th>
<!-- 	            <th class="colclr">Edit</th> -->
	        </tr>	
	         
	        <tr ng-repeat="notValidPanDec in notValidPanDecList">
	        	<td class="rowcel">{{$index+1}}</td>
	        	<!-- <td class="rowcel">{{notValidPanDec.brnchCode}}</td> -->
	            <td class="rowcel">{{notValidPanDec.chdChlnCode}}</td>
	            <td class="rowcel">{{notValidPanDec.chdChlnDt | date : 'dd/MM/yy'}}</td>
	            <td class="rowcel">{{notValidPanDec.chdOwnCode}}</td>
	            <td class="rowcel">{{notValidPanDec.chdBrCode}}</td>
	            <!-- <td class="rowcel">{{notValidPanDec.chdRcNo}}</td> -->
	            <td class="rowcel">{{notValidPanDec.name}}</td>
	            
	            <td class="rowcel">
		        	<input type="text" id="panNoId{{$index}}" name="panNoName" ng-model="notValidPanDec.panNo" ng-minlength="10" ng-maxlength="10" ng-required="true" size="8" disabled="disabled">
		        </td>
	            
	            <td class="rowcel">
		        	<input type="checkbox" ng-model="notValidPanDec.isValidPan" ng-click="isValidPanClick(notValidPanDec, $index)"/>
		        </td>
		        
		        <td class="rowcel">
		        	<input type="checkbox" ng-model="notValidPanDec.isInvalidPan" ng-click="isInvalidPanClick(notValidPanDec, $index)"/>
		        </td>	     
		        
		        <td class="rowcel" ng-show="notValidPanDec.isPanImg">
	            	<a class="btn-floating suffix waves-effect teal" type="button" ng-click="showImage(notValidPanDec, $index)" >
						<i class="mdi-action-visibility white-text"></i>
					</a>
				</td>
				
				<td class="rowcel" ng-hide="notValidPanDec.isPanImg">
	            	<a class="btn-floating suffix waves-effect teal" type="button">
						<i class="mdi-action-visibility-off white-text"></i>
					</a>
				</td>        
	             
	            <td class="rowcel">
	            	<a class="btn-floating suffix waves-effect teal" type="button" ng-click="validatePanDec(notValidPanDec, $index)" >
						<i class="mdi-content-send white-text"></i>
					</a>
				</td>
				
<!-- 				<td class="rowcel"> -->
<!-- 	            	<a class="btn-floating suffix waves-effect teal" type="button" ng-click="editPan($index)" > -->
<!-- 						<i class="mdi-image-edit white-text"></i> -->
<!-- 					</a> -->
<!-- 				</td> -->
	        </tr>
	        
	
    	</table>
          
	</div>
	
	<!-- panInvalidList -->
	<div id="exportable"  class="noprint">
	<div class="row" ng-if="panInvalidList.length>0">
		<table class="tblrow" >
       		
       		<caption class="coltag tblrow">Validate Pan</caption> 
       		
            <tr class="rowclr">
            	<th class="colclr">S.No.</th>
            	<!-- <th class="colclr">Brh Code</th> -->
	            <th class="colclr">Chln No.</th>
	            <th class="colclr">Chln Dt.</th>
	            <th class="colclr">Own Code</th>
	            <th class="colclr">Brk Code</th>
	            <!-- <th class="colclr">Vehicle No</th> -->
	            <th class="colclr">Pan Holder Name</th>
	            <th class="colclr">Pan No.</th>
	            <th class="colclr">Valid Pan</th>
	            <th class="colclr">Invalid Pan</th>
	            <!-- <th class="colclr">Valid Dec</th>
	            <th class="colclr">Invalid Dec</th> -->
	            <th class="colclr">Pan Img</th>
	            <!-- <th class="colclr">Dec Img</th> -->
	            <th class="colclr">Save</th>
<!-- 	            <th class="colclr">Edit</th> -->
	        </tr>	
	         
	        <tr ng-repeat="panInvalid in panInvalidList">
	        	<td class="rowcel">{{$index+1}}</td>
	        	<!-- <td class="rowcel">{{panInvalid.brnchCode}}</td> -->
	            <td class="rowcel">{{panInvalid.chdChlnCode}}</td>
	            <td class="rowcel">{{panInvalid.chdChlnDt | date : 'dd/MM/yy'}}</td>
	            <td class="rowcel">{{panInvalid.chdOwnCode}}</td>
	            <td class="rowcel">{{panInvalid.chdBrCode}}</td>
	            <!-- <td class="rowcel">{{panInvalid.chdRcNo}}</td> -->
	            <td class="rowcel">{{panInvalid.name}}</td>
	            
	            <td class="rowcel">
		        	<input type="text" id="panInvalidNoId{{$index}}" name="panInvalidNoName" ng-model="panInvalid.panNo" ng-minlength="10" ng-maxlength="10" ng-required="true" size="8" disabled="disabled">
		        </td>
	            
	            <td class="rowcel">
		        	<input type="checkbox" ng-model="panInvalid.isValidPan" ng-click="isValidPanClick(panInvalid, $index)"/>
		        </td>
		        
		        <td class="rowcel">
		        	<input type="checkbox" ng-model="panInvalid.isInvalidPan" ng-click="isInvalidPanClick(panInvalid, $index)"/>
		        </td>	      
	            
	            <td class="rowcel" ng-show="panInvalid.isPanImg">
	            	<a class="btn-floating suffix waves-effect teal" type="button" ng-click="showImage(panInvalid, $index)" >
						<i class="mdi-action-visibility white-text"></i>
					</a>
				</td>
				
				<td class="rowcel" ng-hide="panInvalid.isPanImg">
	            	<a class="btn-floating suffix waves-effect teal" type="button">
						<i class="mdi-action-visibility-off white-text"></i>
					</a>
				</td>
	             
	            <td class="rowcel">
	            	<a class="btn-floating suffix waves-effect teal" type="button" ng-click="validatePan(panInvalid, $index)" >
						<i class="mdi-content-send white-text"></i>
					</a>
				</td>
				
<!-- 				<td class="rowcel"> -->
<!-- 	            	<a class="btn-floating suffix waves-effect teal" type="button" ng-click="editPanInvalid($index)" > -->
<!-- 						<i class="mdi-image-edit white-text"></i> -->
<!-- 					</a> -->
<!-- 				</td> -->
	        </tr>
	        
	
    	</table>
          
	</div>
	</div>
	
	<div class="row">
		     		<div class="input-field col s12 center">
		       				<input class="validate" type ="button" value="Download invalid"  ng-click="downInvldPan()">
		       		</div>
		      </div>
	
	<!-- decInvalidList -->
	<!-- <div class="row" ng-if="decInvalidList.length>0">
		<table class="tblrow" >
       		
       		<caption class="coltag tblrow">Validate Declaration</caption> 
       		
            <tr class="rowclr">
	            <th class="colclr">Chln No.</th>
	            <th class="colclr">Own Code</th>
	            <th class="colclr">Brk Code</th>
	            <th class="colclr">Vehicle No</th>
	            <th class="colclr">Pan Hldr</th>
	            <th class="colclr">Pan No.</th>
	            <th class="colclr">Valid Pan</th>
	            <th class="colclr">Invalid Pan</th>
	            <th class="colclr">Valid Dec</th>
	            <th class="colclr">Invalid Dec</th>
	            <th class="colclr">Pan Img</th>
	            <th class="colclr">Dec Img</th>
	            <th class="colclr"></th>
	        </tr>	
	         
	        <tr ng-repeat="decInvalid in decInvalidList">
	            <td class="rowcel">{{decInvalid.chdChlnCode}}</td>
	            <td class="rowcel">{{decInvalid.chdOwnCode}}</td>
	            <td class="rowcel">{{decInvalid.chdBrCode}}</td>
	            <td class="rowcel">{{decInvalid.chdRcNo}}</td>
	            <td class="rowcel">{{decInvalid.panHolder}}</td>
	            <td class="rowcel">{{decInvalid.panNo}}</td>
	            
	            <td class="rowcel">
		        	<input type="checkbox" ng-model="decInvalid.isValidPan" ng-click="isValidPanClick(decInvalid, $index)"/>
		        </td>
		        
		        <td class="rowcel">
		        	<input type="checkbox" ng-model="decInvalid.isInvalidPan" ng-click="isInvalidPanClick(decInvalid, $index)"/>
		        </td>
		        
		        <td class="rowcel">
		        	<input type="checkbox" ng-model="decInvalid.isValidDec" ng-click="isValidDecClick(decInvalid, $index)"/>
		        </td>
		        
		        <td class="rowcel">
		        	<input type="checkbox" ng-model="decInvalid.isInvalidDec" ng-click="isInvalidDecClick(decInvalid, $index)"/>
		        </td>
	            <td class="rowcel">{{decInvalid.isPanImg}}</td>
	            <td class="rowcel">{{decInvalid.isDecImg}}</td>
	             
	            <td class="rowcel">
	            	<a class="btn-floating suffix waves-effect teal" type="button" ng-click="validateDec(decInvalid, $index)" >
						<i class="mdi-content-send white-text"></i>
					</a>
				</td>
	        </tr>
	    </table>
          
	</div> -->
	
</div>