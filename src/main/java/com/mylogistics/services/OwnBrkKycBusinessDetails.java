package com.mylogistics.services;

import java.util.List;

public class OwnBrkKycBusinessDetails {

	private List<String> routeList;
	private List<String> vehilceTypeList;
	private String ownBrkCode;
	
	public List<String> getRouteList() {
		return routeList;
	}
	public void setRouteList(List<String> routeList) {
		this.routeList = routeList;
	}
	public List<String> getVehilceTypeList() {
		return vehilceTypeList;
	}
	public void setVehilceTypeList(List<String> vehilceTypeList) {
		this.vehilceTypeList = vehilceTypeList;
	}
	public String getOwnBrkCode() {
		return ownBrkCode;
	}
	public void setOwnBrkCode(String ownBrkCode) {
		this.ownBrkCode = ownBrkCode;
	}

	
}
