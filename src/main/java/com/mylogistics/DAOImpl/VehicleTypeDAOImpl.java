package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.VehicleTypeDAO;
import com.mylogistics.constants.ContToStnCNTS;
import com.mylogistics.constants.VehicleTypeCNTS;
import com.mylogistics.model.ContToStn;
import com.mylogistics.model.VehicleType;

public class VehicleTypeDAOImpl implements VehicleTypeDAO{
	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	private static Logger logger = Logger.getLogger(VehicleTypeDAOImpl.class);
	
	@Autowired
	public VehicleTypeDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	 @Transactional
	 public int saveVehicleTypeToDB(VehicleType vehicletype){
		 int temp;
		 try{
			  session = this.sessionFactory.openSession();
			  transaction =  session.beginTransaction();
			  session.save(vehicletype);
			  transaction.commit();
			  session.flush();
			  temp= 1;
		 }catch(Exception e){
			 e.printStackTrace();
			 temp= -1;
		 }
		 session.clear();
		 session.close();
		 return temp;
	}
	
	@Transactional
	@SuppressWarnings("unchecked")
	public List<VehicleType> getVehicleType(){
		List<VehicleType> vehicletypeList = new ArrayList<VehicleType>();
		try{
		     session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 Criteria cr = session.createCriteria(VehicleType.class);
		     vehicletypeList=cr.list();
		     /*transaction.commit();*/
		     session.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
	    session.close();
		return vehicletypeList;
	}
	
	@Transactional
	@SuppressWarnings("unchecked")
	public List<VehicleType> getVehicleTypeByVNameCode(String vNameCode){
		List<VehicleType> vehicletypeList = new ArrayList<VehicleType>();
		try{
		     session = this.sessionFactory.openSession();
			 Criteria cr = session.createCriteria(VehicleType.class);
			 cr.add(Restrictions.or(Restrictions.like(VehicleTypeCNTS.VT_CODE, vNameCode+"%"), Restrictions.like(VehicleTypeCNTS.VT_VEHICLE_TYPE, vNameCode)));
		     vehicletypeList=cr.list();
		     session.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
	    session.close();
		return vehicletypeList;
	}
	
	@Transactional
	@SuppressWarnings("unchecked")
	public List<VehicleType> getVehicleTypeByName(String vhType){
		logger.info("Enter into getVehicleTypeByName() : vhType = "+vhType);
		List<VehicleType> vehicletypeList = new ArrayList<VehicleType>();
		try{
		     session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 Criteria cr = session.createCriteria(VehicleType.class);
			 cr.add(Restrictions.like(VehicleTypeCNTS.VT_VEHICLE_TYPE, "%"+vhType+"%"));
		     vehicletypeList=cr.list();
		     /*transaction.commit();*/
		     session.flush();
		}catch(Exception e){
			e.printStackTrace();
			logger.info("Exception = "+e);
		}
		session.clear();
	    session.close();
	    logger.info("Exit from getVehicleTypeByName()");
		return vehicletypeList;
	}
	
	@Transactional
	public int updateVehicleType(VehicleType vehicleType){
		
		 System.out.println("Entered into updateVehicleType function in DAOImpl");
		 int temp;
		 Date date = new Date();
	 	 Calendar calendar = Calendar.getInstance();
	 	 calendar.setTime(date);	 	
		 try{
			  session = this.sessionFactory.openSession();
			  transaction =  session.beginTransaction();
			  vehicleType.setCreationTS(calendar);
			  session.update(vehicleType);
			  transaction.commit();
			  session.flush();
			  temp=1;
		}catch(Exception e){
			e.printStackTrace();
			temp=-1;
		}
		 session.clear();
		session.close();
		return temp;		 
	 }

	@Override
	@Transactional
	public List<Map<String, String>> getVehTypeList() {
		
		List<Map<String, String>> vehTypeList = new ArrayList<>();
		
		try {
			session = this.sessionFactory.openSession();
			Query query = session.createQuery("SELECT new map(vtCode as vtCode, vtServiceType as vtServiceType, vtVehicleType as vtVehicleType) FROM VehicleType");
			vehTypeList = query.list();
			session.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return vehTypeList;
	}
		
	
	@Override
	public String getVehType(String vtCode) {
		
		String vehType=null;
		
		try {
			session = this.sessionFactory.openSession();
			Query query = session.createQuery("SELECT vtVehicleType FROM VehicleType where vtCode='"+vtCode+"'");
			vehType = query.list().toString();
			session.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return vehType;
	}
	
	
	@Override
	public List<String> getHypo(){
		List<String> hypoList = new ArrayList<>();
		try{
		     session = this.sessionFactory.openSession();
		     Query cr=session.createSQLQuery("select name from hypomstr");
		     
			 hypoList=cr.list();
		     /*transaction.commit();*/
		     session.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
	    session.close();
		return hypoList;
	}
	
	
	
	@Override
	public List<VehicleType> getVehTypeByContToStn(String contCode,String toStn,java.sql.Date date){
		
		List<VehicleType> vtList=new ArrayList<>();
		Session session= this.sessionFactory.openSession();
		try {
			List<ContToStn> ctsList=session.createCriteria(ContToStn.class)
					.add(Restrictions.le(ContToStnCNTS.CTS_FR_DT,date))
					.add(Restrictions.ge(ContToStnCNTS.CTS_TO_DT, date))
					.add(Restrictions.eq(ContToStnCNTS.CONT_TO_STN, toStn))
					.add(Restrictions.eq(ContToStnCNTS.CTS_CONT_CODE, contCode)).list();
			
			if(!ctsList.isEmpty()) {
				
				List<String> vtCodeList=new ArrayList<>();
				
				for(ContToStn cts:ctsList) {
					vtCodeList.add(cts.getCtsVehicleType());
				}
				
				vtList=session.createCriteria(VehicleType.class)
						.add(Restrictions.in(VehicleTypeCNTS.VT_CODE, vtCodeList)).list();
			}
			
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			session.clear();
			session.close();
		}
		
		return vtList;
	}
	
	
	@Override
	public List<VehicleType> getVehTypeFrIndent(String contCode,String toStn,java.sql.Date date){
		
		List<VehicleType> vtList=new ArrayList<>();
		Session session= this.sessionFactory.openSession();
		try {
			List<ContToStn> ctsList=session.createCriteria(ContToStn.class)
					.add(Restrictions.le(ContToStnCNTS.CTS_FR_DT,date))
					.add(Restrictions.ge(ContToStnCNTS.CTS_TO_DT, date))
					.add(Restrictions.eq(ContToStnCNTS.CONT_TO_STN, toStn))
					.add(Restrictions.eq(ContToStnCNTS.CTS_CONT_CODE, contCode)).list();
			
			if(!ctsList.isEmpty()) {
				
				List<String> vtCodeList=new ArrayList<>();
				
				for(ContToStn cts:ctsList) {
					vtCodeList.add(cts.getCtsVehicleType());
				}
				
				vtList=session.createCriteria(VehicleType.class)
						.add(Restrictions.in(VehicleTypeCNTS.VT_CODE, vtCodeList)).list();
			}
			
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			session.clear();
			session.close();
		}
		
		return vtList;
	}
	
	
	

	@Override
	public List<String> getVehTypeName() {
		
		List<String> vehTypeList=new ArrayList<>();
		
		try {
			session = this.sessionFactory.openSession();
			Criteria cr = session.createCriteria(VehicleType.class);
			ProjectionList projList = Projections.projectionList();
			  projList.add(Projections.property(VehicleTypeCNTS.VT_VEHICLE_TYPE));
			  cr.setProjection(projList);
			  vehTypeList = cr.list();
			session.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return vehTypeList;
	}
	
}
	

