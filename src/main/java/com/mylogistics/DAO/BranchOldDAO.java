package com.mylogistics.DAO;

import java.util.List;

import com.mylogistics.model.BranchOld;

public interface BranchOldDAO {

	//Satarupa:saves row of branch table to branchold table that is to be modified. 
	public int saveBranchOld(BranchOld branchOld);
	
	//Satarupa:retrieves the details of branchold table on the basis of branchCode
	 public List<BranchOld> getModifiedBranch(String branchCode);
}
