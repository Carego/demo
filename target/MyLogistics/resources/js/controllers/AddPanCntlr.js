'use strict';

var app = angular.module('application');

app.controller('AddPanCntlr',['$scope','$location','$http','$filter','$window', 'FileUploadService',
                                 function($scope,$location,$http,$filter,$window,FileUploadService){

	console.log("AddPanCntlr Started");
	
	$scope.ownerList = [];
	$scope.brokerList = [];
	$scope.owner = {};
	$scope.broker = {};
	$scope.panService = {};
	
	$scope.ownerFlag = false;
	$scope.brokerFlag = false;
	$scope.ownBrkValidFlag = false;
	
	$scope.ownerDBFlag = true;
	$scope.brokerDBFlag = true;
	
	var panImgSize = 0;
	var decImgSize = 0;
		
	$scope.getOwnNameCodeId = function(){
		console.log("getOwnNameCodeId Entered");
		if($scope.owner.ownName.length<=2)
			return;
		
		var response = $http.post($scope.projectName+'/getOwnNameCodeIdByName',$scope.owner.ownName);
		response.success(function(data, status, headers, config){
			
			if (data.result === "success") {
				console.log("getOwnNameCodeId Success");
				$scope.ownerList = data.ownerList;
				$scope.openOwnerDB();
				//$scope.getBrkNameCodeId();
			}else {
				$scope.alertToast("you have no owner");
				console.log("getOwnNameCodeId Error");
			}
		});
		response.error(function(data, status, headers, config){
			console.log("getOwnNameCodeId Error: "+data);
		});
	}
	
	$scope.openOwnerDB = function(){
		console.log("Entered into openOwnerDB");
		$scope.ownerDBFlag = false;
		$('div#ownerDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Owner",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.ownerDBFlag = true;
			}
		});

		$('div#ownerDB').dialog('open');
	}
	
	$scope.saveOwner = function(owner){
		$scope.owner = owner;
		console.log("OwnerName----->"+$scope.owner.ownName);
		$('div#ownerDB').dialog('close');
	}
	
	/*$scope.closeOwnDb =  function(){
		console.log("OwnerName----->"+$scope.owner.ownName);
		$('div#ownerDB').dialog('close');
	}*/
	
	$scope.getBrkNameCodeId = function(){
		console.log("getBrkNameCodeId Entered");
		
		if($scope.broker.brkName.length<=2)
			return;
		
		var response = $http.post($scope.projectName+'/getBrkNameCodeIdByName',$scope.broker.brkName);
		response.success(function(data, status, headers, config){
			
			if (data.result === "success") {
				console.log("getBrkNameCodeId Success");
				$scope.brokerList = data.brokerList;
				$scope.openBrokerDB();
			}else {
				$scope.alertToast("you have no broker");
				console.log("getBrkNameCodeId Error");
			}
		});
		response.error(function(data, status, headers, config){
			console.log("getBrkNameCodeId Error: "+data);
		});
	}
	
	$scope.openBrokerDB = function(){
		console.log("Entered into openBrokerDB");
		$scope.brokerDBFlag = false;
		$('div#brokerDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Broker",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.brokerDBFlag = true;
			}
		});

		$('div#brokerDB').dialog('open');
	}
	
	$scope.saveBroker = function(broker){
		$scope.broker = broker;
		console.log("brokerName----->"+$scope.broker.brkName);
		$('div#brokerDB').dialog('close');
	}
	
	/*$scope.closeBrkDb =  function(){
		console.log("brokerName----->"+$scope.broker.brkName);
		$('div#brokerDB').dialog('close');
	}*/
	
	$scope.ownerRB = function() {
		console.log("ownerRB()");
		$('#brokerId').attr("disabled","disabled");
		$('#ownerId').removeAttr("disabled");
		$scope.ownerFlag = true;
		$scope.brokerFlag = false;
		$scope.broker = {};
	}
	
	$scope.brokerRB = function() {
		console.log("brokerRB()");
		$('#ownerId').attr("disabled","disabled");
		$('#brokerId').removeAttr("disabled");
		$scope.ownerFlag = false;
		$scope.brokerFlag = true;
		$scope.owner = {};
	}
	
	$scope.uploadPanImg = function(){
		console.log("enter into uploadPanImg function");
		var file = $scope.panImg;
		if(angular.isUndefined(file) || file === null || file === ""){
			$scope.alertToast("First choose the file----->");
		}else if(file.size > $scope.maxFileSize){
			$scope.alertToast("image size must be less than or equal to 1mb");
		}else{
			panImgSize = file.size;
			console.log('file is ' + JSON.stringify(file));
			var uploadUrl = $scope.projectName+"/uploadPanImg";
			FileUploadService.uploadFileToUrl(file, uploadUrl);
			console.log("file save on server");
		}
	}
	
	$scope.uploadDecImg = function(){
		console.log("enter into uploadDecImg function");
		var file = $scope.decImg;
		
		if(angular.isUndefined(file) || file === null || file === ""){
			$scope.alertToast("First choose the file----->");
		}else if(file.size > $scope.maxFileSize){
			$scope.alertToast("image size must be less than or equal to 1mb");
		}else{
			decImgSize = file.size;
			console.log('file is ' + JSON.stringify(file));
			var uploadUrl = $scope.projectName+"/uploadDecImg";
			FileUploadService.uploadFileToUrl(file, uploadUrl);
			console.log("file save on server");
		}	
	}
	
	$scope.panFormSubmit = function(panForm){
		console.log("enter into panFormSubmit function = "+panForm .$invalid + "panImgSize = "+panImgSize);
		if(panForm.$invalid){
			 if(panForm.panName.$invalid){
				 $scope.alertToast("please Enter Pan Name");
			 }else if(panForm.panNoName.$invalid){
				 $scope.alertToast("please Enter Valid PanCard No ");
			 }
		}else{
			if(panImgSize > 0){
				if (angular.isUndefined($scope.owner.ownName) || $scope.owner.ownName === null || $scope.owner.ownName === "") {
					
					if (angular.isUndefined($scope.broker.brkName) || $scope.broker.brkName === null || $scope.broker.brkName === "") {
						$scope.ownBrkValidFlag = false;
					}else {
						$scope.ownBrkValidFlag = true;
					}
					
				}else {
					$scope.ownBrkValidFlag = true;
				}
				
				if ($scope.ownBrkValidFlag) {
					
					if ($scope.ownerFlag) {
						$scope.panService.ownId = $scope.owner.ownId;
					}
					if ($scope.brokerFlag) {
						$scope.panService.brkId = $scope.broker.brkId;
					}
					$('#panFormBtnId').attr("disabled","disabled");
					
					var response = $http.post($scope.projectName+'/savePan', $scope.panService);
					response.success(function(data, status, headers, config){
						
						if (data.result === "success") {
							$scope.alertToast(data.result);
							
							$('#panFormBtnId').removeAttr("disabled");
							
							//clear the resources
							$scope.owner = {};
							$scope.broker = {};
							$scope.panService = {};
							
							$scope.ownerFlag = false;
							$scope.brokerFlag = false;
							$scope.ownBrkValidFlag = false;
							
							panImgSize = 0;
							decImgSize = 0;
							
							$('#brokerId').attr("disabled","disabled");
							$('#ownerId').attr("disabled","disabled");
							$('#ownerRBId').attr('checked', false);
							$('#brokerRBId').attr('checked', false);
														
						}else {
							console.log(data.result);
							$scope.alertToast(data.msg);
						}
					});
					response.error(function(data, status, headers, config){
						console.log("getOwnNameCodeId Error: "+data);
					});
				}else {
					$scope.alertToast("Select either owner or broker");
				}
			}else{
				$scope.alertToast("please uplaod scan image of PanCard");
			}
		}
	}
	/*$('#saveVehVenMstrId').attr("disabled","disabled");
	$('#saveVehVenMstrId').removeAttr("disabled");*/
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		//$scope.getOwnNameCodeId();
	}else if($scope.logoutStatus === true){
		$location.path("/");
	}else{
		console.log("****************");
	}
	
	console.log("AddPanCntlr Ended");
}]);