package com.mylogistics.mobileApi.controller.model;




public class OwnerKYCPersonalDetails {
	
	private String ownName;
	
	private String ownBrkType;
	
	private String ownCode;
	
	private String brkCode;
	
	private String companyName;
	
	private String firmType;
	
	private String plotNo;
	
	private String addCity;
	
	private String addState;
	
	private String addPin;
	
	private String addType;
	
	private String addDist;
	
	private String addArea;
	
	private String phoneNo;
	
	private String alternateContactNo;
	
	private String emailAddress;
	
	private String addressProofUrlFront;
	
	private String addressProofUrlBack;
	
	private String panNo;
	
	private String panImageUrl;

	private int empId;
	
	public String getOwnName() {
		return ownName;
	}

	public void setOwnName(String ownName) {
		this.ownName = ownName;
	}

	public String getPlotNo() {
		return plotNo;
	}

	public void setPlotNo(String plotNo) {
		this.plotNo = plotNo;
	}

	public String getAddCity() {
		return addCity;
	}

	public void setAddCity(String addCity) {
		this.addCity = addCity;
	}

	public String getAddState() {
		return addState;
	}

	public void setAddState(String addState) {
		this.addState = addState;
	}

	public String getAddPin() {
		return addPin;
	}

	public void setAddPin(String addPin) {
		this.addPin = addPin;
	}

	public String getAddType() {
		return addType;
	}

	public void setAddType(String addType) {
		this.addType = addType;
	}

	public String getAddDist() {
		return addDist;
	}

	public void setAddDist(String addDist) {
		this.addDist = addDist;
	}

	public String getAddArea() {
		return addArea;
	}

	public void setAddArea(String addArea) {
		this.addArea = addArea;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getAlternateContactNo() {
		return alternateContactNo;
	}

	public void setAlternateContactNo(String alternateContactNo) {
		this.alternateContactNo = alternateContactNo;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}


	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public String getOwnCode() {
		return ownCode;
	}

	public void setOwnCode(String ownCode) {
		this.ownCode = ownCode;
	}

	public String getAddressProofUrlFront() {
		return addressProofUrlFront;
	}

	public void setAddressProofUrlFront(String addressProofUrlFront) {
		this.addressProofUrlFront = addressProofUrlFront;
	}

	public String getAddressProofUrlBack() {
		return addressProofUrlBack;
	}

	public void setAddressProofUrlBack(String addressProofUrlBack) {
		this.addressProofUrlBack = addressProofUrlBack;
	}


	public String getBrkCode() {
		return brkCode;
	}

	public void setBrkCode(String brkCode) {
		this.brkCode = brkCode;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getFirmType() {
		return firmType;
	}

	public void setFirmType(String firmType) {
		this.firmType = firmType;
	}

	public String getOwnBrkType() {
		return ownBrkType;
	}

	public void setOwnBrkType(String ownBrkType) {
		this.ownBrkType = ownBrkType;
	}

	public String getPanNo() {
		return panNo;
	}

	public void setPanNo(String panNo) {
		this.panNo = panNo;
	}

	public String getPanImageUrl() {
		return panImageUrl;
	}

	public void setPanImageUrl(String panImageUrl) {
		this.panImageUrl = panImageUrl;
	}


}
