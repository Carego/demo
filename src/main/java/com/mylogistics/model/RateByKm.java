package com.mylogistics.model;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ratebykm")
public class RateByKm {

	@Id
	@Column(name="rbkmId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int rbkmId;
	
	@Column(name="rbkmContCode")
	private String rbkmContCode;
	
	@Column(name="rbkmFromStation")
	private String rbkmFromStation;
	
	@Column(name="rbkmToStation")
	private String rbkmToStation;
	
	
	@Column(name="rbkmStateCode")
	private String rbkmStateCode;
	
	@Column(name="rbkmFromKm")
	private double rbkmFromKm;
	
	@Column(name="rbkmToKm")
	private double rbkmToKm;
	
	@Column(name="rbkmVehicleType")
	private String rbkmVehicleType;
	
	@Column(name="rbkmProductType")
	private String rbkmProductType;
	
	@Column(name="rbkmRate")
	private double rbkmRate;
	
	@Column(name="rbkmTransitDay")
	private int rbkmTransitDay;
	
	@Column(name="rbkmStatChg")
	private double rbkmStatChg;
	
	@Column(name="rbkmLoad")
	private double rbkmLoad;
	
	@Column(name="rbkmUnLoad")
	private double rbkmUnLoad;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;

	@Column(name="bCode")
	private String bCode;
	
	
	public int getRbkmTransitDay() {
		return rbkmTransitDay;
	}

	public void setRbkmTransitDay(int rbkmTransitDay) {
		this.rbkmTransitDay = rbkmTransitDay;
	}

	public double getRbkmStatChg() {
		return rbkmStatChg;
	}

	public void setRbkmStatChg(double rbkmStatChg) {
		this.rbkmStatChg = rbkmStatChg;
	}

	public double getRbkmLoad() {
		return rbkmLoad;
	}

	public void setRbkmLoad(double rbkmLoad) {
		this.rbkmLoad = rbkmLoad;
	}

	public double getRbkmUnLoad() {
		return rbkmUnLoad;
	}

	public void setRbkmUnLoad(double rbkmUnLoad) {
		this.rbkmUnLoad = rbkmUnLoad;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public int getRbkmId() {
		return rbkmId;
	}

	public void setRbkmId(int rbkmId) {
		this.rbkmId = rbkmId;
	}

	public String getRbkmContCode() {
		return rbkmContCode;
	}

	public void setRbkmContCode(String rbkmContCode) {
		this.rbkmContCode = rbkmContCode;
	}

	public String getRbkmFromStation() {
		return rbkmFromStation;
	}

	public void setRbkmFromStation(String rbkmFromStation) {
		this.rbkmFromStation = rbkmFromStation;
	}

	public String getRbkmToStation() {
		return rbkmToStation;
	}

	public void setRbkmToStation(String rbkmToStation) {
		this.rbkmToStation = rbkmToStation;
	}

	public String getRbkmStateCode() {
		return rbkmStateCode;
	}

	public void setRbkmStateCode(String rbkmStateCode) {
		this.rbkmStateCode = rbkmStateCode;
	}

	public double getRbkmFromKm() {
		return rbkmFromKm;
	}

	public void setRbkmFromKm(double rbkmFromKm) {
		this.rbkmFromKm = rbkmFromKm;
	}

	public double getRbkmToKm() {
		return rbkmToKm;
	}

	public void setRbkmToKm(double rbkmToKm) {
		this.rbkmToKm = rbkmToKm;
	}

	public String getRbkmVehicleType() {
		return rbkmVehicleType;
	}

	public void setRbkmVehicleType(String rbkmVehicleType) {
		this.rbkmVehicleType = rbkmVehicleType;
	}

	public double getRbkmRate() {
		return rbkmRate;
	}

	public void setRbkmRate(double rbkmRate) {
		this.rbkmRate = rbkmRate;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public String getRbkmProductType() {
		return rbkmProductType;
	}

	public void setRbkmProductType(String rbkmProductType) {
		this.rbkmProductType = rbkmProductType;
	}
	
}
