<style type="text/css">
.printable {display: none;}
            @media print {
                .noprint, .menuwrapper, .ui-dialog, footer, #toast-container {display: none !important;}
                .printable {display: block;
                		width: 10in !important;
                		position: fixed;
                		top: 0px;
                		left: 0px;
                		z-index: 999999;
                		}
                		
                		body {
						    width: 10in;
						    height: 210in;
						     
						  }
            }
            @PAGE {
				  size:A4 ;
				  margin:0.5cm 0.5cm 0.5cm 0.5cm;
			      }  
			      
			     			      
}

  .latterh{font-weight:bold;} 
  hr { 
    display: block;
    margin-top: 0.5em;
    margin-bottom: 0.5em;
    margin-left: auto;
    margin-right: auto;
    border-style: inset;
    border-width: 1px;
} 
</style>
<div ng-show="operatorLogin || superAdminLogin">
	<div class="noprint">
		<form name="blPrintForm" ng-submit=blPrintSubmit(blPrintForm) class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
					<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="brhId" name ="brhName" ng-model="branch" ng-required="true" ng-click="openBrhDB()" readonly>
		       			<label for="code">Billing Branch</label>	
		       		</div>
		       		
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="frBlId" name ="frBlName" ng-model="frBlNo" ng-required="true" >
		       			<label for="code">From Bill No</label>	
		       		</div>

		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="toBlId" name ="toBlName" ng-model="toBlNo" ng-required="true" >
		       			<label for="code">To Bill No</label>	
		       		</div>
		       		
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="date" id="dateId" name ="blDateName" ng-model="blDate" >
		       			<label for="code">Change Date</label>	
		       		</div>
		    </div>
		    
		     <div class="row">
	     		<div class="input-field col s12 center">
	       				<input class="validate" type ="submit" value="submit" >
	       		</div>
	         </div>
	      
		      <div class="row">
		     		<div class="input-field col s12 center">
		       				<input class="validate" type ="button" value="Download Annexture" ng-show="relBlShow" ng-click="downloadAnx()">
		       		</div>
		      </div>
		      	 
		</form>    
	</div>
	
	
	<div id ="openBrhId" ng-hide="openBrhFlag">
		  <input type="text" name="filterBrh" ng-model="filterBrh" placeholder="Search....">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Branch Name</th>
 	  	  	  <th>FaCode</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="brh in brhList | filter:filterBrh">
		 	  <td><input type="radio"  name="brh"   value="{{ brh }}" ng-model="brhCode" ng-click="saveBranch(brh)"></td>
              <td>{{brh.branchName}}</td>
              <td>{{brh.branchFaCode}}</td>
          </tr>
      </table> 
	</div>
	
	
	<div id="printVsDB" ng-hide="printVsFlag" class="noprint">
		<div class="row">
			<div class="col s12 center"><h5 class="white-text">{{PRINT_STMT}}</h5></div>
		</div>
		<div>
			<div class="col s12 center">
				<a class="btn white-text" ng-click="cancelPrint()">Cancel</a>
				<a class="btn white-text" ng-click="printVs()">Yes</a>
			</div>
		</div>
	</div>
	
	
	<div class="printable"> 
			 <div  ng-repeat="bill in blList">
			 
			 	<div ng-show="bill.relBillFormat">
						 <div class="myheadRowlld">
			                <div class="mylatterhead">
			                <img src="resources/img/latterhead.png"" />
			                </div>
			            </div> 
			        
			           <div >
			           <!--<br><br><br> <br><br><br><br> -->
			                <div class="mylatterhead1" ><h5>FREIGHT INVOICE</h5></div><br><br>
			                <div class="mylatterhead14" >(Original for Recipient)</div>
			             </div>
			          
			          <div class="divRowlld">
			                <div class="mylatterhead2">Invoice Srl No.{{bill.bill.blBillNo}}</span></div>
			                <div  class="mylatterhead2" style="text-align:right;" ng-if="blDate == undefined || blDate==''">Date: {{bill.bill.blBillDt | date:'dd.MM.yyyy'}}</span></div>
			           		 <div  class="mylatterhead2" style="text-align:right;" ng-if="blDate!= undefined && blDate!=''">Date: {{blDate | date:'dd.MM.yyyy'}}</span></div> 
			             </div>
			          
			            <div class="CSSTableGeneratorhd">
								<table>
									<tr> 
										<td>1.Name & Address of Goods Transport Agency</td>
										<td>M/s Care Go Logistics Pvt Ltd. <br>
										Regd. Office: S-405, Greater Kailash, Part-II,New Delhi-110048 <br>
										Email Id:  gurgaon@carego.co <br>
										MSME Regd. No.: HR05E0000104<br>
										Co CIN No. U6023DL2015PTC279265
										</td>
<!-- 										 {{selBrh.branchAdd}}</td> -->
									</tr>
			
			                         <tr> 
										<td>2. PAN of Goods Transport Agency</td>
										<td><span>AAGCC0032K</span></td>
									</tr>
									
									<tr> 
										<td>3. GST Number of GTA</td>
										<td>NOT APPLICABLE</td>
									</tr>
			
			                         <tr> 
										<td>4. Name & Address of Person Liable for payment of GST</td>
										<td>{{bill.cust.custName}}
										<hr>
											 {{bill.cust.cmpltAddress}}
										</td>
									</tr>
									
									<tr> 
										<td>5. GST of the Recipient of Service</td>
										<td>{{bill.bill.blGstNo}}</td>
									</tr>
			
			                         <tr> 
										<td>6. Description/Category of Service</td>
										<td>Transportation of goods by Road</td>
									</tr>
									
									<tr> 
										<td>7. Service Accounting Code(SAC) </td>
										<td>996791</td>
									</tr>
			
			                         <tr> 
										<td>8. Details of Transportation
										<br>
										i. Supply Invoice No. & Date
										<br>
										ii. Consignment Note No. & Date
										<br>
										iii. Weight of Consignment
										<br>
										iv. Description of Goods
										<br>
										v. Place of Origin
										<br>
										vi. Place of Destination
										<br>
										vii. Vehicle Number
										</td>
										<td>
										<br>
										As per Annexure attached
											<br>
										As per Annexure attached
											<br>
										As per Annexure attached
											<br>
										As per Annexure attached
											<br>
										As per Annexure attached
											<br>
										As per Annexure attached
											<br>
										As per Annexure attached
										</td>
									</tr>
									
									
			   <tr> 
				<td>viii. Value of Total Freight including loading/unloading Charges</td>
				<td><span>{{bill.bill.blFinalTot}}</span></td>
									</tr>
									
									 <tr> 
				<td>Taxable Service amount</td>
				<td><span>{{bill.bill.blFinalTot}}</span></td>
									</tr>
									
									 <tr> 
				<td>CGST &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.50%</td>
				<td><span ng-if="bill.cGST==0">Nill</span>
				<span ng-if="bill.cGST!=0">{{bill.cGST}}</span></td>
									</tr>
									
											 <tr> 
				<td>SGST &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.50%</td>
				<td><span ng-if="bill.sGST==0">Nill</span>
				<span ng-if="bill.sGST!=0">{{bill.sGST}}</td>
									</tr>
								
										 <tr> 
				<td>IGST &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 5.00%</td>
				<td><span ng-if="bill.iGST==0">Nill</span>
				<span ng-if="bill.iGST!=0">{{bill.iGST}}</td>
									</tr>	
									
											 <tr> 
				<td>Total GST Amount</td>
				<td><span>{{bill.cGST + bill.sGST + bill.iGST}}</span></td>
									</tr>
									
											 <tr> 
				<td>GST Amount in Words</td>
				<td><span>{{bill.gstAmt}}</span></td>
									</tr>
									
											 <tr> 
				<td>Total GST to be paid under Reverse Charge by the<br>
				Recipient of the Service
				</td>
				<td></td>
									</tr>
									
											 <tr> 
				<td>9. Total amount due from the Service Recipient</td>
				<td><span>{{bill.bill.blFinalTot}}</span></td>
									</tr>
									
											 <tr> 
				<td>10. Total amount due in words</td>
				<td><span>{{bill.bilAmt}}</span></td>
									</tr>
			
											
											 
											   <tr> 
										<td></td>
										<td>for M/s Care Go Logistics Pvt Ltd.
										<br>
										<br>
										<br>
										<br>
										<br>
										Authorised Signatory
										</td>
									</tr>
				
								</table>
							</div>
			       
			               
			               <div class="divRowlld">
			                <div class="mylatterhead3">Declaration</span></div>
			             </div>
			       
			             <div>
								<table>
									<tr  class="csstd"> 
										<td>1.  We hereby certify that we have not availed any input tax credit on goods & Services used for providing the service under this invoice.</td>
									</tr>
									<tr  class="csstd"> 
										<td>2. Certified that all the particulars given are true and correct</td>
									</tr>
								</table>
							</div>
			       
			            <div class="divRowlld">
			                <div class="mylatterhead4">Enclosure:</span></div>
			             </div> 
			             
			             <div class="divRowlld">
			                <div class="mylatterhead5">1. Annexure 2. Original LR's</span></div>
			             </div>      
			            
			             <div class="divRow1ld">
			                <div class="latterhead6">care go logistics pvt. ltd.</span></div>
			             </div> 
			             
			             <div class="divRowlld">
			                <div class="latterhead7">Regd. office : S-405, Greater Kailash Part-II, New Delh- 110048, Cin No.-U60231DL2015PTC279265</div>
			            <div class="latterhead7">Corporate Office : SCO-44, 4th Floor, Old Judicial Complex, Civil Lines Gurugram - 122001, Ph 0124-4205305</div>
			             </div>
			 	</div>
			         
     		 </div>
			
	
	</div>

	
	
	<div id="exportable" class="noprint">
	 
	<table ng-repeat="bill in blList">
		<!-- <caption class="coltag tblrow">Annexure For Bill No. {{bill.bill.blBillNo}}</caption>-->
		<caption class="coltag tblrow"><b style="font-weight: bold;">Care Go Logistics PVT Ltd<br></b>
		  <b style="border:1px solid black;">RELIANCE CORPORATE PARK BUILDING NO.5B IST FLOOR 5TTC INDUSTRAIL AREA GHANSOLI</b></caption>	
		<tr style="text-align:center;">
			<th style="border:1px solid #000000;">FILE NO</th>
		    <th style="border:1px solid #000000;">FILE DATE</th>
		    <th style="border:1px solid #000000;">BILL NO</th>
		    <th style="border:1px solid #000000;">BILL DATE</th>
		    <th style="border:1px solid #000000;">BILL AMT</th>
		    <th style="border:1px solid #000000;">BILL From</th>
		    <th style="border:1px solid #000000;">BILL To</th>
		    <th style="border:1px solid #000000;">DCPI NO</th>
		    <th style="border:1px solid #000000;">DCPI DATE</th>
		    <th style="border:1px solid #000000;">TRUCK NO</th>
		    <th style="border:1px solid #000000;">LR NO</th>
		    <th style="border:1px solid #000000;">LR DATE</th>  
		    <th style="border:1px solid #000000;">DELIVERED</th>
		    <th style="border:1px solid #000000;">DELIVERY</th>
		    <th style="border:1px solid #000000;">RATE</th>
		    <th style="border:1px solid #000000;">AMOUNT</th>
		    <th style="border:1px solid #000000;">VENDOR</th>
		    <th style="border:1px solid #000000;">COMPANY</th>
		    <th style="border:1px solid #000000;">OVER HEIGHT</th>
		    <th style="border:1px solid #000000;">TOTAL AMOUNT</th>
		    <th style="border:1px solid #000000;">FROM</th>
		    <th style="border:1px solid #000000;">TO</th>
		 <!--    <th style="border:1px solid #000000;">WEIGHT</th>
		    <th style="border:1px solid #000000;">MATERIAL</th> -->
		    <th style="border:1px solid #000000;">TRUCK</th>
		    <!-- <th style="border:1px solid #000000;">REMARKS</th> -->
		</tr>
		
		<tr ng-repeat="bilDet in bill.blDetList" style="text-align:center;">
			<!--<td style="border:1px solid #000000;">{{ bill.bfNo }}</td>-->
			<td style="border:1px solid #000000;">{{ bill.bill.blBillNo }}</td>
			<td style="border:1px solid #000000;" ng-if="blDate == undefined || blDate==''">{{ bill.bill.blBillDt | date:'dd.MM.yyyy' }}</td>
			<td style="border:1px solid #000000;" ng-if="blDate != undefined && blDate!=''">{{ blDate | date:'dd.MM.yyyy'}}</td>
			<td style="border:1px solid #000000;">{{ bill.bill.blBillNo }}</td>
			<td style="border:1px solid #000000;" ng-if="blDate == undefined || blDate==''">{{ bill.bill.blBillDt | date:'dd.MM.yyyy' }}</td>
			<td style="border:1px solid #000000;" ng-if="blDate != undefined && blDate!=''">{{ blDate | date:'dd.MM.yyyy'}}</td>
			<td style="border:1px solid #000000;">{{bill.bill.blSubTot}}</td> <!--  bilDet.billDet.bdTotAmt | number : 2 -->
			<td style="border:1px solid #000000;">DL</td>
			<td style="border:1px solid #000000;">{{bill.cust.stateRelPreFix}}</td>
			<td style="border:1px solid #000000;">'{{ bilDet.cnmt.cnmtInvoiceNo[0].invoiceNo }}</td>
			<td style="border:1px solid #000000;">{{ bilDet.cnmt.cnmtInvoiceNo[0].date | date:'dd.MM.yyyy'}}</td>
			<td style="border:1px solid #000000;">{{ bilDet.truckNo }}</td>
			<td style="border:1px solid #000000;">0{{ bilDet.cnmt.cnmtCode }}</td>
			<td style="border:1px solid #000000;">{{ bilDet.cnmt.cnmtDt | date:'dd.MM.yyyy' }}</td>
			<td style="border:1px solid #000000;">{{ bilDet.delQty }}</td>
			<td style="border:1px solid #000000;">{{ bilDet.delDt | date:'dd.MM.yyyy' }}</td>
			<td style="border:1px solid #000000;">{{changeCommaNumber((bilDet.billDet.bdFreight | number : 0))}}</td> <!--{{bilDet.billDet.bdRate * 1000 | number : 2}}  -->
			<td style="border:1px solid #000000;">{{changeCommaNumber((bilDet.billDet.bdFreight | number : 0))}}</td> 
			<td style="border:1px solid #000000;">3234040</td>
			<td style="border:1px solid #000000;">5075</td>
			<td style="border:1px solid #000000;">{{ changeCommaNumber(((bilDet.billDet.bdTotAmt-bilDet.billDet.bdFreight)| number : 0)) }}</td> <!-- bilDet.ovrHgt | number : 2 --> <!-- bilDet.ovrHgt | number : 0 -->
			<td style="border:1px solid #000000;">{{changeCommaNumber((bilDet.billDet.bdTotAmt | number : 0))}}</td> <!--{{ bilDet.billDet.bdTotAmt | number : 2 }}  -->
			<td style="border:1px solid #000000;">{{ bilDet.frmStn }}</td>
			<td style="border:1px solid #000000;">{{ bilDet.toStn }}</td>
			<!-- <td style="border:1px solid #000000;">
				<div ng-if="bilDet.billDet.bdBlBase == 'chargeWt'">{{bilDet.billDet.bdChgWt/1000 | number : 2}}</div>
                <div ng-if="bilDet.billDet.bdBlBase == 'actualWt'">{{bilDet.billDet.bdActWt/1000 | number : 2}}</div>
                <div ng-if="bilDet.billDet.bdBlBase == 'receiveWt'">{{bilDet.billDet.bdRecWt/1000 | number : 2}}</div>
			</td> 
			<td style="border:1px solid #000000;">{{ bilDet.cnmt.cnmtProductType }}</td>-->
			<td style="border:1px solid #000000;">{{ bilDet.vehType }}</td>
			<!-- <td style="border:1px solid #000000;">{{ bill.bill.blDesc }}</td> -->
		</tr>
	</table>
	</div>
	
</div>	
       
           