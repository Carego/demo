'use strict';

var app = angular.module('application');

app.controller('LhpvSupCntlr',['$scope','$location','$http','$filter','$window',
                                  function($scope,$location,$http,$filter,$window){
	$scope.vs = {};
	$scope.lhpvSup = {};
	$scope.bnkList = [];
	$scope.chlnList = [];
	$scope.actBOList = [];
	$scope.lhpvSupList = [];
	
	
	$scope.bankCodeDBFlag = true;
	$scope.chqNoDBFlag = true;
	$scope.brkOwnDBFlag = true;
	$scope.chlnDBFlag = true;
	$scope.saveVsFlag = true;
	$scope.lhpvSupDBFlag = true;
	
	$scope.lodingFlag = false;
	
	var faCodeTemp = '';
	
	$scope.getLhpvDet = function(){
		console.log("enter into getLhpvDet function");
		var response = $http.post($scope.projectName+'/getLhpvSupDet');
		  response.success(function(data, status, headers, config){
			   if(data.result === "success"){
				   $scope.vs.branch = data.branch;
				   $scope.vs.lhpvStatus = data.lhpvStatus;
				   $scope.vs.voucherType = $scope.LHPV_SUP_VOUCHER;
				   $scope.dateTemp = $filter('date')(data.lhpvStatus.lsDt, "yyyy-MM-dd'");
				   $scope.bnkList = data.bnkList;
				   
				   if($scope.vs.lhpvStatus.lsClose === true){
					   $scope.alertToast("You already close the LHPV of "+$scope.dateTemp);
					   $('#verfChln').attr("disabled","disabled");
				   }else{
					   //$scope.getChlnAndBrOwn();
				   }
			   }else{
				   console.log("error in fetching getLhpvDet data");
				   $scope.alertToast("Server Error");
			   }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
		   });
	}
	
	
	$scope.getChlnAndBrOwn = function(){
		console.log("enter into getChlnAndBrOwn function");
		var response = $http.post($scope.projectName+'/getChlnAndBrOwnLSP');
		  response.success(function(data, status, headers, config){
			   if(data.result === "success"){
				   $scope.chlnList = data.chlnList;
				   $scope.actBOList = data.actBOList;
			   }else{
				   console.log("error in fetching getChlnAndBrOwn data");
				   $scope.alertToast("No Challan avaliable for LHPV SUPPLEMENTARY");
			   }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
		   });
	}
	
	$scope.selectPayBy = function(){
		console.log("enter into selectPayMod function with vs.payBy ="+$scope.vs.payBy);
		if($scope.vs.payBy === 'C'){
			$scope.vs.chequeType = '';
			$('#chequeTypeId').attr("disabled","disabled");
			$scope.vs.bankCode = "";
			$('#bankCodeId').attr("disabled","disabled");
			$scope.vs.chequeLeaves = {};
			$('#chequeNoId').attr("disabled","disabled");
		}else if($scope.vs.payBy === 'Q'){
			$('#chequeTypeId').removeAttr("disabled");
			$scope.vs.bankCode = "";
			$('#bankCodeId').attr("disabled","disabled");
		}else if($scope.vs.payBy === 'R'){
			$('#bankCodeId').removeAttr("disabled");
			$scope.vs.chequeLeaves = {};
			$('#chequeNoId').attr("disabled","disabled");
			$scope.vs.chequeType = '';
			$('#chequeTypeId').attr("disabled","disabled");
		}else{
			console.log("invaid entry");
		}
	}
	
	
	$scope.selectChqType = function(){
		console.log("enter into selectChqType function");
		$('#bankCodeId').removeAttr("disabled");
	}
	
	
	$scope.openBankCodeDB = function(){
		console.log("enter into openBankCodeDB function");
		$scope.bankCodeDBFlag = false;
		$('div#bankCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Bank Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.bankCodeDBFlag = true;
		    }
			});
		
		$('div#bankCodeDB').dialog('open');	
	}
	
	
	$scope.saveBankCode =  function(bankCode){
		console.log("enter into saveBankCode----->"+bankCode);
		$scope.vs.bankCode = bankCode;
		$('div#bankCodeDB').dialog('close');
		$scope.bankCodeDBFlag = true;
			
		if($scope.vs.payBy === 'Q'){
			if(angular.isUndefined($scope.vs.chequeType) ||  $scope.vs.chequeType === null || $scope.vs.chequeType === ""){
				$scope.alertToast("please select cheque type");
			}else{
				var chqDet = {
						"bankCode" : bankCode,
						"CType"    : $scope.vs.chequeType
				};
				
				var response = $http.post($scope.projectName+'/getChequeNoFrLHSP',chqDet);
				response.success(function(data, status, headers, config){
					if(data.result === "success"){
						console.log("Cheque Leaves List--->>"+ data.chequeLeaves);
						$('#chequeNoId').removeAttr("disabled");
						$scope.vs.chequeLeaves = data.chequeLeaves;
						$scope.chqList = data.list;
						$scope.maxChqAmnt = data.chequeLeaves.chqLChqMaxLt;
					}else{
						$scope.alertToast("Branch does not have '"+$scope.vs.chequeType+"' type cheeque of Bank "+bankCode);
						console.log("Error in bringing data from getChequeNo");
					}
				});
				response.error(function(data, status, headers, config){
					console.log(data);
				});
			}
			
		}
		
 }
	
	$scope.openChqNoDB = function(){
		 console.log("enter into openChqNoDB function");
		 $scope.chqNoDBFlag = false;
	    	$('div#chqNoDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			        $scope.chqNoDBFlag = true;
			    }
				});
			$('div#chqNoDB').dialog('open');
	 }
	
	$scope.saveChqNo = function(chq){
		 console.log("enter into saveChqNo function = "+chq.chqLChqNo);
		 $scope.vs.chequeLeaves = chq;
		 $('div#chqNoDB').dialog('close');
	 } 
	
	$scope.openBrkOwnDB = function(){
		 console.log("enter into openBrkOwnDB funciton");
		 $scope.brkOwnDBFlag = false;
	    	$('div#brkOwnDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			        $scope.brkOwnDBFlag = true;
			    }
				});
			$('div#brkOwnDB').dialog('open'); 
	 }
	
	$scope.saveBOCode = function(faCode){
		 console.log("enter into saveBOCode function");
		 $scope.lspFinalTotSum=0;
		 $scope.vs.brkOwnFaCode = faCode;
		 //$('div#brkOwnDB').dialog('close'); 
		 $scope.lhpvSupList = [];
	 }
	
	
	$scope.openChlnDB = function(){
		 console.log("enter into openChlnDB funciton");
		 if(angular.isUndefined($scope.vs.brkOwnFaCode) || $scope.vs.brkOwnFaCode === "" || $scope.vs.brkOwnFaCode === null){
			 $scope.alertToast("Please select Owner/Broker");
		 }else{
			 $scope.chlnDBFlag = false;
		    	$('div#chlnDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					show: UDShow,
					Title: "Select Challan Code",
					hide: UDHide,
					position: UDPos,
					draggable: true,
					close: function(event, ui) { 
				        $(this).dialog('destroy');
				        $(this).hide();
				        $scope.chlnDBFlag = true;
				    }
					});
				$('div#chlnDB').dialog('open'); 
		 }
	 }
	
	
	 $scope.saveChln = function(chlnCode){
		 console.log("enter into saveChln fucniton");
		 $scope.challan = chlnCode;
		 
		 var duplicate = false;
		 if($scope.lhpvSupList.length > 0){
			 for(var i=0;i<$scope.lhpvSupList.length;i++){
				if($scope.lhpvSupList[i].challan.chlnCode === chlnCode){
					duplicate = true;
					break;
				}
			 }
		 }
		 
		 if(duplicate === false){
			 /*var data = {
					 "chlnCode"     : $scope.challan,
					 "brkOwnFaCode" : $scope.vs.brkOwnFaCode
			 };*/
			 
			 var response = $http.post($scope.projectName+'/getChlnFrLSP',$scope.challan);
				response.success(function(data, status, headers, config){
					console.log(data);
					if(data.result === "success"){
						//$('div#chlnDB').dialog('close'); 
						console.log("data.arHoAlw === "+data.arHoAlw);

						$scope.actChln = data.chln;
						$scope.lhpvSup.challan = $scope.actChln;
					
						if(data.arHoAlw === true){
							if(data.wtShrtgF === true){
								$scope.lhpvSup.lspWtShrtgCR  = data.wtShrtg;
								$('#wtSrtgId').removeAttr("disabled");
							}else{
								$('#wtSrtgId').attr("disabled","disabled");
								$scope.lhpvSup.lspWtShrtgCR  = 0;
							}
							
							if(data.drRcvrWtF === true){
								$scope.lhpvSup.lspDrRcvrWtCR  = data.drRcvrWt;
								$('#drRcvrId').removeAttr("disabled");
							}else{
								$('#drRcvrId').attr("disabled","disabled");
								$scope.lhpvSup.lspDrRcvrWtCR  = 0;
							}
							
							if(data.ltDelF === true){
								$scope.lhpvSup.lspLateDelCR  = data.ltDel;
								$('#ltDelId').removeAttr("disabled");
							}else{
								$('#ltDelId').attr("disabled","disabled");
								$scope.lhpvSup.lspLateDelCR  = 0;
							}
							
							if(data.ltAckF === true){
								$scope.lhpvSup.lspLateAckCR  = data.ltAck;
								$('#ltAckId').removeAttr("disabled");
							}else{
								$('#ltAckId').attr("disabled","disabled");
								$scope.lhpvSup.lspLateAckCR  = 0;
							}
							
							if(data.exKmF === true){
								$scope.lhpvSup.lspOthExtKmP  = data.exKm;
								$('#exKmId').removeAttr("disabled");
							}else{
								$('#exKmId').attr("disabled","disabled");
								$scope.lhpvSup.lspOthExtKmP  = 0;
							}
							
							if(data.ovrHgtF === true){
								$scope.lhpvSup.lspOthOvrHgtP = data.ovrHgt;
								$('#ovrHgtId').removeAttr("disabled");
							}else{
								$('#ovrHgtId').attr("disabled","disabled");
								$scope.lhpvSup.lspOthOvrHgtP = 0;
							}
							
							if(data.penaltyF === true){
								$scope.lhpvSup.lspOthPnltyP  = data.penalty;
								$('#penId').removeAttr("disabled");
							}else{
								$('#penId').attr("disabled","disabled");
								$scope.lhpvSup.lspOthPnltyP  = 0;
							}
							
							if(data.othF === true){
								$scope.lhpvSup.lspOthMiscP   = data.oth;
								$('#othId').removeAttr("disabled");
							}else{
								$('#othId').attr("disabled","disabled");
								$scope.lhpvSup.lspOthMiscP   = 0;
							}
							
							if(data.detF === true){
								$scope.lhpvSup.lspUnpDetP    = data.det;
								$('#detId').removeAttr("disabled");
							}else{
								$('#detId').attr("disabled","disabled");
								$scope.lhpvSup.lspUnpDetP    = 0;
							}
							
							if(data.unLdgF === true){
								$scope.lhpvSup.lspUnLoadingP = data.unLdg;
								$('#unLdgId').removeAttr("disabled");
							}else{
								$('#unLdgId').attr("disabled","disabled");
								$scope.lhpvSup.lspUnLoadingP = 0;
							}
						
					
							/*$scope.lhpvSup.lspTotPayAmt = $scope.lhpvSup.lspOthExtKmP + $scope.lhpvSup.lspOthOvrHgtP 
															+ $scope.lhpvSup.lspOthPnltyP + $scope.lhpvSup.lspOthMiscP + $scope.lhpvSup.lspUnpDetP
																+ $scope.lhpvSup.lspUnLoadingP;*/

							$scope.lhpvSup.lspTotPayAmt = $scope.lhpvSup.lspOthExtKmP + $scope.lhpvSup.lspOthOvrHgtP 
															+ $scope.lhpvSup.lspOthPnltyP + $scope.lhpvSup.lspUnpDetP
																+ $scope.lhpvSup.lspUnLoadingP;
							
							$scope.lhpvSup.lspTotRcvrAmt = $scope.lhpvSup.lspWtShrtgCR + $scope.lhpvSup.lspDrRcvrWtCR + $scope.lhpvSup.lspLateDelCR +
															$scope.lhpvSup.lspLateAckCR + + $scope.lhpvSup.lspOthMiscP; 
															
	
	
							$scope.lhpvSup.lspFinalTot = $scope.lhpvSup.lspTotPayAmt - $scope.lhpvSup.lspTotRcvrAmt;
							
						
							$scope.lhpvSupDBFlag = false;
						    	$('div#lhpvSupDB').dialog({
									autoOpen: false,
									modal:true,
									resizable: false,
									show: UDShow,
									title:"LHPV SUPPLEMENTARY FOR CHALLAN ("+$scope.challan+")",
									hide: UDHide,
									position: UDPos,
									draggable: true,
									close: function(event, ui) { 
								        $(this).dialog('destroy');
								        $(this).hide();
								        $scope.lhpvSupDBFlag = true;
								    }
									});
							$('div#lhpvSupDB').dialog('open'); 
						}else{
							$scope.alertToast("Head Office does not allow Lhpv Supplementary for Challan "+$scope.challan);
						}
					}else{
						if(!angular.isUndefined(data.msg)){
							$scope.alertToast(data.msg);
						}else{
							$scope.alertToast("server error");
						}
						console.log("error in fetching getChlnFrLA data");
					}
				});
				response.error(function(data, status, headers, config){
					console.log(data);
				});
		 }else{
			 $scope.alertToast("Already create the lhpv supplementary for challan "+$scope.challan);
		 }
	 }
	 
	 
	 
	 $scope.submitLhpvSup = function(lhpvSupForm,lhSup){
		 console.log("enter into submitLhpvSup function = "+lhpvSupForm.$invalid);
		 if(lhpvSupForm.$invalid){
			 $scope.alertToast("Please fill correct form");
		 }else{
			 $scope.lspFinalTotSum=$scope.lspFinalTotSum+$scope.lhpvSup.lspFinalTot;
			 $scope.lhpvSupList.push(lhSup);
			 $scope.lhpvSup = {};
			 $('div#lhpvSupDB').dialog('close'); 
		 }
	 }
	 
	 
	 $scope.removeLhpvSup = function(index,lhpvsup){
		 console.log("enter into removeLhpvSup function = "+index);
		 $scope.lspFinalTotSum=$scope.lspFinalTotSum-lhpvsup.lspFinalTot;
		 $scope.lhpvSupList.splice(index,1);
	 }
	 
	 
	 $scope.voucherSubmit = function(voucherForm){
		 console.log("enter into voucherForm function = "+voucherForm.$invalid);
		 if(voucherForm.$invalid){
			$scope.alertToast("Please enter correct form");
		 }else{
			 console.log("final submittion");
			    $scope.saveVsFlag = false;
		    	$('div#saveVsDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					show: UDShow,
					title:"LHPV BALANCE FINAL SUBMISSION",
					hide: UDHide,
					position: UDPos,
					draggable: true,
					close: function(event, ui) { 
				        $(this).dialog('destroy');
				        $(this).hide();
				        $scope.saveVsFlag = true;
				    }
					});
				$('div#saveVsDB').dialog('open');
		 }
	 }	 
	 
	 
	 
	 $scope.back = function(){
		 console.log("enter into back function");
		 $('div#saveVsDB').dialog('close');
	 } 
	 
	 
	 $scope.saveVS = function(){
		 console.log("enter into saveVS function ");
			$scope.vs.lhpvSupList = $scope.lhpvSupList;
			$scope.lspFinalTotSum=0;
			faCodeTemp='';
			$('#saveId').attr("disabled","disabled");
			var response = $http.post($scope.projectName+'/submitLhpvSup',$scope.vs);
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					$('div#saveVsDB').dialog('close');
					$('#saveId').removeAttr("disabled");
					
					$scope.alertToast("Successfully save LHPV SUPPLEMENTARY -->> "+data.lspNo);
					
					$scope.vs = {};
					$scope.lhpvSup = {};
					$scope.bnkList = [];
					$scope.chlnList = [];
					$scope.actBOList = [];
					$scope.lhpvSupList = [];
					
					$scope.challan = "";
					$scope.vs.chequeType = '';
					$('#chequeTypeId').attr("disabled","disabled");
					$scope.vs.bankCode = "";
					$('#bankCodeId').attr("disabled","disabled");
					$scope.vs.chequeLeaves = {};
					$('#chequeNoId').attr("disabled","disabled");
					
					$scope.getLhpvDet();
				}else{
					console.log("Error in submitVoucher");
				}
			});
			response.error(function(data, status, headers, config){
				console.log(data);
			});
	 }
	 
	 
	 $scope.verifyChallan = function() {
			console.log("verifyChallan()");
			if (angular.isUndefined($scope.challan) || $scope.challan === "" || $scope.challan === null) {
				$scope.alertToast("Please Enter Challan No");
			} else {
				
				$scope.lodingFlag = true;
				
				var res = $http.post($scope.projectName+'/verifyChlnForLhpvSup',$scope.challan);
				res.success(function(data, status, headers, config) {
					if (data.result === "success") {
						console.log("enter into saveBOCode function");
						if (angular.isUndefined(faCodeTemp) || faCodeTemp === "" || faCodeTemp === null) {
							faCodeTemp = data.faCode;
							$scope.lspFinalTotSum=0;
							$scope.vs.brkOwnFaCode = data.faCode;
							$scope.lhpvSupList = [];
						} else {
							if (faCodeTemp === data.faCode) {
							} else {
								faCodeTemp = data.faCode;
								$scope.lspFinalTotSum=0;
								$scope.vs.brkOwnFaCode = data.faCode;
								$scope.lhpvSupList = [];
							}
						}
						
						$scope.saveChln($scope.challan);
					} else {
						$scope.alertToast(data.msg);
					}
					$scope.lodingFlag = false;
				});
				res.error(function(data, status, headers, config) {
					console.log("error in response of verifyChlnForLhpvAdv: "+data);
					$scope.lodingFlag = false;
				});
			}
		}
		
		 $scope.notifyMsg = function() {
			console.log("notifyMsg");
			if (angular.isUndefined($scope.vs.brkOwnFaCode) || $scope.vs.brkOwnFaCode === "" || $scope.vs.brkOwnFaCode === null) {
				$scope.alertToast("Please Entre Challan No and Verify Challan");
			}		
		}
	 
	
	 
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		 $scope.getLhpvDet();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 } 
}]);