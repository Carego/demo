package com.mylogistics.DAO;

import java.util.List;

import org.hibernate.Session;

import com.mylogistics.model.MasterStationaryStk;
import com.mylogistics.model.User;

public interface MasterStnStkDao {
	public List<MasterStationaryStk> getMasterStnStk(Session session, User user, String branchCode, String minNo, String maxNo, String type);
}
