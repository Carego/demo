package com.mylogistics.constants;

public class SupBillCNTS {

	public static final String USER_BRANCH_CODE = "bCode";
	
	public static final String CREATION_TS = "creationTS";
	
	public static final String SUP_BILL_ID = "supBillId";
	
	public static final String SUP_BILL_CUST_CODE = "sbCustCode";
	
	public static final String SUP_BILL_TYPE = "sbBillType";
	
	public static final String SUP_BILL_FROM_DT = "sbFromDt";
	
	public static final String SUP_BILL_TO_DT = "sbToDt";
	
	public static final String SUP_BILL_CHARGES = "sbCharges";
	
	public static final String SUP_BILL_LORRY_NO = "sbLorryNo";
	
	public static final String SUP_BILL_REMARKS = "sbRemarks";
	
	public static final String SUP_BILL_CURRENCY = "sbCurrency";
	
	public static final String SUP_BILL_ADDRESS_ID = "sbAddressId";
	
	public static final String SUP_BILL_NO = "sbBillNo";
	
	public static final String USER_CODE = "userCode";
}
