<div ng-show="operatorLogin || superAdminLogin">

	<div class="noprint">
		<form name="CashStmtForm" ng-submit="submitCS(CashStmtForm)" class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
		    	<div class="col s3 input-field">
		    		<input class="validate" type ="text" id="branchName" name ="branchName" ng-model="cs.csBranchName" ng-click="openBrhDB()" readonly ng-required="true" >
		       		<input class="validate" type ="hidden" id="branchId" name ="branchId" ng-model="cs.csBranchId" ng-required="true" >
		       		<label for="code">Branch Name</label>	
		       	</div>
		       	<div class="col s3 input-field">
		       		<input class="validate" type ="date" id="fromDate" name ="fromDate" ng-model="cs.csFromDate" ng-required="true">
		       		<label for="code">From Date</label>	
		       	</div>
		       	<div class="col s3 input-field">
		       		<input class="validate" type ="date" id="toDate" name ="toDate" ng-model="cs.csToDate" ng-required="true">
		       		<label for="code">To Date</label>	
		       	</div>	       			       		
		       	<div class="col s3 input-field">
		       		<input class="validate" type ="submit" value="Submit">
		       	</div>
		    </div> 
		</form>
    </div>

	<div id ="brhId" ng-hide="brhFlag">
		<input type="text" name="filterBrhbox" ng-model="filterBrhbox" placeholder="Search...">
 	  	<table>
 	  	  <tr>
 	  	  	  <th></th> 	  	
 	  	  	  <th> Branch Name </th>
 	  	  	  <th> Branch FaCode</th>
 	  	  </tr>
		  <tr ng-repeat="branch in brhList | filter:filterBrhbox">
		 	  <td><input type="radio" name="branch" ng-click="saveBranch(branch)"></td>              
              <td>{{branch.branchName}}</td>
              <td>{{branch.branchFaCode}}</td>
          </tr>
      </table>
	</div>

	
	
	<div id="newCsPrint">	
		<div style="align: center;margin-top: 50px;" ng-repeat="cs in csList">
			<div style="width: 100%;">
				<div style=" border-top:0px solid black; border-left:0px solid black;border-right:0px solid black;">
					<div style="width:70%; float:left; font-weight:bold; margin-top:5px;font-size:12px;font-family: 'Arial';">
						<span style=" font-size:15px;">{{cmpnyName}}</span><br/>
						Cash Statement<br/>
						CS Print Date: {{date | date:'dd/MM/yyyy'}}  Time:{{date | date:'HH:mm:ss'}}<br/>
					</div>			
					<div style="width:29%; float:right; font-weight:bold; margin-top:5px;font-size:12px;font-family: 'Arial';">
						Branch Code:{{cs.branchName}}<br/>
						Sheet No: {{cs.sheetNo}}<br clear="all"/>
						Date: {{cs.csDate | date:'dd/MM/yyyy'}}											
					</div>
				</div>
			</div>
			<br clear="all"/></br>			
			<div style="width: 100%;">				
					<table style="border-collapse: separate;border-spacing: 0px; width:100%; font-family: 'Arial'; width: 100%; border-top: 2px solid black; border-bottom: 2px solid black; border-left: 1px solid black; border-right: 1px solid black;">
						<tr>							
							<td style="font-weight:bold; font-size:12px; height:30px; border-bottom: 1px solid black; padding-left: 10%;" colspan="6">VOUCHER TYPE : </td>
							<td style="font-weight:bold; font-size:12px; height:30px; border-bottom: 1px solid black;" colspan="2">CASH</td>
						</tr>
						<tr style="font-weight:bold;font-size:10px;">
							<td style="width: 40px;border-bottom: 1px solid black;padding: 4px;">CODE</td>
							<td style="width: 40px;border-bottom: 1px solid black;padding: 4px;">ACHEAD</td>
							<td style="width: 80px;border-bottom: 1px solid black;padding: 4px;">VCH TYPE</td>
							<td style="width: 292px;border-bottom: 1px solid black;padding: 4px; text-align:center;">DESCRIPTION</td>
							<td style="width: 100px;border-bottom: 1px solid black;padding: 4px;">TvNo</td>
							<td style="width: 20px;border-bottom: 1px solid black;padding: 4px; text-align:right;">VNo</td>
							<td style="width: 115px;border-bottom: 1px solid black;padding: 4px; text-align:right;">RECEIPT AMT</td>
							<td style="width: 115px;border-bottom: 1px solid black;padding: 4px; text-align:right;">PAYMENT AMT</td>
						</tr>
						<tr style="font-weight:bold;font-size:10px;">
							<td style="border-bottom: 1px solid black; padding-left: 10%; padding-top: 4px; padding-bottom: 4px;" colspan="6">CASH OPENING BALANCE</td>
							<td style="border-bottom: 1px solid black; text-align:right; padding: 4px;">{{ cs.cashOpeningAmt | number:2}}</td>
							<td style="border-bottom: 1px solid black; text-align:right; padding: 4px;"></td>
						</tr>
						<tr style="font-size:10px;" ng-repeat="cash in cs.cashList">
							<td style="border-bottom: 1px solid black;padding:4px;">{{cash.csFaCode}}</td>
							<td style="border-bottom: 1px solid black;padding:4px;">{{cash.csName}}</td>							
							<td style="border-bottom: 1px solid black;padding:4px;">{{cash.csType}}</td>
							<td style="border-bottom: 1px solid black;padding:4px; text-align:center;">{{cash.csDescription}}</td>
							<td style="border-bottom: 1px solid black;padding:4px;">{{cash.csTvNo}}</td>
							<td style="border-bottom: 1px solid black;padding:4px; text-align:right;">{{cash.csVouchNo}}</td>
							<td style="border-bottom: 1px solid black;padding:4px; text-align:right;">{{cash.csCr | number:2}}</td>
							<td style="border-bottom: 1px solid black;padding:4px; text-align:right;">{{cash.csDr | number:2}}</td>
						</tr>
						<tr style="font-weight:bold;font-size:10px;">
							<td style="border-bottom: 1px solid black;padding-left: 10%; padding-top: 4px; padding-bottom: 4px;" colspan="7">CASH CLOSING BALANCE</td>							
							<td style="border-bottom: 1px solid black; padding:4px; text-align:right;">{{ cs.cashClosingAmt | number:2}}</td>
						</tr>
						<tr style="font-weight:bold;font-size:10px;">
							<td style="padding-left: 10%;padding-top: 4px; padding-bottom: 4px;" colspan="6">VOUCHER TOTAL</td>
							<td style="text-align:right; padding: 4px;">{{ cs.cashTotCr | number:2}}</td>
							<td style="text-align:right; padding: 4px;">{{ cs.cashTotDr | number:2}}</td>
						</tr>				
					</table>
					
					<table ng-repeat="bank in cs.bankList" style="margin-top : 20px; border-collapse: separate;border-spacing: 0px; width:100%; font-family: 'Arial'; width: 100%; border-top: 2px solid black; border-bottom: 2px solid black; border-left: 1px solid black; border-right: 1px solid black;">
						<tr style="font-weight:bold;font-size:12px;">
							<td style="border-bottom: 1px solid black;padding-left: 10%;height: 30px;" colspan="3">VOUCHER TYPE : </td>
							<td style="border-bottom: 1px solid black;height: 30px;" colspan="1">BANK</td>
							<td style="border-bottom: 1px solid black;height: 30px;" colspan="3">{{bank.bankName}}</td>
							<td style="border-bottom: 1px solid black;height: 30px;" colspan="1">{{bank.bankCode}}</td>
						</tr>
						<tr style="font-weight:bold;font-size:10px;">
						
							<td style="width: 40px;border-bottom: 1px solid black;padding: 4px;">CODE</td>
							<td style="width: 40px;border-bottom: 1px solid black;padding: 4px;">ACHEAD</td>
							<td style="width: 80px;border-bottom: 1px solid black;padding: 4px;">VCH TYPE</td>
							<td style="width: 292px;border-bottom: 1px solid black;padding: 4px; text-align:center;">DESCRIPTION</td>
							<td style="width: 100px;border-bottom: 1px solid black;padding: 4px;">TvNo</td>
							<td style="width: 20px;border-bottom: 1px solid black;padding: 4px; text-align:right;">VNo</td>
							<td style="width: 115px;border-bottom: 1px solid black;padding: 4px; text-align:right;">RECEIPT AMT</td>
							<td style="width: 115px;border-bottom: 1px solid black;padding: 4px; text-align:right;">PAYMENT AMT</td>
						</tr>
						<tr style="font-weight:bold;font-size:10px;">
							<td style="border-bottom: 1px solid black; padding-left: 10%;padding-top: 4px; padding-bottom: 4px;" colspan="6">BANK OPENING BALANCE</td>
							<td style="border-bottom: 1px solid black;padding: 4px; text-align:right;">{{ bank.bankOpeningAmt | number:2 }}</td>
							<td style="border-bottom: 1px solid black;padding: 4px;"></td>
						</tr>
						<tr style="font-size:10px;" ng-repeat="bankCs in bank.bankCs">
							<td style="border-bottom: 1px solid black;padding:4px;">{{bankCs.csFaCode}}</td>
							<td style="border-bottom: 1px solid black;padding:4px;">{{bankCs.csFaName}}</td>		
							<td style="border-bottom: 1px solid black;padding:4px;">{{bankCs.csType}}</td>
							<td style="border-bottom: 1px solid black;padding:4px; text-align:center;">{{bankCs.csDescription}}</td>
							<td style="border-bottom: 1px solid black;padding:4px;">{{bankCs.csTvNo}}</td>
							<td style="border-bottom: 1px solid black;padding:4px; text-align:right;">{{bankCs.csVouchNo}}</td>
							<td style="border-bottom: 1px solid black;padding:4px; text-align:right;">{{bankCs.csCr | number:2}}</td>
							<td style="border-bottom: 1px solid black;padding:4px; text-align:right;">{{bankCs.csDr | number:2}}</td>
						</tr>
						<tr style="font-weight:bold;font-size:10px;">
							<td style="border-bottom: 1px solid black; padding-left: 10%;padding-top: 4px;padding-botton: 4px;" colspan="6">BANK CLOSING BALANCE</td>
							<td style="border-bottom: 1px solid black;padding: 4px;"></td>
							<td style="border-bottom: 1px solid black;padding: 4px; text-align:right;">{{ bank.bankClosingAmt | number:2}}</td>
						</tr>
						<tr style="font-weight:bold;font-size:10px;">
							<td style="padding-left: 10%;padding-top: 4px;padding-botton: 4px;" colspan="6">VOUCHER TOTAL</td>
							<td style="text-align:right;padding: 4px;">{{ bank.totCr | number:2}}</td>
							<td style="text-align:right;padding: 4px;">{{ bank.totDr | number:2}}</td>
						</tr>
					</table>
					
					<table style="margin-top : 20px; border-collapse: separate;border-spacing: 0px; width:100%; font-family: 'Arial'; width: 100%; border-top: 2px solid black; border-bottom: 2px solid black; border-left: 1px solid black; border-right: 1px solid black;">
						<tr style="font-weight:bold; font-size: 12px;">						
							<td style="border-bottom: 1px solid black;padding-left: 10%; height: 30px;" colspan="6">VOUCHER TYPE : </td>
							<td style="border-bottom: 1px solid black;height: 30px; " colspan="2">CONTRA</td>
						</tr>
						<tr style="font-weight:bold; font-size: 10px;">
							<td style="width: 40px;border-bottom: 1px solid black;padding: 4px;">CODE</td>
							<td style="width: 40px;border-bottom: 1px solid black;padding: 4px;">ACHEAD</td>
							<td style="width: 80px;border-bottom: 1px solid black;padding: 4px;">VCH TYPE</td>
							<td style="width: 292px;border-bottom: 1px solid black;padding: 4px; text-align:center;">DESCRIPTION</td>
							<td style="width: 100px;border-bottom: 1px solid black;padding: 4px;">TvNo</td>
							<td style="width: 20px;border-bottom: 1px solid black;padding: 4px; text-align:right;">VNo</td>
							<td style="width: 115px;border-bottom: 1px solid black;padding: 4px; text-align:right;">RECEIPT AMT</td>
							<td style="width: 115px;border-bottom: 1px solid black;padding: 4px; text-align:right;">PAYMENT AMT</td>
						</tr>
						<tr style="font-size: 10px;" ng-repeat="contraCs in contraCsList">
							<td style="width: 40px;border-bottom: 1px solid black;padding: 4px;">{{contraCs.csFaCode}}</td>
							<td style="width: 40px;border-bottom: 1px solid black;padding: 4px;">
								<div ng-repeat="acHdCont in acHdList">
									<div ng-if="acHdCont.faCode == contraCs.csFaCode">
										{{ acHdCont.faName }}
									</div>
								</div>
							</td>		
							<td style="width: 80px;border-bottom: 1px solid black;padding: 4px;">{{contraCs.csType}}</td>
							<td style="width: 292px;border-bottom: 1px solid black;padding: 4px; text-align:center;">{{contraCs.csDescription}}</td>
							<td style="width: 100px;border-bottom: 1px solid black;padding: 4px;">{{contraCs.csTvNo}}</td>
							<td style="width: 20px;border-bottom: 1px solid black;padding: 4px; text-align:right;">{{contraCs.csVouchNo}}</td>
							<td style="width: 115px;border-bottom: 1px solid black;padding: 4px; text-align:right;">
								<div ng-if="contraCs.csDrCr == 'C'">
									{{contraCs.csAmt | number:2}}
								</div>
							</td>
							<td style="width: 115px;border-bottom: 1px solid black;padding: 4px; text-align:right;">
								<div ng-if="contraCs.csDrCr == 'D'">
									{{contraCs.csAmt | number:2}}
								</div>
							</td>						
						</tr>
					</table>					
				</div>
			
			<div style="width: 100% !important; font-family: 'Arial'; font-size:12px;">
				<div style="border-top:0px solid black; font-weight:bold; border-left:0px solid black;border-right:0px solid black;">
					<div>
						<div style="margin-top:10px;">CS Verfied by:</div>
						<div style="margin-top:15px;">Branch Incharge:</div>
						<div style="margin-top:10px;">Staff Code:</div>					
					</div>				
				    <div align="right" style="margin-top:-75px; text-align:left;  margin-left:70%;">
						<div style="margin-top:0px;">Cashier:</div>
						<div style="margin-top:10px;">Staff Code:</div>
					</div>				
				</div>
			</div>
			
		</div>
	</div>
	
	
	<div id="printVsDB" ng-hide="printVsFlag" class="noprint">
		<div class="row">
			<div class="col s12 center"><h5 class="white-text">{{PRINT_STMT}}</h5></div>
		</div>
		<div>
			<div class="col s12 center">
				<a class="btn white-text" ng-click="cancelPrint()">Cancel</a>
				<a class="btn white-text" ng-click="print()">Yes</a>
			</div>
		</div>
	</div>
	
				
</div>
