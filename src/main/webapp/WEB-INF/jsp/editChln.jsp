<div ng-show="operatorLogin || superAdminLogin">
<title>Edit Challan</title>
<div class="row">
	<div class="col s1 hide-on-med-and-down">&nbsp;</div>
	<div class="col s10 card"
		style="align: center; margin-top: 40px; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">

		<div class="input-field col s6">
			<input type="text" id="chlnCodeId" name="chlnCodeName" ng-model="chln.chlnCode" ng-required="true"/> 
			<label>Enter Challan Code</label>
		</div>

		<div class="input-field col s6 center">
			<input class="btn waves-effect waves-light" type="submit" value="Submit" ng-click="getChln()">
		</div>
	</div>

</div>

<div class="row" ng-show="showChlnFlag">

<form name="chlnForm" ng-submit="submitChallan(chlnForm)" >
	<div class="col s12 card"style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
				<!-- <div class="input-field col s3">
					<input class="validate" type="text" name="branchCodeName" ng-model="chln.branchCode" readonly required> 
					<label>Branch Code</label>
				</div> -->
				<div class="input-field col s3">
					<input type="text" id="branch" name="branchName" ng-model="branch.branchName" ng-click="openBranchDB()" ng-required="true" readonly>
					<label>Branch</label>
				</div>
				
				<div class="input-field col s3">
				 	<input class="validate" type ="text" name ="frmStnName" ng-model="frmStn.stnName" ng-click="openFrmStnDB()" readonly  ng-required="true">
	       			<label>From Station</label>
				</div>
				
				<div class="input-field col s3">
					<input class="validate" type ="text" name ="toStnName" ng-model="toStn.stnName" ng-click="openToStnDB()" readonly  ng-required="true">
	       			<label>To Station</label>
				</div>
				
				<div class="input-field col s3">
					<input class="validate" type ="text" name ="empName" ng-model="emp.empName" ng-click="openEmpDB()" readonly ng-required="true">
		        	<label>Employee Code</label>
				</div>
			</div>

			<div class="row">
				<div class="input-field col s3">
					<input class="validate" type="text" name="chlnNoOfPkgName" ng-model="chln.chlnNoOfPkg" ng-minlength="1" ng-maxlength="10" ng-required="true">
					<label>No. Of Package</label>
				</div>
				
				<div class="input-field col s3">
					<input class="validate" type="text" name="chlnLryRateName" ng-model="chln.chlnLryRate" ng-keyup="calFreight()" ng-minlength="1" ng-maxlength="10" ng-required="true">
					<label>Lorry Rate</label>
				</div>	
				
				<div class="input-field col s3">
					<input class="validate" type="text" name="chlnChgWtName" ng-model="chln.chlnChgWt" ng-keyup="calFreight()" ng-minlength="1" ng-maxlength="10" ng-required="true">
					<label>Charge Weight</label>
				</div>
				<div class="input-field col s3">
					<input class="validate" type="text" name="chlnFreightName"  ng-model="chln.chlnFreight" ng-minlength="1" ng-maxlength="10" ng-required="true" readonly="readonly">
					<label>Freight</label>
				</div>
			</div>

			<div class="row">
				<div class="input-field col s3">
					<input class="validate" type="text" name="chlnLoadingAmtName" ng-model="chln.chlnLoadingAmt" ng-keyup="calTotalFreight()" ng-minlength="1" ng-maxlength="10" ng-required="true">
					<label>Loading Amount</label>
				</div>
				
				<div class="input-field col s3">
					<input class="validate" type="text" name="chlnExtraName" ng-model="chln.chlnExtra" ng-keyup="calTotalFreight()" ng-minlength="1"	ng-maxlength="10" ng-required="true">
					<label>Extra</label>
				</div>
				<div class="input-field col s3">
					<input class="validate" type="text" name="chlnStatisticalChgName" ng-model="chln.chlnStatisticalChg" ng-keyup="calTotalFreight()" ng-minlength="1" ng-maxlength="10" ng-required="true">
					<label>Statistical Charge</label>
				</div>
				<div class="input-field col s3">
					<input class="validate" type="text" name="chlnTdsAmtName" ng-model="chln.chlnTdsAmt" ng-keyup="calTotalFreight()" ng-minlength="1" ng-maxlength="10" ng-required="true">
					<label>TDS Amount</label>
				</div>
			</div>

			<div class="row">
				<div class="input-field col s3">
					<input class="validate" type="number" name="chlnTotalFreightName" ng-model="chln.chlnTotalFreight" ng-minlength="1" ng-maxlength="10" step="0.00001" min="0.00000" readonly>
					<label>Total Freight</label>
				</div>
				<div class="input-field col s3">
					<input class="validate" type="text" name="chlnAdvanceName" ng-model="chln.chlnAdvance" ng-keyup="calBalance()" step="0.00001" min="0.00000" ng-minlength="1" ng-maxlength="10" ng-required="true">
					<label>Advance</label>
				</div>
				<div class="input-field col s3">
					<input class="validate" type="number" name="chlnBalanceName" ng-model="chln.chlnBalance" step="0.00001" min="0.00000" ng-minlength="1" ng-maxlength="10" readonly ng-required="true">
					<label>Balance</label>
				</div>
				<div class="input-field col s3">
					<input class="validate" type ="text" name ="cnmtPayAtName" ng-model="payAt.branchName" ng-click="openPayAtDB()" readonly="readonly">
	        		<label>Pay At</label>
				</div>
			</div>
			
			<div class="row">
				<div class="input-field col s3">
					<input class="validate" type="text" name="chlnTimeAllowName" ng-model="chln.chlnTimeAllow" ng-minlength="1" ng-maxlength="2">
					<label>Time Allow</label>
				</div>
				<div class="input-field col s3">
					<input class="validate" type="date" name="chlnDtName" ng-model="chln.chlnDt" ng-required="true">
					<label>Challan Date</label>
				</div>
				<div class="input-field col s3">
					<input class="validate" type="text" name="chlnBrRateName" ng-model="chln.chlnBrRate" ng-minlength="1" ng-maxlength="10" ng-required="true">
					<label>BR Rate</label>
				</div>
				<div class="input-field col s3">
					<input class="validate" type="text" name="chlnTotalWtName" ng-model="chln.chlnTotalWt" ng-minlength="1" ng-maxlength="10" ng-required="true">
					<label>Total Weight</label>
				</div>
			</div>

			<div class="row">
				<div class="input-field col s3">
					<select name="chlnWtSlipName" ng-model="chln.chlnWtSlip" ng-init="chln.chlnWtSlip='chln.chlnWtSlip'" ng-required="true">
						<option value="Yes">Yes</option>
						<option value="No">No</option>
					</select> <label>Weight Slip</label>
				</div>
				<div class="input-field col s3">
					<input class="validate" type="date" name="chlnLryRepDTName" ng-model="chln.chlnLryRepDT">
					<label>Lorry Reporing Date</label>
				</div>
				<div class="input-field col s3">
					<input class="validate" type="text" name="chlnVehicleTypeName" ng-model="chln.chlnVehicleType" ng-click="openVehicleTypeDB()" ng-required="true" readonly>
					<label>Vehicle Type</label>
				</div>
				<div class="input-field col s3">
					<input class="validate" type="text" name="chlnRrNo" ng-model="chln.chlnRrNo" >
					<label>Railway Reciept No</label>
				</div>
			
			</div>

			<div class="row">
				
				<div class="input-field col s3">
					<input class="validate" type="text" name="chlnTrainNoName" ng-model="chln.chlnTrainNo" >
					<label>Train No</label>
				</div>
				
				<div class="input-field col s3">
					<input class="validate" type="time" name="chlnLryLoadTimeName" ng-model="chln.chlnLryLoadTime" ng-required="true">
					<label>Lorry Load Time</label>
				</div>
				<div class="input-field col s3">
					<input class="validate" type="time"	name="chlnLryRptTimeName" ng-model="chln.chlnLryRptTime">
					<label>Lorry Reporting Time</label>
				</div>
				<div class="input-field col s3">
					<input class="validate" type="number" name="chlnRemAdvName" ng-model="chln.chlnRemAdv" step="0.00001" min="0.00000" ng-required="true">
					<label>Remaining Advance</label>
				</div>
				
			</div>
			
			<div class="row">
				<div class="input-field col s3">
					<input class="validate" type="number" name="chlnRemBalName" ng-model="chln.chlnRemBal" step="0.00001" min="0.00000" ng-required="true">
					<label>Remaining Balance</label>
				</div>
				
				<div class="input-field col s3">
					<input type ="text" id="vehId" name ="chlnLryNoName" ng-model="chln.chlnLryNo" readonly ng-required="true">
					<label>Lry No(RC No)</label>
				</div> 
			</div>

	</div>
	
	<div class="col s12 card"style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
			
			<div class="row">

				<div class="input-field col s3">
					<input type ="text" name ="chdRcNoName" ng-model="chd.chdRcNo" ng-click="openVehicleDB()" ng-click="openVehicleDB()" readonly ng-required="true">
					<label>Select Vehicle</label>
				</div> 
				
				<div class="input-field col s3">
					<input type="text" name="chdBrCodeName" ng-model="brk.brkName" ng-click="openBrkDB()" readonly ng-required="true">
					<label>Broker</label>
				</div>
				<div class="input-field col s3">
					<input type="text" name="chdOwnCodeName" ng-model="own.ownName" ng-click="openOwnDB()" readonly ng-required="true">
					<label>Owner</label>
				</div>
				
				<div class="input-field col s3">
					<input type="text" name="chdBrMobNoName" ng-model="chd.chdBrMobNo" >
					<label>Broker Mobile No</label> 
				</div>
			</div>

			<div class="row">

				<div class="input-field col s3">
					<input type="text" name="chdOwnMobNoName" ng-model="chd.chdOwnMobNo" >
					<label>Owner Mobile No</label> 
				</div>
				<div class="input-field col s3">
					<input type="text" name="chdDvrName" ng-model="chd.chdDvrName">
					<label>Driver Name</label>
				</div>
				<div class="input-field col s3">
					<input type="text" name="chdDvrMobNoName" ng-model="chd.chdDvrMobNo" ng-maxlength="10" ng-minlength="10">
					<label>Driver Mobile No</label>
				</div>
				<div class="input-field col s3">
					<input type="date" name="chdRcIssueDtName" ng-model="chd.chdRcIssueDt">
					<label>RC Issue Date</label>
				</div>
			</div>

			<div class="row">

				<div class="input-field col s3">
					<input type="date" name="chdRcValidDtName" ng-model="chd.chdRcValidDt">
					<label>RC Valid Date</label>
				</div>
				<div class="input-field col s3">
					<input type="text" name="chdDlNoName" ng-model="chd.chdDlNo" ng-minlength="3" ng-maxlength="50">
					<label>DL No</label>
				</div>
				<div class="input-field col s3">
					<input type="date" name="chdDlIssueDtName" ng-model="chd.chdDlIssueDt">
					<label>DL Issue Date</label>
				</div>
				<div class="input-field col s3">
					<input type="date" name="chdDlValidDtName" ng-model="chd.chdDlValidDt">
					<label>DL Valid Date</label>
				</div>
			</div>

			<div class="row">

				<div class="input-field col s3">
					<input type="text" name="chdPerNoName" ng-model="chd.chdPerNo" ng-minlength="3" ng-maxlength="50">
					<label>Per NO</label>
				</div>
				<div class="input-field col s3">
					<input type="date" name="chdPerIssueDtName"	ng-model="chd.chdPerIssueDt">
					<label>Per Issue Date</label>
				</div>
				<div class="input-field col s3">
					<input type="date" name="chdPerValidDtName" ng-model="chd.chdPerValidDt">
					<label>Per Valid Date</label>
				</div>
				<div class="input-field col s3">
					<input type="text" name="chdPerStateName" ng-model="chd.chdPerState" ng-click="openPerStateDB()"	readonly>
					<label>Per State</label> 
				</div>
			</div>

			<div class="row">
				<div class="input-field col s3">
					<input type="text" name="chdFitDocNoName" ng-model="chd.chdFitDocNo">
					 <label>Fit Doc	No</label>
				</div>
				<div class="input-field col s3">
					<input type="date" name="chdFitDocIssueDtName" ng-model="chd.chdFitDocIssueDt">
					<label>Fit Doc Issue Date</label>
				</div>
				<div class="input-field col s3">
					<input type="date" name="chdFitDocValidDtName" ng-model="chd.chdFitDocValidDt">
					<label>Fit Doc Valid Date</label>
				</div>
				<div class="input-field col s3">
					<input type="text" name="chdFitDocPlaceName" ng-model="chd.chdFitDocPlace">
					<label>Fit Doc Place</label>
				</div>
			</div>

			<div class="row">

				<div class="input-field col s3">
					<input type="text" name="chdBankFinanceName" ng-model="chd.chdBankFinance">
					<label>Bank Finance</label>
				</div>
				<div class="input-field col s3">
					<input type="text" name="chdPolicyNoName" ng-model="chd.chdPolicyNo" ng-minlength="3" ng-maxlength="50" >
					<label>Policy No</label>
				</div>
				<div class="input-field col s3">
					<input type="text" name="chdPolicyComName" ng-model="chd.chdPolicyCom">
					<label>Policy Com</label>
				</div>
				<div class="input-field col s3">
					<input type="text" name="chdTransitPassNoName" ng-model="chd.chdTransitPassNo">
					<label>Transit Pass No</label>
				</div>
			</div>

			<div class="row">

				<div class="input-field col s3">
					<select name="chdPanHdrTypeName" ng-model="chd.chdPanHdrType" ng-init=" chd.chdPanHdrType = 'Owner' ">
						<option value="Broker">Broker</option>
						<option value="Owner">Owner</option>
					</select> <label>Pass HDR Type</label>
				</div>
				<div class="input-field col s3">
					<input type="text" name="chdPanIssueStName" ng-model="chd.chdPanIssueSt" ng-click="openPanIssueStateDB()" readonly>
					<label>Pan Issue St</label> 
				</div>
				
			</div>

			<div class="row">
				<div class="col s12 center">
					<input class="btn" type="submit" value="Submit">
				</div>
			</div>
	</div>
</form>

</div>

	<div id ="chlnDB" ng-hide="chlnDBFlag">
		<input type="text" name="filterTextbox1" ng-model="filterTextbox1" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Challan No </th>
 	  	  </tr>
		  <tr ng-repeat="chlnCode in chlnNoList | filter:filterTextbox1">
		 	  <td><input type="radio" name="chlnCode" value="{{ chln }}" ng-click="saveChlnCode(chlnCode)"></td>
              <td>{{chlnCode}}</td>
          </tr>
      </table> 
	</div>
	
	 <div id ="branchDB" ng-hide="branchDBFlag">
		<input type="text" name="filterTextbox2" ng-model="filterTextbox2" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Branch Name </th>
 	  	  	  <th> Branch FaCode </th>
 	  	  </tr>
		  <tr ng-repeat="branch in branchList | filter:filterTextbox2">
		 	  <td><input type="radio"  value="{{ branch }}" ng-click="saveBranch(branch)"></td>
              <td>{{branch.branchName}}</td>
              <td>{{branch.branchFaCode}}</td>
          </tr>
      </table> 
	</div>
	
	<div id ="frmStnDB" ng-hide="frmStnDBFlag">
		<input type="text" name="filterTextbox3" ng-model="filterTextbox3" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Stn Name </th>
 	  	  	  <th> Stn District </th>
 	  	  	  <th> Stn Pin </th>
 	  	  	  <th> Stn Code </th>
 	  	  </tr>
		  <tr ng-repeat="frmStn in stnList | filter:filterTextbox3">
		 	  <td><input type="radio"  value="{{ frmStn }}" ng-click="saveFrmStn(frmStn)"></td>
              <td>{{frmStn.stnName}}</td>
              <td>{{frmStn.stnDistrict}}</td>
              <td>{{frmStn.stnPin}}</td>
              <td>{{frmStn.stnCode}}</td>
          </tr>
      </table> 
	</div>
	
	<div id ="toStnDB" ng-hide="toStnDBFlag">
		<input type="text" name="filterTextbox4" ng-model="filterTextbox4" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Stn Name </th>
 	  	  	  <th> Stn District </th>
 	  	  	  <th> Stn Pin </th>
 	  	  	  <th> Stn Code </th>
 	  	  </tr>
		  <tr ng-repeat="toStn in stnList | filter:filterTextbox4">
		 	  <td><input type="radio" value="{{ toStn }}" ng-click="saveToStn(toStn)"></td>
              <td>{{toStn.stnName}}</td>
              <td>{{toStn.stnDistrict}}</td>
              <td>{{toStn.stnPin}}</td>
              <td>{{toStn.stnCode}}</td>
          </tr>
      </table> 
	</div>
	
	<div id ="empDB" ng-hide="empDBFlag">
		<input type="text" name="filterTextbox5" ng-model="filterTextbox5" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Name </th>
 	  	  	  <th> Code </th>
 	  	  </tr>
		  <tr ng-repeat="emp in empList | filter:filterTextbox5">
		 	  <td><input type="radio" value="{{ emp }}" ng-click="saveEmp(emp)"></td>
              <td>{{emp.empName}}</td>
              <td>{{emp.empCodeTemp}}</td>
          </tr>
      </table> 
	</div>
	  
	<div id ="vehicleTypeDB" ng-hide="vehicleTypeDBFlag">
		<input type="text" name="filterTextbox6" ng-model="filterTextbox6" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Code </th>
 	  	  	  <th> Service Type </th>
 	  	  	  <th> Vehicle Type </th>
 	  	  </tr>
		  <tr ng-repeat="vehicleType in vehTypeList | filter:filterTextbox6">
		 	  <td><input type="radio" name="vehicleType" value="{{ vehicleType }}" ng-click="saveVehicleType(vehicleType)"></td>
              <td>{{vehicleType.vtCode}}</td>
              <td>{{vehicleType.vtServiceType}}</td>
              <td>{{vehicleType.vtVehicleType}}</td>
          </tr>
      </table> 
	</div>
	
	<div id ="payAtDB" ng-hide="payAtDBFlag">
		<input type="text" name="filterTextbox7" ng-model="filterTextbox7" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Name </th>
 	  	  	  <th> FaCode </th>
 	  	  </tr>
		  <tr ng-repeat="payAt in branchList | filter:filterTextbox7">
		 	  <td><input type="radio" name="payAt" value="{{ payAt }}" ng-click="savePayAt(payAt)"></td>
              <td>{{payAt.branchName}}</td>
              <td>{{payAt.branchFaCode}}</td>
          </tr>
      </table> 
	</div>
	
	<div id="vehDB" ng-hide="vehDBFlag">

		<input type="text" name="filterVehCode" ng-model="filterVehCode"
			placeholder="Search....">

		<table>
			<tr>
				<th></th>

				<th>Vehilce No</th>
			</tr>
			<tr ng-repeat="vehicle in vehList | filter:filterVehCode">
				<td><input type="radio" name="vehicleName" value="{{ vehicle }}" ng-click="selectVeh(vehicle)"></td>
				<td>{{ vehicle }}</td>
			</tr>
		</table>
	</div>
	
	<div id ="brkDB" ng-hide="brkDBFlag">
		<input type="text" name="filterTextbox8" ng-model="filterTextbox8" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Name </th>
 	  	  	  <th> Code </th>
 	  	  </tr>
		  <tr ng-repeat="brk in brkList | filter:filterTextbox8">
		 	  <td><input type="radio" name="brk" value="{{ brk }}" ng-click="saveBrk(brk)"></td>
              <td>{{brk.brkName}}</td>
              <td>{{brk.brkCode}}</td>
          </tr>
      </table> 
	</div>
	
	<div id ="ownDB" ng-hide="ownDBFlag">
		<input type="text" name="filterTextbox9" ng-model="filterTextbox9" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Name </th>
 	  	  	  <th> Code </th>
 	  	  </tr>
		  <tr ng-repeat="own in ownList | filter:filterTextbox9">
		 	  <td><input type="radio" name="own" value="{{ own }}" ng-click="saveOwn(own)"></td>
              <td>{{own.ownName}}</td>
              <td>{{own.ownCode}}</td>
          </tr>
      </table> 
	</div>
	
	<div id ="perStateDB" ng-hide="perStateDBFlag">
		<input type="text" name="filterTextbox10" ng-model="filterTextbox10" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> State </th>
 	  	  </tr>
		  <tr ng-repeat="perState in stateList | filter:filterTextbox10">
		 	  <td><input type="radio" name="perState" value="{{ perState }}" ng-click="savePerState(perState)"></td>
              <td>{{perState}}</td>
          </tr>
      </table> 
	</div>
	
	<div id ="panIssueStateDB" ng-hide="panIssueStateDBFlag">
		<input type="text" name="filterTextbox11" ng-model="filterTextbox11" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> State </th>
 	  	  </tr>
		  <tr ng-repeat="panIssueState in stateList | filter:filterTextbox11">
		 	  <td><input type="radio" name="panIssueState" value="{{ panIssueState }}" ng-click="savePanIssueState(panIssueState)"></td>
              <td>{{panIssueState}}</td>
          </tr>
      </table> 
	</div>

</div>