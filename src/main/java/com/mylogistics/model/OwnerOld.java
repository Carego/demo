package com.mylogistics.model;

import java.sql.Date;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "ownerold")
public class OwnerOld {
	
	@Id
	@Column(name="ownOldId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int ownOldId;
	
	@Column(name="ownId")
	private int ownId;
	
	@Column(name="ownCode")
	private String ownCode;
	
	@Column(name="branchCode")
	private String branchCode;
	
	@Column(name="ownName")
	private String ownName;
	
	@Column(name="ownComStbDt")
	private Date ownComStbDt;
	
	@Column(name="ownPanDOB")
	private Date ownPanDOB;
	
	@Column(name="ownPanDt")
	private Date ownPanDt;
	
	@Column(name="ownPanName")
	private String ownPanName;
	
	@Column(name="ownPanNo")
	private String ownPanNo;
	
	@Column(name="ownVehicleType")
	private String ownVehicleType;
	
	@Column(name="ownPPNo")
	private String ownPPNo;
	
	@Column(name="ownVoterId")
	private String ownVoterId;
	
	@Column(name="ownSrvTaxNo")
	private String ownSrvTaxNo;
	
	@Column(name="ownFirmRegNo")
	private String ownFirmRegNo;
	
	@Column(name="ownRegPlace")
	private String ownRegPlace;
	
	@Column(name="ownFirmType")
	private String ownFirmType;
	
	@Column(name="ownCIN")
	private String ownCIN;
	
	@Column(name="ownBsnCard")
	private String ownBsnCard;
	
	@Column(name="ownUnion")
	private String ownUnion;

	@Column(name="ownEmailId")
	private String ownEmailId;
	
	@Column(name="ownActiveDt")
	private Date ownActiveDt;
	
	@Column(name="ownDeactiveDt")
	private Date ownDeactiveDt;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="isView")
	private boolean isView;
	
	public String getOwnPanName() {
		return ownPanName;
	}

	public void setOwnPanName(String ownPanName) {
		this.ownPanName = ownPanName;
	}

	public boolean isView() {
		return isView;
	}

	public void setView(boolean isView) {
		this.isView = isView;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public int getOwnId() {
		return ownId;
	}

	public void setOwnId(int ownId) {
		this.ownId = ownId;
	}

	public String getOwnCode() {
		return ownCode;
	}

	public void setOwnCode(String ownCode) {
		this.ownCode = ownCode;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getOwnName() {
		return ownName;
	}

	public void setOwnName(String ownName) {
		this.ownName = ownName;
	}
	
	public String getOwnVehicleType() {
		return ownVehicleType;
	}

	public void setOwnVehicleType(String ownVehicleType) {
		this.ownVehicleType = ownVehicleType;
	}

	public String getOwnPPNo() {
		return ownPPNo;
	}

	public void setOwnPPNo(String ownPPNo) {
		this.ownPPNo = ownPPNo;
	}

	public String getOwnVoterId() {
		return ownVoterId;
	}

	public void setOwnVoterId(String ownVoterId) {
		this.ownVoterId = ownVoterId;
	}

	public String getOwnSrvTaxNo() {
		return ownSrvTaxNo;
	}

	public void setOwnSrvTaxNo(String ownSrvTaxNo) {
		this.ownSrvTaxNo = ownSrvTaxNo;
	}

	public String getOwnFirmRegNo() {
		return ownFirmRegNo;
	}

	public void setOwnFirmRegNo(String ownFirmRegNo) {
		this.ownFirmRegNo = ownFirmRegNo;
	}

	public String getOwnRegPlace() {
		return ownRegPlace;
	}

	public void setOwnRegPlace(String ownRegPlace) {
		this.ownRegPlace = ownRegPlace;
	}

	public String getOwnFirmType() {
		return ownFirmType;
	}

	public void setOwnFirmType(String ownFirmType) {
		this.ownFirmType = ownFirmType;
	}

	public String getOwnCIN() {
		return ownCIN;
	}

	public void setOwnCIN(String ownCIN) {
		this.ownCIN = ownCIN;
	}

	public String getOwnBsnCard() {
		return ownBsnCard;
	}

	public void setOwnBsnCard(String ownBsnCard) {
		this.ownBsnCard = ownBsnCard;
	}

	public String getOwnUnion() {
		return ownUnion;
	}

	public void setOwnUnion(String ownUnion) {
		this.ownUnion = ownUnion;
	}

	public String getOwnEmailId() {
		return ownEmailId;
	}

	public void setOwnEmailId(String ownEmailId) {
		this.ownEmailId = ownEmailId;
	}

	public Date getOwnComStbDt() {
		return ownComStbDt;
	}

	public void setOwnComStbDt(Date ownComStbDt) {
		this.ownComStbDt = ownComStbDt;
	}

	public Date getOwnPanDOB() {
		return ownPanDOB;
	}

	public void setOwnPanDOB(Date ownPanDOB) {
		this.ownPanDOB = ownPanDOB;
	}

	public Date getOwnPanDt() {
		return ownPanDt;
	}

	public void setOwnPanDt(Date ownPanDt) {
		this.ownPanDt = ownPanDt;
	}

	public Date getOwnActiveDt() {
		return ownActiveDt;
	}

	public void setOwnActiveDt(Date ownActiveDt) {
		this.ownActiveDt = ownActiveDt;
	}

	public Date getOwnDeactiveDt() {
		return ownDeactiveDt;
	}

	public void setOwnDeactiveDt(Date ownDeactiveDt) {
		this.ownDeactiveDt = ownDeactiveDt;
	}

	public int getOwnOldId() {
		return ownOldId;
	}

	public void setOwnOldId(int ownOldId) {
		this.ownOldId = ownOldId;
	}

	public String getOwnPanNo() {
		return ownPanNo;
	}

	public void setOwnPanNo(String ownPanNo) {
		this.ownPanNo = ownPanNo;
	}
	
	
}
