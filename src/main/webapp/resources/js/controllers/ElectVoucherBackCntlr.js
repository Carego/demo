'use strict';

var app = angular.module('application');

app.controller('ElectVoucherBackCntlr',['$scope','$location','$http','$filter',
                                 function($scope,$location,$http,$filter){
	
	$scope.vs = {};
	$scope.emList = [];
	$scope.em_sem = {};
	$scope.emAndSemList = [];
	
	var temp;
	$scope.semPayAmtSum=0;
	$scope.crNoDBFlag = true;
	$scope.electVoucherDBFlag = true;
	$scope.saveVsFlag = true;
	$scope.bankCodeDBFlag = true;
	$scope.selChqDBFlag = true;
	
	
	$scope.getVoucherDetails = function(){
		console.log("Enter into getDetails");
		$scope.saveVsFlag = true;
		var response = $http.post($scope.projectName+'/getVDetFrTelVBack');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.vs.branch = data.branch;
				$scope.bankCodeList = data.bankCodeList;
				$scope.vs.cashStmtStatus = data.cashStmtStatus;
				$scope.vs.voucherType = $scope.ELE_VOUCHER;
				$scope.dateTemp = $filter('date')(data.cashStmtStatus.cssDt, "yyyy-MM-dd'");
				
				$scope.getElectList();
			}else{
				console.log("Error in bringing data");
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}

	
	$scope.getElectList = function(){
		console.log("enter into getElectList function");
		var response = $http.post($scope.projectName+'/getElectList');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.emList = data.emList;
			}else{
				console.log("Error in bringing data");
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	} 
	
	
	$scope.selectPayBy = function(){
		console.log("enter into selectPayMod function with vs.payBy ="+$scope.vs.payBy);
		if($scope.vs.payBy === 'C'){
			$('#crNoId').removeAttr("disabled");
			
			$scope.vs.chequeType = '';
			$('#chequeTypeId').attr("disabled","disabled");
			$scope.vs.bankCode = "";
			$('#bankCodeId').attr("disabled","disabled");
			$scope.vs.chequeLeaves = {};
			$('#chequeNoId').attr("disabled","disabled");
			$scope.vs.payTo = "";
			$('#payToId').attr("disabled","disabled");
			
		}else if($scope.vs.payBy === 'Q'){
			$('#chequeTypeId').removeAttr("disabled");
		}else if($scope.vs.payBy === 'O'){
			$('#bankCodeId').removeAttr("disabled");
			$scope.vs.chequeLeaves = {};
			$('#chequeNoId').attr("disabled","disabled");
		}else{
			console.log("invaid entry");
		}
	}
	
	
	$scope.selectChqType = function(){
		console.log("enter into  selectChqType function");
		$('#bankCodeId').removeAttr("disabled");
	}
	
	
	$scope.openCrNoDB = function(){
		console.log("entre into openCrNoDB function");
		$scope.crNoDBFlag = false;
    	$('div#crNoDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Consumer Number",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		
		$('div#crNoDB').dialog('open');	
	}
	
	
	$scope.saveCRNo = function(em){
		console.log("enter into saveCRNo function");
		$scope.crNo = em.emCrnNo;
		$scope.crNoDBFlag = true;
		$('div#crNoDB').dialog('close');	
		
		$scope.em_sem.elctMstr = em;
		
		$scope.electVoucherDBFlag = false;
		$('div#electVoucherDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: em.emCrnNo+" of "+em.branch.branchName+" Branch ("+ em.branch.branchFaCode +") bill day is "+em.emBillDay,
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		
		$('div#electVoucherDB').dialog('open');	
	}
	
	
	$scope.openBankCodeDB = function(){
		console.log("enter into openBankCodeDB function");
		$scope.bankCodeDBFlag = false;
		$('div#bankCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Bank Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		
		$('div#bankCodeDB').dialog('open');	
	}
	
	
	$scope.saveBankCode =  function(bankCode){
		console.log("enter into saveBankCode----->"+bankCode);
		$scope.vs.bankCode = bankCode;
		$('div#bankCodeDB').dialog('close');
		$scope.bankCodeDBFlag = true;
		

		$('#payToId').removeAttr("disabled");
		$('#phNoId').removeAttr("disabled");
		
		if($scope.vs.payBy === 'Q'){
			
			var chqDet = {
					"bankCode" : bankCode,
					"CType"    : $scope.vs.chequeType
			};
			
			var response = $http.post($scope.projectName+'/getChequeNoFrEV',chqDet);
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					console.log("Cheque Leaves List--->>"+ data.chequeLeaves);
					$('#chequeNoId').removeAttr("disabled");
					$('#crNoId').removeAttr("disabled");
					$scope.chqList = data.list
					$scope.vs.chequeLeaves = data.chequeLeaves;
					$scope.maxChqAmnt = data.chequeLeaves.chqLChqMaxLt;
				}else{
					console.log("Error in bringing data from getChequeNo");
				}

			});
			response.error(function(data, status, headers, config){
				console.log(data);
			});
		}else if($scope.vs.payBy === 'O'){
			$('#crNoId').removeAttr("disabled");
		}else{
			
		}
		
	}
	
	
	
	$scope.openChqDB = function(){
		 console.log("enter into openChqDB function");
		    $scope.selChqDBFlag = false;
	    	$('div#selChqDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				show: UDShow,
				title:"Select Cheque No.",
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			        $scope.selChqDBFlag = true;
			    }
				});
			$('div#selChqDB').dialog('open');
	 }
	
	
	$scope.saveChqNo = function(chq){
		 console.log("enter into saveChqNo function");
		 $scope.vs.chequeLeaves = chq;
		 $('div#selChqDB').dialog('close');
	 }
	
	
	$scope.submitEMVoucher = function(emVoucherForm){
		console.log("enter into submitEMVoucher function = "+emVoucherForm.$invalid);
		if(emVoucherForm.$invalid){
			$scope.alertToast("Please enter correct value");
		}else{
			console.log("***********************");
			var response = $http.post($scope.projectName+'/submitEMVoucher',$scope.em_sem);
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					$scope.electVoucherDBFlag = true;
					$('div#electVoucherDB').dialog('close');	
					$scope.em_sem = {};
					
					$scope.fetchEmAndSem();
				}else{
					console.log(data);
				}
			});
			response.error(function(data, status, headers, config){
				console.log(data);
			});
		}
	}
	
	
	$scope.fetchEmAndSem = function(){
		console.log("enter into fetchEmAndSem function");
		var response = $http.post($scope.projectName+'/fetchEmAndSem');
		response.success(function(data, status, headers, config){
				$scope.emAndSemList = data.list;
				temp=0;
				for(var i=0; i<data.list.length; i++){
					temp=temp+data.list[i].subElectMstr.semPayAmt;
				}
				$scope.semPayAmtSum=temp;
				console.log("length of tmAndStmList ="+$scope.emAndSemList.length);
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
	
	$scope.removeSemV = function(index,emAndSem){
		console.log("enter into removeSemV function = "+index);
		$scope.semPayAmtSum=$scope.semPayAmtSum-emAndSem.subElectMstr.semPayAmt;
		var remIndex = {
				"index" : index
		};
		var response = $http.post($scope.projectName+'/removeSemV',remIndex);
		response.success(function(data, status, headers, config){
			$scope.fetchEmAndSem();
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
	
	
	 $scope.voucherSubmit = function(newVoucherForm){
		 console.log("enter into voucherSubmit function ==>"+newVoucherForm.$invalid);
		 if(newVoucherForm.$invalid){
			 $scope.alertToast("please enter correct value");
		 }else{
			    console.log("final submittion");
			    $scope.saveVsFlag = false;
		    	$('div#saveVsDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					show: UDShow,
					hide: UDHide,
					position: UDPos,
					draggable: true,
					close: function(event, ui) { 
				        $(this).dialog('destroy');
				        $(this).hide();
				    }
					});
				$('div#saveVsDB').dialog('open');
		 }
	 }
	 
	 
	 $scope.back = function(){
		 console.log("enter into back functio");
		 $scope.saveVsFlag = true;
		 $('div#saveVsDB').dialog('close');
	 }
	 
	 
	 $scope.saveVS = function(){
			console.log("enter into saveVs function");
			$('div#saveVsDB').dialog('close');
			$('#saveId').attr("disabled","disabled");
			var response = $http.post($scope.projectName+'/submitElectVoucher',$scope.vs);
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					$('#saveId').removeAttr("disabled");
					$scope.crNo = "";
					$scope.vs = {};
					$scope.emList = [];
					$scope.em_sem = {};
					$scope.emAndSemList = [];
					
					$scope.vs.chequeType = '';
					$('#chequeTypeId').attr("disabled","disabled");
					$scope.vs.bankCode = "";
					$('#bankCodeId').attr("disabled","disabled");
					$scope.vs.chequeLeaves = {};
					$('#chequeNoId').attr("disabled","disabled");
					$scope.vs.payTo = "";
					$('#payToId').attr("disabled","disabled");
					
					$scope.getVoucherDetails();
				}else{
					console.log("Error in submitVoucher");
				}
			});
			response.error(function(data, status, headers, config){
				console.log(data);
			});
		}
	 
	
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		 $scope.getVoucherDetails();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 }
	
}]);