package com.mylogistics.controller;

import java.io.InputStream;
import java.sql.Blob;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.swing.text.DefaultEditorKit.CutAction;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BillDAO;
import com.mylogistics.DAO.BillForwardingDAO;
import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.CashStmtStatusDAO;
import com.mylogistics.DAO.ChallanDAO;
import com.mylogistics.DAO.ChallanDetailDAO;
import com.mylogistics.DAO.CnmtDAO;
import com.mylogistics.DAO.Cnmt_ChallanDAO;
import com.mylogistics.DAO.CustomerDAO;
import com.mylogistics.DAO.CustomerGroupDAO;
import com.mylogistics.DAO.DailyContractDAO;
import com.mylogistics.DAO.RegularContractDAO;
import com.mylogistics.DAO.RelBillAddressDAO;
import com.mylogistics.DAO.ServiceTaxDAO;
import com.mylogistics.DAO.StationDAO;
import com.mylogistics.model.AdaniGST;
import com.mylogistics.model.Bill;
import com.mylogistics.model.BillForwarding;
import com.mylogistics.model.Branch;
import com.mylogistics.model.CashStmtStatus;
import com.mylogistics.model.Cnmt;
import com.mylogistics.model.CnmtImage;
import com.mylogistics.model.Customer;
import com.mylogistics.model.CustomerGroup;
import com.mylogistics.model.DailyContract;
import com.mylogistics.model.HilAddress;
import com.mylogistics.model.RegularContract;
import com.mylogistics.model.RelBillAddress;
import com.mylogistics.model.ServiceTax;
import com.mylogistics.model.Station;
import com.mylogistics.model.User;
import com.mylogistics.services.BillDetService;
import com.mylogistics.services.BillService;
import com.mylogistics.services.ConstantsValues;

@Controller
public class BillCntlr {

	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private BranchDAO branchDAO;
	
	@Autowired
	private CashStmtStatusDAO cashStmtStatusDAO;
	
	@Autowired
	private CustomerDAO customerDAO;
	
	@Autowired
	private CnmtDAO cnmtDAO;
	
	@Autowired
	private Cnmt_ChallanDAO cnmt_ChallanDAO;
	
	@Autowired
	private ChallanDAO challanDAO;
	
	@Autowired
	private StationDAO stationDAO;
	
	@Autowired
	private ChallanDetailDAO challanDetailDAO;
	
	@Autowired
	private ServiceTaxDAO serviceTaxDAO;
	
	@Autowired
	private BillDAO billDAO;
	
	@Autowired
	private DailyContractDAO dailyContractDAO;
	
	@Autowired
	private RegularContractDAO regularContractDAO;
	
	@Autowired
	private CustomerGroupDAO customerGroupDAO;
	
	@Autowired
	private RelBillAddressDAO relBillAddressDAO;
	
	@Autowired
	private BillForwardingDAO billForwardingDAO;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public static Logger logger = Logger.getLogger(BillCntlr.class);
	
	@RequestMapping(value = "/getBillInfoForGroup", method = RequestMethod.POST)
	public @ResponseBody Object getBillInfoForGroup() {
		User user = (User) httpSession.getAttribute("currentUser");
		logger.info("UserID = "+user.getUserId() + " : Enter into getBillInfoForGroup()");
		
		Map<String,Object> resultMap = new HashMap<>();
		Session session = this.sessionFactory.openSession();
		try{
			Branch branch = branchDAO.getBranchById(Integer.parseInt(user.getUserBranchCode()));			
			ServiceTax serviceTax = serviceTaxDAO.getLastSerTax();			
			if(branch != null){
				resultMap.put("brh", branch);				
				CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranch(user.getUserBranchCode());
				if(cashStmtStatus != null){				
					resultMap.put("cssDt", cashStmtStatus.getCssDt());
					List<CustomerGroup> groupList = customerGroupDAO.getAllCustGroupList(session, user);
					if(! groupList.isEmpty()){						
						resultMap.put("groupList", groupList);
						resultMap.put("serTax", serviceTax);
						resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);						
					}else{
						resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
						resultMap.put("msg", "Customer group not found !");
					}
				}else{
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					resultMap.put("msg", "CashStmt not found !");
				}					
			}else{
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				resultMap.put("msg", "Branch not found !");
			}
				
		}catch(Exception e){
			logger.error("UserID = "+user.getUserId() + " : Exception = "+e);
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", "Please try again !");
		}finally{
			session.clear();
			session.close();
		}
		return resultMap;
	}
	
	
	@RequestMapping(value = "/getBillInfo", method = RequestMethod.POST)
	public @ResponseBody Object getBillInfo() {
		System.out.println("enter into getBillInfo function");
		Map<String,Object> map = new HashMap<>();
		User currentUser = (User) httpSession.getAttribute("currentUser");
		System.out.println("userBranchCode="+currentUser.getUserBranchCode());
		
		Branch branch = branchDAO.getBranchById(Integer.parseInt(currentUser.getUserBranchCode()));
		ServiceTax serviceTax = serviceTaxDAO.getLastSerTax();
		if(branch != null){			
			map.put("brh",branch);
			CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranch(currentUser.getUserBranchCode());
			if(cashStmtStatus != null){
				map.put("cssDt",cashStmtStatus.getCssDt());
				List<Map<String,Object>> custList = new ArrayList<>();
				custList = customerDAO.getCustNCI();
				if(!custList.isEmpty()){
					map.put("list",custList);
					map.put("serTax",serviceTax);
					map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
				}else{
					map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
				}
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}

	
	
	
	@RequestMapping(value = "/getBillInfoM", method = RequestMethod.POST)
	public @ResponseBody Object getBillInfoM() {
		System.out.println("enter into getBillInfoM function");
		Map<String,Object> map = new HashMap<>();
		User currentUser = (User) httpSession.getAttribute("currentUser");
		System.out.println("userBranchCode="+currentUser.getUserBranchCode());
		
		Branch branch = branchDAO.getBranchById(Integer.parseInt(currentUser.getUserBranchCode()));
		ServiceTax serviceTax = serviceTaxDAO.getLastSerTax();
		if(branch != null){			
			map.put("brh",branch);
			CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranchBack(currentUser.getUserBranchCode());
			if(cashStmtStatus != null){
				map.put("cssDt",cashStmtStatus.getCssDt());
				List<Map<String,Object>> custList = new ArrayList<>();
				custList = customerDAO.getCustNCI();
				if(!custList.isEmpty()){
					map.put("list",custList);
					map.put("serTax",serviceTax);
					map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
				}else{
					map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
				}
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	

	
	
	
	@RequestMapping(value = "/getCnmtFrBill", method = RequestMethod.POST)
	public @ResponseBody Object getCnmtFrBill(@RequestBody Map<String,Object> clientMap) {
		System.out.println("enter into getCnmtFrBill function");
		Map<String,Object> map = new HashMap<>();
		
		int customerId = (Integer) clientMap.get("custId");
		Date frDt = null;
		Date toDt = null;
		

		User currentUser = (User) httpSession.getAttribute("currentUser");
		System.out.println("userBranchCode="+currentUser.getUserBranchCode());
		
		/*BillForwarding bf=billForwardingDAO.getPreviousBFbyCustBr(customerId, currentUser.getUserBranchCode());
		
		if(bf!=null) {
			if(bf.getBfRecDt()==null) {
			Long lBfDt=bf.getCreationTS().getTimeInMillis();
			
			java.util.Date dt = new java.util.Date();
			Long curntDt = dt.getTime();
			
			System.out.println("date="+curntDt+"Dt="+bf.getCreationTS().getTimeInMillis());
			
			
			Long dif = (curntDt - lBfDt)
					/ (24 * 60 * 60 * 1000);
			System.out.println("Diffrence="+ dif);
			
			if(dif>3) {
				map.put(ConstantsValues.RESULT,"BFError");
				map.put("msg", "Please Enter BillForwarding submission date of "+bf.getBfNo());
				return map;
			}
			
			}
		}*/
		
		if(customerId == 642 || customerId == 863){
			String cnmtFrDt = (String) clientMap.get("cnmtFromDt");		
			String cnmtToDt = (String) clientMap.get("cnmtToDt");
			
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			try{
				frDt = new Date(dateFormat.parse(cnmtFrDt).getTime());
				toDt = new Date(dateFormat.parse(cnmtToDt).getTime());			
			}catch(Exception e){
				System.out.println("Date Conversion Error = "+e);
			}
		}
		
		System.err.println("CustId = "+customerId+" : FrDt = "+frDt+" : ToDt = "+toDt);
		
			
		if(customerId > 0){
			List<String> cnmtList1=new ArrayList<>();
			List<String> cnmtList2=new ArrayList<>();
			
			if(customerId==1754 || customerId==1755 || customerId==1756){
				cnmtList1 = cnmtDAO.getCnmtFrNBillR(String.valueOf(customerId), frDt, toDt);
				cnmtList2 = cnmtDAO.getCnmtFrSBillR(String.valueOf(customerId), frDt, toDt);
			}else{
				cnmtList1 = cnmtDAO.getCnmtFrNBill(String.valueOf(customerId), frDt, toDt);
				cnmtList2 = cnmtDAO.getCnmtFrSBill(String.valueOf(customerId), frDt, toDt);
			}
			
			System.out.println("size of cnmtList1 = "+cnmtList1.size());
			System.out.println("size of cnmtList2 = "+cnmtList2.size());
			if(!cnmtList1.isEmpty() || !cnmtList2.isEmpty()){
				map.put("cnList1",cnmtList1);
				map.put("cnList2",cnmtList2);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	

	@RequestMapping(value = "/getCnInfoFrBl", method = RequestMethod.POST)
	public @ResponseBody Object getCnInfoFrBl(@RequestBody Map<String,Object> clientMap){ 
		logger.info("Enter into /getChInfoFrBl() ");
		Map<String,Object> map = new HashMap<>();
		String cnmtCode = (String) clientMap.get("cnmtCode");
		Date billDt = Date.valueOf((String) clientMap.get("billDt"));
		if(cnmtCode != null){
			List<Cnmt> cnmtList = cnmtDAO.getCnmtList(cnmtCode);
			System.out.println("!cnmtList.isEmpty()="+!cnmtList.isEmpty());
			if(!cnmtList.isEmpty()){
				Cnmt cnmt = cnmtList.get(0);
				if(cnmt.getContractCode().substring(0,3).equalsIgnoreCase("reg")){
					List<RegularContract> regList = regularContractDAO.getRegContractData(cnmt.getContractCode());
					System.out.println("regList.get(0).getRegContBillBasis() = "+regList.get(0).getRegContBillBasis());
					if(!regList.isEmpty()){
						map.put("billBasis", regList.get(0).getRegContBillBasis());
						map.put("contType", regList.get(0).getRegContType());
					}else{
						map.put("billBasis", "chargeWt");
						map.put("contType", "W");
					}
				}else{
					List<DailyContract> dlyList = dailyContractDAO.getDailyContractData(cnmt.getContractCode());
					if(!dlyList.isEmpty()){
						map.put("billBasis", dlyList.get(0).getDlyContBillBasis());
						map.put("contType", dlyList.get(0).getDlyContType());
					}else{
						map.put("billBasis", "chargeWt");
						map.put("contType", "W");
					}
				}				
				
				Integer cmId = cnmt.getCmId();
			
				if(cmId > 0){
					List<Integer> chlnIdList = cnmt_ChallanDAO.getChlnIdByCnmtId(cnmt.getCnmtId());
					if(!chlnIdList.isEmpty()){
						List<Map<String,Object>> chlnList = new ArrayList<>();
						for(int i=0;i<chlnIdList.size();i++){
							Map<String,Object> chlnInfo = challanDAO.getChlnInfoById(chlnIdList.get(i));
							chlnList.add(chlnInfo);
						}
						
						
						if(!chlnList.isEmpty()){
							map.put("lryNo",chlnList.get(0).get("chlnLryNo"));
						}else{
							map.put("lryNo","");
						}
						
						
						List<Station> stnList1 = stationDAO.retrieveStation(cnmt.getCnmtFromSt());
						List<Station> stnList2 = stationDAO.retrieveStation(cnmt.getCnmtToSt());
						List<Customer> consinorList=customerDAO.getCustomer(cnmt.getCnmtConsignor());
						List<Customer> consineeList=customerDAO.getCustomer(cnmt.getCnmtConsignee());
						
						if(!consineeList.isEmpty()){
							map.put("cnsineeName",consineeList.get(0).getCustName());
						}
						if(!consinorList.isEmpty()){
							map.put("cnsinorName",consinorList.get(0).getCustName());
						}
						if(!stnList1.isEmpty()){
							map.put("frStn",stnList1.get(0).getStnName());
							map.put("frState",stnList1.get(0).getStateCode());
							map.put("frStateGst",stnList1.get(0).getState().getStateGST());
						}else{
							map.put("frStn","");
						}
						
						if(!stnList2.isEmpty()){
							map.put("toStn",stnList2.get(0).getStnName());
							map.put("toState",stnList2.get(0).getStateCode());
							map.put("toStateGst",stnList2.get(0).getState().getStateGST());
						}else{
							map.put("toStn","");
						}
						
						map.put("cnmt",cnmt);
						map.put("chlnList",chlnList);
						map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
					}else{
						map.put("msg","Please enter challan of "+cnmt.getCnmtCode());
						map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
					}
				}else{
					int res = customerDAO.checkBillPer(Integer.parseInt(cnmt.getCustCode()),billDt);
					System.out.println("value of res = "+res);
					if(res > 0){
						List<Integer> chlnIdList = cnmt_ChallanDAO.getChlnIdByCnmtId(cnmt.getCnmtId());
						System.out.println("size of chlnIdList = "+chlnIdList.size());
						if(!chlnIdList.isEmpty()){
							List<Map<String,Object>> chlnList = new ArrayList<>();
							
							for(int i=0;i<chlnIdList.size();i++){
								Map<String,Object> chlnInfo = challanDAO.getChlnInfoById(chlnIdList.get(i));
								chlnList.add(chlnInfo);
							}
							
							
							if(!chlnList.isEmpty()){
								map.put("lryNo",chlnList.get(0).get("chlnLryNo"));
							}else{
								map.put("lryNo","");
							}
							
							List<Station> stnList1 = stationDAO.retrieveStation(cnmt.getCnmtFromSt());
							List<Station> stnList2 = stationDAO.retrieveStation(cnmt.getCnmtToSt());
							List<Customer> consinorList=customerDAO.getCustomer(cnmt.getCnmtConsignor());
							List<Customer> consineeList=customerDAO.getCustomer(cnmt.getCnmtConsignee());
							
							if(!consineeList.isEmpty()){
								map.put("cnsineeName",consineeList.get(0).getCustName());
							}
							if(!consinorList.isEmpty()){
								map.put("cnsinorName",consinorList.get(0).getCustName());
							}
							
							if(!stnList1.isEmpty()){
								map.put("frStn",stnList1.get(0).getStnName());
								map.put("frState",stnList1.get(0).getStateCode());
								map.put("frStateGst",stnList1.get(0).getState().getStateGST());
							}else{
								map.put("frStn","");
							}
							
							if(!stnList2.isEmpty()){
								map.put("toStn",stnList2.get(0).getStnName());
								map.put("toState",stnList2.get(0).getStateCode());
								map.put("toStateGst",stnList2.get(0).getState().getStateGST());
							}else{
								map.put("toStn","");
							}
							
							map.put("cnmt",cnmt);
							map.put("chlnList",chlnList);
							map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
						}else{
							map.put("msg","Please enter challan of "+cnmt.getCnmtCode());
							map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
						}
					}else{
						System.out.println("Inside MSG :");
						map.put("msg","Please upload scan image of "+cnmtCode+" OR allow Customer for bill permission");
						map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
					}
				}
			}else{
				map.put("msg",cnmtCode+" is invalid cnmt");
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			map.put("msg",cnmtCode+" is invalid cnmt");
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		
		System.out.println("map.get(billBasis)  == "+map.get("billBasis"));
		return map;
	}
	
	@RequestMapping(value = "/getCnInfoFrBlAll", method = RequestMethod.POST)
	public @ResponseBody synchronized Object getCnInfoFrBlAll(@RequestBody Map<String,Object> clientMap){ 
		logger.info("Enter into /getChInfoFrBl() ");
		Map<String,Object> map = new HashMap<>();
		String cnmtCode = (String) clientMap.get("cnmtCode");
		Date billDt = Date.valueOf((String) clientMap.get("billDt"));
		if(cnmtCode != null){
			List<Cnmt> cnmtList = cnmtDAO.getCnmtList(cnmtCode);
			System.out.println("!cnmtList.isEmpty()="+!cnmtList.isEmpty());
			if(!cnmtList.isEmpty()){
				Cnmt cnmt = cnmtList.get(0);
				if(cnmt.getContractCode().substring(0,3).equalsIgnoreCase("reg")){
					List<RegularContract> regList = regularContractDAO.getRegContractData(cnmt.getContractCode());
					System.out.println("regList.get(0).getRegContBillBasis() = "+regList.get(0).getRegContBillBasis());
					if(!regList.isEmpty()){
						map.put("billBasis", regList.get(0).getRegContBillBasis());
						map.put("contType", regList.get(0).getRegContType());
					}else{
						map.put("billBasis", "chargeWt");
						map.put("contType", "W");
					}
				}else{
					List<DailyContract> dlyList = dailyContractDAO.getDailyContractData(cnmt.getContractCode());
					if(!dlyList.isEmpty()){
						map.put("billBasis", dlyList.get(0).getDlyContBillBasis());
						map.put("contType", dlyList.get(0).getDlyContType());
					}else{
						map.put("billBasis", "chargeWt");
						map.put("contType", "W");
					}
				}				
				
				Integer cmId = cnmt.getCmId();
			
				if(cmId > 0){
					List<Integer> chlnIdList = cnmt_ChallanDAO.getChlnIdByCnmtId(cnmt.getCnmtId());
					if(!chlnIdList.isEmpty()){
						List<Map<String,Object>> chlnList = new ArrayList<>();
						for(int i=0;i<chlnIdList.size();i++){
							//List<Challan> actChList = new ArrayList<>();
							//actChList = challanDAO.getChallanById(chlnIdList.get(i));
							Map<String,Object> chlnInfo = challanDAO.getChlnInfoById(chlnIdList.get(i));
							chlnList.add(chlnInfo);
						}
						
						
						if(!chlnList.isEmpty()){
							map.put("lryNo",chlnList.get(0).get("chlnLryNo"));
						}else{
							map.put("lryNo","");
						}
						
						
						List<Station> stnList1 = stationDAO.retrieveStation(cnmt.getCnmtFromSt());
						List<Station> stnList2 = stationDAO.retrieveStation(cnmt.getCnmtToSt());
						List<Customer> consinorList=customerDAO.getCustomer(cnmt.getCnmtConsignor());
						List<Customer> consineeList=customerDAO.getCustomer(cnmt.getCnmtConsignee());
						
						if(!consineeList.isEmpty()){
							map.put("cnsineeName",consineeList.get(0).getCustName());
						}
						if(!consinorList.isEmpty()){
							map.put("cnsinorName",consinorList.get(0).getCustName());
						}
						if(!stnList1.isEmpty()){
							map.put("frStn",stnList1.get(0).getStnName());
							map.put("frState",stnList1.get(0).getStateCode());
							map.put("frStateGst",stnList1.get(0).getState().getStateGST());
						}else{
							map.put("frStn","");
						}
						
						if(!stnList2.isEmpty()){
							map.put("toStn",stnList2.get(0).getStnName());
							map.put("toState",stnList2.get(0).getStateCode());
							map.put("toStateGst",stnList2.get(0).getState().getStateGST());
						}else{
							map.put("toStn","");
						}
						
						map.put("cnmt",cnmt);
						map.put("chlnList",chlnList);
						map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
					}else{
						map.put("msg","Please enter challan of "+cnmt.getCnmtCode());
						map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
					}
				}else{
					int res = customerDAO.checkBillPer(Integer.parseInt(cnmt.getCustCode()),billDt);
					System.out.println("value of res = "+res);
					if(res > 0){
						List<Integer> chlnIdList = cnmt_ChallanDAO.getChlnIdByCnmtId(cnmt.getCnmtId());
						System.out.println("size of chlnIdList = "+chlnIdList.size());
						if(!chlnIdList.isEmpty()){
							List<Map<String,Object>> chlnList = new ArrayList<>();
							
							for(int i=0;i<chlnIdList.size();i++){
								Map<String,Object> chlnInfo = challanDAO.getChlnInfoById(chlnIdList.get(i));
								chlnList.add(chlnInfo);
							}
							
							
							if(!chlnList.isEmpty()){
								map.put("lryNo",chlnList.get(0).get("chlnLryNo"));
							}else{
								map.put("lryNo","");
							}
							
							List<Station> stnList1 = stationDAO.retrieveStation(cnmt.getCnmtFromSt());
							List<Station> stnList2 = stationDAO.retrieveStation(cnmt.getCnmtToSt());
							List<Customer> consinorList=customerDAO.getCustomer(cnmt.getCnmtConsignor());
							List<Customer> consineeList=customerDAO.getCustomer(cnmt.getCnmtConsignee());
							
							if(!consineeList.isEmpty()){
								map.put("cnsineeName",consineeList.get(0).getCustName());
							}
							if(!consinorList.isEmpty()){
								map.put("cnsinorName",consinorList.get(0).getCustName());
							}
							if(!stnList1.isEmpty()){
								map.put("frStn",stnList1.get(0).getStnName());
								map.put("frState",stnList1.get(0).getStateCode());
								map.put("frStateGst",stnList1.get(0).getState().getStateGST());
							}else{
								map.put("frStn","");
							}
							
							if(!stnList2.isEmpty()){
								map.put("toStn",stnList2.get(0).getStnName());
								map.put("toState",stnList2.get(0).getStateCode());
								map.put("toStateGst",stnList2.get(0).getState().getStateGST());
							}else{
								map.put("toStn","");
							}
							
							map.put("cnmt",cnmt);
							map.put("chlnList",chlnList);
							map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
						}else{
							map.put("msg","Please enter challan of "+cnmt.getCnmtCode());
							map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
						}
					}else{
						System.out.println("Inside MSG :");
						map.put("msg","Please upload scan image of "+cnmtCode+" OR allow Customer for bill permission");
						map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
					}
				}
			}else{
				map.put("msg",cnmtCode+" is invalid cnmt");
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			map.put("msg",cnmtCode+" is invalid cnmt");
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		
		System.out.println("map.get(billBasis)  == "+map.get("billBasis"));
		return map;
	}
	
	
	
	@RequestMapping(value = "/getCnImageFrBill", method = RequestMethod.POST)
	public @ResponseBody Object getCnImageFrBill(@RequestBody Map<String,Object> clientMap)throws Exception {
		System.out.println("enter into getCnImageFrBill function");
		Map<String,Object> map = new HashMap<>();
		int cnmtId = (int) clientMap.get("cnmtId");
		if(cnmtId > 0){
			Blob blob = cnmtDAO.getCnmtImage(cnmtId);
			if(blob != null){
				InputStream in = blob.getBinaryStream();
				
				int blobLength = (int) blob.length();  
				byte[] blobAsBytes = blob.getBytes(1, blobLength);
				
				System.out.println("&&&&&&&&&&&&&&&&&&&"+blobAsBytes.length);
				map.put("cnImage",blobAsBytes);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put("msg","image not avaliable");
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			map.put("msg","invalid cnmt");
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}	
	
	
	@RequestMapping(value = "/submitFBill", method = RequestMethod.POST)
	public @ResponseBody Object submitFBill(@RequestBody BillService billService){
		logger.info("Enter into /submitFBill()");
		
		Map<String,Object> resultMap = new HashMap<>();		
		User currentUser = (User) httpSession.getAttribute("currentUser");
		
		List<BillDetService> bdSerList = billService.getBdSerList();		
		Bill bill = billService.getBill();
		
		CashStmtStatus css = cashStmtStatusDAO.getCssByDt(currentUser.getUserBranchCode(), bill.getBlBillDt());
		CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranch(currentUser.getUserBranchCode());
		
		int res = bill.getBlBillDt().compareTo(cashStmtStatus.getCssDt());
		
		System.out.println("billDate="+bill.getBlBillDt());
		System.out.println("cashStmtDate="+cashStmtStatus.getCssDt());
		System.out.println("res="+res);
		
		if(res == 1){
			String billNo = billDAO.saveAdvBill(billService);
			List<Map<String,Object>> blList = new ArrayList<Map<String,Object>>();
			blList = billDAO.getSingleBlPrint(Integer.parseInt(currentUser.getUserBranchCode()),billNo);
			if(billNo != null && !billNo.isEmpty()){
				resultMap.put("billNo",billNo);
				if(!blList.isEmpty()){
					resultMap.put("list",blList);
				}
				resultMap.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				resultMap.put("msg","Server Error");
				resultMap.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			if(css != null){
				String billNo = billDAO.saveBill(billService);
				List<Map<String,Object>> blList = new ArrayList<Map<String,Object>>();
				//blList = billDAO.getBlFrPrint(Integer.parseInt(currentUser.getUserBranchCode()),billNo,billNo);
				blList = billDAO.getSingleBlPrint(Integer.parseInt(currentUser.getUserBranchCode()),billNo);
				if(billNo != null && !billNo.isEmpty()){
					resultMap.put("billNo",billNo);
					if(!blList.isEmpty()){
						resultMap.put("list",blList);
					}
					resultMap.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
				}else{
					resultMap.put("msg","Server Error");
					resultMap.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
				}
			}else{
				resultMap.put("msg","Invalid date for billing");
				resultMap.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}
		
		return resultMap;
	}	


	
	
	
	//TODO Manual Bill without gst
	@RequestMapping(value = "/submitFBillM", method = RequestMethod.POST)
	public @ResponseBody Object submitFBillM(@RequestBody BillService billService){
		logger.info("Enter into /submitFBillM()");
		
		Map<String,Object> resultMap = new HashMap<>();		
		User currentUser = (User) httpSession.getAttribute("currentUser");
		
		
		int i=billDAO.getBillByBlNo(billService.getBill().getBlBillNo());
		if(i==2){
			resultMap.put("result", "error");
			resultMap.put("msg", "Bill No. Alreaady exist");
			return resultMap;
		}
		
		
		List<BillDetService> bdSerList = billService.getBdSerList();		
		Bill bill = billService.getBill();
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		
		try{
			
			CashStmtStatus css = cashStmtStatusDAO.getCssByDt(currentUser.getUserBranchCode(), bill.getBlBillDt(),session);
				if(css != null){
					String billNo = billDAO.saveBillM(billService,session);
					List<Map<String,Object>> blList = new ArrayList<Map<String,Object>>();
					if(billNo != null && !billNo.isEmpty()){
						resultMap.put("billNo",billNo);
						resultMap.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
					}else{
						resultMap.put("msg","Server Error");
						resultMap.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
					}
				}else{
					resultMap.put("msg","Invalid date for billing");
					resultMap.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
				}

			
			transaction.commit();
		}catch(Exception e){
			e.printStackTrace();
			transaction.rollback();
			logger.info("Exception into /submitFBillMM()"+e);
		}finally{
			session.clear();
			session.close();
		}
		
		return resultMap;
	}	

	
	
	
	//TODO GST by kamal
	@RequestMapping(value = "/submitFBillGSTM", method = RequestMethod.POST)
	public @ResponseBody Object submitFBillGSTM(@RequestBody BillService billService){
		logger.info("Enter into /submitFBillM()");
		
		Map<String,Object> resultMap = new HashMap<>();		
		User currentUser = (User) httpSession.getAttribute("currentUser");
		
		//  check if bill already exist
		int i=billDAO.getBillByBlNo(billService.getBill().getBlBillNo());
		if(i==2){
			resultMap.put("result", "error");
			resultMap.put("msg", "Bill No. Alreaady exist");
			return resultMap;
		}
		
		
		
		List<BillDetService> bdSerList = billService.getBdSerList();		
		Bill bill = billService.getBill();
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		
		try{
			// check cs date is available or not
			CashStmtStatus css = cashStmtStatusDAO.getCssByDt(currentUser.getUserBranchCode(), bill.getBlBillDt(),session);
				if(css != null){
					//save bill into database bill and cashstmt table
					String billNo = billDAO.saveBillM(billService,session);
					

					if(billNo != null && !"".equalsIgnoreCase(billNo.trim())){
						resultMap.put("billNo",billNo);
						resultMap.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
					}else{
						resultMap.put("msg","Please enter bill no.");
						resultMap.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
					}
				}else{
					
					//save bill into database bill and cashstmttemp table
					String billNo = billDAO.saveAdvanceBillM(billService,session);
					

					if(billNo != null && !"".equalsIgnoreCase(billNo.trim())){
						resultMap.put("billNo",billNo);
						resultMap.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
					}else{
						resultMap.put("msg","Please enter bill no.");
						resultMap.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
					}
					
//					resultMap.put("msg","Invalid date for billing");
//					resultMap.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
				}

				transaction.commit();
		}catch(Exception e){
			e.printStackTrace();
			transaction.rollback();
			resultMap.put("msg","Somthing Missing");
			resultMap.put("err","Exception"+e);
			logger.info("Exception into /submitFBillMM()"+e);
		}finally{
			session.clear();
			session.close();
		}
		
		return resultMap;
	}	

	

	
	

	//TODO RelGST by kamal
	@RequestMapping(value = "/submitRelFBillGSTM", method = RequestMethod.POST)
	public @ResponseBody Object submitRelFBillGSTM(@RequestBody BillService billService){
		logger.info("Enter into /submitRelFBillMGSTM()");
		
		Map<String,Object> resultMap = new HashMap<>();		
		User currentUser = (User) httpSession.getAttribute("currentUser");
		
		
		int i=billDAO.getBillByBlNo(billService.getBill().getBlBillNo());
		if(i==2){
			resultMap.put("result", "error");
			resultMap.put("msg", "Bill No. Alreaady exist");
			return resultMap;
		}
			
		

		
		
		List<BillDetService> bdSerList = billService.getBdSerList();		
		Bill bill = billService.getBill();
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		
		try{
			
				String billNo = billDAO.saveRelGstBill(billService,session);
				if(billNo != null && !billNo.isEmpty()){
					resultMap.put("billNo",billNo);
					resultMap.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
				}else{
					resultMap.put("msg","Server Error");
					resultMap.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
				}

			
			transaction.commit();
		}catch(Exception e){
			e.printStackTrace();
			transaction.rollback();
			resultMap.put("msg","Somthing Missing");
			resultMap.put("err","Exception"+e);
			logger.info("Exception into /submitRelFBillGSTM()"+e);
		}finally{
			session.clear();
			session.close();
		}
		
		return resultMap;
	}	


	
	
	
	
	@RequestMapping(value = "/checkBillM", method = RequestMethod.POST)
	public @ResponseBody Object checkBillM(@RequestBody String billNo){
		System.out.println("insert into checkBillM()");
		System.out.println("billNo="+billNo);
		
		Map<Object, String> map= new HashMap();
		
		int i=billDAO.getBillByBlNo(billNo);
		if(i==1){
			map.put("result", "success");
		}else if(i==2){
			map.put("result", "error");
			map.put("msg", "this Bill No is already exist");
		}else{
			map.put("result", "error");
			map.put("msg", "there is some problem to matching bill");
		}
			
		return map;
	}
	
	
	
	
	
	@RequestMapping(value = "/submitFFBill", method = RequestMethod.POST)
	public @ResponseBody Object submitFFBill(@RequestBody BillService billService){				
		Map<String,Object> resultMap = new HashMap<>();	
		List<String> errMsg = new ArrayList<String>();
		User user = (User) httpSession.getAttribute("currentUser");
		logger.info("Enter into /submitFFBill() : UserID = "+user.getUserId());
		// Session And Transaction
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		logger.info("Hi...");
		try{
			// Fetch Detail from Client Side
			List<BillDetService> bdSerList = billService.getBdSerList();
			Bill bill = billService.getBill();
			
			CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranch(session, user.getUserId(), user.getUserBranchCode());			
			int res = bill.getBlBillDt().compareTo(cashStmtStatus.getCssDt());
			
			logger.info("BillDate = "+bill.getBlBillDt()+" : CashStmtDate = "+cashStmtStatus.getCssDt()+" : Res = "+res+" : UserID = "+user.getUserId());
			
			// Going to save Bill (Advance Bill)
			if(res == 1){
				resultMap = billDAO.saveAdvBill(session, user, billService);
				if(String.valueOf(resultMap.get(ConstantsValues.RESULT)).equalsIgnoreCase(ConstantsValues.SUCCESS)){					
				}
			}else{
				// Going to save Bill
				CashStmtStatus css = cashStmtStatusDAO.getCssByCssDtWithoutCs(session, user.getUserId(), user.getUserBranchCode(), bill.getBlBillDt());
				if(css != null){
					resultMap = billDAO.saveBill(session, user, billService);
					List<Map<String,Object>> blList = new ArrayList<Map<String,Object>>();
					if(String.valueOf(resultMap.get(ConstantsValues.RESULT)).equalsIgnoreCase(ConstantsValues.SUCCESS)){						
					}
				}else{
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					errMsg.add("CashStmtStatus not found for this bill !");
					resultMap.put("msg", errMsg);
					return resultMap;
				}
			}
			if(String.valueOf(resultMap.get(ConstantsValues.RESULT)).equalsIgnoreCase(ConstantsValues.SUCCESS)){
				logger.error("Bill is submitted : UserId = "+user.getUserId());
				transaction.commit();
			}else{
				logger.error("Bill is not submitted : UserId = "+user.getUserId());
				transaction.rollback();				
			}
		}catch(Exception e){
			logger.error("UserID = "+user.getUserId()+" : Exception = "+e);
			errMsg.add("Try Again.... !");
			resultMap.put("msg", errMsg);
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);			
		}finally{
			session.clear();
			session.close();
		}		
		return resultMap;
	}
	
//TODO get bill information for gst	
	@RequestMapping(value = "/getBillInfoGST", method = RequestMethod.POST)
	public @ResponseBody Object getBillInfoGST() {
		System.out.println("enter into getBillInfoGST function");
		Map<String,Object> map = new HashMap<>();
		User currentUser = (User) httpSession.getAttribute("currentUser");
		System.out.println("userBranchCode="+currentUser.getUserBranchCode());
		
		Branch branch = branchDAO.getBranchById(Integer.parseInt(currentUser.getUserBranchCode()));
		ServiceTax serviceTax = serviceTaxDAO.getLastSerTax();
		if(branch != null){			
			map.put("brh",branch);
			CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranch(currentUser.getUserBranchCode());
			if(cashStmtStatus != null){
				map.put("cssDt",cashStmtStatus.getCssDt());
				List<Map<String,Object>> custList ;
				List<RelBillAddress> relBlList ;
				custList = customerDAO.getGstCust();
				relBlList = relBillAddressDAO.getAllRelBlAdd();
				List<AdaniGST> adaniGSTList = relBillAddressDAO.getAllAdaniGst();
				List<HilAddress> hilGSTList = relBillAddressDAO.getAllHilAddress();
				//String billNo=billDAO.getNewBillByBr(branch.getBranchId());
				Map<String,Object> billmap=billDAO.getNewBillByBrh(branch.getBranchId());
				if(!custList.isEmpty()){
					map.put("billNo", billmap.get("billNo"));
					map.put("msg", billmap.get("msg"));
					//map.put("billNo",billNo);
					map.put("adaniGSTList", adaniGSTList);
					map.put("hilGSTList", hilGSTList);
					map.put("list",custList);
					map.put("relBlList",relBlList);
					map.put("serTax",serviceTax);
					map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
				}else{
					map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
				}
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/getNewBillNo", method = RequestMethod.POST)
	public @ResponseBody Object getNewBillNo() {
		System.out.println("enter into getNewBillNo function");
		Map<String,Object> map = new HashMap<>();
		User currentUser = (User) httpSession.getAttribute("currentUser");
		System.out.println("userBranchCode="+currentUser.getUserBranchCode());
		
		Branch branch = branchDAO.getBranchById(Integer.parseInt(currentUser.getUserBranchCode()));
		if(branch != null){			
			//String billNo=billDAO.getNewBillByBr(branch.getBranchId());
				Map<String,Object> billmap=billDAO.getNewBillByBrh(branch.getBranchId());
					map.put("billNo", billmap.get("billNo"));
					map.put("msg", billmap.get("msg"));
					//map.put("billNo",billNo);
					map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	

	
	//TODO get bill information for gst	
		@RequestMapping(value = "/getRelBillInfoGST", method = RequestMethod.POST)
		public @ResponseBody Object getRelBillInfoGST() {
			System.out.println("enter into getBillInfoGST function");
			Map<String,Object> map = new HashMap<>();
			User currentUser = (User) httpSession.getAttribute("currentUser");
			System.out.println("userBranchCode="+currentUser.getUserBranchCode());
			
			Branch branch = branchDAO.getBranchById(Integer.parseInt(currentUser.getUserBranchCode()));
			ServiceTax serviceTax = serviceTaxDAO.getLastSerTax();
			if(branch != null){			
				map.put("brh",branch);
				CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranch(currentUser.getUserBranchCode());
				if(cashStmtStatus != null){
					map.put("cssDt",cashStmtStatus.getCssDt());
					List<Map<String,Object>> custList;
					List<RelBillAddress> relBlList ;
					custList = customerDAO.getRelGstCust();
					relBlList = relBillAddressDAO.getAllRelBlAdd();
					
//					String billNo=billDAO.getNewBillByBr(branch.getBranchId());
//					if(!custList.isEmpty()){
//						map.put("billNo", billNo);
				
					Map<String,Object> billmap=billDAO.getNewBillByBrh(branch.getBranchId());
					if(!custList.isEmpty()){
						map.put("billNo", billmap.get("billNo"));
						map.put("msg", billmap.get("msg"));
						map.put("list",custList);
						map.put("relBlList",relBlList);
						map.put("serTax",serviceTax);
						map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
					}else{
						map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
					}
				}else{
					map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
				}
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
			return map;
		}
		

	
		@RequestMapping(value = "/getCnRelInfoFrBl", method = RequestMethod.POST)
		public @ResponseBody Object getCnRelInfoFrBl(@RequestBody Map<String,Object> clientMap){ 
			logger.info("Enter into /getChInfoFrBl() ");
			Map<String,Object> map = new HashMap<>();
			String cnmtCode = (String) clientMap.get("cnmtCode");
			Date billDt = Date.valueOf((String) clientMap.get("billDt"));
			if(cnmtCode != null){
				List<Cnmt> cnmtList = cnmtDAO.getCnmtList(cnmtCode);
				System.out.println("!cnmtList.isEmpty()="+!cnmtList.isEmpty());
				if(!cnmtList.isEmpty()){
					Cnmt cnmt = cnmtList.get(0);
					if(cnmt.getContractCode().substring(0,3).equalsIgnoreCase("reg")){
						List<RegularContract> regList = regularContractDAO.getRegContractData(cnmt.getContractCode());
						System.out.println("regList.get(0).getRegContBillBasis() = "+regList.get(0).getRegContBillBasis());
						if(!regList.isEmpty()){
							map.put("billBasis", regList.get(0).getRegContBillBasis());
							map.put("contType", regList.get(0).getRegContType());
						}else{
							map.put("billBasis", "chargeWt");
							map.put("contType", "W");
						}
					}else{
						List<DailyContract> dlyList = dailyContractDAO.getDailyContractData(cnmt.getContractCode());
						if(!dlyList.isEmpty()){
							map.put("billBasis", dlyList.get(0).getDlyContBillBasis());
							map.put("contType", dlyList.get(0).getDlyContType());
						}else{
							map.put("billBasis", "chargeWt");
							map.put("contType", "W");
						}
					}				
					
					Integer cmId = cnmt.getCmId();
				
					if(cmId > 0){
						List<Integer> chlnIdList = cnmt_ChallanDAO.getChlnIdByCnmtId(cnmt.getCnmtId());
						if(!chlnIdList.isEmpty()){
							List<Map<String,Object>> chlnList = new ArrayList<>();
							for(int i=0;i<chlnIdList.size();i++){
								Map<String,Object> chlnInfo = challanDAO.getChlnInfoById(chlnIdList.get(i));
								chlnList.add(chlnInfo);
							}
							
							
							if(!chlnList.isEmpty()){
								map.put("lryNo",chlnList.get(0).get("chlnLryNo"));
							}else{
								map.put("lryNo","");
							}
							
							
							List<Station> stnList1 = stationDAO.retrieveStation(cnmt.getCnmtFromSt());
							List<Station> stnList2 = stationDAO.retrieveStation(cnmt.getCnmtToSt());
							List<Customer> consinorList=customerDAO.getCustomer(cnmt.getCnmtConsignor());
							List<Customer> consineeList=customerDAO.getCustomer(cnmt.getCnmtConsignee());
							
							if(!consineeList.isEmpty()){
								map.put("cnsineeName",consineeList.get(0).getCustName());
							}
							if(!consinorList.isEmpty()){
								map.put("cnsinorName",consinorList.get(0).getCustName());
							}
							if(!stnList1.isEmpty()){
								map.put("frStn",stnList1.get(0).getStnName());
								map.put("frState",stnList1.get(0).getStateCode());
								map.put("frStateGst",stnList1.get(0).getState().getStateGST());
							}else{
								map.put("frStn","");
							}
							
							if(!stnList2.isEmpty()){
								map.put("toStn",stnList2.get(0).getStnName());
								map.put("toState",stnList2.get(0).getStateCode());
								map.put("toStateGst",stnList2.get(0).getState().getStateGST());
							}else{
								map.put("toStn","");
							}
							
							map.put("cnmt",cnmt);
							map.put("chlnList",chlnList);
							map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
						}else{

								map.put("lryNo","");
							
							List<Station> stnList1 = stationDAO.retrieveStation(cnmt.getCnmtFromSt());
							List<Station> stnList2 = stationDAO.retrieveStation(cnmt.getCnmtToSt());
							List<Customer> consinorList=customerDAO.getCustomer(cnmt.getCnmtConsignor());
							List<Customer> consineeList=customerDAO.getCustomer(cnmt.getCnmtConsignee());
							
							if(!consineeList.isEmpty()){
								map.put("cnsineeName",consineeList.get(0).getCustName());
							}
							if(!consinorList.isEmpty()){
								map.put("cnsinorName",consinorList.get(0).getCustName());
							}
							if(!stnList1.isEmpty()){
								map.put("frStn",stnList1.get(0).getStnName());
								map.put("frState",stnList1.get(0).getStateCode());
								map.put("frStateGst",stnList1.get(0).getState().getStateGST());
							}else{
								map.put("frStn","");
							}
							
							if(!stnList2.isEmpty()){
								map.put("toStn",stnList2.get(0).getStnName());
								map.put("toState",stnList2.get(0).getStateCode());
								map.put("toStateGst",stnList2.get(0).getState().getStateGST());
							}else{
								map.put("toStn","");
							}
							
							map.put("cnmt",cnmt);
							map.put("chlnList","");
							map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
						
						}
					}else{
						int res = customerDAO.checkBillPer(Integer.parseInt(cnmt.getPrBlCode()),billDt);
						System.out.println("value of res = "+res);
						if(res > 0){
							List<Integer> chlnIdList = cnmt_ChallanDAO.getChlnIdByCnmtId(cnmt.getCnmtId());
							System.out.println("size of chlnIdList = "+chlnIdList.size());
							if(!chlnIdList.isEmpty()){
								List<Map<String,Object>> chlnList = new ArrayList<>();
								
								for(int i=0;i<chlnIdList.size();i++){
									Map<String,Object> chlnInfo = challanDAO.getChlnInfoById(chlnIdList.get(i));
									chlnList.add(chlnInfo);
								}
								
								
								if(!chlnList.isEmpty()){
									map.put("lryNo",chlnList.get(0).get("chlnLryNo"));
								}else{
									map.put("lryNo","");
								}
								
								List<Station> stnList1 = stationDAO.retrieveStation(cnmt.getCnmtFromSt());
								List<Station> stnList2 = stationDAO.retrieveStation(cnmt.getCnmtToSt());
								List<Customer> consinorList=customerDAO.getCustomer(cnmt.getCnmtConsignor());
								List<Customer> consineeList=customerDAO.getCustomer(cnmt.getCnmtConsignee());
								
								if(!consineeList.isEmpty()){
									map.put("cnsineeName",consineeList.get(0).getCustName());
								}
								if(!consinorList.isEmpty()){
									map.put("cnsinorName",consinorList.get(0).getCustName());
								}
								
								if(!stnList1.isEmpty()){
									map.put("frStn",stnList1.get(0).getStnName());
									map.put("frState",stnList1.get(0).getStateCode());
									map.put("frStateGst",stnList1.get(0).getState().getStateGST());
								}else{
									map.put("frStn","");
								}
								
								if(!stnList2.isEmpty()){
									map.put("toStn",stnList2.get(0).getStnName());
									map.put("toState",stnList2.get(0).getStateCode());
									map.put("toStateGst",stnList2.get(0).getState().getStateGST());
								}else{
									map.put("toStn","");
								}
								
								map.put("cnmt",cnmt);
								map.put("chlnList",chlnList);
								map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
							}else{

								/*List<Map<String,Object>> chlnList = new ArrayList<>();
								
								for(int i=0;i<chlnIdList.size();i++){
									Map<String,Object> chlnInfo = challanDAO.getChlnInfoById(chlnIdList.get(i));
									chlnList.add(chlnInfo);
								}*/
								
								
								/*if(!chlnList.isEmpty()){
									map.put("lryNo",chlnList.get(0).get("chlnLryNo"));
								}else{*/
									map.put("lryNo","");
								//}
								
								List<Station> stnList1 = stationDAO.retrieveStation(cnmt.getCnmtFromSt());
								List<Station> stnList2 = stationDAO.retrieveStation(cnmt.getCnmtToSt());
								List<Customer> consinorList=customerDAO.getCustomer(cnmt.getCnmtConsignor());
								List<Customer> consineeList=customerDAO.getCustomer(cnmt.getCnmtConsignee());
								
								if(!consineeList.isEmpty()){
									map.put("cnsineeName",consineeList.get(0).getCustName());
								}
								if(!consinorList.isEmpty()){
									map.put("cnsinorName",consinorList.get(0).getCustName());
								}
								
								if(!stnList1.isEmpty()){
									map.put("frStn",stnList1.get(0).getStnName());
									map.put("frState",stnList1.get(0).getStateCode());
									map.put("frStateGst",stnList1.get(0).getState().getStateGST());
								}else{
									map.put("frStn","");
								}
								
								if(!stnList2.isEmpty()){
									map.put("toStn",stnList2.get(0).getStnName());
									map.put("toState",stnList2.get(0).getStateCode());
									map.put("toStateGst",stnList2.get(0).getState().getStateGST());
								}else{
									map.put("toStn","");
								}
								
								map.put("cnmt",cnmt);
								map.put("chlnList","");
								map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
							
							}
						}else{
							System.out.println("Inside MSG :");
							map.put("msg","Please upload scan image of "+cnmtCode+" OR allow Customer for bill permission");
							map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
						}
					}
				}else{
					map.put("msg",cnmtCode+" is invalid cnmt");
					map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
				}
			}else{
				map.put("msg",cnmtCode+" is invalid cnmt");
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
			
			System.out.println("map.get(billBasis)  == "+map.get("billBasis"));
			return map;
		}
		
		@RequestMapping(value = "/getCnRelInfoFrBlAll", method = RequestMethod.POST)
		public @ResponseBody synchronized Object getCnRelInfoFrBlAll(@RequestBody Map<String,Object> clientMap){ 
			logger.info("Enter into /getChInfoFrBl() ");
			Map<String,Object> map = new HashMap<>();
			String cnmtCode = (String) clientMap.get("cnmtCode");
			Date billDt = Date.valueOf((String) clientMap.get("billDt"));
			if(cnmtCode != null){
				List<Cnmt> cnmtList = cnmtDAO.getCnmtList(cnmtCode);
				System.out.println("!cnmtList.isEmpty()="+!cnmtList.isEmpty());
				if(!cnmtList.isEmpty()){
					Cnmt cnmt = cnmtList.get(0);
					if(cnmt.getContractCode().substring(0,3).equalsIgnoreCase("reg")){
						List<RegularContract> regList = regularContractDAO.getRegContractData(cnmt.getContractCode());
						System.out.println("regList.get(0).getRegContBillBasis() = "+regList.get(0).getRegContBillBasis());
						if(!regList.isEmpty()){
							map.put("billBasis", regList.get(0).getRegContBillBasis());
							map.put("contType", regList.get(0).getRegContType());
						}else{
							map.put("billBasis", "chargeWt");
							map.put("contType", "W");
						}
					}else{
						List<DailyContract> dlyList = dailyContractDAO.getDailyContractData(cnmt.getContractCode());
						if(!dlyList.isEmpty()){
							map.put("billBasis", dlyList.get(0).getDlyContBillBasis());
							map.put("contType", dlyList.get(0).getDlyContType());
						}else{
							map.put("billBasis", "chargeWt");
							map.put("contType", "W");
						}
					}				
					
					Integer cmId = cnmt.getCmId();
				
					if(cmId > 0){
						List<Integer> chlnIdList = cnmt_ChallanDAO.getChlnIdByCnmtId(cnmt.getCnmtId());
						if(!chlnIdList.isEmpty()){
							List<Map<String,Object>> chlnList = new ArrayList<>();
							for(int i=0;i<chlnIdList.size();i++){
								//List<Challan> actChList = new ArrayList<>();
								//actChList = challanDAO.getChallanById(chlnIdList.get(i));
								Map<String,Object> chlnInfo = challanDAO.getChlnInfoById(chlnIdList.get(i));
								chlnList.add(chlnInfo);
							}
							
							
							if(!chlnList.isEmpty()){
								map.put("lryNo",chlnList.get(0).get("chlnLryNo"));
							}else{
								map.put("lryNo","");
							}
							
							
							List<Station> stnList1 = stationDAO.retrieveStation(cnmt.getCnmtFromSt());
							List<Station> stnList2 = stationDAO.retrieveStation(cnmt.getCnmtToSt());
							List<Customer> consinorList=customerDAO.getCustomer(cnmt.getCnmtConsignor());
							List<Customer> consineeList=customerDAO.getCustomer(cnmt.getCnmtConsignee());
							
							if(!consineeList.isEmpty()){
								map.put("cnsineeName",consineeList.get(0).getCustName());
							}
							if(!consinorList.isEmpty()){
								map.put("cnsinorName",consinorList.get(0).getCustName());
							}
							if(!stnList1.isEmpty()){
								map.put("frStn",stnList1.get(0).getStnName());
								map.put("frState",stnList1.get(0).getStateCode());
								map.put("frStateGst",stnList1.get(0).getState().getStateGST());
							}else{
								map.put("frStn","");
							}
							
							if(!stnList2.isEmpty()){
								map.put("toStn",stnList2.get(0).getStnName());
								map.put("toState",stnList2.get(0).getStateCode());
								map.put("toStateGst",stnList2.get(0).getState().getStateGST());
							}else{
								map.put("toStn","");
							}
							
							map.put("cnmt",cnmt);
							map.put("chlnList",chlnList);
							map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
						}else{
							map.put("msg","Please enter challan of "+cnmt.getCnmtCode());
							map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
						}
					}else{
						int res = customerDAO.checkBillPer(Integer.parseInt(cnmt.getPrBlCode()),billDt);
						System.out.println("value of res = "+res);
						if(res > 0){
							List<Integer> chlnIdList = cnmt_ChallanDAO.getChlnIdByCnmtId(cnmt.getCnmtId());
							System.out.println("size of chlnIdList = "+chlnIdList.size());
							if(!chlnIdList.isEmpty()){
								List<Map<String,Object>> chlnList = new ArrayList<>();
								
								for(int i=0;i<chlnIdList.size();i++){
									Map<String,Object> chlnInfo = challanDAO.getChlnInfoById(chlnIdList.get(i));
									chlnList.add(chlnInfo);
								}
								
								
								if(!chlnList.isEmpty()){
									map.put("lryNo",chlnList.get(0).get("chlnLryNo"));
								}else{
									map.put("lryNo","");
								}
								
								List<Station> stnList1 = stationDAO.retrieveStation(cnmt.getCnmtFromSt());
								List<Station> stnList2 = stationDAO.retrieveStation(cnmt.getCnmtToSt());
								List<Customer> consinorList=customerDAO.getCustomer(cnmt.getCnmtConsignor());
								List<Customer> consineeList=customerDAO.getCustomer(cnmt.getCnmtConsignee());
								
								if(!consineeList.isEmpty()){
									map.put("cnsineeName",consineeList.get(0).getCustName());
								}
								if(!consinorList.isEmpty()){
									map.put("cnsinorName",consinorList.get(0).getCustName());
								}
								if(!stnList1.isEmpty()){
									map.put("frStn",stnList1.get(0).getStnName());
									map.put("frState",stnList1.get(0).getStateCode());
									map.put("frStateGst",stnList1.get(0).getState().getStateGST());
								}else{
									map.put("frStn","");
								}
								
								if(!stnList2.isEmpty()){
									map.put("toStn",stnList2.get(0).getStnName());
									map.put("toState",stnList2.get(0).getStateCode());
									map.put("toStateGst",stnList2.get(0).getState().getStateGST());
								}else{
									map.put("toStn","");
								}
								
								map.put("cnmt",cnmt);
								map.put("chlnList",chlnList);
								map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
							}else{
								map.put("msg","Please enter challan of "+cnmt.getCnmtCode());
								map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
							}
						}else{
							System.out.println("Inside MSG :");
							map.put("msg","Please upload scan image of "+cnmtCode+" OR allow Customer for bill permission");
							map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
						}
					}
				}else{
					map.put("msg",cnmtCode+" is invalid cnmt");
					map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
				}
			}else{
				map.put("msg",cnmtCode+" is invalid cnmt");
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
			
			System.out.println("map.get(billBasis)  == "+map.get("billBasis"));
			return map;
		}
		
	
}