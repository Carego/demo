package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import com.mylogistics.DAO.VehicleEngagementDAO;
import com.mylogistics.model.CampaignResults;
import com.mylogistics.model.VehicleEngagement;

public class VehicleEngagementDAOImpl implements VehicleEngagementDAO {

	//@Autowired
	private SessionFactory sessionFactory;
	
	public VehicleEngagementDAOImpl(SessionFactory sessionFactory) {
		super();
		this.sessionFactory = sessionFactory;
		
	}
	
	@Override
	public int saveVehicleEngagement(VehicleEngagement engagement,Session session) {
		
		return  (int) session.save(engagement);
		
	}
	
	@Override
	public void updateVehicleEngagement(VehicleEngagement engagement,Session session) {
		
		 session.update(engagement);
		
	}
	
	
	@Override
	public List<VehicleEngagement> getVehicleEngagementByCampainResult(List<CampaignResults> campList ){
		List<VehicleEngagement> veList=new ArrayList<>();
		
		
		Session session=this.sessionFactory.openSession();
		veList=session.createCriteria(VehicleEngagement.class)
				.add(Restrictions.in("campaignResults", campList))
				.add(Restrictions.disjunction().add(Restrictions.isNull("placeReqStatus"))
						.add(Restrictions.eq("placeReqStatus", "Pending Challan"))).list();
		
		session.clear();
		session.close();
		return veList;
	}
	
	@Override
	public boolean checkVehicleEngagementByVehNoAndCampainResult(CampaignResults camp,String vehNo,Session session) {
		
		boolean flag=false;
		
		List<VehicleEngagement> veList=session.createCriteria(VehicleEngagement.class)
				.add(Restrictions.eq("campaignResults", camp))
				.add(Restrictions.eq("vehicleNo", vehNo)).list();

		if(!veList.isEmpty())
			flag=true;
		
		return flag;
		
	}
	
	
	@Override
	public VehicleEngagement getVehicleEngagementByVeId(int vEId) {
		
		VehicleEngagement engagement=null;
		Session session=this.sessionFactory.openSession();
		try {
			
			engagement=(VehicleEngagement) session.get(VehicleEngagement.class, vEId);
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			session.close();
		}
		
		return engagement;
	}

	
}
