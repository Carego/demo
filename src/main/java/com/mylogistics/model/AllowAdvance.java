package com.mylogistics.model;

import java.sql.Date;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="allowadvance")
public class AllowAdvance {
	
	@Id
	@Column(name="advAlwId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int advAlwId;
	
	@Column(name="lryNo",length=20)
	private String lryNo; 
	
	@Column(name="chlnDt")
	private Date chlnDt;
	
	@Column(name="allow")
	private boolean allow;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;
	
	@Column(name="requestBy",length=4)
	private String requestBy;
	
	@Column(name="allowedBy",length=4)
	private String allowedBy;
	
	@Column(name="bCode",length=3)
	private String bCode;
	
	@Column(name="chlnNo",length=40)
	private String chlnNo;
	
	@Column(name="cnmtNo",length=40)
	private String cnmtNo;
	
	@Column(name="fromStn",length=50)
	private String fromStn;
	
	@Column(name="toStn",length=50)
	private String toStn;

	public int getAdvAlwId() {
		return advAlwId;
	}

	public void setAdvAlwId(int advAlwId) {
		this.advAlwId = advAlwId;
	}

	public String getLryNo() {
		return lryNo;
	}

	public void setLryNo(String lryNo) {
		this.lryNo = lryNo;
	}

	public Date getChlnDt() {
		return chlnDt;
	}

	public void setChlnDt(Date chlnDt) {
		this.chlnDt = chlnDt;
	}

	public boolean isAllow() {
		return allow;
	}

	public void setAllow(boolean allow) {
		this.allow = allow;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public String getRequestBy() {
		return requestBy;
	}

	public void setRequestBy(String requestBy) {
		this.requestBy = requestBy;
	}

	public String getAllowedBy() {
		return allowedBy;
	}

	public void setAllowedBy(String allowedBy) {
		this.allowedBy = allowedBy;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public String getChlnNo() {
		return chlnNo;
	}

	public void setChlnNo(String chlnNo) {
		this.chlnNo = chlnNo;
	}

	public String getCnmtNo() {
		return cnmtNo;
	}

	public void setCnmtNo(String cnmtNo) {
		this.cnmtNo = cnmtNo;
	}

	public String getFromStn() {
		return fromStn;
	}

	public void setFromStn(String fromStn) {
		this.fromStn = fromStn;
	}

	public String getToStn() {
		return toStn;
	}

	public void setToStn(String toStn) {
		this.toStn = toStn;
	}
	

}
