package com.mylogistics.pdf;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;

import com.mylogistics.DAO.CustomerRepresentativeDAO;
import com.mylogistics.constants.RateByKmCNTS;
import com.mylogistics.model.Cnmt;
import com.mylogistics.model.ContToStn;
import com.mylogistics.model.Customer;
import com.mylogistics.model.CustomerRepresentative;
import com.mylogistics.model.Indent;
import com.mylogistics.model.VehicleEngagement;
import com.mylogistics.services.CodePatternService;
import com.mylogistics.services.mail.SmtpMailSender;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class CnmtPDFBuilder  extends AbstractITextPdfView {

	
	protected Font font;
	@Override
	protected void buildPdfDocument(Map<String, Object> model, Document document, PdfWriter writer,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		
		Cnmt cnmt=(Cnmt) model.get("cnmt");
		ContToStn cts=(ContToStn) model.get("cts");
		Indent indent=(Indent) model.get("indent");
		String fromStn=(String) model.get("fromStn");
		String toStn=(String) model.get("toStn");
		VehicleEngagement ve=(VehicleEngagement) model.get("ve");
		Customer consinor= (Customer) model.get("consignor");
		Customer consinee= (Customer) model.get("consignee");
		String empFa="0"+(200000+Integer.parseInt(cnmt.getUserCode()));
		//System.out.println("empFa"+empFa);
		 List<CustomerRepresentative> crList=(List<CustomerRepresentative>) model.get("crList");
		
        font = new Font(FontFamily.HELVETICA, 8);
        
        PdfWriter.getInstance(document, new FileOutputStream("/var/www/html/Erp_Image/App/OwnerKycDocs/"+cnmt.getCnmtCode()+".pdf"));
	    
        
        document.open();
        PdfPTable table = new PdfPTable(5);
        table.setWidthPercentage(100.0f);
		table.setWidths(new float[] {0.1f, 0.1f,0.2f, 0.6f,0.3f});
		//table.setSpacingBefore(4);
		
		String path="/var/www/html/Erp_Image/App/OwnerKycDocs/sqrlogo.png";
		Image img = Image.getInstance(path);
        
		PdfPCell headerCell2 = new PdfPCell(img, true);
		headerCell2.setColspan(2);
		headerCell2.setRowspan(3);
		headerCell2.setVerticalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell headerCell1 = new PdfPCell();
		headerCell1.setBackgroundColor(BaseColor.DARK_GRAY);
		headerCell1.setColspan(2);
		headerCell1.setPaddingTop(5);
		headerCell1.setPaddingBottom(5);
		headerCell1.setPaddingLeft(2);
		headerCell1.setPaddingRight(2);
		headerCell1.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		
		PdfPCell subHeaderCell = new PdfPCell();
		subHeaderCell.setBackgroundColor(BaseColor.DARK_GRAY);
		subHeaderCell.setColspan(3);
		subHeaderCell.setPaddingTop(5);
		subHeaderCell.setPaddingBottom(5);
		subHeaderCell.setPaddingLeft(2);
		subHeaderCell.setPaddingRight(2);
		subHeaderCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell subHeaderCell2 = new PdfPCell();
		subHeaderCell2.setBackgroundColor(BaseColor.DARK_GRAY);
		subHeaderCell2.setColspan(1);
		subHeaderCell2.setPaddingTop(5);
		subHeaderCell2.setPaddingBottom(5);
		subHeaderCell2.setPaddingLeft(2);
		subHeaderCell2.setPaddingRight(2);
		subHeaderCell2.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell subHeaderCell3 = new PdfPCell();
		subHeaderCell3.setBackgroundColor(BaseColor.DARK_GRAY);
		subHeaderCell3.setColspan(4);
		subHeaderCell3.setPaddingTop(5);
		subHeaderCell3.setPaddingBottom(5);
		subHeaderCell3.setPaddingLeft(2);
		subHeaderCell3.setPaddingRight(2);
		subHeaderCell3.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell subHeaderCell4 = new PdfPCell();
		subHeaderCell4.setBackgroundColor(BaseColor.DARK_GRAY);
		subHeaderCell4.setColspan(1);
		subHeaderCell4.setRowspan(2);
		subHeaderCell4.setPaddingTop(5);
		subHeaderCell4.setPaddingBottom(5);
		subHeaderCell4.setPaddingLeft(2);
		subHeaderCell4.setPaddingRight(2);
		subHeaderCell4.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		
		Font headerfont = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
		headerfont.setSize(8);
		headerfont.setColor(BaseColor.WHITE);
		
		Font headerfont1 = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
		headerfont1.setSize(8);
		headerfont1.setColor(BaseColor.BLACK);
		
		
		
		// header title
		PdfPCell headerTitle = new PdfPCell();
		headerTitle.setBackgroundColor(BaseColor.WHITE);
		headerTitle.setColspan(2);
		headerTitle.setPaddingTop(5);
		headerTitle.setPaddingBottom(5);
		headerTitle.setPaddingLeft(2);
		headerTitle.setPaddingRight(2);
		headerTitle.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell cnmtTitle = new PdfPCell();
		cnmtTitle.setBackgroundColor(BaseColor.WHITE);
		cnmtTitle.setRowspan(2);
		cnmtTitle.setPaddingTop(5);
		cnmtTitle.setPaddingBottom(5);
		cnmtTitle.setPaddingLeft(2);
		cnmtTitle.setPaddingRight(2);
		cnmtTitle.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		Font headerTitlefont = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
		headerTitlefont.setSize(13);
		headerTitlefont.setColor(BaseColor.BLACK);
		
		
		
		
		SimpleDateFormat parseDateFormat=new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat printDateFormat=new SimpleDateFormat("dd MMMMMMMMMM yyyy");
 
        
        Calendar c=Calendar.getInstance();
        c.setTime(parseDateFormat.parse(cnmt.getCnmtGtOutDt().toString()));
        c.add(Calendar.DATE, cts.getCtsToTransitDay());
        java.util.Date expectedDeleveryDate=c.getTime();
        
		/*headerCell1.setPhrase(new Phrase("Financial Year: ", headerfont));
		table.addCell(headerCell1);
		headerCell2.setPhrase(new Phrase("Upto Date: ", headerfont));
		table.addCell(headerCell2);*/
		
		
		
		// define font for table header row
		
		PdfPTable subTable = new PdfPTable(2);
		
        table.setWidthPercentage(100);
        
               
        table.addCell(headerCell2);
        /*table.addCell(createCell("Care Go Logistics Private Limited \n"
        		+ "Registered Office: S 405, Greater Kailash, Part II, New Delhi, Delhi, 110048\n"
        		+ "Corporate Office: SCO 44, Old Judicial Complex, Civil Lines, Gurgaon, Haryana, 122001", 2, 1, PdfPCell.BOX));
       */ headerTitle.setPhrase(new Phrase("Care Go Logistics Private Limited", headerTitlefont));
		table.addCell(headerTitle);
		cnmtTitle.setPhrase(new Phrase("CNMT No.\n "+cnmt.getCnmtCode(), headerTitlefont));
		table.addCell(cnmtTitle);
		//table.addCell(createCell("", 1, 1, PdfPCell.BOX));
		//  table.addCell(createCell("CNMT No.\n "+cnmt.getCnmtCode(), 1, 2, PdfPCell.BOX)); 
//        table.addCell(createCell("Registered Office: S 405, Greater Kailash, Part II, New Delhi, Delhi, 110048\n"
//        		+ "Corporate Office: SCO 44, Old Judicial Complex, Civil Lines, Gurgaon, Haryana, 122001", 2, 1, PdfPCell.BOX));
       
        headerTitle.setPhrase(new Phrase("Registered Office: S 405, Greater Kailash, Part II, New Delhi, Delhi, 110048\n" + 
        "Corp. Office: SCO 44,Old Judicial Complex,Civil Lines,Gurgaon,Haryana, 122001", headerfont1));
		table.addCell(headerTitle);
      
      
        headerCell1.setPhrase(new Phrase("Ph: +91-124-4205305 #Email: ho@carego.co MSME Regd. No.: HR05E0000104", headerfont));
		table.addCell(headerCell1);
        
        
     //   table.addCell(createCell("Ph: +91-124-4205305 #Email: ho@carego.co MSME Regd. No.: HR05E0000104", 2, 1, PdfPCell.BOX));
        table.addCell(createCell("Please use above number for future correspondance", 1, 1, PdfPCell.BOX));
        
        table.addCell(createCell("CIN:U60231DL2015PTC279265", 3, 1, PdfPCell.BOX));
        table.addCell(createCell("Carriage By Road Regn. No. 390/STA/12/2015", 1, 1, PdfPCell.BOX));
        table.addCell(createCell("PAN No.: AAGCC0032K", 1, 1, PdfPCell.BOX));
        
        subHeaderCell.setPhrase(new Phrase("ORIGIN STATION/CITY", headerfont));
        table.addCell(subHeaderCell);
       // table.addCell(createCell("ORIGIN STATION/CITY", 3, 1, PdfPCell.BOX));
        subHeaderCell2.setPhrase(new Phrase("DESTINATION STATION/CITY", headerfont));
        table.addCell(subHeaderCell2);
       
       // table.addCell(createCell("DESTINATION STATION/CITY", 1, 1, PdfPCell.BOX));
        subHeaderCell2.setPhrase(new Phrase("", headerfont));//DISTANCE
        table.addCell(subHeaderCell2);
        //table.addCell(createCell("DISTANCE", 1, 1, PdfPCell.BOX));
        
        table.addCell(createCell(fromStn, 3, 1, PdfPCell.BOX));
        table.addCell(createCell(toStn, 1, 1, PdfPCell.BOX));
        subTable.addCell(createCell("", 1, 1, PdfPCell.BOX));
        subTable.addCell(createCell("", 1, 1, PdfPCell.BOX));//+cts.getCtsToTransitDay()
        table.addCell(subTable);
        
        
        subHeaderCell.setPhrase(new Phrase("CONSIGNOR", headerfont));
        table.addCell(subHeaderCell);
       // table.addCell(createCell("ORIGIN STATION/CITY", 3, 1, PdfPCell.BOX));
        subHeaderCell2.setPhrase(new Phrase("CONSIGNEE", headerfont));
        table.addCell(subHeaderCell2);
       
       // table.addCell(createCell("DESTINATION STATION/CITY", 1, 1, PdfPCell.BOX));
        subHeaderCell2.setPhrase(new Phrase("CNMT DATE", headerfont));
        table.addCell(subHeaderCell2);
        
       /* table.addCell(createCell("CONSIGNOR", 3, 1, PdfPCell.BOX));
        table.addCell(createCell("CONSIGNEE", 1, 1, PdfPCell.BOX));
        table.addCell(createCell("CNMT DATE", 1, 1, PdfPCell.BOX));
        */
        
        table.addCell(createCell(consinor.getCustName()+"\n"+consinor.getCustAdd(), 3, 4, PdfPCell.BOX));
        table.addCell(createCell(consinee.getCustName()+"\n"+consinee.getCustAdd(), 1, 4, PdfPCell.BOX));
       
        
        table.addCell(createCell((printDateFormat.format(parseDateFormat.parse(cnmt.getCnmtDt().toString()))).toString(), 1, 1, PdfPCell.BOX));
       
        subHeaderCell2.setPhrase(new Phrase("DISPATCH DATE", headerfont));
        table.addCell(subHeaderCell2);
        //table.addCell(createCell("DISPATCH DATE", 1, 1, PdfPCell.BOX));
        table.addCell(createCell((printDateFormat.format(parseDateFormat.parse(cnmt.getCnmtGtOutDt().toString()))).toString(), 1, 1, PdfPCell.BOX));
        
        subHeaderCell2.setPhrase(new Phrase("EXPECTED DELIVERY DATE", headerfont));
        table.addCell(subHeaderCell2);
        //table.addCell(createCell("EXPECTED DELEVIRY DATE ", 1, 1, PdfPCell.BOX));
        
        table.addCell(createCell("#GSTIN:"+consinor.getCustGstNo(), 3, 1, PdfPCell.BOX));
        table.addCell(createCell("#GSTIN:"+consinee.getCustGstNo(), 1, 1, PdfPCell.BOX));
        table.addCell(createCell(printDateFormat.format(expectedDeleveryDate), 1, 1, PdfPCell.BOX));
        
        
        subHeaderCell.setPhrase(new Phrase("EWAY Bill No", headerfont));
        table.addCell(subHeaderCell);
       // table.addCell(createCell("ORIGIN STATION/CITY", 3, 1, PdfPCell.BOX));
        subHeaderCell2.setPhrase(new Phrase("EWAY Bill Valid From", headerfont));
        table.addCell(subHeaderCell2);
       
       // table.addCell(createCell("DESTINATION STATION/CITY", 1, 1, PdfPCell.BOX));
        subHeaderCell2.setPhrase(new Phrase("EWAY Bill Valid Upto", headerfont));
        table.addCell(subHeaderCell2);
        
        /*table.addCell(createCell("EWAY Bill No", 3, 1, PdfPCell.BOX));
        table.addCell(createCell("EWAY Bill Valid From", 1, 1, PdfPCell.BOX));
        table.addCell(createCell("EWAY Bill Valid Upto", 1, 1, PdfPCell.BOX));
        */
        table.addCell(createCell(cnmt.getCnmtEWBNo(), 3, 1, PdfPCell.BOX));
        table.addCell(createCell((printDateFormat.format(parseDateFormat.parse(cnmt.getCnmtEWBDt().toString()))).toString(), 1, 1, PdfPCell.BOX));
        table.addCell(createCell((printDateFormat.format(parseDateFormat.parse(cnmt.getCnmtEWBValidDt().toString()))).toString(), 1, 1, PdfPCell.BOX));
        
        
        
        
        subHeaderCell.setPhrase(new Phrase("Invoice No.", headerfont));
        table.addCell(subHeaderCell);
       // table.addCell(createCell("ORIGIN STATION/CITY", 3, 1, PdfPCell.BOX));
        subHeaderCell2.setPhrase(new Phrase("Invoice Date", headerfont));
        table.addCell(subHeaderCell2);
       
       // table.addCell(createCell("DESTINATION STATION/CITY", 1, 1, PdfPCell.BOX));
        subHeaderCell2.setPhrase(new Phrase("Invoice Value", headerfont));
        table.addCell(subHeaderCell2);
        
        /*table.addCell(createCell("EWAY Bill No", 3, 1, PdfPCell.BOX));
        table.addCell(createCell("EWAY Bill Valid From", 1, 1, PdfPCell.BOX));
        table.addCell(createCell("EWAY Bill Valid Upto", 1, 1, PdfPCell.BOX));
        */
        int row=0;
        //PdfPTable subTable1 = new PdfPTable(4);
        if(cnmt.getCnmtInvoiceNo()!=null) {
        	 for(int i=0;i<cnmt.getCnmtInvoiceNo().size();i++) {
             	
             	
        		 table.addCell(createCell(cnmt.getCnmtInvoiceNo().get(i).get("invoiceNo").toString(), 3, 1, PdfPCell.BOX));
        		 table.addCell(createCell((printDateFormat.format(parseDateFormat.parse(cnmt.getCnmtInvoiceNo().get(i).get("invoiceDt").toString()))).toString(), 1, 1, PdfPCell.BOX));
        		 table.addCell(createCell("", 1, 1, PdfPCell.BOX));
             	row=i;
             	
             }
         //table.addCell(subTable1);
         //table.addCell(createCell(""+cnmt.getCnmtVOG(), 1, 1, PdfPCell.BOX));
        
        }
            /*
            	
            	subHeaderCell2.setPhrase(new Phrase("Invoice No.", headerfont));
            	subTable2.addCell(subHeaderCell2);
            	subTable2.addCell(createCell(cnmt.getCnmtInvoiceNo().get(i).get("invoiceNo").toString(), 1, 1, PdfPCell.BOX));
            	
            	subHeaderCell2.setPhrase(new Phrase("Invoice Date", headerfont));
                subTable2.addCell(subHeaderCell2);
               
            	//subTable2.addCell(createCell("Invoice No.", 1, 1, PdfPCell.BOX));
            	//subTable2.addCell(createCell(cnmt.getCnmtInvoiceNo().get(i).get("invoiceNo").toString(), 1, 1, PdfPCell.BOX));
            	//subTable2.addCell(createCell("Invoice Date", 1, 1, PdfPCell.BOX));
            	subTable2.addCell(createCell((printDateFormat.format(parseDateFormat.parse(cnmt.getCnmtInvoiceNo().get(i).get("invoiceDt").toString()))).toString(), 1, 1, PdfPCell.BOX));
            
            */
        
        
        
        
        subHeaderCell3.setPhrase(new Phrase("PACKAGE DETAILs (AS PER INVOICE)", headerfont));
        table.addCell(subHeaderCell3);
       // table.addCell(createCell("ORIGIN STATION/CITY", 3, 1, PdfPCell.BOX));
        //subHeaderCell4.setPhrase(new Phrase("INVOICE DETAILS", headerfont));
        subHeaderCell4.setPhrase(new Phrase(""+cnmt.getCnmtVOG(), headerfont));
        table.addCell(subHeaderCell4);
       
        
       /* table.addCell(createCell("PACKAGE DETAILs (AS PER INVOICE)", 4, 1, PdfPCell.BOX));
        table.addCell(createCell("INVOICE DETAILS", 1, 2, PdfPCell.BOX));
        */
        table.addCell(createCell("TYPE", 1, 1, PdfPCell.BOX));
        table.addCell(createCell("QTY.", 1, 1, PdfPCell.BOX));
        table.addCell(createCell("DESCRIPTION", 2, 1, PdfPCell.BOX));
        
        table.addCell(createCell(cnmt.getCnmtPackaging(), 1, 1, PdfPCell.BOX));
        table.addCell(createCell(String.valueOf(cnmt.getCnmtNoOfPkg()), 1, 1, PdfPCell.BOX));
        table.addCell(createCell(cnmt.getCnmtProductType(), 2, 1, PdfPCell.BOX));
        
        PdfPTable subTable2 = new PdfPTable(1);
        /*if(cnmt.getCnmtInvoiceNo()!=null)
        for(int i=0;i<cnmt.getCnmtInvoiceNo().size();i++) {
        	
        	subHeaderCell2.setPhrase(new Phrase("Invoice No.", headerfont));
        	subTable2.addCell(subHeaderCell2);
        	subTable2.addCell(createCell(cnmt.getCnmtInvoiceNo().get(i).get("invoiceNo").toString(), 1, 1, PdfPCell.BOX));
        	
        	subHeaderCell2.setPhrase(new Phrase("Invoice Date", headerfont));
            subTable2.addCell(subHeaderCell2);
           
        	//subTable2.addCell(createCell("Invoice No.", 1, 1, PdfPCell.BOX));
        	//subTable2.addCell(createCell(cnmt.getCnmtInvoiceNo().get(i).get("invoiceNo").toString(), 1, 1, PdfPCell.BOX));
        	//subTable2.addCell(createCell("Invoice Date", 1, 1, PdfPCell.BOX));
        	subTable2.addCell(createCell((printDateFormat.format(parseDateFormat.parse(cnmt.getCnmtInvoiceNo().get(i).get("invoiceDt").toString()))).toString(), 1, 1, PdfPCell.BOX));
        }
        
        subHeaderCell2.setPhrase(new Phrase("Invoice Value", headerfont));
        subTable2.addCell(subHeaderCell2);
       //subTable2.addCell(createCell("Invoice Value", 1, 1, PdfPCell.BOX));
        subTable2.addCell(createCell(""+cnmt.getCnmtVOG(), 1, 1, PdfPCell.BOX));
       */
        subHeaderCell2.setPhrase(new Phrase("ACTUAL WEIGHT", headerfont));
        subTable2.addCell(subHeaderCell2);
        //  subTable2.addCell(createCell("ACTUAL WEIGHT", 1, 1, PdfPCell.BOX));
        subTable2.addCell(createCell(""+(cnmt.getCnmtActualWt()/1000), 1, 1, PdfPCell.BOX));
        
        subHeaderCell2.setPhrase(new Phrase("CHARGED WEIGHT", headerfont));
        subTable2.addCell(subHeaderCell2);
        //subTable2.addCell(createCell("CHARGED WEIGHT", 1, 1, PdfPCell.BOX));
        subTable2.addCell(createCell(""+(cnmt.getCnmtGuaranteeWt()/1000), 1, 1, PdfPCell.BOX));
        
        subHeaderCell2.setPhrase(new Phrase("VEHICLE NO.", headerfont));
        subTable2.addCell(subHeaderCell2);
        //subTable2.addCell(createCell("VEHICLE NO.", 1, 1, PdfPCell.BOX));
        subTable2.addCell(createCell(ve.getVehicleNo(), 1, 1, PdfPCell.BOX));
        
        subHeaderCell2.setPhrase(new Phrase("VEHICLE TYPE", headerfont));
        subTable2.addCell(subHeaderCell2);
        //subTable2.addCell(createCell("VEHICLE TYPE", 1, 1, PdfPCell.BOX));
        subTable2.addCell(createCell(indent.getVehicleType(), 1, 1, PdfPCell.BOX));
        
        subHeaderCell2.setPhrase(new Phrase("CNMT DIMENSION", headerfont));
        subTable2.addCell(subHeaderCell2);
        //subTable2.addCell(createCell("CNMT DIMENSION", 1, 1, PdfPCell.BOX));
        subTable2.addCell(createCell(indent.getCnmtDimensionType(), 1, 1, PdfPCell.BOX));
        // if condition to check dimention type
        subTable2.addCell(createCell(indent.getDimension(), 1, 1, PdfPCell.BOX));
        /*subTable2.addCell(createCell("CAREGO Employee Staff Code: "+empFa
        		+ "\nTerms and Conditions:"
        		+ "\n1: All charges shall be applicable as per the contract of carriage."
        		+ "\n2: Please do not pay to the lorry driver."
        		+ "\n3: For tracking of the consignment send email with CNMT No. at tracking@carego.co or visit our website www.carego.co", 1, 1, PdfPCell.BOX));
    	*/
        table.addCell(subTable2);
        
       // table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
       // table.addCell(createCell("------------------------------------------------------------------------------------------------------------", 5, 1, PdfPCell.NO_BORDER));
       /* table.addCell(new Phrase("-------"));
        table.addCell(new Phrase("--------"));
        table.addCell(new Phrase("-----"));
        table.addCell(new Phrase("-----"));*/
        
        
        
        PdfPTable footerTale = new PdfPTable(1);
        footerTale.setWidthPercentage(100.0f);
        footerTale.addCell(new Phrase("CAREGO Employee Staff Code: "+empFa
        		+"\nTerms and Conditions:" 
        		+ "\n1: All charges shall be applicable as per the contract of carriage."
        		+ "\n2: Please do not pay to the lorry driver."
        		+ "\n3: For tracking of the consignment send email with CNMT No. at tracking@carego.co or visit our website www.carego.co", font));
        
        footerTale.addCell(createCell("------------------------------------------------------------------------------------------------------------------------------", 1, 1, PdfPCell.NO_BORDER));
        footerTale.addCell(createCell("TO BE FILLED AFTER UNLOADING OF GOODS AT THE DESTINATION ADDRESS", 1, 1, PdfPCell.BOX));
        footerTale.addCell(createCell("DELIVERY ACKNOWLEDGEMENT", 1, 1, PdfPCell.BOX));
        
        //footerTale.addCell(createCell("DELIVERY ACKNOWLEDGEMENT", 1, 1, PdfPCell.BOX));
        //footerTale.getDefaultCell().setBorder(Rectangle.NO_BORDER);
        footerTale.addCell(new Phrase("I have received the goods of CNMT no. "+cnmt.getCnmtCode()+" on date__________at time_____ in good/damage/short dimention, details are given below:"
        		+ "\nNo. of package received_____in good condition."
        		+ "\nNo. of package received_____in damaged condition."
        		+ "\nNo. of short packages_____"
        		+ "\nName of Receiving Person:"
        		+ "\nMobile No. of Receiving Person:"
        		+ "\nUnloading Address: Same as mentioned above in consignee/Other(Please mention__________________________________)",font));
        //footerTale.getDefaultCell().setBorder(Rectangle.NO_BORDER);
       
        footerTale.addCell(new Phrase("Any other remarks, please mention:\n\n\n\n\n\n"
        		+ "                                                                                                                                  Stamp with Signature                                  Date & Location",font));
        ///TestServer/src/main/java/com/TestServer/UtilityFiles/Lohit-Devanagari.ttf
        BaseFont unicode = BaseFont.createFont("/var/www/html/Erp_Image/App/OwnerKycDocs/Lohit-Devanagari.ttf",BaseFont.IDENTITY_H,BaseFont.EMBEDDED);
        Font font1=new Font(unicode,8,Font.NORMAL,new BaseColor(1,1,1));

        PdfPTable footerSubTale = new PdfPTable(1);
        footerSubTale.setWidthPercentage(100.0f);
        footerSubTale.getDefaultCell().setBorder(Rectangle.NO_BORDER);
        
        footerSubTale.addCell(new Phrase("ATTENTION DRIVER:\n"
        		+ "After unloading the goods at the delivery address, please send the image of this document on WhatsApp No. 9899968827 as soon as you get the DELIVERY ACKNOWLEDGEMENT signed & stamped by the receiving person."
        		+ "50% of your balance amount will be transferred as soon we receive the image on whatsApp.\n\n",font));
        footerSubTale.addCell(new Phrase("चालक कृपया ध्यान दें:\r\n" + 
        		"डिलीवरी पते पर सामान उतारने के बाद, कृपया इस दस्तावेज़ की छवि को व्हाट्सएप नंबर 9899968827 पर  भेजें जैसे ही आप प्राप्त व्यक्ति द्वारा"
        		+ "  हस्ताक्षरित और मुहर लगी DELIVERY ACKNOWLEDGMENT प्राप्त करते हैं। whatsApp पर  हमें छवि मिलते ही आपकी शेष राशि का 50% हस्तांतरित हो जाएगा।",font1));
       footerTale.addCell(footerSubTale);
        document.add(table);
        document.add(footerTale);
        document.close();
        
        //file.close();
        
       // List<CustomerRepresentative> crList=customerRepresentativeDAO.getCustomerRepByCustCode(cnmt.getCustCode());
        
        List<String> bcc=new ArrayList<>();
        List<String> to=new ArrayList<>();
        
        bcc.add("ag@carego.co");
        bcc.add("ms@carego.co");
        bcc.add("ho@carego.co");
        bcc.add("sales@carego.co");
        bcc.add("tracking@carego.co");
        bcc.add("billing@carego.co");
        bcc.add("techops@carego.co");
        List<String> phoneNoList=new ArrayList<>();
        if(!crList.isEmpty()) {
        	for(CustomerRepresentative cr:crList) {
        		if(cr.getCrEmailId()!=null && !cr.getCrEmailId().equalsIgnoreCase("")) {
        		System.out.println(cr.getCrEmailId());
        				//to.add(cr.getCrEmailId());
        		}
        		
        		if(cr.getCrMobileNo()!=null && !cr.getCrMobileNo().equalsIgnoreCase("")) {
            		System.out.println(cr.getCrMobileNo());
            			//	phoneNoList.add(cr.getCrMobileNo());
            		}
        	}
        }
        
        SmtpMailSender mailSend=new SmtpMailSender();
        
       
        String subject="Consignment No. "+cnmt.getCnmtCode()+" generated.";
        String text="Dear Sir/Madam\n  Consignment No."+cnmt.getCnmtCode()+" is generated from station "+fromStn+ " to "+toStn+". PFA the Consignment details.";
        String filePath="/var/www/html/Erp_Image/App/OwnerKycDocs/"+cnmt.getCnmtCode()+".pdf";
        mailSend.sendCnmtOnMail( to,bcc, subject, text, filePath);
        
       
        
        phoneNoList.add("9810090729");
        phoneNoList.add("9718663997");
        phoneNoList.add("8468991703");
        phoneNoList.add("9599282616");
        phoneNoList.add("9873919313");
        phoneNoList.add("9999966633");
        phoneNoList.add("7760503926");
        phoneNoList.add("7905189881");
        String msg="Consignment No."+cnmt.getCnmtCode()+" is generated from station "+fromStn+ " to "+toStn+".\nPlease check your mail for Consignment details.";
        if(!phoneNoList.isEmpty()) {
        	mailSend.sendSMS(phoneNoList,msg);
        }
        
	}
    
    public PdfPCell createCell(String content, int colspan, int rowspan, int border) {
        PdfPCell cell = new PdfPCell(new Phrase(content, font));
        cell.setColspan(colspan);
        cell.setRowspan(rowspan);
        cell.setBorder(border);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        return cell;
    
	}
	

}
