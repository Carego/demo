package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import com.mylogistics.DAO.CampaignResultDAO;
import com.mylogistics.model.CampaignResults;

public class CampaignResultDAOImpl implements CampaignResultDAO{

	
	//@Autowired
		private SessionFactory sessionFactory;
		
		//@Autowired
		private Session session;
		
		public CampaignResultDAOImpl(SessionFactory sessionFactory) {
			super();
			this.sessionFactory = sessionFactory;
			
		}

		@Override
		public int saveCampaignResult(CampaignResults campaignResults,Session session) {
			// TODO Auto-generated method stub
			int i= (int) session.save(campaignResults);
			return i;
			
		}
		
		
		@Override
		public List<CampaignResults> getCampaignResultByIndentId(int indentId){
			List<CampaignResults> campList=new ArrayList<>();
			Session session=sessionFactory.openSession();
			try {
				
				campList=session.createCriteria(CampaignResults.class)
						.add(Restrictions.eq("indentId", indentId)).list();
				
			}catch(Exception e) {
				e.printStackTrace();
			}finally {
				session.close();
			}
			
			return campList;
		}
		
		
		@Override
		public boolean checkCampaignResultByIndentIdMasterId(int indentId,int masterId,Session session){
			
			boolean flag=false;
			List<CampaignResults> campList=new ArrayList<>();
				
				campList=session.createCriteria(CampaignResults.class)
						.add(Restrictions.eq("indentId", indentId))
						.add(Restrictions.eq("Master_Id", masterId)).list();
				
				if(!campList.isEmpty())
					flag=true;
			
			return flag;
		}
		
		
}
