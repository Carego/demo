'use strict';

var app = angular.module('application');

app.controller('RelOsCntlr', ['$scope', '$http', function($scope, $http) {
	
	console.log("OsRptCntlr Started");
	
	
//	$scope.checkOrder='';
	$scope.branchList = [];
	$scope.branch = {};
	$scope.custList = [];
	$scope.cust = {};
	$scope.custGroup={};
	$scope.branchDBFlag = true;
	$scope.custDBFlag = true;
	$scope.custGroupDBFlag=true;
	$scope.lodingFlag = false;
	$scope.printXlsFlag = false;
	$scope.getActiveBrList = function() {
		console.log("getActiveBrList Entered");
		var response = $http.post($scope.projectName+'/getUserBrCustNBrList');
		response.success(function(data, status, headers, config){
			console.log(data.result);
			if (data.result === "success") {
				console.log("getActiveBrList result Success");
				$scope.branchList = data.branchList;
				$scope.branch = data.userBranch;
				$scope.custList = data.custList;
				$scope.allCustGroupList=data.allCustGroupList;
				console.log($scope.allCustGroupList);
			}else {
				$scope.alertToast("getActiveBrNCI: "+data.result);
				console.log("getActiveBrList result Error");
			}
		});
		response.error(function(data, status, headers, config){
			console.log("Error in response getActiveBrList: "+data);
		});
	};
	
	$scope.openBranchDB = function() {
		console.log("openBranchDB()");
		$scope.branchDBFlag = false;
		$('div#branchDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Branch",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.branchDBFlag = true;
			}
		});
		$('div#branchDB').dialog('open');
	};
	
	
/*	$scope.clickOrderWise=function(orderWise){
		console.log(orderWise);
		$scope.checkOrder=orderWise;
	} */
	
	$scope.saveBranch = function(branch){
		console.log("saveBranch()");
		$scope.branch = branch;
		$('div#branchDB').dialog('close');
		$scope.cust = {};
		$scope.custGroup={};
	};
	
	$scope.getCustListByBranch = function() {
		console.log("getCustByBranch");
		var res = $http.post($scope.projectName+'/getCustListByBranch', $scope.branch.branchId);
		res.success(function(data, status, headers, config) {
			if (data.result === "success") {
				console.log("getCustListByBranch result: "+data.result);
				$scope.custList = data.custList;
			} else {
				console.log("getCustListByBranch result: "+data.result);
			}
		});
		res.error(function(data, status, headers, config) {
			console.log("error in response getCustListByBranch: "+data);
		});
	};
	
	$scope.openCustDB = function() {
		console.log("openCustDB()");
		$scope.custDBFlag = false;
		$('div#custDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Customer",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.custDBFlag = false;
			}
		});
		$('div#custDB').dialog('open');
	};
	
	$scope.moreOpenCustDB=function(){
	  console.log("Open groupCustDB..");
	  $scope.custGroupDBFlag=false;
	  $('div#custGroupDB').dialog({
		  autoOpen: false,
		  modal:true,
		  resizable:false,
		  position:UDPos,
		  show:UDShow,
		  hide:UDHide,
		  title:"Customer Group",
		  draggable:true,
		  close:function(event,ui){
			  $(this).dialog('destroy');
			  $(this).hide();
			  $scope.custGroupDBFlag=false;
		  }
	  });
	  $('div#custGroupDB').dialog('open');
	};
	
	$scope.saveCust = function(cust){
		console.log("saveCust()");
		$scope.cust = cust;
		$scope.branch = {};
		$scope.custGroup={};

		$('div#custDB').dialog('close');
	};
	
	$scope.saveCustGroup=function(custGroup){
		console.log(custGroup);
		$scope.custGroup=custGroup;
		 $('div#custGroupDB').dialog('close');
	
		 $scope.branch = {};
		 $scope.cust = {};
	};
	
	$scope.clearAll = function() {
		console.log("clearAll()");
		$scope.cust = {};
		$scope.branch = {};
		$scope.custGroup = {};
	};
	
	$scope.submitOsRpt = function(osRptForm) {
		console.log("submitOsRpt()");
		  console.log($scope.custGroup);
		$scope.clear();
		if (osRptForm.$invalid) {
			console.log("invalidate: "+osRptForm.$invalid);
			if (osRptForm.branchName.$invalid) {
				$scope.alertToast("Please enter valid Branch");
			} else if (osRptForm.custName.$invalid){
				$scope.alertToast("Please enter valid Customer");
			} else if (osRptForm.upToDtName.$invalid) {
				$scope.alertToast("Please enter valid Date");
			}
		} else {
			console.log("invalidate: "+osRptForm.$invalid);
			var osRptService = {
					"branchId"		: $scope.branch.branchId,
					"custId"		: $scope.cust.custId,
					"upToDt"		: $scope.upToDt,
					"custGroupId"	: $scope.custGroup.groupId,
				//	"checkOrder"	: $scope.checkOrder
				};
			
			$('#submitId').attr("disabled", "disabled");
			$scope.printXlsFlag = false;
			$scope.lodingFlag = true;
			
			var res = $http.post($scope.projectName+'/getRelOs', osRptService);
			res.success(function(data, status, headers, config) {
				if (data.result === "success") {
					console.log("response of getOsRpt: "+data.result);
					$('#submitId').removeAttr("disabled");
					$('#printXlsId').removeAttr("disabled");
					$scope.printXlsFlag = true;
					$scope.lodingFlag = false;
					$scope.alertToast(data.result);
					console.log(JSON.stringify(data));
				} else {
					console.log("response of getOsRpt: "+data.result);
					$('#submitId').removeAttr("disabled");
					$scope.lodingFlag = false;
					$scope.alertToast("No Result Found");
				}
			});
			res.error(function(data, status, headers, config) {
				console.log("error in getOsRpt "+data);
				$('#submitId').removeAttr("disabled");
				$scope.lodingFlag = false;
				$scope.alertToast("Error");
			});
			
		}
	};
	
	$scope.printXls = function() {
		console.log("printXls()");
		var blob = new Blob([document.getElementById('exportable').innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
        saveAs(blob, "RelosR.xls");
	};
	
	$scope.clear = function() {
		console.log("clear()");
		$scope.filterTextbox1 = "";
		$scope.filterTextbox2 = "";
		$scope.custGroupFilterTextbox="";
		$scope.orderWise=false;
	};
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		$scope.getActiveBrList();
	}else if($scope.logoutStatus === true){
		$location.path("/");
	}else{
		console.log("****************");
	}
	
	console.log("OsRptCntlr Ended");
}]);