package com.mylogistics.mobileApi.controller.model;


public class OwnerKYCBankAccountDetails {

	private String ownBrkCode;
	
	private String panNo;
	
	private String accountNo;
	
	private String accountHolderName;
	 
	private String ifscCode;
	
	private String bankName;
	
	private String panImageUrl;
	
	//private String decImageUrl;
	
	private int empId;
	
	private String accountProofImageUrl;


	public String getOwnBrkCode() {
		return ownBrkCode;
	}

	public void setOwnBrkCode(String ownBrkCode) {
		this.ownBrkCode = ownBrkCode;
	}

	public String getPanNo() {
		return panNo;
	}

	public void setPanNo(String panNo) {
		this.panNo = panNo;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public String getAccountHolderName() {
		return accountHolderName;
	}

	public void setAccountHolderName(String accountHolderName) {
		this.accountHolderName = accountHolderName;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public String getPanImageUrl() {
		return panImageUrl;
	}

	public void setPanImageUrl(String panImageUrl) {
		this.panImageUrl = panImageUrl;
	}

	public String getAccountProofImageUrl() {
		return accountProofImageUrl;
	}

	public void setAccountProofImageUrl(String accountProofImageUrl) {
		this.accountProofImageUrl = accountProofImageUrl;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}
	
	
	
}
