'use strict';

var app = angular.module('application');

app.controller('ContractRightsCntlr',['$scope','$location','$http','$filter',
                                 function($scope,$location,$http,$filter){
	
	$scope.contractCodesDBFlag = true;
	$scope.contRightsDetails = true;
	$scope.contCodes = true;
	$scope.cr={};
	
	$scope.openContCodeDB = function(){
		$scope.contractCodesDBFlag = false;
    	$('div#contractCodesDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Contract Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		$('div#contractCodesDB').dialog('open');	
	}	
	
		$scope.saveCode = function(codes){
			$scope.contCode = codes;
			$('div#contractCodesDB').dialog("destroy");
			$scope.contractCodesDBFlag = true;
		}
		
		
		 $scope.contRightsView = function(contCode){
				console.log("After submit button");
					$scope.code= $("#contcode").val();
					if($scope.code===""){
						$scope.alertToast("Enter code");
					}else{
						$scope.contCodes = false;
						$scope.contRightsDetails =false;
						var response = $http.post($scope.projectName+'/contRightsDetails',contCode);
						response.success(function(data, status, headers, config) {
							if(data.result==="success"){
								$scope.cr = data.contractRights;			
							}else{
								console.log(data.result);
							}
						});
						response.error(function(data, status, headers, config) {
							$scope.errorToast(data.result);
						});
					}		
				} 
		
		 	$scope.editContRights=function(ViewRightsForm,cr){
		 		console.log("inside editContRights");
		 		var response = $http.post($scope.projectName+'/EditContRights',cr);
		 		response.success(function(data, status, headers, config){
		 			if(data.result==="success"){
		 				$scope.successToast(data.result);
		 				$scope.contRightsDetails  = true;
						$scope.contCodes = true;
						$scope.contCode="";
					}else{
						console.log(data);
					}
				});
		 		response.error(function(data, status, headers, config) {
						$scope.errorToast(data.result);
				}); 	
		 	}
		 
		$scope.getContCodeForRights = function(){
			console.log("Inside getContCodeForRights");
			var response = $http.post($scope.projectName+'/getContCodeForRights');
			response.success(function(data, status, headers, config){
				if(data.result==="success"){
					$scope.codeList = data.list;
				}else{
					console.log(data);
				}
			});
			response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
				});
			}
		
		
		if($scope.adminLogin === true || $scope.superAdminLogin === true){
			$scope.getContCodeForRights();
		 }else if($scope.logoutStatus === true){
			 $location.path("/");
		 }else{
			 console.log("****************");
		 }
	
}]);