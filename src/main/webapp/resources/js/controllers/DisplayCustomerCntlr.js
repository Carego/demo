'use strict';

var app = angular.module('application');

app.controller('DisplayCustomerCntlr',['$scope','$location','$http','$filter',
                                       function($scope,$location,$http,$filter){



	$scope.customerCodeDBFlag = true;
	$scope.availableTags = [];
	$scope.isViewNo=false;
	/*$scope.dlyCont=true;*/
	//$scope.selection=[];
	$scope.showCustomer = true;
	$scope.custByModal=false;
	$scope.custByAuto=false;

	//$scope.divForAutoCode = true;

	$scope.customer = {};
	$scope.show = "true";
	$scope.selection=[];
	$scope.BranchCodeDBFlag=true;
	$scope.CrdCbFlag=true;
	$scope.custrep = {};
	$scope.custDirectorDBFlag=true;
	$scope.custMktHeadDBFlag=true;
	$scope.custCorpExcDBFlag=true;
	$scope.custBrPersonDBFlag=true;
	$scope.custRepFlag=false;
	$scope.billFlag=true;
	$scope.viewCustDetailsFlag=true;

	$scope.OpenCustCodeDB = function(){
		$scope.customerCodeDBFlag = false;
		$('div#customerCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#customerCodeDB').dialog('open');
	}

	$scope.enableModalTextBox = function(){
		$('#custByMId').removeAttr("disabled");
		$('#custByAId').attr("disabled","disabled");
		$('#ok').attr("disabled","disabled");
		$scope.custByA="";
	}

	$scope.enableAutoTextBox = function(){
		$('#custByAId').removeAttr("disabled");
		$('#ok').removeAttr("disabled");
		$('#custByMId').attr("disabled","disabled");
		$scope.custByM="";
	}

	$scope.saveCustomerCode = function(custCodes){
		$scope.custByM=custCodes;
		console.log("custByM--------"+$scope.custByM);
		$('div#customerCodeDB').dialog('close');
		$scope.showCustomer = false;
		//$scope.divForAutoCode = true;
		$scope.isViewNo=true;
		var response = $http.post($scope.projectName+'/submitcustomer',$scope.custByM);
		response.success(function(data, status, headers, config) {
			if(data.result==="success"){
				$scope.customer = data.customer;
				if($scope.customer.custIsDailyContAllow === "yes"){
					$scope.checkcustIsDailyContAllow = true;
				}else{
					$scope.checkcustIsDailyContAllow = false;
				}
				$scope.custRepList = data.customerRepList; 
				$scope.getBranchData();
				$scope.alertToast(data.result);
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
		});
	}


	$scope.getCustCodeList=function(){
		console.log("Enter into getCustCodeList function");
		$( "#custByAId" ).autocomplete({
			source: $scope.availableTags
		});
	}


	$scope.getCustomerList = function(){
		$scope.code = $( "#custByAId" ).val();

		if($scope.code===""){
			$scope.alertToast("Please enter code");	
		}else{
			$scope.saveCustomerCode = false;
			$scope.isViewNo=true;	
			var response = $http.post($scope.projectName+'/submitcustomer',$scope.code);
			response.success(function(data, status, headers, config) {
				$scope.customer = data.customer;
				if($scope.customer.custIsDailyContAllow === "yes"){
					$scope.checkcustIsDailyContAllow = true;
				}else{
					$scope.checkcustIsDailyContAllow = false;
				}
				$scope.custRepList = data.customerRepList;
				$scope.showCustomer = false;
				$scope.getBranchData();
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
		}

	}

	$scope.getAllCustCode = function(){
		console.log("getAllCustCode------>");
		var response = $http.get($scope.projectName+'/getAllCustCode');
		response.success(function(data, status, headers, config) {
			if(data.result==="success"){
				$scope.custCodeList = data.custCodeList;
				$scope.availableTags = data.custCodeList;
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}

	$scope.getCustIsViewNo = function(){
		console.log("getCustIsViewNo------>");
		var response = $http.post($scope.projectName+'/getCustIsViewNo');
		response.success(function(data, status, headers, config){
			if(data.result==="success"){
				$scope.successToast(data.result);
				$scope.custList = data.list;
				$scope.isViewNo=false;
				for(var i=0;i<$scope.custList.length;i++){
					$scope.custList[i].creationTS =  $filter('date')($scope.custList[i].creationTS, 'MM/dd/yyyy hh:mm:ss');
				}
			}else{
				$scope.isViewNo=true;
				$('#bkToList').attr("disabled","disabled");
				$scope.alertToast(data.result);
			}

			$scope.getAllCustCode();
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
		});
	}

	$scope.toggleSelection = function toggleSelection(custCode) {
		console.log("inside toggleSelection check = ")
		var idx = $scope.selection.indexOf(custCode);

		// is currently selected
		if (idx > -1) {
			$scope.selection.splice(idx, 1);
		}
		// is newly selected
		else {
			$scope.selection.push(custCode);
		}
	}	  

	$scope.verifyCustomer = function(){
		if(!$scope.selection.length){
			$scope.alertToast("You have not selected anything");
		}else{
			var response = $http.post($scope.projectName+'/updateIsVerifyCustomer',$scope.selection.toString());
			response.success(function(data, status, headers, config){
				$scope.getCustIsViewNo();
				console.log("---------->message==>"+data.result);
				$scope.successToast(data.result);
			});	 
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
		}
	}

	$scope.backToList = function(){
		$scope.saveCustomerCode = true;
		$scope.isViewNo=false;
		$scope.custByM="";
		$scope.custByA="";
		$('#custByAId').attr("disabled","disabled");
		$('#custByMId').attr("disabled","disabled");
		$scope.showCustomer=true;
	}

	$scope.editCustomerSubmit = function(customer){
		var response = $http.post($scope.projectName+'/saveeditcustomer', customer);
		response.success(function(data, status, headers, config) {

			console.log(data.result);
			$scope.customer="";
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}

	$scope.getListOfEmployee = function(){
		console.log(" entered into getListOfEmployee------>");
		var response = $http.post($scope.projectName+'/getListOfEmpforCustomer');
		response.success(function(data, status, headers, config){
			if(data.result==="success"){
				$scope.employeeList = data.list;
				$scope.successToast(data.result);
				$scope.getDesignation();
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}


	$scope.OpenDirectorDB = function(){
		$scope.custDirectorDBFlag=false;
		$('div#custDirectorDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Director",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#custDirectorDB').dialog('open');
	}

	$scope.getDesignation = function(){
		var response = $http.get($scope.projectName+'/getDesignationForCr');
		response.success(function(data, status, headers, config) {
			$scope.availableTags = data;
			console.log("******************");
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}



	$scope.saveCustDirector =  function(empCustDirector){
		console.log("enter into saveCustDirector----->");
		$scope.customer.custDirector = empCustDirector.empCode;
		$('div#custDirectorDB').dialog('close');
		$scope.custDirectorDBFlag = true;	
	}


	$scope.OpenMarketingDB = function(){
		$scope.custMktHeadDBFlag=false;
		$('div#custMktHeadDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Marketing Head",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#custMktHeadDB').dialog('open');
	}


	$scope.saveCustMktHead =  function(empCustMktHead){
		console.log("enter into saveCustMktHead----->");
		$scope.customer.custMktHead = empCustMktHead.empCode;
		$('div#custMktHeadDB').dialog('close');
		$scope.custMktHeadDBFlag = true;	
	} 


	$scope.OpenExecutiveDB = function(){
		$scope.custCorpExcDBFlag=false;
		$('div#custCorpExcDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Corporate Executive",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#custCorpExcDB').dialog('open');
	} 


	$scope.saveCustCorpExc =  function(empCustCorpExc){
		console.log("enter into saveCustCorpExc----->");
		$scope.customer.custCorpExc = empCustCorpExc.empCode;
		$('div#custCorpExcDB').dialog('close');
		$scope.custCorpExcDBFlag = true;	
	} 


	$scope.OpenBranchPersonDB = function(){
		$scope.custBrPersonDBFlag=false;
		$('div#custBrPersonDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Branch Head",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#custBrPersonDB').dialog('open');
	}

	$scope.saveCustBrPerson =  function(empCustBrPerson){
		console.log("enter into saveCustBrPerson----->");
		$scope.customer.custBrPerson = empCustBrPerson.empCode;
		$('div#custBrPersonDB').dialog('close');
		$scope.custBrPersonDBFlag = true;	
	}


	$scope.OpenCRDB = function(){
		$scope.CrdCbFlag=false;
		$('div#crdCb').dialog({
			autoOpen: false,
			modal:true,
			title: "Customer Representative Details",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#crdCb').dialog('open');
	}


	$scope.getCustDesigList = function(custrep){
		$( "#crDesignation" ).autocomplete({
			source: $scope.availableTags
		});
	} 


	$scope.nextAddCR = function(AddCustomerRepForm,custrep){
		console.log("enter into next function AddCustomerRepForm.$invalid--->"+AddCustomerRepForm.$invalid);
		$scope.custRepFlag=true;
		$('div#crdCb').dialog('close');
		if(AddCustomerRepForm.$invalid){
			if(AddCustomerRepForm.crFirstName.$invalid){
				$scope.alertToast("please enter customer representative first name..");
			}else if(AddCustomerRepForm.crLastName.$invalid){
				$scope.alertToast("please enter customer representative last name..");
			}else if(AddCustomerRepForm.crDesignation.$invalid){
				$scope.alertToast("please enter customer representative designation address..");
			}else if(AddCustomerRepForm.crMobileNo.$invalid){
				$scope.alertToast("Please enter phone number of 4-15 digits..");
			}else if(AddCustomerRepForm.crEmailId.$invalid){
				$scope.alertToast("Please enter correct format of email id..");
			}
		}else{
			$scope.alertToast("success");
			console.log("custrep----->"+custrep)

			/*var response = $http.post($scope.projectName+'/CustomerRepresentative', custrep);
				response.success(function(data, status, headers, config) {
					if(data.result === "success"){
					console.log("data.result = "+data.result);
					$location.path("/operator/"+data.targetPage);
					$scope.successToast(data.result);
					$window.location.href = "/MyLogistics/"+data.targetPage;
					}else{
				    	console.log(data);
				    }
				});
				response.error(function(data, status, headers, config) {
					console.log("error in CustomerRepCntlr");
					$scope.errorToast(data);
				});*/
		}
	}	


	$scope.updateCustomer = function(CustomerForm,customer){
		console.log("enter into Submit function CustomerForm.$invalid--->"+CustomerForm.$invalid);

		if(CustomerForm.$invalid){
			if(CustomerForm.custName.$invalid){
				$scope.alertToast("please enter customer name..");
			}else if(CustomerForm.branchCode.$invalid){
				$scope.alertToast("please enter branch Code..");
			}else if(CustomerForm.custAdd.$invalid){
				$scope.alertToast("please enter customer address..");
			}else if(CustomerForm.custCity.$invalid){
				$scope.alertToast("please enter customer city..");
			}else if(CustomerForm.custState.$invalid){
				$scope.alertToast("please enter customer state..");
			}else if(CustomerForm.custPin.$invalid){
				$scope.alertToast("Please enter PIN number of 6 digits..");
			}else if(CustomerForm.custTdsCircle.$invalid){
				$scope.alertToast("please enter customer tds circle..");
			}else if(CustomerForm.custTdsCity.$invalid){
				$scope.alertToast("please enter customer tds city..");
			}else if(CustomerForm.custPanNo.$invalid){
				$scope.alertToast("Please enter PAN number of 10 digits..");
			}else if(CustomerForm.custTinNo.$invalid){
				$scope.alertToast("Please enter TIN number of 8-12 digits..");
			}else if(CustomerForm.custStatus.$invalid){
				$scope.alertToast("Please enter customer status..");
			}else if(CustomerForm.custCRPeriod.$invalid){
				$scope.alertToast("Please enter correct CRPeriod..");
			}else if(CustomerForm.custDirector.$invalid){
				$scope.alertToast("Please enter director.");
			}else if(CustomerForm.custMarketingHead.$invalid){
				$scope.alertToast("Please enter marketing head.");
			}else if(CustomerForm.custCorpExecutive.$invalid){
				$scope.alertToast("Please enter executive head.");
			}else if(CustomerForm.custBranchPerson.$invalid){
				$scope.alertToast("Please enter branch head.");
			}else if(CustomerForm.crSrvTaxBy.$invalid){
				$scope.alertToast("Please enter service tax.");
			}else if(CustomerForm.crBase.$invalid){
				$scope.alertToast("Please enter bill cycle.");
			}
		}else{

			/*if(customer.custSrvTaxBy === "Customer"){
	    		customer.custSrvTaxBy = 'C';
	    	}else if(customer.custSrvTaxBy === "SECL"){
	    		customer.custSrvTaxBy = 'S';

	    	}else*/
			/*	 console.log($scope.custRepFlag);
	    	 console.log($scope.customer.custBillCycle);

	    	 if($scope.custRepFlag===false){
	    		if(custrep.custRefNo===null || custrep.custRefNo==="" || angular.isUndefined(custrep.custRefNo) || custrep.crName===null || custrep.crName==="" || angular.isUndefined(custrep.crName)
	    			|| custrep.crDesignation===null || custrep.crDesignation ==="" || angular.isUndefined(custrep.crDesignation) || custrep.crMobileNo===null || custrep.crMobileNo==="" || angular.isUndefined(custrep.crMobileNo)	
	    			|| custrep.crEmailId ===null || custrep.crEmailId==="" || angular.isUndefined(custrep.crEmailId)){
	    			 $scope.alertToast("please Enter Customer Representative");
	    		}
	    	}else */
			if($scope.billFlag===false){
				$scope.alertToast("Enter correct bill value");
			}
			else{
				/*console.log($scope.custrep.crFirstName+$scope.custrep.crLastName);
	    			$scope.custrep.crName = $scope.custrep.crFirstName+" " +$scope.custrep.crLastName;
	    	 var custCustRep = {
	    			 "customer"    				: $scope.customer,
	    			 "customerRepresentative"   : $scope.custrep	 
	    	 };*/

				$scope.viewCustDetailsFlag=false;
				$('div#viewCustDetailsDB').dialog({
					autoOpen: false,
					modal:true,
					title: "Customer Info",
					show: UDShow,
					hide: UDHide,
					position: UDPos,
					resizable: false,
					draggable: true,
					close: function(event, ui) { 
						$(this).dialog('destroy') ;
						$(this).hide();
					}
				});

				$('div#viewCustDetailsDB').dialog('open');
			}
		}
	}


	$scope.saveCustomer = function(customer){
		console.log("Entered into saveCustomer function----> ");

		var response = $http.post($scope.projectName+'/updateCustomer',customer);
		response.success(function(data, status, headers, config) {
			if(data.result === "success"){
				$scope.showCustomer = true;
				$('div#viewCustDetailsDB').dialog('close');
				console.log(data.result);
				$scope.successToast(data.result);
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}


	$scope.closeViewCustDetailsDB = function(){
		$('div#viewCustDetailsDB').dialog('close');
	}


	$scope.OpenBranchCodeDB = function(){
		$scope.BranchCodeDBFlag=false;
		$('div#branchCodeDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Branch Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#branchCodeDB').dialog('open');
	}


	$scope.getBranchData = function(){
		console.log("getBranchData------>");
		var response = $http.post($scope.projectName+'/getBranchDataForCustomer');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.branchList = data.list;
				$scope.getListOfEmployee();
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers,config) {
			$scope.errorToast(data);
		});
	}


	$scope.saveBranchCode = function(branch){
		$scope.customer.branchCode = branch.branchCode;
		$('div#branchCodeDB').dialog('close');
		$scope.BranchCodeDBFlag=true;
	}


	$scope.savecustIsDailyContAllow = function(){
		console.log("enter into $scope.savecustIsDailyContAllow function");
		console.log("$scope.checkcustIsDailyContAllow = "+$scope.checkcustIsDailyContAllow);
		if($scope.checkcustIsDailyContAllow === true){
			$scope.customer.custIsDailyContAllow = "yes";
		}else{
			$scope.customer.custIsDailyContAllow = "no";
		}
	}
	
	if($scope.adminLogin === true || $scope.superAdminLogin === true){
		$scope.getCustIsViewNo();
	}else if($scope.logoutStatus === true){
			 $location.path("/");
	}else{
			 console.log("****************");
	}	

}]);
