package com.mylogistics.model;

import java.sql.Date;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "penaltybonusdetention")
public class PenaltyBonusDetention {
	
	@Id
	@Column(name="pbdId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int pbdId;
	
	@Column(name="pbdContCode")
	private String pbdContCode;
	
	@Column(name="pbdFromStn")
	private String pbdFromStn;
	
	@Column(name="pbdToStn")
	private String pbdToStn;
	
	@Column(name="pbdStartDt")
	private Date pbdStartDt;
	
	@Column(name="pbdEndDt")
	private Date pbdEndDt;
	
	@Column(name="pbdFromDay")
	private int pbdFromDay;
	
	@Column(name="pbdToDay")
	private int pbdToDay;
	
	@Column(name="pbdVehicleType")
	private String pbdVehicleType;
	
	@Column(name="pbdPenBonDet")
	private String pbdPenBonDet;
	
	@Column(name="pbdHourNDay")
	private String pbdHourNDay;
	
	@Column(name="pbdAmt")
	private double pbdAmt;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;
	
	@Column(name="bCode")
	private String bCode;

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}


	public int getPbdId() {
		return pbdId;
	}

	public void setPbdId(int pbdId) {
		this.pbdId = pbdId;
	}

	public String getPbdContCode() {
		return pbdContCode;
	}

	public void setPbdContCode(String pbdContCode) {
		this.pbdContCode = pbdContCode;
	}

	public String getPbdFromStn() {
		return pbdFromStn;
	}

	public void setPbdFromStn(String pbdFromStn) {
		this.pbdFromStn = pbdFromStn;
	}

	public String getPbdToStn() {
		return pbdToStn;
	}

	public void setPbdToStn(String pbdToStn) {
		this.pbdToStn = pbdToStn;
	}

	public Date getPbdStartDt() {
		return pbdStartDt;
	}

	public void setPbdStartDt(Date pbdStartDt) {
		this.pbdStartDt = pbdStartDt;
	}

	public Date getPbdEndDt() {
		return pbdEndDt;
	}

	public void setPbdEndDt(Date pbdEndDt) {
		this.pbdEndDt = pbdEndDt;
	}

	public int getPbdFromDay() {
		return pbdFromDay;
	}

	public void setPbdFromDay(int pbdFromDay) {
		this.pbdFromDay = pbdFromDay;
	}

	public int getPbdToDay() {
		return pbdToDay;
	}

	public void setPbdToDay(int pbdToDay) {
		this.pbdToDay = pbdToDay;
	}

	public String getPbdVehicleType() {
		return pbdVehicleType;
	}

	public void setPbdVehicleType(String pbdVehicleType) {
		this.pbdVehicleType = pbdVehicleType;
	}

	public String getPbdPenBonDet() {
		return pbdPenBonDet;
	}

	public void setPbdPenBonDet(String pbdPenBonDet) {
		this.pbdPenBonDet = pbdPenBonDet;
	}

	public String getPbdHourNDay() {
		return pbdHourNDay;
	}

	public void setPbdHourNDay(String pbdHourNDay) {
		this.pbdHourNDay = pbdHourNDay;
	}

	public double getPbdAmt() {
		return pbdAmt;
	}

	public void setPbdAmt(double pbdAmt) {
		this.pbdAmt = pbdAmt;
	}


}
