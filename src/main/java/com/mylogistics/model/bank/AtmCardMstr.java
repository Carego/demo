package com.mylogistics.model.bank;

import java.sql.Date;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.mylogistics.model.Branch;
import com.mylogistics.model.Employee;

@Entity
@Table(name = "atmcardmstr")
public class AtmCardMstr {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="atmCardId")
	private int atmCardId;
	
	@Column(name="atmCardNo" , unique=true)
	private String atmCardNo;
	
	@Column(name="atmCardExpireDt")
	private Date atmCardExpireDt;
	
	/*@ManyToOne
	@LazyCollection(LazyCollectionOption.TRUE)
	@JoinColumn(name = "empId")
	private Employee employee;*/
	@Column(name="empId")
	private int empId;
	
	/*@ManyToOne
	@LazyCollection(LazyCollectionOption.TRUE)
	@JoinColumn(name = "branchId")
	private Branch branch;*/
	@Column(name="branchId")
	private int branchId;
	
	@ManyToOne
	@LazyCollection(LazyCollectionOption.TRUE)
	@JoinColumn(name = "bnkId")
	private BankMstr bankMstr;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="active", columnDefinition="boolean default true")
	private boolean atmActive;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;
	
	@Transient
	private String branchName;
	
	@Transient
	private String empName;

	public int getAtmCardId() {
		return atmCardId;
	}

	public void setAtmCardId(int atmCardId) {
		this.atmCardId = atmCardId;
	}

	public String getAtmCardNo() {
		return atmCardNo;
	}

	public void setAtmCardNo(String atmCardNo) {
		this.atmCardNo = atmCardNo;
	}

	/*public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}*/

	public BankMstr getBankMstr() {
		return bankMstr;
	}

	public void setBankMstr(BankMstr bankMstr) {
		this.bankMstr = bankMstr;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public Date getAtmCardExpireDt() {
		return atmCardExpireDt;
	}

	public void setAtmCardExpireDt(Date atmCardExpireDt) {
		this.atmCardExpireDt = atmCardExpireDt;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public boolean isAtmActive() {
		return atmActive;
	}

	public void setAtmActive(boolean atmActive) {
		this.atmActive = atmActive;
	}

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public int getBranchId() {
		return branchId;
	}

	public void setBranchId(int branchId) {
		this.branchId = branchId;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}
	
	
}
