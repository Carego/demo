package com.mylogistics.model.app;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="apploginotp")
public class AppLoginOTP {
	
	@Id
	@Column(name="aoId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int aoId;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;
	
	@Column(name="otp",length=6)
	private String otp;
	
	@Column(name="phoneNo",length=20,unique = true,nullable=false)
	private String phoneNo;

	public int getAoId() {
		return aoId;
	}

	public void setAoId(int aoId) {
		this.aoId = aoId;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	
	

}
