package com.mylogistics.mobileApi.controller.model;

public class VehicleDataFrmAPI {
	
	private int vId;
	private String id;//"d1d3e622-0fec-4137-bee3-4763f9a244d8"
	private int env;//1
	private String request_timestamp;//"2019-02-02 15:05:27:872 +05:30"
	private String	response_timestamp;//":"2019-02-02 15:05:34:845 +05:30",
	private int	transaction_status;//":1,
	
	 //result":{"
	private String rc_body_type_desc;//":"TRUCK (CLOSED BODY)",
	private String	rc_chasi_no;//":"MAT448202F5F08431",
	private String 	rc_color;//":"NA",
	private String	rc_cubic_cap;//":"0.00",
	private	String  rc_eng_no;//":"51F84234689",
	private String	rc_f_name;//":"NA",
	private String	rc_financer;//":"KOTAK MAHINDRA BANK LTD",
	private String 	rc_fit_upto;//":"17-Aug-2019",
	private String	rc_fuel_desc;//":"DIESEL",
	private String	rc_gvw;//":"25000",
	private String	rc_insurance_comp;//":"Universal Sompo General Insurance  Co. Ltd.",
	private String	rc_insurance_policy_no;//":"2315/58660241/00/000",
	private String	rc_insurance_upto;//":"05-Jul-2019",
	private String	rc_maker_desc;//":"TATA MOTORS LTD",
	private	String  rc_maker_model;//":"TATA MOTORS LTD, LPT 2518",
	private String	rc_manu_month_yr;//":"6/2015",
	private String	rc_mobile_no;//":"9798965423",
	private String	rc_no_cyl;//":"6",
	private String	rc_norms_desc;//":"BHARAT STAGE II",
	private	String  rc_np_issued_by;//":"Secretary RTA, GURGAON",
	private String	rc_np_no;//":"NP/HR/55/082018/25845",
	private String	rc_np_upto;//":"19-Aug-2019",
	private String	rc_owner_name;//":"M/S CARE GO LOGISTICS PVT LTD",
	private String	rc_owner_sr;//":"1",
	private String	rc_permanent_address;//":"A 115 PH 1 SUSHANT LOK, GURGAON, GURGAON, -122001",
	private String	rc_present_address;//":"A 115 PH 1 SUSHANT LOK, GURGAON, GURGAON, -122001",
	private String	rc_registered_at;//":"RTA, GURGAON, Haryana",
	private	String  rc_regn_dt;//":"20-Aug-2015",
	private String	rc_regn_no;//":"HR55V6542",
	private	String  rc_seat_cap;//":"3",
	private String	rc_sleeper_cap;//":"0",
	private String	rc_stand_cap;//":"",
	private String	rc_status_as_on;//":"02-Feb-2019",
	private String	rc_tax_upto;//":"31-Mar-2019",
	private String	rc_unld_wt;//":"9000",
	private String	rc_vch_catg;//":"HGV",
	private String	rc_vh_class_desc;//":"Goods Carrier",
	private String	rc_wheelbase;//":"0",
	private String	stautsMessage;//":"OK"},
	private String	response_msg;//":"Valid Authentication",
	private String	response_code;//":"101"}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public int getEnv() {
		return env;
	}
	public void setEnv(int env) {
		this.env = env;
	}
	public String getRequest_timestamp() {
		return request_timestamp;
	}
	public void setRequest_timestamp(String request_timestamp) {
		this.request_timestamp = request_timestamp;
	}
	public String getResponse_timestamp() {
		return response_timestamp;
	}
	public void setResponse_timestamp(String response_timestamp) {
		this.response_timestamp = response_timestamp;
	}
	public int getTransaction_status() {
		return transaction_status;
	}
	public void setTransaction_status(int transaction_status) {
		this.transaction_status = transaction_status;
	}
	public String getRc_body_type_desc() {
		return rc_body_type_desc;
	}
	public void setRc_body_type_desc(String rc_body_type_desc) {
		this.rc_body_type_desc = rc_body_type_desc;
	}
	public String getRc_chasi_no() {
		return rc_chasi_no;
	}
	public void setRc_chasi_no(String rc_chasi_no) {
		this.rc_chasi_no = rc_chasi_no;
	}
	public String getRc_color() {
		return rc_color;
	}
	public void setRc_color(String rc_color) {
		this.rc_color = rc_color;
	}
	public String getRc_cubic_cap() {
		return rc_cubic_cap;
	}
	public void setRc_cubic_cap(String rc_cubic_cap) {
		this.rc_cubic_cap = rc_cubic_cap;
	}
	public String getRc_eng_no() {
		return rc_eng_no;
	}
	public void setRc_eng_no(String rc_eng_no) {
		this.rc_eng_no = rc_eng_no;
	}
	public String getRc_f_name() {
		return rc_f_name;
	}
	public void setRc_f_name(String rc_f_name) {
		this.rc_f_name = rc_f_name;
	}
	public String getRc_financer() {
		return rc_financer;
	}
	public void setRc_financer(String rc_financer) {
		this.rc_financer = rc_financer;
	}
	public String getRc_fit_upto() {
		return rc_fit_upto;
	}
	public void setRc_fit_upto(String rc_fit_upto) {
		this.rc_fit_upto = rc_fit_upto;
	}
	public String getRc_fuel_desc() {
		return rc_fuel_desc;
	}
	public void setRc_fuel_desc(String rc_fuel_desc) {
		this.rc_fuel_desc = rc_fuel_desc;
	}
	public String getRc_gvw() {
		return rc_gvw;
	}
	public void setRc_gvw(String rc_gvw) {
		this.rc_gvw = rc_gvw;
	}
	public String getRc_insurance_comp() {
		return rc_insurance_comp;
	}
	public void setRc_insurance_comp(String rc_insurance_comp) {
		this.rc_insurance_comp = rc_insurance_comp;
	}
	public String getRc_insurance_policy_no() {
		return rc_insurance_policy_no;
	}
	public void setRc_insurance_policy_no(String rc_insurance_policy_no) {
		this.rc_insurance_policy_no = rc_insurance_policy_no;
	}
	public String getRc_insurance_upto() {
		return rc_insurance_upto;
	}
	public void setRc_insurance_upto(String rc_insurance_upto) {
		this.rc_insurance_upto = rc_insurance_upto;
	}
	public String getRc_maker_desc() {
		return rc_maker_desc;
	}
	public void setRc_maker_desc(String rc_maker_desc) {
		this.rc_maker_desc = rc_maker_desc;
	}
	public String getRc_maker_model() {
		return rc_maker_model;
	}
	public void setRc_maker_model(String rc_maker_model) {
		this.rc_maker_model = rc_maker_model;
	}
	public String getRc_manu_month_yr() {
		return rc_manu_month_yr;
	}
	public void setRc_manu_month_yr(String rc_manu_month_yr) {
		this.rc_manu_month_yr = rc_manu_month_yr;
	}
	public String getRc_mobile_no() {
		return rc_mobile_no;
	}
	public void setRc_mobile_no(String rc_mobile_no) {
		this.rc_mobile_no = rc_mobile_no;
	}
	public String getRc_no_cyl() {
		return rc_no_cyl;
	}
	public void setRc_no_cyl(String rc_no_cyl) {
		this.rc_no_cyl = rc_no_cyl;
	}
	public String getRc_norms_desc() {
		return rc_norms_desc;
	}
	public void setRc_norms_desc(String rc_norms_desc) {
		this.rc_norms_desc = rc_norms_desc;
	}
	public String getRc_np_issued_by() {
		return rc_np_issued_by;
	}
	public void setRc_np_issued_by(String rc_np_issued_by) {
		this.rc_np_issued_by = rc_np_issued_by;
	}
	public String getRc_np_no() {
		return rc_np_no;
	}
	public void setRc_np_no(String rc_np_no) {
		this.rc_np_no = rc_np_no;
	}
	public String getRc_np_upto() {
		return rc_np_upto;
	}
	public void setRc_np_upto(String rc_np_upto) {
		this.rc_np_upto = rc_np_upto;
	}
	public String getRc_owner_name() {
		return rc_owner_name;
	}
	public void setRc_owner_name(String rc_owner_name) {
		this.rc_owner_name = rc_owner_name;
	}
	public String getRc_owner_sr() {
		return rc_owner_sr;
	}
	public void setRc_owner_sr(String rc_owner_sr) {
		this.rc_owner_sr = rc_owner_sr;
	}
	public String getRc_permanent_address() {
		return rc_permanent_address;
	}
	public void setRc_permanent_address(String rc_permanent_address) {
		this.rc_permanent_address = rc_permanent_address;
	}
	public String getRc_present_address() {
		return rc_present_address;
	}
	public void setRc_present_address(String rc_present_address) {
		this.rc_present_address = rc_present_address;
	}
	public String getRc_registered_at() {
		return rc_registered_at;
	}
	public void setRc_registered_at(String rc_registered_at) {
		this.rc_registered_at = rc_registered_at;
	}
	public String getRc_regn_dt() {
		return rc_regn_dt;
	}
	public void setRc_regn_dt(String rc_regn_dt) {
		this.rc_regn_dt = rc_regn_dt;
	}
	public String getRc_regn_no() {
		return rc_regn_no;
	}
	public void setRc_regn_no(String rc_regn_no) {
		this.rc_regn_no = rc_regn_no;
	}
	public String getRc_seat_cap() {
		return rc_seat_cap;
	}
	public void setRc_seat_cap(String rc_seat_cap) {
		this.rc_seat_cap = rc_seat_cap;
	}
	public String getRc_sleeper_cap() {
		return rc_sleeper_cap;
	}
	public void setRc_sleeper_cap(String rc_sleeper_cap) {
		this.rc_sleeper_cap = rc_sleeper_cap;
	}
	public String getRc_stand_cap() {
		return rc_stand_cap;
	}
	public void setRc_stand_cap(String rc_stand_cap) {
		this.rc_stand_cap = rc_stand_cap;
	}
	public String getRc_status_as_on() {
		return rc_status_as_on;
	}
	public void setRc_status_as_on(String rc_status_as_on) {
		this.rc_status_as_on = rc_status_as_on;
	}
	public String getRc_tax_upto() {
		return rc_tax_upto;
	}
	public void setRc_tax_upto(String rc_tax_upto) {
		this.rc_tax_upto = rc_tax_upto;
	}
	public String getRc_unld_wt() {
		return rc_unld_wt;
	}
	public void setRc_unld_wt(String rc_unld_wt) {
		this.rc_unld_wt = rc_unld_wt;
	}
	public String getRc_vch_catg() {
		return rc_vch_catg;
	}
	public void setRc_vch_catg(String rc_vch_catg) {
		this.rc_vch_catg = rc_vch_catg;
	}
	public String getRc_vh_class_desc() {
		return rc_vh_class_desc;
	}
	public void setRc_vh_class_desc(String rc_vh_class_desc) {
		this.rc_vh_class_desc = rc_vh_class_desc;
	}
	public String getRc_wheelbase() {
		return rc_wheelbase;
	}
	public void setRc_wheelbase(String rc_wheelbase) {
		this.rc_wheelbase = rc_wheelbase;
	}
	public String getStautsMessage() {
		return stautsMessage;
	}
	public void setStautsMessage(String stautsMessage) {
		this.stautsMessage = stautsMessage;
	}
	public String getResponse_msg() {
		return response_msg;
	}
	public void setResponse_msg(String response_msg) {
		this.response_msg = response_msg;
	}
	public String getResponse_code() {
		return response_code;
	}
	public void setResponse_code(String response_code) {
		this.response_code = response_code;
	}
	public int getvId() {
		return vId;
	}
	public void setvId(int vId) {
		this.vId = vId;
	}

	

}
