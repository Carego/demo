<div ng-show="operatorLogin || superAdminLogin">
	<div class="noprint">
		<form name="closeTempForm" ng-submit="closeLhpvTemp(closeTempForm)" class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="branchNameId" name ="branchName" ng-model="branch.branchName" ng-click="openBrhDB()" readonly ng-required="true" >
		       			<label for="code">Branch Name</label>	
		       		</div>

		       		<div class="col s4 input-field">
		       				<input class="validate" type ="date" id="lhpvDtId" name ="lhpvDtName" ng-model="lhpvDate" ng-required="true">
		       			<label for="code">Lhpv Date</label>	
		       		</div>
		       		
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="submit" value="Submit" id="saveId">
		       		</div>
		    </div> 
		  </form>
    </div>
    
      <div id ="brhId" ng-hide="brhFlag">
		 <input type="text" name="filterBrhbox" ng-model="filterBrhbox" placeholder="Search...">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	 <!--  <th> Branch Code </th> -->
 	  	  	  
 	  	  	  <th> Branch Name </th>
 	  	  	  
 	  	  	  <th> Branch FaCode</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="branch in brhList | filter:filterBrhbox">
		 	  <td><input type="radio"  name="branchName"  class="branchCls"  value="{{ branch }}" ng-model="brCode" ng-click="saveBranch(branch)"></td>
              <!-- <td>{{branch.branchCode}}</td> -->
              <td>{{branch.branchName}}</td>
              <td>{{branch.branchFaCode}}</td>
          </tr>
      </table>
	</div>
	
</div>    