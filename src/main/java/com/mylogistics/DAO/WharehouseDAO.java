package com.mylogistics.DAO;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mylogistics.model.Wharehouse;

@Repository
public interface WharehouseDAO {

	public List<Wharehouse> getWharehouseList();
}
