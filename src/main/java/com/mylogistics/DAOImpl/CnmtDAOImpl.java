package com.mylogistics.DAOImpl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.sql.Blob;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.sql.rowset.serial.SerialBlob;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.apache.xmlbeans.impl.common.IOUtil;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.AliasToBeanResultTransformer;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.itextpdf.text.pdf.PdfStructTreeController.returnType;
import com.mylogistics.DAO.CnmtDAO;
import com.mylogistics.constants.ArrivalReportCNTS;
import com.mylogistics.constants.BillCNTS;
import com.mylogistics.constants.BillDetailCNTS;
import com.mylogistics.constants.BranchCNTS;
import com.mylogistics.constants.BranchMUStatsCNTS;
import com.mylogistics.constants.BranchSStatsCNTS;
import com.mylogistics.constants.BranchStockLeafDetCNTS;
import com.mylogistics.constants.ChallanCNTS;
import com.mylogistics.constants.CnmtCNTS;
import com.mylogistics.constants.CnmtImageCNTS;
import com.mylogistics.constants.Cnmt_ChallanCNTS;
import com.mylogistics.constants.CustomerCNTS;
import com.mylogistics.constants.LhpvAdvCNTS;
import com.mylogistics.constants.LhpvBalCNTS;
import com.mylogistics.constants.NarrationCNTS;
import com.mylogistics.constants.StateCNTS;
import com.mylogistics.constants.StationCNTS;
import com.mylogistics.constants.VehicleVendorMstrCNTS;
import com.mylogistics.model.ArrivalReport;
import com.mylogistics.model.Bill;
import com.mylogistics.model.BillDetail;
import com.mylogistics.model.Branch;
import com.mylogistics.model.BranchMUStats;
import com.mylogistics.model.BranchMUStatsBk;
import com.mylogistics.model.BranchSStats;
import com.mylogistics.model.BranchStockLeafDet;
import com.mylogistics.model.Broker;
import com.mylogistics.model.Challan;
import com.mylogistics.model.ChallanDetail;
import com.mylogistics.model.Cnmt;
import com.mylogistics.model.CnmtImage;
import com.mylogistics.model.Cnmt_Challan;
import com.mylogistics.model.Customer;
import com.mylogistics.model.DailyContract;
import com.mylogistics.model.Employee;
import com.mylogistics.model.Narration;
import com.mylogistics.model.Owner;
import com.mylogistics.model.RegularContract;
import com.mylogistics.model.State;
import com.mylogistics.model.Station;
import com.mylogistics.model.User;
import com.mylogistics.model.VehicleVendorMstr;
import com.mylogistics.model.lhpv.LhpvAdv;
import com.mylogistics.model.lhpv.LhpvBal;
import com.mylogistics.model.lhpv.LhpvSup;
import com.mylogistics.services.CodePatternService;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.InvAndDateService;
//import java.util.Date;

public class CnmtDAOImpl implements CnmtDAO {
	
	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	private HttpSession httpSession;
	
	private String cnmtImgPath = "/var/www/html/Erp_Image/CNMT/CNMT";
	
	@Autowired
	public CnmtDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public static Logger logger = Logger.getLogger(CnmtDAOImpl.class);

	@SuppressWarnings("unchecked")
	@Override
	public Map<String,Object> getContractCode(String custCode) {
		
		DailyContract dailyContract = new DailyContract();
		RegularContract regularContract = new RegularContract();
		
		try{
			session = this.sessionFactory.openSession();
			Criteria cr=session.createCriteria(DailyContract.class);
			ProjectionList dailyList = Projections.projectionList();
			dailyList.add(Projections.property("dlyContBLPMCode"));
		 	cr.setProjection(dailyList);
		 	dailyContract = (DailyContract) cr.list();
		 	
		 	cr=session.createCriteria(RegularContract.class);
			ProjectionList regularList = Projections.projectionList();
			regularList.add(Projections.property("regContBLPMCode"));
		 	cr.setProjection(regularList);
		 	regularContract = (RegularContract) cr.list();
		 	
			}
			catch(Exception e){
				e.printStackTrace();
			} 
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("dailyContract", dailyContract);
			map.put("regularContract", regularContract);
			session.clear();
			session.close();
			return map;
	}
	
	
	@Transactional//done
	public int saveCnmtToDB(Cnmt cnmt, String serialNo,
			String brsLeafDetStatus, double daysLeft, double average,
			Blob blob, Blob confirmBlob, List<InvAndDateService> invList) {
		
		System.out.println("enter into saveCnmtToDB function");
		System.out.println("cnmt = " + cnmt);
		System.out.println("serialNo = " + serialNo);
		System.out.println("brsLeafDetStatus = " + brsLeafDetStatus);
		System.out.println("daysLeft = " + daysLeft);
		System.out.println("average = " + average);
		System.out.println("blob = " + blob);
		System.out.println("confirmBlob = " + confirmBlob);
		System.out.println("invList = " + invList.size());
		int cnmtId = -1;
		User currentUser = (User) httpSession.getAttribute("currentUser");
		cnmt.setIsDone(1);
		// System.out.println("$$$$$$$$$$$$$$$$$"+cnmt.getCnmtInvoiceNo().size());
		try {
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();

			BranchStockLeafDet branchStockLeafDet = new BranchStockLeafDet();
			Criteria cr = session.createCriteria(BranchStockLeafDet.class);
			cr.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_SNO, serialNo));
			cr.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_STATUS,
					brsLeafDetStatus));
			branchStockLeafDet = (BranchStockLeafDet) cr.list().get(0);
			session.delete(branchStockLeafDet);

			if (branchStockLeafDet != null) {
				BranchSStats branchSStats = new BranchSStats();
				branchSStats.setBrsStatsBrCode(branchStockLeafDet
						.getBrsLeafDetBrCode());
				branchSStats.setBrsStatsType("cnmt");
				branchSStats.setBrsStatsDt(cnmt.getCnmtDt());
				branchSStats.setBrsStatsSerialNo(branchStockLeafDet
						.getBrsLeafDetSNo());
				branchSStats.setUserCode(currentUser.getUserCode());
				branchSStats.setbCode(currentUser.getUserBranchCode());
				int result = (Integer) session.save(branchSStats);

				Calendar cal = Calendar.getInstance();
				Date currentDate = cnmt.getCnmtDt();
				cal.setTime(currentDate);
				System.out.println("Current date is-----" + currentDate);
				int currentMonth = cal.get(Calendar.MONTH) + 1;
				System.out.println("Current month is-----" + currentMonth);
				int currentYear = cal.get(Calendar.YEAR);
				System.out.println("Current year is-----" + currentYear);
				if (result > 0) {
					BranchMUStats branchMUStats = new BranchMUStats();
					branchMUStats.setBmusDaysLast(daysLeft);
					System.out.println("daysLeft = " + daysLeft);
					branchMUStats.setBmusStType("cnmt");

					if (Double.isNaN(average)) {
						branchMUStats.setBmusAvgMonthUsage(0.0);
					} else if (Double.isInfinite(average)) {
						branchMUStats.setBmusAvgMonthUsage(0.0);
					} else {
						branchMUStats.setBmusAvgMonthUsage(average);
					}
					System.out.println("average = " + average);
					branchMUStats.setbCode(currentUser.getUserBranchCode());
					branchMUStats.setUserCode(currentUser.getUserCode());
					branchMUStats.setBmusBranchCode(branchStockLeafDet
							.getBrsLeafDetBrCode());
					System.out
							.println("branchStockLeafDet.getBrsLeafDetBrCode() = "
									+ branchStockLeafDet.getBrsLeafDetBrCode());
					branchMUStats.setBmusDt(currentDate);

					// long rowsCount = branchMUStatsDAO.totalCnmtRows();
					List<BranchMUStats> branchMUStatsList = new ArrayList<BranchMUStats>();
					long rowsCount = -1;
					cr = session.createCriteria(BranchMUStats.class);
					cr.add(Restrictions.eq(BranchMUStatsCNTS.BMUS_ST_TYPE,
							"cnmt"));
					branchMUStatsList = cr.list();
					rowsCount = branchMUStatsList.size();

					// long rowsCount = branchMUStatsDAO.totalBMUSRows("cnmt");
					System.out.println("Rows count for cnmt is-----------"
							+ rowsCount);
					if (rowsCount != -1) {
						if (rowsCount == 0) {
							session.save(branchMUStats);
						} else {
							List<BranchMUStats> listStats = new ArrayList<BranchMUStats>();
							org.hibernate.Query query = session
									.createQuery(
											"from BranchMUStats where bmusStType= :type order by bmusId DESC")
									.setMaxResults(1);
							query.setParameter("type", "cnmt");
							listStats = query.list();

							BranchMUStats branchMUStats2 = listStats.get(0);
							Date lastDate = listStats.get(0).getBmusDt();
							int lastId = listStats.get(0).getBmusId();

							System.out.println("The last date is ==========="
									+ lastDate);
							Calendar calender = Calendar.getInstance();
							calender.setTime(lastDate);
							int lastMonth = calender.get(Calendar.MONTH) + 1;
							System.out.println("lastMonth---------------"
									+ lastMonth);
							int lastyear = calender.get(Calendar.YEAR);
							System.out.println("lastyear---------------"
									+ lastyear);
							if ((currentMonth == lastMonth)
									&& (currentYear == lastyear)) {
								if (Double.isNaN(average)) {
									branchMUStats.setBmusAvgMonthUsage(0.0);
								} else if (Double.isInfinite(average)) {
									branchMUStats.setBmusAvgMonthUsage(0.0);
								} else {
									branchMUStats.setBmusAvgMonthUsage(average);
								}
								// branchMUStats.setBmusAvgMonthUsage(average);
								branchMUStats.setBmusDt(currentDate);
								branchMUStats.setBmusDaysLast(daysLeft);
								branchMUStats.setBmusId(lastId);
								System.out.println("Before update" + daysLeft
										+ currentDate + average);
								session.merge(branchMUStats);
								System.out.println("After update");
							} else if ((currentMonth != lastMonth)
									&& (currentYear != lastyear)) {

								System.out
										.println("Enter into function with id ---------"
												+ lastId);
								int delete = -1;
								cr = session
										.createCriteria(BranchMUStats.class);
								cr.add(Restrictions.eq(
										BranchMUStatsCNTS.BMUS_ID, lastId));
								BranchMUStats bMuStats = (BranchMUStats) cr
										.list().get(0);
								session.delete(bMuStats);
								delete = 1;

								System.out
										.println("After deleting old entry---------------------"
												+ delete);
								if (delete == 1) {
									int save = (Integer) session
											.save(branchMUStats);
									System.out
											.println("--------------------After saving new entry"
													+ save);
									if (save > 0) {
										BranchMUStatsBk bk = new BranchMUStatsBk();
										bk.setbCode(branchMUStats2.getbCode());
										bk.setBmusAvgMonthUsage(branchMUStats2
												.getBmusAvgMonthUsage());
										bk.setBmusBranchCode(branchMUStats2
												.getBmusBranchCode());
										bk.setBmusDaysLast(branchMUStats2
												.getBmusDaysLast());
										bk.setBmusDt(branchMUStats2.getBmusDt());
										bk.setBmusId(branchMUStats2.getBmusId());
										bk.setBmusStType(branchMUStats2
												.getBmusStType());
										bk.setUserCode(branchMUStats2
												.getUserCode());
										session.save(bk);
										// branchMUStatsDAO.saveBMUSBK(bk);
										System.out
												.println("---------------After saving into backup");
									}
								}
							} else if ((currentMonth != lastMonth)
									&& (currentYear == lastyear)) {
								System.out
										.println("Enter into function with id ---------"
												+ lastId);
								int delete = -1;
								cr = session
										.createCriteria(BranchMUStats.class);
								cr.add(Restrictions.eq(
										BranchMUStatsCNTS.BMUS_ID, lastId));
								BranchMUStats bMuStats = (BranchMUStats) cr
										.list().get(0);
								session.delete(bMuStats);
								delete = 1;
								System.out
										.println("After deleting old entry---------------------"
												+ delete);
								if (delete == 1) {
									int save = (Integer) session
											.save(branchMUStats);
									System.out
											.println("--------------------After saving new entry"
													+ save);
									if (save > 0) {
										BranchMUStatsBk bk = new BranchMUStatsBk();
										bk.setbCode(branchMUStats2.getbCode());
										bk.setBmusAvgMonthUsage(branchMUStats2
												.getBmusAvgMonthUsage());
										bk.setBmusBranchCode(branchMUStats2
												.getBmusBranchCode());
										bk.setBmusDaysLast(branchMUStats2
												.getBmusDaysLast());
										bk.setBmusDt(branchMUStats2.getBmusDt());
										bk.setBmusId(branchMUStats2.getBmusId());
										bk.setBmusStType(branchMUStats2
												.getBmusStType());
										bk.setUserCode(branchMUStats2
												.getUserCode());
										session.save(bk);
										System.out
												.println("---------------After saving into backup");
									}
								}
							}
						}
					}
				}
				System.out.println("after saving result = " + result);
			}
			
			
			/*cnmt.setCnmtImage(blob);
			cnmt.setCnmtConfirmImage(confirmBlob);*/
			System.out.println("****************" + invList.size());
			ArrayList<Map<String, Object>> cnmtInvList = new ArrayList<Map<String, Object>>();

			if (!invList.isEmpty()) {
				for (int i = 0; i < invList.size(); i++) {
					Map<String, Object> cnmtInvMap = new HashMap<String, Object>();
					cnmtInvMap.put("invoiceNo", invList.get(i).getInvoice());
					cnmtInvMap.put("date", invList.get(i).getDate());
					cnmtInvList.add(cnmtInvMap);
				}
			}

			if (!cnmtInvList.isEmpty()) {
				cnmt.setCnmtInvoiceNo(cnmtInvList);
			}

			cnmt.setCnmtBillPermission("no");
			//cnmt.setCmId(cmId);
			cnmtId = (Integer) session.save(cnmt);
						
			int cmId = -1;
			
			if(blob != null || confirmBlob != null){
				CnmtImage cnmtImage = new CnmtImage();
				cnmtImage.setCnmtImgPath(cnmtImgPath+cnmtId+".pdf");
				cnmtImage.setUserCode(currentUser.getUserCode());
				cnmtImage.setbCode(currentUser.getUserBranchCode());
				cnmtImage.setCnmtId(cnmtId);				
				
				FileOutputStream out = new FileOutputStream(cnmtImage.getCnmtImgPath());
				out.write(blob.getBytes(1, (int)blob.length()));
				out.close();
				
			    cmId = (Integer) session.save(cnmtImage);
			    if(cmId > 0){
			    	cnmt.setCmId(cmId);
					session.merge(cnmt);
				}
			}
			
			
			
			session.flush();
			session.clear();
			transaction.commit();
			System.out.println("user data inserted successfully");
			// temp= 1;
			
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();
			cnmtId = -1;
		}
		
		session.close();
		return cnmtId;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<Cnmt> getCnmt(String cnmtCode) {
		System.out.println("enter into getCnmt function");
		List<Cnmt> result = new ArrayList<Cnmt>();
		try {
			session = this.sessionFactory.openSession();
			Criteria cr = session.createCriteria(Cnmt.class);
			cr.add(Restrictions.eq(CnmtCNTS.CNMT_CANCEL,false));
			cr.add(Restrictions.eq(CnmtCNTS.CNMT_CODE, cnmtCode));
			result = cr.list();
			session.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return result;
	}
	
	
	
	@Override
	public List<Cnmt> getCnmt(String cnmtCode,Session session) {
		System.out.println("enter into getCnmt function");
		List<Cnmt> result = new ArrayList<Cnmt>();
			Criteria cr = session.createCriteria(Cnmt.class);
			cr.add(Restrictions.eq(CnmtCNTS.CNMT_CANCEL,false));
			cr.add(Restrictions.eq(CnmtCNTS.CNMT_CODE, cnmtCode));
			result = cr.list();
		return result;
	}
	

	@Transactional
	@SuppressWarnings("unchecked")
	public List<Cnmt> getCnmtCodeDtCon() {
		List<Cnmt> cnmtList = new ArrayList<Cnmt>();
		try {
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			cnmtList = session.createCriteria(Cnmt.class).list();
			System.out.println("inside getCnmtCodeDtCon of CnmtDAOImple---->"
					+ cnmtList);
			transaction.commit();
			session.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return cnmtList;
	}

	@Transactional
	@SuppressWarnings("unchecked")
	public List<String> getStateLryPrefix() {
		List<String> stateList = new ArrayList<String>();
		try {
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr = session.createCriteria(State.class);
			ProjectionList projList = Projections.projectionList();
			projList.add(Projections.property(StateCNTS.STATE_LRY_PREFIX));
			cr.setProjection(projList);
			stateList = cr.list();
			transaction.commit();
			session.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return stateList;
	}
	
	@Transactional
	@SuppressWarnings("unchecked")
	public int updateCnmt(Cnmt cnmt){
		int cnmtId;
		Date date = new Date(new java.util.Date().getTime());
 		Calendar calendar = Calendar.getInstance();
 		calendar.setTime(date);
 		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try{
			
			cnmt.setCreationTS(calendar);
			session.update(cnmt);
			cnmtId = cnmt.getCnmtId();
			transaction.commit();
			//session.flush();
		}catch(Exception e){
			e.printStackTrace();
			cnmtId = -1;
			transaction.rollback();
		}finally{
			session.clear();
			session.close();
		}
		
		return cnmtId;
	}
	
	
	
	@Override
	public int updateCnmt(Cnmt cnmt,Session session){
		int cnmtId;
		Date date = new Date(new java.util.Date().getTime());
 		Calendar calendar = Calendar.getInstance();
 		calendar.setTime(date);
			cnmt.setCreationTS(calendar);
			session.update(cnmt);
			cnmtId = cnmt.getCnmtId();
		return cnmtId;
	}
	
	
	@Transactional
	@SuppressWarnings("unchecked")
	 public int updateCnmts(Cnmt cnmt){
	     System.out.println("Entered into updateCnmts of DAOImpl----"+cnmt.getCnmtId());
		 int temp;
		 Date date = new Date(new java.util.Date().getTime());
	 	 Calendar calendar = Calendar.getInstance();
	 	 calendar.setTime(date);
			try{
				System.out.println("i m in try");
			     session = this.sessionFactory.openSession();
				 transaction =  session.beginTransaction();
				 cnmt.setCreationTS(calendar);
				 session.update(cnmt);
				 transaction.commit();
				 temp= 1;
				 session.flush();
			}catch(Exception e){
				System.out.println("i m in catch");
				e.printStackTrace();
				temp= -1;
			}
			session.clear();
			session.close();
			return temp;
	 }
	
	@Transactional
	@SuppressWarnings("unchecked")
	public List<Cnmt> getAllCnmtByCode(List<String> cnmtCodeList){
		List<Cnmt> cnmtList = new ArrayList<Cnmt>();
		for(int i=0;i<cnmtCodeList.size();i++){
			try{
				session = this.sessionFactory.openSession();
				transaction = session.beginTransaction();
				Criteria cr=session.createCriteria(Cnmt.class);
				cr.add(Restrictions.eq(CnmtCNTS.CNMT_CANCEL,false));
				cr.add(Restrictions.eq(CnmtCNTS.CNMT_CODE,cnmtCodeList.get(i)));
				List<Cnmt> list = cr.list();
				cnmtList.add(list.get(0));
				transaction.commit();
				session.flush();
			}catch(Exception e){
				e.printStackTrace();
			} 
		}
		session.clear();
		session.close();
		return cnmtList;
	}
	
	
	@Override
	public List<Cnmt> getAllCnmtByCode(List<String> cnmtCodeList,Session session){
		List<Cnmt> cnmtList = new ArrayList<Cnmt>();
		for(int i=0;i<cnmtCodeList.size();i++){
				Criteria cr=session.createCriteria(Cnmt.class);
				cr.add(Restrictions.eq(CnmtCNTS.CNMT_CANCEL,false));
				cr.add(Restrictions.eq(CnmtCNTS.CNMT_CODE,cnmtCodeList.get(i)));
				List<Cnmt> list = cr.list();
				cnmtList.add(list.get(0));
		}
		return cnmtList;
	}
	
	
	
	@Transactional
	 @SuppressWarnings("unchecked")
	public List<Cnmt> getCnmtisViewFalse(String branchCode){
		 List<Cnmt> result = new ArrayList<Cnmt>();
				 
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 Criteria cr=session.createCriteria(Cnmt.class);
			 cr.add(Restrictions.eq(CnmtCNTS.IS_VIEW,false));
			 cr.add(Restrictions.eq(CnmtCNTS.BRANCH_CODE,branchCode));
			 result =cr.list();
			 transaction.commit();
			 session.flush();
			 System.out.println("size of list is-->>"+result.size());
			 for(int i = 0;i<result.size();i++){
		 System.out.println("The values in the list is"+result.get(i).getBranchCode());
		 System.out.println("The values in the list is"+result.get(i).getCnmtBillAt());
		 System.out.println("The values in the list is"+result.get(i).getCnmtConsignee());
			 }
			 System.out.println("After getting the list");
		 }
		 catch(Exception e){
			 e.printStackTrace();
		 }
		 session.clear();
		 session.close();
		 return  result; 
	 }
	
	@SuppressWarnings("unchecked")
	 @Transactional
	 public int updateCnmtisViewTrue(String code[]){
		int temp;
		Date date = new Date(new java.util.Date().getTime());
 		Calendar calendar = Calendar.getInstance();
 		calendar.setTime(date);
		 for(int i=0;i<code.length;i++){
		 System.out.println("The values inside array is ----------"+code[i]);
		 }
		 try{
			 session = this.sessionFactory.openSession();
			 for(int i=0;i<code.length;i++){
				transaction = session.beginTransaction();
				
				Cnmt cnmt = new Cnmt();
		 		List<Cnmt> list = new ArrayList<Cnmt>();
		 		Criteria cr=session.createCriteria(Cnmt.class);
				cr.add(Restrictions.eq(CnmtCNTS.CNMT_CODE,code[i]));
				list =cr.list();
				cnmt = list.get(0);
				cnmt.setView(true);
				cnmt.setCreationTS(calendar);
				 session.update(cnmt);
				 transaction.commit();
			 	}
			 session.flush();
			 
			 temp = 1;
			 }
		 
		 catch(Exception e){
			 e.printStackTrace();
			 temp = -1;
		 }
		 session.clear();
		 session.close();
		 return temp;
	 }
	
	 @SuppressWarnings("unchecked")
	 @Transactional
	 public List<Cnmt> getCnmtList(String cnmtCode){
				
		 List<Cnmt> list = new ArrayList<Cnmt>();	 
		 try{
			 System.out.println("cnmtCode is"+cnmtCode);
			 session = this.sessionFactory.openSession();
			 Criteria cr=session.createCriteria(Cnmt.class);
			 cr.add(Restrictions.eq(CnmtCNTS.CNMT_CODE,cnmtCode));
			 cr.add(Restrictions.eq(CnmtCNTS.CNMT_CANCEL,false));
			 list =cr.setMaxResults(1).list();
		 }		 
		 catch(Exception e){
			 e.printStackTrace();
		 }finally{
			 session.clear();
			 session.close();
		 }
		 return  list; 
	 }
	 
	 @Transactional
	 public Blob getCnmtImageByCode(String cnmtCode){
		 logger.info("Enter into getCnmtImageByCode()");
		 List<Cnmt> cnmtList = new ArrayList<Cnmt>();
		 Blob blob = null;
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 Criteria cr=session.createCriteria(Cnmt.class);
			 cr.add(Restrictions.eq(CnmtCNTS.CNMT_CODE, cnmtCode));
			 cnmtList = cr.list();
			 Cnmt cnmt = cnmtList.get(0);
			 int cmId = cnmt.getCmId();
			 if(cmId > 0){
				 CnmtImage cnmtImage = (CnmtImage) session.get(CnmtImage.class,cmId);
				 String cnmtImgPath = cnmtImage.getCnmtImgPath();
				 File file = new File(cnmtImgPath);
				 if(file.exists()){
					 FileInputStream fileInput = new FileInputStream(file);
					 byte imageInBytes[] = IOUtils.toByteArray(fileInput);
					 blob = new SerialBlob(imageInBytes);
				 }				
			 }
		 }catch(Exception e){
			 logger.error("Exception : "+e);
		 }
		 session.clear();
		 session.close();
		 logger.info("Exit from getCnmtImageByCode()");
		 return blob;
	 }
	 
	 
	 
	 @Transactional
	 public Blob getCnmtConfImageByCode(String cnmtCode){
		 System.out.println("enter into getCnmtConfImageByCode function");
		 List<Cnmt> cnmtList = new ArrayList<Cnmt>();
		 Blob blob = null;
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 Criteria cr=session.createCriteria(Cnmt.class);
			 cr.add(Restrictions.eq(CnmtCNTS.CNMT_CODE, cnmtCode));
			 cnmtList = cr.list();
			 Cnmt cnmt = cnmtList.get(0);
			 int cmId = cnmt.getCmId();
			 if(cmId > 0){
				 CnmtImage cnmtImage = (CnmtImage) session.get(CnmtImage.class,cmId);
				 File file = new File(cnmtImage.getCnmtImgPath());
				 if(file.exists()){
					byte fileInBytes[] = FileUtils.readFileToByteArray(file);
					blob = new SerialBlob(fileInBytes);
				 }			 
			 }
			 session.flush();
		 }catch(Exception e){
			 e.printStackTrace();
		 }
		 session.clear();
		 session.close();
		 return blob;
	 }

		@Transactional
		@SuppressWarnings("unchecked")
		public List<String> getCnmtCode(String cnmtCode){
			
			List<String> cnmtList = new ArrayList<String>();
			try{
				session = this.sessionFactory.openSession();
				transaction = session.beginTransaction();
				Criteria cr=session.createCriteria(Cnmt.class);
				cr.add(Restrictions.eq(CnmtCNTS.CNMT_CANCEL,false));
				cr.add(Restrictions.like(CnmtCNTS.CNMT_CODE, cnmtCode+"%"));
				ProjectionList projList = Projections.projectionList();
				projList.add(Projections.property(CnmtCNTS.CNMT_CODE));
				cr.setProjection(projList);
				cnmtList = cr.list();
				transaction.commit();
				session.flush();
			}
			catch(Exception e){
				e.printStackTrace();
			} 
			session.clear();
			session.close();
			return cnmtList;
		}
		
		@Transactional
		@SuppressWarnings("unchecked")
		public List<Cnmt> getAllBiltyCodes(String branchCode){
			System.out.println("enter into getAllBiltyCodes function");
			List<Cnmt> cnmtCodeList = new ArrayList<Cnmt>();
			try{
				session = this.sessionFactory.openSession();
				transaction = session.beginTransaction();
				Criteria cr=session.createCriteria(Cnmt.class);
				cr.add(Restrictions.eq(CnmtCNTS.BRANCH_CODE, branchCode));
				cr.add(Restrictions.eq(CnmtCNTS.CNMT_CANCEL,false));
				ProjectionList projList = Projections.projectionList();
				projList.add(Projections.property(CnmtCNTS.CNMT_CODE));
				cr.setProjection(projList);
				cnmtCodeList = cr.list();
				transaction.commit();
				session.flush();
			}
			catch(Exception e){
				e.printStackTrace();
			} 
			session.clear();
			session.close();
			return cnmtCodeList;
		}
		
		@Transactional
		@SuppressWarnings("unchecked")
		public List<String> getAllCnCode(String brhCode){
			System.out.println("enter into getAllCnCode function");
			logger.info("Enter into getAllChCode() : BranchCode : "+brhCode);
			List<String> cnmtCodeList = new ArrayList<String>();
			try{
				session = this.sessionFactory.openSession();
				transaction = session.beginTransaction();
				Criteria cr=session.createCriteria(Cnmt.class);
				cr.add(Restrictions.eq(CnmtCNTS.BRANCH_CODE, brhCode));
				ProjectionList projList = Projections.projectionList();
				projList.add(Projections.property(CnmtCNTS.CNMT_CODE));
				cr.setProjection(projList);
				cnmtCodeList = cr.list();
				if(cnmtCodeList.isEmpty())
					logger.info("Cnmt For Chln not found !");
				else
					logger.info("Total CNMT : "+cnmtCodeList.size());
				session.flush();
			}catch(Exception e){
				e.printStackTrace();
				logger.error("Exception : "+e);
			}
			session.clear();
			session.close();
			logger.info("Exit from getAllChCode()....");
			return cnmtCodeList;
		}
		
		@Transactional
		@SuppressWarnings("unchecked")
		public List<Map<String, Object>> getAllCnCodeByCnmt(String brhCode, List<String> cnmtList){
			System.out.println("enter into getAllCnCode function");
			logger.info("Enter into getAllChCode() : BranchCode : "+brhCode);
			List<Map<String, Object>> cnmtCodeList = new ArrayList<Map<String, Object>>();
			User currentUser = (User) httpSession.getAttribute("currentUser");
			try{
				session = this.sessionFactory.openSession();
				transaction = session.beginTransaction();
				Criteria cr=session.createCriteria(Cnmt.class);
				cr.add(Restrictions.eq(CnmtCNTS.BRANCH_CODE, brhCode));
				cr.add(Restrictions.in(CnmtCNTS.CNMT_CODE, cnmtList));			
				
				List list = cr.list();
				
				Iterator<Cnmt> it = list.iterator();
				while(it.hasNext()){
					Cnmt cnmt = (Cnmt)it.next();
					
					List chlnList=session.createCriteria(Cnmt_Challan.class)
							.add(Restrictions.eq(Cnmt_ChallanCNTS.Cnmt_Chln_cnmtId, cnmt.getCnmtId())).list();
					System.out.println("chlnList size="+chlnList.size());
					
					if(chlnList.isEmpty() || currentUser.getUserBranchCode().equalsIgnoreCase("9")){
						Map<String, Object> map = new HashMap<String, Object>();
						
						map.put("cnmtCode", cnmt.getCnmtCode());
						map.put("cnmtNoOfPkg", cnmt.getCnmtNoOfPkg());
						map.put("cnmtActualWt", cnmt.getCnmtActualWt());
						map.put("cnmtFromSt", cnmt.getCnmtFromSt());
						map.put("cnmtToSt", cnmt.getCnmtToSt());
						map.put("cnmtEmpCode", cnmt.getCnmtEmpCode());
						map.put("cnmtDate", cnmt.getCnmtDt());
						
						System.out.println("cnmt Date = "+cnmt.getCnmtDt());
						
						String fromStnCode = cnmt.getCnmtFromSt();
						String toStnCode = cnmt.getCnmtToSt();
						String empCode = cnmt.getCnmtEmpCode();
						
						Station fromStn = (Station) session.get(Station.class, Integer.parseInt(fromStnCode));
						Station toStn = (Station) session.get(Station.class, Integer.parseInt(toStnCode));
						Employee emp = (Employee) session.get(Employee.class, Integer.parseInt(empCode));
						
						map.put("fromStnName", fromStn.getStnName());
						map.put("toStnName", toStn.getStnName());
						map.put("empFaCode", emp.getEmpFaCode());
						
						cnmtCodeList.add(map);
					}
					
				}
				
				if(cnmtCodeList.isEmpty())
					logger.info("Cnmt For Chln not found !");
				else
					logger.info("Total CNMT : "+cnmtCodeList.size());
				session.flush();
			}catch(Exception e){
				e.printStackTrace();
				logger.error("Exception : "+e);
			}
			session.clear();
			session.close();
			logger.info("Exit from getAllChCode()....");
			return cnmtCodeList;
		}
		
		/*
		@Transactional
		@SuppressWarnings("unchecked")
		public List<String> getAllCnCodeByCnmt(List<String> cnmtList){
			System.out.println("enter into getTransCnmt function");
			logger.info("Enter into getTransCnmt()....");
			List<String> cnmtCodeList = new ArrayList<String>();
			User currentUser = (User) httpSession.getAttribute("currentUser");
			try{
				session = this.sessionFactory.openSession();
				transaction = session.beginTransaction();
				Criteria cr=session.createCriteria(Cnmt.class);
				cr.add(Restrictions.eq(CnmtCNTS.CNMT_IS_TRANS,true));
				cr.add(Restrictions.ne(CnmtCNTS.BRANCH_CODE,currentUser.getUserBranchCode()));
				cr.add(Restrictions.in(CnmtCNTS.CNMT_CODE, cnmtList));
				ProjectionList projList = Projections.projectionList();
				projList.add(Projections.property(CnmtCNTS.CNMT_CODE));
				cr.setProjection(projList);
				cnmtCodeList = cr.list();
				if(cnmtCodeList.isEmpty())
					logger.info("TransCnmt not found !");
				else
					logger.info("Total TransCnmt : "+cnmtCodeList.size());
				session.flush();
			}catch(Exception e){
				e.printStackTrace();
			}
			session.clear();
			session.close();
			logger.info("Exit from getTransCnmt()...");
			return cnmtCodeList;
		}
		*/
		
		@Transactional
		@SuppressWarnings("unchecked")
		public List<String> getAllCnCodeByCnmt(List<String> cnmtList){
			System.out.println("enter into getTransCnmt function");
			logger.info("Enter into getTransCnmt()....");
			List<String> cnmtCodeList = new ArrayList<String>();
			User currentUser = (User) httpSession.getAttribute("currentUser");
			try{
				session = this.sessionFactory.openSession();
				transaction = session.beginTransaction();
				Criteria cr=session.createCriteria(Cnmt.class);
				cr.add(Restrictions.eq(CnmtCNTS.CNMT_IS_TRANS,true));
				cr.add(Restrictions.ne(CnmtCNTS.BRANCH_CODE,currentUser.getUserBranchCode()));
				cr.add(Restrictions.in(CnmtCNTS.CNMT_CODE, cnmtList));
				ProjectionList projList = Projections.projectionList();
				projList.add(Projections.property(CnmtCNTS.CNMT_CODE));
				cr.setProjection(projList);
				cnmtCodeList = cr.list();
				if(cnmtCodeList.isEmpty())
					logger.info("TransCnmt not found !");
				else
					logger.info("Total TransCnmt : "+cnmtCodeList.size());
				session.flush();
			}catch(Exception e){
				e.printStackTrace();
			}
			session.clear();
			session.close();
			logger.info("Exit from getTransCnmt()...");
			return cnmtCodeList;
		}
		
		
		@Transactional
		@SuppressWarnings("unchecked")
		public List<String> getTransCnmt(){
			System.out.println("enter into getTransCnmt function");
			logger.info("Enter into getTransCnmt()....");
			List<String> cnmtCodeList = new ArrayList<String>();
			User currentUser = (User) httpSession.getAttribute("currentUser");
			try{
				session = this.sessionFactory.openSession();
				transaction = session.beginTransaction();
				Criteria cr=session.createCriteria(Cnmt.class);
				cr.add(Restrictions.eq(CnmtCNTS.CNMT_IS_TRANS,true));
				cr.add(Restrictions.ne(CnmtCNTS.BRANCH_CODE,currentUser.getUserBranchCode()));
				ProjectionList projList = Projections.projectionList();
				projList.add(Projections.property(CnmtCNTS.CNMT_CODE));
				cr.setProjection(projList);
				cnmtCodeList = cr.list();
				if(cnmtCodeList.isEmpty())
					logger.info("TransCnmt not found !");
				else
					logger.info("Total TransCnmt : "+cnmtCodeList.size());
				session.flush();
			}catch(Exception e){
				e.printStackTrace();
			}
			session.clear();
			session.close();
			logger.info("Exit from getTransCnmt()...");
			return cnmtCodeList;
		}
		
		@Transactional
		@SuppressWarnings("unchecked")
		public List<Map<String, Object>> getTransCnmtByCnmt(List<String> cnmtList){
			System.out.println("enter into getTransCnmt function");
			logger.info("Enter into getTransCnmt()....");
			List<Map<String, Object>> cnmtCodeList = new ArrayList<Map<String, Object>>();
			User currentUser = (User) httpSession.getAttribute("currentUser");
			try{
				session = this.sessionFactory.openSession();
				transaction = session.beginTransaction();
				Criteria cr=session.createCriteria(Cnmt.class);
				cr.add(Restrictions.eq(CnmtCNTS.CNMT_IS_TRANS,true));
				cr.add(Restrictions.in(CnmtCNTS.CNMT_CODE, cnmtList));
				cr.add(Restrictions.ne(CnmtCNTS.BRANCH_CODE,currentUser.getUserBranchCode()));				
				List list = cr.list();
				
				Iterator<Cnmt> it = list.iterator();
				while(it.hasNext()){
					Cnmt cnmt = (Cnmt)it.next();
					Map<String, Object> map = new HashMap<String, Object>();
					
					map.put("cnmtCode", cnmt.getCnmtCode());
					map.put("cnmtNoOfPkg", cnmt.getCnmtNoOfPkg());
					map.put("cnmtActualWt", cnmt.getCnmtActualWt());
					map.put("cnmtFromSt", cnmt.getCnmtFromSt());
					map.put("cnmtToSt", cnmt.getCnmtToSt());
					map.put("cnmtEmpCode", cnmt.getCnmtEmpCode());
					map.put("cnmtDate", cnmt.getCnmtDt());
					
					System.out.println("Cnmt DAte : "+cnmt.getCnmtDt());
					
					String fromStnCode = cnmt.getCnmtFromSt();
					String toStnCode = cnmt.getCnmtToSt();
					String empCode = cnmt.getCnmtEmpCode();
					
					Station fromStn = (Station) session.get(Station.class, Integer.parseInt(fromStnCode));
					Station toStn = (Station) session.get(Station.class, Integer.parseInt(toStnCode));
					Employee emp = (Employee) session.get(Employee.class, Integer.parseInt(empCode));
					
					map.put("fromStnName", fromStn.getStnName());
					map.put("toStnName", toStn.getStnName());
					map.put("empFaCode", emp.getEmpFaCode());
					
					cnmtCodeList.add(map);
				}
				if(cnmtCodeList.isEmpty())
					logger.info("TransCnmt not found !");
				else
					logger.info("Total TransCnmt : "+cnmtCodeList.size());
				session.flush();
			}catch(Exception e){
				e.printStackTrace();
			}
			session.clear();
			session.close();
			logger.info("Exit from getTransCnmt()...");
			return cnmtCodeList;
		}
		
		
		
		@Transactional
		@SuppressWarnings("unchecked")
		public List<Cnmt> getCnmtById(int cnmtId){
			System.out.println("enter into getCnmtById function--->"+cnmtId);
			List<Cnmt> cnmtList = new ArrayList<Cnmt>();
			try{
				session = this.sessionFactory.openSession();
				transaction = session.beginTransaction();
				Criteria cr=session.createCriteria(Cnmt.class);
				cr.add(Restrictions.eq(CnmtCNTS.CNMT_ID, cnmtId));
				cnmtList = cr.list();
				transaction.commit();
				session.flush();
			}
			catch(Exception e){
				e.printStackTrace();
			} 
			session.clear();
			session.close();
			return cnmtList;
		}
		
		
		
		@Transactional
		@SuppressWarnings("unchecked")
		public List<Cnmt> getCnmtByCustCode(String custCode){
			System.out.println("enter into getCnmtByCustCode function");
			List<Cnmt> cnmtList = new ArrayList<Cnmt>();
			try{
				session = this.sessionFactory.openSession();
				transaction = session.beginTransaction();
				Criteria cr=session.createCriteria(Cnmt.class);
				cr.add(Restrictions.eq(CnmtCNTS.CUST_CODE,custCode));
				cnmtList = cr.list();
				transaction.commit();
				session.flush();
			}
			catch(Exception e){
				e.printStackTrace();
			} 
			session.clear();
			session.close();
			return cnmtList;
		}
		
		@Transactional
		@SuppressWarnings("unchecked")
		public List<Cnmt> getCnmtByContCode(String contCode){
			System.out.println("enter into getCnmtByContCode function");
			List<Cnmt> cnmtList = new ArrayList<Cnmt>();
			try{
				session = this.sessionFactory.openSession();
				transaction = session.beginTransaction();
				Criteria cr=session.createCriteria(Cnmt.class);
				cr.add(Restrictions.eq(CnmtCNTS.CONTRACT_CODE,contCode));
				cnmtList = cr.list();
				transaction.commit();
				session.flush();
			}
			catch(Exception e){
				e.printStackTrace();
			} 
			session.clear();
			session.close();
			return cnmtList;
		}
		
		@Transactional
		@SuppressWarnings("unchecked")
		public List<Cnmt> getAllCnmt(){
			System.out.println("enter into getAllCnmt function");
			List<Cnmt> cnmtList = new ArrayList<Cnmt>();
			try{
				session = this.sessionFactory.openSession();
				transaction = session.beginTransaction();
				Criteria cr=session.createCriteria(Cnmt.class);
				cr.add(Restrictions.eq(CnmtCNTS.CNMT_CANCEL,false));
				cnmtList = cr.list();
				
				session.flush();
			}catch(Exception e){
				e.printStackTrace();
			}
			session.clear();
			session.close();
			return cnmtList;
		}
		
		@Transactional
		@SuppressWarnings("unchecked")
		public int addCnmtImg(int cnmtId , Blob blob, String imagePath){
			System.out.println("enter into addCnmtImg function");
			logger.info("Enter in addCnmtImg()...  - CNMT ID : "+cnmtId);
			int result = 0;
			User currentUser = (User) httpSession.getAttribute("currentUser");
			List<Cnmt> cnmtList = new ArrayList<Cnmt>();
			try{
				session = this.sessionFactory.openSession();
				transaction = session.beginTransaction();
				Criteria cr=session.createCriteria(Cnmt.class);
				cr.add(Restrictions.eq(CnmtCNTS.CNMT_ID,cnmtId));
				cnmtList = cr.list();
				
				if(!cnmtList.isEmpty()){
					Cnmt cnmt = cnmtList.get(0);
					logger.info("CNMT Found - ID : "+cnmt.getCnmtId());
					int cmId = cnmt.getCmId();
					try{						
						imagePath = imagePath+"/CNMT"+cnmt.getCnmtId()+".pdf";
						File file = new File(imagePath);
						if(file.exists())
							file.delete();
						if(file.createNewFile()){
							
							byte imageInBytes[] = blob.getBytes(1, (int)blob.length());							
							OutputStream targetFile=  new FileOutputStream(file);
				            targetFile.write(imageInBytes);			            
				            targetFile.close();
				            
							if(cmId > 0){
								logger.info("CNMT Image Already ID : "+cmId);
								CnmtImage cnmtImage = (CnmtImage) session.get(CnmtImage.class, cmId);
								cnmtImage.setCnmtImgPath(imagePath);
								session.merge(cnmtImage);
							}else{								
								CnmtImage cnmtImage = new CnmtImage();
								cnmtImage.setUserCode(currentUser.getUserCode());
								cnmtImage.setbCode(currentUser.getUserBranchCode());
								cnmtImage.setCnmtId(cnmtId);
								cnmtImage.setCnmtImgPath(imagePath);
								int cnImgId = (Integer) session.save(cnmtImage);
								logger.info("CNMT Image New ID : "+cnImgId);
								cnmt.setCmId(cnImgId);
								session.merge(cnmt);
							}
							result = 1;
						}
					}catch(Exception e){
						logger.error("Error in addCnmtImg() : "+e);
						System.out.println("Error in addCnmtImg() : "+e);
						result = -1;
					}					
					/*cnmt.setCnmtImage(blob);
					session.merge(cnmt);*/
				}else{
					logger.info("CNMT is not found !");
				}
				transaction.commit();
				//session.flush();				
			}catch(Exception e){
				e.printStackTrace();
				result = -1;
			}
			session.clear();
			session.close();
			return result;
		}

		
		
		
		
		@Override
		public int addCnmtImg(int cnmtId , Blob blob, String imagePath,Session session){
			System.out.println("enter into addCnmtImg function");
			logger.info("Enter in addCnmtImg()...  - CNMT ID : "+cnmtId);
			int result = 0;
			User currentUser = (User) httpSession.getAttribute("currentUser");
			List<Cnmt> cnmtList = new ArrayList<Cnmt>();
				Criteria cr=session.createCriteria(Cnmt.class);
				cr.add(Restrictions.eq(CnmtCNTS.CNMT_ID,cnmtId));
				cnmtList = cr.list();
				
				if(!cnmtList.isEmpty()){
					Cnmt cnmt = cnmtList.get(0);
					logger.info("CNMT Found - ID : "+cnmt.getCnmtId());
					int cmId = cnmt.getCmId();
					try{						
						imagePath = imagePath+"/CNMT"+cnmt.getCnmtId()+".pdf";
						File file = new File(imagePath);
						if(file.exists())
							file.delete();
						if(file.createNewFile()){
							
							byte imageInBytes[] = blob.getBytes(1, (int)blob.length());							
							OutputStream targetFile=  new FileOutputStream(file);
				            targetFile.write(imageInBytes);			            
				            targetFile.close();
				            
							if(cmId > 0){
								logger.info("CNMT Image Already ID : "+cmId);
								CnmtImage cnmtImage = (CnmtImage) session.get(CnmtImage.class, cmId);
								cnmtImage.setCnmtImgPath(imagePath);
								session.merge(cnmtImage);
							}else{								
								CnmtImage cnmtImage = new CnmtImage();
								cnmtImage.setUserCode(currentUser.getUserCode());
								cnmtImage.setbCode(currentUser.getUserBranchCode());
								cnmtImage.setCnmtId(cnmtId);
								cnmtImage.setCnmtImgPath(imagePath);
								int cnImgId = (Integer) session.save(cnmtImage);
								logger.info("CNMT Image New ID : "+cnImgId);
								cnmt.setCmId(cnImgId);
								session.merge(cnmt);
							}
							result = 1;
						}
					}catch(Exception e){
						logger.error("Error in addCnmtImg() : "+e);
						System.out.println("Error in addCnmtImg() : "+e);
						result = -1;
					}					
					/*cnmt.setCnmtImage(blob);
					session.merge(cnmt);*/
				}else{
					logger.info("CNMT is not found !");
				}
				//session.flush();				
			return result;
		}

		
		
		
		
		
		@Transactional
		@SuppressWarnings("unchecked")
		public CnmtImage getCnmtImg(int cnmtId){
			logger.info("Enter into getCnmtImg() : Cnmt ID = "+cnmtId);
			CnmtImage cnmtImage = null;
			List<CnmtImage> cmList = new ArrayList<>();
			try{
				session = this.sessionFactory.openSession();
				transaction = session.beginTransaction();
				Criteria cr=session.createCriteria(CnmtImage.class);
				cr.add(Restrictions.eq(CnmtImageCNTS.CM_CNID,cnmtId));
				cmList = cr.list();
				if(!cmList.isEmpty()){
					cnmtImage = cmList.get(0);
					File file = new File(cnmtImage.getCnmtImgPath());
					if(! file.exists()){
						cnmtImage = null;
						logger.info("Cnmt image does not exits !");
					}else
						logger.info("Cnmt image exits !");
				}else
					logger.info("CnmtImage does not exist !");
				session.flush();
			}catch(Exception e){
				logger.error("Exception : "+e);
				e.printStackTrace();
			}
			session.clear();
			session.close();
			logger.info("Exit from getCnmtImg() ");
			return cnmtImage;
		}
		
		@Transactional
		@SuppressWarnings("unchecked")
		public int updateCnImg(CnmtImage cnmtImage, Blob blob){
			logger.info("Enter into updateCnImg() : Blob = "+blob);
			System.out.println("enter into updateCnImg function");
			int res = 0;
			try{
				session = this.sessionFactory.openSession();
				transaction = session.beginTransaction();
				
				cnmtImage.setCnmtImgPath(cnmtImgPath+cnmtImage.getCnmtId()+".pdf");
				
				FileOutputStream out = new FileOutputStream(cnmtImage.getCnmtImgPath());
				out.write(blob.getBytes(1, (int) blob.length()));
				out.close();
				
				session.update(cnmtImage);
				transaction.commit();
				
				res = 1;
			}catch(Exception e){
				transaction.rollback();
				logger.error("Exception : "+e);
				res = -1;
			}finally{
				session.clear();
				session.close();
			}
			logger.info("Exit from updateCnImg()");
			return res;
		}
		
		@Transactional
		@SuppressWarnings("unchecked")
		public int saveCnImg(CnmtImage cnmtImage, Blob blob){
			logger.info("Enter into saveCnImg() Blob = "+blob);
			int cmId = 0;
			try{				
				session = this.sessionFactory.openSession();
				transaction = session.beginTransaction();
				
				cnmtImage.setCnmtImgPath(cnmtImgPath+cnmtImage.getCnmtId()+".pdf");
				
				FileOutputStream out = new FileOutputStream(cnmtImage.getCnmtImgPath());
				out.write(blob.getBytes(1, (int)blob.length()));
				out.close();
				
				cmId = (Integer) session.save(cnmtImage);
				transaction.commit();
			}catch(Exception e){
				transaction.rollback();
				logger.error("Exception : "+e);
			}finally{
				session.clear();
				session.close();
			}
			logger.info("Ext from saveCnImg()");
			return cmId;
		}
		
		
		@Transactional
		@SuppressWarnings("unchecked")
		public int updateCnWS(Map<String,Object> map){
			System.out.println("enter into updateCnWS fucntion");
			int res = 0;
			try{
				session = this.sessionFactory.openSession();
				transaction = session.beginTransaction();
				String cnmtCode = (String) map.get("cnmtCode");
				if(cnmtCode != null){
					List<Cnmt> cnmtList = new ArrayList<>();
					Criteria cr=session.createCriteria(Cnmt.class);
					cr.add(Restrictions.eq(CnmtCNTS.CNMT_CODE,cnmtCode));
					cnmtList = cr.list();
					if(!cnmtList.isEmpty()){
						Cnmt cnmt = cnmtList.get(0);
						double cnmtSrtg = Double.parseDouble(String.valueOf(map.get("srtg")));
						cnmt.setCnmtSrtg(cnmtSrtg);
						session.update(cnmt);
					}
				}
				transaction.commit();
				session.flush();
			}catch(Exception e){
				e.printStackTrace();
			}
			session.clear();
			session.close();
			return res;
		}
		

		
		
		@Override
		public int updateCnWS(Map<String,Object> map,Session session){
			System.out.println("enter into updateCnWS fucntion");
			int res = 0;
				String cnmtCode = (String) map.get("cnmtCode");
				if(cnmtCode != null){
					List<Cnmt> cnmtList = new ArrayList<>();
					Criteria cr=session.createCriteria(Cnmt.class);
					cr.add(Restrictions.eq(CnmtCNTS.CNMT_CODE,cnmtCode));
					cnmtList = cr.list();
					if(!cnmtList.isEmpty()){
						Cnmt cnmt = cnmtList.get(0);
						double cnmtSrtg = Double.parseDouble(String.valueOf(map.get("srtg")));
						cnmt.setCnmtSrtg(cnmtSrtg);
						session.update(cnmt);
					}
				}
			return res;
		}

		
		
		
		@SuppressWarnings("unchecked")
		public List<String> getCnmtFrNBill(String custCode, Date frDt, Date toDt){
			System.out.println("enter into getCnmtFrNBill function");
			List<String> cnList = new ArrayList<>();
			User currentUser = (User)httpSession.getAttribute("currentUser");
			try{
				session = this.sessionFactory.openSession();
				
				Criteria cr=session.createCriteria(Cnmt.class);
				cr.add(Restrictions.eq(CnmtCNTS.CUST_CODE, custCode));
				cr.add(Restrictions.isNull(CnmtCNTS.CNMT_BILL_NO));
				cr.add(Restrictions.isNull(CnmtCNTS.PR_BL_CODE));
				cr.add(Restrictions.eq(CnmtCNTS.CNMT_BILL_AT, currentUser.getUserBranchCode()));
				
				if(frDt != null && toDt != null)
					cr.add(Restrictions.and(Restrictions.ge(CnmtCNTS.CNMT_DT, frDt) , Restrictions.le(CnmtCNTS.CNMT_DT, toDt)));
				
				ProjectionList proj = Projections.projectionList();
				proj.add(Projections.property(CnmtCNTS.CNMT_CODE));
			 	cr.setProjection(proj);
				
			 	cr.addOrder(Order.asc(CnmtCNTS.CNMT_CODE));
			 	cnList = cr.list();
			}catch(Exception e){
				e.printStackTrace();
			}
			session.clear();
			session.close();
			return cnList;
		}
		
		@SuppressWarnings("unchecked")
		public List<String> getCnmtFrSBill(String custCode, Date frDt, Date toDt){
			System.out.println("enter into getCnmtFrSBill function");
			List<String> cnList = new ArrayList<>();
			User currentUser = (User)httpSession.getAttribute("currentUser");
			try{
				session = this.sessionFactory.openSession();
				
				Criteria cr=session.createCriteria(Cnmt.class);
				cr.add(Restrictions.eq(CnmtCNTS.CUST_CODE, custCode));
				cr.add(Restrictions.isNotNull(CnmtCNTS.CNMT_BILL_NO));
				cr.add(Restrictions.isNull(CnmtCNTS.PR_BL_CODE));
				cr.add(Restrictions.eq(CnmtCNTS.CNMT_BILL_AT, currentUser.getUserBranchCode()));
				
				if(frDt != null && toDt != null)
					cr.add(Restrictions.and(Restrictions.ge(CnmtCNTS.CNMT_DT, frDt) , Restrictions.le(CnmtCNTS.CNMT_DT, toDt)));
				
				ProjectionList proj = Projections.projectionList();
				proj.add(Projections.property(CnmtCNTS.CNMT_CODE));
			 	cr.setProjection(proj);
				
			 	cnList = cr.list();
			}catch(Exception e){
				e.printStackTrace();
			}
			session.clear();
			session.close();
			return cnList;
		}
		

		
		@Override // FOR RELIANCE primary secondry and o&m
		public List<String> getCnmtFrNBillR(String custCode, Date frDt, Date toDt){
			System.out.println("enter into getCnmtFrNBill function");
			List<String> cnList = new ArrayList<>();
			User currentUser = (User)httpSession.getAttribute("currentUser");
			try{
				session = this.sessionFactory.openSession();
				
				Criteria cr=session.createCriteria(Cnmt.class);
				cr.add(Restrictions.eq(CnmtCNTS.PR_BL_CODE, custCode));
				cr.add(Restrictions.isNull(CnmtCNTS.CNMT_BILL_NO));
				cr.add(Restrictions.eq(CnmtCNTS.CNMT_BILL_AT, currentUser.getUserBranchCode()));
				
				if(frDt != null && toDt != null)
					cr.add(Restrictions.and(Restrictions.ge(CnmtCNTS.CNMT_DT, frDt) , Restrictions.le(CnmtCNTS.CNMT_DT, toDt)));
				
				ProjectionList proj = Projections.projectionList();
				proj.add(Projections.property(CnmtCNTS.CNMT_CODE));
			 	cr.setProjection(proj);
				
			 	cr.addOrder(Order.asc(CnmtCNTS.CNMT_CODE));
			 	cnList = cr.list();
			}catch(Exception e){
				e.printStackTrace();
			}
			session.clear();
			session.close();
			return cnList;
		}


		@Override
		@SuppressWarnings("unchecked")//For reliance bill O&M and primry secndry
		public List<String> getCnmtFrSBillR(String custCode, Date frDt, Date toDt){
			System.out.println("enter into getCnmtFrSBill function");
			List<String> cnList = new ArrayList<>();
			User currentUser = (User)httpSession.getAttribute("currentUser");
			try{
				session = this.sessionFactory.openSession();
				
				Criteria cr=session.createCriteria(Cnmt.class);
				cr.add(Restrictions.eq(CnmtCNTS.PR_BL_CODE, custCode));
				cr.add(Restrictions.isNotNull(CnmtCNTS.CNMT_BILL_NO));
				cr.add(Restrictions.eq(CnmtCNTS.CNMT_BILL_AT, currentUser.getUserBranchCode()));
				
				if(frDt != null && toDt != null)
					cr.add(Restrictions.and(Restrictions.ge(CnmtCNTS.CNMT_DT, frDt) , Restrictions.le(CnmtCNTS.CNMT_DT, toDt)));
				
				ProjectionList proj = Projections.projectionList();
				proj.add(Projections.property(CnmtCNTS.CNMT_CODE));
			 	cr.setProjection(proj);
				
			 	cnList = cr.list();
			}catch(Exception e){
				e.printStackTrace();
			}
			session.clear();
			session.close();
			return cnList;
		}
		

		
		
		
		
		@Transactional
		@SuppressWarnings("unchecked")
		public Blob getCnmtImage(int cnmtImgId){
			logger.info("Enter into getCnmtImage() : CnmtImgID = "+cnmtImgId);
			Blob blob = null;
			try{
				session = this.sessionFactory.openSession();
				CnmtImage cnmtImage = (CnmtImage) session.get(CnmtImage.class, cnmtImgId);
				if (cnmtImage != null) {
					String cnmtImgPath = cnmtImage.getCnmtImgPath();
					 File file = new File(cnmtImgPath);
					 if(file.exists()){
						 FileInputStream fileInput = new FileInputStream(file);
						 byte imageInBytes[] = IOUtils.toByteArray(fileInput);
						 blob = new SerialBlob(imageInBytes);
					 }					
				}
			}catch(Exception e){
				logger.error("Exception : "+e);
			}
			logger.info("Exit from getCnmtImgage()");
			return blob;
		}

		@Override
		@Transactional
		public List<Map<String, Object>> getChlnNSedrByCnmt(String cnmtCode) {
			
			List<Map<String, Object>> chlnNSedrByCnmt = new ArrayList<>();
			
			/*List<String> chlnCodeList = new ArrayList<>();
			String sedrCode = null;*/
			
			try {
				session = this.sessionFactory.openSession();
				Criteria cr=session.createCriteria(Cnmt.class);
				cr.add(Restrictions.eq(CnmtCNTS.CNMT_CODE,cnmtCode));
				cr.add(Restrictions.eq(CnmtCNTS.CNMT_CANCEL,false));
				Cnmt cnmt = (Cnmt) cr.list().get(0);
				
				cr = session.createCriteria(Cnmt_Challan.class);
				cr.add(Restrictions.eq(Cnmt_ChallanCNTS.Cnmt_Chln_cnmtId, cnmt.getCnmtId()));
				
				ProjectionList proj = Projections.projectionList();
				proj.add(Projections.property(Cnmt_ChallanCNTS.Cnmt_Chln_chlnId));
			 	cr.setProjection(proj);
			 	
			 	List<Integer> chlnIdList = cr.list();
			 	
			 	for (Integer chlnId : chlnIdList) {
			 		System.err.println("enter into for loop");
					Map<String, Object> map = new HashMap<>();
			 		
					Challan challan = (Challan) session.get(Challan.class, chlnId);
					
					//get sedr for given challan
					cr = session.createCriteria(ArrivalReport.class);
					cr.add(Restrictions.eq(ArrivalReportCNTS.AR_CHLN_CODE, challan.getChlnCode()));
					
					List<ArrivalReport> arrivalReportList = cr.list();
					
					if (arrivalReportList.size()>0) {
						map.put("chlnCode", challan.getChlnCode());
						map.put("arCode", arrivalReportList.get(0).getArCode());
					} else {
						map.put("chlnCode", challan.getChlnCode());
					}
					
					chlnNSedrByCnmt.add(map);
				}
			 	
			 	session.flush();
			} catch (Exception e) {
				
			}
			
			session.clear();
			session.close();
			return chlnNSedrByCnmt;
		}

		@Override
		@Transactional
		public Map<String, Object> saveEditCnmt(Cnmt cnmt, Blob cnmtImgBlob, String cnmtImgPath) {
			System.out.println("saveEditCnmt at Impl");
			logger.info("Enter into saveEditCnmt()...");
			Map<String, Object> resultMap = new HashMap<String, Object>();
			int temp = 0;
			int cmId = 0;
			User currentUser = (User) httpSession.getAttribute("currentUser");
			
			try {
				session = this.sessionFactory.openSession();
				transaction = session.beginTransaction();
				
				if (cnmtImgBlob != null) {
										
					Query query = session.createQuery("SELECT cmId FROM CnmtImage where cnmtId = :cnmtId");
					query.setInteger("cnmtId", cnmt.getCnmtId());
					List<Integer> cnmtImageIdList = query.list();
					
					cnmtImgPath = cnmtImgPath+"/CNMT"+cnmt.getCnmtId()+".pdf";					
					
					try{
						byte cnmtImgInBytes[] = cnmtImgBlob.getBytes(1, (int) cnmtImgBlob.length());
						File cnmtImg = new File(cnmtImgPath);
						if(cnmtImg.exists())
							cnmtImg.delete();
						if(cnmtImg.createNewFile()){
							OutputStream targetFile=  new FileOutputStream(cnmtImg);
				            targetFile.write(cnmtImgInBytes);			            
				            targetFile.close();
						}
					}catch(Exception e){
						System.out.println("Error : "+e);
						logger.error("Error in writing image : "+e);
					}
					
					if (!cnmtImageIdList.isEmpty()) {
						//update
						CnmtImage cnmtImage = (CnmtImage) session.get(CnmtImage.class, cnmtImageIdList.get(0));
						cnmtImage.setCnmtImgPath(cnmtImgPath);
						session.update(cnmtImage);
						cnmt.setCmId(cnmtImage.getCmId());
					} else {
						//save
						CnmtImage cnmtImage = new CnmtImage();
						cnmtImage.setUserCode(currentUser.getUserCode());
						cnmtImage.setbCode(currentUser.getUserBranchCode());
						cnmtImage.setCnmtId(cnmt.getCnmtId());
						cnmtImage.setCnmtImgPath(cnmtImgPath);
						cmId = (Integer) session.save(cnmtImage);
						cnmt.setCmId(cmId);
					}
				}
				
				cnmt.setCnmtEdit(true);
				session.update(cnmt);				
				transaction.commit();
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				resultMap.put("msg", "success");
				session.flush();
				temp = 1;
			} catch (Exception e) {
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				resultMap.put("msg", "error");
				logger.error("Error in saveEditCnmt : "+e);
				temp = -1;
			}
			session.clear();
			session.close();
			logger.info("Exit from saveEditCnmt()...");
			return resultMap;
		}

		@Override
		@Transactional
		public Map<String, Object> getCnmtNCnmtImg(String cnmtCode) {
			System.out.println("enter into getCnmtNCnmtImg() DB");
			Map<String, Object> cnmtNCnmtImg = new HashMap<>();
			
			List<Map<String, Object>> contList = new ArrayList<>();
			List<Map<String, Object>> regQryList = new ArrayList<>();
			List<Map<String, Object>> dlyQryList = new ArrayList<>();
			List<Station> stationList = new ArrayList<>();
			
			int temp = 0;
			try {
				session = this.sessionFactory.openSession();
				Criteria cr = session.createCriteria(Cnmt.class);
				cr.add(Restrictions.eq(CnmtCNTS.CNMT_CANCEL,false));
				cr.add(Restrictions.eq(CnmtCNTS.CNMT_CODE, cnmtCode));
				List<Cnmt> cnmtList = cr.list();
				
				if (!cnmtList.isEmpty()) {
					Cnmt cnmt = cnmtList.get(0);				
					cnmtNCnmtImg.put("cnmt", cnmt);
					if (cnmt.getCmId()>0) {
						cnmtNCnmtImg.put("cnmtImgId", cnmt.getCmId());
					}
					
					//get contract of given cnmt
					/*if (cnmt.getContractCode() != null && !cnmt.getContractCode().trim().equalsIgnoreCase("")) {
						String contType = cnmt.getContractCode().substring(0, 3);
						int contId = Integer.parseInt(cnmt.getContractCode().substring(3));
						System.out.println("contType: "+contType);
						System.out.println("contId: "+contId);
						
						if (contType.equalsIgnoreCase("reg")) {
							RegularContract cont = (RegularContract) session.get(RegularContract.class, contId);
							cnmtNCnmtImg.put("cont", cont);
						} else if(contType.equalsIgnoreCase("dly")){
							DailyContract cont = (DailyContract) session.get(DailyContract.class, contId);
							cnmtNCnmtImg.put("cont", cont);
						}
					}*/
					
					//get contract list of given customer
					Query regQry = session.createQuery("select new map(regContId as contId, regContCode as contCode, regContFromDt as contFromDt, regContToDt as contToDt, regContFromStation as contFrmStnId, regContFromStation as contFrmStnName, regContDdl as contDdl, regContCostGrade as contCostGrade) from RegularContract where regContBLPMCode = :custId");
					regQry.setString("custId", cnmt.getCustCode());
					regQryList = regQry.list();
					
					Query dlyQry = session.createQuery("select new map(dlyContId as contId, dlyContCode as contCode, dlyContStartDt as contFromDt, dlyContEndDt as contToDt, dlyContFromStation as contFrmStnId, dlyContFromStation as contFrmStnName, dlyContDdl as contDdl, dlyContCostGrade as contCostGrade) from DailyContract where dlyContBLPMCode = :custId");
					dlyQry.setString("custId", cnmt.getCustCode());
					dlyQryList = dlyQry.list();
									
					contList.addAll(regQryList);
					contList.addAll(dlyQryList);
					
					cr = session.createCriteria(Station.class);
					stationList = cr.list();
					
					//get branch
					if (cnmt.getBranchCode() != null && !cnmt.getBranchCode().trim().equalsIgnoreCase("")) {
						Branch branch = (Branch) session.get(Branch.class, Integer.parseInt(cnmt.getBranchCode()));
						cnmtNCnmtImg.put("branch", branch);
					} else {
						cnmtNCnmtImg.put("branch", "");
					}
					
					//get cust
					if (cnmt.getCustCode() != null && !cnmt.getCustCode().trim().equalsIgnoreCase("")) {
						Customer cust = (Customer) session.get(Customer.class, Integer.parseInt(cnmt.getCustCode()));
						cnmtNCnmtImg.put("cust", cust);
					} else {
						cnmtNCnmtImg.put("cust", "");
					}
					
					//get from Stn
					if (cnmt.getCnmtFromSt() != null && !cnmt.getCnmtFromSt().trim().equalsIgnoreCase("")) {
						Station frmStn = (Station) session.get(Station.class, Integer.parseInt(cnmt.getCnmtFromSt()));
						cnmtNCnmtImg.put("frmStn", frmStn);
					} else {
						cnmtNCnmtImg.put("frmStn", "");
					}
					
					//get toStn
					if (cnmt.getCnmtToSt() != null && !cnmt.getCnmtToSt().trim().equalsIgnoreCase("")) {
						Station toStn = (Station) session.get(Station.class, Integer.parseInt(cnmt.getCnmtToSt()));
						cnmtNCnmtImg.put("toStn", toStn);
					} else {
						cnmtNCnmtImg.put("toStn", "");
					}
					
					//get cngnor
					if (cnmt.getCnmtConsignor() != null && !cnmt.getCnmtConsignor().trim().equalsIgnoreCase("")) {
						Customer cngnor = (Customer) session.get(Customer.class, Integer.parseInt(cnmt.getCnmtConsignor()));
						cnmtNCnmtImg.put("cngnor", cngnor);
					} else {
						cnmtNCnmtImg.put("cngnor", "");
					}
					
					//get cngnee
					if (cnmt.getCnmtConsignee() != null && !cnmt.getCnmtConsignee().trim().equalsIgnoreCase("")) {
						Customer cngnee = (Customer) session.get(Customer.class, Integer.parseInt(cnmt.getCnmtConsignee()));
						cnmtNCnmtImg.put("cngnee", cngnee);
					} else {
						cnmtNCnmtImg.put("cngnee", "");
					}
					
					//get payAt
					if (cnmt.getCnmtPayAt() != null && !cnmt.getCnmtPayAt().trim().equalsIgnoreCase("")) {
						Branch payAt = (Branch) session.get(Branch.class, Integer.parseInt(cnmt.getCnmtPayAt()));
						cnmtNCnmtImg.put("payAt", payAt);
					} else {
						cnmtNCnmtImg.put("payAt", "");
					}
					
					//get billAt
					if (cnmt.getCnmtBillAt() != null && !cnmt.getCnmtBillAt().trim().equalsIgnoreCase("")) {
						Branch billAt = (Branch) session.get(Branch.class, Integer.parseInt(cnmt.getCnmtBillAt()));
						cnmtNCnmtImg.put("billAt", billAt);
					} else {
						cnmtNCnmtImg.put("billAt", "");
					}
					
					//get employee
					if (cnmt.getCnmtEmpCode() != null && !cnmt.getCnmtEmpCode().trim().equalsIgnoreCase("")) {
						Employee emp = (Employee) session.get(Employee.class, Integer.parseInt(cnmt.getCnmtEmpCode()));
						cnmtNCnmtImg.put("emp", emp);
					} else {
						cnmtNCnmtImg.put("emp", "");
					}
					
					
					/*cr = session.createCriteria(CnmtImage.class);
					cr.add(Restrictions.eq(CnmtImageCNTS.CM_CNID, cnmtList.get(0).getCnmtId()));
					List<CnmtImage> cnmtImageList = cr.list();
					if (!cnmtImageList.isEmpty()) {
						cnmtNCnmtImg.put("cnmtImgId", cnmtImageList.get(0).getCmId());
					}*/
					/*Query query = session.createQuery("SELECT cmId FROM CnmtImage where cnmtId = :cnmtId");
					query.setInteger("cnmtId", cnmtList.get(0).getCnmtId());
					List<Integer> cnmtImageIdList = query.list();
					if (!cnmtImageIdList.isEmpty()) {
						cnmtNCnmtImg.put("cnmtImgId", cnmtImageIdList.get(0));
					}*/
					temp = 1;					
				} else {
					temp = -1;
				}
				
				session.flush();
			} catch (Exception e) {
				e.printStackTrace();
				temp = -1;
			}
			session.clear();
			session.close();
			
			if (!contList.isEmpty()) {
				for (Map<String, Object> cont : contList) {
					for (Station station : stationList) {
						if (station.getStnId() == Integer.parseInt(String.valueOf(cont.get("contFrmStnId")))) {
							cont.put("contFrmStnName", station.getStnName());
							break;
						}
					}
				}
			}
			
			
			cnmtNCnmtImg.put("contList", contList);
			cnmtNCnmtImg.put("temp", temp);
			return cnmtNCnmtImg;
		}
		
		
		@Override
		@Transactional
		public List<String> getCnmtNoByBill(String blNo){
			System.out.println("enter into getCnmtNoByBill function");
			List<String> cnmtNoList = new ArrayList<String>(); 
			try{
				session = this.sessionFactory.openSession();
				Criteria cr = session.createCriteria(Cnmt.class);
				cr.add(Restrictions.eq(CnmtCNTS.CNMT_BILL_NO, blNo));
				ProjectionList projList = Projections.projectionList();
				projList.add(Projections.property(CnmtCNTS.CNMT_CODE));
				cr.setProjection(projList);
				cnmtNoList = cr.list();
				session.flush();
			}catch(Exception e){
				e.printStackTrace();
			}
			session.clear();
			session.close();
			
			
		List<Bill> blList=new ArrayList<Bill>();
			try{
				session = this.sessionFactory.openSession();
				Criteria cr = session.createCriteria(Bill.class);
				cr.add(Restrictions.eq(BillCNTS.Bill_NO, blNo));
				blList = cr.list();
			 if(!blList.isEmpty()){
				Bill bill=blList.get(0);
				  if(Character.toUpperCase(bill.getBlType())=='S')
					 cnmtNoList.add("00000000");
			 }
				session.flush();
			}catch(Exception e){
				e.printStackTrace();
			}
			session.clear();
			session.close();
		
			return cnmtNoList;
		}

		@Override
		@Transactional
		public Map<String, Object> getUsrBrCnmtCust() {
			System.out.println("CnmtDAOImpl.getUsrBrCnmtCust()");
			Map<String, Object> map = new HashMap<>();
			int temp = 0;
			User currentUser = (User) httpSession.getAttribute("currentUser");
			try {
				session = this.sessionFactory.openSession();
				
				//get user branch
				Query brQuery = session.createQuery("SELECT new map(branchName as branchName, branchId as branchId, branchFaCode as branchFaCode) " +
						"FROM Branch WHERE branchId = :branchId");
				brQuery.setInteger(BranchCNTS.BRANCH_ID, Integer.parseInt(currentUser.getUserBranchCode()));
				List<Map<String, Object>> brList = new ArrayList<>();
				brList = brQuery.list();
				if (!brList.isEmpty()) {
					map.put("branch", brList.get(0));
				} else {
					map.put("branch", "");
				}
				
				//get custList
				Query custQuery = session.createQuery("SELECT new map(custId as custId, custName as custName, custFaCode as custFaCode, custCode as custCode) " +
						"FROM Customer WHERE branchCode = :branchCode");
				custQuery.setString("branchCode", currentUser.getUserBranchCode());
				List<Map<String, Object>> custList = new ArrayList<>();
				custList = custQuery.list();
				if (!custList.isEmpty()) {
					map.put("custList", custList);
				} else {
					map.put("custList", "");
				}
				
				//get cnmt&challan list
				//get all cnmt of a given branch
				/*Query cnmtQuery = session.createQuery("SELECT brsLeafDetSNo FROM BranchStockLeafDet " +
						"WHERE brsLeafDetBrCode = :branchCode and brsLeafDetStatus = :status");
				cnmtQuery.setString("branchCode", currentUser.getUserBranchCode());
				cnmtQuery.setString("status", "cnmt");
				List<String> cnmtCodeList = new ArrayList<>();
				cnmtCodeList = cnmtQuery.list();
				
				//get all chln of given branch
				Query chlnQuery = session.createQuery("SELECT brsLeafDetSNo FROM BranchStockLeafDet " +
						"WHERE brsLeafDetBrCode = :branchCode and brsLeafDetStatus = :status");
				chlnQuery.setString("branchCode", currentUser.getUserBranchCode());
				chlnQuery.setString("status", "chln");
				List<String> chlnCodeList = new ArrayList<>();
				chlnCodeList = chlnQuery.list();
				
				System.out.println("cnmtCodeListSize: "+cnmtCodeList);
				System.out.println("chlnCodeListSize: "+chlnCodeList);
				
				if (!cnmtCodeList.isEmpty() && !chlnCodeList.isEmpty()) {
					cnmtCodeList.retainAll(chlnCodeList);
					map.put("codeList", cnmtCodeList);
				} else {
					map.put("codeList", "");
				}*/
				
				//System.out.println("cnmtCodeListSize after: "+cnmtCodeList.size());
				//System.out.println("chlnCodeListSize after: "+chlnCodeList.size());
				
				temp = 1;
			} catch (Exception e) {
				e.printStackTrace();
				temp = -1;
			} finally {
				session.clear();
				session.close();
			}
			map.put("temp", temp);
			return map;
		}

		@Override
		@Transactional
		public int saveCnmtBbl(Map<String, Object> cnmtBblService) {
			System.out.println("CnmtDAOImpl.saveCnmtBbl()");
			int temp = 0;
			
			User currentUser = (User) httpSession.getAttribute("currentUser");
			
			String panImgType = "";
			
			try {
				session = this.sessionFactory.openSession();
				transaction = session.beginTransaction();
				//check either owner or broker must have pan card
				Owner own = (Owner) session.get(Owner.class, Integer.parseInt(String.valueOf(cnmtBblService.get("ownId"))));
				Broker brk = (Broker) session.get(Broker.class, Integer.parseInt(String.valueOf(cnmtBblService.get("brkId"))));
				if (own.isOwnIsPanImg() || brk.isBrkIsPanImg()) {
					//either owner or broker has pan image
				} else {
					return -2;
				}
				
				/*if (own.isOwnIsPanImg()) {
					panImgType = "Owner";
				} else if (brk.isBrkIsPanImg()) {
					panImgType = "Broker";
				} else {
					panImgType = "Owner";
				}*/				
				
				panImgType = "Owner"; //panHdrType owner fix for bll branch
				
				//remove stationary from branchstockleafdet
				Criteria cr  = session.createCriteria(BranchStockLeafDet.class);
				cr.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_SNO, String.valueOf(cnmtBblService.get("code"))));
				cr.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_BRCODE, String.valueOf(cnmtBblService.get("branchId"))));
				
				List<BranchStockLeafDet> brsLeafDetList = new ArrayList<>();
				brsLeafDetList = cr.list();
				if (!brsLeafDetList.isEmpty()) {
					for (BranchStockLeafDet branchStockLeafDet : brsLeafDetList) {
						session.delete(branchStockLeafDet);
					}
				}else{
					return -3;
				}
				
				//add stationary to branchsstats
				//add cnmt
				BranchSStats brsStatsCnmt = new BranchSStats();
				brsStatsCnmt.setbCode(String.valueOf(cnmtBblService.get("branchId")));
				brsStatsCnmt.setBrsStatsBrCode(String.valueOf(cnmtBblService.get("branchId")));
				brsStatsCnmt.setBrsStatsSerialNo(String.valueOf(cnmtBblService.get("code")));
				brsStatsCnmt.setBrsStatsType("cnmt");
				brsStatsCnmt.setUserCode(currentUser.getUserBranchCode());
				brsStatsCnmt.setBrsStatsDt(CodePatternService.getSqlDate(String.valueOf(cnmtBblService.get("dt"))));
				session.save(brsStatsCnmt);
				
				//add challan
				BranchSStats brsStatsChln = new BranchSStats();
				brsStatsChln.setbCode(String.valueOf(cnmtBblService.get("branchId")));
				brsStatsChln.setBrsStatsBrCode(String.valueOf(cnmtBblService.get("branchId")));
				brsStatsChln.setBrsStatsSerialNo(String.valueOf(cnmtBblService.get("code")));
				brsStatsChln.setBrsStatsType("chln");
				brsStatsChln.setUserCode(currentUser.getUserBranchCode());
				brsStatsChln.setBrsStatsDt(CodePatternService.getSqlDate(String.valueOf(cnmtBblService.get("dt"))));
				session.save(brsStatsChln);
				
				//add cnmt
				Cnmt cnmt = new Cnmt();
				cnmt.setbCode(currentUser.getUserBranchCode());
				cnmt.setBranchCode(String.valueOf(cnmtBblService.get("branchId")));
				cnmt.setCnmtActualWt(Double.parseDouble(String.valueOf(cnmtBblService.get("wt")))*1000.0);
				cnmt.setCnmtBillAt(String.valueOf(cnmtBblService.get("branchId")));
				cnmt.setCnmtBillPermission("no");
				cnmt.setCnmtCode(String.valueOf(cnmtBblService.get("code")));
				cnmt.setCnmtConsignee(String.valueOf(cnmtBblService.get("custId")));
				cnmt.setCnmtConsignor(String.valueOf(cnmtBblService.get("custId")));
				cnmt.setCnmtDC("1 bill");
				cnmt.setCnmtEmpCode(String.valueOf(cnmtBblService.get("empId")));
				cnmt.setCnmtExtraExp(0);
				cnmt.setCnmtFreight(Double.parseDouble(String.valueOf(cnmtBblService.get("cnmtFrt"))));
				cnmt.setCnmtFromSt(String.valueOf(cnmtBblService.get("frmStnId")));
				cnmt.setCnmtGuaranteeWt(Double.parseDouble(String.valueOf(cnmtBblService.get("wt")))*1000.0);
				cnmt.setCnmtKm(0);
				cnmt.setCnmtNoOfPkg(0);
				cnmt.setCnmtRate(Double.parseDouble(String.valueOf(cnmtBblService.get("cnmtRate")))/1000.0);
				cnmt.setCnmtTOT(Double.parseDouble(String.valueOf(cnmtBblService.get("cnmtFrt"))));
				cnmt.setCnmtToSt(String.valueOf(cnmtBblService.get("toStnId")));
				cnmt.setCnmtVOG(0);
				cnmt.setCnmtVehicleType("Z");//Dumphar should enter into database
				cnmt.setContractCode(String.valueOf(cnmtBblService.get("contCode")));
				cnmt.setCustCode(String.valueOf(cnmtBblService.get("custId")));
				cnmt.setIsDone(0);
				cnmt.setView(false);
				cnmt.setUserCode(currentUser.getUserCode());
				cnmt.setCnmtDt(CodePatternService.getSqlDate(String.valueOf(cnmtBblService.get("dt"))));
				cnmt.setCnmtType(ConstantsValues.TYPE_BARBIL);
				cnmt.setCnmtTpNo(String.valueOf(cnmtBblService.get("tpNo")));
				
				int cnmtId = (Integer) session.save(cnmt);
				
				//save challan
				Challan chln = new Challan();
				chln.setbCode(currentUser.getUserBranchCode());
				chln.setBranchCode(String.valueOf(cnmtBblService.get("branchId")));
				chln.setChlnAdvance(String.valueOf(cnmtBblService.get("adv")));
				chln.setChlnBalance(Double.parseDouble(String.valueOf(cnmtBblService.get("bal"))));
				chln.setChlnBrRate(String.valueOf(cnmtBblService.get("chlnRate")));
				chln.setChlnChgWt(String.valueOf(Double.parseDouble(String.valueOf(cnmtBblService.get("wt")))*1000.0));
				chln.setChlnCode(String.valueOf(cnmtBblService.get("code")));
				chln.setChlnEmpCode(String.valueOf(cnmtBblService.get("empId")));
				chln.setChlnExtra("0");
				chln.setChlnFreight(String.valueOf(cnmtBblService.get("chlnFrt")));
				chln.setChlnFromStn(String.valueOf(cnmtBblService.get("frmStnId")));
				chln.setChlnLoadingAmt("0");
				chln.setChlnLryLoadTime("00:00");
				chln.setChlnLryNo(String.valueOf(cnmtBblService.get("rcNo")));
				chln.setChlnLryRate(String.valueOf(Double.parseDouble(String.valueOf(cnmtBblService.get("chlnRate")))/1000.0));
				chln.setChlnLryRptTime("00:00");
				chln.setChlnNoOfPkg("0");
				chln.setChlnPayAt(String.valueOf(cnmtBblService.get("branchId")));
				chln.setChlnStatisticalChg("0");
				chln.setChlnTdsAmt("0");
				chln.setChlnTimeAllow("0");
				chln.setChlnToStn(String.valueOf(cnmtBblService.get("toStnId")));
				chln.setChlnTotalFreight(Double.parseDouble(String.valueOf(cnmtBblService.get("chlnFrt"))));
				chln.setChlnTotalWt(String.valueOf(Double.parseDouble(String.valueOf(cnmtBblService.get("wt")))*1000.0));
				chln.setChlnVehicleType("Dumphar");
				chln.setChlnWtSlip("No");
				chln.setView(false);
				chln.setUserCode(currentUser.getUserCode());
				chln.setChlnDt(CodePatternService.getSqlDate(String.valueOf(cnmtBblService.get("dt"))));
				chln.setChlnRemAdv(Double.parseDouble(String.valueOf(cnmtBblService.get("adv"))));
				chln.setChlnRemBal(Double.parseDouble(String.valueOf(cnmtBblService.get("bal"))));
				chln.setChlnType(ConstantsValues.TYPE_BARBIL);
				chln.setChlnArId(-1);
				
				int chlnId = (Integer) session.save(chln);
				
				//save cnmt_challan
				Cnmt_Challan cnmt_challan = new Cnmt_Challan();
				cnmt_challan.setCnmtId(cnmtId);
				cnmt_challan.setChlnId(chlnId);
				session.save(cnmt_challan);
				
				Criteria vvCriteria = session.createCriteria(VehicleVendorMstr.class);
				vvCriteria.add(Restrictions.eq(VehicleVendorMstrCNTS.VV_RCNO, String.valueOf(cnmtBblService.get("rcNo"))));
				
				VehicleVendorMstr vvMstr = null;
				
				if (vvCriteria.list() != null && !vvCriteria.list().isEmpty()) {
					vvMstr = (VehicleVendorMstr) vvCriteria.list().get(0);
				}
				
				//save challan detail
				ChallanDetail chlnDetail = new ChallanDetail();
				chlnDetail.setbCode(currentUser.getUserBranchCode());
				chlnDetail.setUserCode(currentUser.getUserCode());
				chlnDetail.setChdBrCode(brk.getBrkCode());
				if (brk.getBrkPhNoList() != null && !brk.getBrkPhNoList().isEmpty()) {
					chlnDetail.setChdBrMobNo(brk.getBrkPhNoList().get(0));
				}
				
				chlnDetail.setChdChlnCode(String.valueOf(cnmtBblService.get("code")));
				
				if (vvMstr != null) {
					chlnDetail.setChdDlIssueDt(vvMstr.getVvDriverDLIssueDt());
					chlnDetail.setChdDlNo(vvMstr.getVvDriverDLNo());
					chlnDetail.setChdDlValidDt(vvMstr.getVvDriverDLValidDt());
					chlnDetail.setChdDvrMobNo(vvMstr.getVvDriverMobNo());
					chlnDetail.setChdDvrName(vvMstr.getVvDriverName());
					chlnDetail.setChdFitDocIssueDt(vvMstr.getVvFitIssueDt());
					chlnDetail.setChdFitDocNo(vvMstr.getVvFitNo());
					chlnDetail.setChdFitDocPlace(vvMstr.getVvFitState());
					chlnDetail.setChdFitDocValidDt(vvMstr.getVvFitValidDt());
					chlnDetail.setChdPerIssueDt(vvMstr.getVvPerIssueDt());
					chlnDetail.setChdPerNo(vvMstr.getVvPerNo());
					chlnDetail.setChdPerState(vvMstr.getVvPerState());
					chlnDetail.setChdPerValidDt(vvMstr.getVvPerValidDt());
					chlnDetail.setChdPolicyCom(vvMstr.getVvPolicyComp());
					chlnDetail.setChdPolicyNo(vvMstr.getVvPolicyNo());
					chlnDetail.setChdRcIssueDt(vvMstr.getVvRcIssueDt());
					chlnDetail.setChdRcNo(vvMstr.getVvRcNo());
					chlnDetail.setChdRcValidDt(vvMstr.getVvRcValidDt());
					chlnDetail.setChdTransitPassNo(vvMstr.getVvTransitPassNo());
				}
				
				chlnDetail.setChdOwnCode(own.getOwnCode());
				if (own.getOwnPhNoList() != null && !own.getOwnPhNoList().isEmpty()) {
					chlnDetail.setChdOwnMobNo(own.getOwnPhNoList().get(0));
				}

				chlnDetail.setChdPanHdrType(panImgType);
				
				if (panImgType.equalsIgnoreCase("Owner")) {
					chlnDetail.setChdPanHdrName(own.getOwnName());
					chlnDetail.setChdPanNo(own.getOwnPanNo());
				} else if (panImgType.equalsIgnoreCase("Broker")) {
					chlnDetail.setChdPanHdrName(brk.getBrkName());
					chlnDetail.setChdPanNo(brk.getBrkPanNo());
				}
				
				chlnDetail.setChdType(ConstantsValues.TYPE_BARBIL);
				session.save(chlnDetail);
				session.flush();
				session.clear();
				transaction.commit();
				temp = 1;
			} catch (Exception e) {
				e.printStackTrace();
				transaction.rollback();
				temp = -1;
			} finally {
				session.close();
			}
			
			return temp;
		}
		
		@Override
		@Transactional
		public Object getBblCnmtCode(String cnmtBblCode) {
			System.out.println("Entered into CnmtDAOImpl.getBblCnmtCode()");
			System.out.println(cnmtBblCode);
			List<String> list = new ArrayList<>();
			
			User currentUser = (User) httpSession.getAttribute("currentUser");
			
			try{
				session = this.sessionFactory.openSession();
				//transaction = session.beginTransaction();
				/*Criteria cr=session.createCriteria(BranchStockLeafDet.class);
				cr.add(Restrictions.eq(BranchStockLeafDetCNTS.USER_BRANCH_CODE, "23"));
				cr.add(Restrictions.like(BranchStockLeafDetCNTS.BRSLD_SNO,"%"+cnmtBblCode+"%"));
				ProjectionList projList = Projections.projectionList();
				projList.add(Projections.property(BranchStockLeafDetCNTS.BRSLD_SNO));
				cr.setProjection(projList);
				list = cr.list();
				System.out.println(list.size());*/
				
				//get cnmt&challan list
				//get all cnmt of a given branch
				Query cnmtQuery = session.createQuery("SELECT brsLeafDetSNo FROM BranchStockLeafDet " +
						"WHERE brsLeafDetBrCode = :branchCode and brsLeafDetStatus = :status and brsLeafDetSNo LIKE :cnmtBblCode");
				cnmtQuery.setString("branchCode", currentUser.getUserBranchCode());
				cnmtQuery.setString("status", "cnmt");
				cnmtQuery.setString("cnmtBblCode", "%"+cnmtBblCode+"%");
				List<String> cnmtCodeList = new ArrayList<>();
				cnmtCodeList = cnmtQuery.list();
				
				//get all chln of given branch
				Query chlnQuery = session.createQuery("SELECT brsLeafDetSNo FROM BranchStockLeafDet " +
						"WHERE brsLeafDetBrCode = :branchCode and brsLeafDetStatus = :status and brsLeafDetSNo LIKE :cnmtBblCode");
				chlnQuery.setString("branchCode", currentUser.getUserBranchCode());
				chlnQuery.setString("status", "chln");
				chlnQuery.setString("cnmtBblCode", "%"+cnmtBblCode+"%");
				List<String> chlnCodeList = new ArrayList<>();
				chlnCodeList = chlnQuery.list();
				
				System.out.println("cnmtCodeListSize: "+cnmtCodeList);
				System.out.println("chlnCodeListSize: "+chlnCodeList);
				
				if (!cnmtCodeList.isEmpty() && !chlnCodeList.isEmpty()) {
					cnmtCodeList.retainAll(chlnCodeList);
					list = cnmtCodeList;
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			session.clear();
			session.close();
			
			return list;
		}	
		
		public int cnmtDtlFrCncl(Map<String, String> cnmtDtl){
			 System.out.println("chlnDtlFrCncl()");
				System.out.println("chlncode="+cnmtDtl.get("cnmtCode"));
				System.out.println("CCA.=="+cnmtDtl.get("cca"));
				System.out.println("Date=="+cnmtDtl.get("date"));
				String date=cnmtDtl.get("date");
				String cnmtCode=cnmtDtl.get("cnmtCode");
				String status="cnmt";
				User currentUser = (User)httpSession.getAttribute("currentUser");
				
				int temp=0;
				
				try{
					
					session = this.sessionFactory.openSession();
					transaction = session.beginTransaction();
					
					Query query=session.createQuery("SELECT brsld FROM BranchStockLeafDet brsld where brsLeafDetStatus = :status  AND brsLeafDetSNo = :cnmtCode");
					query.setParameter("status", status);
					query.setParameter("cnmtCode", cnmtCode);
					List<BranchStockLeafDet> list=query.list();

					if(!list.isEmpty()){
						BranchSStats branchSStats = new BranchSStats();
						branchSStats.setBrsStatsBrCode(list.get(0).getBrsLeafDetBrCode());
						branchSStats.setBrsStatsType("cnmt");
						branchSStats.setBrsStatsSerialNo(list.get(0).getBrsLeafDetSNo());
						branchSStats.setUserCode(currentUser.getUserCode());
						branchSStats.setbCode(currentUser.getUserBranchCode());
						branchSStats.setBrsStatsDt( java.sql.Date.valueOf(date));
						session.save(branchSStats);
						BranchStockLeafDet branchStockLeafDet=list.get(0);
						session.delete(branchStockLeafDet);
						
							Cnmt cnmt=new Cnmt();
							cnmt.setCnmtCode(cnmtCode);
							cnmt.setbCode(list.get(0).getBrsLeafDetBrCode());
							cnmt.setCnmtDt(java.sql.Date.valueOf(date));
							cnmt.setIsCCA(cnmtDtl.get("cca"));
							cnmt.setCancel(true);
							session.save(cnmt);
							session.flush();
							session.clear();
							transaction.commit();
							temp=1;
							
					}else{
						temp=0;
					}
				}catch(Exception e){
						transaction.rollback();
					e.printStackTrace();
					temp=0;
				}
				session.close();
				return temp;
		}

	@Override
	public int cnmtDtlFrCstng(Map<String, Object> cnmtDtl) {
		
		logger.info("Enter into cnmtDtlFrCstng()");
		System.out.println("cnmtDtlFrCstng");
		System.out.println("branch=" + cnmtDtl.get("branch"));
		System.out.println("frommDate=" + cnmtDtl.get("frmDt"));
		System.out.println("ToDate=" + cnmtDtl.get("toDt"));
		System.out.println("isConsolidate=" + cnmtDtl.get("isConsolidate"));
		System.out.println("isBranch=" + cnmtDtl.get("isBranch"));
		System.out.println("custCode=" + cnmtDtl.get("custCode"));

		int result = 0;

		Date fromDt = Date.valueOf((String) cnmtDtl.get("frmDt"));
		Date toDt = Date.valueOf((String) cnmtDtl.get("toDt"));
		List<Challan> chlnList = new ArrayList<Challan>();
		HashSet<Integer> cnmtIdList = new HashSet<Integer>();
		List<Cnmt> allCnmtList = new ArrayList<Cnmt>();
		List<Cnmt> cnmtList = new ArrayList<Cnmt>();
		HashSet<Integer> duplicateCnmtList = new HashSet<Integer>();
		List<BillDetail> allBdList = new ArrayList<BillDetail>();

		Session session=null;
		Transaction transaction=null;
		try {
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			if ((boolean) cnmtDtl.get("isConsolidate")) {
				logger.info("Enter into if ((boolean) cnmtDtl.get isConsolidate) ");
				Criteria cr = session.createCriteria(Challan.class);
				cr.add(Restrictions.ge(ChallanCNTS.CHLN_DT, fromDt));
				cr.add(Restrictions.le(ChallanCNTS.CHLN_DT, toDt));
				cr.add(Restrictions.eq(ChallanCNTS.CHLN_CANCEL, false));
				
				ProjectionList chlnPro = Projections.projectionList();
				chlnPro.add(Projections.property(ChallanCNTS.CHLN_ID),ChallanCNTS.CHLN_ID);
				chlnPro.add(Projections.property(ChallanCNTS.CHLN_REM_ADV),ChallanCNTS.CHLN_REM_ADV);
				chlnPro.add(Projections.property(ChallanCNTS.CHLN_REM_BAL),ChallanCNTS.CHLN_REM_BAL);
				chlnPro.add(Projections.property(ChallanCNTS.CHLN_FREIGHT),ChallanCNTS.CHLN_FREIGHT);
				chlnPro.add(Projections.property(ChallanCNTS.CHLN_TOTAL_FREIGHT),ChallanCNTS.CHLN_TOTAL_FREIGHT);
				
				cr.setProjection(chlnPro);
				cr.setResultTransformer(Transformers.aliasToBean(Challan.class));
				
				chlnList = cr.list();
				logger.info("Exit from if ((boolean) cnmtDtl.get isConsolidate) chlnList Size= "+chlnList.size());
				
			} else if ((boolean) cnmtDtl.get("isBranch")) {
				logger.info("Enter into else if ((boolean) cnmtDtl.get isBranch) ");
				Criteria cr = session.createCriteria(Challan.class);
				cr.add(Restrictions.eq(ChallanCNTS.BRANCH_CODE,String.valueOf(cnmtDtl.get("branch"))));
				cr.add(Restrictions.ge(ChallanCNTS.CHLN_DT, fromDt));
				cr.add(Restrictions.le(ChallanCNTS.CHLN_DT, toDt));
				cr.add(Restrictions.eq(ChallanCNTS.CHLN_CANCEL, false));
				
				ProjectionList chlnPro = Projections.projectionList();
				chlnPro.add(Projections.property(ChallanCNTS.CHLN_ID),ChallanCNTS.CHLN_ID);
				chlnPro.add(Projections.property(ChallanCNTS.CHLN_REM_ADV),ChallanCNTS.CHLN_REM_ADV);
				chlnPro.add(Projections.property(ChallanCNTS.CHLN_REM_BAL),ChallanCNTS.CHLN_REM_BAL);
				chlnPro.add(Projections.property(ChallanCNTS.CHLN_FREIGHT),ChallanCNTS.CHLN_FREIGHT);
				chlnPro.add(Projections.property(ChallanCNTS.CHLN_TOTAL_FREIGHT),ChallanCNTS.CHLN_TOTAL_FREIGHT);
				
				cr.setProjection(chlnPro);
				cr.setResultTransformer(Transformers.aliasToBean(Challan.class));
				
				chlnList = cr.list();
				logger.info("Exit from else if ((boolean) cnmtDtl.get isBranch)  chlnList Size= "+chlnList.size());
			}

			if (!chlnList.isEmpty()) {

				List<Integer> chlnIdList = new ArrayList<Integer>();
				Iterator<Challan> chlnIterator = chlnList.iterator();
				while (chlnIterator.hasNext()) {
					Challan chln = (Challan) chlnIterator.next();
					chlnIdList.add(chln.getChlnId());
				}

				 //Criteria laCr= session
				 List<Map<String, Object>> lhpvAdvList= session
						.createCriteria(LhpvAdv.class)
						.createAlias("challan", "chln")
						.setFetchMode("chln", FetchMode.SELECT)
						.add(Restrictions.in("chln.chlnId", chlnIdList))
						.setProjection(Projections.projectionList()
								.add(Projections.property(LhpvAdvCNTS.LHPV_ADV_LAID),LhpvAdvCNTS.LHPV_ADV_LAID)
								.add(Projections.property(LhpvAdvCNTS.LHPV_ADV_TOT_RCVR),LhpvAdvCNTS.LHPV_ADV_TOT_RCVR)
								.add(Projections.property(LhpvAdvCNTS.LHPV_ADV_FINAL_TOT),LhpvAdvCNTS.LHPV_ADV_FINAL_TOT)
								.add(Projections.property(LhpvAdvCNTS.LHPV_ADV_TOTAL_PAY_AMT),LhpvAdvCNTS.LHPV_ADV_TOTAL_PAY_AMT)
								.add(Projections.property("chln.chlnId"), "chlnId")
								)
						.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP)
						.list();
						
				 
				 List<Map<String, Object>> lhpvBalList= session
							.createCriteria(LhpvBal.class)
							.createAlias("challan", "chln")
							.setFetchMode("chln", FetchMode.SELECT)
							.add(Restrictions.in("chln.chlnId", chlnIdList))
							.setProjection(Projections.projectionList()
									.add(Projections.property(LhpvBalCNTS.LHPV_BAL_ID),LhpvBalCNTS.LHPV_BAL_ID)
									.add(Projections.property(LhpvBalCNTS.LHPV_BAL_TOT_RCVR_AMT),LhpvBalCNTS.LHPV_BAL_TOT_RCVR_AMT)
									.add(Projections.property(LhpvBalCNTS.LHPV_BAL_FINAL_TOT),LhpvBalCNTS.LHPV_BAL_FINAL_TOT)
									.add(Projections.property(LhpvBalCNTS.LHPV_BAL_TOTAL_PAY_AMT),LhpvBalCNTS.LHPV_BAL_TOTAL_PAY_AMT)
									.add(Projections.property("chln.chlnId"), "chlnId")
									)
							.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP)
							.list();

				List<LhpvSup> lhpvSubList = session
						.createCriteria(LhpvSup.class)
						.createAlias("challan", "chln")
						.setFetchMode("chln", FetchMode.SELECT)
						.add(Restrictions.in("chln.chlnId", chlnIdList)).list();

				Criteria criteria = session.createCriteria(Cnmt_Challan.class);
				List<Cnmt_Challan> cnmt_Challans = criteria.list();
				
				Criteria crite = session.createCriteria(Cnmt.class);
				crite.add(Restrictions.eq(CnmtCNTS.CNMT_CANCEL, false));
				crite.add(Restrictions.or(Restrictions.eq(CnmtCNTS.CNMT_BILL_AMT, 0.0), Restrictions.eq(CnmtCNTS.CNMT_LHPV_AMT, 0.0)));
				
				allCnmtList = crite.list();

				
				Criteria blDtCr = session.createCriteria(BillDetail.class)
						.createAlias("bill", "bill")
						.add(Restrictions.ge("bill.blBillDt", fromDt))
						.add(Restrictions.eq("bill.blCancel", false))
						.setFetchMode("bill", FetchMode.LAZY);
				
				ProjectionList blDtPro = Projections.projectionList();
				blDtPro.add(Projections.property(BillDetailCNTS.BD_DET_AMT),BillDetailCNTS.BD_DET_AMT);
				blDtPro.add(Projections.property(BillDetailCNTS.BD_TOT_AMT),BillDetailCNTS.BD_TOT_AMT);
				blDtPro.add(Projections.property(BillDetailCNTS.BD_FREIGHT),BillDetailCNTS.BD_FREIGHT);
				blDtPro.add(Projections.property(BillDetailCNTS.BD_UNLOAD_AMT),BillDetailCNTS.BD_UNLOAD_AMT);
				blDtPro.add(Projections.property(BillDetailCNTS.BD_OTH_CHG_AMT),BillDetailCNTS.BD_OTH_CHG_AMT);
				blDtPro.add(Projections.property(BillDetailCNTS.BD_BONUS_AMT),BillDetailCNTS.BD_BONUS_AMT);
				blDtPro.add(Projections.property(BillDetailCNTS.BD_LOAD_AMT),BillDetailCNTS.BD_LOAD_AMT);
				blDtPro.add(Projections.property(BillDetailCNTS.BD_CNMT_ID),BillDetailCNTS.BD_CNMT_ID);
				blDtPro.add(Projections.property(BillDetailCNTS.BD_ID),BillDetailCNTS.BD_ID);
				
				blDtCr.setProjection(blDtPro);
				blDtCr.setResultTransformer(Transformers.aliasToBean(BillDetail.class));
				
				allBdList = blDtCr.list();

				System.out.println("billdetatil list size"+allBdList.size());
				
				
				
				
				for (int i = 0; i < chlnList.size(); i++) {

					List<Integer> cnmtChlnList = new ArrayList<Integer>();

					Double chlnFrt = 0.0;
					Double lhpvAmt = 0.0;
					Double rcvry = 0.0;
					int chlnId = chlnList.get(i).getChlnId();

					if (chlnList.get(i).getChlnRemAdv() == 0
							&& chlnList.get(i).getChlnRemBal() == 0) {
						// Lhpv Amount added

						for (int lai = 0; lai < lhpvAdvList.size(); lai++) {
							Map<String, Object> advMap = (Map) lhpvAdvList.get(lai);							
							if (Integer.parseInt(advMap.get("chlnId").toString()) == chlnId) {
								
								chlnFrt = chlnFrt + Double.parseDouble(advMap.get(LhpvAdvCNTS.LHPV_ADV_FINAL_TOT).toString());
								rcvry = rcvry + Double.parseDouble(advMap.get(LhpvAdvCNTS.LHPV_ADV_TOT_RCVR).toString());
								lhpvAmt = lhpvAmt + Double.parseDouble(advMap.get(LhpvAdvCNTS.LHPV_ADV_TOTAL_PAY_AMT).toString());
								System.out.println("Adv ID = " + advMap.get(LhpvAdvCNTS.LHPV_ADV_ID));
								
							}
						}

						for (int lbi = 0; lbi < lhpvBalList.size(); lbi++) {
							Map<String, Object> balMap = (Map) lhpvBalList.get(lbi);
							if (Integer.parseInt(balMap.get("chlnId").toString()) == chlnId) {
								chlnFrt = chlnFrt + Double.parseDouble(balMap.get(LhpvBalCNTS.LHPV_BAL_FINAL_TOT).toString());
								rcvry = rcvry + Double.parseDouble(balMap.get(LhpvBalCNTS.LHPV_BAL_TOT_RCVR_AMT).toString());
								lhpvAmt = lhpvAmt + Double.parseDouble(balMap.get(LhpvBalCNTS.LHPV_BAL_TOTAL_PAY_AMT).toString());
								System.out.println("Adv ID = " + balMap.get(LhpvBalCNTS.LHPV_BAL_ID));
							}
						}

						for (int lspi = 0; lspi < lhpvSubList.size(); lspi++) {
							LhpvSup sup = (LhpvSup) lhpvSubList.get(lspi);
							Challan balChln = sup.getChallan();
							if (balChln.getChlnId() == chlnId) {
								chlnFrt = chlnFrt + sup.getLspFinalTot();
								rcvry = rcvry + sup.getLspTotRcvrAmt();
								lhpvAmt = lhpvAmt + sup.getLspTotPayAmt();
								System.out
										.println("Adv ID = " + sup.getLspId());
							}
						}

						System.out.println("lhpv");
					} else {
						chlnFrt = chlnList.get(i).getChlnTotalFreight();
					}

					Iterator<Cnmt_Challan> iterator = cnmt_Challans.iterator();

					while (iterator.hasNext()) {
						Cnmt_Challan cnmtChln = iterator.next();
						if (cnmtChln.getChlnId() == chlnId) {
							cnmtChlnList.add(cnmtChln.getCnmtId());
							System.out.println("cnmtId=" + cnmtChln.getCnmtId());
							System.out.println("size=" + cnmtChlnList.size());
						}
					}

					Double cnmtWt = 0.0;
					if (!cnmtChlnList.isEmpty()) {
						for (int j = 0; j < cnmtChlnList.size(); j++) {

							// check for same cnmt in other challan
							for (int a = 0; a < cnmtChlnList.size(); a++) {
								if (cnmtIdList.contains(cnmtChlnList.get(a))) {
									duplicateCnmtList.add(cnmtChlnList.get(j));
								}

							}

							System.out.println("duplicateCnmtList.size()="
									+ duplicateCnmtList.size());

							// cnmtList=crite.list();
							cnmtList = new ArrayList<Cnmt>();
							for (Cnmt cn : allCnmtList) {
								if (cn.getCnmtId() == cnmtChlnList.get(j)) {
									cnmtList.add(cn);
								}
							}
							// cnmt total Guarantee weight
							if (!cnmtList.isEmpty()) {
								cnmtWt = cnmtWt + cnmtList.get(0).getCnmtGuaranteeWt();
							}
						}

						for (int j = 0; j < cnmtChlnList.size(); j++) {

							cnmtList = new ArrayList<Cnmt>();

							for (Cnmt cn : allCnmtList) {
								if (cn.getCnmtId() == cnmtChlnList.get(j)) {
									cnmtList.add(cn);
								}
							}
							List<BillDetail> bdList = new ArrayList<BillDetail>();
							for (BillDetail bd : allBdList) {
								if (bd.getBdCnmtId() == cnmtChlnList.get(j)) {
									bdList.add(bd);
								}
							}

							if (!cnmtList.isEmpty()) {
								Double pl = 0.0;
								Double cost = 0.0;
								Double bilAmt = 0.0;
								Double tempCnFrt = 0.0;
								float costPer = 0.0f;
								Cnmt cnmt = cnmtList.get(0);
								
								if (cnmtWt > 0)
									cost = (chlnFrt / cnmtWt) * cnmtList.get(0).getCnmtGuaranteeWt();
								
								System.out.println("cost=" + cost);
								tempCnFrt = cnmtList.get(0).getCnmtFreight();

								if (duplicateCnmtList.contains(cnmtList.get(0).getCnmtId())) {
									cost = cost + cnmtList.get(0).getCnmtCost();
								}

								pl = cnmtList.get(0).getCnmtFreight() - cost;//margin calculate on cnmt freight if bill not prepaired

								if (!bdList.isEmpty()) {//if bill exist
									double tempBdPl = 0.0;
									double TempBilAmt = 0.0;

									for (int l = 0; l < bdList.size(); l++) {
										if (l > 0) {//add supplementry bill amount
											tempBdPl = tempBdPl+ bdList.get(l).getBdTotAmt();
											TempBilAmt = TempBilAmt+ bdList.get(l).getBdTotAmt();
										} else {//bill Amount
											tempBdPl = tempBdPl + bdList.get(l).getBdBonusAmt() + bdList.get(l).getBdDetAmt()
													+ bdList.get(l).getBdFreight()+ bdList.get(l).getBdLoadAmt()
													+ bdList.get(l).getBdOthChgAmt()+ bdList.get(l).getBdUnloadAmt();
											TempBilAmt = TempBilAmt+ bdList.get(l).getBdBonusAmt()
													+ bdList.get(l).getBdDetAmt()
													+ bdList.get(l).getBdFreight()
													+ bdList.get(l).getBdLoadAmt()
													+ bdList.get(l).getBdOthChgAmt()
													+ bdList.get(l).getBdUnloadAmt();

										}
									}
									pl = tempBdPl - cost;//calculate margin on bill amount
									bilAmt = TempBilAmt;
									tempCnFrt = bilAmt;
								}

								if (tempCnFrt > 0 || tempCnFrt < 0) {
									costPer = (float) ((pl * 100) / tempCnFrt);
								}
								System.out.println("cost=" + cost);
								System.out.println("Profit or loss=" + pl);
								System.out.println("Profit or loss%=" + costPer);
								System.out.println("billAmt=" + bilAmt);
								cnmt.setBilAmt(bilAmt);
								cnmt.setLhpvAmt(lhpvAmt);
								cnmt.setLhpvRcvry(rcvry);
								cnmt.setCostPer(costPer);
								cnmt.setCnmtCost(cost);
								cnmt.setCnmtPL(pl);
								session.merge(cnmt);

								result = 1;

							}

						}

					}

					if (!cnmtChlnList.isEmpty()) {
						for (int k = 0; k < cnmtChlnList.size(); k++) {
							cnmtIdList.add(cnmtChlnList.get(k));
							System.out.println("cnmtChlnList.get(k)="
									+ cnmtChlnList.get(k));
						}

					}

				}
			}
			session.flush();
			session.clear();
			transaction.commit();
		
		} catch (Exception e) {
			logger.info("Exception into cnmtDtlFrCsting= "+e);
			transaction.rollback();
			return -1;
		}finally{
			session.close();
		}
		logger.info("exit from cnmtDtlFrCstng()");
		return result;
	}

	@Override
	public List<Map<String, Object>> getCostingReport(Map<String, Object> map) {
		// TODO Auto-generated method stub

		Date fromDt = Date.valueOf((String) map.get("frmDt"));
		Date toDt = Date.valueOf((String) map.get("toDt"));
		System.out.println("frmDt=" + fromDt);
		System.out.println("toDt=" + map.get("toDt"));
		System.out.println("custCode=" + map.get("custCode"));
		System.out.println("branchCode=" + map.get("branchCode"));
		System.out.println("isBrhCon=" + map.get("isBrhCon"));
		System.out.println("isBrhCon=" + map.get("isBrhCon"));
		System.out.println("isBrhCon=" + map.get("isBrhCon"));
		
		String fromStation = (String) map.get("fromStation");
		String toStation = (String) map.get("toStation");

		List<String> list = new ArrayList<>();
		List<Map<String, Object>> myList = new ArrayList<Map<String, Object>>();
		Session session=null;
		
		try {
			session = this.sessionFactory.openSession();

			if (map.get("isBrhCon").equals("brh")) {

				if (map.get("custCode") != null && !String.valueOf(map.get("custCode")).equalsIgnoreCase("")) {
					
					String queryStr = "select cnmtCode AS cnmtCode, cnmtFromSt AS cnmtFrmSt, cnmtToSt cnmtToSt,"
							+ " cnmt.bCode AS bCode , cnmtConsignee AS consignee , cnmtConsignor AS consignor , cnmtGuaranteeWt AS guaranteeWt , cnmtBillNo AS billNo ,"
							+ " cnmtDt AS cnmtDt,cnmtFreight AS cnmtFrt,bilAmt AS billAmt,lhpvAmt AS lhpvAmt,cnmtCost AS cost,cnmtPl as netMrgn,costPer AS mrgnPr, lhpvRcvry AS rcvry, "
							+ " chlnCode AS chlnCode ,chlnDt AS chlnDt,chlnFreight AS chlnFrt, chlnTotalFreight As chlnToFrt, "
							+ " arDetention AS arDet , arUnloading AS arUnLdng , arOvrHgt AS ovrHt , arPenalty AS penalty , arClaim as arRcvry , arExtKm AS arExtKm"
							+ " from cnmt inner join cnmt_challan on cnmt.cnmtId=cnmt_challan.cnmtId and cnmtDt>=:cnFrmDt and cnmtDt<=:cnToDt and cnmt.bCode= :bCode and cnmt.custCode =:custCode "
							+ " left join challan on cnmt_challan.chlnId=challan.chlnId "
							+ " left join arrivalreport on chlnArId=arId ";
					
					if (fromStation != null && !fromStation.equalsIgnoreCase("") && toStation != null && !toStation.equalsIgnoreCase(""))
						queryStr = queryStr + " WHERE IF('"+fromStation+"' = '', cnmtFromSt LIKE '%%', cnmtFromSt = '"+fromStation+"' ) "
								+ "AND IF ('"+toStation+"' = '', cnmtToSt LIKE '%%', cnmtToSt = '"+toStation+"')";
					else if (fromStation != null && !fromStation.equalsIgnoreCase("")) 
						queryStr = queryStr + " WHERE IF('"+fromStation+"' = '', cnmtFromSt LIKE '%%', cnmtFromSt = '"+fromStation+"' ) ";
					else if (toStation != null && !toStation.equalsIgnoreCase(""))
						queryStr = queryStr + " WHERE IF ('"+toStation+"' = '', cnmtToSt LIKE '%%', cnmtToSt = '"+toStation+"')";
					
					Query query = session.createSQLQuery(queryStr);
					query.setParameter("custCode", map.get("custCode"));
					query.setParameter("bCode", map.get("branchCode"));
					query.setDate("cnFrmDt", fromDt);
					query.setDate("cnToDt", toDt);
					myList = query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP).list();

				} else {
					String queryStr = "select cnmtCode AS cnmtCode, cnmtFromSt AS cnmtFrmSt, cnmtToSt AS cnmtToSt,"
							+ " cnmt.bCode AS bCode , cnmtConsignee AS consignee , cnmtConsignor AS consignor , cnmtGuaranteeWt AS guaranteeWt , cnmtBillNo AS billNo ,"
							+ " cnmtDt AS cnmtDt,cnmtFreight AS cnmtFrt,bilAmt AS billAmt,lhpvAmt AS lhpvAmt,cnmtCost AS cost,cnmtPl as netMrgn,costPer AS mrgnPr,lhpvRcvry AS rcvry, "
							+ " chlnCode AS chlnCode ,chlnDt AS chlnDt,chlnFreight AS chlnFrt, chlnTotalFreight As chlnToFrt, "
							+ " arDetention AS arDet , arUnloading AS arUnLdng , arOvrHgt AS ovrHt , arPenalty AS penalty , arClaim as arRcvry , arExtKm AS arExtKm"
							+ " from cnmt inner join cnmt_challan on cnmt.cnmtId=cnmt_challan.cnmtId and cnmtDt>=:cnFrmDt and cnmtDt<=:cnToDt and cnmt.bCode= :bCode "
							+ " left join challan on cnmt_challan.chlnId=challan.chlnId "
							+ " left join arrivalreport on chlnArId=arId ";
					if (fromStation != null && !fromStation.equalsIgnoreCase("") && toStation != null && !toStation.equalsIgnoreCase(""))
						queryStr = queryStr + " WHERE IF('"+fromStation+"' = '', cnmtFromSt LIKE '%%', cnmtFromSt = '"+fromStation+"' ) "
								+ "AND IF ('"+toStation+"' = '', cnmtToSt LIKE '%%', cnmtToSt = '"+toStation+"')";
					else if (fromStation != null && !fromStation.equalsIgnoreCase("")) 
						queryStr = queryStr + " WHERE IF('"+fromStation+"' = '', cnmtFromSt LIKE '%%', cnmtFromSt = '"+fromStation+"' ) ";
					else if (toStation != null && !toStation.equalsIgnoreCase(""))
						queryStr = queryStr + " WHERE IF ('"+toStation+"' = '', cnmtToSt LIKE '%%', cnmtToSt = '"+toStation+"')";
					
					Query query = session.createSQLQuery(queryStr);
					// query.setParameter("custCode", map.get("custCode"));
					query.setParameter("bCode", map.get("branchCode"));
					query.setDate("cnFrmDt", fromDt);
					query.setDate("cnToDt", toDt);
					myList = query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP).list();

				}

			} else if (map.get("isBrhCon").equals("con")) {
				if (map.get("custCode") != null && !String.valueOf(map.get("custCode")).equalsIgnoreCase("")) {
					String queryStr = "select cnmtCode AS cnmtCode, cnmtFromSt AS cnmtFrmSt, cnmtToSt AS cnmtToSt,"
							+ " cnmt.bCode AS bCode , cnmtConsignee AS consignee , cnmtConsignor AS consignor , cnmtGuaranteeWt AS guaranteeWt , cnmtBillNo AS billNo ,"
							+ " cnmtDt AS cnmtDt,cnmtFreight AS cnmtFrt,bilAmt AS billAmt,lhpvAmt AS lhpvAmt,cnmtCost AS cost,cnmtPl as netMrgn,costPer AS mrgnPr,lhpvRcvry AS rcvry, "
							+ " chlnCode AS chlnCode ,chlnDt AS chlnDt,chlnFreight AS chlnFrt, chlnTotalFreight As chlnToFrt, "
							+ " arDetention AS arDet , arUnloading AS arUnLdng , arOvrHgt AS ovrHt , arPenalty AS penalty , arClaim as arRcvry , arExtKm AS arExtKm"
							+ " from cnmt inner join cnmt_challan on cnmt.cnmtId=cnmt_challan.cnmtId and cnmtDt>=:cnFrmDt and cnmtDt<=:cnToDt and  cnmt.custCode =:custCode "
							+ " left join challan on cnmt_challan.chlnId=challan.chlnId "
							+ " left join arrivalreport on chlnArId=arId ";
					
					if (fromStation != null && !fromStation.equalsIgnoreCase("") && toStation != null && !toStation.equalsIgnoreCase(""))
						queryStr = queryStr + " WHERE IF('"+fromStation+"' = '', cnmtFromSt LIKE '%%', cnmtFromSt = '"+fromStation+"' ) "
								+ "AND IF ('"+toStation+"' = '', cnmtToSt LIKE '%%', cnmtToSt = '"+toStation+"')";
					else if (fromStation != null && !fromStation.equalsIgnoreCase("")) 
						queryStr = queryStr + " WHERE IF('"+fromStation+"' = '', cnmtFromSt LIKE '%%', cnmtFromSt = '"+fromStation+"' ) ";
					else if (toStation != null && !toStation.equalsIgnoreCase(""))
						queryStr = queryStr + " WHERE IF ('"+toStation+"' = '', cnmtToSt LIKE '%%', cnmtToSt = '"+toStation+"')";
					
					Query query = session.createSQLQuery(queryStr);
					query.setParameter("custCode", map.get("custCode"));
					// query.setParameter("bCode", map.get("branchCode"));
					query.setDate("cnFrmDt", fromDt);
					query.setDate("cnToDt", toDt);
					myList = query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP).list();

				} else {
					String queryStr = "select cnmtCode AS cnmtCode, cnmtFromSt AS cnmtFrmSt, cnmtToSt AS cnmtToSt,"
							+ " cnmt.bCode AS bCode , cnmtConsignee AS consignee , cnmtConsignor AS consignor , cnmtGuaranteeWt AS guaranteeWt , cnmtBillNo AS billNo ,"
							+ " cnmtDt AS cnmtDt,cnmtFreight AS cnmtFrt,bilAmt AS billAmt,lhpvAmt AS lhpvAmt,cnmtCost AS cost,cnmtPl as netMrgn,costPer AS mrgnPr,lhpvRcvry AS rcvry, "
							+ " chlnCode AS chlnCode ,chlnDt AS chlnDt,chlnFreight AS chlnFrt, chlnTotalFreight As chlnToFrt, "
							+ " arDetention AS arDet , arUnloading AS arUnLdng , arOvrHgt AS ovrHt , arPenalty AS penalty , arClaim as arRcvry , arExtKm AS arExtKm"
							+ " from cnmt inner join cnmt_challan on cnmt.cnmtId=cnmt_challan.cnmtId and cnmtDt>=:cnFrmDt and cnmtDt<=:cnToDt"
							+ " left join challan on cnmt_challan.chlnId=challan.chlnId "
							+ " left join arrivalreport on chlnArId=arId";
					
					if (fromStation != null && !fromStation.equalsIgnoreCase("") && toStation != null && !toStation.equalsIgnoreCase(""))
						queryStr = queryStr + " WHERE IF('"+fromStation+"' = '', cnmtFromSt LIKE '%%', cnmtFromSt = '"+fromStation+"' ) "
								+ "AND IF ('"+toStation+"' = '', cnmtToSt LIKE '%%', cnmtToSt = '"+toStation+"')";
					else if (fromStation != null && !fromStation.equalsIgnoreCase("")) 
						queryStr = queryStr + " WHERE IF('"+fromStation+"' = '', cnmtFromSt LIKE '%%', cnmtFromSt = '"+fromStation+"' ) ";
					else if (toStation != null && !toStation.equalsIgnoreCase(""))
						queryStr = queryStr + " WHERE IF ('"+toStation+"' = '', cnmtToSt LIKE '%%', cnmtToSt = '"+toStation+"')";
					
					Query query = session.createSQLQuery(queryStr);
					query.setDate("cnFrmDt", fromDt);
					query.setDate("cnToDt", toDt);
					myList = query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP).list();

				}

			}

		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Exception in getCostingReport"+e);
		}finally{
			session.clear();
			session.close();
		}
		return myList;
	}

	@Override
	public List getCnmtFrmToCwise(Map<String, Object> map){
		System.out.println(map.get("custCode"));
		System.out.println(map.get("frmStnCode"));
		System.out.println(map.get("toStnCode"));
		System.out.println(map.get("frmDt"));
		System.out.println(map.get("toDt"));
//		Date fromDt = Date.valueOf((String) map.get("frmDt"));
	//	Date toDt = Date.valueOf((String) map.get("toDt"));
		List list=new ArrayList<>();
		Session session=null;
		try {
			session = this.sessionFactory.openSession();
		//	Criteria cr=session.createCriteria(Cnmt.class);
			
		/*	SQLQuery query=session.createSQLQuery("SELECT bCode,cnmtCode,cnmtDt," +
					"(SELECT stnName FROM station where stnCode=cnmtFromSt)," +
					"(SELECT stnName FROM station where stnCode=cnmtToSt)," +
					"cnmtNoOfPkg,cnmtActualWt,cnmtFreight" +
					" FROM cnmt WHERE isCancel=0");
			query.setParameter("custCode", map.get("custCode"));
			query.setParameter("frmStnCode", map.get("frmStnCode"));
			query.setParameter("toStnCode", map.get("toStnCode"));
			query.setParameter("frmDt", map.get("frmDt"));
			query.setParameter("toDt", map.get("toDt"));
			list=query.list();*/
			
			if(!map.get("custCode").equals("") && map.get("custCode")!= null){
				if(!map.get("frmStnCode").equals("") && map.get("frmStnCode") != null){
					if(!map.get("toStnCode").equals("") && map.get("toStnCode") != null){
						/*cr.add(Restrictions.eq(CnmtCNTS.CUST_CODE, map.get("custCode")))
						  .add(Restrictions.eq(CnmtCNTS.CNMT_FrSt, map.get("frmStnCode")))
						  .add(Restrictions.eq(CnmtCNTS.CNMT_TSt, map.get("toStnCode")))
						  .add(Restrictions.ge(CnmtCNTS.CNMT_DT, fromDt))
						  .add(Restrictions.le(CnmtCNTS.CNMT_DT, toDt));
						ProjectionList projList = Projections.projectionList();
						 projList.add(Projections.property(CnmtCNTS.BRANCH_CODE));
						 projList.add(Projections.property(CnmtCNTS.CNMT_CODE));
						 projList.add(Projections.property(CnmtCNTS.CNMT_DT));
						 projList.add(Projections.property(CnmtCNTS.CNMT_FrSt));
						 projList.add(Projections.property(CnmtCNTS.CNMT_TSt));
						 projList.add(Projections.property(CnmtCNTS.CNMT_NO_OF_PKG));
						 projList.add(Projections.property(CnmtCNTS.CNMT_ACTUAL_WT));
						 projList.add(Projections.property(CnmtCNTS.CNMT_FREIGHT));
						 cr.setProjection(projList);
				        list=cr.list();*/
						
						SQLQuery query=session.createSQLQuery("SELECT bCode,cnmtCode,cnmtDt," +
								"(SELECT stnName FROM station where stnCode=cnmtFromSt)," +
								"(SELECT stnName FROM station where stnCode=cnmtToSt)," +
								"cnmtNoOfPkg,cnmtActualWt,cnmtFreight" +
								" FROM cnmt WHERE isCancel=0 AND custCode =:custCode and cnmtFromSt =:frmStnCode and cnmtToSt =:toStnCode " +
								"AND (cnmtDt>= :frmDt AND cnmtDt <= :toDt)");
						query.setParameter("custCode", map.get("custCode"));
						query.setParameter("frmStnCode", map.get("frmStnCode"));
						query.setParameter("toStnCode", map.get("toStnCode"));
						query.setParameter("frmDt", map.get("frmDt"));
						query.setParameter("toDt", map.get("toDt"));
						list=query.list();
						
					}else{
						/*cr.add(Restrictions.eq(CnmtCNTS.CUST_CODE, map.get("custCode")))
						  .add(Restrictions.eq(CnmtCNTS.CNMT_FrSt, map.get("frmStnCode")))
						  .add(Restrictions.ge(CnmtCNTS.CNMT_DT, fromDt))
						  .add(Restrictions.le(CnmtCNTS.CNMT_DT, toDt));
						ProjectionList projList = Projections.projectionList();
						 projList.add(Projections.property(CnmtCNTS.BRANCH_CODE));
						 projList.add(Projections.property(CnmtCNTS.CNMT_CODE));
						 projList.add(Projections.property(CnmtCNTS.CNMT_DT));
						 projList.add(Projections.property(CnmtCNTS.CNMT_FrSt));
						 projList.add(Projections.property(CnmtCNTS.CNMT_TSt));
						 projList.add(Projections.property(CnmtCNTS.CNMT_NO_OF_PKG));
						 projList.add(Projections.property(CnmtCNTS.CNMT_ACTUAL_WT));
						 projList.add(Projections.property(CnmtCNTS.CNMT_FREIGHT));
						 cr.setProjection(projList);
				        list=cr.list();*/
						
						SQLQuery query=session.createSQLQuery("SELECT bCode,cnmtCode,cnmtDt," +
								"(SELECT stnName FROM station where stnCode=cnmtFromSt)," +
								"(SELECT stnName FROM station where stnCode=cnmtToSt)," +
								"cnmtNoOfPkg,cnmtActualWt,cnmtFreight" +
								" FROM cnmt WHERE isCancel=0 AND custCode =:custCode and cnmtFromSt =:frmStnCode " +
								"AND (cnmtDt>= :frmDt AND cnmtDt <= :toDt)");
						query.setParameter("custCode", map.get("custCode"));
						query.setParameter("frmStnCode", map.get("frmStnCode"));
						//query.setParameter("toStnCode", map.get("toStnCode"));
						query.setParameter("frmDt", map.get("frmDt"));
						query.setParameter("toDt", map.get("toDt"));
						list=query.list();
					}
				}else if(!map.get("toStnCode").equals("") && map.get("toStnCode") != null){
					/*cr.add(Restrictions.eq(CnmtCNTS.CUST_CODE, map.get("custCode")))
					  .add(Restrictions.eq(CnmtCNTS.CNMT_TSt, map.get("toStnCode")))
					  .add(Restrictions.ge(CnmtCNTS.CNMT_DT, fromDt))
					  .add(Restrictions.le(CnmtCNTS.CNMT_DT, toDt));
					ProjectionList projList = Projections.projectionList();
					 projList.add(Projections.property(CnmtCNTS.BRANCH_CODE));
					 projList.add(Projections.property(CnmtCNTS.CNMT_CODE));
					 projList.add(Projections.property(CnmtCNTS.CNMT_DT));
					 projList.add(Projections.property(CnmtCNTS.CNMT_FrSt));
					 projList.add(Projections.property(CnmtCNTS.CNMT_TSt));
					 projList.add(Projections.property(CnmtCNTS.CNMT_NO_OF_PKG));
					 projList.add(Projections.property(CnmtCNTS.CNMT_ACTUAL_WT));
					 projList.add(Projections.property(CnmtCNTS.CNMT_FREIGHT));
					 cr.setProjection(projList);
			        list=cr.list();*/
					SQLQuery query=session.createSQLQuery("SELECT bCode,cnmtCode,cnmtDt," +
							"(SELECT stnName FROM station where stnCode=cnmtFromSt)," +
							"(SELECT stnName FROM station where stnCode=cnmtToSt)," +
							"cnmtNoOfPkg,cnmtActualWt,cnmtFreight" +
							" FROM cnmt WHERE isCancel=0 AND custCode =:custCode and cnmtToSt =:toStnCode " +
								"AND (cnmtDt>= :frmDt AND cnmtDt <= :toDt)");
					query.setParameter("custCode", map.get("custCode"));
					//query.setParameter("frmStnCode", map.get("frmStnCode"));
					query.setParameter("toStnCode", map.get("toStnCode"));
					query.setParameter("frmDt", map.get("frmDt"));
					query.setParameter("toDt", map.get("toDt"));
					list=query.list();
					
				}else{
					/*cr.add(Restrictions.eq(CnmtCNTS.CUST_CODE, map.get("custCode")))
					  .add(Restrictions.ge(CnmtCNTS.CNMT_DT, fromDt))
					  .add(Restrictions.le(CnmtCNTS.CNMT_DT, toDt));
					ProjectionList projList = Projections.projectionList();
					 projList.add(Projections.property(CnmtCNTS.BRANCH_CODE));
					 projList.add(Projections.property(CnmtCNTS.CNMT_CODE));
					 projList.add(Projections.property(CnmtCNTS.CNMT_DT));
					 projList.add(Projections.property(CnmtCNTS.CNMT_FrSt));
					 projList.add(Projections.property(CnmtCNTS.CNMT_TSt));
					 projList.add(Projections.property(CnmtCNTS.CNMT_NO_OF_PKG));
					 projList.add(Projections.property(CnmtCNTS.CNMT_ACTUAL_WT));
					 projList.add(Projections.property(CnmtCNTS.CNMT_FREIGHT));
					 cr.setProjection(projList);*/
					SQLQuery query=session.createSQLQuery("SELECT bCode,cnmtCode,cnmtDt," +
							"(SELECT stnName FROM station where stnCode=cnmtFromSt)," +
							"(SELECT stnName FROM station where stnCode=cnmtToSt)," +
							"cnmtNoOfPkg,cnmtActualWt,cnmtFreight" +
							" FROM cnmt WHERE isCancel=0 AND custCode =:custCode " +
								"AND (cnmtDt>= :frmDt AND cnmtDt <= :toDt)");
					query.setParameter("custCode", map.get("custCode"));
					//query.setParameter("frmStnCode", map.get("frmStnCode"));
					//query.setParameter("toStnCode", map.get("toStnCode"));
					query.setParameter("frmDt", map.get("frmDt"));
					query.setParameter("toDt", map.get("toDt"));
					list=query.list();
					
			       // list=cr.list();
				}
			}else if(!map.get("frmStnCode").equals("") && map.get("frmStnCode") != null){
				if(!map.get("toStnCode").equals("") && map.get("toStnCode") != null){
					/*cr.add(Restrictions.eq(CnmtCNTS.CNMT_FrSt, map.get("frmStnCode")))
					  .add(Restrictions.eq(CnmtCNTS.CNMT_TSt, map.get("toStnCode")))
					  .add(Restrictions.ge(CnmtCNTS.CNMT_DT, fromDt))
					  .add(Restrictions.le(CnmtCNTS.CNMT_DT, toDt));
					ProjectionList projList = Projections.projectionList();
					 projList.add(Projections.property(CnmtCNTS.BRANCH_CODE));
					 projList.add(Projections.property(CnmtCNTS.CNMT_CODE));
					 projList.add(Projections.property(CnmtCNTS.CNMT_DT));
					 projList.add(Projections.property(CnmtCNTS.CNMT_FrSt));
					 projList.add(Projections.property(CnmtCNTS.CNMT_TSt));
					 projList.add(Projections.property(CnmtCNTS.CNMT_NO_OF_PKG));
					 projList.add(Projections.property(CnmtCNTS.CNMT_ACTUAL_WT));
					 projList.add(Projections.property(CnmtCNTS.CNMT_FREIGHT));
					 cr.setProjection(projList);
			        list=cr.list();*/
					
					SQLQuery query=session.createSQLQuery("SELECT bCode,cnmtCode,cnmtDt," +
							"(SELECT stnName FROM station where stnCode=cnmtFromSt)," +
							"(SELECT stnName FROM station where stnCode=cnmtToSt)," +
							"cnmtNoOfPkg,cnmtActualWt,cnmtFreight" +
							" FROM cnmt WHERE isCancel=0 AND  cnmtFromSt =:frmStnCode and cnmtToSt =:toStnCode " +
								"AND (cnmtDt>= :frmDt AND cnmtDt <= :toDt)");
					//query.setParameter("custCode", map.get("custCode"));
					query.setParameter("frmStnCode", map.get("frmStnCode"));
					query.setParameter("toStnCode", map.get("toStnCode"));
					query.setParameter("frmDt", map.get("frmDt"));
					query.setParameter("toDt", map.get("toDt"));
					list=query.list();
				}else{
					/*cr.add(Restrictions.eq(CnmtCNTS.CNMT_FrSt, map.get("frmStnCode")))
					  .add(Restrictions.ge(CnmtCNTS.CNMT_DT, fromDt))
					  .add(Restrictions.le(CnmtCNTS.CNMT_DT, toDt));
					ProjectionList projList = Projections.projectionList();
					 projList.add(Projections.property(CnmtCNTS.BRANCH_CODE));
					 projList.add(Projections.property(CnmtCNTS.CNMT_CODE));
					 projList.add(Projections.property(CnmtCNTS.CNMT_DT));
					 projList.add(Projections.property(CnmtCNTS.CNMT_FrSt));
					 projList.add(Projections.property(CnmtCNTS.CNMT_TSt));
					 projList.add(Projections.property(CnmtCNTS.CNMT_NO_OF_PKG));
					 projList.add(Projections.property(CnmtCNTS.CNMT_ACTUAL_WT));
					 projList.add(Projections.property(CnmtCNTS.CNMT_FREIGHT));
					 cr.setProjection(projList);
			        list=cr.list();*/
					SQLQuery query=session.createSQLQuery("SELECT bCode,cnmtCode,cnmtDt," +
							"(SELECT stnName FROM station where stnCode=cnmtFromSt)," +
							"(SELECT stnName FROM station where stnCode=cnmtToSt)," +
							"cnmtNoOfPkg,cnmtActualWt,cnmtFreight" +
							" FROM cnmt WHERE isCancel=0 AND  cnmtFromSt =:frmStnCode " +
								"AND (cnmtDt>= :frmDt AND cnmtDt <= :toDt)");
					//query.setParameter("custCode", map.get("custCode"));
					query.setParameter("frmStnCode", map.get("frmStnCode"));
					//query.setParameter("toStnCode", map.get("toStnCode"));
					query.setParameter("frmDt", map.get("frmDt"));
					query.setParameter("toDt", map.get("toDt"));
					list=query.list();
				}
			}else if(!map.get("toStnCode").equals("") && map.get("toStnCode") != null){
				/*cr.add(Restrictions.eq(CnmtCNTS.CNMT_TSt, map.get("toStnCode")))
				  .add(Restrictions.ge(CnmtCNTS.CNMT_DT, fromDt))
				  .add(Restrictions.le(CnmtCNTS.CNMT_DT, toDt));
				ProjectionList projList = Projections.projectionList();
				 projList.add(Projections.property(CnmtCNTS.BRANCH_CODE));
				 projList.add(Projections.property(CnmtCNTS.CNMT_CODE));
				 projList.add(Projections.property(CnmtCNTS.CNMT_DT));
				 projList.add(Projections.property(CnmtCNTS.CNMT_FrSt));
				 projList.add(Projections.property(CnmtCNTS.CNMT_TSt));
				 projList.add(Projections.property(CnmtCNTS.CNMT_NO_OF_PKG));
				 projList.add(Projections.property(CnmtCNTS.CNMT_ACTUAL_WT));
				 projList.add(Projections.property(CnmtCNTS.CNMT_FREIGHT));
				 cr.setProjection(projList);
		        list=cr.list();*/
				SQLQuery query=session.createSQLQuery("SELECT bCode,cnmtCode,cnmtDt," +
						"(SELECT stnName FROM station where stnCode=cnmtFromSt)," +
						"(SELECT stnName FROM station where stnCode=cnmtToSt)," +
						"cnmtNoOfPkg,cnmtActualWt,cnmtFreight" +
						" FROM cnmt WHERE isCancel=0 AND cnmtToSt =:toStnCode " +
								"AND (cnmtDt >= :frmDt AND cnmtDt <= :toDt)");
				//query.setParameter("custCode", map.get("custCode"));
				//query.setParameter("frmStnCode", map.get("frmStnCode"));
				query.setParameter("toStnCode", map.get("toStnCode"));
				query.setParameter("frmDt", map.get("frmDt"));
				query.setParameter("toDt", map.get("toDt"));
				list=query.list();
				
			}else{
				/*cr.add(Restrictions.ge(CnmtCNTS.CNMT_DT, fromDt))
				  .add(Restrictions.le(CnmtCNTS.CNMT_DT, toDt));
				 ProjectionList projList = Projections.projectionList();
				 projList.add(Projections.property(CnmtCNTS.BRANCH_CODE));
				 projList.add(Projections.property(CnmtCNTS.CNMT_CODE));
				 projList.add(Projections.property(CnmtCNTS.CNMT_DT));
				 projList.add(Projections.property(CnmtCNTS.CNMT_FrSt));
				 projList.add(Projections.property(CnmtCNTS.CNMT_TSt));
				 projList.add(Projections.property(CnmtCNTS.CNMT_NO_OF_PKG));
				 projList.add(Projections.property(CnmtCNTS.CNMT_ACTUAL_WT));
				 projList.add(Projections.property(CnmtCNTS.CNMT_FREIGHT));
				 cr.setProjection(projList);
		        list=cr.list();*/
				
				SQLQuery query=session.createSQLQuery("SELECT bCode,cnmtCode,cnmtDt," +
						"(SELECT stnName FROM station where stnCode=cnmtFromSt) AS frmStn," +
						"(SELECT stnName FROM station where stnCode=cnmtToSt) AS toStn," +
						"cnmtNoOfPkg,cnmtActualWt,cnmtFreight" +
						" FROM cnmt WHERE isCancel=0 " +
						"AND (cnmtDt>= :frmDt AND cnmtDt <= :toDt)");
				query.setParameter("frmDt", map.get("frmDt"));
				query.setParameter("toDt", map.get("toDt"));
				list=query.list();
			}
		
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			session.close();
		}
		return list;
	}
	
	@Override
	public List<Map<String, Object>> getCnmtByChlnCodeForMemo(Session session, User user, String chlnCode){
		logger.info("UserID = "+user.getUserId()+" : Enter into getCnmtByChlnCodeForMemo() : ChlnCode = "+chlnCode);
		List<Map<String, Object>> list = new ArrayList<Map<String,Object>>();
		try{
			List<Challan> chlnList = session.createCriteria(Challan.class)
					.add(Restrictions.eq(ChallanCNTS.CHALLAN_CODE, chlnCode))
					.setProjection(
						Projections.projectionList().add(Projections.property(ChallanCNTS.CHLN_ID), ChallanCNTS.CHLN_ID)
							)
					.setResultTransformer(Transformers.aliasToBean(Challan.class))
					.list();
			
			if(! chlnList.isEmpty()){
				Integer chlnId = chlnList.get(0).getChlnId();
				List<Cnmt_Challan> cnmtChlnList = session.createCriteria(Cnmt_Challan.class)
						.add(Restrictions.eq(Cnmt_ChallanCNTS.Cnmt_Chln_chlnId, chlnId))
						.list();
				if(! cnmtChlnList.isEmpty()){
					for(int i=0; i<cnmtChlnList.size(); i++){
						Cnmt_Challan cnmt_Challan = (Cnmt_Challan)cnmtChlnList.get(i);
						
						Cnmt cnmt = (Cnmt) session.get(Cnmt.class, cnmt_Challan.getCnmtId());
						if(cnmt != null){
							Station fromStation = (Station) session.get(Station.class, Integer.parseInt(cnmt.getCnmtFromSt()));
							Station toStation = (Station) session.get(Station.class, Integer.parseInt(cnmt.getCnmtToSt()));
							
							Map<String, Object> cnmtMap = new HashMap<String, Object>();
							
							cnmtMap.put("cnmtId", cnmt.getCnmtId());
							cnmtMap.put("cnmtCode", cnmt.getCnmtCode());
							cnmtMap.put("cnmtDt", cnmt.getCnmtDt());
							cnmtMap.put("cnmtNoOfPkg", cnmt.getCnmtNoOfPkg());
							cnmtMap.put("cnmtActualWt", cnmt.getCnmtActualWt()/1000);
							cnmtMap.put("arRemark", cnmt.getArRemark());
							cnmtMap.put("fromStation", fromStation.getStnName());
							cnmtMap.put("toStation", toStation.getStnName());
							
							list.add(cnmtMap);
						}else
							logger.info("UserID = "+user.getUserId()+" : Cnmt not found !");
					}
				}else{
					logger.info("UserID = "+user.getUserId()+" : CNMT_Challan not found !");
				}
			}else
				logger.info("UserID = "+user.getUserId()+" : Challan not found !");
			
		}catch(Exception e){
			logger.info("UserID = "+user.getUserId()+" : Exception = "+e);
		}
		logger.info("UserID = "+user.getUserId()+" : Exit from getCnmtByChlnCodeForMemo()");
		return list;
	}
	
	
	@Override
	public List<Map<String, Object>> getHoCostingReport(Session session, User user, Map<String, Object> initParam) {
		logger.info("UserID = "+user.getUserId() + " : Enter into getHoCostingReport() : InitParam = "+initParam);
		
		String isConBrh = String.valueOf(initParam.get("isConBrh"));
		
		String brhId = null;
		if(initParam.get("brhId") != null)
				brhId = String.valueOf(initParam.get("brhId"));
		String custId = null;
		if(initParam.get("custId") != null)
				custId = String.valueOf(initParam.get("custId"));
		
		Date fromDt = Date.valueOf((String) initParam.get("frmDt"));
		Date toDt = Date.valueOf((String) initParam.get("toDt"));
		
		List<String> list = new ArrayList<>();
		List<Map<String, Object>> myList = new ArrayList<Map<String, Object>>();
		
		try {

			if (isConBrh.equals("brh")) {

				if (custId != null && !String.valueOf(custId).equalsIgnoreCase("")) {
					Query query = session.createSQLQuery("select cnmtCode AS cnmtCode,(select stnName from station where cnmtFromSt=stnCode) AS cnmtFrmStn,(select stnName from station where cnmtToSt=stnCode) AS cnmtToStn,"
									+ " cnmt.bCode AS bCode , (select custName from customer where custCode=cnmtConsignee) AS consignee , (select custName from customer where custCode=cnmtConsignor) AS consignor , cnmtGuaranteeWt AS guaranteeWt , cnmtBillNo AS billNo ,"
									+ " cnmtDt AS cnmtDt,cnmtFreight AS cnmtFrt,bilAmt AS billAmt,lhpvAmt AS lhpvAmt,cnmtCost AS cost,cnmtPl as netMrgn,costPer AS mrgnPr, lhpvRcvry AS rcvry, "
									+ " chlnCode AS chlnCode ,chlnDt AS chlnDt,chlnFreight AS chlnFrt, chlnTotalFreight As chlnToFrt, "
									+ " arDetention AS arDet , arUnloading AS arUnLdng , arOvrHgt AS ovrHt , arPenalty AS penalty , arClaim as arRcvry , arExtKm AS arExtKm"
									+ " from cnmt inner join cnmt_challan on cnmt.cnmtId=cnmt_challan.cnmtId and cnmtDt>=:cnFrmDt and cnmtDt<=:cnToDt and cnmt.bCode= :bCode and cnmt.custCode =:custCode "
									+ " left join challan on cnmt_challan.chlnId=challan.chlnId "
									+ " left join arrivalreport on chlnArId=arId");
					query.setParameter("custCode", custId);
					query.setParameter("bCode", brhId);
					query.setDate("cnFrmDt", fromDt);
					query.setDate("cnToDt", toDt);
					myList = query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP).list();

				} else {
					Query query = session.createSQLQuery("select cnmtCode AS cnmtCode,(select stnName from station where cnmtFromSt=stnCode) AS cnmtFrmStn,(select stnName from station where cnmtToSt=stnCode) AS cnmtToStn,"
									+ " cnmt.bCode AS bCode , (select custName from customer where custCode=cnmtConsignee) AS consignee , (select custName from customer where custCode=cnmtConsignor) AS consignor , cnmtGuaranteeWt AS guaranteeWt , cnmtBillNo AS billNo ,"
									+ " cnmtDt AS cnmtDt,cnmtFreight AS cnmtFrt,bilAmt AS billAmt,lhpvAmt AS lhpvAmt,cnmtCost AS cost,cnmtPl as netMrgn,costPer AS mrgnPr,lhpvRcvry AS rcvry, "
									+ " chlnCode AS chlnCode ,chlnDt AS chlnDt,chlnFreight AS chlnFrt, chlnTotalFreight As chlnToFrt, "
									+ " arDetention AS arDet , arUnloading AS arUnLdng , arOvrHgt AS ovrHt , arPenalty AS penalty , arClaim as arRcvry , arExtKm AS arExtKm"
									+ " from cnmt inner join cnmt_challan on cnmt.cnmtId=cnmt_challan.cnmtId and cnmtDt>=:cnFrmDt and cnmtDt<=:cnToDt and cnmt.bCode= :bCode "
									+ " left join challan on cnmt_challan.chlnId=challan.chlnId "
									+ " left join arrivalreport on chlnArId=arId");
					// query.setParameter("custCode", map.get("custCode"));
					query.setParameter("bCode", brhId);
					query.setDate("cnFrmDt", fromDt);
					query.setDate("cnToDt", toDt);
					myList = query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP).list();

				}

			} else if (isConBrh.equals("con")) {
				if (custId != null && !String.valueOf(custId).equalsIgnoreCase("")) {
					Query query = session.createSQLQuery("select cnmtCode AS cnmtCode,(select stnName from station where cnmtFromSt=stnCode) AS cnmtFrmStn,(select stnName from station where cnmtToSt=stnCode) AS cnmtToStn,"
									+ " cnmt.bCode AS bCode , (select custName from customer where custCode=cnmtConsignee) AS consignee , (select custName from customer where custCode=cnmtConsignor) AS consignor , cnmtGuaranteeWt AS guaranteeWt , cnmtBillNo AS billNo ,"
									+ " cnmtDt AS cnmtDt,cnmtFreight AS cnmtFrt,bilAmt AS billAmt,lhpvAmt AS lhpvAmt,cnmtCost AS cost,cnmtPl as netMrgn,costPer AS mrgnPr,lhpvRcvry AS rcvry, "
									+ " chlnCode AS chlnCode ,chlnDt AS chlnDt,chlnFreight AS chlnFrt, chlnTotalFreight As chlnToFrt, "
									+ " arDetention AS arDet , arUnloading AS arUnLdng , arOvrHgt AS ovrHt , arPenalty AS penalty , arClaim as arRcvry , arExtKm AS arExtKm"
									+ " from cnmt inner join cnmt_challan on cnmt.cnmtId=cnmt_challan.cnmtId and cnmtDt>=:cnFrmDt and cnmtDt<=:cnToDt and  cnmt.custCode =:custCode "
									+ " left join challan on cnmt_challan.chlnId=challan.chlnId "
									+ " left join arrivalreport on chlnArId=arId");
					query.setParameter("custCode", custId);
					query.setDate("cnFrmDt", fromDt);
					query.setDate("cnToDt", toDt);
					myList = query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP).list();

				} else {
					Query query = session.createSQLQuery("select cnmtCode AS cnmtCode,(select stnName from station where cnmtFromSt=stnCode) AS cnmtFrmStn,(select stnName from station where cnmtToSt=stnCode) AS cnmtToStn,"
									+ " cnmt.bCode AS bCode , (select custName from customer where custCode=cnmtConsignee) AS consignee , (select custName from customer where custCode=cnmtConsignor) AS consignor , cnmtGuaranteeWt AS guaranteeWt , cnmtBillNo AS billNo ,"
									+ " cnmtDt AS cnmtDt,cnmtFreight AS cnmtFrt,bilAmt AS billAmt,lhpvAmt AS lhpvAmt,cnmtCost AS cost,cnmtPl as netMrgn,costPer AS mrgnPr,lhpvRcvry AS rcvry, "
									+ " chlnCode AS chlnCode ,chlnDt AS chlnDt,chlnFreight AS chlnFrt, chlnTotalFreight As chlnToFrt, "
									+ " arDetention AS arDet , arUnloading AS arUnLdng , arOvrHgt AS ovrHt , arPenalty AS penalty , arClaim as arRcvry , arExtKm AS arExtKm"
									+ " from cnmt inner join cnmt_challan on cnmt.cnmtId=cnmt_challan.cnmtId and cnmtDt>=:cnFrmDt and cnmtDt<=:cnToDt"
									+ " left join challan on cnmt_challan.chlnId=challan.chlnId "
									+ " left join arrivalreport on chlnArId=arId");
					query.setDate("cnFrmDt", fromDt);
					query.setDate("cnToDt", toDt);
					myList = query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP).list();

				}

			}

		} catch (Exception e) {
			logger.error("UserID = "+user.getUserId()+ " : Exception = "+e);
		}
		logger.info("UserID = "+user.getUserId()+ " : Exit from getHoCostingReport() : ListSize = "+myList.size());
		return myList;
	}
	
	public Boolean isAlreadyNarrtion(Session session, User user, Narration narr){
		logger.info("UserID = "+user.getUserId()+ " : Enter into isAlreadyNarration");
		Boolean isAlready = false;
		
		try{
			List<Narration> list = session.createCriteria(Narration.class)
					.add(Restrictions.eq(NarrationCNTS.NARR_CODE, narr.getNarrCode()))
					.add(Restrictions.eq(NarrationCNTS.NARR_TYPE, narr.getNarrType()))
					.list();
			if(! list.isEmpty())
					isAlready = true;
				list = null;			
		}catch(Exception e){
			logger.error("UserID = "+user.getUserId() + " : Exception = "+e);
		}
		
		logger.info("UserID = "+user.getUserId()+ " : Exit from isAlreadyNarration");
		return isAlready;
	}
	
	@Override
	public Boolean isCodeValid(Session session, User user, Narration narr){
		logger.info("UserID = "+user.getUserId()+ " : Enter into isAlreadyNarration");
		Boolean isValid = false;
		
		try{
			List list = null;
			if(narr.getNarrType().equalsIgnoreCase("cnmt") || narr.getNarrType().equalsIgnoreCase("chln")){
				list = session.createCriteria(BranchSStats.class)
						.add(Restrictions.eq(BranchSStatsCNTS.BRS_STATUS_TYPE, narr.getNarrType()))
						.add(Restrictions.eq(BranchSStatsCNTS.BRS_SERIAL_NO, narr.getNarrCode()))
						.list();
			}else{
				list = session.createCriteria(ArrivalReport.class)						
						.add(Restrictions.eq(ArrivalReportCNTS.AR_CODE, narr.getNarrCode()))
						.list();
			}
			
			if(! list.isEmpty())			
				isValid = true;
			
		}catch(Exception e){
			logger.error("UserID = "+user.getUserId() + " : Exception = "+e);
		}
		
		logger.info("UserID = "+user.getUserId()+ " : Exit from isAlreadyNarration");
		return isValid;
	}
	
	public Integer saveNarration(Session session, User user, Narration narr){
		logger.info("UserID = "+user.getUserId()+ " : Enter into saveNarrtion");
		Integer narrId = 0;
		
		try{
			narrId = (Integer) session.save(narr);			
		}catch(Exception e){
			logger.error("UserID = "+user.getUserId() + " : Exception = "+e);
		}
		
		logger.info("UserID = "+user.getUserId()+ " : Exit from saveNarrtion");
		return narrId;
	}
	
	@Override
	public List<String> getCnmtByCustCodes(Session session, User user, List<String> custCodes, String billType){
		logger.info("UserID = "+user.getUserId() + " : Enter into getCnmtByGroup() : BillType = "+billType + " : CustCodes = "+custCodes);
		List<String> cnmtCodes = null;
		try{
			Criteria cr = session.createCriteria(Cnmt.class)
					.add(Restrictions.in(CnmtCNTS.CUST_CODE, custCodes))					
					.add(Restrictions.eq(CnmtCNTS.CNMT_BILL_AT, user.getUserBranchCode()))
					.setProjection(Projections.projectionList()
							.add(Projections.property(CnmtCNTS.CNMT_CODE))
							);
			if(billType.equalsIgnoreCase("NBill"))
				cr.add(Restrictions.isNull(CnmtCNTS.CNMT_BILL_NO));
			else if(billType.equalsIgnoreCase("SBill"))
				cr.add(Restrictions.isNotNull(CnmtCNTS.CNMT_BILL_NO));
			
			cnmtCodes = cr.list();
		}catch(Exception e){
			logger.error("UserID = "+user.getUserId() + " : Exception = "+e);
		}
		logger.info("UserID = "+user.getUserId() + " : Exit from getCnmtByGroup : CnmtSize = "+cnmtCodes.size());
		return cnmtCodes;
	}
	
	@Override
	public Map<String, String> getStationMap(List<String> stationCodes) {
		Map<String, String> stationMap = new HashMap<>();
		Session session = sessionFactory.openSession();
		try {
			List<Station> stationList = session.createCriteria(Station.class)
					.add(Restrictions.in(StationCNTS.STN_CODE, stationCodes))
					.setProjection(Projections.projectionList()
							.add(Projections.property(StationCNTS.STN_CODE), StationCNTS.STN_CODE)
							.add(Projections.property(StationCNTS.STN_NAME), StationCNTS.STN_NAME))
					.setResultTransformer(Transformers.aliasToBean(Station.class))
					.list();
			
			for (Station stn : stationList) 
				stationMap.put(stn.getStnCode(), stn.getStnName());
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		} finally {
			session.close();
		}
		return stationMap;
	}
	
	@Override
	public Map<String, String> getCustomerMap(List<String> customerCodes) {
		Map<String, String> customerMap = new HashMap<>();
		Session session = sessionFactory.openSession();
		try {
			List<Customer> customerList = session.createCriteria(Customer.class)
					.add(Restrictions.in(CustomerCNTS.CUST_CODE, customerCodes))
					.setProjection(Projections.projectionList()
							.add(Projections.property(CustomerCNTS.CUST_CODE), CustomerCNTS.CUST_CODE)
							.add(Projections.property(CustomerCNTS.CUST_NAME), CustomerCNTS.CUST_NAME))
							.setResultTransformer(Transformers.aliasToBean(Customer.class))
							.list();
			for(Customer customer : customerList)
				customerMap.put(customer.getCustCode(), customer.getCustName());
							
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		} finally {
			session.close();
		}
		return customerMap;
	}
		
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Map<String, Object>> getRelCnData(Map<String, Object> clientMap) {
	  Session session= this.sessionFactory.openSession();
	  List<Map<String, Object>> list = new ArrayList<>();
	  System.out.println("cnNo: " + clientMap.get("cnNo"));

		
	  if(session!=null)
	  {
		  
		  Query query=  session.createSQLQuery("select cn.cnmtId as cnmtId,ch.chlnLryNo as lryNo," +
		  		" cn.cnmtCode as cnmtCode, DATE_FORMAT(cn.cnmtDt,'%d.%m.%Y')as cnmtDt,ar.arPkg as pkg," +
		  		" DATE_FORMAT(ar.arDt,'%d.%m.%Y')as dlvryDt,cn.cnmtRate as rate,cn.cnmtFreight as cnmtFreight," +
		  		" (ar.arDetention + ar.arUnloading + ar.arOvrHgt + ar.arPenalty) as claim " +
		  		" from cnmt cn " +
		  		" left join cnmt_challan cc on cn.cnmtId=cc.cnmtId" +
		  		" left join challan ch on cc.chlnId=ch.chlnId" +
		  		" left join arrivalreport ar on ch.chlnArId=ar.arId" +
		  		" where cn.isCancel=0 and ch.isCancel=0 and cnmtCode = :cnNo");
	  
		  query.setParameter("cnNo",  clientMap.get("cnNo"));
	  list=query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP).list();
	  session.close();
	  }
		return list;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public int addCnmtImgN(int cnmtId ,int chlnId, Blob blob, String imagePath){
		System.out.println("enter into addCnmtImg function");
		logger.info("Enter in addCnmtImg()...  - CNMT ID : "+cnmtId);
		int result = 0;
		User currentUser = (User) httpSession.getAttribute("currentUser");
		List<Cnmt> cnmtList = new ArrayList<Cnmt>();
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			/*Criteria cr=session.createCriteria(Cnmt.class);
			cr.add(Restrictions.eq(CnmtCNTS.CNMT_ID,cnmtId));
			cnmtList = cr.list();*/
			Cnmt cnmt = (Cnmt) session.get(Cnmt.class, cnmtId);
			//if(!cnmtList.isEmpty()){
			if(cnmt != null){	
				//Cnmt cnmt = cnmtList.get(0);
				logger.info("CNMT Found - ID : "+cnmt.getCnmtId());
				int cmId = cnmt.getCmId();
				try{						
					imagePath = imagePath+"/CNMT"+cnmt.getCnmtId()+"_CHLN"+chlnId+".pdf";
					File file = new File(imagePath);
					if(file.exists())
						file.delete();
					if(file.createNewFile()){
						
						byte imageInBytes[] = blob.getBytes(1, (int)blob.length());							
						OutputStream targetFile=  new FileOutputStream(file);
			            targetFile.write(imageInBytes);			            
			            targetFile.close();
			            HashMap<Integer, String> arImgMap=new HashMap<>();
			            arImgMap.put(chlnId, imagePath);
			            
						if(cmId > 0){
							logger.info("CNMT Image Already ID : "+cmId);
							CnmtImage cnmtImage = (CnmtImage) session.get(CnmtImage.class, cmId);
							if(cnmtImage.getArImgPath()!=null)
								arImgMap.putAll(cnmtImage.getArImgPath());
							cnmtImage.setArImgPath(arImgMap);
							//cnmtImage.setCnmtImgPath(imagePath);
							session.merge(cnmtImage);
						}else{								
							CnmtImage cnmtImage = new CnmtImage();
							cnmtImage.setUserCode(currentUser.getUserCode());
							cnmtImage.setbCode(currentUser.getUserBranchCode());
							cnmtImage.setCnmtId(cnmtId);
							cnmtImage.setCnmtImgPath(imagePath);
							cnmtImage.setArImgPath(arImgMap);
							int cnImgId = (Integer) session.save(cnmtImage);
							logger.info("CNMT Image New ID : "+cnImgId);
							cnmt.setCmId(cnImgId);
							session.merge(cnmt);
						}
						result = 1;
					}
				}catch(Exception e){
					logger.error("Error in addCnmtImg() : "+e);
					System.out.println("Error in addCnmtImg() : "+e);
					result = -1;
				}					
				/*cnmt.setCnmtImage(blob);
				session.merge(cnmt);*/
			}else{
				logger.info("CNMT is not found !");
			}
			transaction.commit();
			//session.flush();				
		}catch(Exception e){
			e.printStackTrace();
			result = -1;
		}
		session.clear();
		session.close();
		return result;
	}

	
	@Override
	public Cnmt getCnmtFrFndAlctn(String cnmtCode) {
		Cnmt cnmt=null;
		System.out.print("Cnmt"+cnmtCode);
		Session session=this.sessionFactory.openSession();
		try{
			List<Cnmt> codeList = session.createCriteria(Cnmt.class)
			.add(Restrictions.eq(CnmtCNTS.CNMT_CODE, cnmtCode))
			.add(Restrictions.eq(CnmtCNTS.CNMT_CANCEL, false)).list();
			System.out.print("size"+codeList.size());
			if(!codeList.isEmpty()) {
				cnmt=codeList.get(0);
			}
			
		}catch(Exception e){
			logger.error(" : Exception in getCodeByCode()= "+e);
		}finally {
			session.clear();
			session.close();
		}
		
		return cnmt;
	}
	

	@Override
	public int saveCnmt(Cnmt cnmt,Session session) {
		return (int) session.save(cnmt);
	}
	
	
	@Override
	public Cnmt getCnmtByVeId(int vEId) {
		Cnmt cnmt=null;
		System.out.print("Cnmt"+vEId);
		Session session=this.sessionFactory.openSession();
		try{
			List<Cnmt> codeList = session.createCriteria(Cnmt.class)
			.add(Restrictions.eq("vEId", vEId)).list();
			System.out.print("size"+codeList.size());
			if(!codeList.isEmpty()) {
				cnmt=codeList.get(0);
			}
			
		}catch(Exception e){
			logger.error(" : Exception in getCodeByCode()= "+e);
		}finally {
			session.clear();
			session.close();
		}
		return cnmt;
	}
	
	
}