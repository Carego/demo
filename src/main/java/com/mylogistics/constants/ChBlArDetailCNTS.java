package com.mylogistics.constants;

public class ChBlArDetailCNTS {
	
	public static final String CBAD_ID = "chBlArId";
	
	public static final String CBAD_CODE = "chBlArCode";
	
	public static final String CBAD_SO_CODE = "chBlArstOdrCode";
	
	public static final String CBAD_SNO= "chBlArStartNo";
	
	public static final String CBAD_NOB =  "chBlArNOB";
	
	public static final String CBAD_ENO =  "chBlArEndNo";
	
	public static final String CBAD_STATUS =  "chBlArStatus";
	
	public static final String USER_CODE = "userCode";
	
	public static final String USER_BRANCH_CODE = "bCode";
	
	public static final String CREATION_TS = "creationTS";
}
