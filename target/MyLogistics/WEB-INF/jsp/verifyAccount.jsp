<div class="row" >
<div class="col s4">
	       				<input class="validate" type ="button" value="Owner" ng-click="getOwnerList()" >
	       		</div>
	       		
	       		<div class="col s4">
	       				<input class="validate" type ="button" value="Broker" ng-click="getBrokerList()" >
	       		</div>
</div>
<div class="row" ng-show="brokerDB">
		<table class="tblrow" >
       		
       		<caption class="coltag tblrow">Account For Verification</caption> 
       		
            <tr class="rowclr">
            	<th class="colclr">S.No</th>
            	<th class="colclr">Broker Code</th>
            	<th class="colclr">Status</th>
            	<th class="colclr">Mobile No</th>
            	<th class="colclr">Broker Name</th>
            	<th class="colclr">Broker PAN Name</th>
            	<th class="colclr">Account Holder Name</th>
            	<th class="colclr">Bank Name</th>
            	<th class="colclr">IFSC Code</th>
            	<th class="colclr">Account No.</th>
            	<th class="colclr">View Image</th>
            	<th class="colclr">Valid</th>
            	<th class="colclr">Invalid</th>
            	 </tr>	
	         
	        <tr ng-repeat="brk in brkList">
	        	<td class="rowcel">{{$index+1}}</td>
	        	<td class="rowcel">{{brk.brkCode}}</td>
	        	<td class="rowcel">{{brk.brkFirmType}}</td>
	        	<td class="rowcel">{{brk.brkPhNoList}}</td>
	        	<td class="rowcel">{{brk.brkName}}</td>
	        	<td class="rowcel">{{brk.brkPanName}}</td>
	        	<td class="rowcel">{{brk.brkAcntHldrName}}</td>
	        	<td class="rowcel">{{brk.brkBnkBranch}}</td>
	        	<td class="rowcel">{{brk.brkIfsc}}</td>
	        	<td class="rowcel">{{brk.brkAccntNo}}</td>
<!-- 	        	<td class="rowcel">{{$index+1}}</td> -->
 			<td class="rowcel" >
	            	<a class="btn-floating suffix waves-effect teal" type="button" ng-click="showImage(brk)" >
						<i class="mdi-action-visibility white-text"></i>
					</a>
				</td>
				
				<td class="rowcel">
	            	<a class="btn-floating suffix waves-effect teal" type="button" ng-click="validBnkDet(brk)" >
						<i class="mdi-content-send white-text"></i>
					</a>
				</td>
				
				<td class="rowcel">
	            	<a class="btn-floating suffix waves-effect teal" type="button" ng-click="invalidBnkDet(brk)" >
						<i class="mdi-content-send white-text"></i>
					</a>
				</td>
				
			</tr>
	</table>
          
	</div>
	
	
	
	<div class="row" ng-show="ownerDB">
		<table class="tblrow" >
       		
       		<caption class="coltag tblrow">Account For Verification</caption> 
       		
            <tr class="rowclr">
            	<th class="colclr">S.No</th>
            	<th class="colclr">Owner Code</th>
            	<th class="colclr">Mobile No</th>
            	<th class="colclr">Owner Name</th>
            	<th class="colclr">Owner PAN Name</th>
            	<th class="colclr">Account Holder Name</th>
            	<th class="colclr">Bank Name</th>
            	<th class="colclr">IFSC Code</th>
            	<th class="colclr">Account No.</th>
            	<th class="colclr">View Image</th>
            	<th class="colclr">Valid</th>
            	<th class="colclr">Invalid</th>
            	 </tr>	
	         
	        <tr ng-repeat="own in ownList">
	        	<td class="rowcel">{{$index+1}}</td>
	        	<td class="rowcel">{{own.ownCode}}</td>
	        	<td class="rowcel">{{own.ownPhNoList}}</td>
	        	<td class="rowcel">{{own.ownName}}</td>
	        	<td class="rowcel">{{own.ownPanName}}</td>
	        	<td class="rowcel">{{own.ownAcntHldrName}}</td>
	        	<td class="rowcel">{{own.ownBnkBranch}}</td>
	        	<td class="rowcel">{{own.ownIfsc}}</td>
	        	<td class="rowcel">{{own.ownAccntNo}}</td>
<!-- 	        	<td class="rowcel">{{$index+1}}</td> -->
 			<td class="rowcel" >
	            	<a class="btn-floating suffix waves-effect teal" type="button" ng-click="showOwnImage(own)" >
						<i class="mdi-action-visibility white-text"></i>
					</a>
				</td>
				
				<td class="rowcel">
	            	<a class="btn-floating suffix waves-effect teal" type="button" ng-click="validOwnBnkDet(own)" >
						<i class="mdi-content-send white-text"></i>
					</a>
				</td>
				
				<td class="rowcel">
	            	<a class="btn-floating suffix waves-effect teal" type="button" ng-click="invalidOwnBnkDet(own)" >
						<i class="mdi-content-send white-text"></i>
					</a>
				</td>
				
			</tr>
	</table>
          
	</div>
	