package com.mylogistics.services;

import java.sql.Date;
import java.util.List;

public class DispatchDetailService {
	
	private String dispatchCode;
	private List<String> cnmtStartNoList;
	private List<String> chlnStartNoList;
	private List<String> sedrStartNoList;
	private Date dispatchDate; 
	private Date receiveDate;
	private String invoiceNo;
	private String branchCode;
	
	
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getDispatchCode() {
		return dispatchCode;
	}
	public void setDispatchCode(String dispatchCode) {
		this.dispatchCode = dispatchCode;
	}
	public List<String> getCnmtStartNoList() {
		return cnmtStartNoList;
	}
	public void setCnmtStartNoList(List<String> cnmtStartNoList) {
		this.cnmtStartNoList = cnmtStartNoList;
	}
	public List<String> getChlnStartNoList() {
		return chlnStartNoList;
	}
	public void setChlnStartNoList(List<String> chlnStartNoList) {
		this.chlnStartNoList = chlnStartNoList;
	}
	public List<String> getSedrStartNoList() {
		return sedrStartNoList;
	}
	public void setSedrStartNoList(List<String> sedrStartNoList) {
		this.sedrStartNoList = sedrStartNoList;
	}
	public Date getDispatchDate() {
		return dispatchDate;
	}
	public void setDispatchDate(Date dispatchDate) {
		this.dispatchDate = dispatchDate;
	}
	public Date getReceiveDate() {
		return receiveDate;
	}
	public void setReceiveDate(Date receiveDate) {
		this.receiveDate = receiveDate;
	}
	public String getInvoiceNo() {
		return invoiceNo;
	}
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	
	
}
