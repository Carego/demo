package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.UserRightsDAO;
import com.mylogistics.constants.UserRightsCNTS;
import com.mylogistics.model.UserRights;

public class UserRightsDAOImpl implements UserRightsDAO{
	
	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	public UserRightsDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}	
	
	@SuppressWarnings("unchecked")
	@Transactional
	public UserRights getUserRights(int userId){
		System.out.println("enter into getUserRights function");
		 UserRights userRights = new UserRights();
		 List<UserRights> urList = new ArrayList<>();
		 try{
			  session = this.sessionFactory.openSession();
			  transaction =  session.beginTransaction();
			  Criteria cr=session.createCriteria(UserRights.class);
			  cr.add(Restrictions.eq(UserRightsCNTS.USER_ID,userId));
			  urList = cr.list();
			  if(!urList.isEmpty()){
				  userRights = urList.get(0);
			  }else{
				  userRights = null;
			  }
			  session.flush();
		}catch(Exception e){
			e.printStackTrace();
			userRights = null;
		}
		 session.clear();
		session.close();
		return userRights;
	}
	
	
	@SuppressWarnings("unchecked")
	@Transactional
	public int saveUserRights(UserRights userRights){
		System.out.println("enter into saveUserRights function = "+userRights.getUrId());
		System.out.println("enter into saveUserRights function = "+userRights.isUrFT());
		int urId = 0;
		try{
			 session = this.sessionFactory.openSession();
			 transaction =  session.beginTransaction();
			 session.saveOrUpdate(userRights);
			 urId = 1;
			 transaction.commit();
			 session.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return urId;
	}
}
