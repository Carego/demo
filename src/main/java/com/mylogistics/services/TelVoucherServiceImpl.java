package com.mylogistics.services;

import java.util.ArrayList;
import java.util.List;

public class TelVoucherServiceImpl implements TelVoucherService{

	private List<TelM_SubTelMService> tmstmList = new ArrayList<TelM_SubTelMService>();

	public void addTmAndSTm(TelM_SubTelMService telM_SubTelMService){
		tmstmList.add(telM_SubTelMService);
	}
	
	public List<TelM_SubTelMService> getAllTmAndSTm(){
		return tmstmList;
	}
	
	public void deleteAllTmAndSTm(){
		tmstmList.clear();
	}
	
	public void remove(int index){
		tmstmList.remove(index);
	}
}
