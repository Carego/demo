package com.mylogistics.services;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.mylogistics.model.OwnerMaster;

public class OwnerMasterService {
	
	private int indentId;
	
	private List<OwnerMaster> brkList;

	public int getIndentId() {
		return indentId;
	}

	public void setIndentId(int indentId) {
		this.indentId = indentId;
	}

	public List<OwnerMaster> getBrkList() {
		return brkList;
	}

	public void setBrkList(List<OwnerMaster> brkList) {
		this.brkList = brkList;
	}
	
	
	
		/*private int Master_Id;
		
		@Column(name="Log_Id",unique=true)
		private int Log_Id;
		
		private long Phone_No;
		
		private String Branch;
		
		private String Selected_States_of_North;
		
		private String Selected_States_of_South;
		
		private String Selected_States_of_Central;
		
		private String Selected_States_of_West;
		
		private String Selected_States_of_East;
		
		private String Selected_States_of_NorthEast;
		
		private String Other_Nations;
		
		private String No_of_Chakka_per_Vehicle;
		
		private String Type_of_Truck;
		
		private String Type_of_Container;
		
		private String Type_of_Trailor_and_Platform;
		
		private String Type_of_Vendor;
		
		private String Number_of_Owned_Vehicles;
		
		private String Number_of_Vehicles_in_Group;
		
		private String Number_of_Persons_in_Group;
		
		private String Company_Name;
		
		private String Company_Owner_Name;
		
		private String Company_Owner_Mobile_1;
		
		private String Company_Owner_Mobile_2;
		
		private String Company_Office_Address;

		public int getLog_Id() {
			return Log_Id;
		}

		public void setLog_Id(int log_Id) {
			Log_Id = log_Id;
		}

		public int getMaster_Id() {
			return Master_Id;
		}

		public void setMaster_Id(int master_Id) {
			Master_Id = master_Id;
		}

		public long getPhoneNo() {
			return Phone_No;
		}

		public void setPhoneNo(long phoneNo) {
			Phone_No = phoneNo;
		}

		public String getBranch() {
			return Branch;
		}

		public void setBranch(String branch) {
			Branch = branch;
		}

		public String getSelected_States_of_North() {
			return Selected_States_of_North;
		}

		public void setSelected_States_of_North(String selected_States_of_North) {
			Selected_States_of_North = selected_States_of_North;
		}

		public String getSelected_States_of_South() {
			return Selected_States_of_South;
		}

		public void setSelected_States_of_South(String selected_States_of_South) {
			Selected_States_of_South = selected_States_of_South;
		}

		public String getSelected_States_of_Central() {
			return Selected_States_of_Central;
		}

		public void setSelected_States_of_Central(String selected_States_of_Central) {
			Selected_States_of_Central = selected_States_of_Central;
		}

		public String getSelected_States_of_West() {
			return Selected_States_of_West;
		}

		public void setSelected_States_of_West(String selected_States_of_West) {
			Selected_States_of_West = selected_States_of_West;
		}

		public String getSelected_States_of_East() {
			return Selected_States_of_East;
		}

		public void setSelected_States_of_East(String selected_States_of_East) {
			Selected_States_of_East = selected_States_of_East;
		}

		public String getSelected_States_of_NorthEast() {
			return Selected_States_of_NorthEast;
		}

		public void setSelected_States_of_NorthEast(String selected_States_of_NorthEast) {
			Selected_States_of_NorthEast = selected_States_of_NorthEast;
		}

		public String getOther_Nations() {
			return Other_Nations;
		}

		public void setOther_Nations(String other_Nations) {
			Other_Nations = other_Nations;
		}

		public String getNo_of_Chakka_per_Vehicle() {
			return No_of_Chakka_per_Vehicle;
		}

		public void setNo_of_Chakka_per_Vehicle(String no_of_Chakka_per_Vehicle) {
			No_of_Chakka_per_Vehicle = no_of_Chakka_per_Vehicle;
		}

		public String getType_of_Truck() {
			return Type_of_Truck;
		}

		public void setType_of_Truck(String type_of_Truck) {
			Type_of_Truck = type_of_Truck;
		}

		public String getType_of_Container() {
			return Type_of_Container;
		}

		public void setType_of_Container(String type_of_Container) {
			Type_of_Container = type_of_Container;
		}

		public String getType_of_Trailor_and_Platform() {
			return Type_of_Trailor_and_Platform;
		}

		public void setType_of_Trailor_and_Platform(String type_of_Trailor_and_Platform) {
			Type_of_Trailor_and_Platform = type_of_Trailor_and_Platform;
		}

		public String getType_of_Vendor() {
			return Type_of_Vendor;
		}

		public void setType_of_Vendor(String type_of_Vendor) {
			Type_of_Vendor = type_of_Vendor;
		}

		public String getNumber_of_Owned_Vehicles() {
			return Number_of_Owned_Vehicles;
		}

		public void setNumber_of_Owned_Vehicles(String number_of_Owned_Vehicles) {
			Number_of_Owned_Vehicles = number_of_Owned_Vehicles;
		}

		public String getNumber_of_Vehicles_in_Group() {
			return Number_of_Vehicles_in_Group;
		}

		public void setNumber_of_Vehicles_in_Group(String number_of_Vehicles_in_Group) {
			Number_of_Vehicles_in_Group = number_of_Vehicles_in_Group;
		}

		public String getNumber_of_Persons_in_Group() {
			return Number_of_Persons_in_Group;
		}

		public void setNumber_of_Persons_in_Group(String number_of_Persons_in_Group) {
			Number_of_Persons_in_Group = number_of_Persons_in_Group;
		}

		public String getCompany_Name() {
			return Company_Name;
		}

		public void setCompany_Name(String company_Name) {
			Company_Name = company_Name;
		}

		public String getCompany_Owner_Name() {
			return Company_Owner_Name;
		}

		public void setCompany_Owner_Name(String company_Owner_Name) {
			Company_Owner_Name = company_Owner_Name;
		}

		public String getCompany_Owner_Mobile_1() {
			return Company_Owner_Mobile_1;
		}

		public void setCompany_Owner_Mobile_1(String company_Owner_Mobile_1) {
			Company_Owner_Mobile_1 = company_Owner_Mobile_1;
		}

		public String getCompany_Owner_Mobile_2() {
			return Company_Owner_Mobile_2;
		}

		public void setCompany_Owner_Mobile_2(String company_Owner_Mobile_2) {
			Company_Owner_Mobile_2 = company_Owner_Mobile_2;
		}

		public String getCompany_Office_Address() {
			return Company_Office_Address;
		}

		public void setCompany_Office_Address(String company_Office_Address) {
			Company_Office_Address = company_Office_Address;
		}*/
		
		

}
