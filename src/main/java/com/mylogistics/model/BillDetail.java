package com.mylogistics.model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "billdetail")
public class BillDetail {

	@Id
	@Column(name="bdId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer bdId;
	
	@Column(name="bdSupBlRsn")
	@NotNull(message = "Sup. Reason can not be null !", groups = BillDetailSup.class)
	private String bdSupBlRsn;
	
	@Column(name="bdLryNo")
	@NotNull(message = "Bill Detail Lorry no. can not be null !", groups = {BillDetailNormal.class, BillDetailSup.class})
	@NotEmpty(message = "Bill Detail Lorry no. can not be empty !", groups = {BillDetailNormal.class, BillDetailSup.class})
	private String bdLryNo;
	
	@Column(name="bdBlBase")
	private String bdBlBase;
	
	@Column(name="bdCnmtId")
	@Min(value = 1, message = "Bill Detail Cnmt ID should be greater than 0 !", groups = {BillDetailNormal.class, BillDetailSup.class}) 
	private int bdCnmtId;
	
	@Column(name="bdLoadAmt" , columnDefinition="double default 0.0")
	private double bdLoadAmt;
	
	@Column(name="bdUnloadAmt" ,columnDefinition="double default 0.0")
	private double bdUnloadAmt;
	
	@Column(name="bdDetAmt" ,columnDefinition="double default 0.0")
	private double bdDetAmt;
	
	@Column(name="bdBonusAmt" ,columnDefinition="double default 0.0")
	private double bdBonusAmt;
	
	@Column(name="bdOthChgList" , columnDefinition="longblob")
	private ArrayList<Map<String,Object>> bdOthChgList = new ArrayList<>();
	
	@Column(name="bdOthChgAmt" ,columnDefinition="double default 0.0")
	private double bdOthChgAmt;
	
	@Column(name="bdCnmtAdv" ,columnDefinition="double default 0.0")
	private double bdCnmtAdv;
	
	@Column(name="bdTollTax" ,columnDefinition="double default 0.0")
	private double bdTollTax;
	
	@Column(name="bdRtoChallan" ,columnDefinition="double default 0.0")
	private Double bdRtoChallan;
	
	@Column(name="bdUnloadDetAmt" ,columnDefinition="double default 0.0")
	private double bdUnloadDetAmt;
	
	@Column(name="bdChgWt" ,columnDefinition="double default 0.0")
	@Min(value = 1, message = "Charge Weight should be greater than 0 !", groups = {BillDetailNormal.class})
	private double bdChgWt;
	
	@Column(name="bdActWt" ,columnDefinition="double default 0.0")
	@Min(value = 1, message = "Actual Weight should be greater than 0 !", groups = {BillDetailNormal.class})
	private double bdActWt;
	
	@Column(name="bdRecWt" ,columnDefinition="double default 0.0")
	//@Min(value = 1, message = "Receive Weight should be greater than 0 !", groups = {BillDetailNormal.class})
	private double bdRecWt;
	
	@Column(name="bdRate" ,columnDefinition="double default 0.0")
	//@Min(value = 0, message = "Rate should be greater than 0 !", groups = {BillDetailNormal.class, BillDetailSup.class})
	private double bdRate;
	
	@Column(name="bdFreight" ,columnDefinition="double default 0.0")
	@Min(value = 1, message = "Freight should be greater than 0 !", groups = {BillDetailNormal.class})	
	private double bdFreight;
	
	@Column(name="bdTotAmt" ,columnDefinition="double default 0.0")
	@Min(value = 1, message = "Total amount should be greater than 0 !", groups = {BillDetailNormal.class, BillDetailSup.class})
	private double bdTotAmt;
	
	@Column(name="bdEntryShNum")
	private String bdEntryShNum;
	
	@Column(name="bdShipmentNum")
	private String bdShipmentNum; 
	
	@Column(name="bdObdNum")
	private String bdObdNum;
	
	@Column(name="bdsalesOdrNum")
	private String bdsalesOdrNum;
	
	@Column(name="bdPoNum")
	private String bdPoNum;
	//Many To One with Bill
	@ManyToOne
	@LazyCollection(LazyCollectionOption.TRUE)
	@JsonBackReference(value = "billBDet")
	@JoinColumn(name = "blId") 
	@NotNull(message = "Bill can not be null for Bill Detail !", groups = {BillDetailNormal.class, BillDetailSup.class})
	private Bill bill;
		
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;
	
	@Column(name="bCode")
	@NotNull(message = "BCode can not be null !", groups = {BillDetailNormal.class, BillDetailSup.class})
	@NotEmpty(message = "BCode can not be empty !", groups = {BillDetailNormal.class, BillDetailSup.class})
	private String bCode;
	
	@Column(name="userCode")
	@NotNull(message = "User Code can not be null !", groups = {BillDetailNormal.class, BillDetailSup.class})
	@NotEmpty(message = "User Code can not be empty !", groups = {BillDetailNormal.class, BillDetailSup.class})
	private String userCode;

		
	public Integer getBdId() {
		return bdId;
	}

	public void setBdId(Integer bdId) {
		this.bdId = bdId;
	}

	public String getBdLryNo() {
		return bdLryNo;
	}

	public void setBdLryNo(String bdLryNo) {
		this.bdLryNo = bdLryNo;
	}

	public int getBdCnmtId() {
		return bdCnmtId;
	}

	public void setBdCnmtId(int bdCnmtId) {
		this.bdCnmtId = bdCnmtId;
	}

	public double getBdLoadAmt() {
		return bdLoadAmt;
	}

	public void setBdLoadAmt(double bdLoadAmt) {
		this.bdLoadAmt = bdLoadAmt;
	}

	public double getBdUnloadAmt() {
		return bdUnloadAmt;
	}

	public void setBdUnloadAmt(double bdUnloadAmt) {
		this.bdUnloadAmt = bdUnloadAmt;
	}

	public double getBdDetAmt() {
		return bdDetAmt;
	}

	public void setBdDetAmt(double bdDetAmt) {
		this.bdDetAmt = bdDetAmt;
	}

	public double getBdBonusAmt() {
		return bdBonusAmt;
	}

	public void setBdBonusAmt(double bdBonusAmt) {
		this.bdBonusAmt = bdBonusAmt;
	}

	public ArrayList<Map<String, Object>> getBdOthChgList() {
		return bdOthChgList;
	}

	public void setBdOthChgList(ArrayList<Map<String, Object>> bdOthChgList) {
		this.bdOthChgList = bdOthChgList;
	}

	public double getBdCnmtAdv() {
		return bdCnmtAdv;
	}

	public void setBdCnmtAdv(double bdCnmtAdv) {
		this.bdCnmtAdv = bdCnmtAdv;
	}

	public double getBdChgWt() {
		return bdChgWt;
	}

	public void setBdChgWt(double bdChgWt) {
		this.bdChgWt = bdChgWt;
	}

	public double getBdActWt() {
		return bdActWt;
	}

	public void setBdActWt(double bdActWt) {
		this.bdActWt = bdActWt;
	}

	public double getBdRecWt() {
		return bdRecWt;
	}

	public void setBdRecWt(double bdRecWt) {
		this.bdRecWt = bdRecWt;
	}

	public double getBdRate() {
		return bdRate;
	}

	public void setBdRate(double bdRate) {
		this.bdRate = bdRate;
	}

	public double getBdFreight() {
		return bdFreight;
	}

	public void setBdFreight(double bdFreight) {
		this.bdFreight = bdFreight;
	}

	public double getBdTotAmt() {
		return bdTotAmt;
	}

	public void setBdTotAmt(double bdTotAmt) {
		this.bdTotAmt = bdTotAmt;
	}

	public Bill getBill() {
		return bill;
	}

	public void setBill(Bill bill) {
		this.bill = bill;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public double getBdOthChgAmt() {
		return bdOthChgAmt;
	}

	public void setBdOthChgAmt(double bdOthChgAmt) {
		this.bdOthChgAmt = bdOthChgAmt;
	}

	public String getBdBlBase() {
		return bdBlBase;
	}

	public void setBdBlBase(String bdBlBase) {
		this.bdBlBase = bdBlBase;
	}

	public String getBdSupBlRsn() {
		return bdSupBlRsn;
	}

	public void setBdSupBlRsn(String bdSupBlRsn) {
		this.bdSupBlRsn = bdSupBlRsn;
	}

	public String getBdEntryShNum() {
		return bdEntryShNum;
	}

	public void setBdEntryShNum(String bdEntryShNum) {
		this.bdEntryShNum = bdEntryShNum;
	}

	public String getBdShipmentNum() {
		return bdShipmentNum;
	}

	public void setBdShipmentNum(String bdShipmentNum) {
		this.bdShipmentNum = bdShipmentNum;
	}

	public String getBdObdNum() {
		return bdObdNum;
	}

	public void setBdObdNum(String bdObdNum) {
		this.bdObdNum = bdObdNum;
	}

	public String getBdsalesOdrNum() {
		return bdsalesOdrNum;
	}

	public void setBdsalesOdrNum(String bdsalesOdrNum) {
		this.bdsalesOdrNum = bdsalesOdrNum;
	}

	public String getBdPoNum() {
		return bdPoNum;
	}

	public void setBdPoNum(String bdPoNum) {
		this.bdPoNum = bdPoNum;
	}

	public interface BillDetailNormal{		
	}
	public interface BillDetailSup{		
	}
	public double getBdUnloadDetAmt() {
		return bdUnloadDetAmt;
	}

	public void setBdUnloadDetAmt(double bdUnloadDetAmt) {
		this.bdUnloadDetAmt = bdUnloadDetAmt;
	}

	public double getBdTollTax() {
		return bdTollTax;
	}

	public void setBdTollTax(double bdTollTax) {
		this.bdTollTax = bdTollTax;
	}

	public Double getBdRtoChallan() {
		return bdRtoChallan;
	}

	public void setBdRtoChallan(Double bdRtoChallan) {
		this.bdRtoChallan = bdRtoChallan;
	}


}
