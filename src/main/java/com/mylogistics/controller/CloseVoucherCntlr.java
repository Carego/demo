package com.mylogistics.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.CashStmtDAO;
import com.mylogistics.DAO.CashStmtStatusDAO;
import com.mylogistics.DAO.CashStmtTempDAO;
import com.mylogistics.DAO.FaMasterDAO;
import com.mylogistics.DAO.LhpvStatusDAO;
import com.mylogistics.model.BankCS;
import com.mylogistics.model.Branch;
import com.mylogistics.model.CashStmt;
import com.mylogistics.model.CashStmtStatus;
import com.mylogistics.model.CashStmtTemp;
import com.mylogistics.model.FAMaster;
import com.mylogistics.model.User;
import com.mylogistics.model.lhpv.LhpvStatus;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.VoucherService;

@Controller
public class CloseVoucherCntlr {

	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private BranchDAO branchDAO;
	
	@Autowired
	private CashStmtStatusDAO cashStmtStatusDAO;
	
	@Autowired
	private CashStmtDAO cashStmtDAO;
	
	@Autowired
	private CashStmtTempDAO cashStmtTempDAO;
	
	@Autowired
	private FaMasterDAO faMasterDAO;
	
	@Autowired
	private LhpvStatusDAO lhpvStatusDAO;
	
	@Autowired
	private SessionFactory sessionFactory;

	
	public static Logger logger = Logger.getLogger(CloseVoucherCntlr.class);

	
	
	@RequestMapping(value = "/getVoucherDetFrCls" , method = RequestMethod.POST)  
	public @ResponseBody Object getVoucherDetFrCls() {  
		System.out.println("Enter into getVoucherDetFrCls---->");
		Map<String, Object> map = new HashMap<>();	
		User user = (User) httpSession.getAttribute("currentUser");
		if(user != null){
			String branchCode = user.getUserBranchCode();
			System.out.println("branchCode = "+branchCode);
			List<Branch> branchList = branchDAO.retrieveBranch(branchCode);
			if(!branchList.isEmpty()){
				CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranch(branchCode);
				
				if(cashStmtStatus != null){
					System.out.println("Enter into If----->>>>");
					map.put(ConstantsValues.BRANCH, branchList.get(0));
					map.put(ConstantsValues.CASH_STMT_STATUS, cashStmtStatus);
					map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				}else{
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}

		return map;
	}

	
	
	
	@RequestMapping(value = "/getPendingCS" , method = RequestMethod.POST)  
	public @ResponseBody Object getPendingCS(@RequestBody CashStmtStatus cashStmtStatus) {  
		System.out.println("Enter into getPendingCS---->");
		Map<String, Object> map = new HashMap<>();	
		List<CashStmtTemp> pendCsList = cashStmtTempDAO.getPendingCS(cashStmtStatus.getCssId());
		if(!pendCsList.isEmpty()){
			map.put("list",pendCsList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}	
	
	
	@RequestMapping(value = "/getClosingBal" , method = RequestMethod.POST)  
	public @ResponseBody Object getClosingBal(@RequestBody CashStmtStatus cashStmtStatus) {  
		System.out.println("Enter into getClosingBal---->");
		Map<String, Object> map = new HashMap<>();	
		int id = cashStmtStatus.getCssId();
		if(id > 0){
			List<CashStmt> cashStmtList = new ArrayList<>();
			cashStmtList  = cashStmtStatusDAO.getCSSById(id).get(0).getCashStmtList();
			System.out.println("size of cashStmtList = "+cashStmtList.size());
			if(!cashStmtList.isEmpty()){
				double closeBal = 0.0;
				double bkCloseBal = 0.0;
				closeBal = cashStmtStatusDAO.getCSSById(id).get(0).getCssOpenBal();
				bkCloseBal = cashStmtStatusDAO.getCSSById(id).get(0).getCssBkOpenBal();
				double crAmt = cashStmtStatusDAO.getCSSById(id).get(0).getCssOpenBal();
				double bkCrAmt = cashStmtStatusDAO.getCSSById(id).get(0).getCssBkOpenBal();
				double dbAmt = 0.0;
				double bkDbAmt = 0.0;
				
				for(int i=0;i<cashStmtList.size();i++){
					if(cashStmtList.get(i).getCsDrCr() == 'C' && cashStmtList.get(i).getCsVouchType().equalsIgnoreCase("CASH")){
						crAmt = crAmt + cashStmtList.get(i).getCsAmt();
					}else if(cashStmtList.get(i).getCsDrCr() == 'D' && cashStmtList.get(i).getCsVouchType().equalsIgnoreCase("CASH")){
						dbAmt = dbAmt + cashStmtList.get(i).getCsAmt();
					}else if(cashStmtList.get(i).getCsDrCr() == 'C' && cashStmtList.get(i).getCsVouchType().equalsIgnoreCase("BANK")){
						if(cashStmtList.get(i).getCsFaCode().substring(0,2).equalsIgnoreCase("07")){
							
						}else{
							System.out.println("*****************");
							bkCrAmt = bkCrAmt + cashStmtList.get(i).getCsAmt();
						}
					}else if(cashStmtList.get(i).getCsDrCr() == 'D' && cashStmtList.get(i).getCsVouchType().equalsIgnoreCase("BANK")){
						if(cashStmtList.get(i).getCsFaCode().substring(0,2).equalsIgnoreCase("07")){
							
						}else{
							bkDbAmt = bkDbAmt + cashStmtList.get(i).getCsAmt();
						}
					}else{
						System.out.println("invalid amount");
					}	
					
				}
				
				closeBal = crAmt - dbAmt;
				bkCloseBal = bkCrAmt - bkDbAmt;
				map.put("closeBal", closeBal);
				map.put("bkCloseBal", bkCloseBal);
				map.put("list", cashStmtList);

				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				double closeBal = 0.0;
				double bkCloseBal = 0.0;
				map.put("closeBal", closeBal);
				map.put("bkCloseBal", bkCloseBal);
				map.put("list", cashStmtList);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}	
	
	
	@RequestMapping(value = "/closeVoucher" , method = RequestMethod.POST)  
	public @ResponseBody Object closeVoucher(@RequestBody VoucherService voucherService) {  
		System.out.println("Enter into closeVoucher---->");
		Map<String, Object> map = new HashMap<>();	
		User currentUser = (User) httpSession.getAttribute("currentUser");
		CashStmtStatus cashStmtStatus = voucherService.getCashStmtStatus();
		List<Integer> penCsIdList = new ArrayList<>();
		penCsIdList = voucherService.getPenCsId();
		int id = cashStmtStatus.getCssId();
		LhpvStatus lhpvStatus = lhpvStatusDAO.getLastLhpvStatus(currentUser.getUserBranchCode());
		if(lhpvStatus == null){
			System.out.println("Lhpv not entered");
		}else{
			if(lhpvStatus.isLsClose() == false){
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
				map.put("msg","Please close the LHPV of "+lhpvStatus.getLsDt()+" date");
			}else{
				if(id > 0){
					List<CashStmtStatus> cssList = cashStmtStatusDAO.getCSSById(id);
					
					if(!cssList.isEmpty()){
						CashStmtStatus css = cssList.get(0);
						
						if(!penCsIdList.isEmpty()){
							int res = cashStmtStatusDAO.closePenCs(penCsIdList,css.getCssId());
						}
						List<CashStmt> cashStmtList  = cashStmtStatusDAO.getCSSById(id).get(0).getCashStmtList();
						System.out.println("size of cashStmtList = "+cashStmtList.size());
						if(!cashStmtList.isEmpty()){
							double closeBal = 0.0;
							double bkCloseBal = 0.0;
							closeBal = css.getCssOpenBal();
							bkCloseBal = css.getCssBkOpenBal();
							double crAmt = css.getCssOpenBal();
							double bkCrAmt = css.getCssBkOpenBal();
							double dbAmt = 0.0;
							double bkDbAmt = 0.0;
							for(int i=0;i<cashStmtList.size();i++){
								if(cashStmtList.get(i).getCsDrCr() == 'C' && cashStmtList.get(i).getCsVouchType().equalsIgnoreCase("CASH")){
									crAmt = crAmt + cashStmtList.get(i).getCsAmt();
								}else if(cashStmtList.get(i).getCsDrCr() == 'D' && cashStmtList.get(i).getCsVouchType().equalsIgnoreCase("CASH")){
									dbAmt = dbAmt + cashStmtList.get(i).getCsAmt();
								}else if(cashStmtList.get(i).getCsDrCr() == 'C' && cashStmtList.get(i).getCsVouchType().equalsIgnoreCase("BANK")){
									if(cashStmtList.get(i).getCsFaCode().substring(0,2).equalsIgnoreCase("07") && 
										cashStmtList.get(i).getCsType().equalsIgnoreCase("CashWithdraw(ATM)") || 
										cashStmtList.get(i).getCsType().equalsIgnoreCase("CashWithdraw(CHQ)") || 
										cashStmtList.get(i).getCsType().equalsIgnoreCase("CashWithdraw")){
										
									}else{
										System.out.println("*****************");
										bkCrAmt = bkCrAmt + cashStmtList.get(i).getCsAmt();
									}
								}else if(cashStmtList.get(i).getCsDrCr() == 'D' && cashStmtList.get(i).getCsVouchType().equalsIgnoreCase("BANK")){
									if(cashStmtList.get(i).getCsFaCode().substring(0,2).equalsIgnoreCase("07") && 
											cashStmtList.get(i).getCsType().equalsIgnoreCase("CashDeposite")){
										
									}else{
										bkDbAmt = bkDbAmt + cashStmtList.get(i).getCsAmt();
									}
								}else{
									System.out.println("invalid amount");
								}	
							}
							closeBal = crAmt - dbAmt;
							bkCloseBal = bkCrAmt - bkDbAmt;
							System.out.println("bkCloseBal === > "+bkCloseBal);
							css.setCssCloseBal(closeBal);
							css.setCssBkCloseBal(bkCloseBal);
							int temp = cashStmtStatusDAO.closeCSS(css);
							
							List<BankCS> bcsList = new ArrayList<>();
							bcsList = cashStmtStatusDAO.getBankCsByDt(cashStmtStatus.getCssDt(),Integer.parseInt(currentUser.getUserBranchCode()));
							
							if(temp > 0){
								List<CashStmt> actCsList  = cashStmtStatusDAO.getCSSById(id).get(0).getCashStmtList();
								List<Map<String,String>> acHdList = new ArrayList<>();
								List<FAMaster> faMList = faMasterDAO.getAllFAM();
								
								List<CashStmt> blCsList = new ArrayList<CashStmt>();
								List<Map<String,Object>> mrCsList = new ArrayList<>();
								List<Map<String,Object>> lhpvCsList = new ArrayList<>();
								if(!faMList.isEmpty() && !actCsList.isEmpty()){
									for(int i=0;i<actCsList.size();i++){
										Map<String,String> acHdMap = new HashMap<>();
										for(int j=0;j<faMList.size();j++){
											if(actCsList.get(i).getCsFaCode().equalsIgnoreCase(faMList.get(j).getFaMfaCode())){
												acHdMap.put("faCode",actCsList.get(i).getCsFaCode());
												acHdMap.put("faName",faMList.get(j).getFaMfaName());
												acHdList.add(acHdMap);
												break;
											}
										}
										
										
										if(actCsList.get(i).getCsType().equalsIgnoreCase("BILL")){
											if(!blCsList.isEmpty()){
												boolean isUpdate = false;
												for(int k=0;k<blCsList.size();k++){
													if(blCsList.get(k).getCsFaCode().equalsIgnoreCase(actCsList.get(i).getCsFaCode())){
														blCsList.get(k).setCsAmt(blCsList.get(k).getCsAmt() + actCsList.get(i).getCsAmt());
														isUpdate = true;
														break;
													}
												}
												
												if(!isUpdate){
													actCsList.get(i).setCsTvNo(String.valueOf(actCsList.get(i).getCsDt()));
													blCsList.add(actCsList.get(i));
												}
											}else{
												actCsList.get(i).setCsTvNo(String.valueOf(actCsList.get(i).getCsDt()));
												blCsList.add(actCsList.get(i));
											}
										}
										
										
										if(actCsList.get(i).getCsType().equalsIgnoreCase(ConstantsValues.MNY_RCT) && 
												actCsList.get(i).getCsVouchType().equalsIgnoreCase("BANK") &&
												actCsList.get(i).getCsFaCode().substring(0,2).equalsIgnoreCase("07")){
											if(!mrCsList.isEmpty()){
												boolean mrUpdate = false;
												for(int k=0;k<mrCsList.size();k++){
													Map<String,Object> mrBnkMap = mrCsList.get(k);
													if(String.valueOf(mrBnkMap.get("bankCode")).equalsIgnoreCase(actCsList.get(i).getCsFaCode())){
														System.out.println("*********************** 1");
														List<CashStmt> mrBnkList = (List<CashStmt>) mrBnkMap.get("list");
														int vouchNo = actCsList.get(i).getCsVouchNo(); 
														for(int l=0;l<actCsList.size();l++){
															if(actCsList.get(l).getCsVouchNo() == vouchNo){
																
																boolean subCount = false;
																for(int m=0;m<mrBnkList.size();m++){
																	System.out.println("*********************** 2");
																	if(mrBnkList.get(m).getCsFaCode().equalsIgnoreCase(actCsList.get(l).getCsFaCode()) && 
																			mrBnkList.get(m).getCsDrCr() == actCsList.get(l).getCsDrCr()){
																		mrBnkList.get(m).setCsAmt(mrBnkList.get(m).getCsAmt() + actCsList.get(l).getCsAmt());
																		System.out.println("*********************** 44 == "+actCsList.get(l).getCsFaCode());
																		subCount = true;
																		break;
																	}
																}
																
																if(!subCount){
																	System.out.println("*********************** 4 == "+actCsList.get(l).getCsFaCode());
																	actCsList.get(l).setCsTvNo(String.valueOf(actCsList.get(l).getCsDt()));
																	actCsList.get(l).setCsVouchNo(mrBnkList.get(0).getCsVouchNo());
																	mrBnkList.add(actCsList.get(l));
																}
																
															}
														}
														
														mrBnkMap.remove("list");
														mrBnkMap.put("list",mrBnkList);
														
														mrCsList.remove(k);
														mrCsList.add(k, mrBnkMap);
														
														mrUpdate = true;
														break;
													}
												}
												
												if(!mrUpdate){
													System.out.println("*********************** 1 *******");
													Map<String,Object> mrBnkMap = new HashMap<String, Object>();
													List<CashStmt> mrBnkList = new ArrayList<CashStmt>();
													
													int vouchNo = actCsList.get(i).getCsVouchNo(); 
													mrBnkMap.put("bankCode", actCsList.get(i).getCsFaCode());
													for(int k=0;k<actCsList.size();k++){
														if(actCsList.get(k).getCsVouchNo() == vouchNo){
															actCsList.get(k).setCsTvNo(String.valueOf(actCsList.get(k).getCsDt()));
															mrBnkList.add(actCsList.get(k));
														}
													}
													mrBnkMap.put("list",mrBnkList);
													
													mrCsList.add(mrBnkMap);
												}
											}else{
												System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
												Map<String,Object> mrBnkMap = new HashMap<String, Object>();
												List<CashStmt> mrBnkList = new ArrayList<CashStmt>();
												
												int vouchNo = actCsList.get(i).getCsVouchNo(); 
												mrBnkMap.put("bankCode", actCsList.get(i).getCsFaCode());
												for(int k=0;k<actCsList.size();k++){
													if(actCsList.get(k).getCsVouchNo() == vouchNo){
														actCsList.get(k).setCsTvNo(String.valueOf(actCsList.get(k).getCsDt()));
														mrBnkList.add(actCsList.get(k));
													}
												}
												mrBnkMap.put("list",mrBnkList);
												
												mrCsList.add(mrBnkMap);
											}
										}
										
										
										if(actCsList.get(i).getCsType().equalsIgnoreCase("LHPV PAYMENT") && 
												 actCsList.get(i).getCsFaCode().substring(0,2).equalsIgnoreCase("07") &&
												 actCsList.get(i).getCsVouchType().equalsIgnoreCase("BANK")){
												if(!lhpvCsList.isEmpty()){
													boolean lhpvUpdate = false;
													for(int k=0;k<lhpvCsList.size();k++){
														Map<String,Object> lhpvBnkMap = lhpvCsList.get(k);
														if(String.valueOf(lhpvBnkMap.get("bankCode")).equalsIgnoreCase(actCsList.get(i).getCsFaCode())){
															System.out.println("################################ 2");
															List<CashStmt> lhpvBnkList = (List<CashStmt>) lhpvBnkMap.get("list");
															int vouchNo = actCsList.get(i).getCsVouchNo(); 
															for(int l=0;l<actCsList.size();l++){
																if(actCsList.get(l).getCsVouchNo() == vouchNo){
																	
																	boolean subCount = false;
																	for(int m=0;m<lhpvBnkList.size();m++){
																		System.out.println("################################ 3");
																		if(lhpvBnkList.get(m).getCsFaCode().equalsIgnoreCase(actCsList.get(l).getCsFaCode()) && 
																				lhpvBnkList.get(m).getCsDrCr() == actCsList.get(l).getCsDrCr()){
																			lhpvBnkList.get(m).setCsAmt(lhpvBnkList.get(m).getCsAmt() + actCsList.get(l).getCsAmt());
																			System.out.println("################################ 4 == "+actCsList.get(l).getCsFaCode());
																			subCount = true;
																			break;
																		}
																	}
																	
																	if(!subCount){
																		System.out.println("################################ 5 == "+actCsList.get(l).getCsFaCode());
																		actCsList.get(l).setCsTvNo(String.valueOf(actCsList.get(l).getCsDt()));
																		actCsList.get(l).setCsVouchNo(lhpvBnkList.get(0).getCsVouchNo());
																		lhpvBnkList.add(actCsList.get(l));
																	}
																	
																}
															}
															
															lhpvBnkMap.remove("list");
															lhpvBnkMap.put("list",lhpvBnkList);
															
															lhpvCsList.remove(k);
															lhpvCsList.add(k, lhpvBnkMap);
															
															lhpvUpdate = true;
															break;
														}
													}
													
													if(!lhpvUpdate){
														System.out.println("################################ 6");
														Map<String,Object> lhpvBnkMap = new HashMap<String, Object>();
														List<CashStmt> lhpvBnkList = new ArrayList<CashStmt>();
														
														int vouchNo = actCsList.get(i).getCsVouchNo(); 
														lhpvBnkMap.put("bankCode", actCsList.get(i).getCsFaCode());
														for(int k=0;k<actCsList.size();k++){
															if(actCsList.get(k).getCsVouchNo() == vouchNo){
																actCsList.get(k).setCsTvNo(String.valueOf(actCsList.get(k).getCsDt()));
																lhpvBnkList.add(actCsList.get(k));
															}
														}
														lhpvBnkMap.put("list",lhpvBnkList);
														
														mrCsList.add(lhpvBnkMap);
													}
												}else{
													System.out.println("################################## 7");
													Map<String,Object> lhpvBnkMap = new HashMap<String, Object>();
													List<CashStmt> lhpvBnkList = new ArrayList<CashStmt>();
													
													int vouchNo = actCsList.get(i).getCsVouchNo(); 
													lhpvBnkMap.put("bankCode", actCsList.get(i).getCsFaCode());
													for(int k=0;k<actCsList.size();k++){
														if(actCsList.get(k).getCsVouchNo() == vouchNo){
															actCsList.get(k).setCsTvNo(String.valueOf(actCsList.get(k).getCsDt()));
															lhpvBnkList.add(actCsList.get(k));
														}
													}
													lhpvBnkMap.put("list",lhpvBnkList);
													
													lhpvCsList.add(lhpvBnkMap);
												}
											}
									}
									
									for(int j=0;j<faMList.size();j++){
										Map<String,String> acHdMap = new HashMap<>();
										if(faMList.get(j).getFaMfaCode().substring(0,2).equalsIgnoreCase("07")){
											acHdMap.put("faCode",faMList.get(j).getFaMfaCode());
											acHdMap.put("faName",faMList.get(j).getFaMfaName());
											acHdList.add(acHdMap);
										}
									}
									
									
									//Remove multiple bill entries
									for(int k=0;k<actCsList.size();k++){
										if(actCsList.get(k).getCsType().equalsIgnoreCase("BILL")){
											actCsList.remove(k);
											k = k-1;
										}
									}
									
									//Add new consolidated bill entries
									if(!blCsList.isEmpty()){
										for(int k=0;k<blCsList.size();k++){
											actCsList.add(blCsList.get(k));
										}
									}
									
									
									//Remove multiple mr entries
									for(int k=0;k<actCsList.size();k++){
										if(actCsList.get(k).getCsType().equalsIgnoreCase(ConstantsValues.MNY_RCT) &&
												actCsList.get(k).getCsVouchType().equalsIgnoreCase("BANK")){
											actCsList.remove(k);
											k = k-1;
										}
									}
									
									
									//Add new consolidated mr entries
									if(!mrCsList.isEmpty()){
										System.out.println("size of mrCsList = "+mrCsList.size());
										for(int k=0;k<mrCsList.size();k++){
											Map<String,Object> mrBnkMap = mrCsList.get(k);
											List<CashStmt> mrBnkList = (List<CashStmt>) mrBnkMap.get("list"); 
											if(!mrBnkList.isEmpty()){
												for(int l=0;l<mrBnkList.size();l++){
													actCsList.add(mrBnkList.get(l));
												}
											}
										}
									}
									
									//remove multiple LHPV bank entries
									for(int k=0;k<actCsList.size();k++){
										if(actCsList.get(k).getCsType().equalsIgnoreCase("LHPV PAYMENT") &&
												actCsList.get(k).getCsVouchType().equalsIgnoreCase("BANK")){
											actCsList.remove(k);
											k = k-1;
										}
									}
									
									
									//Add new consolidated lhpv bank entries
									if(!lhpvCsList.isEmpty()){
										System.out.println("size of lhpvCsList = "+lhpvCsList.size());
										for(int k=0;k<lhpvCsList.size();k++){
											Map<String,Object> lhpvBnkMap = lhpvCsList.get(k);
											List<CashStmt> lhpvBnkList = (List<CashStmt>) lhpvBnkMap.get("list"); 
											if(!lhpvBnkList.isEmpty()){
												for(int l=0;l<lhpvBnkList.size();l++){
													if(lhpvBnkList.get(l).getCsVouchType().equalsIgnoreCase("BANK")){
														actCsList.add(lhpvBnkList.get(l));
													}
												}
											}
										}
									}
									
									
									List<Map<String,Object>> bkCsMapList = new ArrayList<>();
									
									for(int i=0;i<bcsList.size();i++){
										Map<String,Object> bkCsMap = new HashMap<>();
										List<CashStmt> bkCsList = new ArrayList<>();
										for(int j=0;j<actCsList.size();j++){
											if(actCsList.get(j).getCsType().equalsIgnoreCase("Fund Transfer") && bcsList.get(i).getBcsBankCode().equalsIgnoreCase(actCsList.get(j).getCsFaCode())){
												int vhNo = actCsList.get(j).getCsVouchNo();
												int count = 0;
												for(int k=0;k<actCsList.size();k++){
													if(actCsList.get(k).getCsVouchNo() == vhNo && actCsList.get(k).getCsFaCode().substring(0,2).equalsIgnoreCase("07")){
														count = count + 1;
													}
												}
												
												if(count > 1){
													bkCsList.add(actCsList.get(j+1));
												}else{
													for(int k=0;k<actCsList.size();k++){
														if(actCsList.get(k).getCsVouchNo() == vhNo && !actCsList.get(k).getCsFaCode().substring(0,2).equalsIgnoreCase("07")){
															if(actCsList.get(k).getCsVouchType().equalsIgnoreCase("BANK")){
																bkCsList.add(actCsList.get(k));
															}
														}
													}
												}
												
												
											}else if(bcsList.get(i).getBcsBankCode().equalsIgnoreCase(actCsList.get(j).getCsFaCode())){
												int vhNo = actCsList.get(j).getCsVouchNo();
												for(int k=0;k<actCsList.size();k++){
													if(actCsList.get(k).getCsVouchNo() == vhNo && !actCsList.get(k).getCsFaCode().substring(0,2).equalsIgnoreCase("07")){
														if(actCsList.get(k).getCsVouchType().equalsIgnoreCase("BANK")){
															bkCsList.add(actCsList.get(k));
														}
													}
												}
											}
										}
										
										bkCsMap.put("bank",bcsList.get(i));
										bkCsMap.put("bkCsList", bkCsList);
											double recBnkAmt = bcsList.get(i).getBcsOpenBal();
											double payBnkAmt = bcsList.get(i).getBcsCloseBal();
											for(int k=0;k<bkCsList.size();k++){
												if(bkCsList.get(k).getCsDrCr() == 'D'){
													payBnkAmt = payBnkAmt + bkCsList.get(k).getCsAmt();
												}else{
													recBnkAmt = recBnkAmt + bkCsList.get(k).getCsAmt();
												}
											}
											bkCsMap.put("recBnkAmt",recBnkAmt);
											bkCsMap.put("payBnkAmt",payBnkAmt);
										//}
										bkCsMapList.add(bkCsMap);
									}
									
									
									if(!bkCsMapList.isEmpty()){
										for(int i=0;i<bkCsMapList.size();i++){
											Map<String,Object> ccMap = bkCsMapList.get(i) ;
											BankCS bankCS = (BankCS) ccMap.get("bank");
											List<CashStmt> bkCsList = (List<CashStmt>) ccMap.get("bkCsList");
											
											System.out.println("*************************");
											System.out.println("#### Bank Code = "+bankCS.getBcsBankCode());
											for(int j=0;j<bkCsList.size();j++){
												System.out.println("%%%%%%%% ca FaCode = "+bkCsList.get(j).getCsFaCode());
											}
										}
									}
									map.put("bkCsMapList",bkCsMapList);
								}
								
								map.put("list", actCsList);
								map.put("acHdList", acHdList);
								map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
							}else{
								map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
							}
						}else{
							double closeBal = 0.0;
							double bkCloseBal = 0.0;
							closeBal = css.getCssOpenBal();
							bkCloseBal = css.getCssBkOpenBal();
							css.setCssCloseBal(closeBal);
							css.setCssBkCloseBal(bkCloseBal);
							int temp = cashStmtStatusDAO.closeCSS(css);
							
							List<BankCS> bcsList = new ArrayList<>();
							bcsList = cashStmtStatusDAO.getBankCsByDt(cashStmtStatus.getCssDt(),Integer.parseInt(currentUser.getUserBranchCode()));
							System.out.println("$$$$$$$$$$$$ size of bcsList = "+bcsList.size());
							if(temp > 0){
								List<CashStmt> actCsList  = cashStmtStatusDAO.getCSSById(id).get(0).getCashStmtList();
								List<Map<String,String>> acHdList = new ArrayList<>();
								List<FAMaster> faMList = faMasterDAO.getAllFAM();
								
								if(!faMList.isEmpty() && !actCsList.isEmpty()){
									for(int i=0;i<actCsList.size();i++){
										Map<String,String> acHdMap = new HashMap<>();
										for(int j=0;j<faMList.size();j++){
											if(actCsList.get(i).getCsFaCode().equalsIgnoreCase(faMList.get(j).getFaMfaCode())){
												acHdMap.put("faCode",actCsList.get(i).getCsFaCode());
												acHdMap.put("faName",faMList.get(j).getFaMfaName());
												acHdList.add(acHdMap);
												break;
											}
										}
									}
								}
								for(int j=0;j<faMList.size();j++){
									Map<String,String> acHdMap = new HashMap<>();
									if(faMList.get(j).getFaMfaCode().substring(0,2).equalsIgnoreCase("07")){
										acHdMap.put("faCode",faMList.get(j).getFaMfaCode());
										acHdMap.put("faName",faMList.get(j).getFaMfaName());
										acHdList.add(acHdMap);
									}
								}
								
								List<Map<String,Object>> bkCsMapList = new ArrayList<>();
								
								for(int i=0;i<bcsList.size();i++){
									Map<String,Object> bkCsMap = new HashMap<>();
									bkCsMap.put("bank",bcsList.get(i));
									bkCsMapList.add(bkCsMap);
								}
								
								map.put("bkCsMapList",bkCsMapList);
								
								map.put("list", actCsList);
								map.put("acHdList", acHdList);
								map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
							}else{
								map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
							}
						}
					}else{
						map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
					}	
				}else{
					map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
				}
			}
		}

		return map;
	}
	
	@RequestMapping(value = "/getVoucherDetFrClsBack" , method = RequestMethod.POST)  
	public @ResponseBody Object getVoucherDetFrClsBack() {  
		System.out.println("Enter into getVoucherDetFrClsBack---->");
		Map<String, Object> map = new HashMap<>();	
		User user = (User) httpSession.getAttribute("currentUser");
		if(user != null){
			String branchCode = user.getUserBranchCode();
			System.out.println("branchCode = "+branchCode);
			List<Branch> branchList = branchDAO.retrieveBranch(branchCode);
			if(!branchList.isEmpty()){
				CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranchBack(branchCode);
				
				if(cashStmtStatus != null){
					System.out.println("Enter into If----->>>>");
					map.put(ConstantsValues.BRANCH, branchList.get(0));
					map.put(ConstantsValues.CASH_STMT_STATUS, cashStmtStatus);
					map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				}else{
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}

		return map;
	}


	
	@RequestMapping(value = "/closeVoucherBack" , method = RequestMethod.POST)  
	public @ResponseBody Object closeVoucherBack(@RequestBody VoucherService voucherService) {  
		System.out.println("Enter into closeVoucher---->");
		Map<String, Object> map = new HashMap<>();	
		User currentUser = (User) httpSession.getAttribute("currentUser");
		CashStmtStatus cashStmtStatus = voucherService.getCashStmtStatus();
		List<Integer> penCsIdList = new ArrayList<>();
		penCsIdList = voucherService.getPenCsId();
		int id = cashStmtStatus.getCssId();
		LhpvStatus lhpvStatus = lhpvStatusDAO.getLastLhpvStatusBack(currentUser.getUserBranchCode());
		if(lhpvStatus == null){
			System.out.println("Lhpv not entered");
		}else{
			if(lhpvStatus.isLsClose() == false){
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
				map.put("msg","Please close the LHPV of "+lhpvStatus.getLsDt()+" date");
			}else{
				if(id > 0){
					List<CashStmtStatus> cssList = cashStmtStatusDAO.getCSSById(id);
					
					if(!cssList.isEmpty()){
						CashStmtStatus css = cssList.get(0);
						
						if(!penCsIdList.isEmpty()){
							int res = cashStmtStatusDAO.closePenCs(penCsIdList,css.getCssId());
						}
						List<CashStmt> cashStmtList  = cashStmtStatusDAO.getCSSById(id).get(0).getCashStmtList();
						System.out.println("size of cashStmtList = "+cashStmtList.size());
						if(!cashStmtList.isEmpty()){
							double closeBal = 0.0;
							double bkCloseBal = 0.0;
							closeBal = css.getCssOpenBal();
							bkCloseBal = css.getCssBkOpenBal();
							double crAmt = css.getCssOpenBal();
							double bkCrAmt = css.getCssBkOpenBal();
							double dbAmt = 0.0;
							double bkDbAmt = 0.0;
							for(int i=0;i<cashStmtList.size();i++){
								if(cashStmtList.get(i).getCsDrCr() == 'C' && cashStmtList.get(i).getCsVouchType().equalsIgnoreCase("CASH")){
									crAmt = crAmt + cashStmtList.get(i).getCsAmt();
								}else if(cashStmtList.get(i).getCsDrCr() == 'D' && cashStmtList.get(i).getCsVouchType().equalsIgnoreCase("CASH")){
									dbAmt = dbAmt + cashStmtList.get(i).getCsAmt();
								}else if(cashStmtList.get(i).getCsDrCr() == 'C' && cashStmtList.get(i).getCsVouchType().equalsIgnoreCase("BANK")){
									if(cashStmtList.get(i).getCsFaCode().substring(0,2).equalsIgnoreCase("07") && 
										cashStmtList.get(i).getCsType().equalsIgnoreCase("CashWithdraw(ATM)") || 
										cashStmtList.get(i).getCsType().equalsIgnoreCase("CashWithdraw(CHQ)") || 
										cashStmtList.get(i).getCsType().equalsIgnoreCase("CashWithdraw")){
										
									}else{
										System.out.println("*****************");
										bkCrAmt = bkCrAmt + cashStmtList.get(i).getCsAmt();
									}
								}else if(cashStmtList.get(i).getCsDrCr() == 'D' && cashStmtList.get(i).getCsVouchType().equalsIgnoreCase("BANK")){
									if(cashStmtList.get(i).getCsFaCode().substring(0,2).equalsIgnoreCase("07") && 
											cashStmtList.get(i).getCsType().equalsIgnoreCase("CashDeposite")){
										
									}else{
										bkDbAmt = bkDbAmt + cashStmtList.get(i).getCsAmt();
									}
								}else{
									System.out.println("invalid amount");
								}	
							}
							closeBal = crAmt - dbAmt;
							bkCloseBal = bkCrAmt - bkDbAmt;
							System.out.println("bkCloseBal === > "+bkCloseBal);
							css.setCssCloseBal(closeBal);
							css.setCssBkCloseBal(bkCloseBal);
							int temp = cashStmtStatusDAO.closeCSS(css);
							
							List<BankCS> bcsList = new ArrayList<>();
							bcsList = cashStmtStatusDAO.getBankCsByDt(cashStmtStatus.getCssDt(),Integer.parseInt(currentUser.getUserBranchCode()));
							
							if(temp > 0){
								List<CashStmt> actCsList  = cashStmtStatusDAO.getCSSById(id).get(0).getCashStmtList();
								List<Map<String,String>> acHdList = new ArrayList<>();
								List<FAMaster> faMList = faMasterDAO.getAllFAM();
								
								List<CashStmt> blCsList = new ArrayList<CashStmt>();
								List<Map<String,Object>> mrCsList = new ArrayList<>();
								List<Map<String,Object>> lhpvCsList = new ArrayList<>();
								if(!faMList.isEmpty() && !actCsList.isEmpty()){
									for(int i=0;i<actCsList.size();i++){
										Map<String,String> acHdMap = new HashMap<>();
										for(int j=0;j<faMList.size();j++){
											if(actCsList.get(i).getCsFaCode().equalsIgnoreCase(faMList.get(j).getFaMfaCode())){
											
												acHdMap.put("faCode",actCsList.get(i).getCsFaCode());
												acHdMap.put("faName",faMList.get(j).getFaMfaName());
												acHdList.add(acHdMap);
												break;
											}
										}
										
										
										if(actCsList.get(i).getCsType().equalsIgnoreCase("BILL")){
											if(!blCsList.isEmpty()){
												boolean isUpdate = false;
												for(int k=0;k<blCsList.size();k++){
													if(blCsList.get(k).getCsFaCode().equalsIgnoreCase(actCsList.get(i).getCsFaCode())){
														blCsList.get(k).setCsAmt(blCsList.get(k).getCsAmt() + actCsList.get(i).getCsAmt());
														isUpdate = true;
														break;
													}
												}
												
												if(!isUpdate){
													actCsList.get(i).setCsTvNo(String.valueOf(actCsList.get(i).getCsDt()));
													blCsList.add(actCsList.get(i));
												}
											}else{
												actCsList.get(i).setCsTvNo(String.valueOf(actCsList.get(i).getCsDt()));
												blCsList.add(actCsList.get(i));
											}
										}
										
										
										if(actCsList.get(i).getCsType().equalsIgnoreCase(ConstantsValues.MNY_RCT) && 
												actCsList.get(i).getCsVouchType().equalsIgnoreCase("BANK") &&
												actCsList.get(i).getCsFaCode().substring(0,2).equalsIgnoreCase("07")){
											if(!mrCsList.isEmpty()){
												boolean mrUpdate = false;
												for(int k=0;k<mrCsList.size();k++){
													Map<String,Object> mrBnkMap = mrCsList.get(k);
													if(String.valueOf(mrBnkMap.get("bankCode")).equalsIgnoreCase(actCsList.get(i).getCsFaCode())){
														System.out.println("*********************** 1");
														List<CashStmt> mrBnkList = (List<CashStmt>) mrBnkMap.get("list");
														int vouchNo = actCsList.get(i).getCsVouchNo(); 
														for(int l=0;l<actCsList.size();l++){
															if(actCsList.get(l).getCsVouchNo() == vouchNo){
																
																boolean subCount = false;
																for(int m=0;m<mrBnkList.size();m++){
																	System.out.println("*********************** 2");
																	if(mrBnkList.get(m).getCsFaCode().equalsIgnoreCase(actCsList.get(l).getCsFaCode()) && 
																			mrBnkList.get(m).getCsDrCr() == actCsList.get(l).getCsDrCr()){
																		mrBnkList.get(m).setCsAmt(mrBnkList.get(m).getCsAmt() + actCsList.get(l).getCsAmt());
																		System.out.println("*********************** 44 == "+actCsList.get(l).getCsFaCode());
																		subCount = true;
																		break;
																	}
																}
																
																if(!subCount){
																	System.out.println("*********************** 4 == "+actCsList.get(l).getCsFaCode());
																	actCsList.get(l).setCsTvNo(String.valueOf(actCsList.get(l).getCsDt()));
																	actCsList.get(l).setCsVouchNo(mrBnkList.get(0).getCsVouchNo());
																	mrBnkList.add(actCsList.get(l));
																}
																
															}
														}
														
														mrBnkMap.remove("list");
														mrBnkMap.put("list",mrBnkList);
														
														mrCsList.remove(k);
														mrCsList.add(k, mrBnkMap);
														
														mrUpdate = true;
														break;
													}
												}
												
												if(!mrUpdate){
													System.out.println("*********************** 1 *******");
													Map<String,Object> mrBnkMap = new HashMap<String, Object>();
													List<CashStmt> mrBnkList = new ArrayList<CashStmt>();
													
													int vouchNo = actCsList.get(i).getCsVouchNo(); 
													mrBnkMap.put("bankCode", actCsList.get(i).getCsFaCode());
													for(int k=0;k<actCsList.size();k++){
														if(actCsList.get(k).getCsVouchNo() == vouchNo){
															actCsList.get(k).setCsTvNo(String.valueOf(actCsList.get(k).getCsDt()));
															mrBnkList.add(actCsList.get(k));
														}
													}
													mrBnkMap.put("list",mrBnkList);
													
													mrCsList.add(mrBnkMap);
												}
											}else{
												System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
												Map<String,Object> mrBnkMap = new HashMap<String, Object>();
												List<CashStmt> mrBnkList = new ArrayList<CashStmt>();
												
												int vouchNo = actCsList.get(i).getCsVouchNo(); 
												mrBnkMap.put("bankCode", actCsList.get(i).getCsFaCode());
												for(int k=0;k<actCsList.size();k++){
													if(actCsList.get(k).getCsVouchNo() == vouchNo){
														actCsList.get(k).setCsTvNo(String.valueOf(actCsList.get(k).getCsDt()));
														mrBnkList.add(actCsList.get(k));
													}
												}
												mrBnkMap.put("list",mrBnkList);
												
												mrCsList.add(mrBnkMap);
											}
										}
										
										
										if(actCsList.get(i).getCsType().equalsIgnoreCase("LHPV PAYMENT") && 
												 actCsList.get(i).getCsFaCode().substring(0,2).equalsIgnoreCase("07") &&
												 actCsList.get(i).getCsVouchType().equalsIgnoreCase("BANK")){
												if(!lhpvCsList.isEmpty()){
													boolean lhpvUpdate = false;
													for(int k=0;k<lhpvCsList.size();k++){
														Map<String,Object> lhpvBnkMap = lhpvCsList.get(k);
														if(String.valueOf(lhpvBnkMap.get("bankCode")).equalsIgnoreCase(actCsList.get(i).getCsFaCode())){
															System.out.println("################################ 2");
															List<CashStmt> lhpvBnkList = (List<CashStmt>) lhpvBnkMap.get("list");
															int vouchNo = actCsList.get(i).getCsVouchNo(); 
															for(int l=0;l<actCsList.size();l++){
																if(actCsList.get(l).getCsVouchNo() == vouchNo){
																	
																	boolean subCount = false;
																	for(int m=0;m<lhpvBnkList.size();m++){
																		System.out.println("################################ 3");
																		if(lhpvBnkList.get(m).getCsFaCode().equalsIgnoreCase(actCsList.get(l).getCsFaCode()) && 
																				lhpvBnkList.get(m).getCsDrCr() == actCsList.get(l).getCsDrCr()){
																			lhpvBnkList.get(m).setCsAmt(lhpvBnkList.get(m).getCsAmt() + actCsList.get(l).getCsAmt());
																			System.out.println("################################ 4 == "+actCsList.get(l).getCsFaCode());
																			subCount = true;
																			break;
																		}
																	}
																	
																	if(!subCount){
																		System.out.println("################################ 5 == "+actCsList.get(l).getCsFaCode());
																		actCsList.get(l).setCsTvNo(String.valueOf(actCsList.get(l).getCsDt()));
																		actCsList.get(l).setCsVouchNo(lhpvBnkList.get(0).getCsVouchNo());
																		lhpvBnkList.add(actCsList.get(l));
																	}
																	
																}
															}
															
															lhpvBnkMap.remove("list");
															lhpvBnkMap.put("list",lhpvBnkList);
															
															lhpvCsList.remove(k);
															lhpvCsList.add(k, lhpvBnkMap);
															
															lhpvUpdate = true;
															break;
														}
													}
													
													if(!lhpvUpdate){
														System.out.println("################################ 6");
														Map<String,Object> lhpvBnkMap = new HashMap<String, Object>();
														List<CashStmt> lhpvBnkList = new ArrayList<CashStmt>();
														
														int vouchNo = actCsList.get(i).getCsVouchNo(); 
														lhpvBnkMap.put("bankCode", actCsList.get(i).getCsFaCode());
														for(int k=0;k<actCsList.size();k++){
															if(actCsList.get(k).getCsVouchNo() == vouchNo){
																actCsList.get(k).setCsTvNo(String.valueOf(actCsList.get(k).getCsDt()));
																lhpvBnkList.add(actCsList.get(k));
															}
														}
														lhpvBnkMap.put("list",lhpvBnkList);
														
														mrCsList.add(lhpvBnkMap);
													}
												}else{
													System.out.println("################################## 7");
													Map<String,Object> lhpvBnkMap = new HashMap<String, Object>();
													List<CashStmt> lhpvBnkList = new ArrayList<CashStmt>();
													
													int vouchNo = actCsList.get(i).getCsVouchNo(); 
													lhpvBnkMap.put("bankCode", actCsList.get(i).getCsFaCode());
													for(int k=0;k<actCsList.size();k++){
														if(actCsList.get(k).getCsVouchNo() == vouchNo){
															actCsList.get(k).setCsTvNo(String.valueOf(actCsList.get(k).getCsDt()));
															lhpvBnkList.add(actCsList.get(k));
														}
													}
													lhpvBnkMap.put("list",lhpvBnkList);
													
													lhpvCsList.add(lhpvBnkMap);
												}
											}
									}
									
									for(int j=0;j<faMList.size();j++){
										Map<String,String> acHdMap = new HashMap<>();
										if(faMList.get(j).getFaMfaCode().substring(0,2).equalsIgnoreCase("07")){
											acHdMap.put("faCode",faMList.get(j).getFaMfaCode());
											acHdMap.put("faName",faMList.get(j).getFaMfaName());
											acHdList.add(acHdMap);
										}
									}
									
									
									//Remove multiple bill entries
									for(int k=0;k<actCsList.size();k++){
										if(actCsList.get(k).getCsType().equalsIgnoreCase("BILL")){
											actCsList.remove(k);
											k = k-1;
										}
									}
									
									//Add new consolidated bill entries
									if(!blCsList.isEmpty()){
										for(int k=0;k<blCsList.size();k++){
											actCsList.add(blCsList.get(k));
										}
									}
									
									
									//Remove multiple mr entries
									for(int k=0;k<actCsList.size();k++){
										if(actCsList.get(k).getCsType().equalsIgnoreCase(ConstantsValues.MNY_RCT) &&
												actCsList.get(k).getCsVouchType().equalsIgnoreCase("BANK")){
											actCsList.remove(k);
											k = k-1;
										}
									}
									
									
									//Add new consolidated mr entries
									if(!mrCsList.isEmpty()){
										System.out.println("size of mrCsList = "+mrCsList.size());
										for(int k=0;k<mrCsList.size();k++){
											Map<String,Object> mrBnkMap = mrCsList.get(k);
											List<CashStmt> mrBnkList = (List<CashStmt>) mrBnkMap.get("list"); 
											if(!mrBnkList.isEmpty()){
												for(int l=0;l<mrBnkList.size();l++){
													actCsList.add(mrBnkList.get(l));
												}
											}
										}
									}
									
									//remove multiple LHPV bank entries
									for(int k=0;k<actCsList.size();k++){
										if(actCsList.get(k).getCsType().equalsIgnoreCase("LHPV PAYMENT") &&
												actCsList.get(k).getCsVouchType().equalsIgnoreCase("BANK")){
											actCsList.remove(k);
											k = k-1;
										}
									}
									
									
									//Add new consolidated lhpv bank entries
									if(!lhpvCsList.isEmpty()){
										System.out.println("size of lhpvCsList = "+lhpvCsList.size());
										for(int k=0;k<lhpvCsList.size();k++){
											Map<String,Object> lhpvBnkMap = lhpvCsList.get(k);
											List<CashStmt> lhpvBnkList = (List<CashStmt>) lhpvBnkMap.get("list"); 
											if(!lhpvBnkList.isEmpty()){
												for(int l=0;l<lhpvBnkList.size();l++){
													if(lhpvBnkList.get(l).getCsVouchType().equalsIgnoreCase("BANK")){
														actCsList.add(lhpvBnkList.get(l));
													}
												}
											}
										}
									}
									
									
									List<Map<String,Object>> bkCsMapList = new ArrayList<>();
									
									for(int i=0;i<bcsList.size();i++){
										Map<String,Object> bkCsMap = new HashMap<>();
										List<CashStmt> bkCsList = new ArrayList<>();
										for(int j=0;j<actCsList.size();j++){
											if(actCsList.get(j).getCsType().equalsIgnoreCase("Fund Transfer") && bcsList.get(i).getBcsBankCode().equalsIgnoreCase(actCsList.get(j).getCsFaCode())){
												int vhNo = actCsList.get(j).getCsVouchNo();
												int count = 0;
												for(int k=0;k<actCsList.size();k++){
													if(actCsList.get(k).getCsVouchNo() == vhNo && actCsList.get(k).getCsFaCode().substring(0,2).equalsIgnoreCase("07")){
														count = count + 1;
													}
												}
												
												if(count > 1){
													bkCsList.add(actCsList.get(j+1));
												}else{
													for(int k=0;k<actCsList.size();k++){
														if(actCsList.get(k).getCsVouchNo() == vhNo && !actCsList.get(k).getCsFaCode().substring(0,2).equalsIgnoreCase("07")){
															if(actCsList.get(k).getCsVouchType().equalsIgnoreCase("BANK")){
																bkCsList.add(actCsList.get(k));
															}
														}
													}
												}
												
												
											}else if(bcsList.get(i).getBcsBankCode().equalsIgnoreCase(actCsList.get(j).getCsFaCode())){
												int vhNo = actCsList.get(j).getCsVouchNo();
												for(int k=0;k<actCsList.size();k++){
													if(actCsList.get(k).getCsVouchNo() == vhNo && !actCsList.get(k).getCsFaCode().substring(0,2).equalsIgnoreCase("07")){
														if(actCsList.get(k).getCsVouchType().equalsIgnoreCase("BANK")){
															bkCsList.add(actCsList.get(k));
														}
													}
												}
											}
										}
										
										bkCsMap.put("bank",bcsList.get(i));
										bkCsMap.put("bkCsList", bkCsList);
											double recBnkAmt = bcsList.get(i).getBcsOpenBal();
											double payBnkAmt = bcsList.get(i).getBcsCloseBal();
											for(int k=0;k<bkCsList.size();k++){
												if(bkCsList.get(k).getCsDrCr() == 'D'){
													payBnkAmt = payBnkAmt + bkCsList.get(k).getCsAmt();
												}else{
													recBnkAmt = recBnkAmt + bkCsList.get(k).getCsAmt();
												}
											}
											bkCsMap.put("recBnkAmt",recBnkAmt);
											bkCsMap.put("payBnkAmt",payBnkAmt);
										bkCsMapList.add(bkCsMap);
									}
									
									
									if(!bkCsMapList.isEmpty()){
										for(int i=0;i<bkCsMapList.size();i++){
											Map<String,Object> ccMap = bkCsMapList.get(i) ;
											BankCS bankCS = (BankCS) ccMap.get("bank");
											List<CashStmt> bkCsList = (List<CashStmt>) ccMap.get("bkCsList");
											
											System.out.println("*************************");
											System.out.println("#### Bank Code = "+bankCS.getBcsBankCode());
											for(int j=0;j<bkCsList.size();j++){
												System.out.println("%%%%%%%% ca FaCode = "+bkCsList.get(j).getCsFaCode());
											}
										}
									}
									map.put("bkCsMapList",bkCsMapList);
								}
								
								map.put("list", actCsList);
								map.put("acHdList", acHdList);
								map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
							}else{
								map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
							}
						}else{
							double closeBal = 0.0;
							double bkCloseBal = 0.0;
							closeBal = css.getCssOpenBal();
							bkCloseBal = css.getCssBkOpenBal();
							css.setCssCloseBal(closeBal);
							css.setCssBkCloseBal(bkCloseBal);
							int temp = cashStmtStatusDAO.closeCSS(css);
							
							List<BankCS> bcsList = new ArrayList<>();
							bcsList = cashStmtStatusDAO.getBankCsByDt(cashStmtStatus.getCssDt(),Integer.parseInt(currentUser.getUserBranchCode()));
							System.out.println("$$$$$$$$$$$$ size of bcsList = "+bcsList.size());
							if(temp > 0){
								List<CashStmt> actCsList  = cashStmtStatusDAO.getCSSById(id).get(0).getCashStmtList();
								List<Map<String,String>> acHdList = new ArrayList<>();
								List<FAMaster> faMList = faMasterDAO.getAllFAM();
								
								if(!faMList.isEmpty() && !actCsList.isEmpty()){
									for(int i=0;i<actCsList.size();i++){
										Map<String,String> acHdMap = new HashMap<>();
										for(int j=0;j<faMList.size();j++){
											if(actCsList.get(i).getCsFaCode().equalsIgnoreCase(faMList.get(j).getFaMfaCode())){
												acHdMap.put("faCode",actCsList.get(i).getCsFaCode());
												acHdMap.put("faName",faMList.get(j).getFaMfaName());
												acHdList.add(acHdMap);
												break;
											}
										}
									}
								}
								for(int j=0;j<faMList.size();j++){
									Map<String,String> acHdMap = new HashMap<>();
									if(faMList.get(j).getFaMfaCode().substring(0,2).equalsIgnoreCase("07")){
										acHdMap.put("faCode",faMList.get(j).getFaMfaCode());
										acHdMap.put("faName",faMList.get(j).getFaMfaName());
										acHdList.add(acHdMap);
									}
								}
								
								List<Map<String,Object>> bkCsMapList = new ArrayList<>();
								
								for(int i=0;i<bcsList.size();i++){
									Map<String,Object> bkCsMap = new HashMap<>();
									bkCsMap.put("bank",bcsList.get(i));
									bkCsMapList.add(bkCsMap);
								}
								
								map.put("bkCsMapList",bkCsMapList);
								
								map.put("list", actCsList);
								map.put("acHdList", acHdList);
								map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
							}else{
								map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
							}
						}
					}else{
						map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
					}	
				}else{
					map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
				}
			}
		}

		return map;
	}
	
	@RequestMapping(value = "/closeVoucherN" , method = RequestMethod.POST)  
	public @ResponseBody Object closeVoucherN(@RequestBody VoucherService voucherService) {  
		logger.info("Enter into closeVoucherN---->");
		Map<String, Object> map = new HashMap<>();	
		User currentUser = (User) httpSession.getAttribute("currentUser");
		CashStmtStatus cashStmtStatus = voucherService.getCashStmtStatus();
		List<Integer> penCsIdList = voucherService.getPenCsId();
		int id = cashStmtStatus.getCssId();
		
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try{
			
			LhpvStatus lhpvStatus = lhpvStatusDAO.getLastLhpvStatus(currentUser.getUserBranchCode(), session);
			if(lhpvStatus == null) {
				logger.info("Lhpv not entered");
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
				map.put("msg","Lhpv status not found for current user.");
			} else {
				if(! lhpvStatus.isLsClose()){
					map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
					map.put("msg","Please close the LHPV of "+lhpvStatus.getLsDt()+" date");
				} else {
					if(id > 0) {
						List<CashStmtStatus> cssList = cashStmtStatusDAO.getCSSById(id, session);
						
						if(!cssList.isEmpty()){
							CashStmtStatus css = cssList.get(0);
							
							if(! penCsIdList.isEmpty()) {
								cashStmtStatusDAO.closePenCs(penCsIdList, css.getCssId(), session);
							}
							List<CashStmt> cashStmtList  = cashStmtStatusDAO.getCSSById(id, session).get(0).getCashStmtList();
							logger.info("size of cashStmtList = "+cashStmtList.size());
							if(!cashStmtList.isEmpty()){
								double closeBal = css.getCssOpenBal();
								double bkCloseBal = css.getCssBkOpenBal();
								double crAmt = css.getCssOpenBal();
								double bkCrAmt = css.getCssBkOpenBal();
								double dbAmt = 0.0;
								double bkDbAmt = 0.0;
								for(int i=0; i < cashStmtList.size(); i++){
									if(cashStmtList.get(i).getCsDrCr() == 'C' && cashStmtList.get(i).getCsVouchType().equalsIgnoreCase("CASH")){
										crAmt = crAmt + cashStmtList.get(i).getCsAmt();
									} else if(cashStmtList.get(i).getCsDrCr() == 'D' && cashStmtList.get(i).getCsVouchType().equalsIgnoreCase("CASH")){
										dbAmt = dbAmt + cashStmtList.get(i).getCsAmt();
									} else if(cashStmtList.get(i).getCsDrCr() == 'C' && cashStmtList.get(i).getCsVouchType().equalsIgnoreCase("BANK")){
										if(cashStmtList.get(i).getCsFaCode().substring(0,2).equalsIgnoreCase("07") && 
											cashStmtList.get(i).getCsType().equalsIgnoreCase("CashWithdraw(ATM)") || 
											cashStmtList.get(i).getCsType().equalsIgnoreCase("CashWithdraw(CHQ)") || 
											cashStmtList.get(i).getCsType().equalsIgnoreCase("CashWithdraw")) {
											
										} else {
											bkCrAmt = bkCrAmt + cashStmtList.get(i).getCsAmt();
										}
									}else if(cashStmtList.get(i).getCsDrCr() == 'D' && cashStmtList.get(i).getCsVouchType().equalsIgnoreCase("BANK")){
										if(cashStmtList.get(i).getCsFaCode().substring(0,2).equalsIgnoreCase("07") && 
												cashStmtList.get(i).getCsType().equalsIgnoreCase("CashDeposite")){
											
										}else{
											bkDbAmt = bkDbAmt + cashStmtList.get(i).getCsAmt();
										}
									}else{
										logger.info("invalid amount");
									}	
								}
								closeBal = crAmt - dbAmt;
								bkCloseBal = bkCrAmt - bkDbAmt;
								logger.info("bkCloseBal === > "+bkCloseBal);
								css.setCssCloseBal(closeBal);
								css.setCssBkCloseBal(bkCloseBal);
								int temp = cashStmtStatusDAO.closeCSS(css,session);
								
								List<BankCS> bcsList = 
										cashStmtStatusDAO.getBankCsByDt(cashStmtStatus.getCssDt(),Integer.parseInt(currentUser.getUserBranchCode()),session);
								
								if(temp > 0) {
									List<CashStmt> actCsList  = cashStmtStatusDAO.getCSSById(id,session).get(0).getCashStmtList();
									List<Map<String,String>> acHdList = new ArrayList<>();
									List<FAMaster> faMList = faMasterDAO.getAllFAM(session);

									
									try {
										transaction.commit();
										session.close();
									} catch (Exception e) {
										transaction.rollback();
										logger.info(e.getMessage(), e);
									}
									
									List<CashStmt> blCsList = new ArrayList<>();
									List<Map<String,Object>> mrCsList = new ArrayList<>();
									List<Map<String,Object>> lhpvCsList = new ArrayList<>();
									if(!faMList.isEmpty() && !actCsList.isEmpty()){
										
										for(int i=0;i<actCsList.size();i++){
											
											Map<String,String> acHdMap = new HashMap<>();
											for(int j=0;j<faMList.size();j++){
												if(actCsList.get(i).getCsFaCode().equalsIgnoreCase(faMList.get(j).getFaMfaCode())){
													acHdMap.put("faCode",actCsList.get(i).getCsFaCode());
													acHdMap.put("faName",faMList.get(j).getFaMfaName());
													acHdList.add(acHdMap);
													break;
												}
											}
											
											
											if(actCsList.get(i).getCsType().equalsIgnoreCase("BILL")){
												if(!blCsList.isEmpty()){
													boolean isUpdate = false;
													for(int k=0;k<blCsList.size();k++){
														if(blCsList.get(k).getCsFaCode().equalsIgnoreCase(actCsList.get(i).getCsFaCode())){
															blCsList.get(k).setCsAmt(blCsList.get(k).getCsAmt() + actCsList.get(i).getCsAmt());
															isUpdate = true;
															break;
														}
													}
													
													if(!isUpdate){
														actCsList.get(i).setCsTvNo(String.valueOf(actCsList.get(i).getCsDt()));
														blCsList.add(actCsList.get(i));
													}
												}else{
													actCsList.get(i).setCsTvNo(String.valueOf(actCsList.get(i).getCsDt()));
													blCsList.add(actCsList.get(i));
												}
											}
											
											
											if(actCsList.get(i).getCsType().equalsIgnoreCase(ConstantsValues.MNY_RCT) && 
													actCsList.get(i).getCsVouchType().equalsIgnoreCase("BANK") &&
													actCsList.get(i).getCsFaCode().substring(0,2).equalsIgnoreCase("07")){
												if(!mrCsList.isEmpty()){
													boolean mrUpdate = false;
													for(int k=0;k<mrCsList.size();k++){
														Map<String,Object> mrBnkMap = mrCsList.get(k);
														if(String.valueOf(mrBnkMap.get("bankCode")).equalsIgnoreCase(actCsList.get(i).getCsFaCode())){
															logger.info("*********************** 1");
															List<CashStmt> mrBnkList = (List<CashStmt>) mrBnkMap.get("list");
															int vouchNo = actCsList.get(i).getCsVouchNo(); 
															for(int l=0;l<actCsList.size();l++){
																if(actCsList.get(l).getCsVouchNo() == vouchNo){
																	
																	boolean subCount = false;
																	for(int m=0;m<mrBnkList.size();m++){
																		logger.info("*********************** 2");
																		if(mrBnkList.get(m).getCsFaCode().equalsIgnoreCase(actCsList.get(l).getCsFaCode()) && 
																				mrBnkList.get(m).getCsDrCr() == actCsList.get(l).getCsDrCr()){
																			mrBnkList.get(m).setCsAmt(mrBnkList.get(m).getCsAmt() + actCsList.get(l).getCsAmt());
																			logger.info("*********************** 44 == "+actCsList.get(l).getCsFaCode());
																			subCount = true;
																			break;
																		}
																	}
																	
																	if(!subCount){
																		logger.info("*********************** 4 == "+actCsList.get(l).getCsFaCode());
																		actCsList.get(l).setCsTvNo(String.valueOf(actCsList.get(l).getCsDt()));
																		actCsList.get(l).setCsVouchNo(mrBnkList.get(0).getCsVouchNo());
																		mrBnkList.add(actCsList.get(l));
																	}
																	
																}
															}
															
															mrBnkMap.remove("list");
															mrBnkMap.put("list",mrBnkList);
															
															mrCsList.remove(k);
															mrCsList.add(k, mrBnkMap);
															
															mrUpdate = true;
															break;
														}
													}
													
													if(!mrUpdate){
														logger.info("*********************** 1 *******");
														Map<String,Object> mrBnkMap = new HashMap<String, Object>();
														List<CashStmt> mrBnkList = new ArrayList<CashStmt>();
														
														int vouchNo = actCsList.get(i).getCsVouchNo(); 
														mrBnkMap.put("bankCode", actCsList.get(i).getCsFaCode());
														for(int k=0;k<actCsList.size();k++){
															if(actCsList.get(k).getCsVouchNo() == vouchNo){
																actCsList.get(k).setCsTvNo(String.valueOf(actCsList.get(k).getCsDt()));
																mrBnkList.add(actCsList.get(k));
															}
														}
														mrBnkMap.put("list",mrBnkList);
														
														mrCsList.add(mrBnkMap);
													}
												}else{
													logger.info("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
													Map<String,Object> mrBnkMap = new HashMap<String, Object>();
													List<CashStmt> mrBnkList = new ArrayList<CashStmt>();
													
													int vouchNo = actCsList.get(i).getCsVouchNo(); 
													mrBnkMap.put("bankCode", actCsList.get(i).getCsFaCode());
													for(int k=0;k<actCsList.size();k++){
														if(actCsList.get(k).getCsVouchNo() == vouchNo){
															actCsList.get(k).setCsTvNo(String.valueOf(actCsList.get(k).getCsDt()));
															mrBnkList.add(actCsList.get(k));
														}
													}
													mrBnkMap.put("list",mrBnkList);
													
													mrCsList.add(mrBnkMap);
												}
											}
											
											
											if(actCsList.get(i).getCsType().equalsIgnoreCase("LHPV PAYMENT") && 
													 actCsList.get(i).getCsFaCode().substring(0,2).equalsIgnoreCase("07") &&
													 actCsList.get(i).getCsVouchType().equalsIgnoreCase("BANK")){
													if(!lhpvCsList.isEmpty()){
														boolean lhpvUpdate = false;
														for(int k=0;k<lhpvCsList.size();k++){
															Map<String,Object> lhpvBnkMap = lhpvCsList.get(k);
															if(String.valueOf(lhpvBnkMap.get("bankCode")).equalsIgnoreCase(actCsList.get(i).getCsFaCode())){
																logger.info("################################ 2");
																List<CashStmt> lhpvBnkList = (List<CashStmt>) lhpvBnkMap.get("list");
																int vouchNo = actCsList.get(i).getCsVouchNo(); 
																for(int l=0;l<actCsList.size();l++){
																	if(actCsList.get(l).getCsVouchNo() == vouchNo){
																		
																		boolean subCount = false;
																		for(int m=0;m<lhpvBnkList.size();m++){
																			logger.info("################################ 3");
																			if(lhpvBnkList.get(m).getCsFaCode().equalsIgnoreCase(actCsList.get(l).getCsFaCode()) && 
																					lhpvBnkList.get(m).getCsDrCr() == actCsList.get(l).getCsDrCr()){
																				lhpvBnkList.get(m).setCsAmt(lhpvBnkList.get(m).getCsAmt() + actCsList.get(l).getCsAmt());
																				logger.info("################################ 4 == "+actCsList.get(l).getCsFaCode());
																				subCount = true;
																				break;
																			}
																		}
																		
																		if(!subCount){
																			logger.info("################################ 5 == "+actCsList.get(l).getCsFaCode());
																			actCsList.get(l).setCsTvNo(String.valueOf(actCsList.get(l).getCsDt()));
																			actCsList.get(l).setCsVouchNo(lhpvBnkList.get(0).getCsVouchNo());
																			lhpvBnkList.add(actCsList.get(l));
																		}
																		
																	}
																}
																
																lhpvBnkMap.remove("list");
																lhpvBnkMap.put("list",lhpvBnkList);
																
																lhpvCsList.remove(k);
																lhpvCsList.add(k, lhpvBnkMap);
																
																lhpvUpdate = true;
																break;
															}
														}
														
														if(!lhpvUpdate){
															logger.info("################################ 6");
															Map<String,Object> lhpvBnkMap = new HashMap<String, Object>();
															List<CashStmt> lhpvBnkList = new ArrayList<CashStmt>();
															
															int vouchNo = actCsList.get(i).getCsVouchNo(); 
															lhpvBnkMap.put("bankCode", actCsList.get(i).getCsFaCode());
															for(int k=0;k<actCsList.size();k++){
																if(actCsList.get(k).getCsVouchNo() == vouchNo){
																	actCsList.get(k).setCsTvNo(String.valueOf(actCsList.get(k).getCsDt()));
																	lhpvBnkList.add(actCsList.get(k));
																}
															}
															lhpvBnkMap.put("list",lhpvBnkList);
															
															mrCsList.add(lhpvBnkMap);
														}
													}else{
														logger.info("################################## 7");
														Map<String,Object> lhpvBnkMap = new HashMap<String, Object>();
														List<CashStmt> lhpvBnkList = new ArrayList<CashStmt>();
														
														int vouchNo = actCsList.get(i).getCsVouchNo(); 
														lhpvBnkMap.put("bankCode", actCsList.get(i).getCsFaCode());
														for(int k=0;k<actCsList.size();k++){
															if(actCsList.get(k).getCsVouchNo() == vouchNo){
																actCsList.get(k).setCsTvNo(String.valueOf(actCsList.get(k).getCsDt()));
																lhpvBnkList.add(actCsList.get(k));
															}
														}
														lhpvBnkMap.put("list",lhpvBnkList);
														
														lhpvCsList.add(lhpvBnkMap);
													}
												}
										}
										
										for(int j=0;j<faMList.size();j++){
											Map<String,String> acHdMap = new HashMap<>();
											if(faMList.get(j).getFaMfaCode().substring(0,2).equalsIgnoreCase("07")){
												acHdMap.put("faCode",faMList.get(j).getFaMfaCode());
												acHdMap.put("faName",faMList.get(j).getFaMfaName());
												acHdList.add(acHdMap);
											}
										}
										
										
										//Remove multiple bill entries
										for(int k=0;k<actCsList.size();k++){
											if(actCsList.get(k).getCsType().equalsIgnoreCase("BILL")){
												actCsList.remove(k);
												k = k-1;
											}
										}
										
										//Add new consolidated bill entries
										if(!blCsList.isEmpty()){
											for(int k=0;k<blCsList.size();k++){
												actCsList.add(blCsList.get(k));
											}
										}
										
										
										//Remove multiple mr entries
										for(int k=0;k<actCsList.size();k++){
											if(actCsList.get(k).getCsType().equalsIgnoreCase(ConstantsValues.MNY_RCT) &&
													actCsList.get(k).getCsVouchType().equalsIgnoreCase("BANK")){
												actCsList.remove(k);
												k = k-1;
											}
										}
										
										
										//Add new consolidated mr entries
										if(!mrCsList.isEmpty()){
											logger.info("size of mrCsList = "+mrCsList.size());
											for(int k=0;k<mrCsList.size();k++){
												Map<String,Object> mrBnkMap = mrCsList.get(k);
												List<CashStmt> mrBnkList = (List<CashStmt>) mrBnkMap.get("list"); 
												if(!mrBnkList.isEmpty()){
													for(int l=0;l<mrBnkList.size();l++){
														actCsList.add(mrBnkList.get(l));
													}
												}
											}
										}
										
										//remove multiple LHPV bank entries
										for(int k=0;k<actCsList.size();k++){
											if(actCsList.get(k).getCsType().equalsIgnoreCase("LHPV PAYMENT") &&
													actCsList.get(k).getCsVouchType().equalsIgnoreCase("BANK")){
												actCsList.remove(k);
												k = k-1;
											}
										}
										
										
										//Add new consolidated lhpv bank entries
										if(!lhpvCsList.isEmpty()){
											logger.info("size of lhpvCsList = "+lhpvCsList.size());
											for(int k=0;k<lhpvCsList.size();k++){
												Map<String,Object> lhpvBnkMap = lhpvCsList.get(k);
												List<CashStmt> lhpvBnkList = (List<CashStmt>) lhpvBnkMap.get("list"); 
												if(!lhpvBnkList.isEmpty()){
													for(int l=0;l<lhpvBnkList.size();l++){
														if(lhpvBnkList.get(l).getCsVouchType().equalsIgnoreCase("BANK")){
															actCsList.add(lhpvBnkList.get(l));
														}
													}
												}
											}
										}
										
										
										List<Map<String,Object>> bkCsMapList = new ArrayList<>();
										
										for(int i=0;i<bcsList.size();i++){
											Map<String,Object> bkCsMap = new HashMap<>();
											List<CashStmt> bkCsList = new ArrayList<>();
											for(int j=0;j<actCsList.size();j++){
												if(actCsList.get(j).getCsType().equalsIgnoreCase("Fund Transfer") && bcsList.get(i).getBcsBankCode().equalsIgnoreCase(actCsList.get(j).getCsFaCode())){
													int vhNo = actCsList.get(j).getCsVouchNo();
													int count = 0;
													for(int k=0;k<actCsList.size();k++){
														if(actCsList.get(k).getCsVouchNo() == vhNo && actCsList.get(k).getCsFaCode().substring(0,2).equalsIgnoreCase("07")){
															count = count + 1;
														}
													}
													
													if(count > 1){
														bkCsList.add(actCsList.get(j+1));
													}else{
														for(int k=0;k<actCsList.size();k++){
															if(actCsList.get(k).getCsVouchNo() == vhNo && !actCsList.get(k).getCsFaCode().substring(0,2).equalsIgnoreCase("07")){
																if(actCsList.get(k).getCsVouchType().equalsIgnoreCase("BANK")){
																	bkCsList.add(actCsList.get(k));
																}
															}
														}
													}
													
													
												}else if(bcsList.get(i).getBcsBankCode().equalsIgnoreCase(actCsList.get(j).getCsFaCode())){
													int vhNo = actCsList.get(j).getCsVouchNo();
													for(int k=0;k<actCsList.size();k++){
														if(actCsList.get(k).getCsVouchNo() == vhNo && !actCsList.get(k).getCsFaCode().substring(0,2).equalsIgnoreCase("07")){
															if(actCsList.get(k).getCsVouchType().equalsIgnoreCase("BANK")){
																bkCsList.add(actCsList.get(k));
															}
														}
													}
												}
											}
											
											bkCsMap.put("bank",bcsList.get(i));
											bkCsMap.put("bkCsList", bkCsList);

												double recBnkAmt = bcsList.get(i).getBcsOpenBal();
												double payBnkAmt = bcsList.get(i).getBcsCloseBal();
												for(int k=0;k<bkCsList.size();k++){
													if(bkCsList.get(k).getCsDrCr() == 'D'){
														payBnkAmt = payBnkAmt + bkCsList.get(k).getCsAmt();
													}else{
														recBnkAmt = recBnkAmt + bkCsList.get(k).getCsAmt();
													}
												}
												bkCsMap.put("recBnkAmt",recBnkAmt);
												bkCsMap.put("payBnkAmt",payBnkAmt);

											bkCsMapList.add(bkCsMap);
										}
										
										
										if(!bkCsMapList.isEmpty()){
											for(int i=0;i<bkCsMapList.size();i++){
												Map<String,Object> ccMap = bkCsMapList.get(i) ;
												BankCS bankCS = (BankCS) ccMap.get("bank");
												List<CashStmt> bkCsList = (List<CashStmt>) ccMap.get("bkCsList");
												
												logger.info("*************************");
												logger.info("#### Bank Code = "+bankCS.getBcsBankCode());
												for(int j=0;j<bkCsList.size();j++){
													logger.info("%%%%%%%% ca FaCode = "+bkCsList.get(j).getCsFaCode());
												}
											}
										}
										map.put("bkCsMapList",bkCsMapList);
									}
									
									map.put("list", actCsList);
									map.put("acHdList", acHdList);
									map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
								}else{
									map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
								}
							} else {
								double closeBal = 0.0;
								double bkCloseBal = 0.0;
								closeBal = css.getCssOpenBal();
								bkCloseBal = css.getCssBkOpenBal();
								css.setCssCloseBal(closeBal);
								css.setCssBkCloseBal(bkCloseBal);
								int temp = cashStmtStatusDAO.closeCSS(css,session);
								
								List<BankCS> bcsList = 
										cashStmtStatusDAO.getBankCsByDt(cashStmtStatus.getCssDt(),Integer.parseInt(currentUser.getUserBranchCode()),session);
								logger.info("$$$$$$$$$$$$ size of bcsList = "+bcsList.size());
								if(temp > 0){
									List<CashStmt> actCsList  = cashStmtStatusDAO.getCSSById(id,session).get(0).getCashStmtList();
									List<Map<String,String>> acHdList = new ArrayList<>();
									List<FAMaster> faMList = faMasterDAO.getAllFAM(session);
									
									try {
										transaction.commit();
										session.close();
									} catch (Exception e) {
										logger.error(e.getMessage(), e);
										transaction.rollback();
									}
									
									if(!faMList.isEmpty() && !actCsList.isEmpty()){
										for(int i=0;i<actCsList.size();i++){
											Map<String,String> acHdMap = new HashMap<>();
											for(int j=0;j<faMList.size();j++){
												if(actCsList.get(i).getCsFaCode().equalsIgnoreCase(faMList.get(j).getFaMfaCode())){
													acHdMap.put("faCode",actCsList.get(i).getCsFaCode());
													acHdMap.put("faName",faMList.get(j).getFaMfaName());
													acHdList.add(acHdMap);
													break;
												}
											}
										}
									}
									for(int j=0;j<faMList.size();j++){
										Map<String,String> acHdMap = new HashMap<>();
										if(faMList.get(j).getFaMfaCode().substring(0,2).equalsIgnoreCase("07")){
											acHdMap.put("faCode",faMList.get(j).getFaMfaCode());
											acHdMap.put("faName",faMList.get(j).getFaMfaName());
											acHdList.add(acHdMap);
										}
									}
									
									List<Map<String,Object>> bkCsMapList = new ArrayList<>();
									
									for(int i=0;i<bcsList.size();i++){
										Map<String,Object> bkCsMap = new HashMap<>();
										bkCsMap.put("bank",bcsList.get(i));
										bkCsMapList.add(bkCsMap);
									}
									
									map.put("bkCsMapList",bkCsMapList);
									
									map.put("list", actCsList);
									map.put("acHdList", acHdList);
									map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
								}else{
									map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
								}
							}
						}else{
							map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
							map.put("msg", "Cashstmtstatus not found.");
						}	
					}else{
						map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
						map.put("msg", "Cashstmtstatus not found.");
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			transaction.rollback();
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			logger.info("Exception in closing cs"+e);
		}finally{
			if (session.isOpen())
				session.close();
		}
		return map;
	}

}