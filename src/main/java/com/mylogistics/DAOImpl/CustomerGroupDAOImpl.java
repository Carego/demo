package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.CustomerGroupDAO;
import com.mylogistics.constants.CustomerCNTS;
import com.mylogistics.constants.CustomerGroupCNTS;
import com.mylogistics.model.Customer;
import com.mylogistics.model.CustomerGroup;
import com.mylogistics.model.User;


public class CustomerGroupDAOImpl implements CustomerGroupDAO {
	
	public static Logger logger = Logger.getLogger(CustomerGroupDAOImpl.class);
	
	@Override
	public Map<String, Object> getCustAndCustGroup(Session session) {
		// TODO Auto-generated method stub
		System.out.println("enter getCustAndCustGroup..");
		Map<String,Object> map=new HashMap<String,Object>();
		List<Customer> customerList = new ArrayList<Customer>();
		List<CustomerGroup> custGroupList=new ArrayList<CustomerGroup>();
		
			Criteria criteria=session.createCriteria(Customer.class);
			       criteria.add(Restrictions.eq(CustomerCNTS.CUST_GROUP_ID,CustomerGroupCNTS.DEFAULT_CUST_GROUP_ID));
			       customerList=criteria.list();
			       
			criteria=session.createCriteria(CustomerGroup.class);
				custGroupList=criteria.list();
		
		map.put("customerList",customerList);
		map.put("custGroupList",custGroupList);
		return map;
	}
	
	
	@Override
	public void updateCustomerGroupId(Session session,String custGroupId, List<Integer> custListId) {
		// TODO Auto-generated method stub
		  List<Customer> list=new ArrayList<Customer>();
		      Criteria criteria =session.createCriteria(Customer.class);
		       criteria.add(Restrictions.in(CustomerCNTS.CUST_ID,custListId));
		       List<Customer> custList=criteria.list();
		       for(Customer cust:custList){
		    	    cust.setCustGroupId(custGroupId);
		    	    session.update(cust);
		       }
		    		
	}
	
	@Override
	public List<CustomerGroup> getAllCustGroupList(SessionFactory sessionFactory) {
		// TODO Auto-generated method stub
		Session session=null;
		List<CustomerGroup> customerGroupList=new ArrayList<CustomerGroup>();
		 try{
			 session=sessionFactory.openSession();		    
		     Criteria criteria=session.createCriteria(CustomerGroup.class);
		     customerGroupList=criteria.list();	    
		 }catch(Exception e){
			 e.printStackTrace();
		 }finally{
			 if(session!=null)
				 session.close();
		 }
		
		return customerGroupList;
	}
	
	@Override
	public List<CustomerGroup> getAllCustGroupList(Session session, User user) {
		logger.info("UserID = "+user.getUserId() + " : Enter into getAllCustGroupList()");	
		List<CustomerGroup> customerGroupList = null;
		 try{			 		    
		     customerGroupList = session.createCriteria(CustomerGroup.class)
		    		 .list();		     	    
		 }catch(Exception e){
			 logger.error("UserID = "+user.getUserId() + " : Exception = "+e);
		 }
		 logger.info("UserID = "+user.getUserId() + " : Exit from getAllCustGroupList() : Size = "+customerGroupList.size());
		return customerGroupList;
	}
	
	@Override
	public List<String> getCustCodesByGroup(Session session, User user, String groupId){
		logger.info("UserID = "+user.getUserId() + " : Enter into getCustCodesByGroup() : GroupID = "+groupId); 
		List<String> custCodes = null;
		try{
			custCodes = session.createCriteria(Customer.class)
					.add(Restrictions.eq(CustomerCNTS.CUST_GROUP_ID, groupId))
					.setProjection(Projections.projectionList()
								.add(Projections.property(CustomerCNTS.CUST_CODE))
							)					
					.list();
		}catch(Exception e){
			logger.error("UserID = "+user.getUserId() + " : Exception = "+e);
		}
		logger.info("UserID = "+user.getUserId() + " : Enter into getCustCodesByGroup() : CustomerSize = "+custCodes.size());
		return custCodes;
	}

}
