package com.mylogistics.constants;

public class HBO_StnNotificationCNTS {
	
	public static final String HBOSN_ID = "hBOSN_Id";
	
	public static final String HBOSN_CNMTNO = "hBOSN_cnmtNo";
	
	public static final String HBOSN_CHLNNO = "hBOSN_chlnNo";
	
	public static final String HBOSN_SEDRNO = "hBOSN_sedrNo";
	
	public static final String HBOSN_HASREC = "hBOSN_hasRec";
	
	public static final String HBOSN_OPCODE = "hBOSN_opCode";
	
	public static final String HBOSN_ISVIEW = "hBOSN_isView";
	
	public static final String HBOSN_ISCREATE = "hBOSN_isCreate";
	
	public static final String USER_CODE = "userCode";
	
	public static final String B_CODE = "bCode";

	public static final String CREATION_TS = "creationTS";
	
	
	
	
	
	
	

}
