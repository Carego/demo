package com.mylogistics.controller;

import java.io.EOFException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.EmployeeDAO;
import com.mylogistics.DAO.UserDAO;
import com.mylogistics.model.Branch;
import com.mylogistics.model.Employee;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;

@Controller
public class AsgnUserCntlr {

	@Autowired
	private UserDAO userDAO;
	
	@Autowired
	private BranchDAO branchDAO;
	
	@Autowired
	private EmployeeDAO employeeDAO;
	
	
	@RequestMapping(value="/getUserFrAsgn",method = RequestMethod.POST)
	public @ResponseBody Object getUserFrAsgn(){
		System.out.println("Entered into getUserFrAsgn function in controller--");
		Map<String,Object> map = new HashMap<String,Object>();
		List<User> userList = userDAO.getAllUser();
		/*List<Branch> branchList = branchDAO.getAllActiveBranches();*/
		
		if(userList.isEmpty()){
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			map.put("msg","User not avaliable");
		}
		
		/*if(branchList.isEmpty()){
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			map.put("msg","Branch not available");
		}*/
		
		if(!userList.isEmpty()){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("userList",userList);
		}
		
		return map;	
	}
	
	
	@RequestMapping(value="/getBrFrAsgn",method = RequestMethod.POST)
	public @ResponseBody Object getBrFrAsgn(@RequestBody String brCode){
		System.out.println("Entered into getBrFrAsgn function in controller--");
		Map<String,Object> map = new HashMap<String,Object>();
		if(brCode != null){
			List<Branch> branchList = branchDAO.retrieveBranch(brCode);
			if(!branchList.isEmpty()){
				Branch branch = branchList.get(0);
				List<Employee> empList = employeeDAO.getAllActiveEmployees();
				if(!empList.isEmpty()){
					map.put("empList",empList);
					map.put("msg","emp avaliable");
				}else{
					map.put("msg","emp not avaliable");
				}
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
				map.put("branch",branch);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
	
		return map;	
	}
	
	
	@RequestMapping(value="/submitAsgnUsr",method = RequestMethod.POST)
	public @ResponseBody Object submitAsgnUsr(@RequestBody Map<String,Object> clientMap){
		System.out.println("Entered into submitAsgnUsr function in controller--");
		Map<String,Object> map = new HashMap<String,Object>();
		int userId = (int) clientMap.get("userId");
		int empId = (int) clientMap.get("empId");
		int res = userDAO.setUserCode(userId,empId);
		if(res > 0){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;	
	}
}
