package com.mylogistics.pdf;

import java.sql.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class LhpvPDFBuilder extends AbstractLhpvPdfView{

	@SuppressWarnings({ "unused", "unchecked" })
	@Override
	protected void buildPdfDocument(Map<String, Object> map, Document doc,
			PdfWriter writer, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		System.out.println("entered into "+LhpvPDFBuilder.class);
		// get data model which is passed by the Spring container
		/*List<Map<String, Object>> ledgerMapList = (List<Map<String, Object>>) map.get("ledgerMapList");
		List<Map<String, String>> faCodeNameList = (List<Map<String, String>>) map.get("faCodeNameList");
		Map<String, Object> ledgerReportMap = (Map<String, Object>) map.get("ledgerReportMap");*/
		
		List<Map<String,Object>> lhpvAdvList = (List<Map<String,Object>>) map.get("laList");
		List<Map<String,Object>> lhpvBalList = (List<Map<String,Object>>) map.get("lbList");
		List<Map<String,Object>> lhpvSupList = (List<Map<String,Object>>) map.get("lspList");
		
		String brhName = (String) map.get("brhName");
		int lhpvNo = (int) map.get("lhpvNo");
		Date lhpvDt = (Date) map.get("lhpvDt");
		int shNo = (int) map.get("shNo");
		
		double totAdvPay = Math.round((double) map.get("totAdvPay"));
		double totBalPay = Math.round((double) map.get("totBalPay"));
		double totSupPay = Math.round((double) map.get("totSupPay"));
		
		double cashAdvPay = Math.round((double) map.get("cashAdvPay"));
		double chqAdvPay = Math.round((double) map.get("chqAdvPay"));
		double rtgsAdvPay = Math.round((double) map.get("rtgsAdvPay"));
		double petroAdvPay = Math.round((double) map.get("petroAdvPay"));
		
		double cashBalPay = Math.round((double) map.get("cashBalPay"));
		double chqBalPay = Math.round((double) map.get("chqBalPay"));
		double rtgsBalPay = Math.round((double) map.get("rtgsBalPay"));
		double petroBalPay = Math.round((double) map.get("petroBalPay"));
		
		double cashSupPay = Math.round((double) map.get("cashSupPay"));
		double chqSupPay = Math.round((double) map.get("chqSupPay"));
		double rtgsSupPay = Math.round((double) map.get("rtgsSupPay"));
		double petroSupPay = Math.round((double) map.get("petroSupPay"));
		
		
		Paragraph paragraph = new Paragraph();
		
		System.out.println("lhpvAdvList.size = "+lhpvAdvList.size());
		System.out.println("lhpvBalList.size = "+lhpvBalList.size());
		System.out.println("lhpvSupList.size = "+lhpvSupList.size());
		
		Font font = FontFactory.getFont(FontFactory.HELVETICA);
		font.setSize(12);
		
		Chunk cmpnyName = new Chunk("CAREGO LOGISTICS PVT LTD                                                                         " +
				"                                                Branch: "+brhName+"\n" +
				"LORRY HIRE PAYMENT SUMMARY                                                                                           " +
				"                      LHPV No.: " +lhpvNo+
				"                                                                                                                     " +
				"                                                                                            Date: " +lhpvDt+
				"                                                                                                                     " +
				"                                                                                     Sheet No.: "+shNo);
		//Chunk lhpvName = new Chunk("LORRY HIRE PAYMENT SUMMARY");
		
		paragraph.setAlignment(Element.ALIGN_LEFT);
		paragraph.setFont(font);
		paragraph.setSpacingAfter(10);
		paragraph.add(cmpnyName);
		//paragraph.add(lhpvName);
		
		doc.add(paragraph);
		
		/*doc.add(new Paragraph("LORRY HIRE PAYMENT SUMMARY"));*/
		
		PdfPTable table = new PdfPTable(11);
		table.setWidthPercentage(100.0f);
		table.setWidths(new float[] {0.3f, 1.0f, 1.5f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.2f});
		table.setSpacingBefore(8);
		
		PdfPTable table2 = new PdfPTable(21);
		table2.setWidthPercentage(100.0f);
		table2.setWidths(new float[] {0.5f, 1.2f , 1.2f, 1.2f, 0.7f, 1.2f, 0.9f, 0.9f, 0.9f, 0.9f, 0.9f, 0.9f, 0.9f, 0.9f,
				0.9f, 0.9f, 0.9f, 0.9f, 0.9f, 0.9f, 1.3f});
		table2.setSpacingBefore(8);
		
		PdfPTable table3 = new PdfPTable(19);
		table3.setWidthPercentage(100.0f);
		table3.setWidths(new float[] {0.5f, 1.2f , 1.5f, 1.1f, 1.0f, 1.0f, 0.9f, 0.9f,0.9f, 0.9f, 0.9f, 0.9f,
				0.9f, 0.9f, 0.9f, 0.9f, 1.2f, 1.2f, 1.3f});
		table3.setSpacingBefore(8);
		
		// define font for table header row
		Font headerfont = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
		headerfont.setSize(15);
		headerfont.setColor(BaseColor.WHITE);
		
		// define table header cell
		PdfPCell headerCell1 = new PdfPCell();
		headerCell1.setBackgroundColor(BaseColor.DARK_GRAY);
		headerCell1.setPaddingTop(5);
		headerCell1.setPaddingBottom(5);
		headerCell1.setPaddingLeft(2);
		headerCell1.setPaddingRight(2);
		headerCell1.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		headerCell1.setColspan(11);
		
		headerCell1.setPhrase(new Phrase("LHPV ADVANCE", headerfont));
		table.addCell(headerCell1);
		
		
		
		PdfPCell headerCell2 = new PdfPCell();
		headerCell2.setBackgroundColor(BaseColor.DARK_GRAY);
		headerCell2.setPaddingTop(5);
		headerCell2.setPaddingBottom(5);
		headerCell2.setPaddingLeft(2);
		headerCell2.setPaddingRight(2);
		headerCell2.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		headerCell2.setColspan(21);
		
		headerCell2.setPhrase(new Phrase("LHPV BALANCE", headerfont));
		table2.addCell(headerCell2);
		
		
		PdfPCell headerCell3 = new PdfPCell();
		headerCell3.setBackgroundColor(BaseColor.DARK_GRAY);
		headerCell3.setPaddingTop(5);
		headerCell3.setPaddingBottom(5);
		headerCell3.setPaddingLeft(2);
		headerCell3.setPaddingRight(2);
		headerCell3.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		headerCell3.setColspan(19);
		
		headerCell3.setPhrase(new Phrase("LHPV SUPPLEMENTARY", headerfont));
		table3.addCell(headerCell3);
		
		
		//Lhpv advance footer
		PdfPCell footerCellColSpan10 = new PdfPCell();
		footerCellColSpan10.setBackgroundColor(BaseColor.LIGHT_GRAY);
		footerCellColSpan10.setHorizontalAlignment(Element.ALIGN_CENTER);
		footerCellColSpan10.setPaddingTop(2);
		footerCellColSpan10.setPaddingBottom(2);
		footerCellColSpan10.setColspan(10);
		
		PdfPCell footerCellColSpan1 = new PdfPCell();
		footerCellColSpan1.setBackgroundColor(BaseColor.LIGHT_GRAY);
		footerCellColSpan1.setHorizontalAlignment(Element.ALIGN_CENTER);
		footerCellColSpan1.setPaddingBottom(2);
		footerCellColSpan1.setPaddingTop(2);
		footerCellColSpan1.setColspan(1);
		
		Font footerfont = FontFactory.getFont(FontFactory.TIMES);
		footerfont.setSize(11);
		footerfont.setColor(BaseColor.BLACK);
		
		PdfPCell lhpvAdvDetColSpan4 = new PdfPCell();
		lhpvAdvDetColSpan4.setBackgroundColor(BaseColor.LIGHT_GRAY);
		lhpvAdvDetColSpan4.setHorizontalAlignment(Element.ALIGN_CENTER);
		lhpvAdvDetColSpan4.setPaddingTop(2);
		lhpvAdvDetColSpan4.setPaddingBottom(2);
		lhpvAdvDetColSpan4.setColspan(4);
		
		PdfPCell lhpvAdvDetColSpan3 = new PdfPCell();
		lhpvAdvDetColSpan3.setBackgroundColor(BaseColor.LIGHT_GRAY);
		lhpvAdvDetColSpan3.setHorizontalAlignment(Element.ALIGN_CENTER);
		lhpvAdvDetColSpan3.setPaddingBottom(2);
		lhpvAdvDetColSpan3.setPaddingTop(2);
		lhpvAdvDetColSpan3.setColspan(3);
		
		PdfPCell lhpvBalDetColSpan5 = new PdfPCell();
		lhpvBalDetColSpan5.setBackgroundColor(BaseColor.LIGHT_GRAY);
		lhpvBalDetColSpan5.setHorizontalAlignment(Element.ALIGN_CENTER);
		lhpvBalDetColSpan5.setPaddingBottom(2);
		lhpvBalDetColSpan5.setPaddingTop(2);
		lhpvBalDetColSpan5.setColspan(5);
		
		PdfPCell lhpvBalDetColSpan6 = new PdfPCell();
		lhpvBalDetColSpan6.setBackgroundColor(BaseColor.LIGHT_GRAY);
		lhpvBalDetColSpan6.setHorizontalAlignment(Element.ALIGN_CENTER);
		lhpvBalDetColSpan6.setPaddingBottom(2);
		lhpvBalDetColSpan6.setPaddingTop(2);
		lhpvBalDetColSpan6.setColspan(6);
		
		PdfPCell lhpvBalDetColSpan10 = new PdfPCell();
		lhpvBalDetColSpan10.setBackgroundColor(BaseColor.LIGHT_GRAY);
		lhpvBalDetColSpan10.setHorizontalAlignment(Element.ALIGN_CENTER);
		lhpvBalDetColSpan10.setPaddingBottom(2);
		lhpvBalDetColSpan10.setPaddingTop(2);
		lhpvBalDetColSpan10.setColspan(10);
		
		PdfPCell lhpvBalDetColSpan7 = new PdfPCell();
		lhpvBalDetColSpan7.setBackgroundColor(BaseColor.LIGHT_GRAY);
		lhpvBalDetColSpan7.setHorizontalAlignment(Element.ALIGN_CENTER);
		lhpvBalDetColSpan7.setPaddingBottom(2);
		lhpvBalDetColSpan7.setPaddingTop(2);
		lhpvBalDetColSpan7.setColspan(7);
		
		PdfPCell lhpvSupDetColSpan4 = new PdfPCell();
		lhpvSupDetColSpan4.setBackgroundColor(BaseColor.LIGHT_GRAY);
		lhpvSupDetColSpan4.setHorizontalAlignment(Element.ALIGN_CENTER);
		lhpvSupDetColSpan4.setPaddingBottom(2);
		lhpvSupDetColSpan4.setPaddingTop(2);
		lhpvSupDetColSpan4.setColspan(4);
		
		PdfPCell lhpvSupDetColSpan5 = new PdfPCell();
		lhpvSupDetColSpan5.setBackgroundColor(BaseColor.LIGHT_GRAY);
		lhpvSupDetColSpan5.setHorizontalAlignment(Element.ALIGN_CENTER);
		lhpvSupDetColSpan5.setPaddingBottom(2);
		lhpvSupDetColSpan5.setPaddingTop(2);
		lhpvSupDetColSpan5.setColspan(5);
		
		PdfPCell lhpvSupDetColSpan7 = new PdfPCell();
		lhpvSupDetColSpan7.setBackgroundColor(BaseColor.LIGHT_GRAY);
		lhpvSupDetColSpan7.setHorizontalAlignment(Element.ALIGN_CENTER);
		lhpvSupDetColSpan7.setPaddingBottom(2);
		lhpvSupDetColSpan7.setPaddingTop(2);
		lhpvSupDetColSpan7.setColspan(10);
		
		PdfPCell footerCellColSpan20 = new PdfPCell();
		footerCellColSpan20.setBackgroundColor(BaseColor.LIGHT_GRAY);
		footerCellColSpan20.setHorizontalAlignment(Element.ALIGN_CENTER);
		footerCellColSpan20.setPaddingBottom(2);
		footerCellColSpan20.setPaddingTop(2);
		footerCellColSpan20.setColspan(20);
		
		PdfPCell footerCellColSpan18 = new PdfPCell();
		footerCellColSpan18.setBackgroundColor(BaseColor.LIGHT_GRAY);
		footerCellColSpan18.setHorizontalAlignment(Element.ALIGN_CENTER);
		footerCellColSpan18.setPaddingBottom(2);
		footerCellColSpan18.setPaddingTop(2);
		footerCellColSpan18.setColspan(18);
		
		/*PdfPCell headerCell2 = new PdfPCell();
		headerCell2.setBackgroundColor(BaseColor.DARK_GRAY);
		headerCell2.setColspan(2);
		headerCell2.setPaddingTop(5);
		headerCell2.setPaddingBottom(5);
		headerCell2.setPaddingLeft(2);
		headerCell2.setPaddingRight(2);
		headerCell2.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell headerCell3 = new PdfPCell();
		headerCell3.setBackgroundColor(BaseColor.DARK_GRAY);
		headerCell3.setColspan(1);
		headerCell3.setPaddingTop(5);
		headerCell3.setPaddingBottom(5);
		headerCell3.setPaddingLeft(2);
		headerCell3.setPaddingRight(2);
		headerCell3.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell headerCell4 = new PdfPCell();
		headerCell4.setBackgroundColor(BaseColor.DARK_GRAY);
		headerCell4.setColspan(2);
		headerCell4.setPaddingTop(5);
		headerCell4.setPaddingBottom(5);
		headerCell4.setPaddingLeft(2);
		headerCell4.setPaddingRight(2);
		headerCell4.setHorizontalAlignment(Element.ALIGN_CENTER);*/
		
		
		
		// write table header 
		
		/*headerCell2.setPhrase(new Phrase("TO DT: "+CodePatternService.getFormatedDateString((String.valueOf(ledgerReportMap.get("toDt")))), headerfont));
		table.addCell(headerCell2);
		headerCell3.setPhrase(new Phrase("FROM FA: "+ledgerReportMap.get("fromFaCode"), headerfont));
		table.addCell(headerCell3);
		headerCell4.setPhrase(new Phrase("TO FA: "+ledgerReportMap.get("toFaCode"), headerfont));
		table.addCell(headerCell4);*/
		
/*		//define cell for faCode
		PdfPCell cellFaCode = new PdfPCell();
		cellFaCode.setBackgroundColor(new BaseColor(38, 166, 154));
		cellFaCode.setColspan(4);
		cellFaCode.setPaddingTop(5);
		cellFaCode.setPaddingBottom(5);
		cellFaCode.setPaddingLeft(5);
		cellFaCode.setPaddingRight(5);
		cellFaCode.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		//define cell for faName
		PdfPCell cellFaName = new PdfPCell();
		cellFaName.setBackgroundColor(new BaseColor(38, 166, 154));
		cellFaName.setColspan(4);
		cellFaName.setPaddingTop(5);
		cellFaName.setPaddingBottom(5);
		cellFaName.setPaddingLeft(5);
		cellFaName.setPaddingRight(5);
		cellFaName.setHorizontalAlignment(Element.ALIGN_CENTER);*/
		
		// define font for table header row
		/*Font faCodeNamefont = FontFactory.getFont(FontFactory.COURIER_BOLD);
		faCodeNamefont.setSize(12);
		faCodeNamefont.setColor(BaseColor.WHITE)*/;
		
		PdfPCell cellCSHeader = new PdfPCell();
		cellCSHeader.setColspan(1);
		cellCSHeader.setPaddingTop(2);
		cellCSHeader.setPaddingBottom(2);
		cellCSHeader.setBackgroundColor(BaseColor.LIGHT_GRAY);
		cellCSHeader.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		Font cellCSHeaderFont = FontFactory.getFont(FontFactory.TIMES);
		cellCSHeaderFont.setSize(9);
		cellCSHeaderFont.setColor(BaseColor.BLUE);
		
		Font cellCSFont = FontFactory.getFont(FontFactory.TIMES);
		cellCSFont.setSize(9);
		cellCSFont.setColor(BaseColor.BLACK);
		
		Font cellCSHeaderT2Font = FontFactory.getFont(FontFactory.TIMES);
		cellCSHeaderT2Font.setSize(8);
		cellCSHeaderT2Font.setColor(BaseColor.BLUE);
		
		Font cellCST2Font = FontFactory.getFont(FontFactory.TIMES);
		cellCST2Font.setSize(7);
		cellCST2Font.setColor(BaseColor.BLACK);
		
		PdfPCell cellColSpan1 = new PdfPCell();
		cellColSpan1.setColspan(1);
		cellColSpan1.setPaddingBottom(2);
		cellColSpan1.setPaddingTop(2);
		cellColSpan1.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		/*PdfPCell cellColSpan6 = new PdfPCell();
		cellColSpan6.setColspan(6);
		cellColSpan6.setBackgroundColor(BaseColor.LIGHT_GRAY);
		cellColSpan6.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		Font cellFont8 = FontFactory.getFont(FontFactory.TIMES);
		cellFont8.setSize(10);
		cellFont8.setColor(new BaseColor(128,0,128));
		
		PdfPCell cellColSpan8 = new PdfPCell();
		cellColSpan8.setColspan(8);
		cellColSpan8.setHorizontalAlignment(Element.ALIGN_CENTER);*/
				
		// write table row for FaCodeName
		//for (Map<String, String> faCodeNameMap : faCodeNameList) {
			/*cellFaCode.setPhrase(new Phrase("FaCode: "+faCodeNameMap.get("csFaCode"), faCodeNamefont));
			table.addCell(cellFaCode);
			cellFaName.setPhrase(new Phrase("FaName: "+faCodeNameMap.get("csFaName"), faCodeNamefont));
			table.addCell(cellFaName);*/

			cellCSHeader.setPhrase(new Phrase("PM", cellCSHeaderFont));
			table.addCell(cellCSHeader);
			
			cellCSHeader.setPhrase(new Phrase("CHQ NO", cellCSHeaderFont));
			table.addCell(cellCSHeader);
			
			cellCSHeader.setPhrase(new Phrase("BANK NAME", cellCSHeaderFont));
			table.addCell(cellCSHeader);
			
			cellCSHeader.setPhrase(new Phrase("BANK CODE", cellCSHeaderFont));
			table.addCell(cellCSHeader);
			
			cellCSHeader.setPhrase(new Phrase("CHQ AMT", cellCSHeaderFont));
			table.addCell(cellCSHeader);
			
			cellCSHeader.setPhrase(new Phrase("CHLN NO", cellCSHeaderFont));
			table.addCell(cellCSHeader);
			
			cellCSHeader.setPhrase(new Phrase("ADV", cellCSHeaderFont));
			table.addCell(cellCSHeader);
			
			cellCSHeader.setPhrase(new Phrase("CS_DIS", cellCSHeaderFont));
			table.addCell(cellCSHeader);

			cellCSHeader.setPhrase(new Phrase("MUNS", cellCSHeaderFont));
			table.addCell(cellCSHeader);
			
			cellCSHeader.setPhrase(new Phrase("TDS", cellCSHeaderFont));
			table.addCell(cellCSHeader);
			
			cellCSHeader.setPhrase(new Phrase("GRAND TOTAL", cellCSHeaderFont));
			table.addCell(cellCSHeader);
			
			
			for (int i=0;i<lhpvAdvList.size();i++) {
				//if (String.valueOf(faCodeNameMap.get("csFaCode")).equalsIgnoreCase(String.valueOf(ledgerMap.get("csFaCode")))) {
					cellColSpan1.setPhrase(new Phrase(String.valueOf(lhpvAdvList.get(i).get("payM")), cellCSFont));
					table.addCell(cellColSpan1);
					
					cellColSpan1.setPhrase(new Phrase(String.valueOf(lhpvAdvList.get(i).get("chqNo")), cellCSFont));
					table.addCell(cellColSpan1);
					
					cellColSpan1.setPhrase(new Phrase(String.valueOf(lhpvAdvList.get(i).get("bnkName")), cellCSFont));
					table.addCell(cellColSpan1);
					
					cellColSpan1.setPhrase(new Phrase(String.valueOf(lhpvAdvList.get(i).get("bankCode")), cellCSFont));
					table.addCell(cellColSpan1);
					
					cellColSpan1.setPhrase(new Phrase(String.valueOf(lhpvAdvList.get(i).get("chqAmt")), cellCSFont));
					table.addCell(cellColSpan1);
					
					cellColSpan1.setPhrase(new Phrase(String.valueOf(lhpvAdvList.get(i).get("chlnNo")), cellCSFont));
					table.addCell(cellColSpan1);
					
					cellColSpan1.setPhrase(new Phrase(String.valueOf(lhpvAdvList.get(i).get("advAmt")), cellCSFont));
					table.addCell(cellColSpan1);
					
					cellColSpan1.setPhrase(new Phrase(String.valueOf(lhpvAdvList.get(i).get("csDis")), cellCSFont));
					table.addCell(cellColSpan1);
					
					cellColSpan1.setPhrase(new Phrase(String.valueOf(lhpvAdvList.get(i).get("muns")), cellCSFont));
					table.addCell(cellColSpan1);
					
					cellColSpan1.setPhrase(new Phrase(String.valueOf(lhpvAdvList.get(i).get("tds")), cellCSFont));
					table.addCell(cellColSpan1);
					
					cellColSpan1.setPhrase(new Phrase(String.valueOf(lhpvAdvList.get(i).get("gdTot")), cellCSFont));
					table.addCell(cellColSpan1);
					
				//}
			}
			
			footerCellColSpan10.setPhrase(new Phrase("Total Pay", footerfont));
			table.addCell(footerCellColSpan10);
			
			footerCellColSpan1.setPhrase(new Phrase(String.valueOf(totAdvPay), footerfont));
			table.addCell(footerCellColSpan1);
			
			lhpvAdvDetColSpan3.setPhrase(new Phrase("Cash Payment: "+String.valueOf(cashAdvPay), footerfont));
			table.addCell(lhpvAdvDetColSpan3);
			
			lhpvAdvDetColSpan3.setPhrase(new Phrase("Cheque Payment: "+String.valueOf(chqAdvPay), footerfont));
			table.addCell(lhpvAdvDetColSpan3);
			
			lhpvAdvDetColSpan3.setPhrase(new Phrase("RTGS Payment: "+String.valueOf(rtgsAdvPay), footerfont));
			table.addCell(lhpvAdvDetColSpan3);
			
			lhpvAdvDetColSpan3.setPhrase(new Phrase("PETRO Payment: "+String.valueOf(petroAdvPay), footerfont));
			table.addCell(lhpvAdvDetColSpan3);
			
			cellCSHeader.setPhrase(new Phrase("PM", cellCSHeaderT2Font));
			table2.addCell(cellCSHeader);
			
			cellCSHeader.setPhrase(new Phrase("CHQ NO", cellCSHeaderT2Font));
			table2.addCell(cellCSHeader);
			
			cellCSHeader.setPhrase(new Phrase("BANK NAME", cellCSHeaderT2Font));
			table2.addCell(cellCSHeader);
			
			cellCSHeader.setPhrase(new Phrase("BANK CODE", cellCSHeaderT2Font));
			table2.addCell(cellCSHeader);
			
			cellCSHeader.setPhrase(new Phrase("CHQ AMT", cellCSHeaderT2Font));
			table2.addCell(cellCSHeader);
			
			cellCSHeader.setPhrase(new Phrase("CHLN NO", cellCSHeaderT2Font));
			table2.addCell(cellCSHeader);
			
			cellCSHeader.setPhrase(new Phrase("BAL", cellCSHeaderT2Font));
			table2.addCell(cellCSHeader);
			
			cellCSHeader.setPhrase(new Phrase("CS_DIS", cellCSHeaderT2Font));
			table2.addCell(cellCSHeader);

			cellCSHeader.setPhrase(new Phrase("MUNS", cellCSHeaderT2Font));
			table2.addCell(cellCSHeader);
			
			cellCSHeader.setPhrase(new Phrase("TDS", cellCSHeaderT2Font));
			table2.addCell(cellCSHeader);
			
			cellCSHeader.setPhrase(new Phrase("WT_SH", cellCSHeaderT2Font));
			table2.addCell(cellCSHeader);
			
			/*cellCSHeader.setPhrase(new Phrase("DR_C", cellCSHeaderT2Font));
			table2.addCell(cellCSHeader);*/
			cellCSHeader.setPhrase(new Phrase("L_REC", cellCSHeaderT2Font));
			table2.addCell(cellCSHeader);
			
			cellCSHeader.setPhrase(new Phrase("LT_DLY", cellCSHeaderT2Font));
			table2.addCell(cellCSHeader);
			
			cellCSHeader.setPhrase(new Phrase("LT_ACK", cellCSHeaderT2Font));
			table2.addCell(cellCSHeader);
			
			cellCSHeader.setPhrase(new Phrase("EX_KM", cellCSHeaderT2Font));
			table2.addCell(cellCSHeader);
			
			cellCSHeader.setPhrase(new Phrase("OV_HET", cellCSHeaderT2Font));
			table2.addCell(cellCSHeader);
			
			cellCSHeader.setPhrase(new Phrase("PNTY", cellCSHeaderT2Font));
			table2.addCell(cellCSHeader);
			
			cellCSHeader.setPhrase(new Phrase("MISC", cellCSHeaderT2Font));
			table2.addCell(cellCSHeader);
			
			cellCSHeader.setPhrase(new Phrase("DET", cellCSHeaderT2Font));
			table2.addCell(cellCSHeader);
			
			cellCSHeader.setPhrase(new Phrase("UNL", cellCSHeaderT2Font));
			table2.addCell(cellCSHeader);
			
			cellCSHeader.setPhrase(new Phrase("GRAND TOTAL", cellCSHeaderT2Font));
			table2.addCell(cellCSHeader);
			
			for (int i=0;i<lhpvBalList.size();i++) {
				
					cellColSpan1.setPhrase(new Phrase(String.valueOf(lhpvBalList.get(i).get("payM")), cellCST2Font));
					table2.addCell(cellColSpan1);
					
					cellColSpan1.setPhrase(new Phrase(String.valueOf(lhpvBalList.get(i).get("chqNo")), cellCST2Font));
					table2.addCell(cellColSpan1);
					
					cellColSpan1.setPhrase(new Phrase(String.valueOf(lhpvBalList.get(i).get("bnkName")), cellCST2Font));
					table2.addCell(cellColSpan1);
					
					cellColSpan1.setPhrase(new Phrase(String.valueOf(lhpvBalList.get(i).get("bankCode")), cellCST2Font));
					table2.addCell(cellColSpan1);
					
					cellColSpan1.setPhrase(new Phrase(String.valueOf(lhpvBalList.get(i).get("chqAmt")), cellCST2Font));
					table2.addCell(cellColSpan1);
					
					cellColSpan1.setPhrase(new Phrase(String.valueOf(lhpvBalList.get(i).get("chlnNo")), cellCST2Font));
					table2.addCell(cellColSpan1);
					
					cellColSpan1.setPhrase(new Phrase(String.valueOf(lhpvBalList.get(i).get("balAmt")), cellCST2Font));
					table2.addCell(cellColSpan1);
					
					cellColSpan1.setPhrase(new Phrase(String.valueOf(lhpvBalList.get(i).get("csDis")), cellCST2Font));
					table2.addCell(cellColSpan1);
					
					cellColSpan1.setPhrase(new Phrase(String.valueOf(lhpvBalList.get(i).get("muns")), cellCST2Font));
					table2.addCell(cellColSpan1);
					
					cellColSpan1.setPhrase(new Phrase(String.valueOf(lhpvBalList.get(i).get("tds")), cellCST2Font));
					table2.addCell(cellColSpan1);
					
					cellColSpan1.setPhrase(new Phrase(String.valueOf(lhpvBalList.get(i).get("wtSrt")), cellCST2Font));
					table2.addCell(cellColSpan1);
					
					cellColSpan1.setPhrase(new Phrase(String.valueOf(lhpvBalList.get(i).get("drCl")), cellCST2Font));
					table2.addCell(cellColSpan1);
					
					cellColSpan1.setPhrase(new Phrase(String.valueOf(lhpvBalList.get(i).get("ltDel")), cellCST2Font));
					table2.addCell(cellColSpan1);
					
					cellColSpan1.setPhrase(new Phrase(String.valueOf(lhpvBalList.get(i).get("ltAck")), cellCST2Font));
					table2.addCell(cellColSpan1);
					
					cellColSpan1.setPhrase(new Phrase(String.valueOf(lhpvBalList.get(i).get("extKm")), cellCST2Font));
					table2.addCell(cellColSpan1);
					
					cellColSpan1.setPhrase(new Phrase(String.valueOf(lhpvBalList.get(i).get("ovrHt")), cellCST2Font));
					table2.addCell(cellColSpan1);
					
					cellColSpan1.setPhrase(new Phrase(String.valueOf(lhpvBalList.get(i).get("pen")), cellCST2Font));
					table2.addCell(cellColSpan1);
					
					cellColSpan1.setPhrase(new Phrase(String.valueOf(lhpvBalList.get(i).get("misc")), cellCST2Font));
					table2.addCell(cellColSpan1);
					
					cellColSpan1.setPhrase(new Phrase(String.valueOf(lhpvBalList.get(i).get("det")), cellCST2Font));
					table2.addCell(cellColSpan1);
					
					cellColSpan1.setPhrase(new Phrase(String.valueOf(lhpvBalList.get(i).get("unld")), cellCST2Font));
					table2.addCell(cellColSpan1);
					
					cellColSpan1.setPhrase(new Phrase(String.valueOf(lhpvBalList.get(i).get("gdTot")), cellCST2Font));
					table2.addCell(cellColSpan1);
					
				
			}
			
			footerCellColSpan20.setPhrase(new Phrase("Total Pay", footerfont));
			table2.addCell(footerCellColSpan20);
			
			footerCellColSpan1.setPhrase(new Phrase(String.valueOf(totBalPay), footerfont));
			table2.addCell(footerCellColSpan1);
			
			lhpvBalDetColSpan6.setPhrase(new Phrase("Cash Payment: "+String.valueOf(cashBalPay), footerfont));
			table2.addCell(lhpvBalDetColSpan6);
			
			lhpvBalDetColSpan5.setPhrase(new Phrase("Cheque Payment: "+String.valueOf(chqBalPay), footerfont));
			table2.addCell(lhpvBalDetColSpan5);
			
			lhpvBalDetColSpan5.setPhrase(new Phrase("RTGS Payment: "+String.valueOf(rtgsBalPay), footerfont));
			table2.addCell(lhpvBalDetColSpan5);
			
			lhpvBalDetColSpan5.setPhrase(new Phrase("PETRO Payment: "+String.valueOf(petroBalPay), footerfont));
			table2.addCell(lhpvBalDetColSpan5);
			
			cellCSHeader.setPhrase(new Phrase("PM", cellCSHeaderT2Font));
			table3.addCell(cellCSHeader);
			
			cellCSHeader.setPhrase(new Phrase("CHQ NO", cellCSHeaderT2Font));
			table3.addCell(cellCSHeader);
			
			cellCSHeader.setPhrase(new Phrase("BANK NAME", cellCSHeaderT2Font));
			table3.addCell(cellCSHeader);
			
			cellCSHeader.setPhrase(new Phrase("BANK CODE", cellCSHeaderT2Font));
			table3.addCell(cellCSHeader);
			
			cellCSHeader.setPhrase(new Phrase("CHQ AMT", cellCSHeaderT2Font));
			table3.addCell(cellCSHeader);
			
			cellCSHeader.setPhrase(new Phrase("CHLN NO", cellCSHeaderT2Font));
			table3.addCell(cellCSHeader);
			
			cellCSHeader.setPhrase(new Phrase("WT_SH", cellCSHeaderT2Font));
			table3.addCell(cellCSHeader);
			
			cellCSHeader.setPhrase(new Phrase("L_REC", cellCSHeaderT2Font));
			table3.addCell(cellCSHeader);
			
			cellCSHeader.setPhrase(new Phrase("LT_DLY", cellCSHeaderT2Font));
			table3.addCell(cellCSHeader);
			
			cellCSHeader.setPhrase(new Phrase("LT_ACK", cellCSHeaderT2Font));
			table3.addCell(cellCSHeader);
			
			cellCSHeader.setPhrase(new Phrase("EX_KM", cellCSHeaderT2Font));
			table3.addCell(cellCSHeader);
			
			cellCSHeader.setPhrase(new Phrase("OV_HET", cellCSHeaderT2Font));
			table3.addCell(cellCSHeader);
			
			cellCSHeader.setPhrase(new Phrase("PNTY", cellCSHeaderT2Font));
			table3.addCell(cellCSHeader);
			
			cellCSHeader.setPhrase(new Phrase("MISC", cellCSHeaderT2Font));
			table3.addCell(cellCSHeader);
			
			cellCSHeader.setPhrase(new Phrase("DET", cellCSHeaderT2Font));
			table3.addCell(cellCSHeader);
			
			cellCSHeader.setPhrase(new Phrase("UNL", cellCSHeaderT2Font));
			table3.addCell(cellCSHeader);
			
			cellCSHeader.setPhrase(new Phrase("F_RCVR", cellCSHeaderT2Font));
			table3.addCell(cellCSHeader);
			
			cellCSHeader.setPhrase(new Phrase("F_PAY", cellCSHeaderT2Font));
			table3.addCell(cellCSHeader);
			
			cellCSHeader.setPhrase(new Phrase("GRAND TOTAL", cellCSHeaderT2Font));
			table3.addCell(cellCSHeader);
			
			
			for (int i=0;i<lhpvSupList.size();i++) {
				
				cellColSpan1.setPhrase(new Phrase(String.valueOf(lhpvSupList.get(i).get("payM")), cellCSFont));
				table3.addCell(cellColSpan1);
				
				cellColSpan1.setPhrase(new Phrase(String.valueOf(lhpvSupList.get(i).get("chqNo")), cellCSFont));
				table3.addCell(cellColSpan1);
				
				cellColSpan1.setPhrase(new Phrase(String.valueOf(lhpvSupList.get(i).get("bnkName")), cellCSFont));
				table3.addCell(cellColSpan1);
				
				cellColSpan1.setPhrase(new Phrase(String.valueOf(lhpvSupList.get(i).get("bankCode")), cellCSFont));
				table3.addCell(cellColSpan1);
				
				cellColSpan1.setPhrase(new Phrase(String.valueOf(lhpvSupList.get(i).get("chqAmt")), cellCSFont));
				table3.addCell(cellColSpan1);
				
				cellColSpan1.setPhrase(new Phrase(String.valueOf(lhpvSupList.get(i).get("chlnNo")), cellCSFont));
				table3.addCell(cellColSpan1);
				
				cellColSpan1.setPhrase(new Phrase(String.valueOf(lhpvSupList.get(i).get("wtSrt")), cellCSFont));
				table3.addCell(cellColSpan1);
				
				cellColSpan1.setPhrase(new Phrase(String.valueOf(lhpvSupList.get(i).get("drCl")), cellCSFont));
				table3.addCell(cellColSpan1);
				
				cellColSpan1.setPhrase(new Phrase(String.valueOf(lhpvSupList.get(i).get("ltDel")), cellCSFont));
				table3.addCell(cellColSpan1);
				
				cellColSpan1.setPhrase(new Phrase(String.valueOf(lhpvSupList.get(i).get("ltAck")), cellCSFont));
				table3.addCell(cellColSpan1);
				
				cellColSpan1.setPhrase(new Phrase(String.valueOf(lhpvSupList.get(i).get("extKm")), cellCSFont));
				table3.addCell(cellColSpan1);
				
				cellColSpan1.setPhrase(new Phrase(String.valueOf(lhpvSupList.get(i).get("ovrHt")), cellCSFont));
				table3.addCell(cellColSpan1);
				
				cellColSpan1.setPhrase(new Phrase(String.valueOf(lhpvSupList.get(i).get("pen")), cellCSFont));
				table3.addCell(cellColSpan1);
				
				cellColSpan1.setPhrase(new Phrase(String.valueOf(lhpvSupList.get(i).get("misc")), cellCSFont));
				table3.addCell(cellColSpan1);
				
				cellColSpan1.setPhrase(new Phrase(String.valueOf(lhpvSupList.get(i).get("det")), cellCSFont));
				table3.addCell(cellColSpan1);
				
				cellColSpan1.setPhrase(new Phrase(String.valueOf(lhpvSupList.get(i).get("unld")), cellCSFont));
				table3.addCell(cellColSpan1);
				
				cellColSpan1.setPhrase(new Phrase(String.valueOf(lhpvSupList.get(i).get("rcvr")), cellCSFont));
				table3.addCell(cellColSpan1);
				
				cellColSpan1.setPhrase(new Phrase(String.valueOf(lhpvSupList.get(i).get("pay")), cellCSFont));
				table3.addCell(cellColSpan1);
				
				cellColSpan1.setPhrase(new Phrase(String.valueOf(lhpvSupList.get(i).get("gdTot")), cellCSFont));
				table3.addCell(cellColSpan1);
				
			
		}
			
			footerCellColSpan18.setPhrase(new Phrase("Total Pay", footerfont));
			table3.addCell(footerCellColSpan18);
			
			footerCellColSpan1.setPhrase(new Phrase(String.valueOf(totSupPay), footerfont));
			table3.addCell(footerCellColSpan1);
			
			lhpvSupDetColSpan5.setPhrase(new Phrase("Cash Payment: "+String.valueOf(cashSupPay), footerfont));
			table3.addCell(lhpvSupDetColSpan5);
			
			lhpvSupDetColSpan5.setPhrase(new Phrase("Cheque Payment: "+String.valueOf(chqSupPay), footerfont));
			table3.addCell(lhpvSupDetColSpan5);
			
			lhpvSupDetColSpan5.setPhrase(new Phrase("RTGS Payment: "+String.valueOf(rtgsSupPay), footerfont));
			table3.addCell(lhpvSupDetColSpan5);
			
			lhpvSupDetColSpan4.setPhrase(new Phrase("PETRO Payment: "+String.valueOf(petroSupPay), footerfont));
			table3.addCell(lhpvSupDetColSpan4);
			
/*			cellColSpan6.setPhrase(new Phrase("Balance", cellCSFont));
			table.addCell(cellColSpan6);
			if (String.valueOf(faCodeNameMap.get("csDrCrBalance")).equalsIgnoreCase("D")) {
				cellCSHeader.setPhrase(new Phrase(String.valueOf(faCodeNameMap.get("csAmtBalance")), cellCSFont));
				table.addCell(cellCSHeader);
				
				cellCSHeader.setPhrase(new Phrase("", cellCSFont));
				table.addCell(cellCSHeader);
			}else if (String.valueOf(faCodeNameMap.get("csDrCrBalance")).equalsIgnoreCase("C")) {
				cellCSHeader.setPhrase(new Phrase("", cellCSFont));
				table.addCell(cellCSHeader);
				
				cellCSHeader.setPhrase(new Phrase(String.valueOf(faCodeNameMap.get("csAmtBalance")), cellCSFont));
				table.addCell(cellCSHeader);
			}
			
			cellColSpan6.setPhrase(new Phrase("Total", cellCSHeaderFont));
			table.addCell(cellColSpan6);
			
			cellCSHeader.setPhrase(new Phrase(String.valueOf(faCodeNameMap.get("csAmtTotal")), cellCSHeaderFont));
			table.addCell(cellCSHeader);
			
			cellCSHeader.setPhrase(new Phrase(String.valueOf(faCodeNameMap.get("csAmtTotal")), cellCSHeaderFont));
			table.addCell(cellCSHeader);
			
			cellColSpan8.setPhrase(new Phrase("********************************************", cellFont8));
			table.addCell(cellColSpan8);*/
			
		//}
		doc.add(table);
		doc.add(new Paragraph(""));
		doc.add(new Paragraph(""));
		doc.add(table2);
		doc.add(new Paragraph(""));
		doc.add(new Paragraph(""));
		doc.add(table3);
	}
	
}
