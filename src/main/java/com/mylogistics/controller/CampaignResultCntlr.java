package com.mylogistics.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.mylogistics.DAO.CampaignResultDAO;
import com.mylogistics.DAO.IndentDAO;
import com.mylogistics.DAO.IndentOwnerMasterDAO;
import com.mylogistics.DAO.OwnerMasterDAO;
import com.mylogistics.DAO.VehicleEngagementDAO;
import com.mylogistics.model.CampaignResults;
import com.mylogistics.model.Customer;
import com.mylogistics.model.Indent;
import com.mylogistics.model.OwnerMaster;
import com.mylogistics.model.VehicleEngagement;
import com.mylogistics.services.ConstantsValues;

@Controller
public class CampaignResultCntlr {
	
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private CampaignResultDAO campaignResultDAO;
	
	@Autowired
	private IndentOwnerMasterDAO indentOwnerMasterDAO;
	
	@Autowired
	private IndentDAO indentDAO;
	
	@Autowired
	private OwnerMasterDAO ownerMasterDAO;
	
	@Autowired
	private VehicleEngagementDAO vehicleEngagementDAO;
	
	private String campaignResultPath = "/var/www/html/Erp_Excel/CampaignResult";

	
	@RequestMapping(value = "/uploadCampaignResult", method = RequestMethod.POST)
	public @ResponseBody Object uploadCampaignResult(@RequestParam("file") MultipartFile excelFile){	
		System.out.println("file name = "+excelFile.getContentType());
		System.out.println("original file name = "+excelFile.getOriginalFilename());
		String fileType=excelFile.getOriginalFilename().split("\\.")[excelFile.getOriginalFilename().split("\\.").length-1];
		System.out.println("file type = "+fileType);
		
		Map<String, Object> resultMap=new HashMap<>();
		Session session=this.sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		
		try {

			Path path = Paths.get(campaignResultPath);
			if(! Files.exists(path))
				Files.createDirectories(path);//make new directory if not exist
			
			byte [] fileInBytes = excelFile.getBytes();
			
			File acntFile = new File(campaignResultPath+"/campaignResult"+"."+fileType);										
			if(acntFile.exists())
				acntFile.delete();
			
			FileOutputStream out = new FileOutputStream(acntFile);
			out.write(fileInBytes);
			out.close();
			
			
			ArrayList<CampaignResults> campaignList = new ArrayList<>();
			FileInputStream file = new FileInputStream(new File(campaignResultPath+"/campaignResult"+"."+fileType));

			// Create Workbook instance holding reference to .xlsx file
			XSSFWorkbook workbook = new XSSFWorkbook(file);

			// Get first/desired sheet from the workbook
			XSSFSheet sheet = workbook.getSheetAt(0);

			// Iterate through each rows one by one
			Iterator<Row> rowIterator = sheet.iterator();
			while (rowIterator.hasNext()) {
				CampaignResults campaignResults = new CampaignResults();
				Row row = rowIterator.next();
				System.out.println(row.getRowNum());
				if(row.getRowNum()>0) {
					// For each row, iterate through all the columns
					Iterator<Cell> cellIterator = row.cellIterator();

					while (cellIterator.hasNext()) {
						Cell cell = cellIterator.next();
						/*if (cell.getColumnIndex() == 2) {
							campaignResults.setMaster_Id(Integer.parseInt(cell.getStringCellValue()));
						}*/
						if (cell.getColumnIndex() == 3) {
							 cell.setCellType(Cell.CELL_TYPE_STRING);
							//campaignResults.setIndentId(cell.getStringCellValue());
							 campaignResults.setIndentId(Integer.parseInt(cell.getStringCellValue()));
						}
						if (cell.getColumnIndex() == 4) {
							cell.setCellType(Cell.CELL_TYPE_STRING);
							campaignResults.setMaster_Id(Integer.parseInt(cell.getStringCellValue()));
						}
						if (cell.getColumnIndex() == 5) {
							cell.setCellType(Cell.CELL_TYPE_STRING);
							campaignResults.setNoOfVehAvail(Integer.parseInt(cell.getStringCellValue()));
						}
						if (cell.getColumnIndex() == 6) {
							cell.setCellType(Cell.CELL_TYPE_STRING);
							campaignResults.setRates(Integer.parseInt(cell.getStringCellValue()));
						}
						if (cell.getColumnIndex() == 7) {
							campaignResults.setAgentRemark(cell.getStringCellValue());
						}
					}
					
					boolean flag=campaignResultDAO.checkCampaignResultByIndentIdMasterId(campaignResults.getIndentId(), campaignResults.getMaster_Id(), session);
					
					if(!flag)
						campaignResultDAO.saveCampaignResult(campaignResults, session);
				}
				
			}

			file.close();

			session.flush();
			session.clear();
			transaction.commit();
			resultMap.put("result", "success");
			
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();
			resultMap.put("result", "error");
		}finally {
			session.close();
		}
		return resultMap;
	}
	
/*
	@RequestMapping(value = "/saveCampaignResult", method = RequestMethod.POST)
	public @ResponseBody Object saveCampaignResult(@RequestBody Map<String, Object>clientMap){	
		
		
		Map<String, Object> resultMap=new HashMap<>();
		Session session=this.sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		
		try {

				CampaignResults campaignResults = new CampaignResults();
				
							 campaignResults.setIndentId(Integer.parseInt(clientMap.get("indent_code").toString()));
							campaignResults.setMaster_Id(Integer.parseInt(clientMap.get("master_Id").toString()));
							campaignResults.setNoOfVehAvail(Integer.parseInt(clientMap.get("no_of_Vehicles_Available").toString()));
							campaignResults.setRates(Integer.parseInt(clientMap.get("rates").toString()));
							campaignResults.setAgentRemark(clientMap.get("remarks").toString());
					
					boolean flag=campaignResultDAO.checkCampaignResultByIndentIdMasterId(campaignResults.getIndentId(), campaignResults.getMaster_Id(), session);
					
					if(!flag)
						campaignResultDAO.saveCampaignResult(campaignResults, session);
				

			session.flush();
			session.clear();
			transaction.commit();
			resultMap.put("result", "success");
			
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();
			resultMap.put("result", "error");
		}finally {
			session.close();
		}
		return resultMap;
	}
	*/
	
	
	@RequestMapping(value = "/saveCampaignResult", method = RequestMethod.GET)
	public @ResponseBody Object saveCampaignResult(@RequestParam int indent_code,@RequestParam int master_Id,@RequestParam int no_of_Vehicles_Available,@RequestParam int rates,@RequestParam String remarks){	
		
		
		Map<String, Object> resultMap=new HashMap<>();
		Session session=this.sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		
		try {

				CampaignResults campaignResults = new CampaignResults();
				
							 campaignResults.setIndentId(indent_code);
							campaignResults.setMaster_Id(master_Id);
							campaignResults.setNoOfVehAvail(no_of_Vehicles_Available);
							campaignResults.setRates(rates);
							campaignResults.setAgentRemark(remarks);
					
					boolean flag=campaignResultDAO.checkCampaignResultByIndentIdMasterId(campaignResults.getIndentId(), campaignResults.getMaster_Id(), session);
					
					if(!flag)
						campaignResultDAO.saveCampaignResult(campaignResults, session);
				

			session.flush();
			session.clear();
			transaction.commit();
			resultMap.put("result", "success");
			
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();
			resultMap.put("result", "error");
		}finally {
			session.close();
		}
		return resultMap;
	}
	
	
	
	@RequestMapping(value = "/getCampaignResultsByIndentId", method = RequestMethod.POST)
	public @ResponseBody Object getCampaignResultsByIndentId(@RequestBody Map<String, Object>clientMap) {
		System.out.println("enter into getCampaignResultsByIndentId function");
		Map<String,Object> map = new HashMap<>();
		
		int indentId=(int) clientMap.get("indentId");
		List<OwnerMaster>ownMstrList=new ArrayList<>();
		
		List<CampaignResults> campList=campaignResultDAO.getCampaignResultByIndentId(indentId);
		System.out.print(campList.size());
		List<Integer> ownerMasterIdList=new ArrayList<>();
		Map<Integer, Object> campMap=new HashMap<>();
		if(!campList.isEmpty()) {
			for(int i=0;i<campList.size();i++) {
				ownerMasterIdList.add(campList.get(i).getMaster_Id());
				campMap.put(campList.get(i).getMaster_Id(), campList.get(i));//.getNoOfVehAvail()
			}
			ownMstrList=indentOwnerMasterDAO.getIndentOwnerMasterByIdList(ownerMasterIdList);
		}
		
		//map.put("list", campList);
		map.put("campMap", campMap);
		map.put("list", ownMstrList);
		map.put("result", "success");
		return map;
	}
	
	
	

	@RequestMapping(value = "/saveVehicleEngagement", method = RequestMethod.POST)
	public @ResponseBody Object saveVehicleEngagement(@RequestBody List<VehicleEngagement> veList) {
		Map<String,Object> map = new HashMap<>();
		
		if (!veList.isEmpty()) {

			Indent indent = indentDAO.getIndentById(veList.get(0).getCampaignResults().getIndentId());

			Session session = this.sessionFactory.openSession();
			Transaction transaction = session.beginTransaction();
			try {
				int vEId = 0;

				for (VehicleEngagement engagement : veList) {
					boolean flag=vehicleEngagementDAO.checkVehicleEngagementByVehNoAndCampainResult(engagement.getCampaignResults(), engagement.getVehicleNo(), session);
					
					if(!flag)
						vEId = vehicleEngagementDAO.saveVehicleEngagement(engagement, session);

				}

				indent.setIndentStage("Vehicle Engaged");
				indentDAO.updateIndet(indent, session);
				session.flush();
				session.clear();
				transaction.commit();

				map.put("vEId", vEId);
				map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			} catch (Exception e) {
				e.printStackTrace();
				transaction.rollback();
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				map.put("msg", "Please retry");
			} finally {
				session.close();
			}
		}else {
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			map.put("msg","Please Add vehicle details");
		}
		
		return map;
	}
	
	
	
	@RequestMapping(value = "/getOwnerMasterAPI", method = RequestMethod.GET)
	public @ResponseBody Object getOwnerMasterAPI(@RequestParam int master_Id, @RequestParam long phone_No,
			@RequestParam String branch, @RequestParam String selected_States_of_North,
			@RequestParam String selected_States_of_South, @RequestParam String selected_States_of_Central,
			@RequestParam String selected_States_of_West, @RequestParam String selected_States_of_East,
			@RequestParam String selected_States_of_NorthEast, @RequestParam String other_Nations,
			@RequestParam String no_of_Chakka_per_Vehicle, @RequestParam String type_of_Truck,
			@RequestParam String type_of_Container, @RequestParam String type_of_Trailor_and_Platform,
			@RequestParam String type_of_Vendor, @RequestParam String number_of_Owned_Vehicles,
			@RequestParam String number_of_Vehicles_in_Group, @RequestParam String number_of_Persons_in_Group,
			@RequestParam String company_Name, @RequestParam String company_Owner_Name,
			@RequestParam String company_Owner_Mobile_1, @RequestParam String company_Owner_Mobile_2,
			@RequestParam String company_Office_Address) {
		System.out.println("enter into getOwnerMasterAPI function");

		Map<String, Object> map = new HashMap<>();
		OwnerMaster ownerMaster = new OwnerMaster();
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try {

			ownerMaster.setBranch(branch);
			ownerMaster.setCompany_Name(company_Name);
			ownerMaster.setCompany_Office_Address(company_Office_Address);
			ownerMaster.setCompany_Owner_Mobile_1(company_Owner_Mobile_1);
			ownerMaster.setCompany_Owner_Mobile_2(company_Owner_Mobile_2);
			ownerMaster.setCompany_Owner_Name(company_Owner_Name);
			ownerMaster.setMaster_Id(master_Id);
			ownerMaster.setNo_of_Chakka_per_Vehicle(no_of_Chakka_per_Vehicle);
			ownerMaster.setNumber_of_Owned_Vehicles(number_of_Owned_Vehicles);

			ownerMaster.setNumber_of_Persons_in_Group(number_of_Persons_in_Group);

			ownerMaster.setNumber_of_Vehicles_in_Group(number_of_Vehicles_in_Group);
			ownerMaster.setOther_Nations(other_Nations);
			ownerMaster.setPhone_No(phone_No);
			ownerMaster.setSelected_States_of_Central(selected_States_of_Central);
			ownerMaster.setSelected_States_of_East(selected_States_of_East);
			ownerMaster.setSelected_States_of_North(selected_States_of_NorthEast);
			ownerMaster.setSelected_States_of_NorthEast(selected_States_of_NorthEast);

			ownerMaster.setSelected_States_of_South(selected_States_of_South);
			ownerMaster.setSelected_States_of_West(selected_States_of_West);

			ownerMaster.setType_of_Container(type_of_Container);
			ownerMaster.setType_of_Trailor_and_Platform(type_of_Trailor_and_Platform);
			ownerMaster.setType_of_Truck(type_of_Truck);
			ownerMaster.setType_of_Vendor(type_of_Vendor);

			int ownMstrId = ownerMasterDAO.saveOwnerMaster(session, ownerMaster);
			session.flush();
			session.clear();
			transaction.commit();

			map.put("indentId", ownMstrId);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			map.put("msg", "Please retry");
		} finally {
			session.close();
		}
		return map;
	}
	
	

}
