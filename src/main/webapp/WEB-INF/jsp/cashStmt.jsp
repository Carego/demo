<style type="text/css">
            
            .printable{display:none;}
            
            @media print {
                .noprint, .menuwrapper, .ui-dialog, footer, #toast-container {display: none !important;}
                .printable {display: block;
                		width: 11in !important;
                		position: fixed;
                		top: -10px;
                		left:0px;
                		z-index: 999999;
                		border: 0px solid black;
                		}
				          		
                		body  {
						      width: 11in;
						    	height : 210in;						      
						      }
						      
            }
            @PAGE {
				   size: A4 portrait;
				   margin: 2cm 0.20cm 2cm 0.20cm;
			      }
			     table tr{page-break-after:always;}
			     td{  word-wrap: break-word;}
			   
}
</style>

<div ng-show="operatorLogin || superAdminLogin">
	<div class="noprint">
		<form name="cashStmtForm" ng-submit="submitCS(cashStmtForm)" class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
		     		<div class="col s3 input-field" ng-if="currentBranch=='Gurgaon (H.O)'">
		       				<input class="validate" type ="text" id="branchNameId" name ="branchName" ng-model="branch.branchName" ng-click="openBrhDB()" readonly ng-requiblack="true" >
		       			<label for="code">Branch Name</label>	
		       		</div>

		       		<div class="col s3 input-field">
		       				<input class="validate" type ="date" id="csDtId" name ="csDt" ng-model="csDate" ng-requiblack="true">
		       			<label for="code">Date</label>	
		       		</div>		       		
		       		
		       		<div class="col s3 input-field">
		       				<input class="validate" type ="submit" value="Submit">
		       		</div>
		    </div> 
		  </form>
    </div>
    

  <div id ="brhId" ng-hide="brhFlag">
		 <input type="text" name="filterBrhbox" ng-model="filterBrhbox" placeholder="Search...">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	 <!--  <th> Branch Code </th> -->
 	  	  	  
 	  	  	  <th> Branch Name </th>
 	  	  	  
 	  	  	  <th> Branch FaCode</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="branch in brhList | filter:filterBrhbox">
		 	  <td><input type="radio"  name="branchName"  class="branchCls"  value="{{ branch }}" ng-model="brCode" ng-click="saveBranch(branch)"></td>
              <!-- <td>{{branch.branchCode}}</td> -->
              <td>{{branch.branchName}}</td>
              <td>{{branch.branchFaCode}}</td>
          </tr>
      </table>
	</div>

	<div id="printVsDB" ng-hide="printVsFlag" class="noprint">
		<div class="row">
			<div class="col s12 center"><h5 class="white-text">{{PRINT_STMT}}</h5></div>
		</div>
		<div>
			<div class="col s12 center">
				<a class="btn white-text" ng-click="cancelPrint()">Cancel</a>
				<a class="btn white-text" ng-click="printVs()">Yes</a>
			</div>
		</div>
	</div>
	
	<div class="printable"> 
		<div class="col s12 card" style="align: center; margin-top: 0px; padding-top:0px;  background-color: rgba(125, 125, 125, 0.3);" >
			<div class="container" style="width: 100% !important;">
				<div class="row" style="  border-top:0px solid black; border-left:0px solid black;border-right:0px solid black;">
					<div class="col s8">
						<div class="row">
							<div class="col s12" style="font-weight:bold; margin-top:5px;"><span style=" font-size:22px;">{{cmpnyName}}</span><br/>
												<!-- Voucher Type: {{vs.voucherType}} -->
												Cash Statement<br/>
												CS Print Date: {{date | date:'dd/MM/yyyy'}}  Time:{{date | date:'HH:mm:ss'}}<br/>
												<!-- <br/> -->
												<!-- Pay to: {{vs.payTo}} --></div>
						</div>
					</div>
					<div class="col s4">
						<div class="row">
							<div class="col s12" style="font-weight:bold; margin-top:5px;">Branch Code:{{brhName}}<br/>
												Sheet No: {{sheetNo}}<br/>
												Date: {{csDate | date:'dd/MM/yyyy'}}<!-- <br/> -->
												<!-- Voucher No: {{vs.vhNo}}<br/> -->
												<!-- Cheque No: {{vs.chequeLeaves.chqLChqNo}} --></div>
						</div>						
					</div>
				</div>
				<table style="width: 100% !important;  border-top: 3px solid black; border-bottom: 3px solid black; border-left: 1px solid black; border-right: 1px solid black;">
					<tr style="border-top: 3px solid black; border-bottom: 1px solid black;">
						<td style="width: 1in;border-bottom: 1px solid black;"></td>
						<td style="width: 1in;border-bottom: 1px solid black; font-weight:bold; font-size:16px;" colspan="5">VOUCHER TYPE : </td>
						<td style="width: 1in;border-bottom: 1px solid black; font-weight:bold; font-size:16px;" colspan="2">CASH</td>
					</tr>
					<tr style="font-weight:bold;">
						<td style="width: 40px;border-bottom: 1px solid black;">CODE</td>
						<td style="width: 40px;border-bottom: 1px solid black;">ACHEAD</td>
						<td style="width: 80px;border-bottom: 1px solid black;">VCH TYPE</td>
						<td style="width: 292px;border-bottom: 1px solid black; text-align:center;">DESCRIPTION</td>
						<td style="width: 100px;border-bottom: 1px solid black;">TvNo</td>
						<td style="width: 20px;border-bottom: 1px solid black; text-align:right;">VNo</td>
						<td style="width: 115px;border-bottom: 1px solid black; text-align:right;">RECEIPT AMT</td>
						<td style="width: 115px;border-bottom: 1px solid black; text-align:right;">PAYMENT AMT</td>
					</tr>
					<tr>
						<td style="width: 1in;border-bottom: 1px solid black;"></td>
						<td style="width: 1in;border-bottom: 1px solid black; font-weight:bold;" colspan="5">CASH OPENING BALANCE</td>
						<td style="width: 1in;border-bottom: 1px solid black; font-weight:bold; text-align:right;">{{ openBal | number:2}}</td>
						<td style="width: 1in;border-bottom: 1px solid black;"></td>
					</tr>
					<tr ng-repeat="cashCs in cashCsList">
						<!-- <div ng-if="cs.csVochType == 'cash' || cs.csVochType == 'CASH'"> -->
							<td style="width: 40px;border-bottom: 1px solid black;">{{cashCs.csFaCode}}</td>
							<td style="width: 40px;border-bottom: 1px solid black;">
								<div ng-repeat="acHdCash in acHdList">
									<div ng-if="acHdCash.faCode == cashCs.csFaCode">
										{{ acHdCash.faName }}
									</div>
								</div>
							</td>		
							<td style="width: 80px;border-bottom: 1px solid black;">{{cashCs.csType}}</td>
							<td style="width: 292px;border-bottom: 1px solid black; text-align:center;">{{cashCs.csDescription}}</td>
							<td style="width: 100px;border-bottom: 1px solid black;">{{cashCs.csTvNo}}</td>
							<td style="width: 20px;border-bottom: 1px solid black; text-align:right;">{{cashCs.csVouchNo}}</td>
							<td style="width: 115px;border-bottom: 1px solid black; text-align:right;">
								<div ng-if="cashCs.csDrCr == 'C'">
									{{cashCs.csAmt | number:2}}
									<!-- 999999999.99 -->
								</div>
							</td>
							<td style="width: 115px;border-bottom: 1px solid black; text-align:right;">
								<div ng-if="cashCs.csDrCr == 'D'">
									{{cashCs.csAmt | number:2}}
									<!-- 999999999.99 -->
								</div>
							</td>
						<!-- </div> -->
					</tr>
					<tr style="font-weight:bold;">
						<td style="width: 1in;border-bottom: 1px solid black;"></td>
						<td style="width: 1in;border-bottom: 1px solid black;" colspan="5">CASH CLOSING BALANCE</td>
						<td style="width: 1in;border-bottom: 1px solid black;"></td>
						<td style="width: 1in;border-bottom: 1px solid black; text-align:right;">{{ closeBal | number:2}}</td>
					</tr>
					<tr style="font-weight:bold;">
						<td style="width: 1in;"></td>
						<td style="width: 1in;" colspan="5">VOUCHER TOTAL</td>
						<td style="width: 1in; text-align:right;">{{ recAmt | number:2}}</td>
						<td style="width: 1in; text-align:right;">{{ payAmt | number:2}}</td>
					</tr>
				</table>
		<!-- 		<div class="row">
					<div class="col s12">Net Cash Withdrawal Amount {{}}</div>
				</div>
				<div class="row">
					<div class="col s12">Total Amount </div>
				</div>
				<div class="row">
					<div class="col s12">Note: Cashier has confirmed that he has obtained signature of passing authority & Payee on this payment by putting his signature on the same. </div>
				</div>
				<div class="row">
					<div class="col s3">Passed by<br/></>Code No</div><div class="col s3">Cash by<br/></>Code No</div><div class="col s3">Voucheblack by<br/></>Code No</div><div class="col s3">Payees Signature</div>
				</div> -->
											
				<table style="width: 100% !important; border-top: 3px solid black; border-bottom: 3px solid black; border-left: 1px solid black; border-right: 1px solid black; margin-top: 25px;" ng-repeat="bkCsMap in bkCsMapList">
					<tr>
						<td style="width: 1in;border-bottom: 1px solid black;"></td>
						<td style="width: 1in; font-weight:bold; font-size:16px;border-bottom: 1px solid black;" colspan="2">VOUCHER TYPE : </td>
						<td style="width: 1in; font-weight:bold; font-size:16px;border-bottom: 1px solid black;" colspan="1">BANK</td>
						<td style="width: 1in; font-weight:bold; font-size:16px;border-bottom: 1px solid black;" colspan="3">
							<div ng-repeat="acHead in acHdList">
									<div ng-if="acHead.faCode == bkCsMap.bank.bcsBankCode">
										{{ acHead.faName }}
									</div>
							</div>
						</td>
						<td style="width: 1in; font-weight:bold; font-size:18px;border-bottom: 1px solid black;" colspan="1">{{bkCsMap.bank.bcsBankCode}}</td>
					</tr>
					<tr style="font-weight:bold;">
						<td style="width: 40px;border-bottom: 1px solid black;">CODE</td>
						<td style="width: 40px;border-bottom: 1px solid black;">ACHEAD</td>
						<td style="width: 80px;border-bottom: 1px solid black;">VCH TYPE</td>
						<td style="width: 292px;border-bottom: 1px solid black; text-align:center;">DESCRIPTION</td>
						<td style="width: 100px;border-bottom: 1px solid black;">TvNo</td>
						<td style="width: 20px;border-bottom: 1px solid black; text-align:right;">VNo</td>
						<td style="width: 115px;border-bottom: 1px solid black; text-align:right;">RECEIPT AMT</td>
						<td style="width: 115px;border-bottom: 1px solid black; text-align:right;">PAYMENT AMT</td>
					</tr>
					<tr>
						<td style="width: 1in;border-bottom: 1px solid black;"></td>
						<td style="width: 1in;border-bottom: 1px solid black; font-weight:bold;" colspan="5">BANK OPENING BALANCE</td>
						<td style="width: 1in;border-bottom: 1px solid black; font-weight:bold; text-align:right;">{{ bkCsMap.bank.bcsOpenBal | number:2 }}</td>
						<td style="width: 1in;border-bottom: 1px solid black;"></td>
					</tr>
					<tr ng-repeat="bankCs in bkCsMap.bkCsList">
						<!-- <div ng-if="cs.csVochType == 'cash' || cs.csVochType == 'CASH'"> -->
							<td style="width: 40px;border-bottom: 1px solid black;">{{bankCs.csFaCode}}</td>
							<td style="width: 40px;border-bottom: 1px solid black;">
								<div ng-repeat="acHdBnk in acHdList">
									<div ng-if="acHdBnk.faCode == bankCs.csFaCode">
										{{ acHdBnk.faName }}
									</div>
								</div>
							</td>		
							<td style="width: 80px;border-bottom: 1px solid black;">{{bankCs.csType}}</td>
							<td style="width: 292px;border-bottom: 1px solid black; text-align:center;">{{bankCs.csDescription}}</td>
							<td style="width: 100px;border-bottom: 1px solid black;">{{bankCs.csTvNo}}</td>
							<td style="width: 20px;border-bottom: 1px solid black; text-align:right;">{{bankCs.csVouchNo}}</td>
							<td style="width: 115;border-bottom: 1px solid black; text-align:right;">
								<div ng-if="bankCs.csDrCr == 'C'">
									{{bankCs.csAmt | number:2}}
								</div>
							</td>
							<td style="width: 115px;border-bottom: 1px solid black; text-align:right;">
								<div ng-if="bankCs.csDrCr == 'D'">
									{{bankCs.csAmt | number:2}}
								</div>
								<div ng-if="bankCs.csDescription == 'Single Cheque Cancel' && bankCs.csType == 'Cheque Cancel' ">
									{{bankCs.csAmt | number:2}}
								</div>
							</td>
						<!-- </div> -->
					</tr>
					<tr style="font-weight:bold;">
						<td style="width: 1in;border-bottom: 1px solid black;"></td>
						<td style="width: 1in;border-bottom: 1px solid black;" colspan="5">BANK CLOSING BALANCE</td>
						<td style="width: 1in;border-bottom: 1px solid black;"></td>
						<td style="width: 1in;border-bottom: 1px solid black; text-align:right;">{{ bkCsMap.bank.bcsCloseBal | number:2}}</td>
					</tr>
					<tr style="font-weight:bold;">
						<td style="width: 1in;"></td>
						<td style="width: 1in;" colspan="5">VOUCHER TOTAL</td>
						<td style="width: 1in; text-align:right;">{{ bkCsMap.recBnkAmt | number:2}}</td>
						<td style="width: 1in; text-align:right;">{{ bkCsMap.payBnkAmt | number:2}}</td>
					</tr>
					
				</table>
				
				<table style="width: 100% !important; margin-top: 25px; border-top: 3px solid black; border-bottom: 3px solid black; border-left: 1px solid black; border-right: 1px solid black;">
					<tr>
						<td style="width: 1in;border-bottom: 1px solid black;"></td>
						<td style="width: 1in; font-weight:bold; font-size:16px;border-bottom: 1px solid black;" colspan="5">VOUCHER TYPE : </td>
						<td style="width: 1in; font-weight:bold;  font-size:16px;border-bottom: 1px solid black;" colspan="2">CONTRA</td>
					</tr>
					<tr style="font-weight:bold;">
						<td style="width: 40px;border-bottom: 1px solid black;">CODE</td>
						<td style="width: 40px;border-bottom: 1px solid black;">ACHEAD</td>
						<td style="width: 80px;border-bottom: 1px solid black;">VCH TYPE</td>
						<td style="width: 292px;border-bottom: 1px solid black; text-align:center;">DESCRIPTION</td>
						<td style="width: 100px;border-bottom: 1px solid black;">TvNo</td>
						<td style="width: 20px;border-bottom: 1px solid black; text-align:right;">VNo</td>
						<td style="width: 115px;border-bottom: 1px solid black; text-align:right;">RECEIPT AMT</td>
						<td style="width: 115px;border-bottom: 1px solid black; text-align:right;">PAYMENT AMT</td>
					</tr>
					
					<tr ng-repeat="contraCs in contraCsList">
						<!-- <div ng-if="cs.csVochType == 'cash' || cs.csVochType == 'CASH'"> -->
							<td style="width: 40px;border-bottom: 1px solid black;">{{contraCs.csFaCode}}</td>
							<td style="width: 40px;border-bottom: 1px solid black;">
								<div ng-repeat="acHdCont in acHdList">
									<div ng-if="acHdCont.faCode == contraCs.csFaCode">
										{{ acHdCont.faName }}
									</div>
								</div>
							</td>		
							<td style="width: 80px;border-bottom: 1px solid black;">{{contraCs.csType}}</td>
							<td style="width: 292px;border-bottom: 1px solid black; text-align:center;">{{contraCs.csDescription}}</td>
							<td style="width: 100px;border-bottom: 1px solid black;">{{contraCs.csTvNo}}</td>
							<td style="width: 20px;border-bottom: 1px solid black; text-align:right;">{{contraCs.csVouchNo}}</td>
							<td style="width: 115px;border-bottom: 1px solid black; text-align:right;">
								<div ng-if="contraCs.csDrCr == 'C'">
									{{contraCs.csAmt | number:2}}
								</div>
							</td>
							<td style="width: 115px;border-bottom: 1px solid black; text-align:right;">
								<div ng-if="contraCs.csDrCr == 'D'">
									{{contraCs.csAmt | number:2}}
								</div>
							</td>
						<!-- </div> -->
					</tr>
					<tr>
						<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
					</tr>
					
				</table>
				
			</div>
			
			<div class="container" style="width: 100% !important;">
				<div class="row" style="  border-top:0px solid black; font-weight:bold; border-left:0px solid black;border-right:0px solid black;">
					<div class="col s6">
					<div style="margin-top:10px;">CS Verified by:</div>
					<div style="margin-top:15px;">Branch Incharge:</div>
					<div style="margin-top:10px;">Staff Code:</div>
					
					</div>
				
				    <div class="col s6" align="right" style="margin-top:-75px; text-align:left;  margin-left:70%;">
					<div style="margin-top:0px;">Cashier:</div>
					<div style="margin-top:10px;">Staff Code:</div>
					</div>
				
				</div>
			</div>
			
		</div>
	</div>
	
	
	
	
	
	
	
	
	
	
	<div id="newCsPrint" style="display: none;">	
		<div style="align: center;">
			<div style="width: 100%;">
				<div style=" border-top:0px solid black; border-left:0px solid black;border-right:0px solid black;">
					<div style="width:70%; float:left; font-weight:bold; margin-top:5px;font-size:12px;font-family: 'Arial';">
						<span style=" font-size:15px;">{{cmpnyName}}</span><br/>
						Cash Statement<br/>
						CS Print Date: {{date | date:'dd/MM/yyyy'}}  Time:{{date | date:'HH:mm:ss'}}<br/>
					</div>			
					<div style="width:29%; float:right; font-weight:bold; margin-top:5px;font-size:12px;font-family: 'Arial';">
						Branch Code:{{brhName}}<br/>
						Sheet No: {{sheetNo}}<br clear="all"/>
						Date: {{csDate | date:'dd/MM/yyyy'}}											
					</div>
				</div>
			</div>
			<br clear="all"/></br>			
			<div style="width: 100%;">				
					<table style="border-collapse: separate;border-spacing: 0px; width:100%; font-family: 'Arial'; width: 100%; border-top: 2px solid black; border-bottom: 2px solid black; border-left: 1px solid black; border-right: 1px solid black;">
						<tr>							
							<td style="font-weight:bold; font-size:12px; height:30px; border-bottom: 1px solid black; padding-left: 10%;" colspan="6">VOUCHER TYPE : </td>
							<td style="font-weight:bold; font-size:12px; height:30px; border-bottom: 1px solid black;" colspan="2">CASH</td>
						</tr>
						<tr style="font-weight:bold;font-size:10px;">
							<td style="width: 40px;border-bottom: 1px solid black;padding: 4px;">CODE</td>
							<td style="width: 40px;border-bottom: 1px solid black;padding: 4px;">ACHEAD</td>
							<td style="width: 80px;border-bottom: 1px solid black;padding: 4px;">VCH TYPE</td>
							<td style="width: 292px;border-bottom: 1px solid black;padding: 4px; text-align:center;">DESCRIPTION</td>
							<td style="width: 100px;border-bottom: 1px solid black;padding: 4px;">TvNo</td>
							<td style="width: 20px;border-bottom: 1px solid black;padding: 4px; text-align:right;">VNo</td>
							<td style="width: 115px;border-bottom: 1px solid black;padding: 4px; text-align:right;">RECEIPT AMT</td>
							<td style="width: 115px;border-bottom: 1px solid black;padding: 4px; text-align:right;">PAYMENT AMT</td>
						</tr>
						<tr style="font-weight:bold;font-size:10px;">
							<td style="border-bottom: 1px solid black; padding-left: 10%; padding-top: 4px; padding-bottom: 4px;" colspan="6">CASH OPENING BALANCE</td>
							<td style="border-bottom: 1px solid black; text-align:right; padding: 4px;">{{ openBal | number:2}}</td>
							<td style="border-bottom: 1px solid black; text-align:right; padding: 4px;"></td>
						</tr>
						<tr style="font-size:10px;" ng-repeat="cashCs in cashCsList">
							<td style="border-bottom: 1px solid black;padding:4px;">{{cashCs.csFaCode}}</td>
							<td style="border-bottom: 1px solid black;padding:4px;">
								<div ng-repeat="acHdCash in acHdList">
									<div ng-if="acHdCash.faCode == cashCs.csFaCode">
										{{ acHdCash.faName }}
									</div>
								</div>
							</td>		
							<td style="border-bottom: 1px solid black;padding:4px;">{{cashCs.csType}}</td>
							<td style="border-bottom: 1px solid black;padding:4px; text-align:center;">{{cashCs.csDescription}}</td>
							<td style="border-bottom: 1px solid black;padding:4px;">{{cashCs.csTvNo}}</td>
							<td style="border-bottom: 1px solid black;padding:4px; text-align:right;">{{cashCs.csVouchNo}}</td>
							<td style="border-bottom: 1px solid black;padding:4px; text-align:right;">
								<div ng-if="cashCs.csDrCr == 'C'">
									{{cashCs.csAmt | number:2}}
								</div>
							</td>
							<td style="border-bottom: 1px solid black;padding:4px; text-align:right;">
								<div ng-if="cashCs.csDrCr == 'D'">
									{{cashCs.csAmt | number:2}}
								</div>
							</td>
						</tr>
						<tr style="font-weight:bold;font-size:10px;">
							<td style="border-bottom: 1px solid black;padding-left: 10%; padding-top: 4px; padding-bottom: 4px;" colspan="7">CASH CLOSING BALANCE</td>							
							<td style="border-bottom: 1px solid black; padding:4px; text-align:right;">{{ closeBal | number:2}}</td>
						</tr>
						<tr style="font-weight:bold;font-size:10px;">
							<td style="padding-left: 10%;padding-top: 4px; padding-bottom: 4px;" colspan="6">VOUCHER TOTAL</td>
							<td style="text-align:right; padding: 4px;">{{ recAmt | number:2}}</td>
							<td style="text-align:right; padding: 4px;">{{ payAmt | number:2}}</td>
						</tr>				
					</table>
					
					<table ng-repeat="bkCsMap in bkCsMapList" style="margin-top : 20px; border-collapse: separate;border-spacing: 0px; width:100%; font-family: 'Arial'; width: 100%; border-top: 2px solid black; border-bottom: 2px solid black; border-left: 1px solid black; border-right: 1px solid black;">
						<tr style="font-weight:bold;font-size:12px;">
							<td style="border-bottom: 1px solid black;padding-left: 10%;height: 30px;" colspan="3">VOUCHER TYPE : </td>
							<td style="border-bottom: 1px solid black;height: 30px;" colspan="1">BANK</td>
							<td style="border-bottom: 1px solid black;height: 30px;" colspan="3">
								<div ng-repeat="acHead in acHdList">
										<div ng-if="acHead.faCode == bkCsMap.bank.bcsBankCode">
											{{ acHead.faName }}
										</div>
								</div>
							</td>
							<td style="border-bottom: 1px solid black;height: 30px;" colspan="1">{{bkCsMap.bank.bcsBankCode}}</td>
						</tr>
						<tr style="font-weight:bold;font-size:10px;">
						
							<td style="width: 40px;border-bottom: 1px solid black;padding: 4px;">CODE</td>
							<td style="width: 40px;border-bottom: 1px solid black;padding: 4px;">ACHEAD</td>
							<td style="width: 80px;border-bottom: 1px solid black;padding: 4px;">VCH TYPE</td>
							<td style="width: 292px;border-bottom: 1px solid black;padding: 4px; text-align:center;">DESCRIPTION</td>
							<td style="width: 100px;border-bottom: 1px solid black;padding: 4px;">TvNo</td>
							<td style="width: 20px;border-bottom: 1px solid black;padding: 4px; text-align:right;">VNo</td>
							<td style="width: 115px;border-bottom: 1px solid black;padding: 4px; text-align:right;">RECEIPT AMT</td>
							<td style="width: 115px;border-bottom: 1px solid black;padding: 4px; text-align:right;">PAYMENT AMT</td>
						</tr>
						<tr style="font-weight:bold;font-size:10px;">
							<td style="border-bottom: 1px solid black; padding-left: 10%;padding-top: 4px; padding-bottom: 4px;" colspan="6">BANK OPENING BALANCE</td>
							<td style="border-bottom: 1px solid black;padding: 4px; text-align:right;">{{ bkCsMap.bank.bcsOpenBal | number:2 }}</td>
							<td style="border-bottom: 1px solid black;padding: 4px;"></td>
						</tr>
						<tr style="font-size:10px;" ng-repeat="bankCs in bkCsMap.bkCsList">
							<td style="border-bottom: 1px solid black;padding:4px;">{{bankCs.csFaCode}}</td>
							<td style="border-bottom: 1px solid black;padding:4px;">
								<div ng-repeat="acHdBnk in acHdList">
									<div ng-if="acHdBnk.faCode == bankCs.csFaCode">
										{{ acHdBnk.faName }}
									</div>
								</div>
							</td>		
							<td style="border-bottom: 1px solid black;padding:4px;">{{bankCs.csType}}</td>
							<td style="border-bottom: 1px solid black;padding:4px; text-align:center;">{{bankCs.csDescription}}</td>
							<td style="border-bottom: 1px solid black;padding:4px;">{{bankCs.csTvNo}}</td>
							<td style="border-bottom: 1px solid black;padding:4px; text-align:right;">{{bankCs.csVouchNo}}</td>
							<td style="border-bottom: 1px solid black;padding:4px; text-align:right;">
								<div ng-if="bankCs.csDrCr == 'C'">
									{{bankCs.csAmt | number:2}}
								</div>
							</td>
							<td style="border-bottom: 1px solid black;padding:4px; text-align:right;">
								<div ng-if="bankCs.csDrCr == 'D'">
									{{bankCs.csAmt | number:2}}
								</div>
								<div ng-if="bankCs.csDescription == 'Single Cheque Cancel' && bankCs.csType == 'Cheque Cancel' ">
									{{bankCs.csAmt | number:2}}
								</div>
							</td>
						</tr>
						<tr style="font-weight:bold;font-size:10px;">
							<td style="border-bottom: 1px solid black; padding-left: 10%;padding-top: 4px;padding-botton: 4px;" colspan="6">BANK CLOSING BALANCE</td>
							<td style="border-bottom: 1px solid black;padding: 4px;"></td>
							<td style="border-bottom: 1px solid black;padding: 4px; text-align:right;">{{ bkCsMap.bank.bcsCloseBal | number:2}}</td>
						</tr>
						<tr style="font-weight:bold;font-size:10px;">
							<td style="padding-left: 10%;padding-top: 4px;padding-botton: 4px;" colspan="6">VOUCHER TOTAL</td>
							<td style="text-align:right;padding: 4px;">{{ bkCsMap.recBnkAmt | number:2}}</td>
							<td style="text-align:right;padding: 4px;">{{ bkCsMap.payBnkAmt | number:2}}</td>
						</tr>
					</table>
					
					<table style="margin-top : 20px; border-collapse: separate;border-spacing: 0px; width:100%; font-family: 'Arial'; width: 100%; border-top: 2px solid black; border-bottom: 2px solid black; border-left: 1px solid black; border-right: 1px solid black;">
						<tr style="font-weight:bold; font-size: 12px;">						
							<td style="border-bottom: 1px solid black;padding-left: 10%; height: 30px;" colspan="6">VOUCHER TYPE : </td>
							<td style="border-bottom: 1px solid black;height: 30px; " colspan="2">CONTRA</td>
						</tr>
						<tr style="font-weight:bold; font-size: 10px;">
							<td style="width: 40px;border-bottom: 1px solid black;padding: 4px;">CODE</td>
							<td style="width: 40px;border-bottom: 1px solid black;padding: 4px;">ACHEAD</td>
							<td style="width: 80px;border-bottom: 1px solid black;padding: 4px;">VCH TYPE</td>
							<td style="width: 292px;border-bottom: 1px solid black;padding: 4px; text-align:center;">DESCRIPTION</td>
							<td style="width: 100px;border-bottom: 1px solid black;padding: 4px;">TvNo</td>
							<td style="width: 20px;border-bottom: 1px solid black;padding: 4px; text-align:right;">VNo</td>
							<td style="width: 115px;border-bottom: 1px solid black;padding: 4px; text-align:right;">RECEIPT AMT</td>
							<td style="width: 115px;border-bottom: 1px solid black;padding: 4px; text-align:right;">PAYMENT AMT</td>
						</tr>
						<tr style="font-size: 10px;" ng-repeat="contraCs in contraCsList">
							<td style="width: 40px;border-bottom: 1px solid black;padding: 4px;">{{contraCs.csFaCode}}</td>
							<td style="width: 40px;border-bottom: 1px solid black;padding: 4px;">
								<div ng-repeat="acHdCont in acHdList">
									<div ng-if="acHdCont.faCode == contraCs.csFaCode">
										{{ acHdCont.faName }}
									</div>
								</div>
							</td>		
							<td style="width: 80px;border-bottom: 1px solid black;padding: 4px;">{{contraCs.csType}}</td>
							<td style="width: 292px;border-bottom: 1px solid black;padding: 4px; text-align:center;">{{contraCs.csDescription}}</td>
							<td style="width: 100px;border-bottom: 1px solid black;padding: 4px;">{{contraCs.csTvNo}}</td>
							<td style="width: 20px;border-bottom: 1px solid black;padding: 4px; text-align:right;">{{contraCs.csVouchNo}}</td>
							<td style="width: 115px;border-bottom: 1px solid black;padding: 4px; text-align:right;">
								<div ng-if="contraCs.csDrCr == 'C'">
									{{contraCs.csAmt | number:2}}
								</div>
							</td>
							<td style="width: 115px;border-bottom: 1px solid black;padding: 4px; text-align:right;">
								<div ng-if="contraCs.csDrCr == 'D'">
									{{contraCs.csAmt | number:2}}
								</div>
							</td>						
						</tr>
					</table>					
				</div>
			
			<div style="width: 100% !important; font-family: 'Arial'; font-size:12px;">
				<div style="border-top:0px solid black; font-weight:bold; border-left:0px solid black;border-right:0px solid black;">
					<div>
						<div style="margin-top:10px;">CS Verified by:</div>
						<div style="margin-top:15px;">Branch Incharge:</div>
						<div style="margin-top:10px;">Staff Code:</div>					
					</div>				
				    <div align="right" style="margin-top:-75px; text-align:left;  margin-left:70%;">
						<div style="margin-top:0px;">Cashier:</div>
						<div style="margin-top:10px;">Staff Code:</div>
					</div>				
				</div>
			</div>
			
		</div>
	</div>
				
</div>
