

'use strict';

var app = angular.module('application');

app.controller('CampaignResultCntlr',['$scope','$location','$http','FileUploadService',
                                 function($scope,$location,$http,FileUploadService){
	

 $scope.uploadCampaignResult = function(campaignResult){
			console.log("enter into uploadOwnDecImage function");
			var file = campaignResult;
			console.log("file name="+file.name);
			if(angular.isUndefined(file) || file === null || file === ""){
				$scope.alertToast("First choose the file----->");
			}else{
				 var index = file.name.lastIndexOf(".");
                 var strsubstring = file.name.substring(index, file.name.length);
                 if (strsubstring == '.xlsx'   )
                 {
				
				  console.log('file is ' + JSON.stringify(file) );
					var uploadUrl = $scope.projectName+"/uploadCampaignResult";
					FileUploadService.uploadFileToUrl(file, uploadUrl);
//					$scope.ownChqUpldFlag=false;
					
					console.log("file save on server");
//					$('div#ownChqUpldDBId').dialog('close');
                 }else{
                	 
                	 $scope.alertToast("File extension should be .xlsx ");
                 }
			}
			
		}
 
 
 
 
 $scope.ownerListDBFlag=false;
 $scope.singleIndentShowDB=false;
 $scope.allIndentShowDB=true;
 $scope.addDrvrDetFlag=true;
 $scope.vehList=[];
 
 $scope.getIndentList=function(){
	 var response = $http.post($scope.projectName+'/getIndentFrCampaignResult');
	 response.success(function(data, status, headers, config){
		  if(data.result === "success"){
			  $scope.indentList=data.list;
			  $scope.alertToast("success");
		  }else{
			  $scope.alertToast("Error");
			  console.log(data);
		  }
	   });
	   response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
	   });
	 
	 
 }()
 
 $scope.getCampaignByIndent=function(indent,index){
	 $scope.singleIndentShowDB=true;
	 $scope.allIndentShowDB=false;
	 $scope.tempIndex=index;
	 var indent={
			 "indentId":indent.indentId
	 }
	 var response = $http.post($scope.projectName+'/getCampaignResultsByIndentId',indent);
	 response.success(function(data, status, headers, config){
		  if(data.result === "success"){
			  $scope.ownerMstrList=data.list;
			  $scope.campMap=data.campMap;
			  $scope.alertToast("success");
			  $scope.ownerListDBFlag=true;
			  console.log($scope.campaignList);
			  console.log($scope.vehAvail);
		  }else{
			  $scope.alertToast("Error");
			  console.log(data);
		  }
	   });
	   response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
	   });
	 
	 
 }
 
 
 $scope.addVehAndDrvrDet=function(camp,veh){
	 console.log(camp);
	 console.log(veh);
	 $scope.vehList=[];
	 $scope.noOfVehAvailTemp=veh.noOfVehAvail;
	 $scope.campaign=veh;
	 console.log("campaignId="+veh.campainId);
			console.log("Enter Driver and Vehicle detail");
			$scope.addDrvrDetFlag=false;
			$scope.invoiceNo = "";
			$('div#addDrvrDetId').dialog({
				autoOpen: false,
				modal:true,
				title: "Enter Driver and Vehicle detail",
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				resizable: false,
				draggable: true,
				close: function(event, ui) { 
					$(this).dialog('destroy') ;
					$(this).hide();
				}
			});
			$('div#addDrvrDetId').dialog('open');	

	 
 }
 
 
 $scope.make = function(noOfVehicle){
	 $scope.vehList=[];
	 if(noOfVehicle>$scope.noOfVehAvailTemp){
		  $scope.alertToast("Vehicle No. can't be greater than available vehicle");
	  }else{

			 var len = noOfVehicle;
			 console.log(len);
			 
			 for(var i=0; i<len; i++)
				 $scope.vehList.push(i);  
	  }		 
  }
 
 
 $scope.vehDetList=[];
 $scope.vehicleNo="";
 $scope.addDrvrDet=function(vehicleNo,driverName,driverNo){
	
	 console.log("hi");
	 var vehDet={};
	 vehDet={
			 "vehicleNo":vehicleNo,
			 "driverName":driverName,
			 "driverNo":driverNo,
			 "campaignResults":$scope.campaign
	 }
	 var duplicate=false;
	 console.log("$scope.vehDetList if="+vehicleNo);
	 
	 if($scope.vehDetList.length<=0){
		 $scope.vehDetList.push(vehDet);
		 console.log("$scope.vehDetList if="+$scope.vehDetList);
	 }
		 
	 else{
		 for(var i=0;i<$scope.vehDetList.length;i++){
			 if($scope.vehDetList[i].vehicleNo===vehicleNo){
				 $scope.alertToast("Vehicle No. already exist");
				 duplicate = true;
				 break;
			 }
		 }
		 
		 if(duplicate==false)
			 $scope.vehDetList.push(vehDet);
		 console.log("$scope.vehDetList else="+$scope.vehDetList);
	 }
 }
 
 $scope.savePlacement=function(){
	 
	 var response = $http.post($scope.projectName+'/saveVehicleEngagement',$scope.vehDetList);
	 response.success(function(data, status, headers, config){
		  if(data.result === "success"){
			  $scope.alertToast("Placement saved successfully");
			  console.log($scope.vehAvail);
		  }else{
			  $scope.alertToast(data.msg);
			  console.log(data);
		  }
	   });
	   response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
	   });
 

}
 
}]);