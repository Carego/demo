<div ng-show="operatorLogin || superAdminLogin">
<title>View Customer</title>

<div class="row">
	<div class="col s3 hide-on-med-and-down">&nbsp;</div>
	<form class="col s6 card"
		style="align: center; margin-top: 40px; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);"
		ng-submit="submitCustomer(custCode)">

		<div class="input-field col s6">
			<input type="text" id="customerCode" name="custCodeTemp"
				ng-model="custCodeTemp" required readonly
				ng-click="OpenCustomerCodeDB()" /> 
				<input class="validate" type ="hidden" name ="custCode" ng-model="custCode" >
				<label>Enter Customer
				Code</label>
		</div>

		<div class="input-field col s6 center">
			<input class="btn waves-effect waves-light" type="submit"
				value="Submit">
		</div>

	</form>
	<div class="col s3 hide-on-med-and-down">&nbsp;</div>
</div>

<div id="customerCodeDB" ng-hide="CustomerCodeDBFlag">
	<input type="text" name="filterTextbox" ng-model="filterTextbox"
		placeholder="Search">
	<table>
		<tr>
			<th></th>
			<th>customer Code</th>
			<th>customer Name</th>
			<th>Branch Code</th>
			<th>customer FACode</th>
		</tr>
	
		<tr ng-repeat="cust in custCodeList | filter:filterTextbox">
			<td><input type="radio" name="custCode" value="{{ cust }}"
				ng-model="$parent.custCode" ng-change="saveCustomerCode(cust)"></td>
			<td>{{ cust.custCode }}</td>	
			<td>{{ cust.custName }}</td>
			<td>{{ cust.bCode }}</td>
			<td>{{ cust.custFaCode }}</td>
		</tr>
	</table>
</div>



<!-- <form ng-submit="submitCustomer(custCode)">
		<div id ="custCode">
				<table>
					<tr>
						<td>Enter Customer Code</td>
						<td><input type="text" name="custCode" ng-model="custCode"/></td>
					</tr>
					<tr>
						<td><input type="submit" value="Submit"/></td>
					</tr>
				</table>
		</div>
		</form> -->

<div class="row">
	<div class="col s3 hidden-xs hidden-sm">&nbsp;</div>
	<div ng-hide="show" class="col s6 card"
		style="align: center; margin-top: 0px; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
		<h4>
			Here's the detail of <span class="teal-text text-lighten-2">{{customer.custName}}</span>:
		</h4>
		<table class="table-hover table-bordered table-condensed">
			<tr>
				<td>Customer Name:</td>
				<td>{{customer.custName}}</td>
			</tr>
			<tr>
				<td>Customer Address:</td>
				<td>{{customer.custAdd}}</td>
			</tr>
			<tr>
				<td>Customer City:</td>
				<td>{{customer.custCity}}</td>
			</tr>

			<tr>
				<td>Customer State:</td>
				<td>{{customer.custState}}</td>
			</tr>

			<tr>
				<td>Customer Pin:</td>
				<td>{{customer.custPin}}</td>
			</tr>

			<tr>
				<td>Customer TDS Circle:</td>
				<td>{{customer.custTdsCircle}}</td>
			</tr>

			<tr>
				<td>Customer TDS City:</td>
				<td>{{customer.custTdsCity}}</td>
			</tr>

			<tr>
				<td>Customer PAN Number:</td>
				<td>{{customer.custPanNo}}</td>
			</tr>

			<tr>
				<td>Customer TIN Number:</td>
				<td>{{customer.custTinNo}}</td>
			</tr>

			<tr>
				<td>Customer Status:</td>
				<td>{{customer.custStatus}}</td>
			</tr>

			<tr>
				<td>Daily Contract Allowed:</td>
				<td>{{customer.custIsDailyContAllow}}</td>
			</tr>

			<tr>
				<td>Director:</td>
				<td>{{customer.custDirector}}</td>
			</tr>

			<tr>
				<td>Marketing Head:</td>
				<td>{{customer.custMktHead}}</td>
			</tr>

			<tr>
				<td>Corp. Executive:</td>
				<td>{{customer.custCorpExc}}</td>
			</tr>

			<tr>
				<td>Branch Person:</td>
				<td>{{customer.custBrPerson}}</td>
			</tr>

			<tr>
				<td>CR Period</td>
				<td>{{customer.custCrPeriod}}</td>
			</tr>

			<tr>
				<td>CR Base:</td>
				<td>{{customer.custCrBase}}</td>
			</tr>

			<tr>
				<td>Service Tax By:</td>
				<td>{{customer.custSrvTaxBy}}</td>
			</tr>
			
			<tr>
					<td>State GST Code:</td>
					<td>{{customer.stateGST}}</td>
			</tr>
				
			<tr>
					<td>GST No.:</td>
					<td>{{customer.custGstNo}}</td>
				</tr>
				
			<tr>
				<td>Bill Cycle:</td>
				<td>{{customer.custBillCycle}}</td>
			</tr>

			<tr>
				<td>Bill Value:</td>
				<td>{{customer.custBillValue}}</td>
			</tr>

			<tr>
				<td>Customer CreationTS:</td>
				<td>{{customer.creationTS}}</td>
			</tr>

			<tr ng-repeat="CRCode in custRepList">
				<td>Customer Representative Codes:</td>
				<td>{{CRCode}}</td>
			</tr>

		</table>

	</div>

</div>
</body>
</html>


</div>