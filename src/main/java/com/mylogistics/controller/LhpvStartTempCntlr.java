package com.mylogistics.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.LhpvTempDAO;
import com.mylogistics.model.Branch;
import com.mylogistics.model.LhpvTemp;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;

@Controller
public class LhpvStartTempCntlr {

	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private LhpvTempDAO lhpvTempDAO;
	
	@Autowired
	private BranchDAO branchDAO;
	
	@RequestMapping(value = "/getLhpvTempList" , method = RequestMethod.POST)  
	public @ResponseBody Object getLhpvTempList() {  
		System.out.println("Enter into getLhpvTempList---->");
		Map<String, Object> map = new HashMap<>();	
		User currentUser = (User) httpSession.getAttribute("currentUser");
		Branch branch = branchDAO.getBranchById(Integer.parseInt(currentUser.getUserBranchCode()));
		List<LhpvTemp> ltList = new ArrayList<>();
		ltList = lhpvTempDAO.getLhpvList(currentUser.getUserBranchCode());
		if(!ltList.isEmpty()){
			map.put("branch",branch);
			map.put("list",ltList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
}
