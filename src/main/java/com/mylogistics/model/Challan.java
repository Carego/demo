package com.mylogistics.model;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.mylogistics.model.lhpv.LhpvBal;
import com.mylogistics.model.lhpv.LhpvSup;
//import java.util.Date;

@Entity
@Table(name = "challan")

public class Challan{
	
	@Id
	@Column(name="chlnId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int chlnId;
	
	@Column(name="chlnCode")
	private String chlnCode;
	
	@Column(name="branchcode")
	private String branchCode;
	
	@Column(name="chlnLryNo")
	private String chlnLryNo;
	
	@Column(name="chlnFromStn")
	private String chlnFromStn;
	
	@Column(name="chlnToStn")
	private String chlnToStn;
	
	@Column(name="chlnEmpCode")
	private String chlnEmpCode;
	
	@Column(name="chlnLryRate")
	private String chlnLryRate;
	
	@Column(name="chlnChgWt")
	private String chlnChgWt;
	
	@Column(name="chlnNoOfPkg")
	private String chlnNoOfPkg;
	
	@Column(name="chlnTotalWt")
	private String chlnTotalWt;
	
	@Column(name="chlnFreight")
	private String chlnFreight;
	
	@Column(name="chlnLoadingAmt")
	private String chlnLoadingAmt;
	
	@Column(name="chlnExtra")
	private String chlnExtra;
	
	@Column(name="chlnDetection")
	private float chlnDetection;
	
	@Column(name="chlnToolTax")
	private float chlnToolTax;
	
	@Column(name="chlnHeight")
	private float chlnHeight;
	
	@Column(name="chlnUnion")
	private float chlnUnion;
	
	@Column(name="chlnTwoPoint")
	private float chlnTwoPoint;
	
	@Column(name="chlnWeightmentChg")
	private float chlnWeightmentChg;
	
	@Column(name="chlnCraneChg")
	private float chlnCraneChg;
	
	@Column(name="chlnOthers")
	private float chlnOthers;
	
	@Column(name="chlnTotalFreight")
	private double chlnTotalFreight;
	
	@Column(name="chlnAdvance")
	private String chlnAdvance;
	
	@Column(name="chlnBalance")
	private double chlnBalance;
	
	@Column(name="chlnPayAt")
	private String chlnPayAt;
	
	@Column(name="chlnTimeAllow")
	private String chlnTimeAllow;
	
	@Column(name="chlnDt")
	private Date chlnDt;
	
	@Column(name="chlnBrRate")
	private String chlnBrRate;
	
	@Column(name="chlnWtSlip")
	private String chlnWtSlip;
	
	@Column(name="chlnVehicleType")
	private String chlnVehicleType;
	
	@Column(name="chlnStatisticalChg")
	private String chlnStatisticalChg;
	
	@Column(name="chlnTdsAmt")
	private String chlnTdsAmt;
	
	@Column(name="chlnRrNo")
	private String chlnRrNo;
	
	@Column(name="chlnTrainNo")
	private String chlnTrainNo;
	
	@Column(name="chlnLryLoadTime")
	private String chlnLryLoadTime;
	
	@Column(name="chlnLryRepDT")
	private Date chlnLryRepDT;
	
	@Column(name="chlnLryRptTime")
	private String chlnLryRptTime;
	
	@Column(name="chlnArId" , columnDefinition="int default -1")
	private int chlnArId;
	
	@Column(name="chlnRemAdv" , columnDefinition="double default 0")
	private double chlnRemAdv;
	
	@Column(name="chlnRemBal" , columnDefinition="double default 0")
	private double chlnRemBal;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="chlnType", columnDefinition="varchar(255) default 'normal'")
	private String chlnType;

	@Column(name="bankName")
	private String bankName;
	
	@Column(name="ifscCode")
	private String ifscCode;

	@Column(name="accountNo")
	private String accountNo;

	@Column(name="payeeName")
	private String payeeName;

	@Column(name="cardFaCode")
	private String cardFaCode;

//	@Column(name="isPaid")
//	private Boolean isPaid;

	/*@Column(name="challanImage")
	private Blob challanImage;*/
	
	//Many To One
	/*@OneToMany
	@LazyCollection(LazyCollectionOption.TRUE)
	@JoinColumn(name = "laId") 
	private List<LhpvAdv> lhpvAdv = new ArrayList<>();*/
	
	//Many To One
	/*@OneToOne
	@LazyCollection(LazyCollectionOption.TRUE)
	@JoinColumn(name = "lbId") 
	private LhpvBal lhpvBal;*/
	
	//Many To One
	/*@OneToOne
	@LazyCollection(LazyCollectionOption.TRUE)
	@JoinColumn(name = "lspId") 
	private LhpvSup lhpvSup;*/
	
	@Column(name="isView")
	private boolean isView;
	
	@Column(name="bCode")
	private String bCode;

	@Column(name="chlnEdit", columnDefinition="boolean default false")
	private boolean chlnEdit;
	
	@Column(name="chlnBalPayAlw", columnDefinition="boolean default false")
	private boolean chlnBalPayAlw;
	
	
	@Column(name="isCancel" , columnDefinition="boolean default false")
	private boolean isCancel;
	
	@Column(name="chlnRemarks")
	private String chlnRemarks;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;
	
	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public boolean isView() {
		return isView;
	}

	public void setView(boolean isView) {
		this.isView = isView;
	}
	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}
	
	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public int getChlnId() {
		return chlnId;
	}

	public void setChlnId(int chlnId) {
		this.chlnId = chlnId;
	}

	public String getChlnCode() {
		return chlnCode;
	}

	public void setChlnCode(String chlnCode) {
		this.chlnCode = chlnCode;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getChlnLryNo() {
		return chlnLryNo;
	}

	public void setChlnLryNo(String chlnLryNo) {
		this.chlnLryNo = chlnLryNo;
	}

	public String getChlnFromStn() {
		return chlnFromStn;
	}

	public void setChlnFromStn(String chlnFromStn) {
		this.chlnFromStn = chlnFromStn;
	}

	public String getChlnToStn() {
		return chlnToStn;
	}

	public void setChlnToStn(String chlnToStn) {
		this.chlnToStn = chlnToStn;
	}

	public String getChlnEmpCode() {
		return chlnEmpCode;
	}

	public void setChlnEmpCode(String chlnEmpCode) {
		this.chlnEmpCode = chlnEmpCode;
	}

	public String getChlnLryRate() {
		return chlnLryRate;
	}

	public void setChlnLryRate(String chlnLryRate) {
		this.chlnLryRate = chlnLryRate;
	}

	public String getChlnChgWt() {
		return chlnChgWt;
	}

	public void setChlnChgWt(String chlnChgWt) {
		this.chlnChgWt = chlnChgWt;
	}

	public String getChlnNoOfPkg() {
		return chlnNoOfPkg;
	}

	public void setChlnNoOfPkg(String chlnNoOfPkg) {
		this.chlnNoOfPkg = chlnNoOfPkg;
	}

	public String getChlnTotalWt() {
		return chlnTotalWt;
	}

	public void setChlnTotalWt(String chlnTotalWt) {
		this.chlnTotalWt = chlnTotalWt;
	}

	

	public String getChlnLoadingAmt() {
		return chlnLoadingAmt;
	}

	public void setChlnLoadingAmt(String chlnLoadingAmt) {
		this.chlnLoadingAmt = chlnLoadingAmt;
	}

	public String getChlnExtra() {
		return chlnExtra;
	}

	public void setChlnExtra(String chlnExtra) {
		this.chlnExtra = chlnExtra;
	}

	public double getChlnTotalFreight() {
		return chlnTotalFreight;
	}

	public void setChlnTotalFreight(double chlnTotalFreight) {
		this.chlnTotalFreight = chlnTotalFreight;
	}

	public String getChlnAdvance() {
		return chlnAdvance;
	}

	public void setChlnAdvance(String chlnAdvance) {
		this.chlnAdvance = chlnAdvance;
	}

	public double getChlnBalance() {
		return chlnBalance;
	}

	public void setChlnBalance(double chlnBalance) {
		this.chlnBalance = chlnBalance;
	}

	public String getChlnPayAt() {
		return chlnPayAt;
	}

	public void setChlnPayAt(String chlnPayAt) {
		this.chlnPayAt = chlnPayAt;
	}

	public String getChlnTimeAllow() {
		return chlnTimeAllow;
	}

	public void setChlnTimeAllow(String chlnTimeAllow) {
		this.chlnTimeAllow = chlnTimeAllow;
	}

	public Date getChlnDt() {
		return chlnDt;
	}

	public void setChlnDt(Date chlnDt) {
		this.chlnDt = chlnDt;
	}

	public String getChlnBrRate() {
		return chlnBrRate;
	}

	public void setChlnBrRate(String chlnBrRate) {
		this.chlnBrRate = chlnBrRate;
	}

	public String getChlnWtSlip() {
		return chlnWtSlip;
	}

	public void setChlnWtSlip(String chlnWtSlip) {
		this.chlnWtSlip = chlnWtSlip;
	}

	public String getChlnVehicleType() {
		return chlnVehicleType;
	}

	public void setChlnVehicleType(String chlnVehicleType) {
		this.chlnVehicleType = chlnVehicleType;
	}

	public String getChlnStatisticalChg() {
		return chlnStatisticalChg;
	}

	public void setChlnStatisticalChg(String chlnStatisticalChg) {
		this.chlnStatisticalChg = chlnStatisticalChg;
	}

	public String getChlnTdsAmt() {
		return chlnTdsAmt;
	}

	public void setChlnTdsAmt(String chlnTdsAmt) {
		this.chlnTdsAmt = chlnTdsAmt;
	}

	/*public String getChlnChdCode() {
		return chlnChdCode;
	}

	public void setChlnChdCode(String chlnChdCode) {
		this.chlnChdCode = chlnChdCode;
	}*/

	public String getChlnRrNo() {
		return chlnRrNo;
	}

	public void setChlnRrNo(String chlnRrNo) {
		this.chlnRrNo = chlnRrNo;
	}

	public String getChlnTrainNo() {
		return chlnTrainNo;
	}

	public void setChlnTrainNo(String chlnTrainNo) {
		this.chlnTrainNo = chlnTrainNo;
	}

	public String getChlnLryLoadTime() {
		return chlnLryLoadTime;
	}

	public void setChlnLryLoadTime(String chlnLryLoadTime) {
		this.chlnLryLoadTime = chlnLryLoadTime;
	}

	public Date getChlnLryRepDT() {
		return chlnLryRepDT;
	}

	public void setChlnLryRepDT(Date chlnLryRepDT) {
		this.chlnLryRepDT = chlnLryRepDT;
	}

	public String getChlnLryRptTime() {
		return chlnLryRptTime;
	}

	public void setChlnLryRptTime(String chlnLryRptTime) {
		this.chlnLryRptTime = chlnLryRptTime;
	}

	/*public LhpvAdv getLhpvAdv() {
		return lhpvAdv;
	}

	public void setLhpvAdv(LhpvAdv lhpvAdv) {
		this.lhpvAdv = lhpvAdv;
	}*/

	/*public List<LhpvAdv> getLhpvAdv() {
		return lhpvAdv;
	}

	public void setLhpvAdv(List<LhpvAdv> lhpvAdv) {
		this.lhpvAdv = lhpvAdv;
	}*/

	public String getChlnFreight() {
		return chlnFreight;
	}

	public void setChlnFreight(String chlnFreight) {
		this.chlnFreight = chlnFreight;
	}

	/*public LhpvBal getLhpvBal() {
		return lhpvBal;
	}

	public void setLhpvBal(LhpvBal lhpvBal) {
		this.lhpvBal = lhpvBal;
	}*/

	public int getChlnArId() {
		return chlnArId;
	}

	public void setChlnArId(int chlnArId) {
		this.chlnArId = chlnArId;
	}

	/*public LhpvSup getLhpvSup() {
		return lhpvSup;
	}

	public void setLhpvSup(LhpvSup lhpvSup) {
		this.lhpvSup = lhpvSup;
	}*/

	public double getChlnRemAdv() {
		return chlnRemAdv;
	}

	public void setChlnRemAdv(double chlnRemAdv) {
		this.chlnRemAdv = chlnRemAdv;
	}

	public double getChlnRemBal() {
		return chlnRemBal;
	}

	public void setChlnRemBal(double chlnRemBal) {
		this.chlnRemBal = chlnRemBal;
	}

	public boolean isChlnEdit() {
		return chlnEdit;
	}

	public void setChlnEdit(boolean chlnEdit) {
		this.chlnEdit = chlnEdit;
	}

	public String getChlnType() {
		return chlnType;
	}

	public void setChlnType(String chlnType) {
		this.chlnType = chlnType;
	}

	public boolean isCancel() {
		return isCancel;
	}

	public void setCancel(boolean isCancel) {
		this.isCancel = isCancel;
	}

	public float getChlnDetection() {
		return chlnDetection;
	}

	public void setChlnDetection(float chlnDetection) {
		this.chlnDetection = chlnDetection;
	}

	public float getChlnToolTax() {
		return chlnToolTax;
	}

	public void setChlnToolTax(float chlnToolTax) {
		this.chlnToolTax = chlnToolTax;
	}

	public float getChlnHeight() {
		return chlnHeight;
	}

	public void setChlnHeight(float chlnHeight) {
		this.chlnHeight = chlnHeight;
	}

	public float getChlnUnion() {
		return chlnUnion;
	}

	public void setChlnUnion(float chlnUnion) {
		this.chlnUnion = chlnUnion;
	}

	public float getChlnTwoPoint() {
		return chlnTwoPoint;
	}

	public void setChlnTwoPoint(float chlnTwoPoint) {
		this.chlnTwoPoint = chlnTwoPoint;
	}

	public float getChlnWeightmentChg() {
		return chlnWeightmentChg;
	}

	public void setChlnWeightmentChg(float chlnWeightmentChg) {
		this.chlnWeightmentChg = chlnWeightmentChg;
	}

	public float getChlnCraneChg() {
		return chlnCraneChg;
	}

	public void setChlnCraneChg(float chlnCraneChg) {
		this.chlnCraneChg = chlnCraneChg;
	}

	public float getChlnOthers() {
		return chlnOthers;
	}

	public void setChlnOthers(float chlnOthers) {
		this.chlnOthers = chlnOthers;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public String getPayeeName() {
		return payeeName;
	}

	public void setPayeeName(String payeeName) {
		this.payeeName = payeeName;
	}

	public boolean isChlnBalPayAlw() {
		return chlnBalPayAlw;
	}

	public void setChlnBalPayAlw(boolean chlnBalPayAlw) {
		this.chlnBalPayAlw = chlnBalPayAlw;
	}

	public String getCardFaCode() {
		return cardFaCode;
	}

	public void setCardFaCode(String cardFaCode) {
		this.cardFaCode = cardFaCode;
	}

	public String getChlnRemarks() {
		return chlnRemarks;
	}

	public void setChlnRemarks(String chlnRemarks) {
		this.chlnRemarks = chlnRemarks;
	}

//	public Boolean getIsPaid() {
//		return isPaid;
//	}
//
//	public void setIsPaid(Boolean isPaid) {
//		this.isPaid = isPaid;
//	}
	
	
	
}