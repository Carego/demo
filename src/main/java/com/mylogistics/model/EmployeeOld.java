package com.mylogistics.model;

import java.sql.Date;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "employeeold")
public class EmployeeOld {
	
	@Id
	@Column(name="empOldId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int empOldId;
	
	@Column(name="empId")
	private int empId;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="empCode")
	private String empCode;
	
	@Column(name="empName")
	private String empName;
	
	@Column(name="empAdd")
	private String empAdd;
	
	@Column(name="empCity")
	private String empCity;

    @Column(name="empState")
	private String empState;
    
    @Column(name="empPin")
	private String empPin;
    
    @Column(name="empDOB")
	private Date empDOB;
    
    @Column(name="empDOAppointment")
	private Date empDOAppointment;
    
    @Column(name="empDOResig")
	private Date empDOResig;

	@Column(name="empDesignation")
	private String empDesignation;
    
    @Column(name="empBasic")
	private double empBasic;
    
    @Column(name="empHRA")
	private double empHRA;
    
    @Column(name="empOtherAllowance")
	private double empOtherAllowance;
    
    @Column(name="empPFNo")
	private String empPFNo;
    
    @Column(name="empESINo")
	private String empESINo;
    
    @Column(name="empESIDispensary")
	private String empESIDispensary;
    
    @Column(name="empNominee")
	private String empNominee;
    
    @Column(name="empEduQuali")
	private String empEduQuali;
    
    @Column(name="empPF")
	private double empPF;
    
    @Column(name="empESI")
	private double empESI;
    
    @Column(name="empLoanBal")
	private double empLoanBal;
    
    @Column(name="empLoanPayment")
	private double empLoanPayment;
    
    @Column(name="empPresentAdd")
	private String empPresentAdd;
    
    @Column(name="empPresentCity")
	private String empPresentCity;
    
    @Column(name="empPresentState")
	private String empPresentState;
    
    @Column(name="empPresentPin")
	private String empPresentPin;
    
    @Column(name="empSex")
	private String empSex;
    
    @Column(name="empFatherName")
	private String empFatherName;
    
    @Column(name="empLicenseNo")
	private String empLicenseNo;
    
    @Column(name="empLicenseExp")
	private Date empLicenseExp;
    
    @Column(name="bCode")
	private String bCode;
    
    @Column(name="isView")
	private boolean isView;
    
    @Column(name="isTerminate")
	private String isTerminate;
   
	@Column(name="empTelephoneAmt")
	private double empTelephoneAmt;
    
    @Column(name="empMobAmt")
	private double empMobAmt;
    
    @Column(name="empBankAcNo")
   	private String empBankAcNo;
    
    @Column(name="empPanNo")
	private String empPanNo;
    
    @Column(name="empMailId")
	private String empMailId;
    
    @Column(name="empPhNo")
	private String empPhNo;
    
    @Column(name="empBankIFS")
	private String empBankIFS;
    
    @Column(name="empBankName")
	private String empBankName;
    
    @Column(name="empBankBranch")
	private String empBankBranch;

	@Column(name="branchCode")
	private String branchCode;
    
    @Column(name="empGross")
	private double empGross;
    
    @Column(name="netSalary")
	private double netSalary;
    
    @Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
    private Calendar creationTS;

    public Calendar getCreationTS() {
        return creationTS;
    }

    public void setCreationTS(Calendar creationTS) {
        this.creationTS = creationTS;
    }
    
 	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}  

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public String getEmpCode() {
		return empCode;
	}

	public void setEmpCode(String empCode) {
		this.empCode = empCode;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getEmpAdd() {
		return empAdd;
	}

	public void setEmpAdd(String empAdd) {
		this.empAdd = empAdd;
	}

	public String getEmpCity() {
		return empCity;
	}

	public void setEmpCity(String empCity) {
		this.empCity = empCity;
	}

	public String getEmpState() {
		return empState;
	}

	public void setEmpState(String empState) {
		this.empState = empState;
	}

	public String getEmpDesignation() {
		return empDesignation;
	}

	public void setEmpDesignation(String empDesignation) {
		this.empDesignation = empDesignation;
	}

	public String getEmpPFNo() {
		return empPFNo;
	}

	public void setEmpPFNo(String empPFNo) {
		this.empPFNo = empPFNo;
	}

	public String getEmpESINo() {
		return empESINo;
	}

	public void setEmpESINo(String empESINo) {
		this.empESINo = empESINo;
	}

	public String getEmpESIDispensary() {
		return empESIDispensary;
	}

	public void setEmpESIDispensary(String empESIDispensary) {
		this.empESIDispensary = empESIDispensary;
	}

	public String getEmpNominee() {
		return empNominee;
	}

	public void setEmpNominee(String empNominee) {
		this.empNominee = empNominee;
	}

	public String getEmpEduQuali() {
		return empEduQuali;
	}

	public void setEmpEduQuali(String empEduQuali) {
		this.empEduQuali = empEduQuali;
	}

	public String getEmpPresentAdd() {
		return empPresentAdd;
	}

	public void setEmpPresentAdd(String empPresentAdd) {
		this.empPresentAdd = empPresentAdd;
	}

	public String getEmpPresentCity() {
		return empPresentCity;
	}

	public void setEmpPresentCity(String empPresentCity) {
		this.empPresentCity = empPresentCity;
	}

	public String getEmpPresentState() {
		return empPresentState;
	}

	public void setEmpPresentState(String empPresentState) {
		this.empPresentState = empPresentState;
	}

	public String getEmpSex() {
		return empSex;
	}

	public void setEmpSex(String empSex) {
		this.empSex = empSex;
	}

	public String getEmpFatherName() {
		return empFatherName;
	}

	public void setEmpFatherName(String empFatherName) {
		this.empFatherName = empFatherName;
	}

	public String getEmpLicenseNo() {
		return empLicenseNo;
	}

	public void setEmpLicenseNo(String empLicenseNo) {
		this.empLicenseNo = empLicenseNo;
	}

	public double getEmpBasic() {
		return empBasic;
	}

	public void setEmpBasic(double empBasic) {
		this.empBasic = empBasic;
	}

	public double getEmpHRA() {
		return empHRA;
	}

	public void setEmpHRA(double empHRA) {
		this.empHRA = empHRA;
	}

	public double getEmpOtherAllowance() {
		return empOtherAllowance;
	}

	public void setEmpOtherAllowance(double empOtherAllowance) {
		this.empOtherAllowance = empOtherAllowance;
	}

	public double getEmpPF() {
		return empPF;
	}

	public void setEmpPF(double empPF) {
		this.empPF = empPF;
	}

	public double getEmpESI() {
		return empESI;
	}

	public void setEmpESI(double empESI) {
		this.empESI = empESI;
	}

	public double getEmpLoanBal() {
		return empLoanBal;
	}

	public void setEmpLoanBal(double empLoanBal) {
		this.empLoanBal = empLoanBal;
	}

	public double getEmpLoanPayment() {
		return empLoanPayment;
	}

	public void setEmpLoanPayment(double empLoanPayment) {
		this.empLoanPayment = empLoanPayment;
	}

	public double getEmpTelephoneAmt() {
		return empTelephoneAmt;
	}

	public void setEmpTelephoneAmt(double empTelephoneAmt) {
		this.empTelephoneAmt = empTelephoneAmt;
	}

	public double getEmpMobAmt() {
		return empMobAmt;
	}

	public void setEmpMobAmt(double empMobAmt) {
		this.empMobAmt = empMobAmt;
	}

	public double getEmpGross() {
		return empGross;
	}

	public void setEmpGross(double empGross) {
		this.empGross = empGross;
	}

	public double getNetSalary() {
		return netSalary;
	}

	public void setNetSalary(double netSalary) {
		this.netSalary = netSalary;
	}

	public String getEmpBankAcNo() {
		return empBankAcNo;
	}

	public void setEmpBankAcNo(String empBankAcNo) {
		this.empBankAcNo = empBankAcNo;
	}

	public String getEmpPanNo() {
		return empPanNo;
	}

	public void setEmpPanNo(String empPanNo) {
		this.empPanNo = empPanNo;
	}

	public String getEmpMailId() {
		return empMailId;
	}

	public void setEmpMailId(String empMailId) {
		this.empMailId = empMailId;
	}

	public String getEmpPhNo() {
		return empPhNo;
	}

	public void setEmpPhNo(String empPhNo) {
		this.empPhNo = empPhNo;
	}

	public String getEmpBankIFS() {
		return empBankIFS;
	}

	public void setEmpBankIFS(String empBankIFS) {
		this.empBankIFS = empBankIFS;
	}

	public String getEmpBankName() {
		return empBankName;
	}

	public void setEmpBankName(String empBankName) {
		this.empBankName = empBankName;
	}

	public String getEmpBankBranch() {
		return empBankBranch;
	}

	public void setEmpBankBranch(String empBankBranch) {
		this.empBankBranch = empBankBranch;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	
	public Date getEmpDOResig() {
		return empDOResig;
	}

	public void setEmpDOResig(Date empDOResig) {
		this.empDOResig = empDOResig;
	}
	
	public String getIsTerminate() {
		return isTerminate;
	}

	public void setIsTerminate(String isTerminate) {
		this.isTerminate = isTerminate;
	}

	public boolean isView() {
		return isView;
	}

	public void setView(boolean isView) {
		this.isView = isView;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public Date getEmpDOB() {
		return empDOB;
	}

	public void setEmpDOB(Date empDOB) {
		this.empDOB = empDOB;
	}

	public Date getEmpDOAppointment() {
		return empDOAppointment;
	}

	public void setEmpDOAppointment(Date empDOAppointment) {
		this.empDOAppointment = empDOAppointment;
	}

	public Date getEmpLicenseExp() {
		return empLicenseExp;
	}

	public void setEmpLicenseExp(Date empLicenseExp) {
		this.empLicenseExp = empLicenseExp;
	}
	
	 public String getEmpPin() {
			return empPin;
	}

		public void setEmpPin(String empPin) {
			this.empPin = empPin;
	}

		public String getEmpPresentPin() {
			return empPresentPin;
	}

		public void setEmpPresentPin(String empPresentPin) {
			this.empPresentPin = empPresentPin;
	}

}
