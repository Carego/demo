<div ng-show="adminLogin || superAdminLogin">

<title>Display Owner</title>
	<div class="container">
		<div class="row">
			<div class="col s2"><input type="radio" name="ownerCodeList" ng-model="ownCodeByModal" ng-click="enableModalTextBox()"></div>
			<div class="col s10"><input type = "text" id="ownCodeByMId" name="ownCodeByM" ng-model="ownCodeByM" disabled="disabled" ng-click="OpenOwnCodeCodeDB()" readonly></div>
		</div>
		<div class="row">
			<div class="col s2"><input type="radio" name="OwnerCodeList" ng-model="ownCodeByAuto" ng-click="enableAutoTextBox()"></div>
			<div class="col s8"><input type = "text" id="ownCodeByAId" name="ownCodeByA" ng-model="ownCodeByA" disabled="disabled" ng-keyup="getOwnCodeList()" ></div>
			<div class="col s2"><input type ="button" id="ok" value = "OK" ng-click="getOwnerList()" disabled="disabled"></div>
		</div>
		
		<div class="row">
			<div class="col s12"><input type ="button" value = "Back To list" ng-click="backToList()"></div>
		</div>
	</div>
		
		<div id ="OwnerCodeDB" ng-hide = "OwnerCodeFlag">
 	  		<div class="input-field">
 	  		<input type="text" name="filterOwnerCode" ng-model="filterOwnerCode" placeholder="Search by Owner Code">
 	  		</div>
 	   	  <table>
		  <tr ng-repeat="ownerCodes in ownerCodeList | filter:filterOwnerCode">
		 	  <td><input type="radio"  name="OwnerName"  value="{{ownerCodes }}" ng-model="ownCodes" ng-click="saveOwnCode(ownerCodes)"></td>
              <td>{{ ownerCodes }}</td>
          </tr>
         </table>       
	</div>
	
	<div class="container" ng-hide="isViewNo">
	  <input type="text" name="filterTextbox" ng-model="filterTextbox.ownCode" placeholder="Search by Owner Code">	
 	  <table class="table-hover table-bordered table-condensed">
 	 		<tr>
				<th></th>
				
				<th>Owner Code</th>
				
				<th>Owner  Name</th>
			
				<th>User Code</th>
				
				<th>Time Stamp</th>
			</tr>
 	  
		  
		  <tr ng-repeat="owner in OwnerList | filter:filterTextbox">
		 	  <td><input type="checkbox" value="{{ owner.ownCode }}"  ng-checked="selection.indexOf(owner.ownCode) > -1" ng-click="toggleSelection(owner.ownCode)" /></td>
			      <td>{{ owner.ownCode}}</td>
	              <td>{{ owner.ownName }}</td>
	              <td>{{ owner.userCode }}</td>
	              <td>{{ owner.creationTS }}</td>
          </tr>
        </table>
        <table>
         	 <tr>
				<td class="center"><input type="submit" value="Submit" ng-click="verifyOwner()"></td>
			</tr>
		</table>
		
		</div>
		
		<div class="container" ng-hide = "showOwnerDetails" >		
		<div class="row">
			<div class="input-field col s12 m4 l3">
					<input type ="text" name ="branchCode" ng-model="owner.branchCode" value="{{ owner.branchCode }}">
				    <label>Branch Code</label> 
			</div>
			
			<div class="input-field col s12 m4 l3">
					<input type ="text" name ="ownCode" ng-model="owner.ownCode" value="{{ owner.ownCode}}" readonly>
					<label>Owner Code</label> 
			</div>
			
			<div class="input-field col s12 m4 l3">
					<input type ="text" name ="ownName" id="OwnerName" ng-model="owner.ownName" value="{{ owner.ownName}}">
					<label>Owner Name</label> 
			</div>
			
			<div class="input-field col s12 m4 l3">
					<input type ="date" name ="ownComStbDt" value="{{ owner.ownComStbDt}}">
					<label>Owner Com Establishing Date</label> 
			</div>
			
			<div class="input-field col s12 m4 l3">
					<input type ="date" name ="ownPanDOB" ng-model="owner.ownPanDOB" value="{{owner.ownPanDOB }}">
					<label>Owner Pan DOB</label> 
			</div>
			
			<div class="input-field col s12 m4 l3">
					<input type ="date" name ="ownPanDt" ng-model="owner.ownPanDt" value="{{ owner.ownPanDt}}">
					<label>Owner Pan Date</label> 
			</div>
			
			<div class="input-field col s12 m4 l3">
					<input type ="text" name ="ownPanName" ng-model="owner.ownPanName" value="{{ owner.ownPanName}}">
					<label>Owner Pan Name</label> 
			</div>
			
			
			<div class="input-field col s12 m4 l3">
					<input type ="text" name ="ownVehicleType" ng-model="owner.ownVehicleType" value="{{ owner.ownVehicleType }}">
					<label>Owner Vehicle Type</label> 
			</div>
			
			<div class="input-field col s12 m4 l3">
					<input type ="text"  id="ownPPNo" name ="ownPPNo" ng-model="owner.ownPPNo" value="{{ owner.ownPPNo }}" >
					<label>Owner PPNo</label> 
			</div>
			
			<div class="input-field col s12 m4 l3">
					<input type ="text" name ="ownVoterId" id="ownVoterId" ng-model="owner.ownVoterId" value="{{ owner.ownVoterId }}">
					<label>Owner Voter Id</label> 
			</div>
			
			<div class="input-field col s12 m4 l3">
					<input type ="text" name ="ownSrvTaxNo" id="ownSrvTaxNo" ng-model="owner.ownSrvTaxNo" value="{{ owner.ownSrvTaxNo }}">
					<label>Owner Service Tax No</label> 
			</div>
			
			<div class="input-field col s12 m4 l3">
					<input type ="text" name ="ownFirmRegNo" id="ownFirmRegNo" ng-model="owner.ownFirmRegNo" value="{{ owner.ownFirmRegNo }}">
					<label>Owner Firm Reg No</label> 
			</div>
			
			<div class="input-field col s12 m4 l3">
					<input type ="text" name ="ownRegPlace" id="ownRegPlace" ng-model="owner.ownRegPlace" value="{{ owner.ownRegPlace}}">
					<label>Owner Reg Place</label> 
			</div>
			
			<div class="input-field col s12 m4 l3">
                <input type="text" name="ownFirmType"  ng-model="owner.ownFirmType" value="{{ owner.ownFirmType}}">
                <label>Owner Firm Type</label>
            </div>
			
			<div class="input-field col s12 m4 l3">
					<input type ="text" name ="ownCIN" id="ownCIN" ng-model="owner.ownCIN" value="{{owner.ownCIN }}">
					<label>Owner CIN</label> 
			</div>
			
			<div class="input-field col s12 m4 l3">
					<input type ="text" name ="ownBsnCard" ng-model="owner.ownBsnCard" value="{{ owner.ownBsnCard}}">
					<label>Owner Bsn Card</label> 
			</div>
			
			<div class="input-field col s12 m4 l3">
					<input type ="text" name ="ownUnion" id="ownUnion" ng-model="owner.ownUnion" value="{{ owner.ownUnion}}">
					<label>Owner Union</label> 
			</div>
			
			<div class="input-field col s12 m4 l3">
					<input type ="text" name ="ownEmailId" ng-model="owner.ownEmailId" value="{{ owner.ownEmailId}}">
					<label>Owner Email Id</label> 
			</div>
			
			<div class="input-field col s12 m4 l3">
					<input type ="date" name ="ownActiveDt" ng-model="owner.ownActiveDt" value="{{ owner.ownActiveDt}}">
					<label>Owner Activate Date</label> 
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
					<input type="button" value="Submit" ng-click="EditOwnerSubmit(owner)" >
			</div>		
       </div>
	</div>
	</div>