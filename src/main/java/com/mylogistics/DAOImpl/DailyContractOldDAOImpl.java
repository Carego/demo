package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.DailyContractOldDAO;
import com.mylogistics.constants.DailyContractOldCNTS;
import com.mylogistics.model.DailyContractOld;

public class DailyContractOldDAOImpl implements DailyContractOldDAO{

	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	public DailyContractOldDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	 @Transactional
	   public int saveDlyContOld(DailyContractOld dailyContractOld){
		 System.out.println("enter into saveDlyContOld function--->");
		 int temp;
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 session.save(dailyContractOld);
			 transaction.commit();
			 temp = 1;
			 session.flush();
		 }catch(Exception e){
			 e.printStackTrace();
			 temp = -1;
		 }
		 session.clear();
		 session.close();
		 return temp;
	 }
	
	 
	 @Transactional
	 @SuppressWarnings("unchecked")
	 public List<DailyContractOld> getOldDailyContract(String contractCode){
		 System.out.println("Enter into getOldDailyContract function");
		 List<DailyContractOld> result = new ArrayList<DailyContractOld>();
				 
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 Criteria cr=session.createCriteria(DailyContractOld.class);
			 cr.add(Restrictions.eq(DailyContractOldCNTS.DLY_CONT_CODE,contractCode));
			 result =cr.list();
			 transaction.commit();
			 session.flush();
			 System.out.println("After getting the list from old daily contract"+result.size());
		 }
		 catch(Exception e){
			 e.printStackTrace();
		 }
		 session.clear();
		 session.close();
		 return  result; 
	 }
}
