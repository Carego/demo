package com.mylogistics.services;


import java.sql.Date;

import com.mylogistics.model.CashStmtStatus;


public interface CashStmtStatusService {
	
	public CashStmtStatus getCashStmtStatus(String branchCode, Date cssDate);
	public int getLastVoucherNo(int cssId);
	public void merge(CashStmtStatus css);
	
}