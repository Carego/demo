package com.mylogistics.model;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name = "owner")
public class Owner {
	
	@Id
	@Column(name="ownId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int ownId;
	
	@Column(name="ownImgId" , columnDefinition = "int default -1")
	private Integer ownImgId;
	
	@Column(name="ownCode")
	private String ownCode;
	
	@Column(name="ownIsPanImg" , columnDefinition = "boolean default false")
	private boolean ownIsPanImg;
	
	@Column(name="ownIsDecImg" , columnDefinition = "boolean default false")
	private boolean ownIsDecImg;
	
	@Column(name="ownIsChqImg" , columnDefinition = "boolean default false")
	private boolean ownIsChqImg;
	
	@Column(name="branchCode")
	private String branchCode;
	
	@Column(name="ownName")
	private String ownName;
	
	@Column(name="ownComStbDt")
	private Date ownComStbDt;
	
	@Column(name="ownPanDOB")
	private Date ownPanDOB;
	
	@Column(name="ownPanDt")
	private Date ownPanDt;
	
	@Column(name="ownPanName")
	private String ownPanName;
	
	@Column(name="ownPanNo")
	private String ownPanNo;
	
	@Column(name="ownVehicleType")
	private String ownVehicleType;
	
	@Column(name="ownPPNo")
	private String ownPPNo;
	
	@Column(name="ownVoterId")
	private String ownVoterId;
	
	@Column(name="ownSrvTaxNo")
	private String ownSrvTaxNo;
	
	@Column(name="ownFirmRegNo")
	private String ownFirmRegNo;
	
	@Column(name="ownRegPlace")
	private String ownRegPlace;
	
	@Column(name="ownFirmType")
	private String ownFirmType;
	
	@Column(name="ownCIN")
	private String ownCIN;
	
	@Column(name="ownBsnCard")
	private String ownBsnCard;
	
	@Column(name="ownUnion")
	private String ownUnion;

	@Column(name="ownEmailId")
	private String ownEmailId;
	
	@Column(name="ownActiveDt")
	private Date ownActiveDt;
	
	@Column(name="ownDeactiveDt")
	private Date ownDeactiveDt;
	
	@Column(name="ownPhNoList")
	private ArrayList<String> ownPhNoList = new ArrayList<String>();
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="ownFaCode")
	private String ownFaCode;

	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="isView")
	private boolean isView;
	
	@Column(name="ownPanIntRt" , columnDefinition = "float default 0.0")
	private float ownPanIntRt;
	
	@LazyCollection(LazyCollectionOption.TRUE)
	//@Basic(fetch=FetchType.LAZY)
	@OneToOne(cascade = CascadeType.ALL)
    private FAParticular fAParticular;
	
	@Column(name="ownAccntNo")
	private String ownAccntNo;
	
	@Column(name="ownIfsc")
	private String ownIfsc;
	
	@Column(name="ownMicr")
	private String ownMicr;
	
	@Column(name="ownBnkBranch")
	private String ownBnkBranch;
	
	@Column(name="ownAcntHldrName")
	private String ownAcntHldrName;
	
	@Column(name="isOwnBnkDet",nullable = false,columnDefinition = "bit default false")
	private boolean isOwnBnkDet;
	
	@Column(name="ownValidPan",nullable = false,columnDefinition = "bit default false")
	private boolean ownValidPan;
	
	@Column(name="ownInvalidPan",nullable = false,columnDefinition = "bit default false")
	private boolean ownInvalidPan;
	
	@Column(name="ownFather",length=128)
	private String ownFather;
	
	@Column(name="ownBnkDetValid", columnDefinition="boolean default false")
	private boolean ownBnkDetValid;
	
	@Column(name="ownBnkDetInValid", columnDefinition="boolean default false")
	private boolean ownBnkDetInValid;
	
	@Column(name="ownBnkDetVerifyBy", length=6)
	private String ownBnkDetVerifyBy;
	
	@Column(name="empId",columnDefinition = "int default 0")
	private int empId;
	
	@LazyCollection(LazyCollectionOption.TRUE)
	@JsonIgnore
	/*@OneToMany(mappedBy = "owner", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)*/
	@OneToMany(cascade = {CascadeType.ALL})
	@JoinTable(name="owner_vehven",
	joinColumns=@JoinColumn(name="ownId"),
	inverseJoinColumns=@JoinColumn(name="vvId"))
	private List<VehicleVendorMstr> vehicleVendorMstrList = new ArrayList<>();
	
	//@Lob  
	//@Basic(fetch=FetchType.LAZY)  
	/*@Column(name="ownPanImg")
	private Blob ownPanImg;*/
	
	/*@Column(name="ownDecImg")
	private Blob ownDecImg;*/

	public int getOwnId() {
		return ownId;
	}

	public void setOwnId(int ownId) {
		this.ownId = ownId;
	}

	public String getOwnCode() {
		return ownCode;
	}

	public void setOwnCode(String ownCode) {
		this.ownCode = ownCode;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getOwnName() {
		return ownName;
	}

	public void setOwnName(String ownName) {
		this.ownName = ownName;
	}

	public Date getOwnComStbDt() {
		return ownComStbDt;
	}

	public void setOwnComStbDt(Date ownComStbDt) {
		this.ownComStbDt = ownComStbDt;
	}

	public Date getOwnPanDOB() {
		return ownPanDOB;
	}

	public void setOwnPanDOB(Date ownPanDOB) {
		this.ownPanDOB = ownPanDOB;
	}

	public Date getOwnPanDt() {
		return ownPanDt;
	}

	public void setOwnPanDt(Date ownPanDt) {
		this.ownPanDt = ownPanDt;
	}

	public String getOwnPanName() {
		return ownPanName;
	}

	public void setOwnPanName(String ownPanName) {
		this.ownPanName = ownPanName;
	}

	public String getOwnVehicleType() {
		return ownVehicleType;
	}

	public void setOwnVehicleType(String ownVehicleType) {
		this.ownVehicleType = ownVehicleType;
	}

	public String getOwnPPNo() {
		return ownPPNo;
	}

	public void setOwnPPNo(String ownPPNo) {
		this.ownPPNo = ownPPNo;
	}

	public String getOwnVoterId() {
		return ownVoterId;
	}

	public void setOwnVoterId(String ownVoterId) {
		this.ownVoterId = ownVoterId;
	}

	public String getOwnSrvTaxNo() {
		return ownSrvTaxNo;
	}

	public void setOwnSrvTaxNo(String ownSrvTaxNo) {
		this.ownSrvTaxNo = ownSrvTaxNo;
	}

	public String getOwnFirmRegNo() {
		return ownFirmRegNo;
	}

	public void setOwnFirmRegNo(String ownFirmRegNo) {
		this.ownFirmRegNo = ownFirmRegNo;
	}

	public String getOwnRegPlace() {
		return ownRegPlace;
	}

	public void setOwnRegPlace(String ownRegPlace) {
		this.ownRegPlace = ownRegPlace;
	}

	public String getOwnFirmType() {
		return ownFirmType;
	}

	public void setOwnFirmType(String ownFirmType) {
		this.ownFirmType = ownFirmType;
	}

	public String getOwnCIN() {
		return ownCIN;
	}

	public void setOwnCIN(String ownCIN) {
		this.ownCIN = ownCIN;
	}

	public String getOwnBsnCard() {
		return ownBsnCard;
	}

	public void setOwnBsnCard(String ownBsnCard) {
		this.ownBsnCard = ownBsnCard;
	}

	public String getOwnUnion() {
		return ownUnion;
	}

	public void setOwnUnion(String ownUnion) {
		this.ownUnion = ownUnion;
	}

	public String getOwnEmailId() {
		return ownEmailId;
	}

	public void setOwnEmailId(String ownEmailId) {
		this.ownEmailId = ownEmailId;
	}

	public Date getOwnActiveDt() {
		return ownActiveDt;
	}

	public void setOwnActiveDt(Date ownActiveDt) {
		this.ownActiveDt = ownActiveDt;
	}

	public Date getOwnDeactiveDt() {
		return ownDeactiveDt;
	}

	public void setOwnDeactiveDt(Date ownDeactiveDt) {
		this.ownDeactiveDt = ownDeactiveDt;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getOwnFaCode() {
		return ownFaCode;
	}

	public void setOwnFaCode(String ownFaCode) {
		this.ownFaCode = ownFaCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public boolean isView() {
		return isView;
	}

	public void setView(boolean isView) {
		this.isView = isView;
	}

	public FAParticular getfAParticular() {
		return fAParticular;
	}

	public void setfAParticular(FAParticular fAParticular) {
		this.fAParticular = fAParticular;
	}

	public String getOwnAccntNo() {
		return ownAccntNo;
	}

	public void setOwnAccntNo(String ownAccntNo) {
		this.ownAccntNo = ownAccntNo;
	}

	public String getOwnIfsc() {
		return ownIfsc;
	}

	public void setOwnIfsc(String ownIfsc) {
		this.ownIfsc = ownIfsc;
	}

	public String getOwnMicr() {
		return ownMicr;
	}

	public void setOwnMicr(String ownMicr) {
		this.ownMicr = ownMicr;
	}

	public String getOwnBnkBranch() {
		return ownBnkBranch;
	}

	public void setOwnBnkBranch(String ownBnkBranch) {
		this.ownBnkBranch = ownBnkBranch;
	}

	public boolean isOwnBnkDet() {
		return isOwnBnkDet;
	}

	public void setOwnBnkDet(boolean isOwnBnkDet) {
		this.isOwnBnkDet = isOwnBnkDet;
	}

	public String getOwnPanNo() {
		return ownPanNo;
	}

	public void setOwnPanNo(String ownPanNo) {
		this.ownPanNo = ownPanNo;
	}

/*	public Blob getOwnPanImg() {
		return ownPanImg;
	}

	public void setOwnPanImg(Blob ownPanImg) {
		this.ownPanImg = ownPanImg;
	}

	public Blob getOwnDecImg() {
		return ownDecImg;
	}

	public void setOwnDecImg(Blob ownDecImg) {
		this.ownDecImg = ownDecImg;
	}*/

	public Integer getOwnImgId() {
		return ownImgId;
	}

	public void setOwnImgId(Integer ownImgId) {
		this.ownImgId = ownImgId;
	}

	public float getOwnPanIntRt() {
		return ownPanIntRt;
	}

	public void setOwnPanIntRt(float ownPanIntRt) {
		this.ownPanIntRt = ownPanIntRt;
	}

	public boolean isOwnIsPanImg() {
		return ownIsPanImg;
	}

	public void setOwnIsPanImg(boolean ownIsPanImg) {
		this.ownIsPanImg = ownIsPanImg;
	}

	public boolean isOwnIsDecImg() {
		return ownIsDecImg;
	}

	public void setOwnIsDecImg(boolean ownIsDecImg) {
		this.ownIsDecImg = ownIsDecImg;
	}

	public List<VehicleVendorMstr> getVehicleVendorMstrList() {
		return vehicleVendorMstrList;
	}

	public void setVehicleVendorMstrList(
			List<VehicleVendorMstr> vehicleVendorMstrList) {
		this.vehicleVendorMstrList = vehicleVendorMstrList;
	}

	public ArrayList<String> getOwnPhNoList() {
		return ownPhNoList;
	}

	public void setOwnPhNoList(ArrayList<String> ownPhNoList) {
		this.ownPhNoList = ownPhNoList;
	}

	public boolean isOwnValidPan() {
		return ownValidPan;
	}

	public void setOwnValidPan(boolean ownValidPan) {
		this.ownValidPan = ownValidPan;
	}

	public boolean isOwnInvalidPan() {
		return ownInvalidPan;
	}

	public void setOwnInvalidPan(boolean ownInvalidPan) {
		this.ownInvalidPan = ownInvalidPan;
	}

	public String getOwnFather() {
		return ownFather;
	}

	public void setOwnFather(String ownFather) {
		this.ownFather = ownFather;
	}

	public String getOwnAcntHldrName() {
		return ownAcntHldrName;
	}

	public void setOwnAcntHldrName(String ownAcntHldrName) {
		this.ownAcntHldrName = ownAcntHldrName;
	}

	public boolean isOwnIsChqImg() {
		return ownIsChqImg;
	}

	public void setOwnIsChqImg(boolean ownIsChqImg) {
		this.ownIsChqImg = ownIsChqImg;
	}

	public boolean isOwnBnkDetValid() {
		return ownBnkDetValid;
	}

	public void setOwnBnkDetValid(boolean ownBnkDetValid) {
		this.ownBnkDetValid = ownBnkDetValid;
	}

	public String getOwnBnkDetVerifyBy() {
		return ownBnkDetVerifyBy;
	}

	public void setOwnBnkDetVerifyBy(String ownBnkDetVerifyBy) {
		this.ownBnkDetVerifyBy = ownBnkDetVerifyBy;
	}

	public boolean isOwnBnkDetInValid() {
		return ownBnkDetInValid;
	}

	public void setOwnBnkDetInValid(boolean ownBnkDetInValid) {
		this.ownBnkDetInValid = ownBnkDetInValid;
	}

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}
	

}
