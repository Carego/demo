package com.mylogistics.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.mylogistics.model.Employee;
import com.mylogistics.model.FaTravDivision;

@Service
public class TravDivService {

	private Employee employee;
	
	private List<FaTravDivision> ftdList = new ArrayList<FaTravDivision>();
	
	private double totAmt = 0.0;

	public double getTotAmt() {
		return totAmt;
	}

	public void setTotAmt(double totAmt) {
		this.totAmt = totAmt;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public List<FaTravDivision> getFtdList() {
		return ftdList;
	}

	public void setFtdList(List<FaTravDivision> ftdList) {
		this.ftdList = ftdList;
	}
	
}
