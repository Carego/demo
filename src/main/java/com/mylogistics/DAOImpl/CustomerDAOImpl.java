
package com.mylogistics.DAOImpl;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang.NumberUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.CustomerDAO;
import com.mylogistics.constants.BillForwardingCNTS;
import com.mylogistics.constants.BranchCNTS;
import com.mylogistics.constants.CustomerCNTS;
import com.mylogistics.constants.CustomerRepresentativeCNTS;
import com.mylogistics.constants.FAMasterCNTS;
import com.mylogistics.model.BillForwarding;
import com.mylogistics.model.Branch;
import com.mylogistics.model.Customer;
import com.mylogistics.model.CustomerRepresentative;
import com.mylogistics.model.FAMaster;
import com.mylogistics.model.User;

public class CustomerDAOImpl implements CustomerDAO{ 

	private SessionFactory sessionFactory;
	private Session session;
	private org.hibernate.Transaction transaction;

	@Autowired
	private HttpSession httpSession;
	
	public static Logger logger = Logger.getLogger(CustomerDAOImpl.class);
	
	@Autowired
	public CustomerDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<Customer> getCustomer(String custCode){
		List<Customer> result = new ArrayList<Customer>();
		try{
			System.out.println("custCode is"+custCode);
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr=session.createCriteria(Customer.class);
			cr.add(Restrictions.eq(CustomerCNTS.CUST_CODE,custCode));
			result =cr.list();
			transaction.commit();
			session.flush();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return  result; 
	}


	@Transactional
	public long totalCount(){
		long tempCount = -1;
		try{
			session = this.sessionFactory.openSession();
			tempCount = (Long) session.createCriteria(Customer.class).setProjection(Projections.rowCount()).uniqueResult();
			session.flush();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return tempCount;
	}	



	@Transactional//done
	@SuppressWarnings("unchecked")
	public int getLastCustomer(){
		List<Customer> list = new ArrayList<Customer>();
		int id=0;

		try{
			session = this.sessionFactory.openSession();
			list = session.createQuery("from Customer order by custId DESC ").setMaxResults(1).list();
			id = list.get(0).getCustId();
			session.flush();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return id;
	}

	@Transactional
	@SuppressWarnings("unchecked")
	public List<String> getCustomerCode(){

		List<String> custCodeList = new ArrayList<String>();
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr=session.createCriteria(Customer.class);
			ProjectionList projList = Projections.projectionList();
			projList.add(Projections.property(CustomerCNTS.CUST_CODE));
			cr.setProjection(projList);
			custCodeList = cr.list();
			transaction.commit();
			session.flush();
		}
		catch(Exception e){
			e.printStackTrace();
		} 
		session.clear();
		session.close();
		return custCodeList;
	}

	@Transactional
	@SuppressWarnings("unchecked")
	public int saveCustomerToDB(Customer customer , Branch branch){ 
		System.out.println("enter into saveCustomerToDB fucntoin customer.name = "+customer.getCustName());
		List<Branch> brList = new ArrayList<Branch>();
		int temp;
		try{
			session = this.sessionFactory.openSession();//fixed
			transaction = session.beginTransaction();//fixed
				
			//get new branch
			Criteria cr=session.createCriteria(Branch.class);
			cr.add(Restrictions.eq(BranchCNTS.BRANCH_ID,branch.getBranchId()));
			brList = cr.list();
			Branch br = brList.get(0);
			
			//get new cust
			
			
			customer.getBranches().add(br);
			//customer.setfAParticular(null);
			br.getCustomers().add(customer);
			session.update(br);
			
			//temp = customer.getCustId();
			//temp = (Integer) session.save(customer);
			transaction.commit();//fixed
			System.out.println("after saving customer id = "+customer.getCustId());
			temp = customer.getCustId();
			System.out.println("user data inserted successfully");
			
			session.flush();
		}
		catch(Exception e){
			e.printStackTrace();
			temp= -1;
		}
		session.clear();
		session.close();
		return temp;
	}


	@Transactional
	@SuppressWarnings("unchecked")
	public Map<String,Object> getCustomerNCustomerRep(String crCode){

		List<CustomerRepresentative> custRepList = new ArrayList<CustomerRepresentative>();
		List<Customer> custList = new ArrayList<Customer>();
		CustomerRepresentative customerRepresentative = new CustomerRepresentative();
		Customer customer = new Customer();
		int temp;

		try{
			System.out.println("wanna retrieve the list of unverified contracts");
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			System.out.println("Hello There");
			Criteria cr=session.createCriteria(CustomerRepresentative.class);//fixed
			cr.add(Restrictions.eq(CustomerRepresentativeCNTS.CR_CODE,crCode));
			custRepList =cr.setMaxResults(1).list();
			customerRepresentative = custRepList.get(0);

			cr=session.createCriteria(Customer.class);//fixed
			cr.add(Restrictions.eq(CustomerCNTS.CUST_CODE, customerRepresentative.getCustCode()));
			custList = cr.setMaxResults(1).list();
			customer = custList.get(0);
			temp = 1;
			session.flush();
		}
		catch (Exception e) {
			e.printStackTrace();
			temp = -1;
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("customerRepresentative", customerRepresentative);
		map.put("customer", customer);
		map.put("temp", temp);
		session.clear();
		session.close();
		return map;
	}


	@Transactional
	public int updateCustomerByCustCode(Customer customer) {
		int temp;
		Date date = new Date(new java.util.Date().getTime());;
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		System.out.println("****enter into retrieveUser---->");
		String custCode = customer.getCustCode();
		try{
			session = this.sessionFactory.openSession();
			Criteria cr=session.createCriteria(Customer.class);
			cr.add(Restrictions.eq(CustomerCNTS.CUST_CODE,custCode));
			transaction = session.beginTransaction();
			customer.setCreationTS(calendar);
			session.saveOrUpdate(customer);
			transaction.commit();
			temp= 1;
			session.flush();
		}
		catch(Exception e){
			e.printStackTrace();
			temp = -1;
		}
		session.clear();
		session.close();
		return temp;
	}


	@Transactional
	@SuppressWarnings("unchecked")
	public List<Customer> getCustomerListForDailyContract(){
		List<Customer> customerList = new ArrayList<Customer>();
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			/*customerList = session.createCriteria(Customer.class).list();*/
			Criteria cr=session.createCriteria(Customer.class);
			cr.add(Restrictions.eq(CustomerCNTS.CUST_IS_DAILY_CONT_ALLOW,"yes"));
			customerList=cr.list();
			transaction.commit();
			session.flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return customerList;
	}


	@Transactional
	@SuppressWarnings("unchecked")
	public List<Customer> getCustomerisVerifyNo() {
		List<Customer> result = new ArrayList<Customer>();

		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr=session.createCriteria(Customer.class);
			cr.add(Restrictions.eq(CustomerCNTS.IS_VERIFY,"no"));
			result =cr.list();
			transaction.commit();
			session.flush();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return result;	 
	}


	@Transactional
	@SuppressWarnings("unchecked")
	public int updateCustomerisVerify(String[] code) {
		int temp;
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			for(int i=0;i<code.length;i++){
				Customer cust = new Customer();
				List<Customer> list = new ArrayList<Customer>();
				Criteria cr=session.createCriteria(Customer.class);
				cr.add(Restrictions.eq(CustomerCNTS.CUST_CODE,code[i]));
				list =cr.list();
				cust = list.get(0);
				cust.setView(true);
				session.update(cust);
				transaction.commit();
			}
			temp= 1;
			session.flush();
		}
		catch(Exception e){
			e.printStackTrace();
			temp= -1;
		}
		session.clear();
		session.close();
		return temp;
	}



	@Transactional
	@SuppressWarnings("unchecked")
	public List<Customer> getCustomerCodeNameContract(){
		List<Customer> custCodeList = new ArrayList<Customer>();
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			custCodeList = session.createCriteria(Customer.class).list();
			System.out.println("Customer List : "+custCodeList.size());
			transaction.commit();
			session.flush();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return custCodeList;
	}



	@Transactional
	@SuppressWarnings("unchecked")
	public List<Customer> getCustomerForCnmt(String branchCode){
		System.out.println("enter into getCustomerForCnmt function===>"+branchCode);
		List<Customer> custCodeList = new ArrayList<Customer>();
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr=session.createCriteria(Customer.class);
			//cr.add(Restrictions.eq(CustomerCNTS.BRANCH_CODE,branchCode));
			custCodeList = cr.list();
			transaction.commit();
			session.flush();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return custCodeList;
	}



	@Transactional
	@SuppressWarnings("unchecked")
	public List<Customer> getCustmerisViewFalse(String branchCode){

		System.out.println("The value of branch code is "+branchCode);
		List<Customer> result = new ArrayList<Customer>();

		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr=session.createCriteria(Customer.class);
			cr.add(Restrictions.eq(CustomerCNTS.IS_VIEW,false));
			cr.add(Restrictions.eq(CustomerCNTS.BRANCH_CODE,branchCode));
			result =cr.list();
			transaction.commit();
			session.flush();
			System.out.println("size of list is-->>"+result.size());
			for(int i = 0;i<result.size();i++){
				System.out.println("The values in the list is"+result.get(i).getCreationTS());
			}
			System.out.println("After getting the list");
		}
		catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return  result; 
	}

	@Transactional
	@SuppressWarnings("unchecked")
	public List<String> getAllCustCode(String branchCode){
		System.out.println("enter into getAllCustCode function");
		List<String> custCodeList = new ArrayList<String>();
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr=session.createCriteria(Customer.class);
			cr.add(Restrictions.eq(CustomerCNTS.BRANCH_CODE, branchCode));
			ProjectionList projList = Projections.projectionList();
			projList.add(Projections.property(CustomerCNTS.CUST_CODE));
			cr.setProjection(projList);
			custCodeList = cr.list();
			transaction.commit();
			session.flush();
		}
		catch(Exception e){
			e.printStackTrace();
		} 
		session.clear();
		session.close();
		return custCodeList;
	}

	@Transactional
	@SuppressWarnings("unchecked")
	public List<Customer> getCustomerByBranchCode(String branchCode){
		List<Customer> customerList = new ArrayList<Customer>();

		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr=session.createCriteria(Customer.class);
			cr.add(Restrictions.eq(CustomerCNTS.BRANCH_CODE, branchCode));
			customerList = cr.list();
			transaction.commit();
			session.flush();
		}
		catch(Exception e){
			e.printStackTrace();
		} 
		session.clear();
		session.close();
		return customerList;

	}
	
	@Transactional
	@SuppressWarnings("unchecked")
	public List<Customer> getCustomerFV(){
		List<Customer> customerList = new ArrayList<Customer>();

		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr=session.createCriteria(Customer.class);
			customerList = cr.list();
			transaction.commit();
			session.flush();
		}
		catch(Exception e){
			e.printStackTrace();
		} 
		session.clear();
		session.close();
		return customerList;  

	}
	


	@Transactional
	public int updateCustomer(Customer customer){
		System.out.println("entre into updateCustomer function");
		int temp;
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			session.saveOrUpdate(customer);
			transaction.commit();
			session.flush();
			temp = 1;
		}
		catch(Exception e){
			temp = -1;
			e.printStackTrace();
		} 
		session.clear();
		session.close();
		return temp;
	}
	
	 @Transactional
	 public int updateCustomerById(Customer customer){
		 System.out.println("Inside update functon with id-----"+customer.getCustId());
		 System.out.println("Inside update functon with facode-----"+customer.getCustFaCode());
 		int temp;
 		Date date = new Date(new java.util.Date().getTime());;
 		Calendar calendar = Calendar.getInstance();
 		calendar.setTime(date);
 		List<Customer> custList = new ArrayList<Customer>();
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 Criteria cr=session.createCriteria(Customer.class);
			 cr.add(Restrictions.eq(CustomerCNTS.CUST_ID, customer.getCustId()));
			 custList = cr.list();
			 Customer actCust = custList.get(0);
			 actCust.setCustFaCode(customer.getCustFaCode());
			 actCust.setCreationTS(calendar);
			 System.out.println("%%%%%%%%%%%%%%%%%%%%"+actCust.getCustFaCode());
			 session.update(actCust);
			 transaction.commit();
			 session.flush();
			 temp= 1;
		 }
		 catch(Exception e){
			 e.printStackTrace();
			 temp= -1;
		 }		
		 session.clear();
		 session.close();
		 return temp;
	 }
	 
	 
	 @Transactional
	 public List<Customer> getAllCustomer(){
		 System.out.println("enter into getAllCustomer function");
		 List<Customer> list = new ArrayList<Customer>();
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 Criteria cr=session.createCriteria(Customer.class);
			 list = cr.list();
			 transaction.commit();
			 session.flush();
		 }
		 catch(Exception e){
			 e.printStackTrace();
		 }		
		 session.clear(); 
		 session.close();
		 return list;
	 }
	 
	 
	 @Transactional
	 public List<Branch> getAllCustBranch(int custId){
		 System.out.println("enter into getAllCustBranch function");
		 List<Branch> brList = new ArrayList<Branch>();
		 List<Customer> custList = new ArrayList<Customer>();
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 Criteria cr=session.createCriteria(Customer.class);
			 cr.add(Restrictions.eq(CustomerCNTS.CUST_ID, custId));
			 custList = cr.list();
			 if(!custList.isEmpty()){
					Hibernate.initialize(custList.get(0).getBranches());
					Set<Branch> brSet = new HashSet<Branch>();
					brSet = custList.get(0).getBranches();
					for (Iterator<Branch> it = brSet.iterator(); it.hasNext(); ) {
						 Branch actBr = it.next();
						 brList.add(actBr);
					}	 
			}
			 transaction.commit();
			 session.flush();
		 }catch(Exception e){
			 e.printStackTrace();
		 }
		 session.clear();
		 session.close();
		 return brList;
	 }
	 
	 @Transactional
	 @SuppressWarnings("unchecked")
	 public List<String> getAllCustBrCode(String custFaCode){
		 System.out.println("enter into getAllCustBrCode function");
		 List<String> custBrList = new ArrayList<String>(); 
		 List<Customer> custList = new ArrayList<Customer>();
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 Criteria cr=session.createCriteria(Customer.class);
			 cr.add(Restrictions.eq(CustomerCNTS.CUST_FA_CODE,custFaCode));
			 custList = cr.list();
			 if(!custList.isEmpty()){
					Hibernate.initialize(custList.get(0).getBranches());
					Set<Branch> brSet = new HashSet<Branch>();
					brSet = custList.get(0).getBranches();
					for (Iterator<Branch> it = brSet.iterator(); it.hasNext(); ) {
						 Branch actBr = it.next();
						 custBrList.add(String.valueOf(actBr.getBranchFaCode()));
					}	 
			}
			 transaction.commit();
			 session.flush(); 
		 }catch(Exception e){
			 e.printStackTrace();
		 }
		 session.clear();
		 session.close();
		 return custBrList;
	 }
	 
	 
	 @SuppressWarnings("unchecked")
	 public List<Map<String,Object>> getCustNCI(){
		 System.out.println("enter into getCustNCI function");
		 List<Map<String,Object>> custList = new ArrayList<>();
		 try{
			 session = this.sessionFactory.openSession();
			 Query query = session.createQuery("select new map(custId as custId, custName as custName, custFaCode as custFaCode, custIsDailyContAllow as custIsDailyContAllow, custSrvTaxBy as custSrvTaxBy,custEditBillAmt as custEditBillAmt) from Customer");
			 custList = query.list();	 
		 }catch(Exception e){
			 e.printStackTrace();
		 }
		 session.clear();
		 session.close();
		 return custList;
	 }
	 
	 @Override
	 @SuppressWarnings("unchecked")
	 public List<Map<String,Object>> getGstCust(){
		 System.out.println("enter into getGstCust function");
		 List<Map<String,Object>> custList = new ArrayList<>();
		 try{
			 session = this.sessionFactory.openSession();
			 Query query = session.createQuery("select new map(custId as custId, custName as custName, custFaCode as custFaCode, custIsDailyContAllow as custIsDailyContAllow, custSrvTaxBy as custSrvTaxBy,custEditBillAmt as custEditBillAmt ,custGstNo as custGstNo) from Customer where custGstNo is not null and custGstNo!=''");
			 custList = query.list();	 
		 }catch(Exception e){
			 e.printStackTrace();
			 logger.info("Excption in getGstCust()"+e);
		 }finally{
			 session.clear();
			 session.close();
		 }
		
		 return custList;
	 }
	 
	 
	 @Override
	 @SuppressWarnings("unchecked")
	 public List<Map<String,Object>> getRelGstCust(){
		 System.out.println("enter into getGstCust function");
		 List<Map<String,Object>> custList = new ArrayList<>();
		 try{
			 session = this.sessionFactory.openSession();
			 Query query = session.createQuery("select new map(custId as custId, custName as custName, custFaCode as custFaCode, custIsDailyContAllow as custIsDailyContAllow, custSrvTaxBy as custSrvTaxBy,custEditBillAmt as custEditBillAmt ,custGstNo as custGstNo) from Customer where (custName = 'RELIANCE JIO INFOCOMM LTD' and custGstNo is not null and custGstNo!='') or(custCode in('1754','1755','1756'))");
			 custList = query.list();	 
		 }catch(Exception e){
			 e.printStackTrace();
			 logger.info("Excption in getRelGstCust()"+e);
		 }finally{
			 session.clear();
			 session.close();
		 }
		
		 return custList;
	 }

	 
	 
	 @Transactional
	 @SuppressWarnings("unchecked")
	 public List<Customer> getCustFrBillSbmsn(){
		 System.out.println("enter into getCustFrBillSbmsn function");
		 List<Customer> custList = new ArrayList<>();
		 User currentUser = (User) httpSession.getAttribute("currentUser");
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 
			 List<BillForwarding> bfList = new ArrayList<>();
			 Criteria cr = session.createCriteria(BillForwarding.class);
			 cr.add(Restrictions.eq(BillForwardingCNTS.BF_BRH_CODE,currentUser.getUserBranchCode()));
			 cr.add(Restrictions.isNull(BillForwardingCNTS.BF_REC_DT));
			 bfList = cr.list();
			 
			 if(!bfList.isEmpty()){
				 for(int i=0;i<bfList.size();i++){
					 Customer customer = (Customer) session.get(Customer.class,bfList.get(i).getBfCustId());
					 custList.add(customer);
				 }
			 }
			 session.flush();
			 
		 }catch(Exception e){
			 e.printStackTrace();
		 }
		 session.clear();
		 session.close();
		 return custList;
	 }
	 
	 
	 @Transactional
	 @SuppressWarnings("unchecked")
	 public Customer getCustById(int id){
		 System.out.println("enter into getCustById function");
		 Customer customer = null;
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 customer = (Customer) session.get(Customer.class,id);
			 session.flush();
		 }catch(Exception e){
			 e.printStackTrace();
		 }
		 session.clear();
		 session.close();
		 return customer;
	 }
	 
	 
	 @Transactional
	 @SuppressWarnings("unchecked")
	 public int updateBillPer(Customer customer){
		 System.out.println("enter into updateBillPer function = "+customer.getCustId());
		 int res =0;
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 Customer actCust = (Customer) session.get(Customer.class,customer.getCustId());
			 if(actCust != null){
				 actCust.setCustBillFrDt(customer.getCustBillFrDt());
				 actCust.setCustBillToDt(customer.getCustBillToDt());
				 actCust.setCustBillPer(customer.isCustBillPer());
				 
				 session.merge(actCust);
				 transaction.commit();
				 res = 1;
			 }
			 
			 session.flush();
		 }catch(Exception e){
			 e.printStackTrace();
		 }
		 session.clear();
		 session.close();
		 return res;
	 }
	 
	 
	 
	 @Transactional
	 @SuppressWarnings("unchecked")
	 public int checkBillPer(int custId , Date billDt){
		 System.out.println("enter into checkBillPer function = "+custId+"date="+billDt);
		 int res = 0;
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 Customer customer = (Customer) session.get(Customer.class,custId);
			 
			 System.out.println("Bill Dt : "+billDt);
			 System.out.println("Cust Bill Fr Dt : "+customer.getCustBillFrDt());
			 System.out.println("Cust Bill To Dt : "+customer.getCustBillToDt());
			 			 
			 
			 System.out.println("customer="+customer);
			 if(customer != null){
				 if( customer.getCustBillFrDt() == null ||  customer.getCustBillToDt() == null){
					 System.out.println("Inside 1");
					 res = -1;
				 }else{
					 int result1 = customer.getCustBillFrDt().compareTo(billDt);
					 int result2 = customer.getCustBillToDt().compareTo(billDt);
					 System.out.println("Inside 2");
					 System.out.println("Result 1 : "+result1);
					 System.out.println("Result 2 : "+result2);
					 if(result1 <= 0 && result2 >= 0){
						 res = 1;
					 }else{
						 res = 0;
					 }
				 }
			 }
			 session.flush();
		 }catch(Exception e){
			 e.printStackTrace();
		 }
		 System.out.println("RES : "+res);
		 session.clear();
		 session.close();
		 return res;
	 }

	@Override
	@Transactional
	public List<Map<String, Object>> getCustListByBranch(int branchId) {
		
		System.out.println("CustomerDAOImpl.getCustListByBranch()");
		List<Map<String, Object>> custList = new ArrayList<>();
		
		try {
			session = this.sessionFactory.openSession();
			Query query = session.createQuery("Select new map(custId as custId, custName as custName, custFaCode as custFaCode)" +
					"FROM Customer " +
					"WHERE branchCode = :branchCode)");
			query.setString("branchCode", String.valueOf(branchId));
			custList = query.list();
			session.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		session.clear();
		session.close();
		return custList;
	}
	
	@Override
	public List<Customer> getCustomerByName_FaCode(Session session, User user, String nameFaCode, List<String> proList){
		logger.info("UserID = "+user.getUserId()+" : Enter into getCustomerByName_FaCode() : NamFaCode = "+nameFaCode);
		List<Customer> custList = null;
		try{
			Criteria cr = session.createCriteria(Customer.class);
			if(proList != null){
				ProjectionList projectionList = Projections.projectionList();
				for(int i=0; i<proList.size(); i++)
					projectionList.add(Projections.property(proList.get(i)), proList.get(i));
				cr.setProjection(projectionList);
				cr.setResultTransformer(Transformers.aliasToBean(Customer.class));
			}
			if(NumberUtils.isNumber(nameFaCode))
				cr.add(Restrictions.like(CustomerCNTS.CUST_FA_CODE, "%"+nameFaCode));
			else
				cr.add(Restrictions.like(CustomerCNTS.CUST_NAME, nameFaCode+"%"));
			custList = cr.list();
			logger.info("UserID = "+user.getUserId()+" : CustomerList Size = "+custList.size());
		}catch(Exception e){
			logger.error("UserID = "+user.getUserId()+" : Exception = "+e);
		}
		logger.info("UserID = "+user.getUserId()+" : Exit from getCustomerByName_FaCode() : NamFaCode = "+nameFaCode);
		return custList;
	}
	
	@Override
	public List<Map<String, Object>> getCustomerByGroup(Session session, User user, String groupId){
		logger.info("UserID = "+user.getUserId() + " : Enter inot getCustomerByGroup() : GroupID = "+groupId);
		List<Map<String, Object>> custList = null;
		try{
			custList = session.createCriteria(Customer.class)
					.add(Restrictions.eq(CustomerCNTS.CUST_GROUP_ID, groupId))
					.setProjection(Projections.projectionList()
								.add(Projections.property(CustomerCNTS.CUST_NAME).as(CustomerCNTS.CUST_NAME))
								.add(Projections.property(CustomerCNTS.CUST_FA_CODE).as(CustomerCNTS.CUST_FA_CODE))
								.add(Projections.property(CustomerCNTS.CUST_CODE).as(CustomerCNTS.CUST_CODE))
							)
					.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP)
					.list();			
		}catch(Exception e){
			logger.error("UserID = "+user.getUserId() + " : Exception = "+e);
		}
		logger.info("UserID = "+user.getUserId() + " : Enter inot getCustomerByGroup() : GroupSize = "+custList.size());
		return custList;
	}
	
	@Override
	public int saveUpdateCustGstNo(Session session, User user, Map<String,String> custData){
		logger.info(" enter into saveUpdateCustGstNo() UserID = "+user.getUserId());
		int result=1;
		
		String faCode=(String) custData.get("custFaCode");
		String gstNo = (String) custData.get("custGstNo");
		String stateCode=gstNo.substring(0, 2);
		System.out.println("Facode= "+faCode+"  GstNo= "+gstNo+"  StateCode = "+stateCode);
		
		
		Criteria criteria= session.createCriteria(Customer.class)
				.add(Restrictions.eq(CustomerCNTS.CUST_FA_CODE, faCode));
				
				Customer customer=(Customer) criteria.list().get(0);
				customer.setCustGstNo(gstNo);
				customer.setStateGST(stateCode);
				customer.setCustSAC(custData.get("custSAC"));
		
			
			session.update(customer);
		
		return result;
	}
	
	public Customer getCustomer(int customerId) {
		return (Customer) this.sessionFactory.getCurrentSession().get(Customer.class, customerId);
	}
	
	
	@Override
	public List<Customer> getRelConsignee(Session session,User user,List<String> proList){
		logger.info("UserID = "+user.getUserId()+" : Enter into getRelConsignee()");
		List<Customer> custList = null;
		List<String> list=new ArrayList<>();
		list.add("1754");
		list.add("1755");
		list.add("1756");
		try{
			Criteria cr = session.createCriteria(Customer.class);
				ProjectionList projectionList = Projections.projectionList();
				for(int i=0; i<proList.size(); i++)
					projectionList.add(Projections.property(proList.get(i)), proList.get(i));
					
				cr.setProjection(projectionList);
				cr.setResultTransformer(Transformers.aliasToBean(Customer.class));
				cr.add(Restrictions.in(CustomerCNTS.CUST_CODE, list));
			custList = cr.list();
			logger.info("UserID = "+user.getUserId()+" : CustomerList Size = "+custList.size());
		}catch(Exception e){
		//	logger.error("UserID = "+user.getUserId()+" : Exception = "+e);
		}
//		logger.info("UserID = "+user.getUserId()+" : Exit from getCustomerByName_FaCode() : NamFaCode = "+nameFaCode);
		return custList;
	}
	
	
	@Override
	public int getCustCode(String custName, String bcode) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.openSession();
		Object custCode = session.createCriteria(Customer.class)
				.add(Restrictions.eq(CustomerCNTS.CUST_NAME, custName))
				.add(Restrictions.eq(CustomerCNTS.BRANCH_CODE, bcode))
				.setProjection(Projections.property(CustomerCNTS.CUST_CODE))
				.list().get(0);
		int cid = 0;
		if (custCode != null) {
			cid = Integer.parseInt(custCode.toString());
		}
		session.close();
		return cid;
	}	
	
	
	@Override
	public List<Customer> getCutomerByCustCodeList(List<String> custCodeList){
		
		List<Customer> customerList = new ArrayList<Customer>();
		Session session=null;
		try{
			session = this.sessionFactory.openSession();
			Criteria cr=session.createCriteria(Customer.class)
					.add(Restrictions.in(CustomerCNTS.CUST_CODE, custCodeList));
			customerList = cr.list();
			session.flush();
		}
		catch(Exception e){
			e.printStackTrace();
		} 
		session.clear();
		session.close();
		return customerList;
		
	}
	
	@Override
	public String getCustNameByCustCode(String custCode){

		String custName=null;
		Session session=null;
		try{
			session = this.sessionFactory.openSession();
			Criteria cr=session.createCriteria(Customer.class)
					.add(Restrictions.eq(CustomerCNTS.CUST_CODE, custCode));
			ProjectionList projectionList=Projections.projectionList();
			projectionList.add(Projections.property(CustomerCNTS.CUST_NAME),CustomerCNTS.CUST_NAME);
			
			cr.setProjection(projectionList);
			
			custName=(String) cr.list().get(0);
		
		}
		catch(Exception e){
			e.printStackTrace();
		} 
		session.clear();
		session.close();
		return custName;
		
	}
	
	
	
}