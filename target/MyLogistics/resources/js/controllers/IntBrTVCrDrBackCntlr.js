'use strict';

var app = angular.module('application');

app.controller('IntBrTVCrDrBackCntlr',['$scope','$location','$http','$filter','$window',
                                  function($scope,$location,$http,$filter,$window){
	
	$scope.vs = {};
	$scope.tib = {};
	
	$scope.tibList = [];
	
	$scope.tibAmtSum=0;
	$scope.sVouchDBFlag = true;
	$scope.brListDBFlag = true;
	$scope.saveVsFlag = true;
	
	$scope.getVoucherDetails = function(){
		console.log("Enter into getDetails");
		$scope.saveVsFlag = true;
		var response = $http.post($scope.projectName+'/getVDetFrIBTVBack');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.vs.branch = data.branch;
				$scope.bankCodeList = data.bankCodeList;
				$scope.vs.cashStmtStatus = data.cashStmtStatus;
				$scope.vs.voucherType = $scope.IBTV_VOUCHER;
				$scope.dateTemp = $filter('date')(data.cashStmtStatus.cssDt, "yyyy-MM-dd'");
				
				$scope.getBranchList();
			}else{
				console.log("Error in bringing data");
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
	
	$scope.getBranchList = function(){
		console.log("enter into getBranchList funciton");
		var response = $http.post($scope.projectName+'/getBrListFrIBTV');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.branchList = data.list; 
			}else{
				console.log("Error in bringing branch List");
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
	
	$scope.singleVoucher = function(){
		console.log("enter into singleVoucher funciton");
		$scope.sVouchDBFlag = false;
		$('div#sVouchDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Select FACode",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.sVouchDBFlag = true;
		    }
			});
		
		$('div#sVouchDB').dialog('open');	
	}
	
	
	
	$scope.openFaCodeBD = function(){
		console.log("enter into openFaCodeBD function");
		$scope.brListDBFlag = false;
		$('div#brListDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Select Branch",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.brListDBFlag = true;
		    }
			});
		
		$('div#brListDB').dialog('open');	
	}
	
	
	$scope.saveBrCode = function(branch){
		console.log("enter into saveBrCode function");
		$scope.tib.tibFaCode = branch.branchFaCode;
		$('div#brListDB').dialog('close');	
	}
	
	
	$scope.submitSVouch = function(sVouchForm){
		console.log("enter into sVouchForm function = "+sVouchForm.$invalid);
		if(sVouchForm.$invalid){
			$scope.alertToast("please fill correct form");
		}else{
			$scope.tibAmtSum=$scope.tibAmtSum+$scope.tib.tibAmt;
			$scope.tibList.push($scope.tib);
			$scope.tib = {};
			$scope.tib.tibDOrC = 'C';
			$('div#sVouchDB').dialog('close');	
		}
	}
	
	
	$scope.removeTIB = function(index,tibTv){
		console.log("enter into removeTIB function");
		$scope.tibAmtSum=$scope.tibAmtSum-tibTv.tibAmt;
		$scope.tibList.splice(index,1);
	}
	
	
	
	$scope.voucherSubmit = function(newVoucherForm){
		console.log("enter into newVoucherForm function = "+newVoucherForm.$invalid);
		if(newVoucherForm.$invalid){
			console.log("please fill the correct form");
		}else{
		
			 $scope.saveVsFlag = false;
		    	$('div#saveVsDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					show: UDShow,
					title: "Voucher Details",
					hide: UDHide,
					position: UDPos,
					draggable: true,
					close: function(event, ui) { 
				        $(this).dialog('destroy');
				        $(this).hide();
				        $scope.saveVsFlag = true;
				    }
					});
			$('div#saveVsDB').dialog('open');
		}
	}
	
	$scope.back = function(){
		console.log("enter into back function");
		$('div#saveVsDB').dialog('close');
	}
	
	$scope.saveVS = function(){
		console.log("enter into saveVS function");
		$('div#saveVsDB').dialog('close');
		var ibtService = {
				"voucherService" : $scope.vs,
				"tibList"        : $scope.tibList
		};
		$('#saveId').attr("disabled","disabled");
		var response = $http.post($scope.projectName+'/submitIBTVouch',ibtService);
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$('#saveId').removeAttr("disabled");
				$scope.alertToast("successfully save InterBrTv C/D voucher");
				$scope.vs = {};
				$scope.tib = {};
				$scope.tib.tibDOrC = 'C';
				$scope.tibList = [];
				$scope.getVoucherDetails();
			}else{
				console.log("Error in saving voucher");
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	} 
	
	
	
	
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		 $scope.getVoucherDetails();
	}else if($scope.logoutStatus === true){
		 $location.path("/");
	}else{
		 console.log("****************");
	}
}]);