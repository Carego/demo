package com.mylogistics.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="campaignresults")
public class CampaignResults {
	
	@Id
	@Column(name="campainId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int campainId;
	
	@Column(name="indentId")
	private int indentId;
	
	@Column(name="Master_Id")
	private int Master_Id;
	
	@Column(name="noOfVehAvail")
	private int noOfVehAvail;
	
	@Column(name="rates")
	private int rates;
	
	@Column(name="agentRemark")
	private String agentRemark;

	public int getCampainId() {
		return campainId;
	}

	public void setCampainId(int campainId) {
		this.campainId = campainId;
	}

	public int getIndentId() {
		return indentId;
	}

	public void setIndentId(int indentId) {
		this.indentId = indentId;
	}

	public int getMaster_Id() {
		return Master_Id;
	}

	public void setMaster_Id(int master_Id) {
		Master_Id = master_Id;
	}

	public int getNoOfVehAvail() {
		return noOfVehAvail;
	}

	public void setNoOfVehAvail(int noOfVehAvail) {
		this.noOfVehAvail = noOfVehAvail;
	}

	public int getRates() {
		return rates;
	}

	public void setRates(int rates) {
		this.rates = rates;
	}

	public String getAgentRemark() {
		return agentRemark;
	}

	public void setAgentRemark(String agentRemark) {
		this.agentRemark = agentRemark;
	}
	
	
	
	

}
