package com.mylogistics.DAO;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mylogistics.model.OwnerMaster;

@Repository
public interface IndentOwnerMasterDAO {
	
	public List<OwnerMaster> getIndentOwnerMaster(String branchName, String toState, String toStateZone, String vehicleType);
	public List<OwnerMaster> getIndentOwnerMasterByIdList(List<Integer> ownerMasterIdList);

}
