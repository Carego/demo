package com.mylogistics.model;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;


@Entity
@Table(name = "fabprom")
public class FaBProm {

	
	@Id
	@Column(name="fbpId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int fbpId;
	
	@Column(name="fbpCsId" , columnDefinition = "int default -1")
	private int fbpCsId;
	
	@Column(name="fbpAmt")
	private double fbpAmt;
	
	@Column(name="fbpArticle")
	private String fbpArticle;
	
	@Column(name="fbpPaidName")
	private String fbpPaidName;
	
	@Column(name="fbpPaidDesig")
	private String fbpPaidDesig;
	
	@Column(name="fbpPurpose")
	private String fbpPurpose;
	
	@Column(name="fbpTurnOver")
	private double fbpTurnOver;
	
	@Column(name="fbpExpTurn")
	private double fbpExpTurn;
	
	@Column(name="fbpBrfaCode")
	private String fbpBrfaCode;
	
	
	@Column(name="fbpDesc" , columnDefinition="TEXT")
	private String fbpDesc;
	
	//Many To One
	@ManyToOne
	@LazyCollection(LazyCollectionOption.FALSE)
	@JoinColumn(name = "custId") 
	private Customer customer;
	
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;

	public int getFbpId() {
		return fbpId;
	}

	public void setFbpId(int fbpId) {
		this.fbpId = fbpId;
	}

	public int getFbpCsId() {
		return fbpCsId;
	}

	public void setFbpCsId(int fbpCsId) {
		this.fbpCsId = fbpCsId;
	}

	public double getFbpAmt() {
		return fbpAmt;
	}

	public void setFbpAmt(double fbpAmt) {
		this.fbpAmt = fbpAmt;
	}

	public String getFbpArticle() {
		return fbpArticle;
	}

	public void setFbpArticle(String fbpArticle) {
		this.fbpArticle = fbpArticle;
	}

	public String getFbpPaidName() {
		return fbpPaidName;
	}

	public void setFbpPaidName(String fbpPaidName) {
		this.fbpPaidName = fbpPaidName;
	}

	public String getFbpPaidDesig() {
		return fbpPaidDesig;
	}

	public void setFbpPaidDesig(String fbpPaidDesig) {
		this.fbpPaidDesig = fbpPaidDesig;
	}

	public String getFbpPurpose() {
		return fbpPurpose;
	}

	public void setFbpPurpose(String fbpPurpose) {
		this.fbpPurpose = fbpPurpose;
	}

	public double getFbpTurnOver() {
		return fbpTurnOver;
	}

	public void setFbpTurnOver(double fbpTurnOver) {
		this.fbpTurnOver = fbpTurnOver;
	}

	public double getFbpExpTurn() {
		return fbpExpTurn;
	}

	public void setFbpExpTurn(double fbpExpTurn) {
		this.fbpExpTurn = fbpExpTurn;
	}

	public String getFbpDesc() {
		return fbpDesc;
	}

	public void setFbpDesc(String fbpDesc) {
		this.fbpDesc = fbpDesc;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public String getFbpBrfaCode() {
		return fbpBrfaCode;
	}

	public void setFbpBrfaCode(String fbpBrfaCode) {
		this.fbpBrfaCode = fbpBrfaCode;
	}
	
}
