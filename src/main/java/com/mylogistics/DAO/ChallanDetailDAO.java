package com.mylogistics.DAO;

import java.util.List;

import org.hibernate.Session;

import com.mylogistics.model.ChallanDetail;

public interface ChallanDetailDAO {
	
	//Shikha: saves challan detail to database
	 public int saveChlnDetailToDB(ChallanDetail cd);
	 
	 public int updateChallanDetail(ChallanDetail challanDetail);
	 
	 public List<ChallanDetail> getChlnDet();
	 
	 public String getLryNoByChCode(String chlnCode);
	 
	 public ChallanDetail getChdbyChlnCode(String chlnCode);

	public int saveChlnDetailToDB(ChallanDetail cd, Session session);
	 

}
