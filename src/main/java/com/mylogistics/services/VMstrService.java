package com.mylogistics.services;

import com.mylogistics.model.Address;
import com.mylogistics.model.Branch;
import com.mylogistics.model.Employee;
import com.mylogistics.model.VehicleMstr;

public class VMstrService {

	private VehicleMstr vehicleMstr;
	
	private Branch branch;
	
	private Employee employee;
	
	private Address address;
	
	private Address ownAdd;

	public Address getOwnAdd() {
		return ownAdd;
	}

	public void setOwnAdd(Address ownAdd) {
		this.ownAdd = ownAdd;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public VehicleMstr getVehicleMstr() {
		return vehicleMstr;
	}

	public void setVehicleMstr(VehicleMstr vehicleMstr) {
		this.vehicleMstr = vehicleMstr;
	}

	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	
	
}
