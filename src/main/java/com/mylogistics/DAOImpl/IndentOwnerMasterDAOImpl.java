package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import com.mylogistics.DAO.IndentOwnerMasterDAO;
import com.mylogistics.constants.OwnerCNTS;
import com.mylogistics.model.OwnerMaster;
import com.mylogistics.model.Owner;

public class IndentOwnerMasterDAOImpl implements IndentOwnerMasterDAO{
	
	private SessionFactory sessionFactory;
	
	
	private Session session;
	private Transaction transaction;
	@Autowired
	public IndentOwnerMasterDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Override
	public List<OwnerMaster> getIndentOwnerMaster(String branchName,String toState,String toStateZone,String vehicleType){
		
		Session session = this.sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		
		String zone=null;
		if(toStateZone.equalsIgnoreCase("CENTRAL")) {
			zone="selected_States_of_Central";
		}else if(toStateZone.equalsIgnoreCase("EAST")) {
			zone="selected_States_of_East";
		}else if(toStateZone.equalsIgnoreCase("NORTH")) {
			zone="selected_States_of_North";
		}else if(toStateZone.equalsIgnoreCase("NORTH_EAST")) {
			zone="selected_States_of_NorthEast";
		}else if(toStateZone.equalsIgnoreCase("OTHER_NATION")) {
			zone="other_Nations";
		}else if(toStateZone.equalsIgnoreCase("SOUTH")) {
			zone="selected_States_of_South";
		}else if(toStateZone.equalsIgnoreCase("WEST")) {
			zone="selected_States_of_West";
		}
		List<OwnerMaster> ownerMastersList=new ArrayList<>();
		try {
			System.out.println("Start"+branchName+"State "+toState+"zone"+zone+"vt "+vehicleType);
			ownerMastersList=session.createCriteria(OwnerMaster.class)
				.add(Restrictions.eq("branch", branchName))
				.add(Restrictions.like(zone, "%"+toState+"%"))
				.add(Restrictions.disjunction()
				.add(Restrictions.like("type_of_Container", "%"+vehicleType+"%"))
				.add(Restrictions.like("type_of_Trailor_and_Platform", "%"+vehicleType+"%"))
				.add(Restrictions.like("type_of_Truck", "%"+vehicleType+"%"))).list();
			
			System.out.println("End"+ownerMastersList.size());
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		session.flush();
		session.clear();
		transaction.commit();
		session.close();
		System.out.println("ownerMastersList="+ownerMastersList.size());
		
		return ownerMastersList;
	}
	
	
	public List<OwnerMaster> getIndentOwnerMasterByIdList(List<Integer> ownerMasterIdList){
		
		Session session=this.sessionFactory.openSession();
		List<OwnerMaster> ownMstrList=session.createCriteria(OwnerMaster.class)
				.add(Restrictions.in("master_Id", ownerMasterIdList)).list();
		session.close();
		System.out.println(ownMstrList.size());
		return ownMstrList;
	}
	

}
