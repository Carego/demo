package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.SubVehicleMstrDAO;
import com.mylogistics.constants.FaCarDivisionCNTS;
import com.mylogistics.constants.SubTelephoneMstrCNTS;
import com.mylogistics.constants.SubVehicleMstrCNTS;
import com.mylogistics.model.CashStmt;
import com.mylogistics.model.FaCarDivision;
import com.mylogistics.model.SubTelephoneMstr;
import com.mylogistics.model.SubVehicleMstr;
import com.mylogistics.services.VM_SubVMService;

public class SubVehicleMstrDAOImpl implements SubVehicleMstrDAO{

	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	public SubVehicleMstrDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<VM_SubVMService> getSubFileDetails(List<CashStmt> reqCsList){
		System.out.println("enter into getSubFileDetails function");
		List<VM_SubVMService> vDetList = new ArrayList<VM_SubVMService>();
		if(!reqCsList.isEmpty()){
			try{
				session = this.sessionFactory.openSession();
				transaction = session.beginTransaction();
				for(int i=0;i<reqCsList.size();i++){
					List<SubVehicleMstr> tempList = new ArrayList<SubVehicleMstr>();
					Criteria cr = session.createCriteria(SubVehicleMstr.class);
					cr.add(Restrictions.eq(SubVehicleMstrCNTS.SVM_ID, reqCsList.get(i).getCsSFId()));
					tempList = cr.list(); 
					if(!tempList.isEmpty()){
						VM_SubVMService vm_SubVMService = new VM_SubVMService();
						vm_SubVMService.setsVehMstr(tempList.get(0));
						
						List<FaCarDivision> facList = new ArrayList<FaCarDivision>();
						Criteria cr1 = session.createCriteria(FaCarDivision.class);
						cr1.add(Restrictions.eq(FaCarDivisionCNTS.FCD_SVM_ID, tempList.get(0).getSvmId()));
						facList = cr1.list();
						
						vm_SubVMService.setFaCarList(facList);
						
						vDetList.add(vm_SubVMService);
					}
				} 
				transaction.commit();
				session.flush();
			}catch(Exception e){
				e.printStackTrace();
			}
			session.clear();
			session.close();
		}else{
			System.out.println("reqCsList is empty");
		}
		return vDetList;
	}

}
