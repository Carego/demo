<div ng-show="operatorLogin || superAdminLogin">

<title>Regular Contract</title>
 
 

			<!-- Angular.js UI design started -->
	
	 <div class="row">
		<form class="col s12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3);" name="RegularContractForm" ng-submit="submit(RegularContractForm,regCnt,regContRatePer,tnc,rbkm,pbdP,pbdD,pbdB,cts)">
			<div class="row">
      			<!-- <div class="input-field col s3">
        			<input type ="hidden"  name ="branchCode" ng-model="regCnt.branchCode" ng-click="OpenBranchCodeDB()" readonly>
        			<label>Branch Code</label>
      			</div> -->
      			<div class="input-field col s3">
      				<input type ="text" id="regContBLPMCodeTemp" name ="regContBLPMCodeTemp" ng-model="regContBLPMCodeTemp" ng-click="OpenregContBLPMCodeDB()" readonly required>
      				<label>BLPM Code</label>
      				<input type ="hidden" id="regContBLPMCode" name ="regContBLPMCode" ng-model="regCnt.regContBLPMCode">
      			</div>
      			<div class="input-field col s3">
        			<input type ="text" id="regContCngrCodeTemp" name ="regContCngrCodeTemp" ng-model="regContCngrCodeTemp" ng-click="OpenregContCngrCodeDB()" readonly>
        			<label>Consignor Code</label>
        			<input type ="hidden" id="regContCngrCode" name ="regContCngrCode" ng-model="regCnt.regContCngrCode">
      			</div>
      			<div class="input-field col s3">
        			<input type ="text" id="regContCrNameTemp" name ="regContCrNameTemp" ng-model="regCnt.regContCrNameTemp" ng-click="OpenregContCrNameDB()"readonly required disabled="disabled">
        			<label>CR Name</label>
        			<input type ="hidden" id="regContCrName" name ="regContCrName" ng-model="regCnt.regContCrName" readonly>
      			</div>
      			<!-- <div class="input-field col s3">
      				<select name="metricType" id="metricType" ng-model="metricType" required ng-blur="sendMetricType(metricType)">
						<option value="Ton">Ton</option>
						<option value="Kg">Kg</option>
					</select>
					<label>Metric Type</label>
				</div> -->
				<div class="input-field col s3">
	      				<select name="metricType" id="metricType" ng-model="metricType"  ng-init="metricType = 'Ton'" ng-blur="sendMetricType(metricType)" required>
							<option value="Ton">Ton</option>
							<option value="Kg">Kg</option>
						</select>
						<label>Metric Type</label>
				</div>
    		</div>
    		
    		
    		<div class="row">
    		
	    		<div class="input-field col s3">
	      			<input type="date" id="regContFromDt" class="datepicker" name ="regContFromDt" ng-model="regCnt.regContFromDt" ng-blur="setStartDate(regCnt.regContFromDt)" required>
	      			<label>From Date</label>
	      		</div>
	      		
	      		<div class="input-field col s3">
	      			<input type="date" id="regContToDt" class="datepicker" name ="regContToDt" ng-model="regCnt.regContToDt" ng-blur="setEndDate(regCnt.regContToDt)" required>
	      			<label>To Date</label>
	      		</div>
	    		
	    		<div class="col s3">
	      				<div class="input-field col s8">
	        				<input type="radio" id="regContProportionate" name ="regContProportionate" ng-model="regCnt.regContProportionate" value ="P" ng-click="clickP()">
							<label>Proportionate</label>
						</div>
						<div class="input-field col s4">
							<input type ="radio" id="regContProportionate1" name ="regContProportionate" ng-model="regCnt.regContProportionate" value="F" ng-click="clickF()">
	        				<label>Fixed</label>
	        			</div>
	      		</div>
	      		
	      		<div class="col s3">
	      	  		<div class=" input-field col s3"> 
	      				<input class="with-gap" type="radio" id="regContType" name ="regContType" ng-model="regCnt.regContType" value="W" ng-click="clickW(metricType,regCnt.regContProportionate)">
	      				<label for="regContType">W</label>
	      			</div>
	      			<div class="input-field col s3">
						<input class="with-gap" type="radio" id="regContType1" name ="regContType" ng-model="regCnt.regContType" value="Q" ng-click="clickQ(metricType,regCnt.regContProportionate)">
						<label for="regContType1">Q</label>
					</div>
	      			<div class="input-field col s3">
						<input class="with-gap" type="radio" id="regContType2" name ="regContType" ng-model="regCnt.regContType" value="K" ng-click="clickK(metricType,regCnt.regContProportionate)">
	      				<label for="regContType2">K</label>
	      			</div>
      			</div>
      		
      			 
          </div>
          
         
          
    	<div class="row">
    	
	      		<div class="input-field col s2">
	      			<input type ="text" id="regContFromStationTemp" name ="regContFromStationTemp" ng-model="regCnt.regContFromStationTemp" ng-click="OpenregContFromStationDB()"readonly required>
	      			<label>From Station</label>	
	      			<input type ="hidden" id="regContFromStation" name ="regContFromStation" ng-model="regCnt.regContFromStation" readonly>
	      		</div>
	      		<!-- <div class="input-field col s1">
	      			<input class="col s12 btn teal white-text" type ="button"  id="addStation" name ="addStation" ng-model="addStation" ng-click="openAddNewStnDB()" value="NEW">
	      		</div> -->
	      			
	      		<div class="input-field col s3" ng-show="onWQClick">
	        		<input type ="button" id="ToStations" name ="ToStations" ng-model="ToStations" ng-click="contToStnDB(regCnt)" value="ADD TO STATION">
	        		<label>To Station</label>
	      		</div>
	      		
	      		<div class="input-field col s3" ng-show="onKClick">
	      			<input type ="text" id="regContToStationTemp" name ="regContToStationTemp" ng-model="regCnt.regContToStationTemp" ng-click="OpenregContToStationDB()" readonly ng-blur="setToStnFlag(regCnt)">
	      			<label>To Station</label>
	      			<input type ="hidden" id="regContToStation" name ="regContToStation" ng-model="regCnt.regContToStation" readonly>
	      		</div>
	      		
	      		<div class="col s3">
	      				<input class="col s12 btn teal white-text" type ="button" value="Add RBKM" id="regContRbkmId" name ="regContRbkmId" ng-model="regCnt.regContRbkmId" ng-click="OpenregContRbkmIdDB(regCnt)" disabled="disabled">
	      		</div>
	      		
	      		<div class="input-field col s3">
	      		
	      		<select name="regTransExpBearerName" id="regTransExpBearerId" ng-model="regCnt.regTransExpBearer" ng-init="regCnt.regTransExpBearer='Vendor'" required>
								<option value="Vendor">Vendor</option>
								<option value="Customer">Customer</option>
							</select>
							<label>Transit Expense Bearer</label>
	      		</div>
	      		
      	</div>	
      		
      		
        <div class="row" ng-show="ctsFlagW" style="margin-bottom: 50px;">
			<div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
			<div class="col-lg-12 col-sm-12 col-xs-12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3); overflow:auto; " >		
		 		<table class="tblrow mrg">
		 			<caption class="coltag tblrow">Contract To Station Data</caption>
				    <thead>
				        <tr class="rowclr">
				            <th class="colclr">From Station</th>
				            <th class="colclr">To Station</th>
				            <th class="colclr">Vehicle Type</th>
				            <th class="colclr">From Weight</th>
				            <th class="colclr">To Weight</th>
				            <th class="colclr">Load</th>
				            <th class="colclr">UnLoad</th>
				            <th class="colclr">Transit Day</th>
				            <th class="colclr">ST Chg</th>
				            <th class="colclr">Rate</th>
				            <th class="colclr">Additional Rate</th>	
				            <th class="colclr">Action</th>		            
				        </tr>
				    </thead>
				    <tbody>
				        <tr class="tbl" ng-repeat="cts in ctsList">
							<td class="rowcel">{{regContFrmStn}}</td>
						    <td class="rowcel">{{contToStnTemp[$index].ToStnTemp}}</td>
						    <td class="rowcel">{{cts.ctsVehicleType}}</td>
						    <td class="rowcel">{{cts.ctsFromWt}}</td>
				            <td class="rowcel">{{cts.ctsToWt}}</td>
				            <td class="rowcel">{{cts.ctsToLoad}}</td>
				            <td class="rowcel">{{cts.ctsToUnLoad}}</td>
				            <td class="rowcel">{{cts.ctsToTransitDay}}</td>
				            <td class="rowcel">{{cts.ctsToStatChg}}</td>
				          	<td class="rowcel">{{cts.ctsRate}}</td>
				          	<td class="rowcel">{{cts.ctsAdditionalRate}}</td>
				            
				            <td class="rowcel"><input type ="button" ng-click="removeCTS(cts)" value="Remove"></td>
				        </tr>
				    </tbody>
				</table>
				<div class="center col s12">
					<input type ="button" ng-show="ctsList.length > 1" ng-click="removeAllCTS()" value="Remove All CTS">
				</div>
			</div>
		</div>
   		
   		<div class="row" ng-show="ctsFlagQ" style="margin-bottom: 50px;">
		<div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
		<div class="col-lg-12 col-sm-12 col-xs-12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3); overflow:auto;" >		
 		<table class="tblrow mrg">
 			<caption class="coltag tblrow">Contract To Station Data</caption>
		    <thead>
		        <tr class="rowclr">
		            <th class="colclr">From Station</th>
		            <th class="colclr">To Station</th>
		            <th class="colclr">Vehicle Type</th>
		            <th class="colclr">From Weight</th>
		            <th class="colclr">To Weight</th>
		            <th class="colclr">Product Type</th>
		            <th class="colclr">Load</th>
		            <th class="colclr">UnLoad</th>
		            <th class="colclr">Transit Day</th>
		            <th class="colclr">ST Chg</th>
		            <th class="colclr">Rate</th>		            
		        </tr>
		    </thead>
		    <tbody>
		        <tr class="tbl" ng-repeat="cts in ctsList">
					<td class="rowcel">{{regContFrmStn}}</td>
				    <td class="rowcel">{{contToStnTemp[$index].ToStnTemp}}</td>
				    <td class="rowcel">{{cts.ctsVehicleType}}</td>
				    <td class="rowcel">{{cts.ctsFromWt}}</td>
		            <td class="rowcel">{{cts.ctsToWt}}</td>
		            <td class="rowcel">{{cts.ctsProductType}}</td>
		            <td class="rowcel">{{cts.ctsToLoad}}</td>
		            <td class="rowcel">{{cts.ctsToUnLoad}}</td>
		            <td class="rowcel">{{cts.ctsToTransitDay}}</td>
		            <td class="rowcel">{{cts.ctsToStatChg}}</td>
		          	<td class="rowcel">{{cts.ctsRate}}</td>
		            
		            <td class="rowcel"><input type ="button" ng-click="removeCTS(cts)" value="Remove"></td>
		        </tr>
		    </tbody>
		</table>
		<div class="center col s12">
			<input type ="button" ng-show="ctsList.length > 1" ng-click="removeAllCTS()" value="Remove All CTS">
		</div>	
		</div>
		</div>
          
          <div class="row">
      		
      	</div>
      	<div class="row">
      		
          </div>
              
      	 
<div class="row"  ng-show="rbkmFlag" style="margin-bottom: 50px; margin-top: -100px;">
    <div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
	<div class="col-lg-12 col-sm-12 col-xs-12 card" style="align: center; margin-top: 40px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3); overflow:auto;" >
		<table class="tblrow mrg">
			<caption class="coltag tblrow">RBKM</caption>
		    <thead>
		        <tr class="rowclr">
		            
		            <th class="colclr">From Station</th>
		            <th class="colclr">To Station</th>
		            <th class="colclr">State Code</th>
		            <th class="colclr">From Km</th>
		            <th class="colclr">To Km</th>
		            <th class="colclr">Vehicle Type</th>
		            <th class="colclr">Load</th>
		            <th class="colclr">UnLoad</th>
		            <th class="colclr">Transit Day</th>
		            <th class="colclr">ST Chg</th>
		            <th class="colclr">Rate</th>
		            <th class="colclr">Action</th>   
		        </tr>
		    </thead>
		    <tbody>
		        <tr class="tbl" ng-repeat="rbkm in rbkmList">
		            
		             <td class="rowcel">{{rbkmStnList[$index].FromStn}}</td>
		            <td class="rowcel">{{rbkmStnList[$index].ToStn}}</td>
		            <td class="rowcel">{{rbkm.rbkmStateCode}}</td>
		            <td class="rowcel">{{rbkm.rbkmFromKm}}</td>
		            <td class="rowcel">{{rbkm.rbkmToKm}}</td>
		            <td class="rowcel">{{rbkm.rbkmVehicleType}}</td>
		            <td class="rowcel">{{rbkm.rbkmLoad}}</td>
		            <td class="rowcel">{{rbkm.rbkmUnLoad}}</td>
		            <td class="rowcel">{{rbkm.rbkmTransitDay}}</td>
		            <td class="rowcel">{{rbkm.rbkmStatChg}}</td>
		            <td class="rowcel">{{rbkm.rbkmRate}}</td>
		            <td class="rowcel"><input type="button" class="btn" ng-click="removeRbkm(rbkm)" value="Remove"></td>
		            <!-- <button class="btn" ng-click="removeRbkm(rbkm)">Remove</button> -->
		        </tr>
		    </tbody>
		</table>
		<div class="center col s12">
			<input type="button" ng-show="rbkmList.length > 1" ng-click="removeAllRbkm()" value="Remove All RBKM">
		</div>	
   </div>
</div>
          
          
          <div class="row">
      			<div class="input-field col s3">
      			<select name="regContInsuredBy" id="regContInsuredBy" ng-model="regCnt.regContInsuredBy">
						<option value="CUSTOMER">CUSTOMER</option>
						<option value="SECL">SECL</option>
						<option value="OTHER">OTHER</option>
					</select>
        			<!-- <input class="validate" type ="text" id="regContInsuredBy" name ="regContInsuredBy" ng-model="regCnt.regContInsuredBy"> -->
        			<label for="regContInsuredBy">Insured By</label>
      			</div>
      			<div class="input-field col s3">
      				<input class="validate" type ="text" id="regContInsureComp" name ="regContInsureComp" ng-model="regCnt.regContInsureComp">
      				<label for="regContInsureComp">Insured Company</label>
      			</div>	
      			<div class="input-field col s3">
      				<input class="validate" type ="text" id="regContInsureUnitNo" name ="regContInsureUnitNo" ng-model="regCnt.regContInsureUnitNo">
        			<label for="regContInsureUnitNo">Insured Unit Number</label>
        			
      			</div>
      			<div class="input-field col s3">
        			<input class="validate" type ="text" id="regContInsurePolicyNo" name ="regContInsurePolicyNo" ng-model="regCnt.regContInsurePolicyNo">
        			<label for="regContInsurePolicyNo">Insured Policy Number</label>
      			</div>
      			
      			
    		</div>
      		
      		<div class="row">
      			<div class="input-field col s3">
      				
        			<select name="regContDc" id="regContDc" ng-model="regCnt.regContDc" required>
					<option value="01">One bill</option>
					<option value="02">Two bill</option>
					<option value="10">Direct Payment through CNMT</option>
					<option value="20">Twice Payment</option>
					<option value="11">Partially through bill Partially through CNMT</option>
					</select>
					<label>DC</label>
      			</div>
      			<div class="input-field col s3">
					
      				<select name="regContCostGrade" id="regContCostGrade" ng-model="regCnt.regContCostGrade" required>
					<option value="A">Heavy Material</option>
					<option value="B">Natural Rubber</option>
					<option value="C">General Goods</option>
					<option value="D">Light Goods</option>
					</select>
					<label>Cost Grade</label>
      			</div>
      			<div class="input-field col s3">
        			
        			<select name="regContDdl" id="regContDdl" ng-model="regCnt.regContDdl" required>
					<option value="Yes">Yes</option>
					<option value="No">No</option>
					</select>
					<label>DDL</label>
      			</div>
      			
      			<div class="input-field col s3">
        			
        			<select name="regContBB" id="regContBBId" ng-model="regCnt.regContBillBasis" ng-init="regCnt.regContBillBasis = 'chargeWt' " required>
							<option value="chargeWt">Charge Weight</option>
							<option value="receiveWt">Receive Weight</option>
							<option value="actualWt">Actual Weight</option>
							<option value="fixed">Fixed</option>
					</select>
					<label>Bill Basis</label>
      			</div>
      			
      			
      			
      		</div>	
      		
      		<div class="row">
      			<div class="input-field col s3">
      				
      				<select name="regContRenew" id="regContRenew" ng-model="regCnt.regContRenew" required>
					<option value="F">Fresh</option>
					<option value="R">Routine</option>
					<option value="D">Diesel Hike</option>
					</select>
					<label>Renew</label>
      			</div>
      			<div class="input-field col s3">
      				<input type ="date" id="regContRenewDt" name ="regContRenewDt" ng-model="regCnt.regContRenewDt">
      				<label>Renew Date</label>
      			</div>
      			<div class="input-field col s3">
        			<input type ="number" id="regContWt" name ="regContWt" ng-model="regCnt.regContWt"  step="0.01" min="0.01" required>
        			<label>Weight(cont)</label>
      			</div>
      			
      			<div class="input-field col s3">
        			<input class="validate" type ="number" id="regContValue" name ="regContValue" ng-model="regCnt.regContValue"  step="0.01" min="0.01" required>
        			<label>Value</label>
      			</div>
      			
      		
      		</div>	
      		
      		<div class="row">
      			
      			
      			
    		</div>
    		<div class="row"  ng-show="pbdFlag" style="margin-bottom: 50px; margin-top: -100px;">
	<div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
	<div class="col-lg-12 col-sm-12 col-xs-12 card" style="align: center; margin-top: 40px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3); overflow:auto;" >
	<table class="tblrow mrg">
		<caption class="coltag tblrow">P B D</caption>
		    <thead>
		        <tr class="rowclr">
		            <th class="colclr">From Station</th>
		            <th class="colclr">To Station</th>
		            <th class="colclr">Start Date</th>
		            <th class="colclr">End Date</th>
		            <th class="colclr">From Day</th>
		            <th class="colclr">To Day</th>
		            <th class="colclr">Vehicle Type</th>
		            <th class="colclr">P/B/D</th>
		            <th class="colclr">Hour N Day</th>
		            <th class="colclr">Amount</th>
		            <th class="colclr">Action</th>
		        </tr>
		    </thead>
		    <tbody>
		        <tr class="tbl" ng-repeat="pbd in pbdList">
		            <td class="rowcel">{{pbdStnList[$index].FromStn}}</td>
		            <td class="rowcel">{{pbdStnList[$index].ToStn}}</td>
		            <td class="rowcel">{{pbd.pbdStartDt}}</td>
		            <td class="rowcel">{{pbd.pbdEndDt}}</td>
		            <td class="rowcel">{{pbd.pbdFromDay}}</td>
		            <td class="rowcel">{{pbd.pbdToDay}}</td>
		            <td class="rowcel">{{pbd.pbdVehicleType}}</td>
		            <td class="rowcel">{{pbd.pbdPenBonDet}}</td>
		            <td class="rowcel">{{pbd.pbdHourNDay}}</td>
		            <td class="rowcel">{{pbd.pbdAmt}}</td>
		            <td class="rowcel"><input type="button" class="btn" ng-click="removePbd(pbd)" value="Remove"></td>
		        </tr>
		    </tbody>
	</table>
		<div class="center col s12">
			<input type="button" ng-show="pbdList.length > 1" ng-click="removeAllPbd()" value="Remove All PBD">
		</div>	
          </div>
          </div>
    		
    		<div class="row">
      			<div class="input-field col s12">
      				<i class="mdi-editor-mode-edit prefix"></i>
        			<textarea id="textarea1" class="materialize-textarea"  rows="3" cols="92"  name ="regContRemark" ng-model="regCnt.regContRemark"></textarea>
        			<label>Remark</label>
      			</div>
      		</div>	
      		
    		<div class="row">
    		<div class="input-field col s12 center">
      				<input type="submit" id="submit"value="Submit">
      			</div>
    		
    		</div>

		</form>
	</div> 
	

 
 <div id="afterSaveDB" ng-hide="afterSaveDBFlag"> 
	<table>
		<tr>
			<td>Contract Code : </td>
			<td>{{ afterSaveContCode }} </td>
		</tr>
		<tr>
			<td>
				<input type="button" value="OK" ng-click="afterSaveClose()"/>
			</td>
		</tr>
	</table>
</div>
 
	<div id="regContRbkmIdDB" ng-hide = "RbkmIdFlag">	
	<form name="RbkmForm" ng-submit="saveRbkmId(RbkmForm,rbkm)"> 
	
		<table class="noborder" >
			<tr>
				<!-- <td>From Station *</td> -->
				<td><input type ="hidden" id="rbkmFromStationTemp" name ="rbkmFromStationTemp" ng-model="rbkm.rbkmFromStationTemp" ng-click="OpenrbkmFromStationDB()"readonly></td>
				<td><input type ="hidden" id="rbkmFromStation" name ="rbkmFromStation" ng-model="rbkm.rbkmFromStation" ></td>
			</tr>
			<tr>
				<td>Vehicle Type *</td>
				<!-- <td> -->
					<!-- <table>
						<tr> -->
							<td><input type ="text"  id="rbkmVehicleType" name ="rbkmVehicleType" ng-model="rbkm.rbkmVehicleType" ng-click="OpenRbkmVehicleTypeDB()" required readonly></td>
							<td><input type="button" ng-model="addVTForRbkm" ng-click="OpenAddVehicleTypeDB()" value="NEW"></td>
						<!-- </tr>
					</table> -->
				<!-- </td> -->
			</tr>
			
			<tr>
				<td>Product Type*</td>
				<td>
					<table>
						<tr>
							<td><input type ="text" id="rbkmProductType" name ="rbkmProductType" ng-model="rbkm.rbkmProductType" ng-click="openRbkmProductTypeDB()" readonly required></td>
							<td><a class="btn-floating suffix waves-effect teal" id="openProductTypeDB" ng-model="savProducttype" ng-click="OpenProductTypeDB1()" disabled="disabled"><i class="mdi-content-add white-text" style="font-size: 24px; margin-left: -5px;"></i></a></td>
						</tr>
					</table>
				</td>
			</tr>
			
			<tr>
				<td>From Kilometer *</td>
				<td colspan="2"><input type ="number"  id="rbkmFromKm" name ="rbkmFromKm" ng-model="rbkm.rbkmFromKm" min="1" ng-minlength="1" ng-maxlength="7" required></td>
			</tr>
			
			<tr>
				<td>To Kilometer *</td>
				<td colspan="2"><input type ="number"  id="rbkmToKm" name ="rbkmToKm" ng-model="rbkm.rbkmToKm" min="1" ng-minlength="1" ng-maxlength="7" required></td>
			</tr>
			
			<tr>
				<td>To Station </td>
				<td colspan="2"><input type ="text"  id="rbkmToStationTemp" name ="rbkmToStationTemp" ng-model="rbkm.rbkmToStationTemp" ng-click="OpenrbkmToStationDB()" readonly></td>
			<td><input type ="hidden"  id="rbkmToStation" name ="rbkmToStation" ng-model="rbkm.rbkmToStation" ></td>
			</tr>
			
			<tr>
				<td>State Code *</td>
				<td colspan="2"><input type ="text"  id="rbkmStateCodeTemp" name ="rbkmStateCodeTemp" ng-model="rbkm.rbkmStateCodeTemp" ng-click="OpenrbkmStateCodeDB()" readonly required></td>
				<td><input type ="hidden"  id="rbkmStateCode" name ="rbkmStateCode" ng-model="rbkm.rbkmStateCode"></td>
			</tr>
			
			<tr>
				<td>Rate(Per Km) *</td>
				<td colspan="2"><input type ="number"  id="rbkmRate" name ="rbkmRate" ng-model="rbkm.rbkmRate" step="0.01" min="0.01"  required></td>
			</tr>
			<tr>
				<td></td>
			</tr>
			<tr>
				<td> <div class="input-field col s3">
					<input class="prefix" type="checkbox" id ="checkLoadRbkm" ng-model="checkLoadRbkm" ng-change="saveTncLoadRbkm()">
					<input class="validate" type ="number" id="rbkmLoad" name ="rbkmLoad" ng-model="rbkm.rbkmLoad" ng-disabled="!checkLoadRbkm"  step="0.01" min="0.01"  ng-blur="setLoadFlagTrueRbkm(rbkm)">				
					<label>Load</label>
					<input type ="hidden" ng-model="tnc.tncLoading" id="tncLoading" ng-init="tnc.tncLoading='no'" >
      	   		 </div></td>
      	   
      	   		<td>	 
      			<div class="input-field col s3">
					<input class="prefix" type="checkbox" id ="checkUnLoadRbkm" ng-model="checkUnLoadRbkm" ng-change="saveTncUnloadRbkm()">
					<input class="validate" type ="number" id="rbkmtUnLoad" name ="rbkmUnLoad" ng-model="rbkm.rbkmUnLoad" ng-disabled="!checkUnLoadRbkm"  step="0.01" min="0.01"  ng-blur="setUnLoadFlagTrueRbkm(rbkm)">
					<label>UnLoad</label>
					<input type ="hidden" ng-model="tnc.tncUnLoading" id="tncUnLoading"  ng-init="tnc.tncUnLoading='no'">	
      			</div>
				</td>
			</tr>
			
			<tr>
				<td>
				<div class="input-field col s3">
					<input class="prefix" type="checkbox" id ="checkTransitDayRbkm" ng-model="checkTransitDayRbkm" ng-change="saveTncTransitDayRbkm()">
      				<input class="validate" type ="number" id="rbkmTransitDay" name ="rbkmTransitDay" ng-model="rbkm.rbkmTransitDay" ng-disabled="!checkTransitDayRbkm"  min="1" ng-blur="setTransitFlagTrueRbkm(rbkm)" >
					<label for="rbkmTransitDay">Transit Day</label>
					<input type ="hidden" ng-model="tnc.tncTransitDay" id="tncTransitDay" ng-init="tnc.tncTransitDay='no'" >
        		</div>
        		</td>
        		
      			<td><div class="input-field col s3">	
      				<input class="prefix" type="checkbox" id ="checkStatisticalChargeRbkm" ng-model="checkStatisticalChargeRbkm" ng-change="saveTncStatisticalChargeRbkm()">
      				<input class="validate" type ="number" id="rbkmStatChg" name ="rbkmStatChg" ng-model="rbkm.rbkmStatChg" ng-disabled="!checkStatisticalChargeRbkm"  step="0.01" min="0.01"  ng-blur="setSCFlagTrueRbkm(rbkm)">
					<label for="rbkmStstChg">Statistical Charge</label>
					<input type ="hidden" ng-model="tnc.tncStatisticalCharge" id="tncStatisticalCharge" ng-init="tnc.tncStatisticalCharge='no'" >
      			</div>
				</td>
			</tr>
			
			
			
			<tr>
				<td>
					<div class="input-field col s3">
				   		<input class="prefix" type="checkbox" id ="checkPenalty" ng-model="checkPenalty" ng-change="saveTncPenalty()">
				   		<input class="btn teal white-text nopadding" type ="button" value="Penalty" id="regContPenalty"  ng-disabled="!checkPenalty" ng-click="OpenPenaltyDB()">
				   		<label>Add</label>
				   		<input type ="hidden" ng-model="tnc.tncPenalty" id="tncPenalty" ng-init="tnc.tncPenalty='no'">
	      			</div>
				</td>
			</tr>
			
			<tr>
				<td>
					<div class="input-field col s3">
				    	<input class="prefix" type="checkbox" id ="checkBonus" ng-model="checkBonus" ng-change="saveTncBonus()">
				    	<input class="btn teal white-text nopadding" type ="button" id="regContBonus" value="Bonus" ng-disabled="!checkBonus" ng-click="OpenBonusDB()">
				    	<label>Add</label>
				    	<input type ="hidden" ng-model="tnc.tncBonus" id="tncBonus" ng-init="tnc.tncBonus='no'">
        			</div>
				</td>
			</tr>	
			
			<tr>
				<td>
					<div class="input-field col s3">
						<input class="prefix" type="checkbox" id ="checkDetention" ng-model="checkDetention" ng-change="saveTncDetention()">
						<input class="btn teal white-text nopadding" type ="button" id="regContDetention" value="Detention" ng-disabled="!checkDetention" ng-click="OpenDetentionDB()">
						<label>Add</label>
						<input type ="hidden" ng-model="tnc.tncDetention" id="tncDetention" ng-init="tnc.tncDetention='no'">
      				</div>
				</td>
			</tr>
		 	<tr>
				<td><input type="submit" id="submit3" value="Submit" ></td>
			</tr>
		</table>
	</form>
	</div>	
	
	
	
	<div id="AddProductTypeDB" ng-hide = "AddProductTypeFlag">
	<form name="ProductTypeDB1Form" ng-submit="saveproducttype(ProductTypeDB1Form,pt)">
		<table>
			<tr>
				<td>Product Name</td>
				<td><input type ="text" id="ptName" name ="ptName" ng-model="pt.ptName" required></td>
			</tr>
			
		 	<tr>
				<td><input type="submit"  value="Submit" ></td>
			</tr>
		</table>
		</form>
	</div>	
	
	<div id="addVehicleTypeDB1" ng-hide = "VehicleTypeFlag1">
	<form name="VehicleForm" ng-submit="savevehicletype(VehicleForm,vt)">
		<table class="noborder">
			<tr>
				<td>Service Type *</td>
				<td><input type ="text" id="vtServiceType" name ="vtServiceType" ng-model="vt.vtServiceType" ng-minlength="1" ng-maxlength="30" required></td>
			</tr>
			
			<tr>
				<td>Vehicle Type *</td>
				<td><input type ="text" id="vtVehicleType" name ="vtVehicleType" ng-model="vt.vtVehicleType" ng-minlength="1" ng-maxlength="30" required></td>
			</tr>
			
			<tr>
				<td>Code *</td>
				<td><input type ="text" id="vtCode" name ="vtCode" ng-model="vt.vtCode"  required></td>
			</tr>
			
			<tr>
				<td>Load Limit *</td>
				<td><input type ="number" id="vtLoadLimit" name ="vtLoadLimit" ng-model="vt.vtLoadLimit" ng-minlength="1" ng-maxlength="7" min ="1"  required></td>
			</tr>
			
			<tr>
				<td>Guarantee Weight *</td>
				<td><input type ="number" id="vtGuaranteeWt" name ="vtGuaranteeWt" ng-model="vt.vtGuaranteeWt" step="0.01" min="0.01" required></td>
			</tr>
			
		 	<tr>
				<td><input type="submit" value="Submit"></td>
			</tr>
		</table>
		</form>
	</div>
	
		<div id="PenaltyDB" ng-hide="PenaltyFlag">
	<form name="PenaltyForm" ng-submit="savePbdForPenalty(PenaltyForm,pbdP)"> 
		<table>
		
			<tr>
				<td>From Station *</td>
				<td><input type ="text" id="pbdFromStnTempP" name ="pbdFromStnTempP" ng-model="pbdP.pbdFromStnTemp" readonly></td>
				<td><input type ="hidden" id="pbdFromStnP" name ="pbdFromStnP" ng-model="pbdP.pbdFromStn"  ></td>
			</tr>
		
			<tr>
				<td>To Station *</td>
				<td><input type ="text" id="pbdToStnTempP" name ="pbdToStnTempP" ng-model="pbdP.pbdToStnTemp" ng-click="OpenPenaltyToStationDB()" readonly required></td>
				<td><input type ="hidden" id="pbdToStnP" name ="pbdToStnP" ng-model="pbdP.pbdToStn"></td>
			
			</tr>
			
			<tr>
				<td>Start Date *</td>
				<td><input type ="date" id="pbdStartDtP" name ="pbdStartDtP" ng-model="pbdP.pbdStartDt" required readonly></td>
			</tr>
			
			<tr>
				<td>End Date *</td>
				<td><input type ="date" id="pbdEndDtP" name ="pbdEndDtP" ng-model="pbdP.pbdEndDt" required readonly></td>
			</tr>
			
			<tr>
				<td>From Day*</td>
				<td><input type ="number" id="pbdFromDayP" name ="pbdFromDayP" ng-model="pbdP.pbdFromDay" required min="1" ng-minlength="1" ng-maxlength="7"></td>
			</tr>
			
			<tr>
				<td>To Day *</td>
				<td><input type ="number" id="pbdToDayP" name ="pbdToDayP" ng-model="pbdP.pbdToDay" required min="1" ng-minlength="1" ng-maxlength="7"></td>
			</tr>
			
			<tr>
				<td>Vehicle Type *</td>
				<td><input type ="text" id="pbdVehicleTypeP" name ="pbdVehicleTypeP" ng-model="pbdP.pbdVehicleType" ng-click="OpenPenaltyVehicleTypeDB()" readonly required></td>
			</tr>
			
			<tr>
				<td> Hour Or Day *</td>
				<td>
				<select name="pbdHourNDayP" id="pbdHourNDayP" ng-model="pbdP.pbdHourNDay" required>
				<option value="Hour">Hour</option>
				<option value="Day">Day</option>
				</select></td>
			</tr>
			
			
			
			<tr>
				<td>Amount *</td>
				<td><input type ="number" id="pbdAmtP" name ="pbdAmtP" ng-model="pbdP.pbdAmt" required step="0.01" min="0.01"></td>
			</tr>
			
			<tr>
				<td><input type="submit" value="Submit" id="savePenalty"  ></td>
			</tr>
		</table>
		</form>
	</div>	
	
	<div id="BonusDB" ng-hide="BonusFlag">
	<form name="BonusForm" ng-submit="savePbdForBonus(BonusForm,pbdB)">
		<table>
		
			<tr>
				<td>From Station *</td>
				<td><input type ="text" id="pbdFromStnTempB" name ="pbdFromStnTempB" ng-model="pbdB.pbdFromStnTemp" readonly></td>
				<td><input type ="hidden" id="pbdFromStnB" name ="pbdFromStnB" ng-model="pbdB.pbdFromStn" ></td>
			</tr>
		
			<tr>
				<td>To Station *</td>
				<td><input type ="text" id="pbdToStnTempB" name ="pbdToStnTempB" ng-model="pbdB.pbdToStnTemp" ng-click="OpenBonusToStationDB()" readonly required></td>
				<td><input type ="hidden" id="pbdToStnB" name ="pbdToStnB" ng-model="pbdB.pbdToStn"></td>
			</tr>
			
			<tr>
				<td>Start Date *</td>
				<td><input type ="date" id="pbdStartDtB" name ="pbdStartDtB" ng-model="pbdB.pbdStartDt" required readonly></td>
			</tr>
			
			<tr>
				<td>End Date *</td>
				<td><input type ="date" id="pbdEndDtB" name ="pbdEndDtB" ng-model="pbdB.pbdEndDt" required readonly></td>
			</tr>
			
			<tr>
				<td>From Day *</td>
				<td><input type ="number" id="pbdFromDayB" name ="pbdFromDayB" ng-model="pbdB.pbdFromDay" min="1" required></td>
			</tr>
			
			<tr>
				<td>To Day *</td>
				<td><input type ="number" id="pbdToDayB" name ="pbdToDayB" ng-model="pbdB.pbdToDay" min="1" required></td>
			</tr>
			
			<tr>
				<td>Vehicle Type *</td>
				<td><input type ="text" id="pbdVehicleTypeB" name ="pbdVehicleTypeB" ng-model="pbdB.pbdVehicleType" ng-click="OpenBonusVehicleTypeDB()" readonly required></td>
			</tr>
			
			<tr>
				<td> Hour Or Day *</td>
				<td>
				<select name="pbdHourNDayB" id="pbdHourNDayB" ng-model="pbdB.pbdHourNDay" required>
				<option value="Hour">Hour</option>
				<option value="Day">Day</option>
				</select></td>
			</tr>
			
			
			
			<tr>
				<td>Amount *</td>
				<td><input type ="text" id="pbdAmtB" name ="pbdAmtB" ng-model="pbdB.pbdAmt" step="0.01" min="0.01" required></td>
			</tr>
			
		 	<tr>
				<td><input type="submit" value="Submit"  id="saveBonus" ></td>
			</tr>
		</table>
		</form>
	</div>	
	
	<div id="DetentionDB" ng-hide="DetentionFlag">
	<form name="DetentionForm"  ng-submit="savePbdForDetention(DetentionForm,pbdD)">
		<table>
		
			<tr>
				<td>From Station *</td>
				<td><input type ="text" id="pbdFromStnTempD" name ="pbdFromStnTempD" ng-model="pbdD.pbdFromStnTemp" readonly></td>
				<td><input type ="hidden" id="pbdToStnD" name ="pbdToStnD" ng-model="pbdD.pbdToStn" ></td>
			</tr>
		
			<tr>
				<td>To Station *</td>
				<td><input type ="text" id="pbdToStnTempD" name ="pbdToStnTempD" ng-model="pbdD.pbdToStnTemp" ng-click="OpenDetentionToStationDB()" readonly required></td>
				<td><input type ="hidden" id="pbdToStnD" name ="pbdToStnD" ng-model="pbdD.pbdToStn"></td>
			</tr>
			
			<tr>
				<td>Start Date *</td>
				<td><input type ="date" id="pbdStartDtD" name ="pbdStartDtD" ng-model="pbdD.pbdStartDt" required readonly></td>
			</tr>
			
			<tr>
				<td>End Date *</td>
				<td><input type ="date" id="pbdEndDtD" name ="pbdEndDtD" ng-model="pbdD.pbdEndDt" required readonly></td>
			</tr>
			
			<tr>
				<td>From Day *</td>
				<td><input type ="number" id="pbdFromDayD" name ="pbdFromDayD" ng-model="pbdD.pbdFromDay" required min="1"></td>
			</tr>
			
			<tr>
				<td>To Day *</td>
				<td><input type ="number" id="pbdToDayD" name ="pbdToDayD" ng-model="pbdD.pbdToDay" required min="1"></td>
			</tr>
			
			<tr>
				<td>Vehicle Type *</td>
				<td><input type ="text" id="pbdVehicleTypeD" name ="pbdVehicleTypeD" ng-model="pbdD.pbdVehicleType" ng-click="OpenDetentionVehicleTypeDB()" readonly required></td>
			</tr>
			
			<tr>
				<td> Hour Or Day *</td>
				<td>
				<select name="pbdHourNDayD" id="pbdHourNDayD" ng-model="pbdD.pbdHourNDay" required>
				<option value="Hour">Hour</option>
				<option value="Day">Day</option>
				</select></td>
			</tr>
			
			<tr>
				<td>Amount *</td>
				<td><input type ="number" id="pbdAmtD" name ="pbdAmtD" ng-model="pbdD.pbdAmt" required step="0.01" min="0.01"></td>
			</tr>
			
		 	<tr>
				<td><input type="submit" value="Submit" id="saveDetention" ></td>
			</tr>
		</table>
		</form>
	</div>	
		
	<div id="contToStationW" ng-hide = "contToStationFlagW">
	<form name="ContToStationFormW" ng-submit="saveContToStnW(ContToStationFormW,cts,regCnt,metricType)">
		<table>
			
			<tr>
				<td>GPS Require*</td>
				<td>
					<table>
						<tr>
							<select name="ctsGpsRqrName" id="ctsGpsRqrId" ng-model="cts.ctsGpsRequir" ng-init="cts.ctsGpsRequir='No'" required>
								<option value="Yes">Yes</option>
								<option value="No">No</option>
							</select>
				</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>Vehicle Type *</td>
				<td>
					<table>
						<tr>
							<td><input type ="text" id="ctsVehicleType" name ="ctsVehicleType" ng-model="cts.ctsVehicleType"  required ng-click="openCtsVehicleType()" readonly></td>
							<td><a class="btn-floating suffix waves-effect teal" type="button" id="openVehicleTypeDB" ng-model="savVehicletype" ng-click="OpenAddVehicleTypeDB()" ><i class="mdi-content-add white-text"></i></a></td>
						</tr>
					</table>
				</td>
			</tr>
			
			<tr>
				<td>Product Type*</td>
				<td>
					<table>
						<tr>
							<td><input type ="text" id="ctsProductType" name ="ctsProductType" ng-model="cts.ctsProductType" ng-click="openProductTypeDB()" readonly required></td>
							<td><a class="btn-floating suffix waves-effect teal" id="openProductTypeDB" ng-model="savProducttype" ng-click="OpenProductTypeDB1()" disabled="disabled"><i class="mdi-content-add white-text" style="font-size: 24px; margin-left: -5px;"></i></a></td>
						</tr>
					</table>
				</td>
			</tr>
			
			<tr>
				<td>From Weight *</td>
				<td><input type ="number" id="ctsFromWt" name ="ctsFromWt" ng-model="cts.ctsFromWt" step="0.00001" required></td>
			</tr>
			
			<tr>
				<td>To Weight *</td>
				<td><input type ="number" id="ctsToWt" name ="ctsToWt" ng-model="cts.ctsToWt" step="0.00001" required></td>
			</tr>
			
			<tr>
				<td>To Station *</td>
				<td><input type ="text" id="ctsToStnTemp" name ="ctsToStnTemp" ng-model="cts.ctsToStnTemp" ng-click="openToStationsDB()" readonly required>
				<input type ="hidden" id="ctsToStn" name ="ctsToStn" ng-model="cts.ctsToStn"></td>
			</tr>
			
			<!-- <tr>
				<td>Distance/KM *</td>
				<td><input type ="number" id="ctsDistance" name ="ctsDistance" ng-model="cts.ctsDistanceKm" ng-blur="calculateTransitDay(cts)" required></td>
			</tr> -->
			
		
			<tr>
				<td>Rate *</td>
				<td><input type ="number" id="ctsRate" name ="ctsRate" ng-model="cts.ctsRate" step="0.00001" required></td>
			</tr>
			
			<tr>
				<td>Additional Rate *</td>
				<td><input type ="number" id="ctsAdditionalRate" name ="ctsAdditionalRate" ng-model="cts.ctsAdditionalRate" step="0.00001" required disabled="disabled"></td>
			</tr>
			
			<tr>
				<td></td>
			</tr>
			
			<tr>
				<td> 
				<div class="input-field col s3">
					<input class="prefix" type="checkbox" id ="checkLoad" ng-model="checkLoad" ng-change="saveTncLoad()">
					<input class="validate" type ="number" id="ctsToLoad" name ="ctsToLoad" ng-model="cts.ctsToLoad" ng-disabled="!checkLoad"  step="0.00001" min="0.00001"  ng-blur="setLoadFlagTrue(cts)">				
					<label>Load</label>
					<input type ="hidden" ng-model="tnc.tncLoading" id="tncLoading" ng-init="tnc.tncLoading='no'" >
      	   		 </div>
      	   		 </td>
      	   
      	   		<td>	 
      			<div class="input-field col s3">
					<input class="prefix" type="checkbox" id ="checkUnLoad" ng-model="checkUnLoad" ng-change="saveTncUnload()">
					<input class="validate" type ="number" id="ctsToUnLoad" name ="ctsToUnLoad" ng-model="cts.ctsToUnLoad" ng-disabled="!checkUnLoad"  step="0.00001" min="0.00001"  ng-blur="setUnLoadFlagTrue(cts)">
					<label>UnLoad</label>
					<input type ="hidden" ng-model="tnc.tncUnLoading" id="tncUnLoading"  ng-init="tnc.tncUnLoading='no'">	
      			</div>
				</td>
			</tr>
			
			<tr>
				<td>
				<div class="input-field col s3">
					<input class="prefix" type="checkbox" id ="checkTransitDay" ng-model="checkTransitDay" ng-change="saveTncTransitDay()">
      				<input class="validate" type ="number" id="ctsToTransitDay" name ="ctsToTransitDay" ng-model="cts.ctsToTransitDay" ng-disabled="!checkTransitDay"  min="1" ng-blur="setTransitFlagTrue(cts)" >
					<label for="regContTransitDay">Transit Day</label>
					<input type ="hidden" ng-model="tnc.tncTransitDay" id="tncTransitDay" ng-init="tnc.tncTransitDay='no'" >
        		</div>
        		</td>
        		
      			<td><div class="input-field col s3">	
      				<input class="prefix" type="checkbox" id ="checkStatisticalCharge" ng-model="checkStatisticalCharge" ng-change="saveTncStatisticalCharge()">
      				<input class="validate" type ="number" id="ctsToStatChg" name ="ctsToStatChg" ng-model="cts.ctsToStatChg" ng-disabled="!checkStatisticalCharge"  step="0.00001" min="0.00001"  ng-blur="setSCFlagTrue(cts)">
					<label for="regContStatisticalCharge">Statistical Charge</label>
					<input type ="hidden" ng-model="tnc.tncStatisticalCharge" id="tncStatisticalCharge" ng-init="tnc.tncStatisticalCharge='no'" >
      			</div>
				</td>
			</tr>
			
			<tr>
				<td>
					<div class="input-field col s3">
				   		<input class="prefix" type="checkbox" id ="checkPenalty" ng-model="checkPenalty" ng-change="saveTncPenalty()">
				   		<input class="btn teal white-text nopadding" type ="button" value="Penalty" id="regContPenalty"  ng-disabled="!checkPenalty" ng-click="OpenPenaltyDB()">
				   		<label>Add</label>
				   		<input type ="hidden" ng-model="tnc.tncPenalty" id="tncPenalty" ng-init="tnc.tncPenalty='no'">
	      			</div>
				</td>
			</tr>
			
			<tr>
				<td>
					<div class="input-field col s3">
				    	<input class="prefix" type="checkbox" id ="checkBonus" ng-model="checkBonus" ng-change="saveTncBonus()">
				    	<input class="btn teal white-text nopadding" type ="button" id="regContBonus" value="Bonus" ng-disabled="!checkBonus" ng-click="OpenBonusDB()">
				    	<label>Add</label>
				    	<input type ="hidden" ng-model="tnc.tncBonus" id="tncBonus" ng-init="tnc.tncBonus='no'">
        			</div>
				</td>
			</tr>	
			
			<tr>
				<td>
					<div class="input-field col s3">
						<input class="prefix" type="checkbox" id ="checkDetention" ng-model="checkDetention" ng-change="saveTncDetention()">
						<input class="btn teal white-text nopadding" type ="button" id="regContDetention" value="Detention" ng-disabled="!checkDetention" ng-click="OpenDetentionDB()">
						<label>Add</label>
						<input type ="hidden" ng-model="tnc.tncDetention" id="tncDetention" ng-init="tnc.tncDetention='no'">
      				</div>
				</td>
			</tr>
			
		 	<tr>
				<td colspan="2"><input type="submit" value="Submit"></td>
			</tr>
		</table>
		</form>
	</div>
	
	
	<div id="contToStationQ" ng-hide = "contToStationFlagQ">
	<form name="ContToStationFormQ" ng-submit="saveContToStnQ(ContToStationFormQ,cts,regCnt)">
		<table>
			
			<tr>
				<td>GPS Require*</td>
				<td>
					<table>
						<tr>
							<select name="ctsGpsRqrName" id="ctsGpsRqrId" ng-model="cts.ctsGpsRequir" ng-init="cts.ctsGpsRequir='No'" required>
								<option value="Yes">Yes</option>
								<option value="No">No</option>
							</select>
				</tr>
					</table>
				</td>
			</tr>
			
			<tr>
				<td>Vehicle Type *</td>
				<td>
					<table>
						<tr>
							<td><input type ="text" id="ctsVehicleType" name ="ctsVehicleType" ng-model="cts.ctsVehicleType"  required ng-click="openCtsVehicleType()" readonly></td>
							<td><a class="btn-floating suffix waves-effect teal" id="openVehicleTypeDB" ng-model="savVehicletype" ng-click="OpenAddVehicleTypeDB()" ><i class="mdi-content-add white-text"></i></a></td>
						</tr>
					</table>
				</td>
			</tr>
			
			<tr>
				<td>Product Type*</td>
				<td>
					<table>
						<tr>
							<td><input type ="text" id="ctsProductType" name ="ctsProductType" ng-model="cts.ctsProductType" ng-click="openProductTypeDB()" readonly required></td>
							<td><a class="btn-floating suffix waves-effect teal" id="openProductTypeDB" ng-model="savProducttype" ng-click="OpenProductTypeDB1()" disabled="disabled"><i class="mdi-content-add white-text" style="font-size: 24px; margin-left: -5px;"></i></a></td>
						</tr>
					</table>
				</td>
			</tr>
			
			<tr>
				<td>From Weight *</td>
				<td><input type ="number" id="ctsFromWtQ" name ="ctsFromWt" ng-model="cts.ctsFromWt" step="0.00001" required></td>
			</tr>
			
			<tr>
				<td>To Weight *</td>
				<td><input type ="number" id="ctsToWtQ" name ="ctsToWt" ng-model="cts.ctsToWt" step="0.00001" required></td>
			</tr>
			
			<tr>
				<td>To Station *</td>
				<td><input type ="text" id="ctsToStnTemp" name ="ctsToStnTemp" ng-model="cts.ctsToStnTemp" ng-click="openToStationsDB()" readonly required></td>
				<td><input type ="hidden" id="ctsToStn" name ="ctsToStn" ng-model="cts.ctsToStn"></td>
			</tr>
			
			<!-- <tr>
				<td>Distance/KM *</td>
				<td><input type ="number" id="ctsDistanceQ" name ="ctsDistance" ng-model="cts.ctsDistanceKm" ng-blur="calculateTransitDay(cts)" required></td>
			</tr> -->
			
			<tr>
				<td>Rate *</td>
				<td><input type ="number" id="ctsRateQ" name ="ctsRate" ng-model="cts.ctsRate" step="0.00001" required></td>
			</tr>
			
			<tr>
				<td>Additional Rate *</td>
				<td><input type ="number" id="ctsAdditionalRate" name ="ctsAdditionalRate" ng-model="cts.ctsAdditionalRate" step="0.00001" required disabled="disabled"></td>
			</tr>
			
			<tr>
				<td></td>
			</tr>
			
			<tr>
				<td> <div class="input-field col s3">
					<input class="prefix" type="checkbox" id ="checkLoad" ng-model="checkLoad" ng-change="saveTncLoad()">
					<input class="validate" type ="number" id="ctsToLoad" name ="ctsToLoad" ng-model="cts.ctsToLoad" ng-disabled="!checkLoad"  step="0.00001" min="0.00001"  ng-blur="setLoadFlagTrue(cts)">				
					<label>Load</label>
					<input type ="hidden" ng-model="tnc.tncLoading" id="tncLoading" ng-init="tnc.tncLoading='no'" >
      	   		 </div></td>
      	   
      	   		<td>	 
      			<div class="input-field col s3">
					<input class="prefix" type="checkbox" id ="checkUnLoad" ng-model="checkUnLoad" ng-change="saveTncUnload()">
					<input class="validate" type ="number" id="ctsToUnLoad" name ="ctsToUnLoad" ng-model="cts.ctsToUnLoad" ng-disabled="!checkUnLoad"  step="0.00001" min="0.00001"  ng-blur="setUnLoadFlagTrue(cts)">
					<label>UnLoad</label>
					<input type ="hidden" ng-model="tnc.tncUnLoading" id="tncUnLoading"  ng-init="tnc.tncUnLoading='no'">	
      			</div>
				</td>
			</tr>
			
			<tr>
				<td>
				<div class="input-field col s3">
					<input class="prefix" type="checkbox" id ="checkTransitDay" ng-model="checkTransitDay" ng-change="saveTncTransitDay()">
      				<input class="validate" type ="number" id="ctsToTransitDay" name ="ctsToTransitDay" ng-model="cts.ctsToTransitDay" ng-disabled="!checkTransitDay"  min="1" ng-blur="setTransitFlagTrue(cts)" >
					<label for="regContTransitDay">Transit Day</label>
					<input type ="hidden" ng-model="tnc.tncTransitDay" id="tncTransitDay" ng-init="tnc.tncTransitDay='no'" >
        		</div>
        		</td>
        		
      			<td><div class="input-field col s3">	
      				<input class="prefix" type="checkbox" id ="checkStatisticalCharge" ng-model="checkStatisticalCharge" ng-change="saveTncStatisticalCharge()">
      				<input class="validate" type ="number" id="ctsToStatChg" name ="ctsToStatChg" ng-model="cts.ctsToStatChg" ng-disabled="!checkStatisticalCharge"  step="0.00001" min="0.00001"  ng-blur="setSCFlagTrue(cts)">
					<label for="regContStatisticalCharge">Statistical Charge</label>
					<input type ="hidden" ng-model="tnc.tncStatisticalCharge" id="tncStatisticalCharge" ng-init="tnc.tncStatisticalCharge='no'" >
      			</div>
				</td>
			</tr>
			
			<tr>
				<td>
					<div class="input-field col s3">
				   		<input class="prefix" type="checkbox" id ="checkPenalty" ng-model="checkPenalty" ng-change="saveTncPenalty()">
				   		<input class="btn teal white-text nopadding" type ="button" value="Penalty" id="regContPenalty"  ng-disabled="!checkPenalty" ng-click="OpenPenaltyDB()">
				   		<label>Add</label>
				   		<input type ="hidden" ng-model="tnc.tncPenalty" id="tncPenalty" ng-init="tnc.tncPenalty='no'">
	      			</div>
				</td>
			</tr>
			
			<tr>
				<td>
					<div class="input-field col s3">
				    	<input class="prefix" type="checkbox" id ="checkBonus" ng-model="checkBonus" ng-change="saveTncBonus()">
				    	<input class="btn teal white-text nopadding" type ="button" id="regContBonus" value="Bonus" ng-disabled="!checkBonus" ng-click="OpenBonusDB()">
				    	<label>Add</label>
				    	<input type ="hidden" ng-model="tnc.tncBonus" id="tncBonus" ng-init="tnc.tncBonus='no'">
        			</div>
				</td>
			</tr>	
			
			<tr>
				<td>
					<div class="input-field col s3">
						<input class="prefix" type="checkbox" id ="checkDetention" ng-model="checkDetention" ng-change="saveTncDetention()">
						<input class="btn teal white-text nopadding" type ="button" id="regContDetention" value="Detention" ng-disabled="!checkDetention" ng-click="OpenDetentionDB()">
						<label>Add</label>
						<input type ="hidden" ng-model="tnc.tncDetention" id="tncDetention" ng-init="tnc.tncDetention='no'">
      				</div>
				</td>
			</tr>
			
		 	<tr>
				<td colspan="2"><input type="submit" value="Submit"></td>
			</tr>
		</table>
		</form>
	</div>
	
		
	
	<div id="addStation" ng-hide="addStationFlag">
	<form name="StationForm" ng-submit="saveNewStation(StationForm,station)">
		<table>
			<tr>
				<td>Station Name*</td>
				<td><input id="stnName" type ="text" name ="stnName" ng-model="station.stnName" ng-required="true" ng-minlength="3" ng-maxlength="40"></td>
			</tr>
			
			<tr>
				<td>Station District*</td>
				<td><input class="validate" id="district" type ="text" name ="stnDistrict" ng-model="station.stnDistrict" ng-required="true" ng-minlength="3" ng-maxlength="40"></td>
			</tr>
			
			<tr>
				<td>State Code *</td>
				<td><input class="validate" id="code" type ="text" name ="stateCode"  ng-model="station.stateCode" readonly ng-click="openStateCodeDB()" required></td>
			</tr>
			<tr>
				<td>Pin</td>
				<td><input class="validate"  type ="text" id="stnPin" name ="stnPin" ng-model="station.stnPin" ng-minlength="6" ng-maxlength="6" ng-required="true" ></td>
			</tr>
			
		 	<tr>
				<td colspan="2"><input type="submit" value="Submit"></td>
			</tr>
		</table>
		</form>
	</div>
		
	
		
	 <div id ="regContCngrCodeDB" ng-hide = "CngrCodeFlag">
	 
	 <input type="text" name="filterCngr" ng-model="filterCngr" placeholder="Search by Customer Code">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				
				<th>Customer Code</th>
				
				<th>Customer Name</th>
			
				<th>Customer Pin</th>
				
				<th>Customer FaCode</th>
				
				<th>Daily Contract Allowed</th>
			</tr>
 	  
		  
		  <tr ng-repeat="cngrCode in customerList | filter:filterCngr">
		 	  <td><input type="radio"  name="cngrCodeName"  class="custCode"  value="{{ cngrCode }}" ng-model="custCode" ng-click="savCngrCode(cngrCode)"></td>
              <td>{{ cngrCode.custCode }}</td>
              <td>{{ cngrCode.custName }}</td>
              <td>{{ cngrCode.custPin }}</td>
              <td>{{ cngrCode.custFaCode }}</td>
              <td>{{ cngrCode.custIsDailyContAllow }}</td>
          </tr>
      </table>   
	</div>

	<div id ="regContBLPMCodeDB" ng-hide = "blpmCodeFlag">
	
	<input type="text" name="filterBlpm" ng-model="filterBlpm" placeholder="Search by Customer Code">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				
				<!-- <th>Customer Code</th> -->
				
				<th>Customer Name</th>
			
				<th>Customer Pin</th>
				
				<th>Customer FaCode</th>
				
				<th>Daily Contract Allowed</th>
			</tr>
 	  
		  
		  <tr ng-repeat="blpmCode in customerList | filter:filterBlpm">
		 	  <td><input type="radio"  name="blpmCodeName"  class="custCode"  value="{{ blpmCode }}" ng-model="custCode1" ng-click="savBLPMCode(blpmCode)"></td>
              <!-- <td>{{ blpmCode.custCode }}</td> -->
              <td>{{ blpmCode.custName }}</td>
              <td>{{ blpmCode.custPin }}</td>
              <td>{{ blpmCode.custFaCode }}</td>
              <td>{{ blpmCode.custIsDailyContAllow }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="branchCodeDB" ng-hide = "branchCodeFlag">
 	 
 	 <input type="text" name="filterBranch" ng-model="filterBranch" placeholder="Search by Branch Code"> 
 	  <table>
 	 		<tr>
				<th></th>
				
				<th>Branch Code</th>
				
				<th>Branch Name</th>
			
				<th>Branch Pin</th>
			</tr>
 	  
		  
		  <tr ng-repeat="branch in branchList | filter:filterBranch">
		 	  <td><input type="radio"  name="branchCodeName" class="branchCode"  value="{{ branch.branchCode }}" ng-model="branchCode" ng-click="savBranchCode(branch)"></td>
              <td>{{ branch.branchCode }}</td>
              <td>{{ branch.branchName }}</td>
              <td>{{ branch.branchPin }}</td>
          </tr>
         </table>
         
	</div>
	
	
	<div id ="regContFromStationDB" ng-hide = "FromStationFlag">
 	 
 	 <input type="text" name="filterFStn" ng-model="filterFStn" placeholder="Search by Station Code">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				<th>Station Code</th>
				
				<th>Station Name</th>
			
				<th>Station District</th>
			</tr>
 	  
		  
		  <tr ng-repeat="fromStation in stationList  | filter:filterFStn">
		 	  <td><input type="radio"  name="fromStationName"  class="stnName"  value="{{ fromStation }}" ng-model="stnName"  ng-click="savFrmStnCode(fromStation)"></td>
               <td>{{ fromStation.stnCode }}</td>
              <td>{{ fromStation.stnName }}</td>
              <td>{{ fromStation.stnDistrict }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="regContToStationDB" ng-hide = "ToStationFlag">
	
	 <input type="text" name="filterTStn" ng-model="filterTStn" placeholder="Search by Station Code">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				<th>Station Code</th>
				
				<th>Station Name</th>
			
				<th>Station District</th>
			</tr>
 	  
		  
		  <tr ng-repeat="toStation in stationList | filter:filterTStn">
		 	  <td><input type="radio"  name="toStationName"  class="stnName"  value="{{ toStation }}" ng-model="stnName1" ng-click="savToStnCode(toStation)"></td>
               <td>{{ toStation.stnCode }}</td>
              <td>{{ toStation.stnName }}</td>
              <td>{{ toStation.stnDistrict }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="regContCrNameDB" ng-hide = "CrNameFlag">
	
	<input type="text" name="filterCr" ng-model="filterCr" placeholder="Search by CR Code">
 	  
 	   	  <table>
 	 		<tr>
 	 			<th></th>
 	 			
 	 			<!-- <th>CR Code</th> -->
 	 			
				<th>Customer Name</th>
				
				<th>CR Name</th>
			
				<th>CR Designation</th>
			</tr>
 	 
		  
		  <tr ng-repeat="cr in crList | filter:filterCr">
		 	  <td><input type="radio"  name="cr" class="custRepCode"  value="{{ cr}}" ng-model="custRepCode" ng-click="savCrName(cr)"></td>
             <!-- <td>{{ cr.custRepCode }}</td> -->
              <td>{{ cr.custName }}</td>
              <td>{{ cr.custRepName }}</td>
              <td>{{ cr.custRepDesig }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="ctsVehicleTypeDB" ng-hide = "VehicleTypeFlag">
	
	<input type="text" name="filterVehicleType" ng-model="filterVehicleType" placeholder="Search by Vehicle Type Code">
 	  
 	   	  <table>
 	 		<tr>
				<th></th>
				<th>Vehicle Type Code</th>
				
				<th>Vehicle Type</th>
				
				<th>Service Type</th>
			</tr>
 	  
		  
		  <tr ng-repeat="VT in vtList | filter:filterVehicleType">
		 	  <td><input type="radio"  name="VTName" class="vtCode"  value="{{ VT }}" ng-model="vtCode" ng-click="savVehicleType(VT)"></td>
              <td>{{ VT.vtCode }}</td>
              <td>{{ VT.vtVehicleType }}</td>
              <td>{{VT.vtServiceType}}</td>
          </tr>
         </table>       
	</div>
	
	<div id ="rbkmFromStationDB" ng-hide = "rbkmFromStationFlag">
	
	<input type="text" name="filterRbkmTStn" ng-model="filterRbkmTStn" placeholder="Search by Station Code">
 	  
 	  <table>
 	 		<tr>
				<th></th> 
				
				<th>Station Code</th>
				
				<th>Station Name</th>
			
				<th>Station District</th>
			</tr>
 	 
		  
		  <tr ng-repeat="rbkmFromstation in stationList | filter:filterRbkmTStn">
		 	  <td><input type="radio"  name="rbkmFromstationName"  class="station"  value="{{rbkmFromstation}}" ng-model="stnCode2" ng-click="savRbkmFromStnCode(rbkmFromstation)"></td>
              	<td>{{ rbkmFromstation.stnCode }}</td>
              	<td>{{ rbkmFromstation.stnName }}</td>
              	<td>{{ rbkmFromstation.stnDistrict }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="rbkmToStationDB" ng-hide = "rbkmToStationFlag">
	
	<input type="text" name="filterRbkmTStn" ng-model="filterRbkmTStn" placeholder="Search by Station Code">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				<th>Station Code</th>
				
				<th>Station Name</th>
			
				<th>Station District</th>
			</tr>
 	 
		  
		  <tr ng-repeat="rbkmTostation in stationList | filter:filterRbkmTStn">
		 	  <td><input type="radio"  name="rbkmTostationName"  value="{{rbkmTostation }}" ng-model="stnCode3" ng-click="saveRbkmToStnCode(rbkmTostation)"></td>
          		<td>{{ rbkmTostation.stnCode }}</td>
              	<td>{{ rbkmTostation.stnName }}</td>
              	<td>{{ rbkmTostation.stnDistrict }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="rbkmStateCodeDB" ng-hide = "rbkmStateCodeFlag">
	
	<input type="text" name="filterRbkmState" ng-model="filterRbkmState" placeholder="Search by State Code">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				<th>State Code</th>
				
				<th>State Name</th>
			
			</tr>
 	 
		  
		  <tr ng-repeat="state in listState | filter:filterRbkmState">
		 	  <td><input type="radio"  name="stateName"  value="{{ state }}" ng-model="stateCode" ng-click="saveStateCode(state)"></td>
               <td>{{ state.stateCode }}</td>
               <td>{{ state.stateName }}</td>
          </tr>
       </table>
         
	</div>
	
	<div id ="rbkmVehicleTypeDB" ng-hide = "rbkmVehicleTypeFlag">
 	 
 	 <input type="text" name="filterRbkmVehicle" ng-model="filterRbkmVehicle" placeholder="Search by Vehicle Type Code">
 	  
 	   	  <table>
 	 		<tr>
				<th></th>
				<th>Vehicle Type Code</th>
				
				<th>Vehicle Type</th>
				
				<th>Service Type</th>
			</tr>
 	  
		  
		 <tr ng-repeat="rbkmVType in vtList | filter:filterRbkmVehicle">
		 	  <td><input type="radio"  name="rbkmVTypeName"  value="{{ rbkmVType }}" ng-model="vtCode" ng-click="savRbkmVehicleType(rbkmVType)"></td>
              <td>{{ rbkmVType.vtCode }}</td>
              <td>{{ rbkmVType.vtVehicleType }}</td>
              <td>{{rbkmVType.vtServiceType}}</td>
              
          </tr>
         </table>       
	</div>
	
	<div id ="productTypeDB" ng-hide="ProductTypeFlag"> 
	
	<input type="text" name="filterProduct" ng-model="filterProduct" placeholder="Search by Product Name">
 	   	<table>
 	 		<tr>
				<th></th>
				<th>Product Name</th>
			</tr>
 	  
		  
		  <tr ng-repeat="pt in ptList  | filter:filterProduct">
		 	  <td><input type="radio"  name="ptName" class="ptName"  value="{{ pt }}" ng-model="ptName" ng-click="savProductName(pt)"></td>
              <td>{{ pt }}</td>
          </tr>
         </table>
   </div>
   
   
   <div id ="rbkmProductTypeDB" ng-hide="rbkmProductTypeFlag"> 
	
	<input type="text" name="filterProduct" ng-model="filterProduct" placeholder="Search by Product Name">
 	   	<table>
 	 		<tr>
				<th></th>
				<th>Product Name</th>
			</tr>
 	  
		  
		  <tr ng-repeat="pt in ptList  | filter:filterProduct">
		 	  <td><input type="radio"  name="ptName" class="ptName"  value="{{ pt }}" ng-model="ptName" ng-click="savRbkmProductName(pt)"></td>
              <td>{{ pt }}</td>
          </tr>
         </table>
   </div>
   
   
   <div id ="PenaltyFromStationDB" ng-hide = "PenaltyFromStationFlag">
 	
 	<input type="text" name="filterPenaltyFStn" ng-model="filterPenaltyFStn" placeholder="Search by Station Code">
 	  
 	  <table>
 	 		<tr>
				<th></th> 
				
				<th>Station Code</th>
				
				<th>Station Name</th>
			
				<th>Station District</th>
			</tr>
 	 
		  
		  <tr ng-repeat="penaltyFromstation in stationList  | filter:filterPenaltyFStn">
		 	  <td><input type="radio"  name="penaltyFromstationName" class="station"  value="{{ penaltyFromstation }}" ng-model="stnCode2" ng-click="savPenaltyFromStn(penaltyFromstation)"></td>
              <td>{{ penaltyFromstation.stnCode }}</td>
              <td>{{ penaltyFromstation.stnName }}</td>
              <td>{{ penaltyFromstation.stnDistrict }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="PenaltyToStationDB" ng-hide = "PenaltyToStationFlag">
	
	<input type="text" name="filterPenaltyTStn" ng-model="filterPenaltyTStn" placeholder="Search by Station Code">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				
				<th>Station Code</th>
				
				<th>Station Name</th>
				
				<th>Station District</th>
			</tr>
 	 
		  
		  <tr ng-repeat="penaltyTostation in stationList | filter:filterPenaltyTStn">
		 	  <td><input type="radio"  name="penaltyTostationName"  value="{{ penaltyTostation }}" ng-model="stnCode3" ng-click="savPenaltyToStn(penaltyTostation)"></td>
              <td>{{ penaltyTostation.stnCode }}</td>
              <td>{{ penaltyTostation.stnName }}</td>
              <td>{{ penaltyTostation.stnDistrict }}</td>
          </tr>
         </table>
         
	</div>
	

	<div id ="PenaltyVehicleTypeDB" ng-hide = "PenaltyVehicleTypeFlag">
 	 
 	 <input type="text" name="filterPenaltyVehicle" ng-model="filterPenaltyVehicle" placeholder="Search by Vehicle Code"> 
 	   	  <table>
 	 		<tr>
				<th></th>
				<th>Vehicle Type Code</th>
				
				<th>Vehicle Type</th>
			
				<th>Service Type</th>
			</tr>
 	  
		  
		 <tr ng-repeat="VtPenalty in vtList | filter:filterPenaltyVehicle">
		 	  <td><input type="radio"  name="VtPenaltyName" class="vtCode"  value="{{ VtPenalty }}" ng-model="vtCode" ng-click="savPenaltyVehicleType(VtPenalty)"></td>
              <td>{{ VtPenalty.vtCode }}</td>
              <td>{{ VtPenalty.vtVehicleType }}</td>
               <td>{{VtPenalty.vtServiceType}}</td>
             
          </tr>
         </table>       
	</div>
	
		<div id ="BonusFromStationDB" ng-hide = "BonusFromStationFlag">
		
		<input type="text" name="filterBonusFStn" ng-model="filterBonusFStn" placeholder="Search by Station Code">
 	  
 	  <table>
 	 		<tr>
				<th></th> 
				<th>Station Code</th>
				
				<th>Station Name</th>
			
				<th>Station District</th>
			</tr>
 	 
		  
		  <tr ng-repeat="bonusFromstation in stationList | filter:filterBonusFStn">
		 	  <td><input type="radio"  name="bonusFromstationName" class="station"  value="{{ bonusFromstation }}" ng-model="stnCode2" ng-click="savBonusFromStn(bonusFromstation)"></td>
              <td>{{ bonusFromstation.stnCode }}</td>
              <td>{{ bonusFromstation.stnName }}</td>
              <td>{{ bonusFromstation.stnDistrict }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="BonusToStationDB" ng-hide = "BonusToStationFlag">
	
	<input type="text" name="filterBonusTStn" ng-model="filterBonusTStn" placeholder="Search by Station Code">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				
				<th>Station Code</th>
				
				<th>Station Name</th>
			
				<th>Station District</th>
			</tr>
 	 
		  
		  <tr ng-repeat="bonusTostation in stationList | filter:filterBonusTStn">
		 	  <td><input type="radio"  name="bonusTostationName"  value="{{ bonusTostation }}" ng-model="stnCode3" ng-click="savBonusToStn(bonusTostation)"></td>
              <td>{{ bonusTostation.stnCode }}</td>
              <td>{{ bonusTostation.stnName }}</td>
              <td>{{ bonusTostation.stnDistrict }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="BonusVehicleTypeDB" ng-hide = "BonusVehicleTypeFlag">
	
	<input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search by Vehicle Code">
 	  
 	   	  <table>
 	 		<tr>
				<th></th>
				<th>Vehicle Type Code</th>
				
				<th>Vehicle Type</th>
				
				<th>Service Type</th>
			</tr>
 	  
		  
		 <tr ng-repeat="vtBonus in vtList  | filter:filterTextbox">
		 	  <td><input type="radio"  name="vtBonusName"  class="vtCode"  value="{{ vt.vtCode }}" ng-model="vtCode" ng-click="savBonusVehicleType(vtBonus)"></td>
              <td>{{ vtBonus.vtCode }}</td>
              <td>{{ vtBonus.vtVehicleType }}</td>
              <td>{{vtBonus.vtServiceType }}</td>
              
          </tr>
         </table>       
	</div>
	
		<div id ="DetentionFromStationDB" ng-hide = "DetentionFromStationFlag">
 	  	<input type="text" name="filterDetentionFStn" ng-model="filterDetentionFStn" placeholder="Search by Station Code">
 	  <table>
 	 		<tr>
				<th></th> 
				
				<th>Station Code</th>
				
				<th>Station Name</th>
			
				<th>Station District</th>
			</tr>
 	 
		  
		  <tr ng-repeat="detentionFromstation in stationList | filter:filterDetentionFStn">
		 	  <td><input type="radio"  name="detentionFromstationName"  class="station"  value="{{ detentionFromstation }}" ng-model="stnCode2" ng-click="savDetentionFromStn(detentionFromstation)"></td>
             <td>{{ detentionFromstation.stnCode }}</td>
              <td>{{ detentionFromstation.stnName }}</td>
              <td>{{ detentionFromstation.stnDistrict }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="DetentionToStationDB" ng-hide = "DetentionToStationFlag">
	
	<input type="text" name="filterDetentionTStn" ng-model="filterDetentionTStn" placeholder="Search by Station Code">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				<th>Station Code</th>
				
				<th>Station Name</th>
			
				<th>Station District</th>
			</tr>
 	 
		  
		  <tr ng-repeat="detentionTostation in stationList | filter:filterDetentionTStn">
		 	  <td><input type="radio"  name="detentionTostationName"   value="{{ detentionTostation }}" ng-model="stnCode3" ng-click="savDetentionToStn(detentionTostation)"></td>
             <td>{{ detentionTostation.stnCode }}</td>
              <td>{{ detentionTostation.stnName }}</td>
              <td>{{ detentionTostation.stnDistrict }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id ="DetentionVehicleTypeDB" ng-hide = "DetentionVehicleTypeFlag">
 	  
 	  <input type="text" name="filterDetentionVehicle" ng-model="filterDetentionVehicle" placeholder="Search by Vehicle Code">
 	   	  <table>
 	 		<tr>
				<th></th>
				<th>Vehicle Type Code</th>
				
				<th>Vehicle Type</th>
				
				<th>Service Type</th>
			</tr>
 	  
		  
		 <tr ng-repeat="vtDetention in vtList | filter:filterDetentionVehicle">
		 	  <td><input type="radio"  name="vtDetentionName"  class="vtCode"  value="{{ vtDetention }}" ng-model="vtCode" ng-click="savDetentionVehicleType(vtDetention)"></td>
               <td>{{ vtDetention.vtCode }}</td>
              <td>{{ vtDetention.vtVehicleType }}</td>
              <td>{{ vtDetention.vtServiceType }}</td>
          </tr>
         </table>       
	</div>
	
	<div id ="ToStationsDB" ng-hide ="ToStationsDBFlag">
 	  	<input type="text" name="filterToStns" ng-model="filterToStns" placeholder="Search by Station Code">
 	  <table>
 	 		<tr>
				<th></th> 
				
				<th>Station Code</th>
				
				<th>Station Name</th>
			
				<th>Station District</th>
			</tr>
 	 
		  
		  <tr ng-repeat="toStations in stationList | filter:filterToStns">
		 	  <td><input type="radio"  name="toStations"  class="station"  value="{{ toStations }}" ng-model="toStns" ng-click="saveToStations(toStations)"></td>
             <td>{{ toStations.stnCode }}</td>
              <td>{{ toStations.stnName }}</td>
              <td>{{ toStations.stnDistrict }}</td>
          </tr>
         </table>
	</div>
	
	<div id ="StateCodeDB" ng-hide="StateCodeFlag">
	
	<input type="text" name="filterState" ng-model="filterState" placeholder="Search by State Code">
 	  
 	  <table>
 	 		<tr>
				<th></th>
				<th>State Code</th>
			</tr>
 	 	<tr ng-repeat="statecode in listState  | filter:filterState">
		 	  <td><input type="radio"  name="statecode"  value="{{ statecode }}" ng-model="stateCode1" ng-click="savStateCode(statecode)"></td>
               <td>{{ statecode.stateCode }}</td>
          </tr>
         </table>
         
	</div>
	
	<div id="regContDB" ng-hide = "regContFlag">
	<div class="col s3 hidden-xs hidden-sm"> &nbsp; </div> 
	<div class="col s6 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >
	<table class="table-hover table-bordered table-bordered">
		<tr>
			<td><h3>Regular Contract Details</h3></td>
		</tr>
		<tr>
			<td>BLPM Code</td>
			<td>{{regCnt.regContBLPMCode}}</td>
		</tr>
		
		<tr>
			<td>Consignor Code</td>
			<td>{{regCnt.regContCngrCode}}</td>
		</tr>
		
		<tr>
			<td>CR Name</td>
			<td>{{regCnt.regContCrNameTemp}}</td>
		</tr>
		
		<tr>
			<td>From Station</td>
			<td>{{regCnt.regContFromStation}}</td>
		</tr>
		
		<tr ng-show="regCnt.regContToStation">
			<td>To Station</td>
			<td>{{regCnt.regContToStation}}</td>
		</tr>
		
		<tr>
			<td>Start Date</td>
			<td>{{regCnt.regContFromDt}}</td>
		</tr>
		
		<tr>
			<td>End Date</td>
			<td>{{regCnt.regContToDt}}</td>
		</tr>
	
		<tr>
			<td>DC</td>
			<td>{{regCnt.regContDc}}</td>
		</tr>
		
		<tr>
			<td>Cost Grade</td>
			<td>{{regCnt.regContCostGrade}}</td>
		</tr>
		
		<tr>
			<td>DDL</td>
			<td>{{regCnt.regContDdl}}</td>
		</tr>
		
		<tr ng-show="regCnt.regContAdditionalRate">
			<td>Additional Rate</td>
			<td>{{regCnt.regContAdditionalRate}}</td>
		</tr>
		
		<!-- <tr>
			<td>Product Type</td>
			<td>{{regCnt.regContProductType}}</td>
		</tr> -->
		
		<tr ng-show="regCnt.regContLoad">
			<td>Load</td>
			<td>{{regCnt.regContLoad}}</td>
		</tr>
		
		<tr ng-show="regCnt.regContUnLoad">
			<td>Unload</td>
			<td>{{regCnt.regContUnLoad}}</td>
		</tr>
		
		<tr ng-show="regCnt.regContTransitDay">
			<td>Transit Day</td>
			<td>{{regCnt.regContTransitDay}}</td>
		</tr>
		
		<tr ng-show="regCnt.regContStatisticalCharge">
			<td>Statistical Charge</td>
			<td>{{regCnt.regContStatisticalCharge}}</td>
		</tr>
		
		<tr>
			<td>Value</td>
			<td>{{regCnt.regContValue}}</td>
		</tr>
		
		<tr>
			<td>Renew</td>
			<td>{{regCnt.regContRenew}}</td>
		</tr>
		
		<tr>
			<td>Renew Date</td>
			<td>{{regCnt.regContRenewDt}}</td>
		</tr>
		
		<tr ng-show="regCnt.regContInsuredBy">
			<td>Insure By</td>
			<td>{{regCnt.regContInsuredBy}}</td>
		</tr>
		
		<tr ng-show="regCnt.regContInsureComp">
			<td>Insure Company</td>
			<td>{{regCnt.regContInsureComp}}</td>
		</tr>
		
		<tr ng-show="regCnt.regContInsureUnitNo">
			<td>Insure Unit Number</td>
			<td>{{regCnt.regContInsureUnitNo}}</td>
		</tr>
		
		<tr ng-show="regCnt.regContInsurePolicyNo">
			<td>Insure Policy Number</td>
			<td>{{regCnt.regContInsurePolicyNo}}</td>
		</tr>
		
		<tr ng-show="regCnt.regContRemark">
			<td>Remarks</td>
			<td>{{regCnt.regContRemark}}</td>
		</tr>
	</table>
	
		<div class="row"  ng-show="rbkmFlag" style="margin-bottom: 50px;">
	<div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
	<div class="col-lg-10 col-sm-12 col-xs-12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3); overflow:auto;" >		
 		<table>
		    <thead>
		     <tr><td><h5>RBKM</h5></td></tr>
		        <tr>
		            <th>From Station</th>
		            <th>To Station</th>
		            <th>State Code</th>
		            <th>From Km</th>
		            <th>To Km</th>
		            <th>Vehicle Type</th>
		            <th>Load</th>
		            <th>UnLoad</th>
		            <th>Transit Day</th>
		            <th>ST Chg</th>
		            <th>Rate</th>   
		        </tr>
		    </thead>
		    <tbody>
		        <tr ng-repeat="rbkm in rbkmList">
		            <td>{{rbkm.rbkmFromStation}}</td>
		            <td>{{rbkm.rbkmToStation}}</td>
		            <td>{{rbkm.rbkmStateCode}}</td>
		            <td>{{rbkm.rbkmFromKm}}</td>
		            <td>{{rbkm.rbkmToKm}}</td>
		            <td>{{rbkm.rbkmVehicleType}}</td>
		            <td>{{rbkm.rbkmLoad}}</td>
		            <td>{{rbkm.rbkmUnLoad}}</td>
		            <td>{{rbkm.rbkmTransitDay}}</td>
		            <td>{{rbkm.rbkmStatChg}}</td>
		            <td>{{rbkm.rbkmRate}}</td>
		        </tr>
		    </tbody>
		</table>
		</div>
	</div>
		
	
			<div class="row" ng-show="pbdFlag" style="margin-bottom: 50px;">
			<div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
			<div class="col-lg-10 col-sm-12 col-xs-12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3); overflow:auto;" >		
 		<table>
 			<caption>PBD</caption>
		    <thead>
		    	 <tr><td><h5>PBD</h5></td></tr>
		        <tr>
		            <th>From Station</th>
		            <th>To Station</th>
		            <th>Start Date</th>
		            <th>End Date</th>
		            <th>From Dlay</th>
		            <th>To Dlay</th>
		            <th>Vehicle Type</th>
		            <th>P/B/D</th>
		            <th>Hour N Day</th>
		            <th>Amount</th>
		        </tr>
		    </thead>
		    <tbody>
		        <tr ng-repeat="pbd in pbdList">
		            
		            <td>{{pbd.pbdFromStn}}</td>
		            <td>{{pbd.pbdToStn}}</td>
		            <td>{{pbd.pbdStartDt}}</td>
		            <td>{{pbd.pbdEndDt}}</td>
		            <td>{{pbd.pbdFromDay}}</td>
		            <td>{{pbd.pbdToDay}}</td>
		            <td>{{pbd.pbdVehicleType}}</td>
		            <td>{{pbd.pbdPenBonDet}}</td>
		            <td>{{pbd.pbdHourNDay}}</td>
		            <td>{{pbd.pbdAmt}}</td>
		        </tr>
		    </tbody>
		</table>
		</div>
		</div>
		
		 <div class="row" ng-show="ctsFlagW" style="margin-bottom: 50px;">
		<div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
		<div class="col-lg-10 col-sm-12 col-xs-12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3);" >		
 		<table>
 			
		    <thead>
		        <tr>
		            <th>From Station</th>
		            <th>To Station</th>
		            <th>Vehicle Type</th>
		            <th>From Weight</th>
		            <th>To Weight</th>
		            <th>Load</th>
		            <th>UnLoad</th>
		            <th>Transit Day</th>
		            <th>ST Chg</th>
		            <th>Rate</th>
		            <th>Additional Rate</th>		            
		        </tr>
		    </thead>
		    <tbody>
		        <tr ng-repeat="cts in ctsList">
					<td>{{regCnt.regContFromStation}}</td>
				    <td>{{cts.ctsToStn}}</td>
				    <td>{{cts.ctsVehicleType}}</td>
				    <td>{{cts.ctsFromWt}}</td>
		            <td>{{cts.ctsToWt}}</td>
		            <td>{{cts.ctsToLoad}}</td>
		            <td>{{cts.ctsToUnLoad}}</td>
		            <td>{{cts.ctsToTransitDay}}</td>
		            <td>{{cts.ctsToStatChg}}</td>
		          	<td>{{cts.ctsRate}}</td>
		            <td>{{cts.ctsAdditionalRate}}</td>
		        </tr>
		    </tbody>
		</table>
		</div>
		</div>
   		
   		<div class="row" ng-show="ctsFlagQ" style="margin-bottom: 50px;">
		<div class="col-lg-1 hidden-xs hidden-sm hidden-md "> &nbsp; </div>
		<div class="col-lg-10 col-sm-12 col-xs-12 card" style="align: center; padding-top:40px; background-color: rgba(125, 125, 125, 0.3);" >		
 		<table>
		    <thead>
		        <tr>
		            <th>From Station</th>
		            <th>To Station</th>
		            <th>Vehicle Type</th>
		            <th>From Weight</th>
		            <th>To Weight</th>
		            <th>Load</th>
		            <th>UnLoad</th>
		            <th>Transit Day</th>
		            <th>ST Chg</th>
		            <th>Product Type</th>
		            <th>Rate</th>		            
		        </tr>
		    </thead>
		    <tbody>
		        <tr ng-repeat="cts in ctsList">
					<td>{{regCnt.regContFromStation}}</td>
				    <td>{{cts.ctsToStn}}</td>
				    <td>{{cts.ctsVehicleType}}</td>
				    <td>{{cts.ctsFromWt}}</td>
		            <td>{{cts.ctsToWt}}</td>
		            <td>{{cts.ctsToLoad}}</td>
		            <td>{{cts.ctsToUnLoad}}</td>
		            <td>{{cts.ctsToTransitDay}}</td>
		            <td>{{cts.ctsToStatChg}}</td>
		            <td>{{cts.ctsProductType}}</td>
		          	<td>{{cts.ctsRate}}</td>
		           
		        </tr>
		    </tbody>
		</table>
		</div>
		</div>
		
		<input type="button" value="Cancel" ng-click="back()">
		<input type="button" value="Save" id="saveBtnId" ng-click="saveContract(regCnt,tnc,regContRatePer,metricType)"> 
		
	</div>
	</div>
	</div>