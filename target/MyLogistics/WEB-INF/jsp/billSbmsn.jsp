<div ng-show="operatorLogin || superAdminLogin">
	<div class="row">
		<form name="billFrwdForm" ng-submit=billFrwdSubmit(billFrwdForm) class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
			
			
			
			<div class="row">
					<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="custId" name ="custName" ng-model="customer.custName" ng-click="selectCust()" ng-required="true" readonly>
		       			<label for="code">Customer Name</label>	
		       		</div>
		       		
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="bfId" name ="bfName" ng-model="selBillFrwd.bfNo" ng-required="true" ng-click="selBillFr()" readonly>
		       			<label for="code">Bill Forwarding No</label>	
		       		</div>
		       		
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="date" id="recDtId" name ="recDtName" ng-model="selBillFrwd.bfRecDt" ng-required="true">
		       			<label for="code">Receiving Date</label>	
		       		</div>
		    </div>
		    
		    <div class="row" ng-show="uploadFlag">
					<div class="input-field col s9">
						<input type="file" class="teal white-text btn" file-model="bfImage">
						<label>Choose File for {{ selBillFrwd.bfNo }}</label>
					</div>
					<div class="col s3">
						<a class="btn waves-effect waves-light white-text col s12"
							type="button" ng-click="uploadBfImage(bfImage)">Upload</a>
					</div>
				</div>
		    
		    <div ng-show="billList.length > 0">
		    	<div class="row">
		    		<table class="tblrow">
					<caption class="coltag tblrow">Bill List</caption>
						<tr class="rowclr">
							<th class="colclr">Bill No</th>
							<th class="colclr">Bill Date</th>
							<th class="colclr">Amount</th>
						</tr>
						<tr class="tbl" ng-repeat="bil in billList">
							<td class="rowcel">{{bil.blBillNo}}</td>
							<td class="rowcel">{{bil.blBillDt | date : format : timezone}}</td>
							<td class="rowcel">{{bil.blFinalTot}}</td>
						</tr>
					</table>	
		    	</div>
		    </div>
		    
		    <div class="row">
	     		<div class="input-field col s12 center">
	       				<input class="validate" type ="submit" id="saveId" value="submit" >
	       		</div>
	        </div>
		    
		</form>
	</div>
	
	
	<div id ="selCustId" ng-hide="selCustFlag">
		  <input type="text" name="filterCust" ng-model="filterCust" placeholder="Search....">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Customer Name</th>
 	  	  	  <th>FaCode</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="cust in custList | filter:filterCust">
		 	  <td><input type="radio"  name="cust"   value="{{ cust }}" ng-model="custCode" ng-click="saveCustomer(cust)"></td>
              <td>{{cust.custName}}</td>
              <td>{{cust.custFaCode}}</td>
          </tr>
      </table> 
	</div>
	
	
	<div id ="selBFrwdId" ng-hide="selBFrwdFlag">
		  <input type="text" name="filterBF" ng-model="filterBf" placeholder="Search....">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Bill Forwarding No</th>
 	  	  	  <th>Amount</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="bfwrd in bfList | filter:filterBF">
		 	  <td><input type="radio"  name="bfwrd"   value="{{ bfwrd }}" ng-model="bfwrdCode" ng-click="saveBFwrd(bfwrd)"></td>
              <td>{{bfwrd.bfNo}}</td>
              <td>{{bfwrd.bfTotAmt}}</td>
          </tr>
      </table> 
	</div>
</div>