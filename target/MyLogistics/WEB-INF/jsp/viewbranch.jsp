<div ng-show="operatorLogin || superAdminLogin">
<title>View Branch</title>
<div class="row">
	<div class="col s3 hide-on-med-and-down"> &nbsp; </div>
		<form name="BranchForm" class="col s6 card" style="align: center; margin-top: 40px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" ng-submit="Submit(BranchForm,br)">
			<div class="input-field col s6">
				<input type="text" name="branchName" ng-model="br.branchName" required readonly ng-click="openBranchCodeDB()" />
   				<input type="hidden" name="branchCode" ng-model="br.branchCode" />
   				<label>Enter Branch Code</label>
 			</div>
      		<div class="input-field col s6 center">
      			<input class="btn waves-effect waves-light" type="submit" value="Submit">
      		</div>
		</form>
		<div class="col s3"> &nbsp; </div>
</div>

<div id ="branchCodeDB" ng-hide="branchCodeDBFlag">
		 <input type="text" name="filterTextbox" ng-model="filterTextbox.branchCodeTemp" placeholder="Search by Branch Code Temp">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	 <!--  <th> Branch Code </th> -->
 	  	  	  
 	  	  	  <th> Branch Name </th>
 	  	  	  
 	  	  	  <th> Branch FaCode</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="branch in branchCodeList | filter:filterTextbox ">
		 	  <td><input type="radio"  name="branchName"  class="branchCls"  value="{{ branch }}" ng-model="brCode" ng-click="saveBranchCode(branch)"></td>
              <!-- <td>{{branch.branchCode}}</td> -->
              <td>{{branch.branchName}}</td>
              <td>{{branch.branchFaCode}}</td>
          </tr>
      </table>
	</div>


<!-- <form ng-submit="Submit(branchCode)" >
		<div id ="branchCodeDiv">
			
				<table>
				
					<tr>
						<td>Enter Branch Code</td>
						<td><input type="text" name="branchCode" ng-model="branchCode"/></td>
					</tr>
					<tr>
						<td><input type="submit" value="Submit"/></td>
					</tr>
				</table>
				
			
		</div>
</form> -->
	
	<div class="row" ng-hide = "showBranchDetails" >
	<div class="col s3 hidden-xs hidden-sm"> &nbsp; </div>
	<div class="col s6 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >
		<h4>Here's the details of <span class="teal-text text-lighten-2">{{branch.branchName}}</span> branch:</h4>
		<table class="table-hover table-bordered table-condensed">
				<tr>
	                <td>Name:</td>
	                <td>{{branch.branchName}}</td>
	            </tr>
	           
	            <tr>
	                <td>Code:</td>
	                <td>{{branch.branchCode}}</td>
	            </tr>
	            
	            <tr>
	                <td>IsNCR:</td>
	                <td>{{branch.isNCR}}</td>
	            </tr>
	            
	            <tr>
	                <td>Address:</td>
	                <td>{{branch.branchAdd}}</td>
	            </tr>
	            <tr>
	                <td>City:</td>
	                <td>{{branch.branchCity}}</td>
	            </tr>
	            <tr>
	                <td>State:</td>
	                <td>{{branch.branchState}}</td>
	            </tr>
	            <tr>
	                <td>Pin:</td>
	                <td>{{branch.branchPin}}</td>
	            </tr>
	            <tr>
	                <td>Open Date:</td>
	                <td>{{branch.branchOpenDt}}</td>
	            </tr>
	            <tr>
	                <td>Station Code:</td>
	                <td>{{branch.branchStationCode}}</td>
	            </tr>
	            <tr>
	                <td>Director:</td>
	                <td>{{branch.branchDirector}}</td>
	                
	            </tr>
	            <tr>
	                <td>Marketing HD:</td>
	                <td>{{branch.branchMarketingHD}}</td>
	            </tr>
	            <tr>
	                <td>Outstanding HD:</td>
	                <td>{{branch.branchOutStandingHD}}</td>
	            </tr>
	            <tr>
	                <td>Marketing:</td>
	                <td>{{branch.branchMarketing}}</td>
	            </tr>
	            <tr>
	                <td>Manager:</td>
	                <td>{{branch.branchMngr}}</td>
	            </tr>
	            <tr>
	                <td>Cashier:</td>
	                <td>{{branch.branchCashier}}</td>
	            </tr>
	            <tr>
	                <td>Traffic:</td>
	                <td>{{branch.branchTraffic}}</td>
	            </tr>
	            <tr>
	                <td>Area Manager:</td>
	                <td>{{branch.branchAreaMngr}}</td>
	            </tr>
	            <tr>
	                <td>Regional Manager:</td>
	                <td>{{branch.branchRegionalMngr}}</td>
	            </tr>
	            <tr>
	                <td>Phone:</td>
	                <td>{{branch.branchPhone}}</td>
	            </tr>
	            <tr>
	                <td>Fax:</td>
	                <td>{{branch.branchFax}}</td>
	            </tr>
	            
	            <tr>
	                <td>Email Id:</td>
	                <td>{{branch.branchEmailId}}</td>
	            </tr>
	            
	            <tr>
	                <td>Website:</td>
	                <td>{{branch.branchWebsite}}</td>
	            </tr>
	            
	            <tr>
	                <td>IsOpen:</td>
	                <td>{{branch.isOpen}}</td>
	            </tr>
	            
	            <tr>
	                <td>Closing Date and Time:</td>
	                <td>{{branch.branchCloseDt}}</td>
	            </tr>
	            
	            <tr>
	                <td>Creation Timestamp:</td>
	                <td>{{branch.creationTS}}</td>
	            </tr>
	  </table>
	   
	   
       
	   <div class="row">
			<table class="tblrow">
	       		
	       		<!-- <caption class="coltag tblrow" ><a href="" ng-click="getChildBankMstrList()">Bank Assigned to this Branch</a></caption> -->
	       		<!-- <tr>
	       			<td colspan="3">
	       				<input class="btn waves-effect waves-light" type="button" value="view" ng-click="getChildBankMstrList()">	
	       			</td>
	       		</tr> -->
	       		
	       		<!-- <tr>
	                <td>
	                	<caption  style=width:100%; class="btn waves-effect waves-light"  value="view" ng-click="getChildBankMstrList()" >Bank Assigned to this Branch</caption>
	                </td>
                </tr> --> 
	       		
	       		<caption><a href="" id="ankr" class="btn btn-danger1" ng-click="getChildBankMstrList()">Bank Assigned to this Branch</a></caption> 
	       		
	            <tr class="rowclr" ng-if="showTableFlag">
		            <th class="colclr">SNo</th>
		            <th class="colclr">Name</th>
		            <th class="colclr">FaCode</th>
		        </tr>	
		         
		        <tr ng-repeat="bankMstr in bankMstrList">
		             <td class="rowcel">{{$index+1}}</td>
		            <td class="rowcel">{{bankMstr.bnkName}}</td>
		            <td class="rowcel">{{bankMstr.bnkFaCode}}</td>
		        </tr>
	         
		
	    	</table>
	          
		</div>
		
		
</div>	
</div>
</div>