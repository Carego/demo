<div ng-show="operatorLogin || superAdminLogin">
	
	<div class="row" class="col s12 card" style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
		
		<div class="col s4 input-field">
			<input class="validate" type="radio" id="byBranchID" name="receiveRBG" ng-click="byBranchRB()" > 
			<label for="code">By Branch</label>
		</div>
		
		<div class="col s4 input-field">
			<input class="validate" type="radio" id="byBankID" name="receiveRBG" ng-click="byBankRB()"> 
			<label for="code">By Bank</label>
		</div>
		
		<!-- <div class="col s4 input-field">
			<input class="validate" type="radio" id="byRefNoID"	name="receiveRBG" ng-click="byRefNoRB()" > 
			<label for="code">By Reference No</label>
		</div> -->
	</div>
	
	<!--By Branch  -->
	<div class="row" ng-show="byBranchRBFlag">
		<form name="byBranchForm" ng-submit=submitByBranch(byBranchForm) class="col s12 card" style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
				
				<div class="col s4 input-field">
		       		<input class="validate" type ="text" id="branchId" name ="branchName" ng-model="branch.branchName" ng-click="openBranchDB()" readonly ng-required="true" >
		       	<label for="code">Branch</label>	
		       	</div>
		       		
		       	<div class="col s4 input-field">
		       		<input class="validate" type ="text" id="bankMstrId" name ="bankMstrName" value="bank" ng-model="bankMstr.bnkName" ng-click="openBankDB()" disabled="disabled" readonly ng-required="true">
		       	<label for="code">Bank</label>	
		       	</div>
				
				<div class="col s4 input-field">
					<input class="validate" type="submit" value="Submit">
				</div>
			</div>
		</form>
		
	</div>
	
	<!-- By Bank -->
	<div class="row" ng-show="byBankRBFlag">
		<form name="byBankForm" ng-submit=submitByBank(byBankForm) class="col s12 card" style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
				<div class="col s4 input-field">
		       		<input class="validate" type ="text" id="bMstrByBnk" name ="bMstrByBnkName" value="bank" ng-model="bMstrByBnk.bnkName" ng-click="openBankByBnkDB()" readonly ng-required="true">
		       	<label for="code">Bank</label>	
		       	</div>

				<div class="col s4 input-field">
					<input class="validate" type="submit" value="submit">
				</div>
			</div>
		</form>
		
	</div>
	
	<!-- div for Cheque Request data -->
	
	<div class="row" ng-show="showChqReqFlag" class="col s12 card" style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
			
			<div class="row">
				<div class="col s4 input-field">
					<input class="validate" type="text" id="cReqNOBookId" name="cReqNOBookName" ng-model="chqReq.cReqNOBook" readonly> 
					<label for="code">No of Book</label>
				</div>

				<div class="col s4 input-field">
					<input class="validate" type="text" id="cReqNOChqPerBookId" name="cReqNOChqPerBookName" ng-model="chqReq.cReqNOChqPerBook" readonly> 
					<label for="code">No of chq per book</label>
				</div>
				
				<div class="col s2 input-field">
					<div>
						<input class="validate" type="text" id="cReqTypeId" name="cReqTypeName" ng-model="chqReq.cReqType" readonly> 
						<label for="code">Chq Type</label>
					</div>
				</div>
				
				<div class="col s2 input-field">
					<input class="validate" type="text" id="cReqPrintTypeId" name="cReqPrintTypeName" ng-model="chqReq.cReqPrintType" readonly> 
					<label for="code">Print Type</label>
				</div>
			</div>
			
			<div class="row">
				<div class="col s4 input-field">
					<input class="validate" type="text" id="cReqRefNoId" name="cReqRefNoName" ng-model="chqReq.cReqRefNo" readonly> 
					<label for="code">Reference No</label>
				</div>

				<div class="col s4 input-field">
					<input class="validate" type="text" id="cReqDtId" name="cReqDtName" ng-model="chqReq.cReqDt" readonly> 
					<label for="code">Date</label>
				</div>
				
				<div class="col s4 input-field">
					<input class="validate" type="button" value="Enter Chq" ng-click="enterChq()">
				</div>
				
			</div>
	</div>
	
	<!-- By Reference no -->
	<!-- <div class="row" ng-show="byRefNoRBFlag">
		<form name="assignBankForm" ng-submit=assignBank(assignBankForm) class="col s12 card" style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
				<div class="col s4 input-field">
					<input class="validate" type="text" id="selectBnkId" name="selectBnk" ng-model="bankMstr.bnkName" ng-click="openBankDB()" readonly ng-required="true"> 
					<label for="code">Select Ref</label>
				</div>

				<div class="col s4 input-field">
					<input class="validate" type="text" id="selectBranchId"	name="selectBranch" ng-model="branch.branchName" ng-click="openBranchDB()" disabled="disabled" readonly ng-required="true"> 
					<label for="code">Select Branch</label>
				</div>
				
				<div class="col s4 input-field">
					<input class="validate" type="submit" value="Assign">
				</div>
			</div>
		</form>
		
	</div> -->
	
	<div id ="branchDB" ng-hide="branchDBFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search by branch Name ">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Name </th>
 	  	  	  <th> FaCode </th>
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="branch in branchList | filter:filterTextbox">
		 	  <td><input type="radio"  name="branch" value="{{ branch }}" ng-model="brnch" ng-click="saveBranch(branch)"></td>
              <td>{{branch.branchName}}</td>
              <td>{{branch.branchFaCode}}</td>
              
          </tr>
      </table> 
	</div>
	
	<div id ="bankMstrDB" ng-hide="bankMstrDBFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Name </th>
 	  	  	  <th> FaCode </th>
 	  	  	  <th> A/c No </th>
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="bankMstr in bankMstrList | filter:filterTextbox ">
		 	  <td><input type="radio"  name="bankMstr" value="{{ bankMstr }}" ng-model="bnkMstr" ng-click="saveBank(bankMstr)"></td>
              <td>{{bankMstr.bnkName}}</td>
              <td>{{bankMstr.bnkFaCode}}</td>
              <td>{{bankMstr.bnkAcNo}}</td>
          </tr>
      </table>  
	</div>
	
	<div id ="bankByBnkDB" ng-hide="bankByBnkDBFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Branch Name </th>
 	  	  	  <th> Branch FaCode </th>
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="bankMstr in bMstrListByBnk | filter:filterTextbox ">
		 	  <td><input type="radio"  name="bankMstr" value="{{ bankMstr }}" ng-model="bnkMstr" ng-click="saveBankByBnk(bankMstr)"></td>
              <td>{{bankMstr.bnkName}}</td>
              <td>{{bankMstr.bnkFaCode}}</td>
              
          </tr>
      </table> 
	</div>
	
	
	
	<div id ="chqReqDB" ng-hide="chqReqDBFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Reference no </th>
 	  	  	  <th> Date </th>
 	  	  	  <th> No Of Books </th>
 	  	  	  <th> No Of Chq per Book </th>
 	  	  	  
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="chqReq in chqReqList | filter:filterTextbox ">
		 	  <td><input type="radio"  name="chqReq" value="{{ chqReq }}" ng-model="chqReq" ng-click="selectChqReq(chqReq)"></td>
              <td>{{chqReq.cReqRefNo}}</td>
              <td>{{chqReq.cReqDt}}</td>
              <td>{{chqReq.cReqNOBook}}</td>
              <td>{{chqReq.cReqNOChqPerBook}}</td>
              
          </tr>
      </table> 
	</div>
	
		
	<div id ="enterChqDB" ng-hide="enterChqDBFlag" >
			
		<form name="enterChqForm" ng-submit=okEnterChq(enterChqForm) class="col s12 card" style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">		
			<div class="row">
				
				<div class="col s6 input-field">
		       		<input class="validate" type ="text" id="sNoId" name ="sNoName" ng-model="sNo" readonly>
		       	<label for="code">S.No.</label>	
		       	</div>
		       		
		       	<div class="col s6 input-field">
		       		<input class="validate" type ="text" id="totalChqId" name ="totalChqName" ng-model="totalChq" readonly >
		       	<label for="code">Total Cheques</label>	
		       	</div>
				
			</div>
			
			<div class="row">
				
				<div class="col s6 input-field">
		       		<input class="validate" type ="text" id="chqBkFromChqNoId" name ="chqBkFromChqNoName" ng-model="chequeBook.chqBkFromChqNo" ng-blur="fromChqNo()" ng-required="true" >
		       	<label for="code">From Cheque No</label>	
		       	</div>
		       		
		       	<div class="col s6 input-field">
		       		<input class="validate" type ="text" id="chqBkToChqNoId" name ="chqBkToChqNoName" ng-model="chequeBook.chqBkToChqNo" ng-blur="toChqNo()" ng-required="true">
		       	<label for="code">To cheque no</label>	
		       	</div>
				
			</div>
			
			<div class="row">
				
				<div class="col s6 input-field">
		       		<input class="validate" type ="submit" value="Ok" id="okEnterChqId" name ="okEnterChqName" >
		       	</div>
		       		
		       	<div class="col s6 input-field">
		       		<input class="validate" type ="button" value="Close" id="closeEnterChqBtnId" name ="closeEnterChqBtnName" ng-click="closeEnterChqBtn()" >
		       	</div> 
				
			</div>
		</form>	
	</div>
		
	<div class="row" ng-if="showTableFlag && chequeBookList.length>0">
		<form class="col s12 card" style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">	
			
			<div class="row">
				<table class="tblrow">
		       		
		       		<caption class="coltag tblrow">Cheque book entry</caption> 
		       		
		            <tr class="rowclr">
			            <th class="colclr">SNo</th>
			            <th class="colclr">From Chq No</th>
			            <th class="colclr">To Chq No</th>
			            <th class="colclr">No of Chq</th>
			            <th class="colclr">Action</th>
			        </tr>	
			         
			        <tr ng-repeat="chqBook in chequeBookList track by $index">
			            <td class="rowcel">{{$index+1}}</td>
			            <td class="rowcel">{{chqBook.chqBkFromChqNo}}</td>
			            <td class="rowcel">{{chqBook.chqBkToChqNo}}</td>
			            <td class="rowcel">{{chqBook.chqBkNOChq}}</td>
			            <td class="rowcel"><input type="button" value="Remove" ng-click="removeChqBook(chqBook, $index)"/></td>
			        </tr>
		         
		        </table>
	    	</div>
	    	
	    	<div class="row">
					
				<div class="input-field col s12 center" >
		       		<input type="button" value="Submit" ng-click="submitChequeBook()"/>
		       	</div> 
				
			</div>
	    	
	    </form>
	          
	</div>
	
	
</div>
