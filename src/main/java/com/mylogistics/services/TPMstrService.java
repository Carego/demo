package com.mylogistics.services;

import org.springframework.stereotype.Service;

import com.mylogistics.model.Address;
import com.mylogistics.model.Branch;
import com.mylogistics.model.Employee;
import com.mylogistics.model.TelephoneMstr;

@Service
public class TPMstrService {

	private TelephoneMstr telephoneMstr;
	
	private Branch branch;
	
	private Employee employee;
	
	private Address address;
	
	

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public TelephoneMstr getTelephoneMstr() {
		return telephoneMstr;
	}

	public void setTelephoneMstr(TelephoneMstr telephoneMstr) {
		this.telephoneMstr = telephoneMstr;
	}

	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	
	
}
