package com.mylogistics.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name ="hiladdress")
public class HilAddress {
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="custName")
	private String custName;
	
	@Column(name="stateCode")
	private String stateCode;
	
	@Column(name="stateGST")
	private String stateGST;
	
	@Column(name="gstNo")
	private String gstNo;
	
	@Column(name="phoneNo")
	private String phoneNo;
	
	@Column(name="custCity")
	private String custCity;
	
	@Column(name="custPin")
	private Integer custPin;
	
	@Column(name="custAdd")
	private String custAdd;
	
	@Column(name="cmpleteAddress")
	private String cmpleteAddress;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public String getStateGST() {
		return stateGST;
	}

	public void setStateGST(String stateGST) {
		this.stateGST = stateGST;
	}

	public String getGstNo() {
		return gstNo;
	}

	public void setGstNo(String gstNo) {
		this.gstNo = gstNo;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getCustCity() {
		return custCity;
	}

	public void setCustCity(String custCity) {
		this.custCity = custCity;
	}

	public Integer getCustPin() {
		return custPin;
	}

	public void setCustPin(Integer custPin) {
		this.custPin = custPin;
	}

	public String getCustAdd() {
		return custAdd;
	}

	public void setCustAdd(String custAdd) {
		this.custAdd = custAdd;
	}

	public String getCmpleteAddress() {
		return cmpleteAddress;
	}

	public void setCmpleteAddress(String cmpleteAddress) {
		this.cmpleteAddress = cmpleteAddress;
	}
	
	
	
	
}
