'use strict';

var app = angular.module('application');

app.controller('ChqPayVoucherBackCntlr',['$scope','$location','$http','$filter',
                                 function($scope,$location,$http,$filter){
	
	$scope.vs = {};
	
	$scope.actFaMList = [];
	$scope.chqList = [];
	//$scope.custBrFaList = []; 
	$scope.brFaList = [];
	$scope.faName = "";
	$scope.chqNo = "";
	
	$scope.amtSum=0;
	$scope.bankCodeDBFlag = true;
	$scope.faCodeDBFlag = true;
	$scope.tdsCodeFlag = true;
	$scope.chqVoucherDBFlag = true;
	$scope.saveVsFlag = true;
	$scope.custBrDBFlag = true;
	$scope.chqNoDBFlag = true;
	$scope.loadingFlag = false;
	
	var max = 15;
	
	$('#amtId').keypress(function(key) {
        if(key.charCode < 46 || key.charCode > 57)
        	return false;
    });
	
	 $('#amtId').keypress(function(e) {
     	if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == max)){
             e.preventDefault();
         } 
     });
	
	
	
	
	$scope.getVoucherDetails = function(){
		console.log("Enter into getDetails");
		var response = $http.post($scope.projectName+'/getVDetFrCPVBack');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.vs.branch = data.branch;
				$scope.bankCodeList = data.bankCodeList;
				$scope.vs.cashStmtStatus = data.cashStmtStatus;
				$scope.vs.voucherType = "Cheque Payment";
				$scope.dateTemp = $filter('date')(data.cashStmtStatus.cssDt, "yyyy-MM-dd'");				
			}else{
				console.log("Error in bringing data");
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
	
	
	$scope.getAllFaCode = function(){
		console.log("enter into getAllFaCode function");
		$scope.loadingFlag = true;
		var response = $http.post($scope.projectName+'/getAllFaCodeFrCPVByFaName', $scope.faName);
		response.success(function(data, status, headers, config){
			console.log(data);
			if(data.result === "success"){
				$scope.loadingFlag = false;
				$scope.faMList = data.faMList;
				$scope.alertToast("FaMaster Size = "+$scope.faMList.length);
			}else{
				$scope.loadingFlag = false;
				console.log("Error in bringing FaCode");
			}
		});
		response.error(function(data, status, headers, config){
			$scope.loadingFlag = false;
			console.log(data);
		});
	}
	
	$scope.selectPayBy = function(){
		console.log("enter into selectPayMod function with vs.payBy ="+$scope.vs.payBy);
		if($scope.vs.payBy === 'Q'){
			$('#chequeTypeId').removeAttr("disabled");
		}else if($scope.vs.payBy === 'O'){
			$('#bankCodeId').removeAttr("disabled");
			$scope.vs.chequeLeaves = {};
			$('#chequeNoId').attr("disabled","disabled");
			$scope.vs.chequeType = '';
			$('#chequeTypeId').attr("disabled","disabled");
		}else{
			console.log("invaid entry");
		}
	}
	
	$scope.selectChqType = function(){
		console.log("enter into selectChqType function");
		$('#bankCodeId').removeAttr("disabled");
	}
	
	
	$scope.openBankCodeDB = function(){
		console.log("enter into openBankCodeDB function");
		$scope.bankCodeDBFlag = false;
		$('div#bankCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Bank Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		
		$('div#bankCodeDB').dialog('open');	
	}
	
	
	$scope.saveBankCode =  function(bankCode){
		console.log("enter into saveBankCode----->"+bankCode);
		$scope.vs.bankCode = bankCode;
		$('div#bankCodeDB').dialog('close');
		$scope.bankCodeDBFlag = true;
		console.log("$scope.vs.chequeType = "+$scope.vs.chequeType);		
		$('#faCodeId').removeAttr("disabled");		
		$('#chequeNoId').removeAttr("disabled");
	}
	
	
	$scope.getCheque = function(){
		if(angular.isUndefined($scope.vs.chequeType) || $scope.vs.chequeType === null || $scope.vs.chequeType === ''){
			console.log("payment by rtgs");
		}else{
			var chqDet = {
					"bankCode" 	:	$scope.vs.bankCode,
					"CType"    	:	$scope.vs.chequeType,
					"chqNo"		:	$scope.chqNo
			};
			
			var response = $http.post($scope.projectName+'/getChequeNoFrBPV',chqDet);
			response.success(function(data, status, headers, config){
				if(data.result === "success"){	
					
					$scope.chqNoDBFlag = false;
					$('div#chqNoDB').dialog('open');
					
					if(data.list.length != 0){
						console.log("Cheque Leaves List--->>"+ data.chequeLeaves);					
						$('#payToId').removeAttr("disabled");
						$('#faCodeId').removeAttr("disabled");
						$scope.chqList = data.list;			
					}else{
						$scope.alertToast("Wrong cheque type or enter correct cheque no.");
					}						
				}else{
					console.log("Error in bringing data from getChequeNo");
					$scope.alertToast("Wrong cheque type or enter correct cheque no.");
				}

			});
			response.error(function(data, status, headers, config){
				console.log(data);
			});		
		}
	}
	
	$scope.openFACodeDB = function(keyCode){
		if(keyCode === 8)
			return;
		
		var len = $scope.faCode.length;
		if(len < 3)
			return ;
		
		$scope.faName = $scope.faCode;
		$scope.faCode = "";
		
		$scope.getAllFaCode();
		
		$scope.filterTextbox3 = "";
		console.log("enter into openFACodeDB function");
		$scope.faCodeDBFlag = false;
		$('div#faCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "FA Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		
		$('div#faCodeDB').dialog('open');	
	}
	
	
	$scope.saveFACode = function(faMstr){
		console.log("enter into saveFACode function");
		$scope.faCodeDBFlag = true;
		$('div#faCodeDB').dialog('close');
		$('#brId').attr("disabled","disabled"); 
		$scope.faCode = "";
		$scope.amt = "";
		$scope.custBrCode = "";
		$scope.custBrFaList = [];
		$scope.brFaList = [];
		
		var countFANo = 0;
		 for(var i=0;i<$scope.actFaMList.length;i++){
			 if($scope.actFaMList[i].faCode === faMstr.faMfaCode){
				 countFANo = countFANo +1;
			 }
		 }
		
		 if(countFANo > 0){
				$scope.alertToast("you already create the voucher of "+faMstr.faMfaCode);
		 }else{
			     $scope.faCode = faMstr.faMfaCode;
			     
			     var decided = $scope.faCode.substring(0,2);
			     console.log("decided === >" +decided);
			     
			     if(decided === "03"){
			    	var response = $http.post($scope.projectName+'/getCustBrFrCPV',$scope.faCode);
					response.success(function(data, status, headers, config){
						//$scope.custBrFaList = data.list;
						$scope.brFaList = data.list;
						console.log("$scope.brList length = "+$scope.brFaList.length+"  0th value =>"+$scope.brFaList[0]);
						if($scope.brFaList.length > 0){
							//$('#custBrId').removeAttr("disabled");
							$('#brId').removeAttr("disabled");
						}
					});
					response.error(function(data, status, headers, config){
						console.log(data);
					});	
			     }else if(decided === "02"){
				    	var response = $http.post($scope.projectName+'/getEmpBrFrCPV',$scope.faCode);
						response.success(function(data, status, headers, config){
							//$scope.custBrFaList = data.list;
							$scope.brFaList = data.list;
							console.log("$scope.brList length = "+$scope.brFaList.length+"  0th value =>"+$scope.brFaList[0]);
							if($scope.brFaList.length > 0){
								//$('#custBrId').removeAttr("disabled");
								$('#brId').removeAttr("disabled");
							}
						});
						response.error(function(data, status, headers, config){
							console.log(data);
						});	
				 }else if(decided === "01"){
			    	 
			     }else{
			    	 var response = $http.post($scope.projectName+'/getAllBrFrCPV',$scope.faCode);
						response.success(function(data, status, headers, config){
							//$scope.custBrFaList = data.list;
							$scope.brFaList = data.list;
							console.log("$scope.brList length = "+$scope.brFaList.length+"  0th value =>"+$scope.brFaList[0]);
							if($scope.brFaList.length > 0){
								//$('#custBrId').removeAttr("disabled");
								$('#brId').removeAttr("disabled");
							}
						});
						response.error(function(data, status, headers, config){
							console.log(data);
						});	
			     }
			     
			     
				 $scope.chqVoucherDBFlag = false;
				 $('div#chqVoucherDB').dialog({
						autoOpen: false,
						modal:true,
						resizable: false,
						title: "Amount for ("+faMstr.faMfaCode+")" ,
						show: UDShow,
						hide: UDHide,
						position: UDPos,
						draggable: true,
						close: function(event, ui) { 
					        $(this).dialog('destroy');
					        $(this).hide();
					    }
						});
					
				$('div#chqVoucherDB').dialog('open');
		 }		
		
	}
	
	
	$scope.selectBrh = function(){
		console.log("enter into selectBrh function");
		
		
		for(var i=0;i<$scope.brFaList.length;i++){
			console.log("$scope.brList length = "+$scope.brFaList.length+"  0th value =>"+$scope.brFaList[i]);
		}
		
		 $scope.custBrDBFlag = false;
		 $('div#custBrDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				title: "Branch FACode",
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			    }
				});
			
		$('div#custBrDB').dialog('open');
	}
	
	
	$scope.saveBrFA = function(custBrFa){
		console.log("enter into saveBrFA function");
		$scope.custBrDBFlag = true;
		$('div#custBrDB').dialog('close');
		
		$scope.brCode = custBrFa;
	}
	
	
	$scope.submitCPVoucher = function(chqVoucherForm){
		console.log("enter into submitCPVoucher function = "+chqVoucherForm.$invalid);
		if(chqVoucherForm.$invalid){
			$scope.alertToast("please enter correct amount");
		}else{
			console.log("$scope.amt = "+$scope.amt);
		
		    var decided = $scope.faCode.substring(0,2);
		    if(decided === "01"){
		    	
		    	if($scope.amt > 0){
					$scope.chqVoucherDBFlag = true;
					$('div#chqVoucherDB').dialog('close');
					var amount = parseFloat($scope.amt);
					
					var faPay = {
							"faCode" : $scope.faCode,
							"amt"	 : amount
					};
					
					$scope.actFaMList.push(faPay);
				}else{
					$scope.alertToast("please enter correct amount");
				}
		    	
		    }else{
		    	if(angular.isUndefined($scope.brCode) || $scope.brCode === "" || $scope.brCode === null){
		    		$scope.alertToast("please select branch");
		    	}else{
		    		if($scope.amt > 0){
						$scope.chqVoucherDBFlag = true;
						$('div#chqVoucherDB').dialog('close');
						var amount = parseFloat($scope.amt);
						
						var faPay = {
								"faCode"     : $scope.faCode,
								"amt"	     : amount,
								"custBrCode" : $scope.brCode
						};
						 $scope.amtSum=$scope.amtSum+$scope.amt;
						$scope.actFaMList.push(faPay);
					}else{
						$scope.alertToast("please enter correct amount");
					}
		    	}
		    }
			
			
		}
	}
	
	
	$scope.removeCPV = function(index,actFaM){
		console.log("enter into removeCPV function");
		$scope.amtSum=$scope.amtSum-actFaM.amt;
		$scope.actFaMList.splice(index,1);
	}
	
	
	$scope.getAllTds = function(){
		var response = $http.post($scope.projectName+'/getAllTdsForChqPay');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.faTList = data.faTList;
			}else
				$scope.alertToast("No TDs list fetched !");
		});
		response.error(function(){
			$scope.alertToast("No TDs list fetched !");
		});
	} 
	
	$scope.selectTds = function(){
		
		$scope.getAllTds();
		
		console.log("enter into selectTds function");
		$scope.tdsCodeFlag = false;
		$('div#tdsCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		    }
			});
		$('div#tdsCodeDB').dialog('open');
	}
	
	
	$scope.saveTdsCode = function(fsMTds){
		console.log("enter into saveTdsCode function");
		$scope.vs.tdsCode = fsMTds.faMfaCode;
		$scope.tdsCodeFlag = true;
		$('div#tdsCodeDB').dialog('close');
		
	} 
	
	
	$scope.voucherSubmit = function(newVoucherForm){
		console.log("entre into voucherSubmit function = "+newVoucherForm.$invalid);
		if(newVoucherForm.$invalid){
			$scope.alertToast("please enter correct vouher details") 
		}else{
			console.log("final submittion");
		    $scope.saveVsFlag = false;
	    	$('div#saveVsDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			    }
				});
			$('div#saveVsDB').dialog('open');
		}
	}
	
	
	
	$scope.back = function(){
		console.log("enter into back function");
		$scope.saveVsFlag = true;
		$('div#saveVsDB').dialog('close');
	}
	
	
	$scope.saveVS = function(){
		 console.log("enter into saveVS function");
		 $scope.saveVsFlag = true;
		 $('div#saveVsDB').dialog('close');
		 
		 var cpVouch = {
				 "voucherService" : $scope.vs,
		 		 "faList"         : $scope.actFaMList
		 };
		 $('#saveId').attr("disabled","disabled");
		 
		 var response = $http.post($scope.projectName+'/submitCPVoucher',cpVouch);
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					$scope.alertToast("success");
					$('#saveId').removeAttr("disabled");
					$scope.vs = {};
					$scope.actFaMList = [];
					$scope.chqList = [];
					$scope.getVoucherDetails();
				}else{
					console.log("Error in submitVoucher");
				}
			});
			response.error(function(data, status, headers, config){
				console.log(data);
			});
	 }
	
	
	$scope.openChqNoDB = function(keyCode){	
		
		if(keyCode === 8)
			return;
		
		var len = $scope.vs.chequeLeaves.chqLChqNo.length;
		
		if(len < 3)
			return;
		
		$scope.chqNo = $scope.vs.chequeLeaves.chqLChqNo;
		$scope.vs.chequeLeaves.chqLChqNo = "";
		
		$scope.getCheque();
		
		$scope.vs.chequeLeaves.chqLChqNo = "";
		
		 console.log("enter into openChqNoDB function");
		 
	    	$('div#chqNoDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			        $scope.chqNoDBFlag = true;
			    }
				});			
     }
	
	
	 $scope.saveChqNo = function(chq){
		 console.log("enter into saveChqNo function = "+chq.chqLChqNo);
		 $scope.vs.chequeLeaves = chq;
		 $scope.cNo = $scope.vs.chequeLeaves.chqLChqNo;
		 $('div#chqNoDB').dialog('close');
	 }
	
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		 $scope.getVoucherDetails();		 
	}else if($scope.logoutStatus === true){
		 $location.path("/");
	}else{
		 console.log("****************");
	}
	
}]);