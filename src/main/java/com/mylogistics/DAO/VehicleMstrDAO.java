package com.mylogistics.DAO;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.mylogistics.model.Branch;
import com.mylogistics.model.Employee;
import com.mylogistics.model.SubVehicleMstr;
import com.mylogistics.model.VehicleMstr;

@Repository
public interface VehicleMstrDAO {
	
	public int saveVehicleMstr(Branch branch , Employee employee , VehicleMstr vehicleMstr);
	
	public List<VehicleMstr> getAllVehicleMstr();
	
	public int updateVMBySVM(int vmId , SubVehicleMstr subVehicleMstr);
	
	public List<String> getAllVehNo();
	
	public VehicleMstr getVehByVehNo(String vehNo);
	
	
}
