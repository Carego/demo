package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.MunsianaDAO;
import com.mylogistics.model.lhpv.Munsiana;

public class MunsianaDAOImpl implements MunsianaDAO{

	private SessionFactory sessionFactory;
	private Session session;
	private org.hibernate.Transaction transaction;

	@Autowired
	public MunsianaDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public double getMunsAmt(double adv){
		System.out.println("enter into getMunsAmt function = "+adv);
		double munsAmt = 0.0;
		List<Munsiana> munsList = new ArrayList<>(); 
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr=session.createCriteria(Munsiana.class);
			munsList = cr.list();
			if(!munsList.isEmpty()){
				for(int i=0;i<munsList.size();i++){
					System.out.println(i+" munsList.get().getMunsFrAdv() = "+munsList.get(i).getMunsFrAdv());
					System.out.println(i+" munsList.get().getMunsToAdv()) = "+munsList.get(i).getMunsToAdv());
					/*if(adv >= munsList.get(i).getMunsFrAdv() && adv <= munsList.get(i).getMunsToAdv()){
						System.out.println("****************");
						munsAmt = munsList.get(i).getMunsAmt();
						break;
					}*/if(Math.abs(adv) >= Math.abs(munsList.get(i).getMunsFrAdv()) && Math.abs(adv) <= Math.abs(munsList.get(i).getMunsToAdv())){
						System.out.println("****************");
						munsAmt = munsList.get(i).getMunsAmt();
						break;
					}
				}
			}
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return munsAmt;
	}
}
