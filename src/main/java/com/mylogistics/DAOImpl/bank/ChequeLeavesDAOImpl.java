package com.mylogistics.DAOImpl.bank;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.bank.ChequeLeavesDAO;
import com.mylogistics.constants.BranchCNTS;
import com.mylogistics.constants.CashStmtCNTS;
import com.mylogistics.constants.bank.BankMstrCNTS;
import com.mylogistics.constants.bank.ChequeLeavesCNTS;
import com.mylogistics.controller.bank.BankMstrCntlr;
import com.mylogistics.model.Branch;
import com.mylogistics.model.CashStmt;
import com.mylogistics.model.CashStmtStatus;
import com.mylogistics.model.User;
import com.mylogistics.model.bank.BankMstr;
import com.mylogistics.model.bank.ChequeBook;
import com.mylogistics.model.bank.ChequeLeaves;

public class ChequeLeavesDAOImpl implements ChequeLeavesDAO{
	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;

	@Autowired
	public ChequeLeavesDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Autowired
	private HttpSession httpSession;


	@SuppressWarnings("unchecked")
	@Transactional
	public List<ChequeLeaves> getChqLByChqNo(String chqLChqNo){

		System.out.println("Enter into getChqLByChqNo "+ chqLChqNo);
		List <ChequeLeaves> chequeLeavesList = new ArrayList<>();
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr=session.createCriteria(ChequeLeaves.class);
			cr.add(Restrictions.eq(ChequeLeavesCNTS.CHQ_L_CHQ_NO,chqLChqNo));
			chequeLeavesList = cr.list();
			//System.out.println("Size of List in getChqLByChqNo --->"+cr.list());
			transaction.commit();
			session.flush();
		}
		catch(Exception e){
			e.printStackTrace();
		} 
		session.clear();
		session.clear();
		session.close();
		return chequeLeavesList;
	}

	
	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public int updateChqLeaves(ChequeLeaves chequeLeaves){
		System.out.println("Enter into getChqLByChqNo = "+chequeLeaves.getChqLId());
		List<ChequeLeaves> chqL = new ArrayList<ChequeLeaves>();
		int temp =-1;
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr=session.createCriteria(ChequeLeaves.class);
			cr.add(Restrictions.eq(ChequeLeavesCNTS.CHQ_L_ID,chequeLeaves.getChqLId()));
			chqL = cr.list();
			
			ChequeLeaves chql = chqL.get(0);
			chql.setChqLChqAmt(chequeLeaves.getChqLChqAmt());
			chql.setChqLUsed(chequeLeaves.isChqLUsed());
			chql.setChqLUsedDt(chequeLeaves.getChqLUsedDt());
			
			session.merge(chql);
			
			//BankMstr update start
			BankMstr bank = chql.getBankMstr();
			double balAmt = bank.getBnkBalanceAmt();
			double newBalAmt = balAmt - chequeLeaves.getChqLChqAmt();
			bank.setBnkBalanceAmt(newBalAmt);
			session.merge(bank);
			//BankMstr update end
			
			
			transaction.commit();
			session.flush();
			temp = 1;
		}catch(Exception e){
			e.printStackTrace();
		} 
		session.clear();
		session.close();
		return temp;
	}
	
	
	@Transactional
	@Override
	public int saveChqLeaves(ChequeLeaves chequeLeaves){
		System.out.println("enter into saveChqLeaves function");
		int temp = -1;
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			session.save(chequeLeaves);
			transaction.commit();
			session.flush();
			temp = 1;
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return temp;
	}

	
	@Override
	@Transactional
	public List<ChequeLeaves> getUnUsedChqLeave(BankMstr bankMstr) {
		System.out.println("Enter into getUnUsedChqLeave = ");
		List<ChequeLeaves> chqChequeLeaveList = new ArrayList<>();
		
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr=session.createCriteria(BankMstr.class);
			cr.add(Restrictions.eq(BankMstrCNTS.BNK_ID,bankMstr.getBnkId()));
			BankMstr bMstr = (BankMstr) cr.list().get(0);
			List<ChequeLeaves> chequeLeaveList = bMstr.getChequeLeavesList();
			
			if (!chequeLeaveList.isEmpty()) {
				Collections.sort(chequeLeaveList,
						new Comparator<ChequeLeaves>() {
							@Override
							public int compare(final ChequeLeaves cl1,
									final ChequeLeaves cl2) {
								return cl1.getChqLChqNo().compareTo(
										cl2.getChqLChqNo());
							}
						});
			}
			
			for (ChequeLeaves chqLeave : chequeLeaveList) {
				if (!chqLeave.isChqLUsed() && !chqLeave.isChqLCancel()) {
					chqChequeLeaveList.add(chqLeave);
				}
			}
			
			session.flush();
			
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return chqChequeLeaveList;
	}

	/*@Override
	@Transactional
	public ChequeLeaves getUnUsedChqLeave(BankMstr bankMstr) {
		System.out.println("Enter into getUnUsedChqLeave = ");
		ChequeLeaves chequeLeave = null;
		
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr=session.createCriteria(BankMstr.class);
			cr.add(Restrictions.eq(BankMstrCNTS.BNK_ID,bankMstr.getBnkId()));
			BankMstr bMstr = (BankMstr) cr.list().get(0);
			List<ChequeLeaves> chequeLeaveList = bMstr.getChequeLeavesList();
			
			if (!chequeLeaveList.isEmpty()) {
				Collections.sort(chequeLeaveList,
						new Comparator<ChequeLeaves>() {
							@Override
							public int compare(final ChequeLeaves cl1,
									final ChequeLeaves cl2) {
								return cl1.getChqLChqNo().compareTo(
										cl2.getChqLChqNo());
							}
						});
			}
			
			for (ChequeLeaves chqLeave : chequeLeaveList) {
				if (!chqLeave.isChqLUsed() && !chqLeave.isChqLCancel()) {
					chequeLeave = chqLeave;
					break;
				}
			}
			
			session.flush();
			
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return chequeLeave;
	}*/


	@Override
	public int cancelChqLeave(ChequeLeaves chequeLeaves) {
		
		System.out.println("Enter into cancelChqLeave = ");
		User currentUser = (User) httpSession.getAttribute("currentUser");
		/*java.sql.Date sqlDate = new java.sql.Date(new Date().getTime());*/
		int voucherNo = 0;
		int temp =-1;
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			/*Criteria cr=session.createCriteria(ChequeLeaves.class);
			cr.add(Restrictions.eq(ChequeLeavesCNTS.CHQ_L_ID,chequeLeaves.getChqLId()));
			ChequeLeaves chqLeave = (ChequeLeaves) cr.list().get(0);*/		
			
			
			ChequeLeaves chqLeave = (ChequeLeaves) session.get(ChequeLeaves.class, chequeLeaves.getChqLId());
			// Find CashStmt According to userBranchCode And cssId
			CashStmtStatus cashStmtStatus = (CashStmtStatus) session.createCriteria(CashStmtStatus.class)
					.add(Restrictions.eq(CashStmtCNTS.USER_BRANCH_CODE, chqLeave.getbCode()))
					.setMaxResults(1)
					.addOrder(Order.desc(CashStmtCNTS.CS_CSS_ID))
					.list().get(0);
			// Find bank Mstr
			BankMstr bankMstr = chqLeave.getBankMstr();		
			List<CashStmt> csList = cashStmtStatus.getCashStmtList();
			if(!csList.isEmpty()){
				/*int vNo  = csList.get(csList.size() - 1).getCsVouchNo();
				voucherNo = vNo + 1;*/
				for(int i=0;i<csList.size();i++){
					if(voucherNo < csList.get(i).getCsVouchNo()){
						voucherNo = csList.get(i).getCsVouchNo();
					}
				}
				voucherNo = voucherNo + 1;
			}else{
				voucherNo = 1;
			}
			
			
			chqLeave.setChqLCancel(true);
			chqLeave.setChqLChqCancelDt(chequeLeaves.getChqLChqCancelDt());
			
			
			CashStmt cashStmt = new CashStmt();
			cashStmt.setbCode(chqLeave.getbCode());
			cashStmt.setCsAmt(0);
			cashStmt.setCsChequeType(chequeLeaves.getChqLCType());
			cashStmt.setCsDescription("Single Cheque Cancel");
			cashStmt.setCsDt(cashStmtStatus.getCssDt());
			cashStmt.setCsFaCode(bankMstr.getBnkFaCode());		
			cashStmt.setCsTvNo(chqLeave.getChqLChqNo());			
			cashStmt.setCsType("Cheque Cancel");
			cashStmt.setCsVouchNo(voucherNo);
			cashStmt.setCsVouchType("BANK");
			cashStmt.setUserCode(currentUser.getUserBranchCode());
			cashStmt.setCsNo(cashStmtStatus);
			cashStmt.setPayMode("Q");			
			cashStmtStatus.getCashStmtList().add(cashStmt);	
					
			session.merge(chqLeave);
			session.merge(cashStmtStatus);
			
			transaction.commit();
			session.flush();
			temp = 1;
		}catch(Exception e){
			e.printStackTrace();
		} 
		session.clear();
		session.close();
		return temp;
	}


	@Override
	@Transactional
	public List<ChequeLeaves> getChqLeavesForUpdate(Map<String, Object> chqStatusService) {
		
		List<ChequeLeaves> chequeLeaveList = null;
		
		try {
			
			session = this.sessionFactory.openSession();
			BankMstr bankMstr = (BankMstr) session.get(BankMstr.class, Integer.parseInt(String.valueOf(chqStatusService.get("bnkId"))));
			Hibernate.initialize(bankMstr.getChequeLeavesList());
			chequeLeaveList = bankMstr.getChequeLeavesList();
			session.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return chequeLeaveList;
	}


	@Override
	@Transactional
	public int updateChqMaxLtNType(ChequeLeaves chequeLeaves) {
		int temp = 0;
		
		try {
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			ChequeLeaves chqLeaves = (ChequeLeaves) session.get(ChequeLeaves.class, chequeLeaves.getChqLId());
			//chqLChqType
			chqLeaves.setChqLChqType(chequeLeaves.getChqLChqType());
			chqLeaves.setChqLChqMaxLt(chequeLeaves.getChqLChqMaxLt());
			session.merge(chqLeaves);
			
			transaction.commit();
			session.flush();
			temp = 1;
		} catch (Exception e) {
			e.printStackTrace();
			temp = -1;
		}
		session.clear();
		session.close();
		return temp;
	}


	@Override
	@Transactional
	public int removeChqLeave(String chqLId) {
		int temp = 0;
		
		try {
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			ChequeLeaves chqLeaves = (ChequeLeaves) session.get(ChequeLeaves.class, Integer.parseInt(chqLId));
			
			BankMstr bankMstr = (BankMstr) session.get(BankMstr.class, chqLeaves.getBankMstr().getBnkId());
			Branch branch = (Branch) session.get(Branch.class, chqLeaves.getBranch().getBranchId());
			/*ChequeBook chequeBook = (ChequeBook) session.get(ChequeBook.class, chqLeaves.getChequeBook().getChqBkId());*/
			
			bankMstr.getChequeLeavesList().remove(chqLeaves);
			branch.getChequeLeaveList().remove(chqLeaves);
			chqLeaves.setChequeBook(null);
			
			session.update(bankMstr);
			session.update(branch);
			
			session.delete(chqLeaves);
			
			transaction.commit();
			session.flush();
			temp = 1;
		} catch (Exception e) {
			e.printStackTrace();
			temp = -1;
		}
		session.clear();
		session.close();
		return temp;
	}


	@Override
	@Transactional
	public List<String> getChqNoListByBankId(int bankId) {
		System.out.println("getChqNoListByBankId: "+bankId);
		List<String> chqNoList = new ArrayList<>();
		
		try {
			session = this.sessionFactory.openSession();
			Query query = session.createQuery("SELECT chqLChqNo FROM ChequeLeaves where bankMstr.bnkId = :bankId and chqLCancel = :chqLCancel and chqLUsed = :chqLUsed");
			query.setInteger("bankId", bankId);
			query.setBoolean("chqLCancel", false);
			query.setBoolean("chqLUsed", false);
			chqNoList = query.list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		session.flush();
		session.clear();
		session.close();
		return chqNoList;
	}

	
	@SuppressWarnings("unchecked")
//	@Transactional
	@Override
	public int updateChqLeavesN(Session session, ChequeLeaves chequeLeaves){
		System.out.println("Enter into getChqLByChqNo = "+chequeLeaves.getChqLId());
		List<ChequeLeaves> chqL = new ArrayList<ChequeLeaves>();
		int temp =-1;
//		try{
			Criteria cr=session.createCriteria(ChequeLeaves.class);
			cr.add(Restrictions.eq(ChequeLeavesCNTS.CHQ_L_ID,chequeLeaves.getChqLId()));
			chqL = cr.list();
			
			ChequeLeaves chql = chqL.get(0);
			chql.setChqLChqAmt(chequeLeaves.getChqLChqAmt());
			chql.setChqLUsed(chequeLeaves.isChqLUsed());
			chql.setChqLUsedDt(chequeLeaves.getChqLUsedDt());
			
			session.merge(chql);
			
			//BankMstr update start
			BankMstr bank = chql.getBankMstr();
			double balAmt = bank.getBnkBalanceAmt();
			double newBalAmt = balAmt - chequeLeaves.getChqLChqAmt();
			bank.setBnkBalanceAmt(newBalAmt);
			session.merge(bank);
			//BankMstr update end
			
			temp = 1;
//		}catch(Exception e){
//			e.printStackTrace();
//		} 
		return temp;
	}
	
	
	@SuppressWarnings("unchecked")
//	@Transactional
	@Override
	public int updateChqLeaves(Session session, ChequeLeaves chequeLeaves){
		System.out.println("Enter into getChqLByChqNo = "+chequeLeaves.getChqLId());
		List<ChequeLeaves> chqL = new ArrayList<ChequeLeaves>();
		int temp =-1;
//		try{
			Criteria cr=session.createCriteria(ChequeLeaves.class);
			cr.add(Restrictions.eq(ChequeLeavesCNTS.CHQ_L_ID,chequeLeaves.getChqLId()));
			chqL = cr.list();
			
			ChequeLeaves chql = chqL.get(0);
			chql.setChqLChqAmt(chequeLeaves.getChqLChqAmt());
			chql.setChqLUsed(chequeLeaves.isChqLUsed());
			chql.setChqLUsedDt(chequeLeaves.getChqLUsedDt());
			
			session.merge(chql);
			
			//BankMstr update start
			BankMstr bank = chql.getBankMstr();
			double balAmt = bank.getBnkBalanceAmt();
			double newBalAmt = balAmt - chequeLeaves.getChqLChqAmt();
			bank.setBnkBalanceAmt(newBalAmt);
			session.merge(bank);
			//BankMstr update end
			temp = 1;
//		}catch(Exception e){
//			e.printStackTrace();
//		} 
		return temp;
	}
	
}
