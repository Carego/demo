'use strict';

var app = angular.module('application');

app.controller('DailyContractCntlr',['$scope','$location','$http','$window',
                                 function($scope,$location,$http,$window){
	
	$scope.dlyCnt = {};
	$scope.dlyCont=true;
	$scope.regCont=true;
	$scope.editRegCont = true;
	$scope.editDlyCont = true;
	$scope.branchCodeFlag = true;
	$scope.blpmCodeFlag = true;
	$scope.CngrCodeFlag = true;
	$scope.CrNameFlag = true;
	$scope.FromStationFlag = true;
	$scope.ToStationFlag = true;
	$scope.VehicleTypeFlag = true;
	$scope.ProductTypeFlag = true;
	$scope.RbkmIdFlag = true;
	$scope.AddProductTypeFlag = true;
	$scope.VehicleTypeFlag1 = true;
	$scope.rbkmToStationFlag = true;
	$scope.rbkmFromStationFlag = true;
	$scope.PenaltyFlag = true;
	$scope.BonusFlag = true;
	$scope.DetentionFlag = true;
	$scope.rbkmVehicleTypeFlag = true;
	$scope.rbkmStateCodeFlag = true;
	$scope.PenaltyVehicleTypeFlag = true;
	$scope.PenaltyToStationFlag = true;
	$scope.PenaltyFromStationFlag = true;
	$scope.BonusVehicleTypeFlag = true;
	$scope.BonusToStationFlag = true;
	$scope.BonusFromStationFlag = true;
	$scope.DetentionVehicleTypeFlag = true;
	$scope.DetentionToStationFlag = true;
	$scope.DetentionFromStationFlag = true;
	//$scope.Qflag=true;
	$scope.Wflag=true;
	$scope.Kflag=true;
	$scope.checkUnLoadFlag=true;
	$scope.checkPenaltyFlag=true;
	$scope.checkLoadFlag=true;
	$scope.checkBonusFlag=true;
	$scope.checkDetentionFlag=true;
	$scope.checkTransitFlag=true;
	$scope.checkStatisticalChargeFlag=true;
	$scope.Fflag=true;
	$scope.dlyCnt.dlyContType= false;
	$scope.dlyContFlag = true;
	$scope.type=[];
	$scope.contToStationFlagW = true;
	$scope.contToStationFlagQ = true;
	$scope.ctsflagW=false;
	$scope.ctsflagQ=false;
	$scope.onWQClick=false;
	$scope.onKClick=false;
	$scope.ToStationsDBFlag = true;
	$scope.addStationFlag = true;
	$scope.StateCodeFlag = true;
	$scope.RbkmAddVTFlag = true;
	
	$scope.checkLoadFlagRbkm=true;
	$scope.checkUnLoadFlagRbkm=true;
	$scope.checkTransitFlagRbkm=true;
	$scope.checkStatisticalChargeFlagRbkm=true;
	$scope.showFaCode = true;
	
	$scope.station={};
	$scope.rbkm = {};
	$scope.pt = {};
	$scope.vt = {};
	$scope.pbdP = {};
	$scope.pbdB = {};
	$scope.pbdD = {};
	$scope.tnc={};
	$scope.cts={};
	$scope.stationList = [];
	$scope.contToStnTemp = [];
	$scope.rbkmStnList=[];
	$scope.pbdStnList=[];
	$scope.tempStList = [];

	    	$('#dlyContUnLoad').keypress(function(key) {
	    		if(key.charCode < 46 || key.charCode > 57)
	    			return false;
	    	});
	    
	    	$('#dlyContProportionate').keypress(function(key) {
	    		if(key.charCode < 48 || key.charCode > 57)
	    			return false;
	    	});	
	    
	    	$('#dlyContLoad').keypress(function(key) {
	    		if(key.charCode < 46 || key.charCode > 57)
	    			return false;
	    	});
	    
	    	$('#dlyContTransitDay').keypress(function(key) {
	    		if(key.charCode < 48 || key.charCode > 57)
	    			return false;
	    	});
	    
	    	$('#dlyContStatisticalCharge').keypress(function(key) {
	    		if(key.charCode < 46 || key.charCode > 57)
	    			return false;
	    	});
	    
	    	$('#dlyContAdditionalRate').keypress(function(key) {
	    		if(key.charCode < 46 || key.charCode > 57)
	    			return false;
	    	});
	    
	    	$('#dlyContFromWt').keypress(function(key) {
	    		if(key.charCode < 46 || key.charCode > 57)
	    			return false;
	    	});
	    
	    	$('#dlyContToWt').keypress(function(key) {
	    		if(key.charCode < 46 || key.charCode > 57)
	    			return false;
	    	});
	    
	    	$('#dlyContRate').keypress(function(key) {
	    		if(key.charCode < 46 || key.charCode > 57)
	    			return false;
	    	});
	    
	    	$('#rbkmFromKm').keypress(function(key) {
	    		if(key.charCode < 48 || key.charCode > 57)
	    			return false;
	    	});
	    
	    	$('#rbkmToKm').keypress(function(key) {
	    		if(key.charCode < 48 || key.charCode > 57)
	    			return false;
	    	});
	    
	    	$('#rbkmRate').keypress(function(key) {
	    		if(key.charCode < 46 || key.charCode > 57)
	    			return false;
	    	});
	    
	    	$('#pbdFromDayP').keypress(function(key) {
	    		if(key.charCode < 48 || key.charCode > 57)
	    			return false;
	    	});
	    
	    	$('#pbdToDayP').keypress(function(key) {
	    		if(key.charCode < 48 || key.charCode > 57)
	    			return false;
	    	});
	    
	    	$('#pbdAmtP').keypress(function(key) {
	    		if(key.charCode < 46 || key.charCode > 57)
	    			return false;
	    	});
	    
	    	$('#pbdFromDayB').keypress(function(key) {
	    		if(key.charCode < 48 || key.charCode > 57)
	    			return false;
	    	});
	    
	    	$('#pbdToDayB').keypress(function(key) {
	    		if(key.charCode < 48 || key.charCode > 57)
	    			return false;
	    	});
	    
	    	$('#pbdAmtB').keypress(function(key) {
	    		if(key.charCode < 46 || key.charCode > 57)
	    			return false;
	    	});
	    
	    	$('#pbdFromDayD').keypress(function(key) {
	    			if(key.charCode < 48 || key.charCode > 57)
	    				return false;
	    	});
	    
	    	$('#pbdToDayD').keypress(function(key) {
	    		if(key.charCode < 48 || key.charCode > 57)
	    			return false;
	    	});
	    
	    	$('#pbdAmtD').keypress(function(key) {
	    		if(key.charCode < 46 || key.charCode > 57)
	    			return false;
	    	});
	    
	    	$('#vtLoadLimit').keypress(function(key) {
	    		if(key.charCode < 48 || key.charCode > 57)
	    			return false;
	    	});
	    
	    	$('#vtGuaranteeWt').keypress(function(key) {
	    		if(key.charCode < 46 || key.charCode > 57)
	    			return false;
	    	});
	    
	    	$('#ctsRate').keypress(function(key) {
	    		if(key.charCode < 46 || key.charCode > 57)
	    			return false;
	    	});
	    	
	    	$('#ctsFromWt').keypress(function(key) {
	    		if(key.charCode < 46 || key.charCode > 57)
	    			return false;
	    	});
	    	
	    	$('#ctsToWt').keypress(function(key) {
	    		if(key.charCode < 46 || key.charCode > 57)
	    			return false;
	    	});
	    	
	    	$('#ctsRateQ').keypress(function(key) {
	    		if(key.charCode < 46 || key.charCode > 57)
	    			return false;
	    	});
	    	
	    	$('#ctsFromWtQ').keypress(function(key) {
	    		if(key.charCode < 46 || key.charCode > 57)
	    			return false;
	    	});
	    	
	    	$('#ctsToWtQ').keypress(function(key) {
	    		if(key.charCode < 46 || key.charCode > 57)
	    			return false;
	    	});
	    
	        	var max = 15;
	        
	        	$('#pbdAmtD').keypress(function(e) {
	        		if((/\d*\.\d\d\d$/.test(this.value)) ||  (this.value.length == max)){
	        			e.preventDefault();
	        		}
	        	});
	        
	        	$('#pbdToDayD').keypress(function(e) {
	        		if (this.value.length == max) {
	        			e.preventDefault();
	        		} 
	        	});
	        	
	        	$('#ctsFromWt').keypress(function(e) {
	        		if((/\d*\.\d\d\d$/.test(this.value)) ||  (this.value.length == max)){
	        			e.preventDefault();
	        		}
	        	});
	        	
	        	$('#ctsToWt').keypress(function(e) {
	        		if((/\d*\.\d\d\d$/.test(this.value)) ||  (this.value.length == max)){
	        			e.preventDefault();
	        		}
	        	});
	        
	        	$('#ctsRate').keypress(function(e) {
	        		if (this.value.length == max) {
	        			e.preventDefault();
	        		} 
	        	});
	        	
	        	
	        	$('#ctsFromWtQ').keypress(function(e) {
	        		if((/\d*\.\d\d\d$/.test(this.value)) ||  (this.value.length == max)){
	        			e.preventDefault();
	        		}
	        	});
	        	
	        	$('#ctsToWtQ').keypress(function(e) {
	        		if((/\d*\.\d\d\d$/.test(this.value)) ||  (this.value.length == max)){
	        			e.preventDefault();
	        		}
	        	});
	        
	        	$('#ctsRateQ').keypress(function(e) {
	        		if (this.value.length == max) {
	        			e.preventDefault();
	        		} 
	        	});
	        
	        	$('#pbdFromDayD').keypress(function(e) {
	        		if (this.value.length == max) {
	        			e.preventDefault();
	        		} 
	        	});
	        
	        	$('#pbdAmtB').keypress(function(e) {
	        		if((/\d*\.\d\d\d$/.test(this.value)) ||  (this.value.length == max)){
	        			e.preventDefault();
	        		}
	        	});
	        
	        	$('#pbdToDayB').keypress(function(e) {
	        		if (this.value.length == max) {
	        			e.preventDefault();
	        		} 
	        	});
	        
	        	$('#pbdFromDayB').keypress(function(e) {
	        		if (this.value.length == max) {
	        			e.preventDefault();
	        		} 
	        	});
	        
	        	$('#pbdAmtP').keypress(function(e) {
	        		if((/\d*\.\d\d\d$/.test(this.value)) ||  (this.value.length == max)){
	        			e.preventDefault();
	        		} 
	        	});
	        
	        	$('#pbdFromDayP').keypress(function(e) {
	        		if (this.value.length == max) {
	        			e.preventDefault();
	        		}
	        	});
	        
	        	$('#pbdToDayP').keypress(function(e) {
	        		if (this.value.length == max) {
	        			e.preventDefault();
	        		} 
	        	});
	        
	        	$('#rbkmFromKm').keypress(function(e) {
	        		if (this.value.length == max) {
	        			e.preventDefault();
	        		} 
	        	});
	        
	        	$('#rbkmToKm').keypress(function(e) {
	        		if (this.value.length == max) {
	        			e.preventDefault();
	        		} 
	        	});
	        
	        	$('#rbkmRate').keypress(function(e) {
	        		if((/\d*\.\d\d\d$/.test(this.value)) ||  (this.value.length == max)){
	        			e.preventDefault();
	        		} 
	        	});
	        
	        	$('#dlyContRate').keypress(function(e) { 
	        		if((/\d*\.\d\d\d$/.test(this.value)) ||  (this.value.length == max)){
	        			e.preventDefault();
	        		} 
	        	});
	        
	        	$('#dlyContFromWt').keypress(function(e) {
	        		if((/\d*\.\d\d\d$/.test(this.value)) ||  (this.value.length == max)){
	        			e.preventDefault();
	        		} 
	        	});
	        
	        	$('#dlyContToWt').keypress(function(e) {
	        		if((/\d*\.\d\d\d$/.test(this.value)) ||  (this.value.length == max)){
	        			e.preventDefault();
	        		}
	        	});
	        
	        	$('#dlyContTransitDay').keypress(function(e) {
	        		if (this.value.length == max) {
	        			e.preventDefault();
	        		}
	        	});
	        
	        	$('#dlyContStatisticalCharge').keypress(function(e) {
	        		if((/\d*\.\d\d\d$/.test(this.value)) ||  (this.value.length == max)){
	        			e.preventDefault();
	        		} 
	        	});
	        
	        	$('#dlyContLoad').keypress(function(e) {
	        		if((/\d*\.\d\d\d$/.test(this.value)) ||  (this.value.length == max)){
	        			e.preventDefault();
	        		} 
	        	});
	        
	        	$('#dlyContUnLoad').keypress(function(e) {
	        		if((/\d*\.\d\d\d$/.test(this.value)) ||  (this.value.length == max)){
	        			e.preventDefault();
	        		} 
	        	});
	        
	        	$('#dlyContAdditionalRate').keypress(function(e) {
	        		if((/\d*\.\d\d\d$/.test(this.value)) ||  (this.value.length == max)){
	        			e.preventDefault();
	        		} 
	        	});
	        
	        	$('#vtLoadLimit').keypress(function(e) {
	        		if (this.value.length == max) {
	        			e.preventDefault();
	        		} 
	        	});
	        
	        	$('#vtGuaranteeWt').keypress(function(e) {
	        		if((/\d*\.\d\d\d$/.test(this.value)) ||  (this.value.length == max)){
	        			e.preventDefault();
	        		} 
	        	});
	        
	        	$('#vtVehicleType').keypress(function(e) {
	        		if (this.value.length == 30) {
	        			e.preventDefault();
	        		} 
	        	});
	        
	        	$('#vtServiceType').keypress(function(e) {
	        		if (this.value.length == 30) {
	        			e.preventDefault();
	        		} 
	        	});
	        
	        	$('#vtCode').keypress(function(e) {
	        		if (this.value.length == 1) {
	        			e.preventDefault();
	        		}             
	        	});
	
	        	$('#stnPin').keypress(function(key) {
	        		if(key.charCode < 48 || key.charCode > 57)
	        			return false;
	        	});

	        	$('#stnPin').keypress(function(e) {
	        		if (this.value.length == 6) {
	        			e.preventDefault();
	        		}
	        	});
	 	 
	        	$('#stnName').keypress(function(e) {
	        		if (this.value.length == 40) {
	        			e.preventDefault();
	        		}
	        	});
	 	 
	        	$('#district').keypress(function(e) {
	        		if (this.value.length == 40) {
	        			e.preventDefault();
	        		}
	        	});
	
	        $scope.saveTncUnload = function(){
	        	if($scope.checkUnLoad){
	        		$scope.checkUnLoadFlag=false;
	        		$scope.tnc.tncUnLoading="yes";
	        	}
	        	if(!($scope.checkUnLoad)){
	        		$scope.checkUnLoadFlag=true;
	        		$scope.tnc.tncUnLoading="no";
	        		//$scope.dlyCnt.dlyContUnLoad="";
	        		$scope.cts.ctsToUnLoad="";
	        	}	
	        }
	
			$scope.saveTncPenalty = function(){
				if($scope.checkPenalty){
					$scope.checkPenaltyFlag=false;
					$scope.tnc.tncPenalty="yes";
				}
				if(!($scope.checkPenalty)){
					$scope.checkPenaltyFlag=true;
					$scope.tnc.tncPenalty="no";
				}	
			}
	
			$scope.saveTncLoad= function(){
				if($scope.checkLoad){
					$scope.checkLoadFlag=false;
					$scope.tnc.tncLoading="yes";
				}
				if(!($scope.checkLoad)){	
					$scope.checkLoadFlag=true;
					//$scope.dlyCnt.dlyContLoad="";
					$scope.cts.ctsToLoad="";
					$scope.tnc.tncLoading="no";
				}	
			}
	
			$scope.saveTncBonus = function(){
				if($scope.checkBonus){
					$scope.checkBonusFlag=false;
					$scope.tnc.tncBonus="yes";
				}
				if(!($scope.checkBonus)){
					$scope.checkBonusFlag=true;
					$scope.tnc.tncBonus="no";
				}	
			}
	
			$scope.saveTncDetention = function(){
				if($scope.checkDetention){
					$scope.checkDetentionFlag=false;
					$scope.tnc.tncDetention="yes";
				}
				if(!($scope.checkDetention)){
					$scope.checkDetentionFlag=true;
					$scope.tnc.tncDetention="no";
				}	
			}
	
			$scope.calculateTransitDay=function(cts){
				$scope.cts.ctsToTransitDay=Math.round(cts.ctsDistanceKm/350);
			}
			
			
			$scope.saveTncTransitDay = function(){
				if($scope.checkTransitDay){
					$scope.checkTransitFlag=false;
					$scope.tnc.tncTransitDay="yes";
				}
				if(!($scope.checkTransitDay)){
					$scope.checkTransitFlag=true;
					//$scope.dlyCnt.dlyContTransitDay="";
					$scope.cts.ctsToTransitDay="";
					$scope.tnc.tncTransitDay="no";
				}	
			}	
	
			$scope.saveTncStatisticalCharge = function(){
				if($scope.checkStatisticalCharge){
					$scope.checkStatisticalChargeFlag=false;
					$scope.tnc.tncStatisticalCharge="yes";
				}
				if(!($scope.checkStatisticalCharge)){
					$scope.checkStatisticalChargeFlag=true;
					//$scope.dlyCnt.dlyContStatisticalCharge="";
					$scope.cts.ctsToStatChg="";
					$scope.tnc.tncStatisticalCharge="no";
				}	
			}
		
			$scope.setTransitFlagTrue = function(cts){
				if(cts.ctsToTransitDay ===null || cts.ctsToTransitDay==="" || angular.isUndefined(cts.ctsToTransitDay)){
					$scope.checkTransitFlag=false;
				}else{
					$scope.checkTransitFlag=true;
				}	
			}
		
			$scope.setSCFlagTrue = function(cts){
				if(cts.ctsToStatChg ===null || cts.ctsToStatChg==="" || angular.isUndefined(cts.ctsToStatChg)){
					$scope.checkStatisticalChargeFlag=false;
				}else{
					$scope.checkStatisticalChargeFlag=true;
				}
			}
		
			$scope.setLoadFlagTrue = function(cts){
				if(cts.ctsToLoad ===null || cts.ctsToLoad==="" || angular.isUndefined(cts.ctsToLoad)){
					$scope.checkLoadFlag=false;	
				}else{
					$scope.checkLoadFlag=true;
				}
			}
		
			$scope.setUnLoadFlagTrue = function(cts){
				if(cts.ctsToUnLoad ===null || cts.ctsToUnLoad==="" || angular.isUndefined(cts.ctsToUnLoad)){
					$scope.checkUnLoadFlag=false;	
				}else{
					$scope.checkUnLoadFlag=true;
				}
			}
		
			$scope.setFflagTrue = function(dlyCnt){
				console.log(dlyCnt.dlyContAdditionalRate);
				if(dlyCnt.dlyContAdditionalRate===null || dlyCnt.dlyContAdditionalRate==="" || angular.isUndefined(dlyCnt.dlyContAdditionalRate)){
					$scope.Fflag=false;	
				}
				else{
					$scope.Fflag=true;
				}
			}

	
			$scope.OpenBranchCodeDB = function(){
				$scope.branchCodeFlag = false;
				$('div#branchCodeDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					position: UDPos,
					show: UDShow,
					hide: UDHide,
					title: "Branch Code",
					draggable: true,
					close: function(event, ui) { 
						$(this).dialog('destroy');
						$(this).hide();
					}
					});
					$('div#branchCodeDB').dialog('open');
				}
	
			$scope.OpendlyContBLPMCodeDB = function(){
				$scope.blpmCodeFlag = false;
				$('div#dlyContBLPMCodeDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					position: UDPos,
					show: UDShow,
					hide: UDHide,
					title: "BLPM Code",
					draggable: true,
					close: function(event, ui) { 
						$(this).dialog('destroy') ;
						$(this).hide();
					}
					});
					$('div#dlyContBLPMCodeDB').dialog('open');
				}
		
			$scope.OpendlyContCngrCodeDB=function(){
				$scope.CngrCodeFlag = false;
				$('div#dlyContCngrCodeDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					position: UDPos,
					show: UDShow,
					hide: UDHide,
					title: "Consignor",
					draggable: true,
					close: function(event, ui) { 
						$(this).dialog('destroy');
						$(this).hide();
					}
					});
					$('div#dlyContCngrCodeDB').dialog('open');
				}
		
			$scope.OpendlyContCrNameDB = function(){
				$scope.CrNameFlag = false;
				$('div#dlyContCrNameDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					position: UDPos,
					show: UDShow,
					hide: UDHide,
					title: "Customer Representative Name",
					draggable: true,
					close: function(event, ui) { 
						$(this).dialog('destroy');
						$(this).hide();
					}
					});
				$('div#dlyContCrNameDB').dialog('open');
				}
		
			$scope.OpendlyContFromStationDB = function(){
				$scope.FromStationFlag = false;
				$('div#dlyContFromStationDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					position: UDPos,
					show: UDShow,
					hide: UDHide,
					title: "From Station",
					draggable: true,
					close: function(event, ui) { 
						$(this).dialog('destroy');
						$(this).hide();
					}
					});
					$('div#dlyContFromStationDB').dialog('open');
					}
		
			$scope.OpendlyContToStationDB = function(){
				$scope.ToStationFlag = false;
				$('div#dlyContToStationDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					position: UDPos,
					show: UDShow,
					hide: UDHide,
					title: "To Station",
					draggable: true,
					close: function(event, ui) { 
						$(this).dialog('destroy');
						$(this).hide();
					}
					});
					$('div#dlyContToStationDB').dialog('open');
				}
		
			$scope.openCtsVehicleType = function(){
				$scope.VehicleTypeFlag = false;
				$('div#ctsVehicleTypeDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					position: UDPos,
					show: UDShow,
					hide: UDHide,
					title: "Vehicle Type",
					draggable: true,
					close: function(event, ui) { 
						$(this).dialog('destroy');
						$(this).hide();
					}
					});
					$('div#ctsVehicleTypeDB').dialog('open');		
				}

				$scope.openProductTypeDB = function(){
					$scope.ProductTypeFlag = false;
					$('div#productTypeDB').dialog({
						autoOpen: false,
						modal:true,
						resizable: false,
						position: UDPos,
						show: UDShow,
						hide: UDHide,
						title: "Product Type",
						draggable: true,
						close: function(event, ui) { 
							$(this).dialog('destroy');
							$(this).hide();
						}
					});
					$('div#productTypeDB').dialog('open');
				}
		
			$scope.OpendlyContRbkmIdDB = function(dlyCnt){
					if(dlyCnt.dlyContFromStationTemp==="" || dlyCnt.dlyContFromStationTemp===null || angular.isUndefined(dlyCnt.dlyContFromStationTemp) || 
							dlyCnt.dlyContToStationTemp==="" || dlyCnt.dlyContToStationTemp===null || angular.isUndefined(dlyCnt.dlyContToStationTemp)){
						$scope.alertToast("Please enter From Station & To station");
					}else{
						$scope.RbkmIdFlag = false;
						$('div#dlyContRbkmIdDB').dialog({
							autoOpen: false,
							modal:true,
							resizable: false,
							position: UDPos,
							show: UDShow,
							hide: UDHide,
							title: "RBKM ID",
							draggable: true,
							close: function(event, ui) { 
								$(this).dialog('destroy');
								$(this).hide();
							}
						});	
						$('div#dlyContRbkmIdDB').dialog('open');	
						$scope.rbkm.rbkmToStationTemp=$scope.dlyCnt.dlyContToStationTemp;
						$scope.rbkm.rbkmToStation=$scope.dlyCnt.dlyContToStation;
					}
				}
		
			$scope.OpenProductTypeDB1 = function(){
				$scope.AddProductTypeFlag = false;
				$('div#AddProductTypeDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					position: UDPos,
					show: UDShow,
					hide: UDHide,
					title: "Product Type",
					draggable: true,
					close: function(event, ui) { 	
						$(this).dialog('destroy');
						$(this).hide();
					}
					});
					$('div#AddProductTypeDB').dialog('open');
				}
		
			$scope.OpenAddVehicleTypeDB = function(){
				$scope.VehicleTypeFlag1 = false;
				$('div#addVehicleTypeDB1').dialog({
					autoOpen: false,
					modal:true,
					title: "Vehicle Type",
					position: UDPos,
					show: UDShow,
					hide: UDHide,
					resizable: false,
					draggable: true,
					close: function(event, ui) { 
						$(this).dialog('destroy');
						$(this).hide();
					}
					});
					$('div#addVehicleTypeDB1').dialog('open');
				}
		
			$scope.OpenrbkmFromStationDB = function(){
				$scope.rbkmFromStationFlag = false;
				$('div#rbkmFromStationDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					position: UDPos,
					show: UDShow,
					hide: UDHide,
					title: "From Station",
					draggable: true,
					close: function(event, ui) { 
						$(this).dialog('destroy');
						$(this).hide();
					}
					});
					$('div#rbkmFromStationDB').dialog('open');
				}
		
			$scope.OpenrbkmToStationDB = function(){
				$scope.rbkmToStationFlag = false;
				$('div#rbkmToStationDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					position: UDPos,
					show: UDShow,
					hide: UDHide,
					title: "To Station",
					draggable: true,
					close: function(event, ui) { 
						$(this).dialog('destroy');
						$(this).hide();
					}
					});
					$('div#rbkmToStationDB').dialog('open');
				}
		
			$scope.OpenrbkmStateCodeDB = function(){
				$scope.rbkmStateCodeFlag = false;
				$('div#rbkmStateCodeDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					position: UDPos,
					show: UDShow,
					hide: UDHide,
					title: "State Code",
					draggable: true,
					close: function(event, ui) { 	 
						$(this).dialog('destroy');
						$(this).hide();
					}
					});
					$('div#rbkmStateCodeDB').dialog('open');
				}
		
			$scope.OpenPenaltyDB = function(){
				$scope.PenaltyFlag = false;
				$scope.pbdP.pbdPenBonDet = "P";
				
				$('div#PenaltyDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					position: UDPos,
					show: UDShow,
					hide: UDHide,
					title: "Penalty",
					draggable: true,
					close: function(event, ui) { 	
						$(this).dialog('destroy');
						$(this).hide();
					}
				});
				$('div#PenaltyDB').dialog('open');
			}
		
			$scope.OpenBonusDB = function(){
				$scope.BonusFlag = false;
				$scope.pbdB.pbdPenBonDet = "B";
				
				$('div#BonusDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					position: UDPos,
					show: UDShow,
					hide: UDHide,
					title: "Bonus",
					draggable: true,
					close: function(event, ui) { 
						$(this).dialog('destroy');
						$(this).hide();
					}
				});
				$('div#BonusDB').dialog('open');
				}
		
			$scope.OpenDetentionDB = function(){
				$scope.DetentionFlag = false;
				$scope.pbdD.pbdPenBonDet = "D";
				
				$('div#DetentionDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					position: UDPos,
					show: UDShow,
					hide: UDHide,
					title: "Detention",
					draggable: true,
					close: function(event, ui) { 
						$(this).dialog('destroy');
						$(this).hide();
					}
				});
				$('div#DetentionDB').dialog('open');
			}
		
			$scope.OpenRbkmVehicleTypeDB = function(){
				$scope.rbkmVehicleTypeFlag = false;
				$('div#rbkmVehicleTypeDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					position: UDPos,
					show: UDShow,
					hide: UDHide,
					title: "Vehicle Type",
					draggable: true,
					close: function(event, ui) { 
						$(this).dialog('destroy');
						$(this).hide();
					}
					});
					$('div#rbkmVehicleTypeDB').dialog('open');
				}
		
			$scope.OpenPenaltyFromStationDB = function(){
				$scope.PenaltyFromStationFlag = false;
				$('div#PenaltyFromStationDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					position: UDPos,
					show: UDShow,
					hide: UDHide,
					title: "From Station",
					draggable: true,
					close: function(event, ui) { 
						$(this).dialog('destroy');
						$(this).hide();
					}
					});
					$('div#PenaltyFromStationDB').dialog('open');
				}
			
		
			$scope.OpenPenaltyToStationDB = function(){
				$scope.PenaltyToStationFlag = false;
				$('div#PenaltyToStationDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					position: UDPos,
					show: UDShow,
					hide: UDHide,
					title: "To Station",
					draggable: true,
					close: function(event, ui) { 	 
						$(this).dialog('destroy');
						$(this).hide();
					}
					});
					$('div#PenaltyToStationDB').dialog('open');
				}
		
			$scope.OpenPenaltyVehicleTypeDB = function(){
				$scope.PenaltyVehicleTypeFlag = false;
				$('div#PenaltyVehicleTypeDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					position: UDPos,
					show: UDShow,
					hide: UDHide,
					title: "Vehicle Type",
					draggable: true,
					close: function(event, ui) { 
						$(this).dialog('destroy') ;
						$(this).hide();
					}
					});
					$('div#PenaltyVehicleTypeDB').dialog('open');
				}
		
			$scope.OpenBonusFromStationDB = function(){
				$scope.BonusFromStationFlag = false;
				$('div#BonusFromStationDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					position: UDPos,
					show: UDShow,
					hide: UDHide,
					title: "From Station",
					draggable: true,
					close: function(event, ui) { 
						$(this).dialog('destroy') ;
						$(this).hide();
					}
					});
					$('div#BonusFromStationDB').dialog('open');
				}
			
		
			$scope.OpenBonusToStationDB = function(){
				$scope.BonusToStationFlag = false;
				$('div#BonusToStationDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					position: UDPos,
					show: UDShow,
					hide: UDHide,
					title: "To Station",
					draggable: true,
					close: function(event, ui) { 
						$(this).dialog('destroy') ;
						$(this).hide();
					}
					});				
					$('div#BonusToStationDB').dialog('open');
				}
		
			$scope.OpenBonusVehicleTypeDB = function(){
				$scope.BonusVehicleTypeFlag = false;
				$('div#BonusVehicleTypeDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					position: UDPos,
					show: UDShow,
					hide: UDHide,
					title: "Vehicle Type",
					draggable: true,
					close: function(event, ui) { 
						$(this).dialog('destroy');
						$(this).hide();
					}
					});
					$('div#BonusVehicleTypeDB').dialog('open');
				}
		
			$scope.OpenDetentionFromStationDB = function(){
				$scope.DetentionFromStationFlag = false;
				$('div#DetentionFromStationDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					position: UDPos,
					show: UDShow,
					hide: UDHide,
					title: "From Station",
					draggable: true,
					close: function(event, ui) { 
						$(this).dialog('destroy');
						$(this).hide();
					}
					});
				
				$('div#DetentionFromStationDB').dialog('open');
				}
		
			$scope.OpenDetentionToStationDB = function(){
				$scope.DetentionToStationFlag = false;
				$('div#DetentionToStationDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					position: UDPos,
					show: UDShow,
					hide: UDHide,
					title: "To Station",
					draggable: true,
					close: function(event, ui) {  
						$(this).dialog('destroy');
						$(this).hide();
					}
					});
				
				$('div#DetentionToStationDB').dialog('open');
				}
		
			$scope.OpenDetentionVehicleTypeDB = function(){
				
				$scope.DetentionVehicleTypeFlag = false;
				$('div#DetentionVehicleTypeDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					position: UDPos,
					show: UDShow,
					hide: UDHide,
					title: "Vehicle Type",
					draggable: true,
					close: function(event, ui) { 
						$(this).dialog('destroy');
						$(this).hide();
					}
					});
				
				$('div#DetentionVehicleTypeDB').dialog('open');
				}
			
			$scope.contToStnDB = function(dlyCnt){
				console.log("enter into contToStnDB function");
				/*if(dlyCnt.dlyContFromStationTemp===null || dlyCnt.dlyContFromStationTemp==="" || angular.isUndefined(dlyCnt.dlyContFromStationTemp))
				{
					$scope.alertToast("Please Enter From Station");
				}else */if(dlyCnt.dlyContType==='W'){
					$scope.contToStationFlagW = false;
					$scope.cts.ctsProductType="-100";
					if($scope.dlyCnt.dlyContProportionate === "F"){
						$('#ctsAdditionalRate').removeAttr("disabled");
					}
					$('div#contToStationW').dialog({
						autoOpen: false,
						modal:true,
						resizable: false,
						position: UDPos,
						show: UDShow,
						hide: UDHide,
						title: "Contract To Station",
						draggable: true,
						close: function(event, ui) { 
							$(this).dialog('destroy');
							$(this).hide();
						}
						});
					
					$('div#contToStationW').dialog('open');
				}else if(dlyCnt.dlyContType==='Q'){
					$scope.contToStationFlagQ = false;
					$scope.cts.ctsProductType="";
					$('div#contToStationQ').dialog({
						autoOpen: false,
						modal:true,
						resizable: false,
						position: UDPos,
						show: UDShow,
						hide: UDHide,
						title: "Contract To Station",
						draggable: true,
						close: function(event, ui) { 
							$(this).dialog('destroy');
							$(this).hide();
						}
						});
					
					$('div#contToStationQ').dialog('open');
				}
			}
		
			$scope.openToStationsDB = function(){	
				$scope.ToStationsDBFlag = false;
				$('div#ToStationsDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					position: UDPos,
					show: UDShow,
					hide: UDHide,
					title: "To Stations",
					draggable: true,
					close: function(event, ui) { 
						$(this).dialog('destroy');
						$(this).hide();
					}
					});
				
				$('div#ToStationsDB').dialog('open');
				}
		
			$scope.openAddNewStnDB = function(){	
				$scope.addStationFlag = false;
				$('div#addStation').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					position: UDPos,
					show: UDShow,
					hide: UDHide,
					title: "Station",
					draggable: true,
					close: function(event, ui) { 
						$(this).dialog('destroy');
						$(this).hide();
					}
					});
				
				$('div#addStation').dialog('open');
				}
		
			$scope.openStateCodeDB = function(){	
				$scope.StateCodeFlag = false;
				$('div#StateCodeDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					position: UDPos,
					show: UDShow,
					hide: UDHide,
					title: "State Code",
					draggable: true,
					close: function(event, ui) { 
						$(this).dialog('destroy');
						$(this).hide();
					}
					});
				
					$('div#StateCodeDB').dialog('open');
				}
		
			$scope.saveTncLoadRbkm = function(){
				if($scope.checkLoadRbkm){
					$scope.checkLoadFlagRbkm=false;
					$scope.tnc.tncLoading="yes";
				}
				if(!($scope.checkLoadRbkm)){
					$scope.rbkm.rbkmLoad = "";
					
					$scope.checkLoadFlagRbkm=true;
					$scope.tnc.tncLoading="no";
				}	
			}
			
			$scope.setLoadFlagTrueRbkm = function(rbkm){
				if(rbkm.rbkmLoad ===null || rbkm.rbkmLoad==="" || angular.isUndefined(rbkm.rbkmLoad)){
					$scope.checkLoadFlagRbkm=false;	
				}else{
					$scope.checkLoadFlagRbkm=true;
				}
			}
			
			
			$scope.saveTncUnloadRbkm = function(){
				if($scope.checkUnLoadRbkm){
					$scope.checkUnLoadFlagRbkm=false;
					$scope.tnc.tncUnLoading="yes";
				}
				if(!($scope.checkUnLoadRbkm)){
					$scope.rbkm.rbkmUnLoad = "";
					$scope.checkUnLoadFlagRbkm=true;
					$scope.tnc.tncUnLoading="no";
				}	
			}
			
			
			$scope.setUnLoadFlagTrueRbkm = function(rbkm){
				if(rbkm.rbkmUnLoad ===null || rbkm.rbkmUnLoad==="" || angular.isUndefined(rbkm.rbkmUnLoad)){
					$scope.checkUnLoadFlagRbkm=false;	
				}else{
					$scope.checkUnLoadFlagRbkm=true;
				}
			}
			
			
			$scope.saveTncTransitDayRbkm = function(){
				if($scope.checkTransitDayRbkm){
					$scope.checkTransitFlagRbkm=false;
					$scope.tnc.tncTransitDay="yes";
				}
				if(!($scope.checkTransitDayRbkm)){
					$scope.rbkm.rbkmTransitDay = "";
					$scope.checkTransitFlagRbkm=true;
					$scope.tnc.tncTransitDay="no";
				}	
			}
			
			
			$scope.saveTncStatisticalChargeRbkm = function(){
				if($scope.checkStatisticalChargeRbkm){
					$scope.checkStatisticalChargeFlagRbkm=false;
					$scope.tnc.tncStatisticalCharge="yes";
				}
				if(!($scope.checkStatisticalChargeRbkm)){
					$scope.rbkm.rbkmStatChg = "";
					$scope.checkStatisticalChargeFlagRbkm=true;
					$scope.tnc.tncStatisticalCharge="no";
				}	
			}
			
			$scope.setTransitFlagTrueRbkm = function(rbkm){
				if(rbkm.rbkmTransitDay ===null || rbkm.rbkmTransitDay==="" || angular.isUndefined(rbkm.rbkmTransitDay)){
					$scope.checkTransitFlagRbkm=false;
				}else{
					$scope.checkTransitFlagRbkm=true;
				}	
			}
			
			
			
			$scope.setSCFlagTrueRbkm = function(rbkm){
				if(rbkm.rbkmStatChg ===null || rbkm.rbkmStatChg==="" || angular.isUndefined(rbkm.rbkmStatChg)){
					$scope.checkStatisticalChargeFlagRbkm=false;
				}else{
					$scope.checkStatisticalChargeFlagRbkm=true;
				}
			}
			
			
			$scope.setStartDate=function(startDate){
				console.log("Inside setStartDate");
				$scope.pbdP.pbdStartDt=startDate;
				$scope.pbdB.pbdStartDt=startDate;
				$scope.pbdD.pbdStartDt=startDate;
			}
			
			$scope.setEndDate=function(endDate){
				console.log("Inside setEndDate");
				$scope.pbdP.pbdEndDt=endDate;
				$scope.pbdB.pbdEndDt=endDate;
				$scope.pbdD.pbdEndDt=endDate;	
			}
			
			$scope.savBranchCode = function(branch){
				$scope.dlyCnt.branchCode = branch.branchCode;
				$('div#branchCodeDB').dialog("destroy");
				$scope.branchCodeFlag = true;
			}
		
			$scope.savBLPMCode = function(customer){
				console.log(customer.custFaCode);
				$scope.dlyCnt.dlyContBLPMCode = customer.custCode;
				$scope.dlyContBLPMCodeTemp = customer.custFaCode;
				
				$scope.dlyCnt.dlyContCrNameTemp="";
				$('div#dlyContBLPMCodeDB').dialog("destroy");
				$scope.blpmCodeFlag = true;	
				console.log("custCode--"+customer.custCode);
				var response = $http.post($scope.projectName+'/getCustomerRepresentativeData',customer.custCode);
				response.success(function(data, status, headers, config){
					if(data.result==="success"){
						$scope.list = data.list;
						if(data.isDca === "true"){
							$scope.dca = data.dca;
							$scope.tempStList = data.stationList;
							if($scope.tempStList.length > 0){
								$scope.stationList = [];
								$scope.stationList = $scope.tempStList;
							}
							$scope.dlyCnt.dlyContStartDt =  $scope.dca.dcaFDt;
							$scope.dlyCnt.dlyContEndDt = $scope.dca.dcaTDt;
							
							$scope.pbdP.pbdStartDt=$scope.dlyCnt.dlyContStartDt;
							$scope.pbdB.pbdStartDt=$scope.dlyCnt.dlyContStartDt;
							$scope.pbdD.pbdStartDt=$scope.dlyCnt.dlyContStartDt;
							$scope.pbdP.pbdEndDt=$scope.dlyCnt.dlyContEndDt;
							$scope.pbdB.pbdEndDt=$scope.dlyCnt.dlyContEndDt;
							$scope.pbdD.pbdEndDt=$scope.dlyCnt.dlyContEndDt;	
						}
						$('#dlyContCrNameTemp').removeAttr("disabled");
						console.log(data.result);
						//$scope.getVehicleTypeCode();
					}else{
						console.log(data);
					}
				});
				response.error(function(data, status, headers, config) {
						$scope.errorToast(data.result);
					});
			}
		
			$scope.savCngrCode = function(cngrCode){
				$scope.dlyCnt.dlyContCngrCode = cngrCode.custCode;
				$scope.dlyContCngrCodeTemp = cngrCode.custFaCode;
				$('div#dlyContCngrCodeDB').dialog("destroy");
				$scope.CngrCodeFlag = true;
			}
		
			$scope.savFrmStnCode = function(Fstation){
				$scope.dlyCnt.dlyContFromStationTemp = Fstation.stnName;
				$scope.dlyCnt.dlyContFromStation = Fstation.stnCode;
			
				$scope.rbkm.rbkmFromStationTemp=Fstation.stnName;
				$scope.rbkm.rbkmFromStation=Fstation.stnCode;
				
				$scope.pbdP.pbdFromStnTemp=Fstation.stnName;
				$scope.pbdP.pbdFromStn=Fstation.stnCode;
				
				$scope.pbdB.pbdFromStnTemp=Fstation.stnName;
				$scope.pbdB.pbdFromStn=Fstation.stnCode;
				
				$scope.pbdD.pbdFromStnTemp=Fstation.stnName;
				$scope.pbdD.pbdFromStn=Fstation.stnCode;
				
				$('div#dlyContFromStationDB').dialog("destroy");
				$scope.FromStationFlag = true;
			}
		
			$scope.savToStnCode = function(Tstation){
				$scope.dlyCnt.dlyContToStationTemp = Tstation.stnName;
				$scope.dlyCnt.dlyContToStation =Tstation.stnCode;
			
				/*$scope.rbkm.rbkmToStationTemp=$scope.dlyCnt.dlyContToStationTemp;
				$scope.rbkm.rbkmToStation=$scope.dlyCnt.dlyContToStation;*/
				$('div#dlyContToStationDB').dialog("destroy");
				$scope.ToStationFlag = true;
				$scope.toStnFlag=true;
			}
		
			$scope.savCrName = function(cr){
				$scope.dlyCnt.dlyContCrNameTemp =cr.custRepName;
				$scope.dlyCnt.dlyContCrName = cr.custRepCode;
				$('div#dlyContCrNameDB').dialog("destroy");
				$scope.CrNameFlag = true;
			}
		
			$scope.savVehicleType = function(vtype){
				$scope.cts.ctsVehicleType = vtype.vtCode;
				$scope.pbdP.pbdVehicleType = vtype.vtCode;
				$scope.pbdB.pbdVehicleType = vtype.vtCode;
				$scope.pbdD.pbdVehicleType = vtype.vtCode;
				$('div#ctsVehicleTypeDB').dialog("destroy");
				$scope.VehicleTypeFlag = true;
			}
		
			$scope.savProductName = function(value){
			//	$scope.Qflag =true;
				$scope.cts.ctsProductType = value;
				$('div#productTypeDB').dialog("destroy");
				$scope.ProductTypeFlag = true;
			}
		
			$scope.savRbkmFromStnCode = function(rbkmFStation){
				$scope.rbkm.rbkmFromStationTemp = rbkmFStation.stnName;
				$scope.rbkm.rbkmFromStation =rbkmFStation.stnCode;;
				$('div#rbkmFromStationDB').dialog("destroy");
				$scope.rbkmFromStationFlag = true;
			}
		
			$scope.savRbkmVehicleType = function(RbkmVType){
				$scope.rbkm.rbkmVehicleType = RbkmVType.vtCode;
				$scope.pbdP.pbdVehicleType = RbkmVType.vtCode;
				$scope.pbdB.pbdVehicleType = RbkmVType.vtCode;
				$scope.pbdD.pbdVehicleType = RbkmVType.vtCode;
				$('div#rbkmVehicleTypeDB').dialog("destroy");
				$scope.rbkmVehicleTypeFlag = true;
			}
		
			$scope.saveRbkmToStnCode = function(rbkmTStation){
				$scope.rbkm.rbkmToStationTemp = rbkmTStation.stnName;
				$scope.rbkm.rbkmToStation =rbkmTStation.stnCode;;
				$('div#rbkmToStationDB').dialog("destroy");
				$scope.rbkmToStationFlag = true;
			}
		
			$scope.savPenaltyFromStn = function(PFstation){
				$scope.pbdP.pbdFromStnTemp = PFstation.stnName;
				$scope.pbdP.pbdFromStn =PFstation.stnCode;;
				$('div#PenaltyFromStationDB').dialog("destroy");
				$scope.PenaltyFromStationFlag = true;
			}
		
			$scope.savPenaltyToStn = function(PTstation){
				$scope.pbdP.pbdToStnTemp = PTstation.stnName;
				$scope.pbdP.pbdToStn =PTstation.stnCode;;
				$('div#PenaltyToStationDB').dialog("destroy");
				$scope.PenaltyToStationFlag = true;
			}
		
		$scope.savPenaltyVehicleType = function(VtPenalty){
			$scope.pbdP.pbdVehicleType=VtPenalty.vtCode;
			$('div#PenaltyVehicleTypeDB').dialog("destroy");
			$scope.PenaltyVehicleTypeFlag = true;
		}
		
		
			$scope.savBonusFromStn = function(BFstation){
				$scope.pbdB.pbdFromStnTemp = BFstation.stnName;
				$scope.pbdB.pbdFromStn =BFstation.stnCode;;
				$('div#BonusFromStationDB').dialog("destroy");
				$scope.BonusFromStationFlag = true;
			}
		
			$scope.savBonusToStn = function(BTstation){
				$scope.pbdB.pbdToStnTemp = BTstation.stnName;
				$scope.pbdB.pbdToStn =BTstation.stnCode;;
				$('div#BonusToStationDB').dialog("destroy");
				$scope.BonusToStationFlag = true;
			}
		
			$scope.savBonusVehicleType = function(vehicleType){
				$scope.pbdB.pbdVehicleType=vehicleType.vtCode;
				$('div#BonusVehicleTypeDB').dialog("destroy");
				$scope.BonusVehicleTypeFlag = true;
			}
		
			$scope.savDetentionFromStn = function(DFstation){
				$scope.pbdD.pbdFromStnTemp = DFstation.stnName;
				$scope.pbdD.pbdFromStn =DFstation.stnCode;;
				$('div#DetentionFromStationDB').dialog("destroy");
				$scope.DetentionFromStationFlag = true;
			}
		
			$scope.savDetentionToStn = function(DTstation){
				$scope.pbdD.pbdToStnTemp = DTstation.stnName;
				$scope.pbdD.pbdToStn =DTstation.stnCode;;
				$('div#DetentionToStationDB').dialog("destroy");
				$scope.DetentionToStationFlag = true;
			}
		
			$scope.savDetentionVehicleType = function(vtDetention){
				$scope.pbdD.pbdVehicleType=vtDetention.vtCode;
				$('div#DetentionVehicleTypeDB').dialog("destroy");
				$scope.DetentionVehicleTypeFlag = true;
			}
		
			$scope.saveStateCode = function(state){
				$scope.rbkm.rbkmStateCodeTemp=state.stateName;
				$scope.rbkm.rbkmStateCode=state.stateCode;
				$('div#rbkmStateCodeDB').dialog("destroy");
				$scope.rbkmStateCodeFlag = true;
			}
		
			$scope.saveToStations = function(toStations){
				console.log(toStations.stnName);
				$scope.cts.ctsToStnTemp=toStations.stnName;
				$scope.cts.ctsToStn=toStations.stnCode;
				$scope.pbdP.pbdToStnTemp = toStations.stnName;
				$scope.pbdP.pbdToStn = toStations.stnCode;
				$scope.pbdB.pbdToStnTemp = toStations.stnName;
				$scope.pbdB.pbdToStn = toStations.stnCode;
				$scope.pbdD.pbdToStnTemp = toStations.stnName;
				$scope.pbdD.pbdToStn = toStations.stnCode;
				$('div#ToStationsDB').dialog("destroy");
				$scope.ToStationsDBFlag = true;
			}	
		
			$scope.savStateCode = function(statecode){
				$scope.station.stateCode=statecode.stateCode;
				$('div#StateCodeDB').dialog("destroy");
				$scope.StateCodeFlag = true;
			}
		
			$scope.clickW = function(metricType,dlyContProportionate){
				if(metricType==="" || angular.isUndefined(metricType)){
					$scope.alertToast("please Enter metric type ");
					$('input[name=dlyContType]').attr('checked',false);
				}else if(dlyContProportionate==="" || angular.isUndefined(dlyContProportionate)) {
					$scope.alertToast("please Select Proportionate");
					$('input[name=dlyContType]').attr('checked',false);
				}else{
					$scope.Wflag=false;
					$scope.Kflag=true;
					$scope.ContToStnFlag=false;
					$scope.toStnFlag=true;
				//	$scope.ctsFlagQ=false;
				
					$scope.dlyCnt.dlyContFromStationTemp="";
					$scope.dlyCnt.dlyContFromStation="";
					$scope.dlyCnt.dlyContToStationTemp="";
					$scope.dlyCnt.dlyContToStation="";
					
					$scope.removeAllCTS();
					
					$scope.rbkm.rbkmFromStationTemp="";
					$scope.rbkm.rbkmToStationTemp="";
					$scope.rbkm.rbkmStartDate="";
					$scope.rbkm.rbkmStateCodeTemp="";
					$scope.rbkm.rbkmEndDate="";
					$scope.rbkm.rbkmFromKm="";
					$scope.rbkm.rbkmToKm="";
					$scope.rbkm.rbkmVehicleType="";
					$scope.rbkm.rbkmRate="";
					$('#dlyContProductType').removeAttr("disabled");
					$('#dlyContRbkmId').attr("disabled","disabled");
					$('#openProductTypeDB').removeAttr("disabled");
					$scope.dlyCnt.dlyContProductType="";
					$scope.removeAllRbkm();
					$scope.onWQClick=true;
					$scope.onKClick=false;
				}
			}
			
			$scope.clickQ = function(metricType,dlyContProportionate){
				if(metricType==="" || angular.isUndefined(metricType)){
					$scope.alertToast("please Enter metric type ");
					$('input[name=dlyContType]').attr('checked',false);
				}else if(dlyContProportionate==="" || angular.isUndefined(dlyContProportionate)){
					console.log(dlyContProportionate);
					$scope.alertToast("please Select Proportionate--");	
					$('input[name=dlyContType]').attr('checked',false);
				}else if(dlyContProportionate==='P'){
					console.log(dlyContProportionate);
				//	$scope.Qflag=false;
					$scope.Wflag=true;
					$scope.Kflag=true;
					$scope.ContToStnFlag=false;
					$scope.toStnFlag=true;
				//	$scope.ctsFlagW=false;
		
					$scope.dlyCnt.dlyContFromStationTemp="";
					$scope.dlyCnt.dlyContFromStation="";
					$scope.dlyCnt.dlyContToStationTemp="";
					$scope.dlyCnt.dlyContToStation="";
				
					$scope.removeAllCTS();
				
					$scope.rbkm.rbkmFromStationTemp="";
					$scope.rbkm.rbkmToStationTemp="";
					$scope.rbkm.rbkmStartDate="";
					$scope.rbkm.rbkmStateCodeTemp="";
					$scope.rbkm.rbkmEndDate="";
					$scope.rbkm.rbkmFromKm="";
					$scope.rbkm.rbkmToKm="";
					$scope.rbkm.rbkmVehicleType="";
					$scope.rbkm.rbkmRate="";
					$('#dlyContProductType').removeAttr("disabled");
					$('#dlyContRbkmId').attr("disabled","disabled");
					$('#openProductTypeDB').removeAttr("disabled");
					$scope.removeAllRbkm();
					$scope.onWQClick=true;
					$scope.onKClick=false;	
				}else{
					$scope.alertToast("Select Proportionate");
					$('input[name=dlyContType]').attr('checked',false);
				}
			}
	
			$scope.clickK = function(metricType,dlyContProportionate){
				if(metricType==="" || angular.isUndefined(metricType)){
					$scope.alertToast("please Enter metric type ");	
					$('input[name=dlyContType]').attr('checked',false);
				}else if(dlyContProportionate==="" || angular.isUndefined(dlyContProportionate)){
					console.log(dlyContProportionate);
					$scope.alertToast("please Select Proportionate--");	
					$('input[name=dlyContType]').attr('checked',false);
				}else if(dlyContProportionate==='P'){
					console.log(dlyContProportionate);
				//	$scope.Qflag=true;
					$scope.Wflag=true;
					$scope.Kflag=false;
					$scope.toStnFlag=false;
					$scope.ContToStnFlag=true;
					//$scope.ctsLengthFlag=true;
					
					$scope.dlyCnt.dlyContFromStationTemp="";
					$scope.dlyCnt.dlyContFromStation="";
					$scope.dlyCnt.dlyContToStationTemp="";
					$scope.dlyCnt.dlyContToStation="";
					
					$('#dlyContRbkmId').removeAttr("disabled");
					$('#dlyContProductType').attr("disabled","disabled");
					$('#openProductTypeDB').attr("disabled","disabled");
					$scope.dlyCnt.dlyContProductType="";
					$scope.onWQClick=false;
					$scope.onKClick=true;
					$scope.removeAllCTS();
				}else{
					$scope.alertToast("Select Proportionate");	
					$('input[name=dlyContType]').attr('checked',false);
				}
			}
			
			$scope.clickF = function(){
				$scope.Fflag=false;
				$('#dlyContAdditionalRate').removeAttr("disabled");
			}
		
			$scope.clickP = function(){
				$scope.Fflag=true;
				$scope.dlyCnt.dlyContAdditionalRate="";
				$('#dlyContAdditionalRate').attr("disabled","disabled");
			}
		
			$scope.setToStnFlag=function(dlyCnt){
				console.log("setToStnFlag");
				if(dlyCnt.dlyContToStationTemp==="" || dlyCnt.dlyContToStationTemp===null || angular.isUndefined(dlyCnt.dlyContToStationTemp)){
					$scope.toStnFlag=false;
					console.log($scope.toStnFlag);
				}else{
					$scope.toStnFlag=true;
					console.log($scope.toStnFlag);
				}
			}	
		
			$scope.submit = function(DailyContractForm,dlyCnt,metricType,tnc,rbkm,pbdB,pbdP,pbdD,vt,cts){
				console.log("Enter into Daily contract controller js --->"+DailyContractForm.$invalid);
				
				if(DailyContractForm.$invalid){
					if(DailyContractForm.dlyContBLPMCode.$invalid){
						$scope.alertToast("please Enter BLPM Code");
					}else if(DailyContractForm.dlyContCrNameTemp.$invalid){
						$scope.alertToast("please Enter CR Name");
					}else if(DailyContractForm.dlyContFromStationTemp.$invalid){
						$scope.alertToast("please Enter From Station Name");
					}else if(DailyContractForm.dlyContDc.$invalid){
						$scope.alertToast("please Enter DC");
					}else if(DailyContractForm.dlyContCostGrade.$invalid){
						$scope.alertToast("please Enter Cost Grade");
					}else if(DailyContractForm.dlyContDdl.$invalid){
						$scope.alertToast("please Enter DDL");
					}else if(DailyContractForm.dlyContHourOrDay.$invalid){
						$scope.alertToast("please Enter Hour/Day");
					}else if(DailyContractForm.metricType.$invalid){
						$scope.alertToast("please Enter Metric Type");
					}
				}else{
					/*console.log("value of dlyCnt.dlyContAdditionalRate = "+dlyCnt.dlyContAdditionalRate);
					if($scope.Fflag === false){
						if(!angular.isNumber(dlyCnt.dlyContAdditionalRate) || angular.isUndefined(dlyCnt.dlyContAdditionalRate))
						{
							$scope.alertToast("please fill Additional Rate");	
						}
					}else */if ( $('input[name=dlyContType]:checked').length === 0 ){
						$scope.alertToast("Please select contract Type");	
					}else if($scope.Kflag===false){
						if(rbkm.rbkmFromStationTemp === null || angular.isUndefined(rbkm.rbkmFromStationTemp) || rbkm.rbkmFromStationTemp==="" || rbkm.rbkmToStationTemp===null || angular.isUndefined(rbkm.rbkmToStationTemp) || rbkm.rbkmToStationTemp==="" || rbkm.rbkmStartDate===null || angular.isUndefined(rbkm.rbkmStartDate) || rbkm.rbkmStartDate===""
							|| rbkm.rbkmEndDate===null || angular.isUndefined(rbkm.rbkmEndDate) || rbkm.rbkmEndDate==="" || rbkm.rbkmStateCodeTemp===null || angular.isUndefined(rbkm.rbkmStateCodeTemp) || rbkm.rbkmStateCodeTemp==="" || rbkm.rbkmFromKm===null || angular.isUndefined(rbkm.rbkmFromKm) || rbkm.rbkmFromKm===""
								|| rbkm.rbkmToKm ===null || angular.isUndefined(rbkm.rbkmToKm) || rbkm.rbkmToKm==="" || rbkm.rbkmVehicleType ===null || angular.isUndefined(rbkm.rbkmVehicleType) || rbkm.rbkmVehicleType==="" || rbkm.rbkmRate===null || angular.isUndefined(rbkm.rbkmRate) || rbkm.rbkmRate==="")	
						{
							$scope.alertToast("Please fill RBKM");
						}
					}else if($scope.checkTransitFlag===false){
						if(dlyCnt.dlyContTransitDay === null || dlyCnt.dlyContTransitDay === "" || angular.isUndefined(dlyCnt.dlyContTransitDay))
						{
							$scope.alertToast("Please fill Transit Day");
						}
					}else if($scope.checkStatisticalChargeFlag===false){
						if(dlyCnt.dlyContStatisticalCharge === null || dlyCnt.dlyContStatisticalCharge==="" || angular.isUndefined(dlyCnt.dlyContStatisticalCharge))
						{
							$scope.alertToast("Please fill StatisticalCharge");
						}
					}else if($scope.checkLoadFlag === false){
						if(dlyCnt.dlyContLoad === null || dlyCnt.dlyContLoad==="" || angular.isUndefined(dlyCnt.dlyContLoad))
						{
							$scope.alertToast("Please fill Load");
						}
					}else if($scope.checkUnLoadFlag === false){
						if(dlyCnt.dlyContUnLoad === null || dlyCnt.dlyContUnLoad==="" || angular.isUndefined(dlyCnt.dlyContUnLoad))
						{
							$scope.alertToast("Please fill UnLoad");
						}
					}else if($scope.dlyCnt.dlyContStartDt>$scope.dlyCnt.dlyContEndDt){
						$scope.alertToast("From date cannot be greater than to date");
					}else if($scope.dlyCnt.dlyContFromWt>$scope.dlyCnt.dlyContToWt){
						$scope.alertToast("From weight cannot be greater than to weight");
					}else if($scope.toStnFlag===false){
						console.log($scope.toStnFlag);
						if(dlyCnt.dlyContToStationTemp==="" || dlyCnt.dlyContToStationTemp===null || angular.isUndefined(dlyCnt.dlyContToStationTemp)){
							$scope.alertToast("Please fill To Station");
						}
					}else if($scope.ContToStnFlag===false){
						if(cts.ctsToStn==="" || cts.ctsToStn===null || angular.isUndefined(cts.ctsToStn) || cts.ctsRate==="" || cts.ctsRate===null || angular.isUndefined(cts.ctsRate) || cts.ctsVehicleType=="" || cts.ctsVehicleType==null || angular.isUndefined(cts.ctsVehicleType) || cts.ctsProductType===null || cts.ctsProductType==="" || angular.isUndefined(cts.ctsProductType) ){
							$scope.alertToast("Please fill To Stations");
						}
					}else if($scope.checkPenaltyFlag === false){
						if(pbdP.pbdFromStnTemp === null || pbdP.pbdFromStnTemp === "" || angular.isUndefined(pbdP.pbdFromStnTemp) || pbdP.pbdToStnTemp === null || pbdP.pbdToStnTemp === "" || angular.isUndefined(pbdP.pbdToStnTemp) || pbdP.pbdStartDt === null || pbdP.pbdStartDt === "" || angular.isUndefined(pbdP.pbdStartDt) ||
							pbdP.pbdEndDt===null || pbdP.pbdEndDt==="" ||angular.isUndefined(pbdP.pbdEndDt) || pbd.pbdFromDay===null || pbdP.pbdFromDay==="" || angular.isUndefined(pbdP.pbdFromDay) || pbdP.pbdToDay===null || pbdP.pbdToDay==="" || angular.isUndefined(pbdP.pbdToDay) || pbdP.pbdVehicleType===null || pbdP.pbdVehicleType==="" || angular.isUndefined(pbdP.pbdVehicleType) ||
							pbdP.pbdHourNDay===null || pbdP.pbdHourNDay==="" || angular.isUndefined(pbdP.pbdHourNDay) || pbdP.pbdAmt===null || pbdP.pbdAmt==="" || angular.isUndefined(pbdP.pbdAmt))
							{
								$scope.alertToast("Please fill Penalty");
							}
							}else if($scope.checkBonusFlag === false){
								if(pbdB.pbdFromStnTemp===null || pbdB.pbdFromStnTemp==="" || angular.isUndefined(pbdB.pbdFromStnTemp) || pbdB.pbdToStnTemp===null || pbdB.pbdToStnTemp==="" || angular.isUndefined(pbdB.pbdToStnTemp) || pbdB.pbdStartDt===null || pbdB.pbdStartDt==="" || angular.isUndefined(pbdB.pbdStartDt)
										|| pbdB.pbdEndDt===null || pbdB.pbdEndDt==="" ||angular.isUndefined(pbdB.pbdEndDt) || pbdB.pbdFromDay===null || pbdB.pbdFromDay==="" || angular.isUndefined(pbdB.pbdFromDay) || pbdB.pbdToDay===null || pbdB.pbdToDay==="" || angular.isUndefined(pbdB.pbdToDay) || pbdB.pbdVehicleType===null || pbdB.pbdVehicleType==="" || angular.isUndefined(pbdB.pbdVehicleType)
										|| pbdB.pbdHourNDay===null || pbdB.pbdHourNDay==="" || angular.isUndefined(pbdB.pbdHourNDay) || pbdB.pbdAmt===null || pbdB.pbdAmt==="" || angular.isUndefined(pbdB.pbdAmt))
								{
									$scope.alertToast("Please fill Bonus");
								}
							}else if($scope.checkDetentionFlag === false){
								if(pbdD.pbdFromStnTemp===null || pbdD.pbdFromStnTemp==="" || angular.isUndefined(pbdD.pbdFromStnTemp) || pbdD.pbdToStnTemp===null || pbdD.pbdToStnTemp==="" || angular.isUndefined(pbdD.pbdToStnTemp) || pbdD.pbdStartDt===null || pbdD.pbdStartDt==="" || angular.isUndefined(pbdD.pbdStartDt)
										|| pbdD.pbdEndDt===null || pbdD.pbdEndDt==="" ||angular.isUndefined(pbdD.pbdEndDt) || pbdD.pbdFromDay===null || pbdD.pbdFromDay==="" || angular.isUndefined(pbdD.pbdFromDay) || pbdD.pbdToDay===null || pbdD.pbdToDay==="" || angular.isUndefined(pbdD.pbdToDay) || pbdD.pbdVehicleType===null || pbdD.pbdVehicleType==="" || angular.isUndefined(pbdD.pbdVehicleType)
										|| pbdD.pbdHourNDay===null || pbdD.pbdHourNDay==="" || angular.isUndefined(pbdD) || pbdD.pbdAmt===null || pbdD.pbdAmt==="" || angular.isUndefined(pbdD.pbdAmt))
								{
									$scope.alertToast("Please fill Detention");
								}
							}else if ( $('input[name=dlyContProportionate]:checked').length === 0 ){
								$scope.alertToast("Please select contract Proportionate/Fixed");	
							}else{
								console.log("final submittion");
								$scope.dlyContFlag = false;
								$('div#dlyContDB').dialog({
									autoOpen: false,
									modal:true,
									resizable: false,
									show: UDShow,
									hide: UDHide,
									position: UDPos,
									draggable: true,
									close: function(event, ui) { 
										$(this).dialog('destroy');
										$(this).hide();
									}
								});	
								$('div#dlyContDB').dialog('open');
							}
						}
					}
		
			$scope.back= function(){
				$scope.dlyContFlag = true;
				$('div#dlyContDB').dialog('close');
			}
		
			$scope.saveContract = function(dlyCnt,tnc,metricType){
				console.log("enter into saveContract function"+metricType);
				$('div#dlyContDB').dialog('close');
				
				/*if(metricType==="Ton"){
					dlyCnt.dlyContAdditionalRate=(dlyCnt.dlyContAdditionalRate/907.185);
					console.log(dlyCnt.dlyContAdditionalRate);
				}
		*/
				
				$('#saveBtnId').attr("disabled","disabled");
				var response = $http.post($scope.projectName+'/saveDailyContract',dlyCnt);
				response.success(function(data, status, headers, config) {
					$('#saveBtnId').removeAttr("disabled");
						$scope.dlyFaCode = data.faCode;
						$scope.showFaCode = false;
						$('div#showFaCodeId').dialog({
							autoOpen: false,
							modal:true,
							resizable: false,
							title: "Contract Code",
							show: UDShow,
							hide: UDHide,
							position: UDPos,
							draggable: true,
							close: function(event, ui) { 
								$(this).dialog('destroy');
								$(this).hide();
								$scope.showFaCode = true;
							}
						});	
						$('div#showFaCodeId').dialog('open');
						var response = $http.post($scope.projectName+'/saveTnc',tnc);
						response.success(function(data, status, headers, config) {
							$scope.successToast(data.resultTnc);
						});
						response.error(function(data, status, headers, config) {
							$scope.errorToast(data.resultTnc);
						});	
						$scope.successToast(data.result);
						$scope.successToast(data.resultRbkm);
						$scope.successToast(data.resultPbd);
						$scope.successToast(data.resultCts);
						/*$scope.dlyCnt.branchCode="";
						$scope.dlyCnt.dlyContBLPMCode="";
						$scope.dlyCnt.dlyContCngrCode="";
						$scope.dlyCnt.dlyContCrNameTemp="";
						$scope.dlyCnt.dlyContStartDt="";
						$scope.dlyCnt.dlyContEndDt="";
						$scope.dlyCnt.dlyContFromStation="";
						$scope.dlyCnt.dlyContToStation="";
						$scope.dlyCnt.dlyContFromStationTemp="";
						$scope.dlyCnt.dlyContToStationTemp="";
						$scope.dlyCnt.dlyContRate="";
						$scope.dlyCnt.dlyContFromWt="";
						$scope.dlyCnt.dlyContToWt="";
						$scope.dlyCnt.dlyContType="";
						$scope.dlyCnt.dlyContProductType="";
						$scope.dlyCnt.dlyContVehicleType="";
						$scope.dlyCnt.dlyContDc="";
						$scope.dlyCnt.dlyContCostGrade="";
						$scope.dlyCnt.dlyContTransitDay="";
						$scope.dlyCnt.dlyContStatisticalCharge="";
						$scope.dlyCnt.dlyContLoad="";
						$scope.dlyCnt.dlyContUnLoad="";
						$scope.dlyCnt.dlyContHourOrDay="";
						$scope.dlyCnt.dlyContProportionate="";
						$scope.dlyCnt.dlyContAdditionalRate="";
						$scope.dlyCnt.dlyContDdl="";
						$scope.dlyCnt.dlyContRemark="";*/
						$scope.dlyCnt = {};
					
						$scope.dlyContBLPMCodeTemp = "";
						$scope.dlyContCngrCodeTemp = "";
						$scope.metricType="";
						$scope.checkLoad=false;
						$scope.checkUnLoad=false;
						$scope.checkTransitDay=false;
						$scope.checkStatisticalCharge=false;
						$scope.dlyCnt.dlyContType=false;
						$scope.checkDetention=false;
						$scope.checkPenalty=false;
						$scope.checkBonus=false;
						$('input[name=dlyContProportionate]').attr('checked',false);
						$('#dlyContAdditionalRate').attr("disabled","disabled");
						$('#dlyContCrNameTemp').attr("disabled","disabled");
						$scope.onWQClick=false;
						$scope.onKClick=false;
						$scope.toStnFlag=true;
						$('#openProductTypeDB').attr("disabled","disabled");
						$scope.fetch();
					});
					response.error(function(data, status, headers, config) {
						$scope.errorToast(data.result);
					});
				}
		
			$scope.saveRbkmId = function(RbkmForm,rbkm,metricType){
				console.log("Save rbkm --"+metricType);
				$('div#dlyContRbkmIdDB').dialog("destroy");
				$scope.RbkmIdFlag = true;
				//$scope.Kflag=true;
				
				if($scope.rbkmList.length>0){
					console.log("If list exists");
					if(RbkmForm.rbkmFromStationTemp.$invalid){
						$scope.alertToast("please Enter From station");
					}else if(RbkmForm.rbkmToStationTemp.$invalid){
						$scope.alertToast("please Enter To station");
					}else if(RbkmForm.rbkmStateCodeTemp.$invalid){
						$scope.alertToast("please Enter State code");
					}else if(RbkmForm.rbkmToKm.$invalid){
						$scope.alertToast("to km cannot be greater than 7 characters");
					}else if(RbkmForm.rbkmFromKm.$invalid){
						$scope.alertToast("from km cannot be greater than 7 characters");
					}else if(RbkmForm.rbkmVehicleType.$invalid){
						$scope.alertToast("please Enter Vehicle Type");
					}else if(RbkmForm.rbkmRate.$invalid){
						$scope.alertToast("please Enter correct rbkm rate Type");
					}else if($scope.rbkm.rbkmStartDate>$scope.rbkm.rbkmEndDate){
						$scope.alertToast("Start date cannot be greater than end date");
					}else if($scope.rbkm.rbkmFromKm>$scope.rbkm.rbkmToKm){
						$scope.alertToast("from km cannot be greater than to km");
					}else{
						rbkm.rbkmStateCode=rbkm.rbkmStateCode.toUpperCase();
						rbkm.rbkmVehicleType=rbkm.rbkmVehicleType.toUpperCase();
						$scope.rbkmTemp=rbkm.rbkmStateCode+rbkm.rbkmVehicleType;
						
						for(var i=0;i<$scope.rbkmList.length;i++){
							$scope.type[i] = $scope.rbkmList[i].rbkmStateCode+$scope.rbkmList[i].rbkmVehicleType;
							$scope.type.push($scope.rbkmList[i].rbkmStateCode+$scope.rbkmList[i].rbkmVehicleType);
						}
						if($.inArray($scope.rbkmTemp,$scope.type)!==-1){
							$scope.alertToast("Name already exists");
							//$scope.rbkm.rbkmFromStationTemp="";
							$scope.rbkm.rbkmToStationTemp="";
							$scope.rbkm.rbkmStateCodeTemp="";
							$scope.rbkm.rbkmStartDate="";
							$scope.rbkm.rbkmEndDate="";
							$scope.rbkm.rbkmFromKm="";
							$scope.rbkm.rbkmToKm="";
							$scope.rbkm.rbkmVehicleType="";
							$scope.rbkm.rbkmRate="";
						} else{
							var temp = {
									"ToStn" : 	rbkm.rbkmToStationTemp,
									//"ToStnTemp" :rbkm.rbkmToStation,
									"FromStn" : rbkm.rbkmFromStationTemp,
									//"FromStnTemp" :rbkm.rbkmFromStation,
									"vehicleType" :rbkm.rbkmVehicleType,
									"stateCode" :rbkm.rbkmStateCode
								};
							$scope.rbkmStnList.push(temp);
							console.log("length of $scope.contToStnTemp = "+$scope.rbkmStnList.length);
							var response = $http.post($scope.projectName+'/addRbkm', rbkm);
							
							response.success(function(data, status, headers, config) {
								if(data.result==="success"){
									
									console.log("from server ---->"+data.result);
									$scope.fetchRbkmList();
									$scope.rbkm.rbkmFromStationTemp="";
									$scope.rbkm.rbkmToStationTemp="";
									$scope.rbkm.rbkmStateCodeTemp="";
									$scope.rbkm.rbkmStartDate="";
									$scope.rbkm.rbkmEndDate="";
									$scope.rbkm.rbkmFromKm="";
									$scope.rbkm.rbkmToKm="";
									$scope.rbkm.rbkmVehicleType="";
									$scope.rbkm.rbkmRate="";
						
									$('input[name=rbkmFStationName]').attr('checked',false);
									$('input[name=rbkmTStationName]').attr('checked',false);
									$('input[name="state"]').attr('checked',false);
									$('input[name=rbkmVTypeName]').attr('checked',false);
								}else{
									console.log(data);
								}
							});
							response.error(function(data, status, headers, config) {
								$scope.errorToast(data.result);
							});
						}	
					}
				}else{
					console.log("no list exists");
					if(RbkmForm.rbkmFromStationTemp.$invalid){
						$scope.alertToast("please Enter From station");
					}else if(RbkmForm.rbkmToStationTemp.$invalid){
						$scope.alertToast("please Enter To station");
					}else if(RbkmForm.rbkmStateCodeTemp.$invalid){
						$scope.alertToast("please Enter State code");
					}else if(RbkmForm.rbkmToKm.$invalid){
						$scope.alertToast("to km cannot be greater than 7 characters");
					}else if(RbkmForm.rbkmFromKm.$invalid){
						$scope.alertToast("from km cannot be greater than 7 characters");
					}else if(RbkmForm.rbkmVehicleType.$invalid){
						$scope.alertToast("please Enter Vehicle Type");
					}
					else if($scope.rbkm.rbkmStartDate>$scope.rbkm.rbkmEndDate){
	   					$scope.alertToast("Start date cannot be greater than end date");
	   				}else if($scope.rbkm.rbkmFromKm>$scope.rbkm.rbkmToKm){
	   					$scope.alertToast("from km cannot be greater than to km");
	   				}else{
	   					rbkm.rbkmStateCode=rbkm.rbkmStateCode.toUpperCase();
						rbkm.rbkmVehicleType=rbkm.rbkmVehicleType.toUpperCase();
						
						var temp = {
	   							"ToStn" : 	rbkm.rbkmToStationTemp,
								//"ToStnTemp" :rbkm.rbkmToStation,
								"FromStn" : rbkm.rbkmFromStationTemp,
								//"FromStnTemp" :rbkm.rbkmFromStation,
								"vehicleType" :rbkm.rbkmVehicleType,
								"stateCode" :rbkm.rbkmStateCode
							};
						$scope.rbkmStnList.push(temp);
						
	   					var response = $http.post($scope.projectName+'/addRbkmForRegCont',rbkm);
	   					response.success(function(data, status, headers, config) {
	   							if(data.result==="success"){
	   								$scope.fetchRbkmList();
	   								//$scope.rbkm.rbkmFromStationTemp="";
	   								$scope.rbkm.rbkmToStationTemp="";
	   								$scope.rbkm.rbkmStartDate="";
	   								$scope.rbkm.rbkmStateCodeTemp="";
	   								$scope.rbkm.rbkmEndDate="";
	   								$scope.rbkm.rbkmFromKm="";
	   								$scope.rbkm.rbkmToKm="";
	   								$scope.rbkm.rbkmVehicleType="";
	   								$scope.rbkm.rbkmRate="";
	   								
	   								$('input[name=rbkmFromstationName]').attr('checked',false);
	   								$('input[name=rbkmTostationName]').attr('checked',false);
	   								$('input[name="stateName"]').attr('checked',false);
	   								$('input[name=rbkmVTypeName]').attr('checked',false);			
	   							}else{
	   								console.log(data);
	   							}
	   						});
	   						response.error(function(data, status, headers, config) {
	   							$scope.errorToast(data.result);
	   						});
	   					}
	   				}	
				}	
		
		$scope.fetchRbkmList = function(){
			 var response = $http.get($scope.projectName+'/fetchRbkmList');
			 response.success(function(data, status, headers, config){
				 if(data.result==="success"){
					 //$scope.removeAllPbd();
					 $scope.rbkmList = data.list;
					 $scope.rbkmFlag=true;
					 $scope.Kflag=true;
				 }else{
					 console.log(data.result);
					// $scope.removeAllPbd();
					 $scope.rbkmList = data.list;
					 $scope.rbkmFlag=false;
					 $('#dlyContRbkmId').attr("disabled","disabled");
					 console.log($scope.dlyCnt.dlyContType);
					 if($scope.dlyCnt.dlyContType==="K"){
						 $scope.dlyCnt.dlyContType=false;
						 $scope.Kflag=false;
					 }
				 }		
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
		}
		
		$scope.removeRbkm = function(rbkm) {
			console.log("enter into removeRbkm function");
			var response = $http.post($scope.projectName+'/removeRbkmForRegCont',rbkm);
			response.success(function(data, status, headers, config) {
				if(data.result==="success"){
					for(var i=0;i<$scope.rbkmStnList.length;i++){
						if(($scope.rbkmStnList[i].stateCode === rbkm.rbkmStateCode) && ($scope.rbkmStnList[i].vehicleType === rbkm.rbkmVehicleType)){
							$scope.rbkmStnList.splice(i,1);
							i--;
						}
					}
					console.log($scope.rbkmStnList.length);
					$scope.fetchRbkmList();
				}else{
					console.log(data);
				}
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	    }
		
		$scope.removeAllRbkm = function() {
			var response = $http.post($scope.projectName+'/removeAllRbkm');
			response.success(function(data, status, headers, config) {
				if($scope.rbkmStnList.length > 0){
					for(var i=0;i<$scope.rbkmStnList.length;i++){
						$scope.rbkmStnList.splice(i,1);
						i--;
					}
				}
				console.log($scope.rbkmStnList.length);
				$scope.fetchRbkmList();
				console.log(data.result);
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	    }

	    	$scope.savePbdForPenalty = function(PenaltyForm,pbdP){
	    		$('div#PenaltyDB').dialog("destroy");
	    		$scope.PenaltyFlag=true;
	    		if(PenaltyForm.$invalid){
	    			if(PenaltyForm.pbdFromStnTempP.$invalid){
	    				$scope.alertToast("please Enter From Station");
	    			}else if(PenaltyForm.pbdToStnTempP.$invalid){
	    				$scope.alertToast("please Enter to Station");
	    			}else if(PenaltyForm.pbdVehicleTypeP.$invalid){
						$scope.alertToast("please Enter vehicle type");
	    			}else if(PenaltyForm.pbdToDayP.$invalid){
	    				$scope.alertToast("please Enter correct todlay");
	    			}else if(PenaltyForm.pbdFromDayP.$invalid){
	    				$scope.alertToast("please Enter correct fromdlay");
	    			}else if(PenaltyForm.pbdHourNDayP.$invalid){
	    				$scope.alertToast("please Enter Hour/Day");
	    			}else if(PenaltyForm.pbdAmtP.$invalid){
	    				$scope.alertToast("please Enter correct amount");
	    			}
	    		}else{
	    			if($scope.pbdP.pbdFromDay>$scope.pbdP.pbdToDay){
	    				$scope.alertToast("From day cannot be greater than to day");
	    			}else if($scope.pbdP.pbdStartDt>$scope.pbdP.pbdEndDt){
						$scope.alertToast("Start date cannot be greater than end date");
	    			}else {
	    				var temp = {
	    						 "ToStn" : 	pbdP.pbdToStnTemp,
									"ToStnTemp" : 	pbdP.pbdToStn,
									"FromStn" : pbdP.pbdFromStnTemp,
									"FromStnTemp" : pbdP.pbdFromStn,
									"pbd"	  :pbdP.pbdPenBonDet
								};
							$scope.pbdStnList.push(temp);
							console.log("length of $scope.pbdStnList = "+$scope.pbdStnList.length);
	    				
	    				var response = $http.post($scope.projectName+'/addPbd', pbdP);
	    				response.success(function(data, status, headers, config) {
	    					if(data.result==="success"){
	    						$scope.checkPenaltyFlag=true;
	    						$scope.fetchPbdList();
	    						//$scope.pbdP.pbdFromStnTemp = "";
	    						//$scope.pbdP.pbdToStnTemp = "";
	    						//$scope.pbdP.pbdStartDt = "";
	    						//$scope.pbdP.pbdEndDt = "";
	    						$scope.pbdP.pbdFromDay = "";
	    						$scope.pbdP.pbdToDay = "";
	    						//$scope.pbdP.pbdVehicleType= "";
	    						$scope.pbdP.pbdHourNDay = "";
	    						$scope.pbdP.pbdAmt = "";
	    						
	    						$('input[name=pFromstationName]').attr('checked',false);
	    						$('input[name=pTostationName]').attr('checked',false);
	    						$('input[name="vtPenaltyName"]').attr('checked',false);
	    					}else{
	    						console.log(data);
	    					}
	    				});
	    				response.error(function(data, status, headers, config) {
	    					$scope.errorToast(data.result);
	    				});
	    			}
				}
			}
			
			$scope.savePbdForBonus = function(BonusForm,pbdB){
				$('div#BonusDB').dialog("destroy");
				
				$scope.BonusFlag=true;
				if(BonusForm.$invalid){
					if(BonusForm.pbdFromStnTempB.$invalid){
						$scope.alertToast("please Enter From Station");
					}else if(BonusForm.pbdToStnTempB.$invalid){
						$scope.alertToast("please Enter to Station");
					}else if(BonusForm.pbdVehicleTypeB.$invalid){
						$scope.alertToast("please Enter vehicle type");
					}else if(BonusForm.pbdToDayB.$invalid){
						$scope.alertToast("please Enter correct todlay");
					}else if(BonusForm.pbdFromDayB.$invalid){
						$scope.alertToast("please Enter correct fromdlay");
					}else if(BonusForm.pbdHourNDayB.$invalid){
						$scope.alertToast("please Enter Hour/Day");
					}else if(BonusForm.pbdAmtB.$invalid){
						$scope.alertToast("please Enter correct amount");
					}
				}else{
					if($scope.pbdB.pbdFromDay>$scope.pbdB.pbdToDay){
						$scope.alertToast("From day cant be greater than to day");
					}else if($scope.pbdB.pbdStartDt>$scope.pbdB.pbdEndDt){
						$scope.alertToast("Start date cannot be greater than end date");
					}else{
						var temp = {
								 "ToStn" : 	pbdB.pbdToStnTemp,
									"ToStnTemp" : 	pbdB.pbdToStn,
									"FromStn" : pbdB.pbdFromStnTemp,
									"FromStnTemp" : pbdB.pbdFromStn,
									"pbd"	  :pbdB.pbdPenBonDet
								};
							$scope.pbdStnList.push(temp);
							console.log("length of $scope.pbdStnList = "+$scope.pbdStnList.length);
						var response = $http.post($scope.projectName+'/addPbd', pbdB);
						response.success(function(data, status, headers, config) {
							if(data.result==="success"){
								$scope.checkBonusFlag=true;
								$scope.fetchPbdList();
								//$scope.pbdB.pbdFromStnTemp = "";
								//$scope.pbdB.pbdToStnTemp = "";
								//$scope.pbdB.pbdStartDt = "";
								//$scope.pbdB.pbdEndDt = "";
								$scope.pbdB.pbdFromDay = "";
								$scope.pbdB.pbdToDay = "";
								//$scope.pbdB.pbdVehicleType = "";
								$scope.pbdB.pbdHourNDay = "";
								$scope.pbdB.pbdAmt = "";
								
								$('input[name=bFromstationName]').attr('checked',false);
								$('input[name=bTostationName]').attr('checked',false);
								$('input[name="vtBonusName"]').attr('checked',false);
							}else{
								console.log(data);
							}
						});
						response.error(function(data, status, headers, config) {
							$scope.errorToast(data.result);
						});
					}
				}
			}
			
			$scope.savePbdForDetention = function(DetentionForm,pbdD){
				$('div#DetentionDB').dialog("destroy");
				$scope.DetentionFlag=true;
				if(DetentionForm.$invalid){
					if(DetentionForm.pbdFromStnTempD.$invalid){
						$scope.alertToast("please Enter From Station");
					}else if(DetentionForm.pbdToStnTempD.$invalid){
						$scope.alertToast("please Enter to Station");
					}else if(DetentionForm.pbdVehicleTypeD.$invalid){
						$scope.alertToast("please Enter vehicle type");
					}else if(DetentionForm.pbdToDayD.$invalid){
						$scope.alertToast("please Enter correct todlay");
					}else if(DetentionForm.pbdFromDayD.$invalid){
						$scope.alertToast("please Enter correct fromdlay");
					}else if(DetentionForm.pbdHourNDayD.$invalid){
						$scope.alertToast("please Enter Hour/Day");
					}else if(DetentionForm.pbdAmtD.$invalid){
						$scope.alertToast("please Enter correct amount");
					}
					}else{
						if($scope.pbdD.pbdFromDay>$scope.pbdD.pbdToDay){
							$scope.alertToast("From day cannot be greater than to day");
						} else if($scope.pbdD.pbdStartDt>$scope.pbdD.pbdEndDt){
							$scope.alertToast("Start date cannot be greater than end date");
						}else{
							var temp = {
									 "ToStn" : 	pbdD.pbdToStnTemp,
										"ToStnTemp" : 	pbdD.pbdToStn,
										"FromStn" : pbdD.pbdFromStnTemp,
										"FromStnTemp" : pbdD.pbdFromStn,
										"pbd"	  :pbdD.pbdPenBonDet
									};
								$scope.pbdStnList.push(temp);
								console.log("length of $scope.pbdStnList = "+$scope.pbdStnList.length);
							var response = $http.post($scope.projectName+'/addPbd', pbdD);
							response.success(function(data, status, headers, config) {
								if(data.result==="success"){
									$scope.checkDetentionFlag=true;
									$scope.fetchPbdList();
									//$scope.pbdD.pbdFromStnTemp= "";
									//$scope.pbdD.pbdToStnTemp = "";
									//$scope.pbdD.pbdStartDt = "";
									//$scope.pbdD.pbdEndDt = "";
									$scope.pbdD.pbdFromDay = "";
									$scope.pbdD.pbdToDay = "";
									//$scope.pbdD.pbdVehicleType = "";
									$scope.pbdD.pbdHourNDay = "";
									$scope.pbdD.pbdAmt = "";
									
									$('input[name=dFromstationName]').attr('checked',false);
									$('input[name=dTostationName]').attr('checked',false);
									$('input[name="vtDetentionName"]').attr('checked',false);
								}else{
									console.log(data);
								}
							});
							response.error(function(data, status, headers, config) {
								$scope.errorToast(data.result);
							});
						}
					}
				}
			
			$scope.fetchPbdList = function(){
			 	var response = $http.get($scope.projectName+'/fetchPbdList');
			 	response.success(function(data, status, headers, config){
				 		if(data.result==="success"){
				 			//$scope.removeAllCTS();
				 			$scope.pbdList = data.list;
				 			//$scope.getBranchData();	
				 			$scope.pbdFlag=true;
				 		}else{
				 			console.log(data.result);
				 			//$scope.removeAllCTS();
				 			//$scope.getBranchData();	
				 			$scope.checkPenalty=false;
				 			$scope.checkBonus=false;
				 			$scope.checkDetention=false;
				 			$scope.pbdFlag=false;
				 		}
			 	});
			 	response.error(function(data, status, headers, config) {
			 		$scope.errorToast(data.result);
			 	});
			}
		
			$scope.removePbd = function(pbd) {
				console.log("enter into removePbd function");
				var response = $http.post($scope.projectName+'/removePbd',pbd);
				response.success(function(data, status, headers, config){
					for(var i=0;i<$scope.pbdStnList.length;i++){
						if(($scope.pbdStnList[i].ToStnTemp === pbd.pbdToStn) && ($scope.pbdStnList[i].FromStnTemp === pbd.pbdFromStn) && ($scope.pbdStnList[i].pbd === pbd.pbdPenBonDet)){
							$scope.pbdStnList.splice(i,1);
							i--;
						}
					}
					console.log($scope.pbdStnList.length);
					$scope.fetchPbdList();
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
				});
		    }
			
		
			$scope.removeAllPbd = function() {
					var response = $http.post($scope.projectName+'/removeAllPbd');
					response.success(function(data, status, headers, config) {
						if($scope.pbdStnList.length > 0){
							for(var i=0;i<$scope.pbdStnList.length;i++){
								$scope.pbdStnList.splice(i,1);
								i--
							}
						}
						console.log($scope.pbdStnList.length);
						$scope.fetchPbdList();
				
					});
					response.error(function(data, status, headers, config) {
						$scope.errorToast(data.result);
					});
	    		}
		
			$scope.saveproducttype = function(ProductTypeDB1Form,pt){
				$('div#AddProductTypeDB').dialog("destroy");
				//$scope.ProductTypeFlag1=true;
				$scope.ProductTypeFlag=true;
				$scope.AddProductTypeFlag=true;
			//	$scope.Qflag=true;
				pt.ptName=pt.ptName.toUpperCase();
				console.log("pt==========="+pt.ptName);
				if($.inArray(pt.ptName,$scope.ptList)!== -1){
					$scope.alertToast("Name already exists");
					$scope.pt.ptName="";
				}else{
					var response = $http.post($scope.projectName+'/saveproducttype',pt);
					response.success(function(data, status, headers, config) {
						if(data.result==="success"){
							$scope.successToast(data.result);
							$scope.getProductName();
							$scope.pt.ptName="";
						}else{
						$scope.errorToast(data.result);
						}
					});
					response.error(function(data, status, headers, config) {
						$scope.errorToast(data.result);
					});
				}	
			}
		
			$scope.savevehicletype = function(VehicleForm,vt){
				$scope.VehicleTypeFlag1=true;
				console.log("enter into saveVehicleType function--->");
				$('div#addVehicleTypeDB1').dialog("destroy");
				vt.vtServiceType=vt.vtServiceType.toUpperCase();
				vt.vtVehicleType=vt.vtVehicleType.toUpperCase();
				$scope.code = vt.vtServiceType+vt.vtVehicleType;
				if($scope.vtList.length>0){
					console.log("if list exists");
					for(var i=0;i<$scope.vtList.length;i++){
						$scope.type[i] = $scope.vtList[i].vtServiceType+$scope.vtList[i].vtVehicleType;
						$scope.type.push($scope.vtList[i].vtServiceType+$scope.vtList[i].vtVehicleType);
					}
					if(VehicleForm.vtGuaranteeWt.$invalid){
						$scope.alertToast("Enter correct guarantee weight");
					}else if(VehicleForm.vtLoadLimit.$invalid){
						$scope.alertToast("Load limit can't be greater than 7 digits");
					}else if(VehicleForm.vtVehicleType.$invalid){
						$scope.alertToast("Enter correct vehicle type");
					}else if(VehicleForm.vtServiceType.$invalid){
						$scope.alertToast("Enter correct service type");
					}else if($.inArray($scope.code,$scope.type)!==-1){
						$scope.alertToast("Name already exists");
						$scope.vt="";
					}else {
					var response = $http.post($scope.projectName+'/saveVehicleType',vt);
					response.success(function(data, status, headers, config) {
						if(data.result==="success"){
							$scope.successToast(data.result);
							$scope.getVehicleTypeCode();
							$scope.vt="";
						}else{
							$scope.errorToast(data.result);
						}
					});
					response.error(function(data, status, headers, config) {
						$scope.errorToast(data.result);
					});
				}
				}else{
					console.log("if list does not exist");
					if(VehicleForm.vtGuaranteeWt.$invalid){
						$scope.alertToast("Enter correct guarantee weight");
					}else if(VehicleForm.vtLoadLimit.$invalid){
						$scope.alertToast("Load limit can't be greater than 7 digits");
					}else if(VehicleForm.vtVehicleType.$invalid){
						$scope.alertToast("Enter correct vehicle type");
					}else if(VehicleForm.vtServiceType.$invalid){
						$scope.alertToast("Enter correct service type");
					}else if($.inArray($scope.code,$scope.type)!==-1){
						$scope.alertToast("Name already exists");
						$scope.vt="";
					}else {
					var response = $http.post($scope.projectName+'/saveVehicleType',vt);
					response.success(function(data, status, headers, config) {
						if(data.result==="success"){
							$scope.successToast(data.result);
							$scope.getVehicleTypeCode();
							$scope.vt="";	
						}else{
							$scope.errorToast(data.result);	
						}
					});
					response.error(function(data, status, headers, config) {
						$scope.errorToast(data.result);
					});
				}
			}			
		}
		 
			$scope.getCustomerData = function(){
				var response = $http.post($scope.projectName+'/getCustomerData');
				response.success(function(data, status, headers, config){
					if(data.result==="success"){
						$scope.customerList = data.list;
						$scope.getVehicleTypeCode();
					}else{
						console.log(data);
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
				});
			}
	  
	   
			$scope.getBranchData = function(){
				var response = $http.post($scope.projectName+'/getBranchData');
				response.success(function(data, status, headers, config){
					if(data.result==="success"){
						$scope.branchList = data.list;
						$scope.getCustomerData();
					}else{
						console.log(data);
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
				});
			}
	   
			$scope.getStateData = function(){
				var response = $http.post($scope.projectName+'/getStateData');
				response.success(function(data, status, headers, config){
					if(data.result==="success"){
						$scope.listState = data.list;
					}else{
						console.log(data);
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
				});
			}
	  
	   
			$scope.getStationData = function(){
				var response = $http.post($scope.projectName+'/getStationData');
				response.success(function(data, status, headers, config){
					if(data.result==="success"){
						$scope.stationList = data.list;
						if($scope.tempStList.length > 0){
							$scope.stationList = [];
							$scope.stationList = $scope.tempStList;
						}
						for(var i=0;i<$scope.stationList.length;i++){
							$scope.type[i] = $scope.stationList[i].stnName+$scope.stationList[i].stnPin;
							$scope.type.push($scope.stationList[i].stnName+$scope.stationList[i].stnPin);
						}
						$scope.getStateData();
					}else{
						console.log(data);
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
				});
			}
	  
	   
			$scope.getVehicleTypeCode = function(){
				var response = $http.post($scope.projectName+'/getVehicleTypeCode');
				response.success(function(data, status, headers, config){
					if(data.result==="success"){
						$scope.vtList = data.list;
						$scope.getProductName();
					}else{
						console.log(data);   
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
				});
			}
	   
	   
			$scope.getProductName = function(){
				var response = $http.post($scope.projectName+'/getProductName');
				response.success(function(data, status, headers, config){
					if(data.result==="success"){
						$scope.ptList = data.list;
						$scope.getStationData();
					}else{
						console.log(data);
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
				});
			}
	  
	   
			$scope.EditContract = function(ContCode){
				console.log("The entered contcode is "+ContCode);
				$scope.code = ContCode.substring(0,3);
				console.log("The substring is "+$scope.code);
			
				if($scope.code==="dly")
				{
					$scope.editDlyCont = false;
					$scope.editRegCont = true;
		
					var response = $http.post($scope.projectName+'/EditDailyContract',ContCode);
					response.success(function(data, status, headers, config) {
						console.log(data);
						$scope.dailyContract = data.dailyContract;
						
					});	
					response.error(function(data, status, headers, config) {
						$scope.errorToast(data.result);
					});
				}else if($scope.code==="reg"){
					$scope.editRegCont = false;
					$scope.editDlyCont = true;
					var response = $http.post($scope.projectName+'/regularcontractdetails',ContCode);
					response.success(function(data, status, headers, config) {
						$scope.regularContract = data.regularContract;	
					});
					response.error(function(data, status, headers, config) {
						$scope.errorToast(data.result);
					});
				}
			}			
			
			$scope.EditDailyContractSubmit = function(dailyContract){
				console.log("enter into EditDailyContractSubmit function--->"+dailyContract.branchCode);
				$scope.dlyContCode="";
				var response = $http.post($scope.projectName+'/EditDailyContractSubmit', dailyContract);
				response.success(function(data, status, headers, config) {
					console.log(data.result);
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
				});
			}
	   
	   
			$scope.saveContToStnW = function(ContToStationFormW,cts,dlyCnt,metricType){
				console.log("Enter into saveContToStnW function"+ContToStationFormW.$invalid);
				$('div#contToStationW').dialog("destroy");
				$scope.contToStationFlagW = true;
				$scope.ctsflagW=true;
				//$scope.ctsToStnTemp=cts.ctsToStnTemp;
				console.log(cts.ctsToStnTemp);
				$scope.dlyContFrmStn=dlyCnt.dlyContFromStationTemp;
				
				/*if(metricType==="Ton"){
					cts.ctsAdditionalRate=(cts.ctsAdditionalRate/907.185);
					console.log(cts.ctsAdditionalRate);
				}*/
				
				if($scope.ctsList.length>0){
					console.log("if list exists"+$scope.ctsList.length);
					if(ContToStationFormW.ctsToStnTemp.$invalid){
						$scope.alertToast("Please enter To Station");
					}else if(ContToStationFormW.ctsVehicleType.$invalid){
						$scope.alertToast("Please enter Vehicle Type");
					} else if(cts.ctsFromWt>cts.ctsToWt){
						$scope.alertToast("From Weight cannot be greater than To Weight");
					}else {
						cts.ctsToStn=cts.ctsToStn.toUpperCase();
						cts.ctsVehicleType=cts.ctsVehicleType.toUpperCase();
						$scope.ctsTemp= cts.ctsToStn+cts.ctsVehicleType;
						
						for(var i=0;i<$scope.ctsList.length;i++){
							console.log($scope.ctsList[i].ctsToStn);
							$scope.type[i] = $scope.ctsList[i].ctsToStn+$scope.ctsList[i].ctsVehicleType;
							$scope.type.push($scope.ctsList[i].ctsToStn+$scope.ctsList[i].ctsVehicleType);
						}
						if($.inArray($scope.ctsTemp,$scope.type)!==-1){
							$scope.alertToast("Already exists");
							/*cts.ctsToStn="";
							cts.ctsToStnTemp="";
							cts.ctsRate="";
							cts.ctsFromWt="";
							cts.ctsToWt="";
							cts.ctsVehicleType="";
							cts.ctsAdditionalRate="";*/
						}else{
							
							var temp = {
									"ToStn" : 	cts.ctsToStn,
									"ToStnTemp" : cts.ctsToStnTemp,
									"vehicleType" :cts.ctsVehicleType
								};
							
							$scope.contToStnTemp.push(temp);
							console.log("lenght of $scope.contToStnTemp = "+$scope.contToStnTemp.length);
							var response = $http.post($scope.projectName+'/addCTS',cts);	   
							response.success(function(data, status, headers, config) {
									if(data.result==="success"){
										console.log("from server ---->"+data.result);
										$scope.fetchCTSList();	
										$scope.ContToStnFlag=true;
									/*	cts.ctsToStn="";
										cts.ctsToStnTemp="";
										cts.ctsRate="";
										cts.ctsFromWt="";
										cts.ctsToWt="";
										cts.ctsVehicleType="";
										cts.ctsAdditionalRate="";*/
										$('input[name=toStations]').attr('checked',false);
										$('input[name=VTName]').attr('checked',false);
									}else{
										console.log(data);
									}
								});
								response.error(function(data, status, headers, config) {
									$scope.errorToast(data.result);
								});	
							}			
						}
				}else{
					console.log("if list does not exists");
					if(ContToStationFormW.ctsToStnTemp.$invalid){
						$scope.alertToast("Please enter To Station");
					}else if(ContToStationFormW.ctsVehicleType.$invalid){
						$scope.alertToast("Please enter Vehicle Type");
					} else if(cts.ctsFromWt>cts.ctsToWt){
						$scope.alertToast("From Weight cannot be greater than To Weight");
					}else{
						cts.ctsToStn=cts.ctsToStn.toUpperCase();
						cts.ctsVehicleType=cts.ctsVehicleType.toUpperCase();
						
						var temp = {
								"ToStn" : 	cts.ctsToStn,
								"ToStnTemp" : cts.ctsToStnTemp,
								"vehicleType" :cts.ctsVehicleType
							};
						$scope.contToStnTemp.push(temp);
						console.log("lenght of $scope.contToStnTemp = "+$scope.contToStnTemp.length);
						var response = $http.post($scope.projectName+'/addCTS',cts);	   
							response.success(function(data, status, headers, config) {
								if(data.result==="success"){
									console.log("from server ---->"+data.result);
									$scope.fetchCTSList();	
									$scope.ContToStnFlag=true;
							/*		cts.ctsToStn="";
									cts.ctsToStnTemp="";
									cts.ctsRate="";
									cts.ctsFromWt="";
									cts.ctsToWt="";
									cts.ctsVehicleType="";
									cts.ctsAdditionalRate="";*/
									$('input[name=toStations]').attr('checked',false);
									$('input[name=VTName]').attr('checked',false);
								}else{
									console.log(data);
								}
							});
							response.error(function(data, status, headers, config) {
								$scope.errorToast(data.result);
							});	
						}	
					}
				}
			
			$scope.saveContToStnQ = function(ContToStationFormQ,cts,dlyCnt){
				console.log("Enter into saveContToStnQ function"+ContToStationFormQ.$invalid);
				$('div#contToStationQ').dialog("destroy");
				$scope.contToStationFlagQ = true;
				$scope.ctsflagQ=true;
				//$scope.ContToStnFlag=true;
				//$scope.ToStationQ=cts.ctsToStnTemp;
				$scope.dlyContFrmStn=dlyCnt.dlyContFromStationTemp;
				
			
				if($scope.ctsList.length>0){
					console.log("if list exists"+$scope.ctsList.length);
					if(ContToStationFormQ.ctsToStnTemp.$invalid){
						$scope.alertToast("Please enter To Station");
					}else if(ContToStationFormQ.ctsVehicleType.$invalid){
						$scope.alertToast("Please enter Vehicle Type");
					}else if(ContToStationFormQ.ctsProductType.$invalid){
						$scope.alertToast("Please enter Product Type");
					}else if(cts.ctsFromWt>cts.ctsToWt){
						$scope.alertToast("From Weight cannot be greater than To Weight");
					}else{
						cts.ctsVehicleType=cts.ctsVehicleType.toUpperCase();
						cts.ctsToStn=cts.ctsToStn.toUpperCase();
						cts.ctsProductType=cts.ctsProductType.toUpperCase();
						$scope.ctsTemp= cts.ctsToStn+cts.ctsProductType;
						
						for(var i=0;i<$scope.ctsList.length;i++){
							console.log($scope.ctsList[i].ctsToStn);
							$scope.type[i] = $scope.ctsList[i].ctsToStn+$scope.ctsList[i].ctsProductType;
							$scope.type.push($scope.ctsList[i].ctsToStn+$scope.ctsList[i].ctsProductType);
						}
						if($.inArray($scope.ctsTemp,$scope.type)!==-1){
							$scope.alertToast("Already exists");
						/*	cts.ctsToStn="";
							cts.ctsToStnTemp="";
							cts.ctsRate="";
							cts.ctsFromWt="";
							cts.ctsToWt="";
							cts.ctsVehicleType="";*/
							
						}else{
							var temp = {
									"ToStn" : 	cts.ctsToStn,
									"ToStnTemp" : cts.ctsToStnTemp,
									"vehicleType" :cts.ctsVehicleType
							};
							$scope.contToStnTemp.push(temp);
							console.log("lenght of $scope.contToStnTemp = "+$scope.contToStnTemp.length);
							
							var response = $http.post($scope.projectName+'/addCTS', cts);	   
								response.success(function(data, status, headers, config) {
									if(data.result==="success"){
										console.log("from server ---->"+data.result);
										$scope.fetchCTSList();	
										$scope.ContToStnFlag=true;
									/*	cts.ctsToStn="";
										cts.ctsToStnTemp="";
										cts.ctsRate="";
										cts.ctsFromWt="";
										cts.ctsToWt="";
										cts.ctsVehicleType="";
										cts.ctsProductType="";*/
										$('input[name=toStations]').attr('checked',false);
										$('input[name=VTName]').attr('checked',false);
									}else{
										console.log(data);
									}
								});
								response.error(function(data, status, headers, config) {
									$scope.errorToast(data.result);
								});	
							}
						} 
				}else{
					console.log("if list does not exists");
					if(ContToStationFormQ.ctsToStnTemp.$invalid){
						$scope.alertToast("Please enter To Station");
					}else if(ContToStationFormQ.ctsVehicleType.$invalid){
						$scope.alertToast("Please enter Vehicle Type");
					}else if(ContToStationFormQ.ctsProductType.$invalid){
						$scope.alertToast("Please enter Product Type");
					}else if(cts.ctsFromWt>cts.ctsToWt){
						$scope.alertToast("From Weight cannot be greater than To Weight");
					}else{
						cts.ctsVehicleType=cts.ctsVehicleType.toUpperCase();
						cts.ctsToStn=cts.ctsToStn.toUpperCase();
						cts.ctsProductType=cts.ctsProductType.toUpperCase();
						
						var temp = {
								"ToStn" : 	cts.ctsToStn,
								"ToStnTemp" : cts.ctsToStnTemp,
								"vehicleType" :cts.ctsVehicleType
							};
						
						$scope.contToStnTemp.push(temp);
						console.log("lenght of $scope.contToStnTemp = "+$scope.contToStnTemp.length);
						var response = $http.post($scope.projectName+'/addCTS', cts);	   
							response.success(function(data, status, headers, config) {
								if(data.result==="success"){
									console.log("from server ---->"+data.result);
									$scope.fetchCTSList();	
									$scope.ContToStnFlag=true;
									/*cts.ctsToStn="";
									cts.ctsToStnTemp="";
									cts.ctsRate="";
									cts.ctsFromWt="";
									cts.ctsToWt="";
									cts.ctsVehicleType="";
									cts.ctsProductType="";*/
									$('input[name=toStations]').attr('checked',false);
									$('input[name=VTName]').attr('checked',false);
								}else{
									console.log(data);
								}
							});
							response.error(function(data, status, headers, config) {
								$scope.errorToast(data.result);
							});	
						}	
					}
				}
			
			$scope.fetchCTSList = function(){
				console.log("Entr into fetchCTSList function");
				var response = $http.get($scope.projectName+'/fetchCTSList');
				response.success(function(data, status, headers, config){
					if(data.result==="success"){
						$scope.ctsList = data.list;
						console.log($scope.dlyCnt.dlyContType);
						if($scope.dlyCnt.dlyContType==='W'){
							$scope.ctsFlagW=true;	 
						}else if($scope.dlyCnt.dlyContType==='Q'){
							$scope.ctsFlagQ=true;	 
						}
						$scope.getBranchData();
					}else{
						console.log(data.result);
						$scope.getBranchData();
						$scope.ctsList = data.list;
						$scope.ctsFlagW=false;
						$scope.ctsFlagQ=false;
						 if($scope.dlyCnt.dlyContType==='W' || $scope.dlyCnt.dlyContType==='Q'){
							 console.log($scope.dlyCnt.dlyContType); 
							 $scope.ContToStnFlag=false;
						 }
					}		
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
				});
			}
		
			$scope.removeCTS = function(cts) {
				console.log("Entr into removeCTS function");
				var response = $http.post($scope.projectName+'/removeCTS',cts);
				response.success(function(data, status, headers, config) {
					console.log(cts.ctsToStn);
					
					for(var i=0;i<$scope.contToStnTemp.length;i++){
						if(($scope.contToStnTemp[i].ToStn === cts.ctsToStn) && ($scope.contToStnTemp[i].vehicleType === cts.ctsVehicleType)){
							$scope.contToStnTemp.splice(i,1);
							i--;
						}
					}
					console.log($scope.contToStnTemp.length);
					$scope.fetchCTSList();
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
				});
			}
		
			$scope.removeAllCTS = function() {
				console.log("Entr into removeAllCts function");
				var response = $http.post($scope.projectName+'/removeAllCTS');
				response.success(function(data, status, headers, config) {
					$scope.ctsflagW=false;
					$scope.ctsflagQ=false;
					if($scope.contToStnTemp.length > 0){
						for(var i=0;i<$scope.contToStnTemp.length;i++){
							$scope.contToStnTemp.splice(i,1);
							i--;
						}
					}
					console.log($scope.contToStnTemp.length);
					$scope.fetchCTSList();
					console.log(data.result);
					
				});
				response.error(function(data, status, headers, config) {
					$scope.errorToast(data.result);
				});
			}
		
			$scope.sendMetricType=function(metricType){
				console.log(metricType);
				if(metricType==="" || metricType===null || angular.isUndefined(metricType)){
					$scope.alertToast("Please enter Metric type");
				}else{
					var response = $http.post($scope.projectName+'/sendMetricType',metricType);
					response.success(function(data, status, headers, config) {
						$scope.alertToast(data.result);
					});
					response.error(function(data, status, headers, config) {
						$scope.errorToast(data.result);
					});
				}
			}
		
			$scope.saveNewStation=function(StationForm,station){
				console.log("Entered into Submit function");
				$scope.station.stnName = station.stnName.toUpperCase();
				$scope.codeState = station.stnName+station.stnPin;
				console.log($scope.codeState);
				if(StationForm.$invalid){
					if(StationForm.stnName.$invalid){
						$scope.alertToast("Please enter station name between 3-40 characters...");
						$scope.station.stnName="";
					}else if(StationForm.stnDistrict.$invalid){
						$scope.alertToast("Please enter station district between 3-40 characters...");
						$scope.station.stnDistrict="";
					}else if(StationForm.stateCode.$invalid){
						$scope.alertToast("Please enter state code...");
					}else if(StationForm.stnPin.$invalid){
						$scope.alertToast("Please enter station pin of 6 digits...");
						$scope.station.stnPin="";
					}
				}else{
					if($.inArray($scope.codeState,$scope.type)!==-1){
						$scope.alertToast("Station already exists...");
						$scope.station.stnName="";
						$scope.station.stnDistrict="";
						$scope.station.stateCode="";
						$scope.station.stnPin="";
					}else{
						var response = $http.post($scope.projectName+'/submitStation',station);
						response.success(function(data, status, headers, config) {
							if(data.result==="success"){
								$scope.successToast(data.result);
								$scope.station.stnName="";
								$scope.station.stnDistrict="";
								$scope.station.stateCode="";
								$scope.station.stnPin="";
								$('input[name=stateName]').attr('checked',false);
								$scope.getStationData();
								$('div#addStation').dialog('close');
							}else{
								console.log(data);
							}
						});
						response.error(function(data, status, headers, config) {
							$scope.errorToast(data);
						});
					}
				}	
			}
			
			
			$scope.exit = function(){
				console.log("enter into exit function");
				$('div#showFaCodeId').dialog('close');
				$scope.dlyFaCode = "";
			}

			$scope.fetch = function(){
				$scope.removeAllRbkm();
				$scope.removeAllPbd();
				$scope.removeAllCTS();
			}
			
			if($scope.operatorLogin === true || $scope.superAdminLogin === true){
				$scope.fetch();
			 }else if($scope.logoutStatus === true){
				 $location.path("/");
			 }else{
				 console.log("****************");
			 } 
			
	
}]);