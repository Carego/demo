package com.mylogistics.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.CashStmtStatusDAO;
import com.mylogistics.DAO.bank.AtmCardDAO;
import com.mylogistics.DAO.bank.BankMstrDAO;
import com.mylogistics.model.Branch;
import com.mylogistics.model.CashStmtStatus;
import com.mylogistics.model.User;
import com.mylogistics.model.bank.AtmCardMstr;
import com.mylogistics.model.bank.BankMstr;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.VoucherService;

@Controller
public class AtmVoucherCntlr {
	
	public static final Logger logger = Logger.getLogger(AtmVoucherCntlr.class);

	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private BranchDAO branchDAO;
	
	@Autowired
	private CashStmtStatusDAO cashStmtStatusDAO;
	
	@Autowired
	private BankMstrDAO bankMstrDAO;
	
	@Autowired
	private AtmCardDAO atmCardDAO;
	
	
	@RequestMapping(value = "/getVouchDetFrAtm" , method = RequestMethod.POST)  
	public @ResponseBody Object getVouchDetFrAtm() {  
		System.out.println("Enter into getVouchDetFrAtm---->");
		Map<String, Object> map = new HashMap<>();	
		User user = (User) httpSession.getAttribute("currentUser");
		if(user != null){
			String branchCode = user.getUserBranchCode();
			System.out.println("branchCode = "+branchCode);
			List<Branch> branchList = branchDAO.retrieveBranch(branchCode);
			if(!branchList.isEmpty()){
				CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranch(branchCode);
				List<AtmCardMstr> atmList = new ArrayList<>();
				List<String> atmCodeList = new ArrayList<>();
				Branch branch = branchDAO.getBranchBMstrLt(branchList.get(0).getBranchId());
				atmList = atmCardDAO.getAtmByBrh(branch.getBranchId());
				//atmList = branch.getAtmCardMstrList();
				System.out.println("atmList === "+atmList.size());
				if(!atmList.isEmpty()){
					for(int i=0;i<atmList.size();i++){
						atmCodeList.add(atmList.get(i).getAtmCardNo());
					}
				}
				System.out.println("Size of atmCodeList ----->>> "+atmCodeList.size());
				System.out.println("cashStmtStatus ----->>> "+ cashStmtStatus);
				if(cashStmtStatus != null && !atmCodeList.isEmpty()){
					System.out.println("Enter into If----->>>>");
					map.put(ConstantsValues.BRANCH, branchList.get(0));
					map.put(ConstantsValues.CASH_STMT_STATUS, cashStmtStatus);
					map.put(ConstantsValues.ATM_CODE_LIST, atmCodeList);
					//map.put(ConstantsValues.BANK_CODE_LIST, bankCodeList);
					map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				}else{
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}

		return map;
	}
	
	
	@RequestMapping(value = "/submitAtmVouch" , method = RequestMethod.POST)  
	public @ResponseBody Object submitAtmVouch(@RequestBody VoucherService voucherService) {  
		System.out.println("Enter into submitAtmVouch---->");
		Map<String, Object> map = cashStmtStatusDAO.saveAtmVoucher(voucherService);
		int vhNo = 0;
		vhNo = (int) map.get("vhNo");
		if(vhNo > 0){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	@RequestMapping(value = "/submitAtmVouchN" , method = RequestMethod.POST)  
	public @ResponseBody Object submitAtmVouchN(@RequestBody VoucherService voucherService) {  
		logger.info("Enter into submitAtmVouch---->");
		Map<String, Object> map = cashStmtStatusDAO.saveAtmVoucher(voucherService);
		int vhNo = (int) map.get("vhNo");
		if(vhNo > 0){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	//TODO BAck Data
	@RequestMapping(value = "/getVouchDetFrAtmBack" , method = RequestMethod.POST)  
	public @ResponseBody Object getVouchDetFrAtmBack() {  
		System.out.println("Enter into getVouchDetFrAtm---->");
		Map<String, Object> map = new HashMap<>();	
		User user = (User) httpSession.getAttribute("currentUser");
		if(user != null){
			String branchCode = user.getUserBranchCode();
			System.out.println("branchCode = "+branchCode);
			List<Branch> branchList = branchDAO.retrieveBranch(branchCode);
			if(!branchList.isEmpty()){
				CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranchBack(branchCode);
				List<AtmCardMstr> atmList = new ArrayList<>();
				List<String> atmCodeList = new ArrayList<>();
				Branch branch = branchDAO.getBranchBMstrLt(branchList.get(0).getBranchId());
				atmList = atmCardDAO.getAtmByBrh(branch.getBranchId());
				//atmList = branch.getAtmCardMstrList();
				System.out.println("atmList === "+atmList.size());
				if(!atmList.isEmpty()){
					for(int i=0;i<atmList.size();i++){
						atmCodeList.add(atmList.get(i).getAtmCardNo());
					}
				}
				System.out.println("Size of atmCodeList ----->>> "+atmCodeList.size());
				System.out.println("cashStmtStatus ----->>> "+ cashStmtStatus);
				if(cashStmtStatus != null && !atmCodeList.isEmpty()){
					System.out.println("Enter into If----->>>>");
					map.put(ConstantsValues.BRANCH, branchList.get(0));
					map.put(ConstantsValues.CASH_STMT_STATUS, cashStmtStatus);
					map.put(ConstantsValues.ATM_CODE_LIST, atmCodeList);
					//map.put(ConstantsValues.BANK_CODE_LIST, bankCodeList);
					map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				}else{
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}

		return map;
	}

	
	
}
