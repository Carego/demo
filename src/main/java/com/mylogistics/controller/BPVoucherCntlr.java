package com.mylogistics.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.CashStmtStatusDAO;
import com.mylogistics.DAO.CustomerDAO;
import com.mylogistics.DAO.FaBPromDAO;
import com.mylogistics.DAO.bank.BankMstrDAO;
import com.mylogistics.model.Branch;
import com.mylogistics.model.CashStmtStatus;
import com.mylogistics.model.Customer;
import com.mylogistics.model.User;
import com.mylogistics.model.bank.BankMstr;
import com.mylogistics.model.bank.ChequeLeaves;
import com.mylogistics.services.BPVoucherService;
import com.mylogistics.services.BPVoucherServiceImpl;
import com.mylogistics.services.BPromService;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.VoucherService;

@Controller
public class BPVoucherCntlr {

	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private BranchDAO branchDAO;
	
	@Autowired
	private CashStmtStatusDAO cashStmtStatusDAO;
	
	@Autowired
	private BankMstrDAO bankMstrDAO;
	
	@Autowired
	private CustomerDAO customerDAO;
	
	@Autowired
	private FaBPromDAO faBPromDAO;
	
	private BPVoucherService bpVoucherService = new BPVoucherServiceImpl();
	
	private static Logger logger = Logger.getLogger(BPVoucherCntlr.class);	
	
	@RequestMapping(value = "/getVDetFrBPV" , method = RequestMethod.POST)  
	public @ResponseBody Object getVDetFrBPV() {  
		System.out.println("Enter into getVDetFrBPV---->");
		Map<String, Object> map = new HashMap<>();	
		User user = (User) httpSession.getAttribute("currentUser");
		if(user != null){
			String branchCode = user.getUserBranchCode();
			List<Branch> branchList = branchDAO.retrieveBranch(branchCode);
			if(!branchList.isEmpty()){
				CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranch(branchCode);
				//List<String> bankCodeList = bankMstrDAO.getBankCodeList(branchCode);
				List<String> bankCodeList = new ArrayList<>();
				Branch branch = branchDAO.getBranchBMstrLt(branchList.get(0).getBranchId());
				List<BankMstr> bnkList = branch.getBankMstrList();
				System.out.println("bnkList === "+bnkList.size());
				if(!bnkList.isEmpty()){
					for(int i=0;i<bnkList.size();i++){
						bankCodeList.add(bnkList.get(i).getBnkFaCode());
					}
				}
				System.out.println("Size of bankCodeList ----->>> "+ bankCodeList.size());
				if(cashStmtStatus != null && !bankCodeList.isEmpty()){
					System.out.println("Enter into If----->>>>");
					map.put(ConstantsValues.BRANCH, branchList.get(0));
					map.put(ConstantsValues.CASH_STMT_STATUS, cashStmtStatus);
					map.put(ConstantsValues.BANK_CODE_LIST, bankCodeList);
					map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				}else{
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	
	@RequestMapping(value = "/getAllCustFrBPV" , method = RequestMethod.POST)  
	public @ResponseBody Object getAllCustFrBPV() {  
		System.out.println("Enter into getAllCustFrBPV---->");
		Map<String,Object> map = new HashMap<String,Object>();
		List<Customer> custList = customerDAO.getAllCustomer();
		if(!custList.isEmpty()){
			map.put("list",custList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/getCustBrFrBPV" , method = RequestMethod.POST)  
	public @ResponseBody Object getCustBrFrBPV(@RequestBody Map<String,Object> clientMap) {  
		System.out.println("Enter into getCustBrFrBPV---->");
		Map<String,Object> map = new HashMap<String,Object>();
		int custId = (int) clientMap.get("index");
		List<Branch> brList = customerDAO.getAllCustBranch(custId);
		if(!brList.isEmpty()){
			map.put("list",brList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/getChequeNoFrBPV" , method = RequestMethod.POST)  
	public @ResponseBody Object getChequeNoFrBPV(@RequestBody Map<String,String> clientMap){
		logger.info("Enter into getChequeNoFrBPV() : bankCode = "+clientMap.get("bankCode") +" : CType = "+clientMap.get("CType")+" : chqNo = "+clientMap.get("chqNo"));
		System.out.println("Enter into getChequeNoFrBPV---->");
		
		Map<String, Object> map = new HashMap<>();	
		String bankCode = clientMap.get("bankCode");
		String cType = clientMap.get("CType");
		String chqNo = clientMap.get("chqNo");
		
		char CType = cType.charAt(0);
		System.out.println("value of CType = "+CType);		
		List<ChequeLeaves> chqList = bankMstrDAO.getAPChqLByBnkC(bankCode, CType, chqNo);
		System.out.println("Chq List : "+chqList.size());
		if(!chqList.isEmpty()){
			System.out.println("***************** 1");
			map.put("list",chqList);			
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else{
			System.out.println("***************** 2");
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	
	@RequestMapping(value = "/addBPDetails" , method = RequestMethod.POST)  
	public @ResponseBody Object addBPDetails(@RequestBody BPromService bPromService) {  
		System.out.println("Enter into addBPDetails---->");
		Map<String, Object> map = new HashMap<>();	
		bpVoucherService.addBPromSer(bPromService);
		return map;
	}	
	
	
	@RequestMapping(value = "/fetchBPDetails" , method = RequestMethod.POST)  
	public @ResponseBody Object fetchBPDetails() {  
		System.out.println("Enter into fetchBPDetails---->");
		Map<String, Object> map = new HashMap<>();	
		List<BPromService> bpList = bpVoucherService.getAllBPromSer();
		map.put("list",bpList);
		return map;
	}	
	
	@RequestMapping(value = "/removeBPV" , method = RequestMethod.POST)  
	public @ResponseBody Object removeBPV(@RequestBody Map<String,Object> clientMap) {  
		System.out.println("Enter into removeBPV---->");
		Map<String, Object> map = new HashMap<>();
		int index = (int) clientMap.get("index");
		bpVoucherService.remove(index);
		return map;
	}
	
	
	@RequestMapping(value = "/submitBPVoucher" , method = RequestMethod.POST)
	public @ResponseBody Object submitBPVoucher(@RequestBody VoucherService voucherService) {  
		System.out.println("enter into submitBPVoucher function");
		Map<String,Object> map = new HashMap<String,Object>();
		if(voucherService.getVoucherType().equalsIgnoreCase(ConstantsValues.BP_VOUCHER)){
			 map = saveBPVoucher(voucherService);
			 int vhNo = (int) map.get("vhNo");
			if(vhNo>0){
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}
		return map;
	}
	
	
	public Map<String,Object> saveBPVoucher(VoucherService voucherService){
		System.out.println("Enter into saveBPVoucher----->>>");
		Map<String,Object> map = new HashMap<String,Object>();
		char payBy = voucherService.getPayBy();
		System.out.println("value of payBy = "+payBy);
		if(payBy == 'C'){
			map = faBPromDAO.saveBPVoucByCash(voucherService,bpVoucherService);
			bpVoucherService.deleteAllBPromSer();
		}else if(payBy == 'Q'){
			map = faBPromDAO.saveBPVoucByChq(voucherService, bpVoucherService);
			bpVoucherService.deleteAllBPromSer();
		}else{
			map.put("vhNo",0);
			map.put("tvNo","0");
			System.out.println("invalid payment method");
		}
		
		return map;
	}
	
	//TODO Back Data
	@RequestMapping(value = "/getVDetFrBPVBack" , method = RequestMethod.POST)  
	public @ResponseBody Object getVDetFrBPVBack() {  
		System.out.println("Enter into getVDetFrBPV---->");
		Map<String, Object> map = new HashMap<>();	
		User user = (User) httpSession.getAttribute("currentUser");
		if(user != null){
			String branchCode = user.getUserBranchCode();
			List<Branch> branchList = branchDAO.retrieveBranch(branchCode);
			if(!branchList.isEmpty()){
				CashStmtStatus cashStmtStatus = cashStmtStatusDAO.getLastCssByBranchBack(branchCode);
				//List<String> bankCodeList = bankMstrDAO.getBankCodeList(branchCode);
				List<String> bankCodeList = new ArrayList<>();
				Branch branch = branchDAO.getBranchBMstrLt(branchList.get(0).getBranchId());
				List<BankMstr> bnkList = branch.getBankMstrList();
				System.out.println("bnkList === "+bnkList.size());
				if(!bnkList.isEmpty()){
					for(int i=0;i<bnkList.size();i++){
						bankCodeList.add(bnkList.get(i).getBnkFaCode());
					}
				}
				System.out.println("Size of bankCodeList ----->>> "+ bankCodeList.size());
				if(cashStmtStatus != null && !bankCodeList.isEmpty()){
					System.out.println("Enter into If----->>>>");
					map.put(ConstantsValues.BRANCH, branchList.get(0));
					map.put(ConstantsValues.CASH_STMT_STATUS, cashStmtStatus);
					map.put(ConstantsValues.BANK_CODE_LIST, bankCodeList);
					map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				}else{
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	
}
