package com.mylogistics.constants;

import java.util.ArrayList;
import java.util.List;

import com.mylogistics.model.ArrivalReport;

public class MemoCNTS {
	
	public final static String MEMO_MEMO_ID = "memoId";
	
	public final static String MEMO_MEMO_NO = "memoNo";
	
	public final static String MEMO_MEMO_DATE = "memoDate";
	
	public final static String MEMO_FROM_BRANCH = "fromBranch";
	
	public final static String MEMO_TO_BRANCH = "toBranch";
	
	public final static String MEMO_BRANCH_CODE = "branchCode";
	
	public final static String MEMO_USER_CODE = "userCode";
	
	public final static String MEMO_CREATION_TS= "creationTS";
	
	public final static String MEMO_ARRIVAL_REPORTS = "arrivalReports";
	
}