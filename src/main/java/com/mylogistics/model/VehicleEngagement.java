package com.mylogistics.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="vehicleengagement")
public class VehicleEngagement {
	
	@Id
	@Column(name="vEId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int vEId;
	
	@ManyToOne
	@JoinColumn(name = "campainId")
	private CampaignResults campaignResults;
	
	@Column
	private String vehicleNo;
	
	@Column
	private String driverName;
	
	@Column
	private String driverNo;
	
	@Column
	private String consent;
	
	@Column
	private String driverAlternateNo;
	
	@Column
	private String alternateConsent;
	
	@Column
	private String network;
	
	@Column
	private String alternateNetwork;
	
	
	@Column(name="placeReqStatus")
	private String placeReqStatus;
	
	@Column(name="vehicleStatus")
	private String vehicleStatus;
	
	@Column(name="newVeId", columnDefinition="int default 0")
	private int newVeId;
	
	public int getvEId() {
		return vEId;
	}
	public void setvEId(int vEId) {
		this.vEId = vEId;
	}
	public CampaignResults getCampaignResults() {
		return campaignResults;
	}
	public void setCampaignResults(CampaignResults campaignResults) {
		this.campaignResults = campaignResults;
	}
	public String getVehicleNo() {
		return vehicleNo;
	}
	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}
	public String getDriverName() {
		return driverName;
	}
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}
	public String getDriverNo() {
		return driverNo;
	}
	public void setDriverNo(String driverNo) {
		this.driverNo = driverNo;
	}
	
	public String getConsent() {
		return consent;
	}
	public void setConsent(String consent) {
		this.consent = consent;
	}
	public String getDriverAlternateNo() {
		return driverAlternateNo;
	}
	public void setDriverAlternateNo(String driverAlternateNo) {
		this.driverAlternateNo = driverAlternateNo;
	}
	public String getAlternateConsent() {
		return alternateConsent;
	}
	public void setAlternateConsent(String alternateConsent) {
		this.alternateConsent = alternateConsent;
	}
	public String getNetwork() {
		return network;
	}
	public void setNetwork(String network) {
		this.network = network;
	}
	public String getAlternateNetwork() {
		return alternateNetwork;
	}
	public void setAlternateNetwork(String alternateNetwork) {
		this.alternateNetwork = alternateNetwork;
	}
	public String getPlaceReqStatus() {
		return placeReqStatus;
	}
	public void setPlaceReqStatus(String placeReqStatus) {
		this.placeReqStatus = placeReqStatus;
	}
	public String getVehicleStatus() {
		return vehicleStatus;
	}
	public void setVehicleStatus(String vehicleStatus) {
		this.vehicleStatus = vehicleStatus;
	}
	public int getNewVeId() {
		return newVeId;
	}
	public void setNewVeId(int newVeId) {
		this.newVeId = newVeId;
	}
	
	
}
