package com.mylogistics.model;

import java.sql.Date;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "customerrights")
public class CustomerRights {

	@Id
	@Column(name="crhId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int crhId;
	
	@Column(name="crhCustCode")
	private String crhCustCode;
	
	@Column(name="crhAdvAllow")
	private String crhAdvAllow;
	
	@Column(name="crhIsCustomer")
	private String crhIsCustomer;
	
	@Column(name="crhIsConsignor")
	private String crhIsConsignor;
	
	@Column(name="crhIsConsignee")
	private String crhIsConsignee;
	
	@Column(name="crhAdvValidDt")
	private Date crhAdvValidDt;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="bCode")
	private String bCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;

	public int getCrhId() {
		return crhId;
	}

	public void setCrhId(int crhId) {
		this.crhId = crhId;
	}

	public String getCrhCustCode() {
		return crhCustCode;
	}

	public void setCrhCustCode(String crhCustCode) {
		this.crhCustCode = crhCustCode;
	}

	public String getCrhAdvAllow() {
		return crhAdvAllow;
	}

	public void setCrhAdvAllow(String crhAdvAllow) {
		this.crhAdvAllow = crhAdvAllow;
	}

	public String getCrhIsCustomer() {
		return crhIsCustomer;
	}

	public void setCrhIsCustomer(String crhIsCustomer) {
		this.crhIsCustomer = crhIsCustomer;
	}

	public String getCrhIsConsignor() {
		return crhIsConsignor;
	}

	public void setCrhIsConsignor(String crhIsConsignor) {
		this.crhIsConsignor = crhIsConsignor;
	}

	public String getCrhIsConsignee() {
		return crhIsConsignee;
	}

	public void setCrhIsConsignee(String crhIsConsignee) {
		this.crhIsConsignee = crhIsConsignee;
	}

	public Date getCrhAdvValidDt() {
		return crhAdvValidDt;
	}

	public void setCrhAdvValidDt(Date crhAdvValidDt) {
		this.crhAdvValidDt = crhAdvValidDt;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	
}
