<div ng-show="operatorLogin || superAdminLogin">
<title>CNMT</title>
<div class="row">
	<form class="col s12 card"
		style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);"
		name="CnmtForm" ng-submit="submitCnmt(CnmtForm,cnmt)">
		<div class="row">
			<div class="input-field col s4">
				<input type="text" id="cnmtCode" name="cnmtCode" ng-model="cnmt.cnmtCode" ng-keyup="OpenCnmtCodeDB($event.keyCode)" required>
				<label>CNMT Code</label>
			</div>
			<div class="input-field col s4">
				<input class="validate" type ="text" name ="customerCode" ng-model="customerCode" ng-keyup="OpenCustomerCodeDB($event.keyCode)" required>
       		 	<input class="validate" type ="hidden" name ="custCode" ng-model="cnmt.custCode" >
       		 	<label>Cust. Name/FaCode</label>
			</div>
			<div class="input-field col s4">
				<input type="number" name="cnmtVOG" id="cnmtVOG" step="0.01"
					min="0.00" ng-model="cnmt.cnmtVOG" ng-minlength="1"
					ng-maxlength="9" ng-required="true"> <label>Value
					Of Goods</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s3">
				<input type="date" name="cnmtDt" id="cnmtDt" ng-model="cnmt.cnmtDt"
					ng-required="true" ng-blur="dateSelect(cnmt.cnmtDt)"> <label>Date</label>
			</div>
			<div class="input-field col s3">
			 	<input class="validate" type ="text" name ="fromStationName" ng-model="frmStationCode" ng-keyup="getStationFrom($event.keyCode)" ng-required="true" readonly>
       			 	<input class="validate" type ="hidden" name ="cnmtFromSt" ng-model="cnmt.cnmtFromSt" >
					<label>From Station</label>
			</div>
			<div class="input-field col s3">
				<input type="text" name="contractCode" id="contractCode1"
					ng-model="contractCodeTemp" disabled="disabled"
					ng-click="OpenContractCodeDB()" readonly ng-required="true"> <label>Contract Code</label>
			</div>
			<div class="input-field col s3">
				<input class="validate" type ="text" name ="toStationName" ng-model="toStationCode" ng-click="getStationTo()" ng-required="true" readonly>
       			 <input class="validate" type ="hidden" name ="cnmtToSt" ng-model="cnmt.cnmtToSt" >
				<label>To Station</label>
			</div>

		</div>
		<div class="row">
			<div class="input-field col s3">
				<input class="validate" type ="text" name ="consignorTmpCode" ng-model="consignorTmpCode" ng-keyup="getConsignorList($event.keyCode)" required>
	        	<input class="validate" type ="hidden" name ="cnmtConsignor" ng-model="cnmt.cnmtConsignor" >
				<label>Consignor</label>
			</div>
			<div class="input-field col s3">
				<input class="validate" type ="text" name ="consigneeTmpCode" ng-model="consigneeTmpCode" ng-keyup="getConsigneeList($event.keyCode)" required>
	        	<input class="validate" type ="hidden" name ="cnmtConsignee" ng-model="cnmt.cnmtConsignee" >
				<!-- <input type="text" name="cnmtConsignee" id="cnmtConsignee"
					ng-model="cnmt.cnmtConsignee" ng-click="OpenCnmtConsigneeDB()"
					required readonly> --> <label>Consignee</label>
			</div>
			<div class="input-field col s3">
				<input class="validate" type ="text" name ="vehicleName" ng-model="vehicleName" readonly ng-click="OpenVehicleTypeDB()">
       			 <input class="validate" type ="hidden" name ="cnmtVehicleType" ng-model="cnmt.cnmtVehicleType" >
				<!-- <input type="text" name="cnmtVehicleType" id="cnmtVehicleType"
					ng-model="cnmt.cnmtVehicleType" ng-click="OpenVehicleTypeDB()"
					readonly>  --><label>Vehicle Type</label>
			</div>
			<div class="input-field col s3">
				<input type="text" name="cnmtProductType" id="cnmtProductType"
					ng-model="cnmt.cnmtProductType" ng-click="OpencnmtProductTypeDB()"
					 readonly> <label>Product Type</label>
			</div>
		</div>

		<div class="row">
			<div class="input-field col s3">
				<input type="number" name="cnmtActualWt" id="cnmtActualWt"
					ng-model="cnmt.cnmtActualWt" step="0.00001" min="0.00000" required>
				<label>Actual Weight</label>
			</div>
			<div class="input-field col s3">
				<label>Unit</label> <select name="cnmtActualWt1" id="cnmtActualWt1"
					ng-model="cnmtActualWtPer" ng-init="cnmtActualWtPer = 'Ton'" disabled="disabled"
					required>
					<option value="Ton">Ton</option>
					<option value="Kg">Kg</option>
				</select>
			</div>
			<div class="input-field col s3">
				<input type="number" name="cnmtGuaranteeWt" id="cnmtGuaranteeWt"  
					ng-model="cnmt.cnmtGuaranteeWt" step="0.000000001" min="0.00000000" required>
				<!-- <input type ="number" name ="cnmtGuaranteeWt" id="cnmtGuaranteeWt" ng-model="cnmtGuaranteeWt" ng-init="cnmtActualWtPer = 'Ton'" required> -->
				<label>Guarantee Weight</label>
			</div>
			<div class="input-field col s3">
				<label>Unit</label> <select name="cnmtGuaranteeWt1"
					id="cnmtGuaranteeWt1" ng-model="cnmtGuaranteeWtPer"
					ng-init="cnmtGuaranteeWtPer = 'Ton'"  disabled="disabled" required>
					<option value="Ton">Ton</option>
					<option value="Kg">Kg</option>
				</select>
			</div>
		</div>

		<div class="row">
			<div class="input-field col s3">
				<input type="number" name="cnmtKm" id="cnmtKmId"
					ng-model="cnmt.cnmtKm" ng-minlength="1" ng-maxlength="7"
					ng-blur="changeKm()" disabled="disabled"> <label>Kilometer</label>
			</div>
			<div class="input-field col s3">
				<input type="text" name="cnmtState" id="cnmtStateId"
					ng-model="cnmt.cnmtState" ng-click="getStateList()"
					disabled="disabled" readonly> <label>State</label>
			</div>
			<div class="input-field col s3">
				<input type="text" name="cnmtDDL" ng-model="cnmtDDL" readonly>
				<label>Door-to-Door Delivery</label>
			</div>
			<div class="input-field col s3">
				<input class="validate" type ="text" id="cnmtPayAt" name ="cnmtPayAt" ng-model="payOnCode" readonly ng-click="getBranchData()" >
        		<input class="validate" type ="hidden" id="cnmtPayAt" name ="cnmtPayAt" ng-model="cnmt.cnmtPayAt" >
				<label>Pay	On</label>
			</div>
		</div>

		<div class="row">
			<!-- <div class="input-field col s3">
			<input class="validate" type ="text" id="cnmtBillAt" name ="cnmtBillAt" ng-model="billOnCode" readonly ng-click="OpenCnmtBillAtDB()" >
        	<input class="validate" type ="hidden" id="cnmtBillAt" name ="cnmtBillAt" ng-model="cnmt.cnmtBillAt" ng-required="true">
				 <label>Bill On</label>
			</div> -->
			<!-- <div class="input-field col s3">
				<input type="number" name="cnmtRate" id="cnmtRateId"
					ng-model="cnmt.cnmtRate" step="0.000000001" min="0.00000000" readonly ng-required="true"> <label>Rate</label>
			</div> -->
			
			<!-- <div class="input-field col s3">
				<select name="chlnLryRate1" id="chlnLryRate1"
					ng-model="chlnLryRatePer" ng-init="chlnLryRatePer='Ton'"
					ng-change="chngRate(chlnLryRatePer)">
					<option value="Ton">Ton</option>
					<option value="Kg">Kg</option>
				</select>
			</div> -->
			<div class="input-field col s3">
				<input type="number" name="cnmtNoOfPkg" id="cnmtNoOfPkg"
					ng-model="cnmt.cnmtNoOfPkg" ng-minlength="1" ng-maxlength="7"
					ng-required="true" ng-keyup="calculateFreight() || calculateTOT() "> <label>No. Of Packages</label>
			</div>
			<!-- <div class="input-field col s3">
				<input type="number" name="cnmtFreight" id="cnmtFreight" ng-click="calculateFreight() || calculateTOT() "
					ng-model="cnmt.cnmtFreight"  readonly ng-required="true"> <label>Freight</label>
			</div> -->
		</div>

		<div class="row">
			<!-- <div class="input-field col s3">
				<input type="number" name="cnmtTOT" id="cnmtTOT"
					ng-model="cnmt.cnmtTOT" readonly> <label>Cnmt Total</label>
			</div> -->
			
<!-- 			<div class="input-field col s3"> -->
<!-- 				<input type="text" name="cnmtDC" id="cnmtDC" ng-model="cnmtDC"> -->
<!-- 				<label>D C</label> -->
<!-- 			</div> -->
			
			
			<div class="input-field col s3">
	     				
	       			<select name="cnmtDCName" ng-model="cnmt.cnmtDC" required>
						<option value="0 bill">FOC</option>
						<option value="1 bill">One bill</option>
<!-- 						<option value="2 bill">Two bill</option> -->
<!-- 						<option value="Direct Payment through CNMT">Direct Payment through CNMT</option> -->
<!-- 						<option value="Twice Payment">Twice Payment</option> -->
<!-- 						<option value="Partially through bill Partially through CNMT">Partially through bill Partially through CNMT</option> -->
					</select>
					<label>DC</label>
	   			</div>
			

			<div class="input-field col s3">
				<input type="text" name="cnmtCostGrade" ng-model="cnmtCostGrade">
				<label>Cost Grade</label>
			</div>

			<div class="input-field col s3">
				<input type="date" name="cnmtGtOutDt"  ng-model="cnmt.cnmtGtOutDt">
				<label>Gate Out Date</label>
			</div>
			
		</div>

		<div class="row">
			<div class="input-field col s3">
				<input class="validate" type ="text" name ="empTempCode" ng-model="empTempCode" readonly ng-click="getEmployeeList()" required>
	        	<input class="validate" type ="hidden" name ="cnmtEmpCode" ng-model="cnmt.cnmtEmpCode" >
				<label>Employee Code</label>
			</div>
			
			<div class="input-field col s3">
				<input type="number" name="cnmtExtraExp" id="cnmtExtraExp"
					ng-model="cnmt.cnmtExtraExp" step="0.01" min="0.00"
					ng-minlength="1" ng-maxlength="7"> <label>Extra
					Expenses</label>
			</div>
			
			<div class="input-field col s3">
				<input type="date" name="cnmtDtOfDly" ng-blur="cnmtDlyDtInFY()" ng-model="cnmt.cnmtDtOfDly">
				<label>Date Of Delivery</label>
			</div>
			<div class="input-field col s3">
				<!-- <input type ="text" id="cnmtInvoiceNo" name ="cnmtInvoiceNo" ng-model="cnmt.cnmtInvoiceNo" ng-minlength="3" ng-maxlength="40" ng-required="true"> -->
				<input type="button" value="Add Invoice Number"
					ng-click="openInvoiceNo()"> <label>Invoice Number</label>
				<!-- <a style="width: 40px; position: absolute; right: -10px; padding: 0 1rem" class="btn waves-effect teal" id="openInvNo" ng-model="savInvoiceNo" ng-click="openInvoiceNo()"><i class="mdi-content-add white-text" style="font-size: 24px; margin-left: -6px;"></i></a> -->
			</div>
		</div>
		


		<div ng-show="cnmtInvList.length > 0" class="col s12 center">
			<table style="width: 20rem; margin: 0 auto;">
				<thead>
					<tr>
						<th>Cnmt Invoice Number</th>
						<th>Cnmt Invoice Date</th>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="cnmtInv in cnmtInvList">
						<td>{{cnmtInv.invoice}}</td>
						<td>{{cnmtInv.date | date : format : timezone}}</td>
						<td><input type="button" class="btn"
							ng-click="deleteCnmtInv(cnmtInv)" value="Remove"></td>
					</tr>
				</tbody>
			</table>
			<input type="button" ng-show="cnmtInvList.length > 1"
				ng-click="deleteAllInv()" value="Remove All Invoice Number">

		</div>

<!-- 		<div class="row">
			<div class="input-field col s8">
				<input class="teal btn white-text" type="file"
					file-model="cnmtImage" />
			</div>

			<div class="input-field col s4">
				<input class="col s12" type="button" ng-click="uploadCnmtImage()"
					value="upload ACK Image" />
			</div>

		</div>

		<div class="row">
			<div class="input-field col s8">
				<input class="teal btn white-text" type="file"
					file-model="cnmtConfirmImage" />
			</div>

			<div class="input-field col s4">
				<input class="col s12" type="button"
					ng-click="uploadConfCnmtImage()" value="upload Confirm ACK Image" />
			</div>

		</div> -->

		<div class="row">
			<div class="input-field col s12 center">
				<input type="submit" value="Submit">
			</div>

		</div>

	</form>



</div>



<div id="branchCodeDB" ng-hide="BranchCodeDBFlag">

	<input type="text" name="filterBrCode"
		ng-model="filterBrCode.branchCode" placeholder="Search by Code">

	<table>
		<tr>
			<th></th>

			<th>Branch Code</th>

			<th>Branch Name</th>

			<th>Branch Pin</th>
		</tr>
	</table>
	<table>

		<tr ng-repeat="branch in branchList | filter:filterBrCode">
			<td><input type="radio" name="branchCode" id="branchId"
				value="{{ branch.branchCode }}" ng-model="branchCode"
				ng-click="saveBranchCode(branch)"></td>
			<td>{{ branch.branchCode }}</td>
			<td>{{ branch.branchName }}</td>
			<td>{{ branch.branchPin }}</td>
		</tr>
	</table>

</div>

<div id="customerCodeDB" ng-hide="CustomerCodeDBFlag">

	<input type="text" name="filterCustomerCode"
		ng-model="filterCustomerCode" placeholder="Search">
<!-- 	<input type="text" name="filterCustomerName"
		ng-model="filterCustomerName.custName"
		placeholder="Search by Customer Name"> -->

	<table>
		<tr>
			<th></th>
			<th>Customer Code</th>
			<th>Branch Code</th>
			<th>Customer Name</th>
			<th>Daily Contract Allowed</th>
			<th>Customer FACode</th>
		</tr>

		<tr
			ng-repeat="customer in customerList | filter:filterCustomerCode">
			<td><input type="radio" name="custCode" id="custId"
				value="{{ customer }}" ng-click="saveCustomerCode(customer)"></td>
			<td>{{ customer.custCode }}</td>
			<td>{{ customer.branchCode }}</td>
			<td>{{ customer.custName }}</td>
			<td>{{ customer.custIsDailyContAllow }}</td>
			<td>{{ customer.custFaCode }}</td>
		</tr>
	</table>

</div>

<div id="CNMTConsignorDB" ng-hide="CNMTConsignorDBFlag">

	<input type="text" name="filterCNMTConsignor"
		ng-model="filterCNMTConsignor" placeholder="Search by Code">

	<table>
		<tr>
			<th></th>
			<th>Customer Code</th>
			<th>Branch Code</th>
			<th>Customer Name</th>
			<th>Daily Contract Allowed</th>
		</tr>

		<tr ng-repeat="consignor in customerList | filter:filterCNMTConsignor">
			<td><input type="radio" name="custCode" id="custId"
				value="{{ consignor.custCode }}" ng-model="consignorTmpCode"
				ng-click="saveConsignorCustomerCode(consignor)"></td>
			<td>{{ consignor.custCode }}</td>
			<td>{{ consignor.branchCode }}</td>
			<td>{{ consignor.custName }}</td>
			<td>{{ consignor.custIsDailyContAllow }}</td>
		</tr>
	</table>

</div>


<!-- rel wherehouse show -->
<div id="warehouseId" ng-show="whShowFlag">

	<input type="text" name="filterwarehouseId"
		ng-model="filterWhareHouse" placeholder="Search ">

	<table>
		<tr>
			<th></th>
			<th>ID</th>
			<th>SR No</th>
			<th>Count</th>
			<th>State</th>
			<th>Location</th>
			<th>Address1</th>
			<th>Address2</th>
		</tr>

		<tr ng-repeat="wh in warehouseList | filter:filterWhareHouse">
			<td><input type="radio" name="whCode" id="whId"
				value="{{ wh.whName }}" ng-model="whName"
				ng-click="saveWarehouseCode(wh)"></td>
			<td>{{ wh[0] }}</td>
			<td>{{ wh[1] }}</td>
			<td>{{ wh[2] }}</td>
			<td>{{ wh[3]}}</td>
			<td>{{ wh[4] }}</td>
			<td>{{ wh[5] }}</td>
			<td>{{ wh[6] }}</td>
		</tr>
	</table>

</div>





<div id="CNMTConsigneeDB" ng-hide="CNMTConsigneeDBFlag">

	<input type="text" name="filterCNMTConsignee"
		ng-model="filterCNMTConsignee" placeholder="Search by Code">

	<table>
		<tr>
			<th></th>
			<th>Customer Code</th>
			<th>Branch Code</th>
			<th>Customer Name</th>
			<th>Daily Contract Allowed</th>
		</tr>
		<tr ng-repeat="consignee in consigneeList | filter:filterCNMTConsignee">
			<td><input type="radio" name="custCode" id="custId"
				value="{{ consignee.custCode }}" ng-model="consigneeTmpCode"
				ng-click="saveConsigneeCustomerCode(consignee)"></td>
			<td>{{ consignee.custCode }}</td>
			<td>{{ consignee.branchCode }}</td>
			<td>{{ consignee.custName }}</td>
			<td>{{ consignee.custIsDailyContAllow }}</td>
		</tr>
	</table>
</div>

<div id="contractCodeDB" ng-hide="ContractCodeDBFlag">

	<input type="text" name="filterContractCode"
		ng-model="filterContractCode.contCode" placeholder="Search by Code">

	<table class="table" style="border-bottom: 1px solid white;">
		<tr>
			<th style="border-right: 1px solid grey"><br />
				<div class="hori-td">&nbsp;</div> <br />
				<div class="hori-td">Customer Name</div> <br />
				<!-- <div class="hori-td">Contract Code</div> <br /> -->
				<div class="hori-td">Contract FACode</div> <br />
				<div class="hori-td">From Date</div> <br />
				<div class="hori-td">To Date</div> <br />
				<div class="hori-td">From Station</div> <br />
				<div class="hori-td">To Station</div> <br />
				<!-- <div class="hori-td">Daily Contract</div> <br /> -->
				<div class="hori-td">Door To Door Delivery</div> <br />
				<div class="hori-td">Cost Grade</div> <br />
				<div class="hori-td">Contract Type</div> <br />
				<div class="hori-td">Proportionate</div> <br /></th>


			<td ng-repeat="contract in contractList | filter:filterContractCode">
				<br />
				<div class="hori-td">
					<input type="radio" name="contCode" id="contId"
						value="{{ contract.contCode }}" ng-model="contCode"
						ng-click="saveContractCode(contract)">
				</div> <br />
				<div class="hori-td">{{ contract.custName }}</div> <br />
				<!-- <div class="hori-td">{{ contract.contCode }}</div> <br /> -->
				<div class="hori-td">{{ contract.FACode }}</div> <br />
				<div class="hori-td">{{ contract.fromDate }}</div> <br />
				<div class="hori-td">{{ contract.toDate }}</div> <br />
				<div class="hori-td">{{ contract.fromStnName }}</div> <br />
				<div class="hori-td">{{ contract.toStation }}</div> <br />
				<!-- <div class="hori-td">{{ contract.cnmtDC }}</div> <br /> -->
				<div class="hori-td">{{ contract.cnmtDDL }}</div> <br />
				<div class="hori-td">{{ contract.cnmtCostGrade }}</div> <br />
				<div class="hori-td">{{ contract.contType }}</div> <br />
				<div class="hori-td">{{ contract.proportionate }}</div>
			</td>
		</tr>
	</table>

</div>

<div id="CNMTPayAtDB" ng-hide="CNMTPayAtDBFlag">

	<input type="text" name="filterCNMTPay"
		ng-model="filterCNMTPay.branchCode" placeholder="Search by Code">

	<table>
		<tr>
			<th></th>

			<th>Branch Code</th>

			<th>Branch Name</th>

			<th>Branch Pin</th>
		</tr>
	</table>
	<table>

		<tr ng-repeat="payat in branchList | filter:filterCNMTPay">
			<td><input type="radio" name="branchCode" id="branchId"
				value="{{ payat.branchCode }}" ng-model="payOnCode"
				ng-click="savePayAtBranchCode(payat)"></td>
			<td>{{ payat.branchCode }}</td>
			<td>{{ payat.branchName }}</td>
			<td>{{ payat.branchPin }}</td>
		</tr>
	</table>

</div>

<div id="CNMTBillAtDB" ng-hide="CNMTBillAtDBFlag">

	<input type="text" name="filterCNMTBill"
		ng-model="filterCNMTBill" placeholder="Search by Code">

	<table>
		<tr>
			<th></th>

			<!-- <th>Branch Code</th> -->

			<th>Branch Name</th>

			<th>Branch Pin</th>
			
			<th>Branch FaCode</th>
		</tr>
	</table>
	<table>

		<tr ng-repeat="billat in branchList | filter:filterCNMTBill">
			<td><input type="radio" name="branchCode" id="branchId"
				value="{{ billat.branchCode }}" ng-model="billOnCode"
				ng-click="saveBillAtBranchCode(billat)"></td>
			<!-- <td>{{ billat.branchCode }}</td> -->
			<td>{{ billat.branchName }}</td>
			<td>{{ billat.branchPin }}</td>
			<td>{{ billat.branchFaCode }}</td>
		</tr>
	</table>

</div>

<div id="employeeCodeDB" ng-hide="EmployeeCodeDBFlag">

	<input type="text" name="filterEmployeeCode"
		ng-model="filterEmployeeCode" placeholder="Search by Code">

	<table>
		<tr>
			<th></th>

			<!-- <th>Employee Code</th> -->

			<th>Employee Name</th>
			
			<th>Employee FaCode</th>
			
			<th>Employee Code</th>
			
		</tr>

		<tr ng-repeat="employee in employeeList | filter:filterEmployeeCode">
			<td><input type="radio" name="empCode" id="empId"
				value="{{ employee.empCode }}" ng-model="empTempCode"
				ng-click="saveEmployeeCode(employee)"></td>
			<!-- <td>{{ employee.empCode }}</td> -->
			<td>{{ employee.empName }}</td>
			<td>{{ employee.empFaCode }}</td>
			<td>{{ employee.empCodeTemp }}</td>
		</tr>
	</table>

</div>

<div id="stateCodeDB" ng-hide="StateCodeDBFlag">

	<input type="text" name="filterStateCode"
		ng-model="filterStateCode.stateCode" placeholder="Search by Code">
	<table>
		<tr>
			<th></th>
			<th>State Code</th>

			<th>State Name</th>

			<th>State Lorry Prefix</th>
		</tr>
	</table>
	<table>
		<tr ng-repeat="state in stateList | filter:filterStateCode">
			<td><input type="radio" name="stateCode" id="stateId"
				value="{{ state.stateCode }}" ng-model="stateCode"
				ng-click="saveStateCode(state)"></td>
			<td>{{ state.stateCode }}</td>
			<td>{{ state.stateName }}</td>
			<td>{{ state.stateLryPrefix }}</td>
		</tr>
	</table>
</div>



<div id="cnmtToStationDB" ng-hide="CnmtToStationDBFlag">
	<input type="text" name="filterCnmtToStation"
		ng-model="filterCnmtToStation" placeholder="Search by Code">
	<table>
		<tr>
			<th></th>

			<!-- <th>Station Code</th> -->

			<th>Station Name</th>

			<th>Station District</th>
		</tr>
		<tr ng-repeat="station in stationList | filter:filterCnmtToStation">
			<td><input type="radio" name="stnName" id="stnName"
				class="stnName" value="{{ station}}" ng-model="toStationCode"
				ng-click="saveToStnCode(station)"></td>
			<!-- <td>{{ station.stnCode }}</td> -->
			<td>{{ station.stnName }}</td>
			<td>{{ station.stnDistrict }}</td>
		</tr>
	</table>
</div>


<div id="cnmtCodeDB" ng-hide="CnmtCodeDBFlag">

	<input type="text" name="filterCnmtCode"
		ng-model="filterCnmtCode.brsLeafDetSNo" placeholder="Search by Code">
	<table>
		<tr>
			<th></th>
			<th>Cnmt Code</th>
		</tr>
		<tr ng-repeat="cnmt in cnmtCodeList | filter:filterCnmtCode">
			<td><input type="radio" name="cnmtCode" id="cnmtId"
				value="{{ cnmt.brsLeafDetSNo }}" ng-model="cnmtCode"
				ng-click="saveCnmtCode(cnmt)"></td>
			<td>{{ cnmt.brsLeafDetSNo }}</td>
		</tr>
	</table>
</div>


<div id="saveInvoiceNo" ng-hide="saveInvoiceNoFlag">
	<form name="saveInvoiceNoForm"
		ng-submit="saveInvoiceNo(saveInvoiceNoForm,invoiceNo,invoiceDt)">
		<table>
			<tr>
				<td>Invoice No:</td>
				<td><input type="text" id="invoiceNo" name="invoiceNo"
					ng-model="invoiceNo" ng-minlength="3" ng-maxlength="25" required></td>
				<td><input type="date" id="invoiceDt" name="invoiceDt"
					ng-model="invoiceDt" ng-blur="invcDtInFy()" required></td>
			</tr>
			<tr>
				<td><input type="submit" value="Submit"></td>
			</tr>

		</table>
	</form>
</div>

<!--  reliance challan for o&m -->
<div id="saveRelChlnNoId" ng-show="relChlnShowFlag">
	<form name="saveRelChlnNoForm"
		ng-submit="saveRelChlnNo(relChln)">
		<table>
			<tr>
				<td>Rel Challan No:</td>
				<td><input type="text" id="relChlnNoId" name="relChln"
					ng-model="relChln" ng-minlength="3" ng-maxlength="25" required></td>
			</tr>
			<tr>
				<td><input type="submit" value="Submit"></td>
			</tr>

		</table>
	</form>
</div>


<div id="cnmtFromStationDB" ng-hide="CnmtFromStationDBFlag">
	<input type="text" name="filterCnmtFromStation"
		ng-model="filterCnmtFromStation" placeholder="Search by Code">
	<table>
		<tr>
			<th></th>

			<!-- <th>Station Code</th> -->

			<th>Station Name</th>

			<th>Station District</th>
		</tr>


		<tr ng-repeat="station in stationList | filter:filterCnmtFromStation">
			<td><input type="radio" name="stnName" id="stnName"
				class="stnName" value="{{ station }}" ng-model="frmStationCode"
				ng-click="saveFrmStnCode(station)"></td>
			<!-- <td>{{ station.stnCode }}</td> -->
			<td>{{ station.stnName }}</td>
			<td>{{ station.stnDistrict }}</td>
		</tr>
	</table>
</div>


<div id="cnmtProductTypeDB" ng-hide="ProductTypeFlag">
	<input type="text" name="filterProductType"
		ng-model="filterProductType" placeholder="Search by Product Name">
	<table class="noborder">
		<tr>
			<th></th>
			<th>Product Name</th>
		</tr>


		<tr ng-repeat="pt in ptList | filter:filterProductType">
			<td><input type="radio" name="ptName" class="ptName"
				value="{{ pt }}" ng-model="ptName" ng-click="savProductName(pt)"></td>
			<td>{{ pt }}</td>
		</tr>
	</table>
</div>



<div id="vehicleTypeDB" ng-hide="VehicleTypeDBFlag">
	<input type="text" name="filterVehicleType"
		ng-model="filterVehicleType.vtCode" placeholder="Search by Code">

	<table>
		<tr>
			<th></th>
			<th>Vehicle Type</th>
			<th>Vehicle Type Code</th>
		</tr>
		<tr ng-repeat="vt in vtList | filter:filterVehicleType">
			<td><input type="radio" name="vtCode" id="vtId" value="{{ vt }}"
				ng-model="vehicleName" ng-click="saveVehicleType(vt)"></td>
			<td>{{ vt.vtVehicleType }}</td>
			<td>{{ vt.vtCode }}</td>
		</tr>
	</table>
</div>


<div id="viewCnmtDetailsDB" ng-hide="viewCnmtDetailsFlag">
	<div class="row">
		<div class="col s12 card"
			style="align: center; margin-top: 0px; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
			<h4>
				Here's the review of <span class="teal-text text-lighten-2">{{cnmtCode}}</span>:
			</h4>
			<table class="table-bordered table-hover table-condensed">

				<!--  <tr>
	                <td>Branch Code:</td>
	                <td>{{cnmt.branchCode}}</td>
	            </tr> -->
				<tr>
					<td>CNMT Date:</td>
					<td>{{cnmt.cnmtDt}}</td>
				</tr>
				<tr>
					<td>Customer Code:</td>
					<td>{{cnmt.custCode}}</td>
				</tr>
				<tr>
					<td>CNMT Consignor:</td>
					<td>{{cnmt.cnmtConsignor}}</td>
				</tr>
				<tr>
					<td>CNMT Consignee:</td>
					<td>{{cnmt.cnmtConsignee}}</td>
				</tr>
				<tr>
					<td>Contract Code:</td>
					<td>{{cnmt.contractCode}}</td>
				</tr>
				<tr>
					<td>CNMT DC:</td>
					<td>{{cnmtDC}}</td>
				</tr>
				<tr>
					<td>NO. Of Package:</td>
					<td>{{cnmt.cnmtNoOfPkg}}</td>
				</tr>
				<tr>
					<td>CNMT Actual Weight:</td>
					<td>{{cnmt.cnmtActualWt}}</td>
				</tr>
				<tr>
					<td>CNMT Guarantee Weight:</td>
					<td>{{cnmt.cnmtGuaranteeWt}}</td>
				</tr>
				<tr>
					<td>Door to Door Delivery:</td>
					<td>{{cnmtDDL}}</td>
				</tr>
				<tr>
					<td>CNMT Pay AT:</td>
					<td>{{cnmt.cnmtPayAt}}</td>
				</tr>
				<tr>
					<td>CNMT Bill AT:</td>
					<td>{{cnmt.cnmtBillAt}}</td>
				</tr>
				<!-- <tr>
					<td>CNMT Rate:</td>
					<td>{{cnmt.cnmtRate}}</td>
				</tr> -->
				<!-- <tr>
					<td>CNMT Freight:</td>
					<td>{{cnmt.cnmtFreight}}</td>
				</tr> -->
				<tr>
					<td>CNMT VOG:</td>
					<td>{{cnmt.cnmtVOG}}</td>
				</tr>
				<tr>
					<td>CNMT Extra Exp.:</td>
					<td>{{cnmt.cnmtExtraExp}}</td>
				</tr>
				<tr>
					<td>CNMT Cost Grade:</td>
					<td>{{cnmtCostGrade}}</td>
				</tr>
				<tr>
					<td>Date of Dly:</td>
					<td>{{cnmt.cnmtDtOfDly}}</td>
				</tr>
				<tr>
					<td>Contract Allowed:</td>
					<td>{{cnmt.cnmtEmpCode}}</td>
				</tr>
				<tr>
					<td>CNMT KM:</td>
					<td>{{cnmt.cnmtKm}}</td>
				</tr>
				<tr>
					<td>Vehicle Type:</td>
					<td>{{cnmt.cnmtVehicleType}}</td>
				</tr>
				<tr>
					<td>CNMT State:</td>
					<td>{{cnmt.cnmtState}}</td>
				</tr>
				<tr>
					<td>CNMT Invoice No:</td>
					<td ng-repeat="cnmtInv in cnmtInvList">
						<table>
							<tr>
								<td>{{cnmtInv.invoice}} {{cnmtInv.date | date : format :
									timezone}}</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>

			<input type="button" value="Save" id="saveBtnId"
				ng-click="saveCnmt(cnmt,cnmtActualWtPer,cnmtGuaranteeWtPer,chlnLryRatePer)">
			<input type="button" value="Cancel"
				ng-click="closeViewCnmtDetailsDB()">


		</div>
	</div>
</div>
</div>