<div ng-show="superAdminLogin">
	<div class="row">
		<div class="col s3 hide-on-med-and-down"> &nbsp; </div>
			<form name="viewMunsForm" class="col s6 card" style="align: center; margin-top: 40px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" ng-submit="submit(viewMunsForm)">
				<div class="input-field col s6">
					<input type="text" name="brhName" id="brhId" ng-model="branchName" required readonly ng-click="openBranchDB()" />
	   				<label>Select Branch</label>
	 			</div>
	      		<div class="input-field col s6 center">
	      			<input class="btn waves-effect waves-light" type="submit" value="Submit">
	      		</div>
			</form>
			<div class="col s3"> &nbsp; </div>
	</div>
	
	
	<div id="displayId" ng-hide="displayDB"> 
		<div class="row">
		<div class="col s3 hide-on-med-and-down"> &nbsp; </div>
		<form class="col s6 card" style="align: center; margin-top: 40px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
			<h4>
				Details of <span class="teal-text text-lighten-2">{{branchName}}</span> Munsiana/TDS
			</h4>
			<table class="table-bordered table-hover table-condensed">
				<tr>
					<td>Branch</td>
					<td>{{branchName}}</td>
				</tr>
				
				<tr>
					<td>Munsiana</td>
					<td>{{brhWiseMuns.bwmMuns}}</td>
				</tr>
				
				<tr>
					<td>Cash Discount%</td>
					<td>{{brhWiseMuns.bwmCsDis}}</td>
				</tr>
				
				<tr>
					<td>Active</td>
					<td>{{brhWiseMuns.bwmActive}}</td>
				</tr>
				
			</table>
			</form>
		</div>
	</div>	
	
	
	<div id ="branchCodeDB" ng-hide="branchCodeDBFlag">
		 <input type="text" name="filterBrh" ng-model="filterBrh" placeholder="Search....">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	 
 	  	  	  <th> Branch Name </th>
 	  	  	  
 	  	  	  <th> Branch FaCode</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="branch in brhList | filter:filterBrh">
		 	  <td><input type="radio"  name="branchName"  class="branchCls"  value="{{ branch }}" ng-model="brCode" ng-click="saveBranch(branch)"></td>
              <td>{{branch.branchName}}</td>
              <td>{{branch.branchFaCode}}</td>
          </tr>
      </table>
	</div>
	
	
</div>