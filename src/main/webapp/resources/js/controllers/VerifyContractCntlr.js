'use strict';

var app = angular.module('application');

app.controller('VerifyContractCntlr',['$scope','$location','$http','$window',
                                 function($scope,$location,$http,$window){
	$scope.selection=[];
	
	  $scope.getDlyContIsVerify = function(){
		   console.log("getDlyContIsVerify------>");
		   var response = $http.post($scope.projectName+'/verifyDailyContract');
		   response.success(function(data, status, headers, config){
			   console.log("---------->list==>"+data.result);
			   $scope.successToast(data.result);
			   $scope.ContractList = data.list;
	    });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	 }
	
	  $scope.toggleSelection = function toggleSelection(dlyContCode) {
		   var idx = $scope.selection.indexOf(dlyContCode);
		   
		   // is currently selected
		   if (idx > -1) {
			   $scope.selection.splice(idx, 1);
			   }
		   // is newly selected
		   else {
			   $scope.selection.push(dlyContCode);
		   }
	   }	  
	   

	 $scope.verifyContracts = function(){
		 console.log("enter into update with $scope.selection = "+$scope.selection);
		 
		 var response = $http.post($scope.projectName+'/updateIsVerifyContract',$scope.selection.toString());
		 response.success(function(data, status, headers, config){
			   console.log("---------->message==>"+data.result);
				$scope.successToast(data.result);
	    });	 
		 response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
			});
	 }
	
	$scope.getDlyContIsVerify();
}]);