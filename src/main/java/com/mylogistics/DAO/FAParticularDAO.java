package com.mylogistics.DAO;

import java.util.List;

import org.hibernate.Session;

import com.mylogistics.model.FAParticular;

public interface FAParticularDAO {

	public List<FAParticular> getFAParticular(String type);
	
	public List<FAParticular> getFAParticular(String typeSession,Session session);
	
}
