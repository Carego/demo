package com.mylogistics.controller;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.mylogistics.DAO.CnmtDAO;
import com.mylogistics.DAO.CustomerRepresentativeDAO;
import com.mylogistics.model.Cnmt;
import com.mylogistics.model.CustomerRepresentative;
import com.itextpdf.text.FontFactory;
import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.CampaignResultDAO;
import com.mylogistics.DAO.ContToStnDAO;
import com.mylogistics.DAO.CustomerDAO;
import com.mylogistics.DAO.DailyContractDAO;
import com.mylogistics.DAO.EmployeeDAO;
import com.mylogistics.DAO.IndentDAO;
import com.mylogistics.DAO.PenaltyBonusDetentionDAO;
import com.mylogistics.DAO.RegularContractDAO;
import com.mylogistics.DAO.StationDAO;
import com.mylogistics.DAO.VehicleEngagementDAO;
import com.mylogistics.DAO.VehicleTypeDAO;
import com.mylogistics.DAO.VehicleVendorDAO;
import com.mylogistics.model.Branch;
import com.mylogistics.model.CampaignResults;
import com.mylogistics.model.ContToStn;
import com.mylogistics.model.Customer;
import com.mylogistics.model.DailyContract;
import com.mylogistics.model.Employee;
import com.mylogistics.model.FundAllocation;
import com.mylogistics.model.Indent;
import com.mylogistics.model.OwnerMaster;
import com.mylogistics.model.PenaltyBonusDetention;
import com.mylogistics.model.RegularContract;
import com.mylogistics.model.Station;
import com.mylogistics.model.VehicleEngagement;
import com.mylogistics.model.VehicleType;
import com.mylogistics.model.VehicleVendorMstr;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.OwnerMasterService;

@Controller
public class IndentCntlr {
	
	@Autowired
	private CustomerDAO customerDAO;
	
	@Autowired
	private BranchDAO branchDAO;
	
	@Autowired
	private RegularContractDAO regularContractDAO;
	
	@Autowired
	private DailyContractDAO dailyContractDAO;
	
	@Autowired
	private StationDAO stationDAO;
	
	@Autowired
	private EmployeeDAO employeeDAO;
	
	@Autowired
	private VehicleTypeDAO vehicleTypeDAO;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private ContToStnDAO contToStnDAO; 
	
	@Autowired
	private IndentDAO indentDAO;
	
	@Autowired
	private CampaignResultDAO campaignResultDAO;
	
	@Autowired
	private PenaltyBonusDetentionDAO penaltyBonusDetentionDAO;
	
	@Autowired
	private VehicleEngagementDAO vehicleEngagementDAO;
	
	@Autowired
	private CnmtDAO cnmtDAO;
	
	@Autowired
	private VehicleVendorDAO vehicleVendorDAO;
	
	@Autowired
	private CustomerRepresentativeDAO customerRepresentativeDAO;
	
	
	private Map<String, Object> excelMap=new HashMap<>();
	
	
	@RequestMapping(value = "/getCustFrIndent", method = RequestMethod.POST)
	public @ResponseBody Object getCustFrIndent(@RequestBody Map<String, Object>clientMap) {
		System.out.println("enter into getCustFrBillF function");
		Map<String,Object> map = new HashMap<>();
		List<Customer> custList = new ArrayList<>();
		String dt= (String) clientMap.get("indentPlacementDtTime");
		String[] sp=dt.split("T");
		Date date=Date.valueOf(sp[0]);
		System.out.println("indentPlacementDtTime= "+date);
		
		List<String>dlyList=dailyContractDAO.getContractByDate(date);
		List<String>regList=regularContractDAO.getContractByDate(date);
		List<String> custCodeList=new ArrayList<>();
		if(!dlyList.isEmpty())
			custCodeList.addAll(dlyList);
		
		System.out.println("custCodeList size="+custCodeList);
		
		if(!regList.isEmpty())
			custCodeList.addAll(regList);
		
		System.out.println("custCodeList size="+custCodeList);
		
		custList = customerDAO.getCutomerByCustCodeList(custCodeList);
		if(!custList.isEmpty()){
			map.put("list",custList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/getBranchFrIndent", method = RequestMethod.POST)
	public @ResponseBody Object getBranchFrIndent() {
		System.out.println("enter into getCustFrBillF function");
		Map<String,Object> map = new HashMap<>();
		List<Branch> brhList = new ArrayList<>();
		brhList = branchDAO.getAllActiveBranches();
		if(!brhList.isEmpty()){
			map.put("list",brhList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/getContractFrIndent", method = RequestMethod.POST)
	public @ResponseBody Object getContractFrIndent(@RequestBody Map<String, Object> clientMap) {
		System.out.println("enter into getCustFrBillF function");
		Map<String,Object> map = new HashMap<>();
		List<Map<String, String>> stnCtrctList=new ArrayList<>();
		try {
			String dt= (String) clientMap.get("indentPlacementDtTime");
			String[] sp=dt.split("T");
			Date date=Date.valueOf(sp[0]);
			System.out.println("indentPlacementDtTime= "+date);
			
			List<RegularContract> regList=regularContractDAO.getContractByCustCodeAndDate(clientMap.get("custCode").toString(), date);
			
			
			
			if(regList.isEmpty()) {
				List<DailyContract> dlyList=dailyContractDAO.getContractByCustCodeAndDate(clientMap.get("custCode").toString(), date);
				
				if(dlyList.isEmpty()) {
					map.put("msg", "Contract not found");
					map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
				}else {
					for(int i=0;i<dlyList.size();i++) {
						Map<String, String> localMap=new HashMap<>();
						localMap.put("contCode",dlyList.get(i).getDlyContCode());
						localMap.put("stnCode",dlyList.get(i).getDlyContFromStation());
						stnCtrctList.add(localMap);
					}
					map.put("contractList", stnCtrctList);
					map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
					
				}
				
			}else {
				
				for(int i=0;i<regList.size();i++) {
					Map<String, String> localMap=new HashMap<>();
					localMap.put("contCode",regList.get(i).getRegContCode());
					localMap.put("stnCode",regList.get(i).getRegContFromStation());
					stnCtrctList.add(localMap);
				}
				
				map.put("contractList", stnCtrctList);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}
			
			
		}catch (Exception e) {
			e.printStackTrace();
			map.put("msg", "There is some problem to get contract");
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/getFrmStnFrIndent", method = RequestMethod.POST)
	public @ResponseBody Object getFrmStnFrIndent(@RequestBody Map<String, Object> clientMap) {
		System.out.println("enter into getCustFrBillF function");
		Map<String,Object> map = new HashMap<>();
		try {
			String dt= (String) clientMap.get("indentPlacementDtTime");
			String[] sp=dt.split("T");
			Date date=Date.valueOf(sp[0]);
			System.out.println("indentPlacementDtTime= "+date);
			
			List<Station> frmStnList= stationDAO.getFrmStnFrIndnt(clientMap.get("custCode").toString(), date);
			if(frmStnList.isEmpty()) {
				
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
				
			}else {
				
				map.put("frmStnList", frmStnList);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}
			
			
		}catch (Exception e) {
			e.printStackTrace();
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/getToStnFrIndent", method = RequestMethod.POST)
	public @ResponseBody Object getToStnFrIndent(@RequestBody Map<String, Object> clientMap) {
		System.out.println("enter into getToStnFrIndent function");
		Map<String,Object> map = new HashMap<>();
		try {
			String dt= (String) clientMap.get("indentPlacementDtTime");
			String[] sp=dt.split("T");
			Date date=Date.valueOf(sp[0]);
			System.out.println("indentPlacementDtTime= "+date+"clientMap.get(\"contCode\").toString()="+clientMap.get("contCode").toString());
			
			List<Station> toStnList= stationDAO.getToStnFrIndnt(clientMap.get("custCode").toString(), date,clientMap.get("frmStnCode").toString());
			
			if(toStnList.isEmpty()) {
				
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
				
			}else {
				
				
				map.put("toStnList", toStnList);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}
			
			
		}catch (Exception e) {
			e.printStackTrace();
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/getVehTypeFrIndent", method = RequestMethod.POST)
	public @ResponseBody Object getVehTypeFrIndent(@RequestBody Map<String, Object> clientMap) {
		System.out.println("enter into getVehPrdctTypeFrIndent function");
		Map<String,Object> map = new HashMap<>();
		try {
			String dt= (String) clientMap.get("indentPlacementDtTime");
			String[] sp=dt.split("T");
			Date date=Date.valueOf(sp[0]);
			System.out.println("indentPlacementDtTime= "+date+"clientMap.get(\"contCode\").toString()="+clientMap.get("contCode").toString());
			String contCode= (String) clientMap.get("contCode");
			String toStnCode= (String) clientMap.get("toStnCode");
			
			List<VehicleType> vtList= vehicleTypeDAO.getVehTypeFrIndent(contCode,toStnCode,date);
			
			if(vtList.isEmpty()) {
				
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
				
			}else {
				map.put("vtList", vtList);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}
			
			
		}catch (Exception e) {
			e.printStackTrace();
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/getPrdctTypeFrIndent", method = RequestMethod.POST)
	public @ResponseBody Object getPrdctTypeFrIndent(@RequestBody Map<String, Object> clientMap) {
		System.out.println("enter into getVehPrdctTypeFrIndent function");
		Map<String,Object> map = new HashMap<>();
		try {
			String dt= (String) clientMap.get("indentPlacementDtTime");
			String[] sp=dt.split("T");
			Date date=Date.valueOf(sp[0]);
			System.out.println("indentPlacementDtTime= "+date+"clientMap.get(\"contCode\").toString()="+clientMap.get("contCode").toString());
			String contCode= (String) clientMap.get("contCode");
			String toStnCode= (String) clientMap.get("toStnCode");//
			String vehicleType= (String) clientMap.get("vehicleType");
			//List<VehicleType> vtList= vehicleTypeDAO.getVehTypeFrIndent(contCode,toStnCode,date);
			
			List<ContToStn> ctsList=contToStnDAO.getContToStnFrIndent(contCode, toStnCode, vehicleType, date);
			
			if(ctsList.isEmpty()) {
				
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
				
			}else {
				map.put("ctsList", ctsList);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}
			
			
		}catch (Exception e) {
			e.printStackTrace();
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/saveIndent", method = RequestMethod.POST)
	public @ResponseBody Object saveIndent(@RequestBody Map<String, Object> clientMap) {
		System.out.println("enter into getVehPrdctTypeFrIndent function");
		Map<String,Object> map = new HashMap<>();
		Session session=this.sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		try {
		 
	        String FORMAT_DATETIME = "yyyy-MM-dd'T'HH:mm";
	        SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_DATETIME);
	        java.util.Date date =  sdf.parse(clientMap.get("indentDateTime").toString());
	        Calendar indentDt = Calendar.getInstance();
	        indentDt.setTime(date);
	        
	        java.util.Date pdate =  sdf.parse(clientMap.get("placementDateTime").toString());
	        Calendar placementDt = Calendar.getInstance();
	        placementDt.setTime(pdate);
	        
	        String dt= (String) clientMap.get("placementDateTime");
			String[] sp=dt.split("T");
			Date pDate=Date.valueOf(sp[0]);
			System.out.println("placementDateTime= "+pDate);
			
			
			Indent indent=new Indent();
			indent.setOrderId(clientMap.get("orderId").toString());
			indent.setCtsId(Integer.parseInt(clientMap.get("ctsId").toString()));
			indent.setBranchName(clientMap.get("branchName").toString());
			//indent.setCnsneCnsnrName(clientMap.get("cnsneCnsnrName").toString());
			indent.setCustCode(clientMap.get("custCode").toString());
			//indent.setCutomerType(clientMap.get("cutomerType").toString());
			indent.setCutomerName(clientMap.get("custName").toString());
			if(clientMap.get("cutomerType").toString().equalsIgnoreCase("Consignor")) {
				indent.setConsigneeName(clientMap.get("cnsneCnsnrCode").toString());
				indent.setConsignorName(clientMap.get("custCode").toString());
			}else if(clientMap.get("cutomerType").toString().equalsIgnoreCase("Consignee")) {
				indent.setConsigneeName(clientMap.get("custCode").toString());
				indent.setConsignorName(clientMap.get("cnsneCnsnrCode").toString());
			}else {
				indent.setConsigneeName(clientMap.get("custCode").toString());
				indent.setConsignorName(clientMap.get("custCode").toString());
			}
			
			indent.setFromStn(clientMap.get("fromStn").toString());
			//indent.setGps(clientMap.get("gps").toString());
			indent.setIndentDateTime(indentDt);
			//indent.setKilometer((Integer)clientMap.get("kilometer"));
			indent.setMaterial(clientMap.get("material").toString());
			
			indent.setPlacementDateTime(placementDt);
			indent.setPrTonRate(Double.parseDouble(clientMap.get("prTonRate").toString()));
			indent.setTools(clientMap.get("tools").toString());
			indent.setToStn(clientMap.get("toStn").toString());
			indent.setTotalWeight(Double.parseDouble(clientMap.get("totalWeight").toString()));
			//indent.setTransitTime(Integer.parseInt(clientMap.get("transitTime").toString()));
			indent.setTrgtRate(Double.parseDouble(clientMap.get("trgtrate").toString()));
			indent.setVehicleRqr((Integer)clientMap.get("vehicleRqr"));
			indent.setVehicleType(clientMap.get("vehicleType").toString());
			indent.setMinGuaranteeWeight(Double.parseDouble(clientMap.get("minGuaranteeWeight").toString()));
			indent.setCnmtDimensionType(clientMap.get("cnmtDimensionType").toString());
			if(clientMap.get("dimension")!=null)
			indent.setDimension(clientMap.get("dimension").toString());
			indent.setAdvancePercent(Integer.parseInt(clientMap.get("advancePercent").toString()));
			indent.setAdvPayMode(clientMap.get("advPayMode").toString());
			indent.setBalPayMode(clientMap.get("balPayMode").toString());
			indent.setBalTerms(clientMap.get("balTerms").toString());
			indent.setIndentStatus("Pending");
			
			int indentId=indentDAO.saveNewIndent(indent, session);
			session.flush();
			session.clear();
			transaction.commit();
			
			
			
			map.put("indentId", indentId);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			map.put("msg","Please retry");
		}finally {
			session.close();
		}
		return map;
	}
	
	
	@RequestMapping(value = "/checkOrderIdExist", method = RequestMethod.POST)
	public @ResponseBody Object checkOrderIdExist(@RequestBody String orderId) {
		System.out.println("enter into checkOrderIdExist function");
		Map<String,Object> map = new HashMap<>();
		try {
			
			
			boolean flag=indentDAO.checkOrderIdExist(orderId);
			
			if(flag) {
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else {
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
			
			
		}catch (Exception e) {
			e.printStackTrace();
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/sendDetailToDialer", method = RequestMethod.POST)
	public @ResponseBody Object sendDetailToDialer(@RequestBody OwnerMasterService ownMstrSrvc) {
		System.out.println("enter into checkOrderIdExist function");
		Map<String,Object> map = new HashMap<>();
		try {
			System.out.println(ownMstrSrvc.getIndentId());
			System.out.println(ownMstrSrvc.getBrkList().get(0).getMaster_Id());
			
			Indent indent=indentDAO.getIndentById(ownMstrSrvc.getIndentId());
			
			ContToStn contToStn=contToStnDAO.getContToStnById(indent.getCtsId());
			
			System.out.println("contractCode="+contToStn.getCtsContCode().substring(0, 2));
			if(contToStn.getCtsContCode().substring(0, 3).equalsIgnoreCase("reg")) {
				RegularContract reg=regularContractDAO.getRegularContractByContCode(contToStn.getCtsContCode());
				if(reg!=null)
					excelMap.put("transExpBearer", reg.getRegTransExpBearer());
				else
					excelMap.put("transExpBearer", "");
			}else {
				DailyContract dly=dailyContractDAO.getDailyContractByContCode(contToStn.getCtsContCode());
				if(dly!=null)
					excelMap.put("transExpBearer", dly.getDlyTransExpBearer());
				else
					excelMap.put("transExpBearer", "");
			}
			
			
			SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");

			String formatted = format1.format(indent.getPlacementDateTime().getTime());
			System.out.println(formatted);
			Date date=Date.valueOf(formatted);
			System.out.println(date);
			List<PenaltyBonusDetention> pbdList=penaltyBonusDetentionDAO.getPenaltyBonusDetention(contToStn.getCtsContCode(), date,  contToStn.getCtsToStn());
			
			if(!pbdList.isEmpty()) {
				for(PenaltyBonusDetention pbd:pbdList) {
					if(pbd.getPbdPenBonDet().equalsIgnoreCase("P")) {
						excelMap.put("penalty", pbd.getPbdAmt());
					}
					if(pbd.getPbdPenBonDet().equalsIgnoreCase("B")) {
						excelMap.put("bonus", pbd.getPbdAmt());
					}
					if(pbd.getPbdPenBonDet().equalsIgnoreCase("D")) {
						excelMap.put("det", pbd.getPbdAmt());
					}
				}
			}
			
			Station frmStn=stationDAO.getStationByStnCode(indent.getFromStn());
			Station tStn=stationDAO.getStationByStnCode(contToStn.getCtsToStn());
			
			tStn.getState().getStateName();
			
			String fromStn=frmStn.getStnName()+","+frmStn.getState().getStateName();
			String toStn=tStn.getStnName()+","+tStn.getState().getStateName();
			
			
			String customer=customerDAO.getCustNameByCustCode(indent.getCustCode());
			String consignor=customerDAO.getCustNameByCustCode(indent.getConsignorName());
			String consignee=customerDAO.getCustNameByCustCode(indent.getConsigneeName());
			
			
			//List<Map<String, Object>> excelMapList=new ArrayList<>();
			
			
			excelMap.put("indent", indent);
			excelMap.put("fromStn", fromStn);
			excelMap.put("toStn", toStn);
			excelMap.put("consignor", consignor);
			excelMap.put("consignee", consignee);
			excelMap.put("customer", customer);
			excelMap.put("ownBrkList", ownMstrSrvc.getBrkList());
			excelMap.put("cts", contToStn);
			
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			hitDialerSoftware();
		}catch (Exception e) {
			e.printStackTrace();
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/downloadIndentAllExcel", method = RequestMethod.POST)
	public void downloadIndentAllExcel(HttpServletResponse response) {
		
		
		System.out.println("Here........................................");
		XSSFWorkbook workbook = new XSSFWorkbook();

		// Header font
		XSSFFont fontHeader = workbook.createFont();
		fontHeader.setFontHeightInPoints((short) 11);
		fontHeader.setColor(IndexedColors.WHITE.getIndex());
		fontHeader.setFontName(FontFactory.COURIER_BOLD);
		fontHeader.setBold(true);

		// Header style
		XSSFCellStyle styleHeader = workbook.createCellStyle();
		styleHeader.setFont(fontHeader);
		styleHeader.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		styleHeader.setFillForegroundColor(IndexedColors.BLUE_GREY
				.getIndex());
		styleHeader.setFillPattern(CellStyle.SOLID_FOREGROUND);
		styleHeader.setBorderTop(XSSFCellStyle.BORDER_MEDIUM);
		styleHeader.setBorderRight(XSSFCellStyle.BORDER_MEDIUM);
		styleHeader.setBorderBottom(XSSFCellStyle.BORDER_MEDIUM);
		styleHeader.setBorderLeft(XSSFCellStyle.BORDER_MEDIUM);

		//
		XSSFCellStyle styleData = workbook.createCellStyle();
		styleData.setAlignment(XSSFCellStyle.ALIGN_CENTER);

		XSSFSheet sheet = workbook.createSheet("Indent");
		/*sheet.setColumnWidth(0, 20 * 256);
		sheet.setColumnWidth(1, 25 * 256);
		sheet.setColumnWidth(2, 35 * 256);
		sheet.setColumnWidth(3, 30 * 256);
		sheet.setColumnWidth(4, 30 * 256);
		sheet.setColumnWidth(5, 30 * 256);
		sheet.setColumnWidth(6, 30 * 256);
		sheet.setColumnWidth(7, 30 * 256);
		sheet.setColumnWidth(8, 30 * 256);
		sheet.setColumnWidth(9, 30 * 256);
		sheet.setColumnWidth(10, 30 * 256);
		sheet.setColumnWidth(11, 30 * 256);
		sheet.setColumnWidth(12, 40 * 256);
		sheet.setColumnWidth(13, 40 * 256);
		sheet.setColumnWidth(14, 30 * 256);
		sheet.setColumnWidth(15, 30 * 256);
		sheet.setColumnWidth(16, 30 * 256);
		sheet.setColumnWidth(17, 30 * 256);
		sheet.setColumnWidth(18, 30 * 256);
		sheet.setColumnWidth(19, 30 * 256);
		sheet.setColumnWidth(20, 30 * 256);
		sheet.setColumnWidth(21, 30 * 256);
		sheet.setColumnWidth(22, 30 * 256);
		sheet.setColumnWidth(23, 30 * 256);
		sheet.setColumnWidth(24, 30 * 256);
		sheet.setColumnWidth(25, 30 * 256);
		sheet.setColumnWidth(26, 30 * 256);
		sheet.setColumnWidth(27, 30 * 256);
		sheet.setColumnWidth(28, 30 * 256);
		sheet.setColumnWidth(29, 30 * 256);
		sheet.setColumnWidth(30, 30 * 256);
		sheet.setColumnWidth(31, 30 * 256);
		sheet.setColumnWidth(32, 30 * 256);
		sheet.setColumnWidth(33, 30 * 256);
		sheet.setColumnWidth(34, 30 * 256);
		sheet.setColumnWidth(35, 30 * 256);
		sheet.setColumnWidth(36, 30 * 256);*/
		XSSFRow row = sheet.createRow(0);

		XSSFCell cell = null;
		cell = row.createCell(0);
		cell.setCellValue("phone_no");
		cell.setCellStyle(styleHeader);
		
		cell = row.createCell(1);
		cell.setCellValue("unique_id");
		cell.setCellStyle(styleHeader);

		cell = row.createCell(2);
		cell.setCellValue("indent_code");
		cell.setCellStyle(styleHeader);

		cell = row.createCell(3);
		cell.setCellValue("vendor_code");
		cell.setCellStyle(styleHeader);

		cell = row.createCell(4);
		cell.setCellValue("consignor");
		cell.setCellStyle(styleHeader);

		cell = row.createCell(5);
		cell.setCellValue("consignee");
		cell.setCellStyle(styleHeader);

		cell = row.createCell(6);
		cell.setCellValue("vehicle_type");
		cell.setCellStyle(styleHeader);

		cell = row.createCell(7);
		cell.setCellValue("consignment_dimension");
		cell.setCellStyle(styleHeader);

		cell = row.createCell(8);
		cell.setCellValue("odc_dimension");
		cell.setCellStyle(styleHeader);

		cell = row.createCell(9);
		cell.setCellValue("no_of_vehicles_required");
		cell.setCellStyle(styleHeader);

		cell = row.createCell(10);
		cell.setCellValue("from_location");
		cell.setCellStyle(styleHeader);

		cell = row.createCell(11);
		cell.setCellValue("to_location");
		cell.setCellStyle(styleHeader);

		cell = row.createCell(12);
		cell.setCellValue("material_type");
		cell.setCellStyle(styleHeader);

		cell = row.createCell(13);
		cell.setCellValue("total_weight");
		cell.setCellStyle(styleHeader);

		cell = row.createCell(14);
		cell.setCellValue("placement_date_and_time");
		cell.setCellStyle(styleHeader);

		cell = row.createCell(15);
		cell.setCellValue("transit_time");
		cell.setCellStyle(styleHeader);

		cell = row.createCell(16);
		cell.setCellValue("detention_charge_per_day");
		cell.setCellStyle(styleHeader);

		cell = row.createCell(17);
		cell.setCellValue("detention_charge_applicable_after");
		cell.setCellStyle(styleHeader);

		cell = row.createCell(18);
		cell.setCellValue("halting_charge_per_day");
		cell.setCellStyle(styleHeader);

		cell = row.createCell(19);
		cell.setCellValue("delivery_delay_penalty_per_day");
		cell.setCellStyle(styleHeader);

		cell = row.createCell(20);
		cell.setCellValue("delay_penalty_applicable_after");
		cell.setCellStyle(styleHeader);

		cell = row.createCell(21);
		cell.setCellValue("transit_expense_bearer");
		cell.setCellStyle(styleHeader);

		cell = row.createCell(22);
		cell.setCellValue("tools_required");
		cell.setCellStyle(styleHeader);

		cell = row.createCell(23);
		cell.setCellValue("gps_required");
		cell.setCellStyle(styleHeader);

		cell = row.createCell(24);
		cell.setCellValue("minimum_weight_guarantee");
		cell.setCellStyle(styleHeader);

		cell = row.createCell(25);
		cell.setCellValue("per_tonne_rate");
		cell.setCellStyle(styleHeader);

		cell = row.createCell(26);
		cell.setCellValue("target_rate");
		cell.setCellStyle(styleHeader);

		cell = row.createCell(27);
		cell.setCellValue("advance_percentage");
		cell.setCellStyle(styleHeader);
		
		cell = row.createCell(28);
		cell.setCellValue("advance_payment_mode");
		cell.setCellStyle(styleHeader);
		
		cell = row.createCell(29);
		cell.setCellValue("balance_payment_mode");
		cell.setCellStyle(styleHeader);
	
		cell = row.createCell(30);
		cell.setCellValue("balance_payment_terms");
		cell.setCellStyle(styleHeader);
		
		cell = row.createCell(31);
		cell.setCellValue("vendor_name");
		cell.setCellStyle(styleHeader);
		
		cell = row.createCell(32);
		cell.setCellValue("alternate_contact_1");
		cell.setCellStyle(styleHeader);
		
		cell = row.createCell(33);
		cell.setCellValue("alternate_contact_2");
		cell.setCellStyle(styleHeader);
		
		cell = row.createCell(34);
		cell.setCellValue("address");
		cell.setCellStyle(styleHeader);
		
		cell = row.createCell(35);
		cell.setCellValue("agent_name ");
		cell.setCellStyle(styleHeader);
		
		cell = row.createCell(36);
		cell.setCellValue("call_again_time");
		cell.setCellStyle(styleHeader);
		

		int rowToCreate = 1;
		XSSFRow dataRow = null;
		XSSFCell dataCell = null;
		
		try {
		@SuppressWarnings("unchecked")
		List<OwnerMaster> brkList=(List<OwnerMaster>) this.excelMap.get("ownBrkList");
		Indent indent=(Indent) this.excelMap.get("indent");
		ContToStn cts=(ContToStn) this.excelMap.get("cts");
		
		if(!brkList.isEmpty()) {

			for(int i=0;i<brkList.size();i++) {
				dataRow = sheet.createRow(rowToCreate);
				dataCell = dataRow.createCell(0);
					dataCell.setCellStyle(styleData);
					dataCell.setCellValue(brkList.get(i).getPhone_No());

					
					dataCell = dataRow.createCell(1);
					dataCell.setCellStyle(styleData);
					dataCell.setCellValue("");//unique_id
					
					dataCell = dataRow.createCell(2);
					dataCell.setCellStyle(styleData);
					dataCell.setCellValue(indent.getIndentId());//indent_code
					
					dataCell = dataRow.createCell(3);
					dataCell.setCellStyle(styleData);
					dataCell.setCellValue(brkList.get(i).getMaster_Id());//vendor_code
					
					dataCell = dataRow.createCell(4);
					dataCell.setCellStyle(styleData);
					dataCell.setCellValue(this.excelMap.get("consignor").toString());//consignor
					
					dataCell = dataRow.createCell(5);
					dataCell.setCellStyle(styleData);
					dataCell.setCellValue(this.excelMap.get("consignee").toString());//consignee
					
					dataCell = dataRow.createCell(6);
					dataCell.setCellStyle(styleData);
					dataCell.setCellValue(indent.getVehicleType());//vehicle_type
					
					dataCell = dataRow.createCell(7);
					dataCell.setCellStyle(styleData);
					dataCell.setCellValue(indent.getCnmtDimensionType());//consignment_dimension
					
					dataCell = dataRow.createCell(8);
					dataCell.setCellStyle(styleData);
					dataCell.setCellValue(indent.getDimension());//odc_dimension
					
					dataCell = dataRow.createCell(9);
					dataCell.setCellStyle(styleData);
					dataCell.setCellValue(indent.getVehicleRqr());//no_of_vehicles_required
					
					dataCell = dataRow.createCell(10);
					dataCell.setCellStyle(styleData);
					dataCell.setCellValue(this.excelMap.get("fromStn").toString());//from_location
					
					dataCell = dataRow.createCell(11);
					dataCell.setCellStyle(styleData);
					dataCell.setCellValue(this.excelMap.get("toStn").toString());//to_location
									
					dataCell = dataRow.createCell(12);
					dataCell.setCellStyle(styleData);
					dataCell.setCellValue(cts.getCtsProductType());//material_type
					
					dataCell = dataRow.createCell(13);
					dataCell.setCellStyle(styleData);
					dataCell.setCellValue(indent.getTotalWeight());//total_weight
					
					SimpleDateFormat sdc = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
					dataCell = dataRow.createCell(14);
				    dataCell.setCellStyle(styleData);
					dataCell.setCellValue(sdc.format(indent.getPlacementDateTime().getTime()));//placement_date_and_time
				        
				        
					dataCell = dataRow.createCell(15);
					dataCell.setCellStyle(styleData);
					dataCell.setCellValue(cts.getCtsToTransitDay());//transit_time
					
					dataCell = dataRow.createCell(16);
					dataCell.setCellStyle(styleData);
					if(excelMap.get("det")!=null)
						dataCell.setCellValue(excelMap.get("det").toString());//detention_charge_per_day
					else
						dataCell.setCellValue(500);
					
					dataCell = dataRow.createCell(17);
					dataCell.setCellStyle(styleData);
					dataCell.setCellValue(cts.getCtsToTransitDay()+1);//detention_charge_applicable_after
					
					dataCell = dataRow.createCell(18);
					dataCell.setCellStyle(styleData);
					dataCell.setCellValue("");//halting_charge_per_day
					
					dataCell = dataRow.createCell(19);
					dataCell.setCellStyle(styleData);
					if(excelMap.get("det")!=null)
						dataCell.setCellValue(excelMap.get("penalty").toString());//detention_charge_per_day
					else
						dataCell.setCellValue(500);//delivery_delay_penalty_per_day
					
					dataCell = dataRow.createCell(20);
					dataCell.setCellStyle(styleData);
					dataCell.setCellValue(cts.getCtsToTransitDay()+1);//delay_penalty_applicable_after
					
					dataCell = dataRow.createCell(21);
					dataCell.setCellStyle(styleData);
					if(this.excelMap.get("transExpBearer")!=null)
						dataCell.setCellValue(this.excelMap.get("transExpBearer").toString());//transit_expense_bearer
					else
						dataCell.setCellValue("Vendor");
					
					dataCell = dataRow.createCell(22);
					dataCell.setCellStyle(styleData);
					dataCell.setCellValue(indent.getTools());//tools_required
					
					dataCell = dataRow.createCell(23);
					dataCell.setCellStyle(styleData);
					dataCell.setCellValue(cts.getCtsGpsRequir());//gps_required
					
					dataCell = dataRow.createCell(24);
					dataCell.setCellStyle(styleData);
					dataCell.setCellValue(indent.getMinGuaranteeWeight());//minimum_weight_guarantee
					
					dataCell = dataRow.createCell(25);
					dataCell.setCellStyle(styleData);
					dataCell.setCellValue(indent.getPrTonRate());//per_tonne_rate
					
					dataCell = dataRow.createCell(26);
					dataCell.setCellStyle(styleData);
					dataCell.setCellValue(indent.getTrgtRate());//target_rate
					
					dataCell = dataRow.createCell(27);
					dataCell.setCellStyle(styleData);
					dataCell.setCellValue(indent.getAdvancePercent());//advance_percentage
					
					dataCell = dataRow.createCell(28);
					dataCell.setCellStyle(styleData);
					dataCell.setCellValue(indent.getAdvPayMode());//advance_payment_mode
					
					dataCell = dataRow.createCell(29);
					dataCell.setCellStyle(styleData);
					dataCell.setCellValue(indent.getBalPayMode());//balance_payment_mode
					
					dataCell = dataRow.createCell(30);
					dataCell.setCellStyle(styleData);
					dataCell.setCellValue(indent.getBalTerms());//balance_payment_terms
					
					dataCell = dataRow.createCell(31);
					dataCell.setCellStyle(styleData);
					dataCell.setCellValue(brkList.get(i).getCompany_Name());//vendor_name
					
					dataCell = dataRow.createCell(32);
					dataCell.setCellStyle(styleData);
					dataCell.setCellValue(brkList.get(i).getCompany_Owner_Mobile_1());//alternate_contact_1
					
					dataCell = dataRow.createCell(33);
					dataCell.setCellStyle(styleData);
					dataCell.setCellValue(brkList.get(i).getCompany_Owner_Mobile_2());//alternate_contact_2
					
					dataCell = dataRow.createCell(34);
					dataCell.setCellStyle(styleData);
					dataCell.setCellValue(brkList.get(i).getCompany_Office_Address());//address
					
					dataCell = dataRow.createCell(35);
					dataCell.setCellStyle(styleData);
					dataCell.setCellValue("");//agent_name
					
					dataCell = dataRow.createCell(36);
					dataCell.setCellStyle(styleData);
					dataCell.setCellValue("");//call_again_time
					
					rowToCreate++;

			}

		}
		
		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition",
				"attachment; filename=indent.xlsx");
		ServletOutputStream out;
		
			out = response.getOutputStream();
			workbook.write(out);
			out.flush();
			out.close();


		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		

	}
	
	
	public void hitDialerSoftware() throws IOException {
		@SuppressWarnings("unchecked")
		List<OwnerMaster> brkList = (List<OwnerMaster>) this.excelMap.get("ownBrkList");
		Indent indent = (Indent) this.excelMap.get("indent");
		ContToStn cts = (ContToStn) this.excelMap.get("cts");

		if (!brkList.isEmpty()) {

			for (int i = 0; i < brkList.size(); i++) {

				SimpleDateFormat sdc = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");

				int det = 0;
				if (excelMap.get("det") != null)
					det = Integer.parseInt(excelMap.get("det").toString());// detention_charge_per_day
				else
					det = 500;

				int penalty = 0;
				if (excelMap.get("penalty") != null)
					penalty = Integer.parseInt(excelMap.get("penalty").toString());// detention_charge_per_day
				else
					penalty = 500;

				String fromLocation = URLEncoder.encode(this.excelMap.get("fromStn").toString(), "UTF-8");
				String toLocation = URLEncoder.encode(this.excelMap.get("toStn").toString(), "UTF-8");
				String consignor = URLEncoder.encode(this.excelMap.get("consignor").toString(), "UTF-8");
				String consignee = URLEncoder.encode(this.excelMap.get("consignee").toString(), "UTF-8");
				String productType = URLEncoder.encode(indent.getMaterial(), "UTF-8");
				String vehicleType = URLEncoder.encode(indent.getVehicleType(), "UTF-8");
				String placementDtTime = URLEncoder.encode(sdc.format(indent.getPlacementDateTime().getTime()),
						"UTF-8");
				String tools = URLEncoder.encode(indent.getTools(), "UTF-8");
				String balTerms = URLEncoder.encode(indent.getBalTerms(), "UTF-8");
				String vendorName = URLEncoder.encode(brkList.get(i).getCompany_Name(), "UTF-8");
				String address = URLEncoder.encode(brkList.get(i).getCompany_Office_Address(), "UTF-8");
				String balPayMode = URLEncoder.encode(indent.getBalPayMode(), "UTF-8");
				String advPayMode = URLEncoder.encode(indent.getAdvPayMode(), "UTF-8");
				System.out
						.println("5ssss" + brkList.get(i).getPhone_No() + "masterId=" + brkList.get(i).getMaster_Id());

				String requestUrl = "http://103.227.69.202:8086/ExternalAPI.aspx?RequestFor=INSERTRECORD&Pid=10008&Cid=2002&phoneNo="
						+ brkList.get(i).getPhone_No() + "&unique_id=" + "ind0" + indent.getIndentId() + i
						+ "&indent_code='" + indent.getIndentId() + "'&vendor_code='" + brkList.get(i).getMaster_Id()
						+ "'&consignor='" + consignor + "'&consignee='" + consignee + "'&vehicle_type='" + vehicleType
						+ "'&consignment_dimension='" + indent.getCnmtDimensionType() + "'&odc_dimension='"
						+ indent.getDimension() + "'&no_of_vehicles_required=" + indent.getVehicleRqr()
						+ "&from_location='" + fromLocation + "'&to_location='" + toLocation + "'&material_type='"
						+ productType + "'&total_weight='" + indent.getMinGuaranteeWeight()
						+ "'&placement_date_and_time='" + placementDtTime + "'&transit_time=" + cts.getCtsToTransitDay()
						+ "&detention_charge_per_day='" + det + "'&detention_charge_applicable_after='"
						+ (cts.getCtsToTransitDay() + 1)
						+ "'&halting_charge_per_day=''&delivery_delay_penalty_per_day='" + penalty
						+ "'&delay_penalty_applicable_after='" + (cts.getCtsToTransitDay() + 1)
						+ "'&transit_expense_bearer='Broker'&tools_required='" + tools + "'&gps_required='"
						+ cts.getCtsGpsRequir() + "'&minimum_weight_guarantee='" + indent.getMinGuaranteeWeight()
						+ "'&per_tonne_rate='" + indent.getPrTonRate() + "'&target_rate=" + indent.getTrgtRate()
						+ "&advance_percentage=" + indent.getAdvancePercent() + "&advance_payment_mode='" + advPayMode
						+ "'&balance_payment_mode='" + balPayMode + "'&balance_payment_terms='" + balTerms
						+ "'&vendor_name='" + vendorName + "'&alternate_contact_1=''&alternate_contact_2=''&address='"
						+ address + "'&SetCallBack=0&UpdateMaster=1&FileName=" + "indent" + indent.getIndentId();
				System.out.println(indent.getIndentId() + i);
				System.out.println(requestUrl + "5ssss" + brkList.get(i).getPhone_No());
				URL url = new URL(requestUrl);
				HttpURLConnection uc = (HttpURLConnection) url.openConnection();
				System.out.println(uc.getResponseMessage() + " ssss");
				String resp = uc.getResponseMessage();
				System.out.println(resp);

				uc.disconnect();
			}
		}
	}
	
	
	@RequestMapping(value = "/getIndentFrCampaignResult", method = RequestMethod.POST)
	public @ResponseBody Object getIndentFrCampaignResult() {
		System.out.println("enter into getCustFrBillF function");
		Map<String,Object> map = new HashMap<>();
		List<Indent> indentList = new ArrayList<>();
		indentList = indentDAO.getIndentByStatus("Pending");
		/*SimpleDateFormat sdc = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		System.out.println(sdc.format(indentList.get(0).getPlacementDateTime().getTime()));
		*/List<Map<String, Object>> list=new ArrayList<>();
		
		List<Station>stnList=stationDAO.getStationData();
		Map<String,String> stnMap=new HashMap<>();
		for(Station stn:stnList) {
			stnMap.put(stn.getStnCode(), stn.getStnName());
		}
		
		for(Indent ind:indentList) {
			Map<String,Object> tempMap=new HashMap<>();
			tempMap.put("indentId", ind.getIndentId());
			tempMap.put("branchName", ind.getBranchName());
			tempMap.put("cutomerName", ind.getCutomerName());
			tempMap.put("fromStn", stnMap.get(ind.getFromStn()));
			tempMap.put("toStn", stnMap.get(ind.getToStn()));
			tempMap.put("placementDateTime", ind.getPlacementDateTime());
			tempMap.put("vehicleType", ind.getVehicleType());
			list.add(tempMap);
		}
		
		if(!indentList.isEmpty()){
			map.put("list",list);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/getIndentFrPlacementRequest", method = RequestMethod.POST)
	public @ResponseBody Object getIndentFrPlacementRequest(@RequestBody Map<String, Object> clientMap) {
		System.out.println("enter into getIndentFrPlacementRequest function");
		Map<String,Object> map = new HashMap<>();
		List<Indent> indentList = new ArrayList<>();
		Employee employee=employeeDAO.getCmpltEmp(clientMap.get("empId").toString());
		
		indentList = indentDAO.getIndentByBranchAndStage("Vehicle Engaged",employee.getPrBranch().getBranchName());
		List<Map<String, Object>> list=new ArrayList<>();
		
		List<Station>stnList=stationDAO.getStationData();
		Map<String,String> stnMap=new HashMap<>();
		for(Station stn:stnList) {
			stnMap.put(stn.getStnCode(), stn.getStnName());
		}
		
		
		System.out.println("indentId="+indentList.size());
		for(Indent ind:indentList) {
			System.out.println("indentId="+ind.getIndentId());
			
			SimpleDateFormat sdc = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
			System.out.println(sdc.format(ind.getPlacementDateTime().getTime()));
			
			
			List<CampaignResults> campaignResultsList=campaignResultDAO.getCampaignResultByIndentId(ind.getIndentId());
			
			List<VehicleEngagement> veList= vehicleEngagementDAO.getVehicleEngagementByCampainResult(campaignResultsList);
			
			for(VehicleEngagement ve:veList) {
				Map<String,Object> tempMap=new HashMap<>();
				tempMap.put("indentId", ind.getIndentId());
				tempMap.put("branchName", ind.getBranchName());
				tempMap.put("cutomerName", ind.getCutomerName());
//				tempMap.put("fromStn", stnMap.get(ind.getFromStn()));
//				tempMap.put("toStn", stnMap.get(ind.getToStn()));
				tempMap.put("placementDateTime", sdc.format(ind.getPlacementDateTime().getTime()));
//				tempMap.put("vehicleType", ind.getVehicleType());
//				tempMap.put("totalWeight", ind.getTotalWeight());
//				tempMap.put("tools", ind.getTools());
//				tempMap.put("dimension", ind.getDimension());
//				tempMap.put("dimensionType", ind.getCnmtDimensionType());
				tempMap.put("vehicleNo", ve.getVehicleNo());
//				tempMap.put("driverName", ve.getDriverName());
//				tempMap.put("driverPhoneNo", ve.getDriverNo());
				tempMap.put("vEId", ve.getvEId());
				
				if(ve.getPlaceReqStatus()==null) {
					tempMap.put("status", "CNMT Pending");
				}else {
					Cnmt cnmt=cnmtDAO.getCnmtByVeId(ve.getvEId());
					tempMap.put("cnmtCode", cnmt.getCnmtCode());
					tempMap.put("status", "Challan Pending");
					tempMap.put("driverName", ve.getDriverName());
					tempMap.put("driverPhoneNo", ve.getDriverNo());
					
				}
				
				list.add(tempMap);
			}
		}
		
		if(!indentList.isEmpty()){
			map.put("data",list);
			map.put("isSuccess",true);
		}else{
			map.put("data",null);
			map.put("isSuccess",false);
			map.put("message","There is no any pending request!");
		}
		return map;
	}
	
	
	@RequestMapping(value = "/getPlacementRequestDetailByV", method = RequestMethod.POST)
	public @ResponseBody Object getPlacementRequestDetailByV(@RequestBody Map<String, Object> clientMap) {
		System.out.println("enter into getIndentFrPlacementRequest function");
		Map<String,Object> map = new HashMap<>();
		
		VehicleEngagement ve=vehicleEngagementDAO.getVehicleEngagementByVeId(Integer.parseInt(clientMap.get("vEId").toString()));
		
		if(ve!=null) {
			
			
			Indent ind = indentDAO.getIndentById(ve.getCampaignResults().getIndentId());
			
			List<Station>stnList=stationDAO.getStationData();
			Map<String,String> stnMap=new HashMap<>();
			for(Station stn:stnList) {
				stnMap.put(stn.getStnCode(), stn.getStnName());
			}
				System.out.println("indentId="+ind.getIndentId());
				
				SimpleDateFormat sdc = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
				System.out.println(sdc.format(ind.getPlacementDateTime().getTime()));
				
				
					Map<String,Object> tempMap=new HashMap<>();
					tempMap.put("indentId", ind.getIndentId());
					tempMap.put("branchName", ind.getBranchName());
					tempMap.put("cutomerName", ind.getCutomerName());
					tempMap.put("fromStn", stnMap.get(ind.getFromStn()));
					tempMap.put("toStn", stnMap.get(ind.getToStn()));
					tempMap.put("placementDateTime", sdc.format(ind.getPlacementDateTime().getTime()));
					tempMap.put("vehicleType", ind.getVehicleType());
					tempMap.put("totalWeight", ind.getTotalWeight());
					tempMap.put("tools", ind.getTools());
					tempMap.put("dimension", ind.getDimension());
					tempMap.put("dimensionType", ind.getCnmtDimensionType());
					tempMap.put("vehicleNo", ve.getVehicleNo());
					tempMap.put("driverName", ve.getDriverName());
					tempMap.put("driverPhoneNo", ve.getDriverNo());
					
					
					
					tempMap.put("vEId", ve.getvEId());
			
				map.put("data",tempMap);
				map.put("isSuccess",true);
		}else {
			map.put("data",null);
			map.put("isSuccess",false);
		}
		
		return map;
	}
	
	
	@RequestMapping(value = "/getPlacementRequestDetailFrCnmtByVeId", method = RequestMethod.POST)
	public @ResponseBody Object getPlacementRequestDetailFrCnmtByVeId(@RequestBody Map<String, Object> clientMap) {
		System.out.println("enter into getIndentFrPlacementRequest function");
		Map<String,Object> map = new HashMap<>();
		
		VehicleEngagement ve=vehicleEngagementDAO.getVehicleEngagementByVeId(Integer.parseInt(clientMap.get("vEId").toString()));
		
		if(ve!=null) {
			
			
			Indent ind = indentDAO.getIndentById(ve.getCampaignResults().getIndentId());
			
			ContToStn cts=contToStnDAO.getContToStnById(ind.getCtsId());
			
			String consignee=customerDAO.getCustNameByCustCode(ind.getConsigneeName());
			String consignor=customerDAO.getCustNameByCustCode(ind.getConsignorName());
			
			List<Station>stnList=stationDAO.getStationData();
			Map<String,String> stnMap=new HashMap<>();
			for(Station stn:stnList) {
				stnMap.put(stn.getStnCode(), stn.getStnName());
			}
				System.out.println("indentId="+ind.getIndentId());
				
				SimpleDateFormat sdc = new SimpleDateFormat("dd-MM-yyyy");
				System.out.println(sdc.format(ind.getPlacementDateTime().getTime()));
				
				
					Map<String,Object> tempMap=new HashMap<>();
					tempMap.put("indentId", ind.getIndentId());
					tempMap.put("branchName", ind.getBranchName());
					tempMap.put("cutomerName", ind.getCutomerName());
					tempMap.put("consignee", consignee);
					tempMap.put("consignor", consignor);
					tempMap.put("fromStn", stnMap.get(ind.getFromStn()));
					tempMap.put("toStn", stnMap.get(ind.getToStn()));
					//tempMap.put("placementDate", sdc.format(ind.getPlacementDateTime().getTime()));
					tempMap.put("vehicleType", ind.getVehicleType());
					tempMap.put("material", cts.getCtsProductType());
					tempMap.put("guaranteeWeight", cts.getCtsToWt()/1000);
					//tempMap.put("totalWeight", ind.getTotalWeight());
					//tempMap.put("tools", ind.getTools());
					//tempMap.put("dimension", ind.getDimension());
					//tempMap.put("dimensionType", ind.getCnmtDimensionType());
					tempMap.put("vehicleNo", ve.getVehicleNo());
					//tempMap.put("driverName", ve.getDriverName());
					//tempMap.put("driverPhoneNo", ve.getDriverNo());
					tempMap.put("vEId", ve.getvEId());
			
				map.put("data",tempMap);
				map.put("isSuccess",true);
		}else {
			map.put("data",null);
			map.put("isSuccess",false);
		}
		
		return map;
	}
	
	
	@RequestMapping(value = "/editPlacementRequestDetailFrVehicleNo", method = RequestMethod.POST)
	public @ResponseBody Object editPlacementRequestDetailFrVehicleNo(@RequestBody Map<String, Object> clientMap) {
		
		VehicleEngagement ve=vehicleEngagementDAO.getVehicleEngagementByVeId(Integer.parseInt(clientMap.get("vEId").toString()));
		if(ve.getVehicleNo().equalsIgnoreCase(clientMap.get("vehicleNo").toString())) {
			VehicleVendorMstr vv=vehicleVendorDAO.getVehByVehNo(ve.getVehicleNo());
			
		}
		
		
		return null;
	}
	

	@RequestMapping(value = "/saveCNMTByApp", method = RequestMethod.POST)
	public @ResponseBody Object saveCNMTByApp(@RequestBody Map<String, Object> clientMap) {
		System.out.println("enter into getVehPrdctTypeFrIndent function");
		Map<String,Object> map = new HashMap<>();
		Session session=this.sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		try {
			
			int vEId=Integer.parseInt(clientMap.get("vEId").toString());
			String empId= (String) clientMap.get("empId");
			
			Cnmt existCnmt=cnmtDAO.getCnmtByVeId(vEId);
			if(existCnmt!=null) {
				map.put("isSuccess",false);
				map.put("msg","CNMT already generated for this Vehicle Engagement!");
				return map;
			}
			
			String packaging=(String) clientMap.get("packaging");
			int quantity=Integer.parseInt(clientMap.get("quantity").toString());
			double actualWeight=Double.parseDouble(clientMap.get("actualWeight").toString());
			String ewbBillNo=(String) clientMap.get("ewbBillNo");
			Date ewbBillDate=Date.valueOf(clientMap.get("ewbBillDate").toString());
			Date ewbBillValidDate=Date.valueOf(clientMap.get("ewbBillValidDate").toString());
			//Date gateOutDate=Date.valueOf(clientMap.get("gateOutDate").toString());
			int valueOfGood=Integer.parseInt(clientMap.get("valueOfGood").toString());
			
	        String FORMAT_DATETIMEA = "yyyy-MM-dd HH:mm:ss";
	        SimpleDateFormat sdf1= new SimpleDateFormat(FORMAT_DATETIMEA);
	        java.util.Date date1 =  sdf1.parse(clientMap.get("placedOn").toString());
	        Calendar cal1 = Calendar.getInstance();
	        cal1.setTime(date1);
	        
	        SimpleDateFormat sdf2= new SimpleDateFormat(FORMAT_DATETIMEA);
	        java.util.Date date2 =  sdf1.parse(clientMap.get("gateOutDate").toString());
	        Calendar cal2 = Calendar.getInstance();
	        cal2.setTime(date2);
	        Date gateOutDate=new Date(cal2.getTimeInMillis());
	        String gateOutTime=cal2.get(Calendar.HOUR_OF_DAY)+":"+cal2.get(Calendar.MINUTE);
	        
			List<Map<String, Object>> invoiceList=(List<Map<String, Object>>) clientMap.get("invoiceList");
			System.out.println("invoiceList="+invoiceList);
			Employee employee=employeeDAO.getCmpltEmp(empId);
			VehicleEngagement ve=vehicleEngagementDAO.getVehicleEngagementByVeId(vEId);
			
				
				Indent ind = indentDAO.getIndentById(ve.getCampaignResults().getIndentId());
				
				ContToStn cts=contToStnDAO.getContToStnById(ind.getCtsId());
		
				String contractBillBasis=null;
				String conttype=null;
				if(cts.getCtsContCode().substring(0, 3).equalsIgnoreCase("reg")) {
					RegularContract regularContract=regularContractDAO.getRegularContractByContCode(cts.getCtsContCode());
					contractBillBasis=regularContract.getRegContBillBasis();
					conttype=regularContract.getRegContType();
				}else {
					DailyContract regularContract=dailyContractDAO.getDailyContractByContCode(cts.getCtsContCode());
					contractBillBasis=regularContract.getDlyContBillBasis();
					conttype=regularContract.getDlyContType();
				}
				
				Cnmt cnmt=new Cnmt();
				cnmt.setbCode(employee.getPrBranch().getBranchCode());
				cnmt.setBranchCode(employee.getPrBranch().getBranchCode());
				cnmt.setCnmtActualWt(actualWeight*1000);
				cnmt.setCnmtBillAt("1");
				cnmt.setCnmtGtOutTime(gateOutTime);
				cnmt.setCnmtConsignee(ind.getConsigneeName());
				cnmt.setCnmtConsignor(ind.getConsignorName());
				cnmt.setCnmtDC("1 bill");
				cnmt.setCnmtBillPermission("no");
				cnmt.setCnmtDt(new Date(new java.util.Date().getTime()));
				//cnmt.setCnmtDtOfDly(placedOn);
				cnmt.setCnmtEmpCode(employee.getEmpCode());
				
				if(contractBillBasis.equalsIgnoreCase("fixed")) {
					cnmt.setCnmtFreight(1000*cts.getCtsRate());
					cnmt.setCnmtTOT(Math.round(1000*cts.getCtsRate()));
					
				}else if(conttype.equalsIgnoreCase("Q")) {
					cnmt.setCnmtFreight(quantity*cts.getCtsRate());
					cnmt.setCnmtTOT(Math.round(quantity*cts.getCtsRate()));
					
				}else {
					cnmt.setCnmtFreight(cts.getCtsToWt()*cts.getCtsRate());
					cnmt.setCnmtTOT(Math.round(cts.getCtsToWt()*cts.getCtsRate()));
					
				}
				
				cnmt.setCnmtFromSt(ind.getFromStn());
				cnmt.setCnmtGtOutDt(gateOutDate);
				cnmt.setCnmtGuaranteeWt(cts.getCtsToWt());
				cnmt.setCnmtInvoiceNo((ArrayList<Map<String, Object>>) invoiceList);
				cnmt.setCnmtNoOfPkg(quantity);
				cnmt.setCnmtProductType(ind.getMaterial());
				cnmt.setCnmtRate(cts.getCtsRate());
				cnmt.setCnmtToSt(cts.getCtsToStn());
				cnmt.setCnmtVehicleType(cts.getCtsVehicleType());
				cnmt.setCnmtVOG(valueOfGood);
				cnmt.setContractCode(cts.getCtsContCode());
				cnmt.setCustCode(ind.getCustCode());
				cnmt.setCnmtEWBDt(ewbBillDate);
				cnmt.setCnmtEWBNo(ewbBillNo);
				cnmt.setCnmtEWBValidDt(ewbBillValidDate);
				cnmt.setCnmtPackaging(packaging);
				cnmt.setPlacedOn(cal1);
				cnmt.setUserCode(employee.getEmpCode());
				cnmt.setRemarks(clientMap.get("remarks").toString());
				//cnmt.setVehicleEngagement(ve);
				cnmt.setvEId(vEId);
				if(ind.getCustCode().equalsIgnoreCase("1754") ||ind.getCustCode().equalsIgnoreCase("1755") || ind.getCustCode().equalsIgnoreCase("1756")) {
					cnmt.setPrBlCode(ind.getCustCode());
					//cnmt.setWhCode(0);
					if(ind.getCustCode().equalsIgnoreCase("1754"))
							cnmt.setRelType("P");
					if(ind.getCustCode().equalsIgnoreCase("1755"))
						cnmt.setRelType("S");
					if(ind.getCustCode().equalsIgnoreCase("1756"))
						cnmt.setRelType("O");
				}
				
				
			int cnmtId=cnmtDAO.saveCnmt(cnmt, session);
			
			String bcode=null;
			if(Integer.parseInt(employee.getPrBranch().getBranchCode())<10) {
				bcode="0"+employee.getPrBranch().getBranchCode();
			}else {
				bcode=employee.getPrBranch().getBranchCode();
			}
			String custCode=""+(100000+Integer.parseInt(ind.getCustCode()));
			
			/*int cnmtCode=90000000+cnmtId;*/
			cnmt.setCnmtCode("CG"+bcode+custCode.substring(2)+cnmtId);
			cnmtDAO.updateCnmt(cnmt, session);
				
			ve.setPlaceReqStatus("Pending Challan");
			vehicleEngagementDAO.updateVehicleEngagement(ve, session);
			session.flush();
			session.clear();
			transaction.commit();
			map.put("cnmtCode",cnmt.getCnmtCode());
			map.put("isSuccess",true);
		}catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();
			map.put("isSuccess",false);
			map.put("msg","Please retry");
		}finally {
			session.close();
		}
		return map;
		}

	
	@RequestMapping(value = "/printCnmtPdf", method = RequestMethod.POST)
	public ModelAndView printCnmtPdf(@RequestBody Map<String, Object> clientMap) {
		System.out.println("enter into printCnmtPdf funciton");
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		List<Cnmt> cnmtList=cnmtDAO.getCnmt(clientMap.get("cnmtCode").toString());
		Cnmt cnmt=null;
		if(!cnmtList.isEmpty()) {
			cnmt=cnmtList.get(0);
			
			List<CustomerRepresentative> crList=customerRepresentativeDAO.getCustomerRepByCustCode(cnmt.getCustCode());
	        
			
			VehicleEngagement ve=vehicleEngagementDAO.getVehicleEngagementByVeId(cnmt.getvEId());
			
			Indent ind=indentDAO.getIndentById(ve.getCampaignResults().getIndentId());
			
			Customer consignor=customerDAO.getCustById(Integer.parseInt(cnmt.getCnmtConsignor()));
			Customer consignee=customerDAO.getCustById(Integer.parseInt(cnmt.getCnmtConsignee()));
			
			Station frmStn=stationDAO.getStationByStnCode(cnmt.getCnmtFromSt());
			Station tStn=stationDAO.getStationByStnCode(cnmt.getCnmtToSt());
			
			//tStn.getState().getStateName();
			
			String fromStn=frmStn.getStnName()+","+frmStn.getState().getStateName();
			String toStn=tStn.getStnName()+","+tStn.getState().getStateName();
			
			ContToStn cts=contToStnDAO.getContToStnById(ind.getCtsId());
			
			
			map.put("cnmt", cnmt);
			map.put("cts", cts);
			map.put("indent", ind);
			map.put("fromStn", fromStn);
			map.put("toStn", toStn);
			map.put("ve", ve);
			map.put("consignor", consignor);
			map.put("consignee", consignee);
			map.put("crList", crList);
		}
			
		
		/*map.put("faCodeNameList", "");
		map.put("trialReportMap", "");
		map.put("cashInClosing", "");*/
		
		/*String authentication = authentication();
		if(authentication.equalsIgnoreCase(UN_AUTH))
			return new ModelAndView("sessionExpired", null);
		else*/	
			return new ModelAndView("cnmtPdfView", map);
	}

}
