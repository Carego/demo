package com.mylogistics.services;

import java.util.ArrayList;
import java.util.List;
import com.mylogistics.model.PenaltyBonusDetention;

public class PBDServiceImpl implements PBDService{
	
	private static List<PenaltyBonusDetention> pbdList = new  ArrayList<PenaltyBonusDetention>();
	
	 public List<PenaltyBonusDetention> getAllPBD() {
	        return pbdList;
	    }

	 public void addPBD(PenaltyBonusDetention penaltyBonusDetention){
		 pbdList.add(penaltyBonusDetention);
	 }
	 
	 public void deletePBD(PenaltyBonusDetention penaltyBonusDetention){
     	System.out.println("enter into deletePBD of PBDServiceImpl");
     	
     	String temp = penaltyBonusDetention.getPbdFromStn()
     			+penaltyBonusDetention.getPbdToStn()
     			+penaltyBonusDetention.getPbdStartDt()
     			+penaltyBonusDetention.getPbdEndDt()
     			+penaltyBonusDetention.getPbdFromDay()
     			+penaltyBonusDetention.getPbdToDay()
     			+penaltyBonusDetention.getPbdVehicleType()
     			+penaltyBonusDetention.getPbdPenBonDet()
     			+penaltyBonusDetention.getPbdHourNDay()
     			+penaltyBonusDetention.getPbdAmt();
     			
     	
     	if (!pbdList.isEmpty()) {
     		System.out.println("size of pbdList = "+pbdList.size());
				for (int i = 0; i < pbdList.size(); i++) {
					String listTemp = pbdList.get(i).getPbdFromStn()
							+pbdList.get(i).getPbdToStn()
							+pbdList.get(i).getPbdStartDt()
							+pbdList.get(i).getPbdEndDt()
							+pbdList.get(i).getPbdFromDay()
							+pbdList.get(i).getPbdToDay()
							+pbdList.get(i).getPbdVehicleType()
							+pbdList.get(i).getPbdPenBonDet()
							+pbdList.get(i).getPbdHourNDay()
							+pbdList.get(i).getPbdAmt();
					if (temp.equalsIgnoreCase(listTemp)) {
						pbdList.remove(pbdList.get(i));
					}
				}
				System.out.println("after deleting size of pbdList = "+pbdList.size());
			}
	 }
	 
	 public void deleteAllPBD() {
		 
		 pbdList.clear();
	 	}
}