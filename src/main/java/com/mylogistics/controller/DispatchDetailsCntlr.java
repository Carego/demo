package com.mylogistics.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BranchStockDisDetDAO;
import com.mylogistics.DAO.BranchStockDispatchDAO;
import com.mylogistics.DAO.BranchStockLeafDetDAO;
import com.mylogistics.DAO.Optr_NotificationDAO;
import com.mylogistics.model.BranchStockDispatch;
import com.mylogistics.model.BranchStockLeafDet;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.DispatchCodeService;
import com.mylogistics.services.DispatchDetailService;

@Controller
public class DispatchDetailsCntlr {
	
	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private BranchStockDispatchDAO branchStockDispatchDAO;
	
	@Autowired
	private BranchStockDisDetDAO branchStockDisDetDAO;
	
	@Autowired
	private BranchStockLeafDetDAO branchStockLeafDetDAO;
	
	@Autowired
	private Optr_NotificationDAO optr_NotificationDAO;
	
	private DispatchCodeService dispatchCodeService = new DispatchCodeService();
	
	@RequestMapping(value = "/getBrStDisDetData", method = RequestMethod.POST)
	public @ResponseBody Object getBrStDisDetData(@RequestBody String dispatchCode) {
		System.out.println("enter into getBrStDisDetData function---->"+dispatchCode);
		Map<String, Object> map = new HashMap<String, Object>();
		//List<BranchStockDisDet> bStockDisDets = new ArrayList<BranchStockDisDet>();
		
		//User currentUser = (User)httpSession.getAttribute("currentUser");
		List<BranchStockDispatch> brSDList = branchStockDispatchDAO.getBSDataByDisCode(dispatchCode);
		List<String> cnmtStartNoList = branchStockDisDetDAO.getStartNo(dispatchCode,"cnmt");
		List<String> chlnStartNoList = branchStockDisDetDAO.getStartNo(dispatchCode,"chln");
		List<String> sedrStartNoList = branchStockDisDetDAO.getStartNo(dispatchCode,"sedr");
		
		System.out.println("branchDispatchCode = "+brSDList.get(0).getBrsDisCnmt());
		
		 if(!brSDList.isEmpty()){
			 	BranchStockDispatch branchStockDispatch = brSDList.get(0);
				map.put("brsdData", branchStockDispatch);
				map.put("cnmtStNoList", cnmtStartNoList);
				map.put("chlnStNoList", chlnStartNoList);
				map.put("sedrStNoList", sedrStartNoList);
				map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			} else {
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
		 return map;
	}
	
	@RequestMapping(value = "/setIsReceivedYesForOprtrNot", method = RequestMethod.POST)
	public @ResponseBody Object setIsReceivedYesForOprtrNot(@RequestBody DispatchDetailService dispatchDetailService) {
		System.out.println("enter into setIsReceivedYesForOprtrNot function"+dispatchDetailService.getBranchCode());
		int temp = optr_NotificationDAO.updateIsReceiveYes(dispatchDetailService.getDispatchCode());
		
		int msgUpadte=branchStockDispatchDAO.updateDispatchMsg(dispatchDetailService.getDispatchCode());
		System.out.println("------"+msgUpadte);
		User currentUser = (User)httpSession.getAttribute("currentUser");
		
		List<String> cnmtStartNoList = dispatchDetailService.getCnmtStartNoList();
		List<String> chlnStartNoList = dispatchDetailService.getChlnStartNoList();
		List<String> sedrStartNoList = dispatchDetailService.getSedrStartNoList();
		List<String> startNoLists = new ArrayList<String>();
		
		startNoLists.addAll(chlnStartNoList);
		startNoLists.addAll(cnmtStartNoList);
		startNoLists.addAll(sedrStartNoList);
		int sizeCnmt = cnmtStartNoList.size();
		int sizeChln = chlnStartNoList.size();
		int sizeSedr = sedrStartNoList.size();
		int totalSize = sizeChln+sizeCnmt+sizeSedr;
		System.out.println("totalSize========"+totalSize);
		String startNos[] = new String[totalSize]; 
		List<Long> totalStartNo = new ArrayList<Long>();
		for(int i =0;i<startNoLists.size();i++){
			startNos[i] = startNoLists.get(i);
			totalStartNo.add(Long.parseLong(startNos[i]));
		}
		 int updateIsVerify = branchStockDisDetDAO.updateisVerify(startNos, totalStartNo);
		 
		 
		 System.out.println("After updating is verify-----"+updateIsVerify);
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		int cnmtTemp = 0;
		int chlnTemp = 0;
		int sedrTemp = 0;
		
		if(temp>0){
			//String branchCode = branchStockDispatchDAO.getBrCodeByDisCode(dispatchDetailService.getDispatchCode(),dispatchDetailService.getReceiveDate());
			String branchCode = dispatchDetailService.getBranchCode();
			System.out.println("*************branchCode = "+branchCode);
			if(branchCode!=null && !branchCode.isEmpty()){
				if(!cnmtStartNoList.isEmpty()){
					for(int i=0;i<cnmtStartNoList.size();i++){
						BranchStockLeafDet branchStockLeafDet = new BranchStockLeafDet();
						branchStockLeafDet.setbCode(currentUser.getUserBranchCode());
						branchStockLeafDet.setUserCode(currentUser.getUserCode());
						branchStockLeafDet.setBrsLeafDetBrCode(branchCode);
						branchStockLeafDet.setBrsLeafDetSNo(cnmtStartNoList.get(i));
						branchStockLeafDet.setBrsLeafDetStatus("cnmt");
						cnmtTemp = branchStockLeafDetDAO.saveBranchStockLeafDet(branchStockLeafDet);
					}
				}
				if(!chlnStartNoList.isEmpty()){
					for(int i=0;i<chlnStartNoList.size();i++){
						BranchStockLeafDet branchStockLeafDet = new BranchStockLeafDet();
						branchStockLeafDet.setbCode(currentUser.getUserBranchCode());
						branchStockLeafDet.setUserCode(currentUser.getUserCode());
						branchStockLeafDet.setBrsLeafDetBrCode(branchCode);
						branchStockLeafDet.setBrsLeafDetSNo(chlnStartNoList.get(i));
						branchStockLeafDet.setBrsLeafDetStatus("chln");
						chlnTemp = branchStockLeafDetDAO.saveBranchStockLeafDet(branchStockLeafDet);
					}
				}
				if(!sedrStartNoList.isEmpty()){
					for(int i=0;i<sedrStartNoList.size();i++){
						BranchStockLeafDet branchStockLeafDet = new BranchStockLeafDet();
						branchStockLeafDet.setbCode(currentUser.getUserBranchCode());
						branchStockLeafDet.setUserCode(currentUser.getUserCode());
						branchStockLeafDet.setBrsLeafDetBrCode(branchCode);
						branchStockLeafDet.setBrsLeafDetSNo(sedrStartNoList.get(i));
						branchStockLeafDet.setBrsLeafDetStatus("sedr");
						sedrTemp = branchStockLeafDetDAO.saveBranchStockLeafDet(branchStockLeafDet);
					}
				}
			}
			
			////
			
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/saveDispatchCode", method = RequestMethod.POST)
	public @ResponseBody Object saveDispatchCode(@RequestBody String dispatchCode) {
		System.out.println("enter into saveDispatchCode function (DispatchCode = "+dispatchCode+" )");
		Map<String, Object> map = new HashMap<String, Object>();
		dispatchCodeService.setDispatchCode(dispatchCode);
		map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		return map;
	}
	
	
	@RequestMapping(value = "/getDispatchCode", method = RequestMethod.POST)
	public @ResponseBody Object getDispatchCode(){
		System.out.println("enter into getDispatchCode function");
		Map<String, Object> map = new HashMap<String, Object>();
		String dispatchCode= dispatchCodeService.getDispatchCode();
		System.out.println("---------------------------------"+dispatchCode);
		map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		map.put("dispatchCode",dispatchCode);
		return map;
	}
}
