package com.mylogistics.controller.bank;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.poi.ss.formula.constant.ConstantValueParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.bank.BankMstrDAO;
import com.mylogistics.DAO.bank.ChequeBookDAO;
import com.mylogistics.DAO.bank.ChequeLeavesDAO;
import com.mylogistics.model.Branch;
import com.mylogistics.model.Employee;
import com.mylogistics.model.User;
import com.mylogistics.model.bank.BankMstr;
import com.mylogistics.model.bank.ChequeBook;
import com.mylogistics.model.bank.ChequeLeaves;
import com.mylogistics.model.bank.ChequeRequest;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.bank.ChequeService;

@Controller
public class ChequeCntlr {

	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private ChequeBookDAO chequeBookDAO;
	
	@Autowired
	private ChequeLeavesDAO chequeLeavesDAO;
	
	@Autowired
	private BankMstrDAO bankMstrDAO;
	
	@Autowired
	private BranchDAO branchDAO;

	@RequestMapping(value = "/submitChqReq", method = RequestMethod.POST)
	public @ResponseBody Object submitChqReq(@RequestBody ChequeService chequeService) {
		System.out.println("Enter into dissBankFromBranch---->");
		Map<String, Object> map = new HashMap<>();

		User currentUser = (User)httpSession.getAttribute("currentUser");
		
		ChequeRequest chequeRequest = chequeService.getChqReq();
		chequeRequest.setBranch(chequeService.getBranch());
		chequeRequest.setBankMstr(chequeService.getBankMstr());
		chequeRequest.setUserCode(currentUser.getUserCode());
		chequeRequest.setbCode(currentUser.getUserBranchCode());
		
		String str = chequeBookDAO.saveChequeRequest(chequeRequest);
		
		if (str != null && !str.isEmpty()) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("chqReqRefNo", str);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	
	@RequestMapping(value = "/getChqReqByBank", method = RequestMethod.POST)
	public @ResponseBody Object getChqReqByBank(@RequestBody BankMstr bankMstr) {
		System.out.println("Enter into dissBankFromBranch---->");
		Map<String, Object> map = new HashMap<>();

		List<ChequeRequest> chequeRequestList = chequeBookDAO.getChqReqList(bankMstr);
		List<ChequeRequest> chqReqList = new ArrayList<>();
		
		for (ChequeRequest chequeRequest : chequeRequestList) {
			System.out.println("getcReqRefNo==>"+chequeRequest.getcReqRefNo());
			System.out.println("Cancelled==>"+chequeRequest.iscReqIsCancelled());
			System.out.println("Received==>"+chequeRequest.iscReqIsReceived());
			if (!chequeRequest.iscReqIsCancelled() && !chequeRequest.iscReqIsReceived()) {
				chqReqList.add(chequeRequest);
			}
			
		}
		
		if (!chqReqList.isEmpty()) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("chqReqList", chqReqList);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	@RequestMapping(value = "/getAllbankMstr", method = RequestMethod.POST)
	public @ResponseBody Object getAllbankMstr() {
		System.out.println("Enter into getAllbankMstr---->");
		Map<String, Object> map = new HashMap<>();

		List<BankMstr> bankMstrList = bankMstrDAO.getBankMstr();
		
		if (!bankMstrList.isEmpty()) {
			map.put("bankMstrList", bankMstrList);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	@RequestMapping(value = "/submitChequeBook", method = RequestMethod.POST)
	public @ResponseBody Object submitChequeBook(@RequestBody ChequeService chequeService) {
		System.out.println("Enter into submitChequeBook---->");
		Map<String, Object> map = new HashMap<>();
		
		List<ChequeBook> chequeBookList = chequeService.getChequeBookList();
		ChequeRequest chequeRequest = chequeService.getChqReq();
		BankMstr bankMstr = chequeService.getBankMstr();
		
		for (ChequeBook chequeBook : chequeBookList) {
			System.out.println("FromChqNo: "+chequeBook.getChqBkFromChqNo());
			System.out.println("ToChqNo: "+chequeBook.getChqBkToChqNo());
			System.out.println("No of cheque: "+chequeBook.getChqBkNOChq());
		}
		
		System.out.println("ReqRefNo: "+chequeRequest.getcReqRefNo());
		System.out.println("ReqNOBook: "+chequeRequest.getcReqNOBook());
		System.out.println("BankMstr: "+bankMstr.getBnkFaCode());
		
		User user = (User)httpSession.getAttribute("currentUser");
		
		int temp = chequeBookDAO.receiveChequeBook(chequeBookList, chequeRequest, bankMstr, user);
		
		if (temp>0) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	@RequestMapping(value = "/getUnIssueChqBook", method = RequestMethod.POST)
	public @ResponseBody Object getUnIssueChqBook(@RequestBody BankMstr bankMstr) {
		System.out.println("Enter into getUnIssueChqBook---->");
		Map<String, Object> map = new HashMap<>();
		
		List<ChequeBook> chqBookList = new ArrayList<>();
		
		List<ChequeBook> chequeBookList = chequeBookDAO.getChqBook(bankMstr);
		
		System.out.println("cheque book list==>"+chequeBookList.size());
		
		for (ChequeBook chequeBook : chequeBookList) {
			System.out.println("chq book from: "+chequeBook.getChqBkFromChqNo());
			System.out.println("chq book ID: "+chequeBook.getChqBkId());
			if (!chequeBook.isChqBkIsIssued()) {
				chqBookList.add(chequeBook);
				System.out.println("IF");
			}else {
				System.out.println("ELSE");
			}
		}
		
		if (!chequeBookList.isEmpty()) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("unIssueChqBookList", chqBookList);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	@RequestMapping(value = "/issueChqBook", method = RequestMethod.POST)
	public @ResponseBody Object issueChqBook (@RequestBody ChequeService chequeService) {
		System.out.println("Enter into getUnIssueChqBook---->");
		Map<String, Object> map = new HashMap<>();
		
		List<ChequeBook> chequeBookList = chequeService.getChequeBookList();
		
		Employee authEmp1 = chequeService.getAuthEmp1();
		
		Employee authEmp2 = chequeService.getAuthEmp2();
		
		int temp = chequeBookDAO.issueChequeBook(chequeBookList, authEmp1, authEmp2);
		
		/*for (ChequeBook chequeBook : chequeBookList) {
			System.out.println("cheque Id: "+chequeBook.getChqBkId());
			System.out.println("from cheque No: "+chequeBook.getChqBkFromChqNo());
			System.out.println("to cheque No: "+chequeBook.getChqBkToChqNo());
		}*/
		
		if (temp>0) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);	
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	@RequestMapping(value = "/getUsrBrBMstrList", method = RequestMethod.POST)
	public @ResponseBody Object getUsrBrBMstrList(){
		
		Map<String,Object> map = new HashMap<String , Object>();
		User currentUser = (User)httpSession.getAttribute("currentUser");
		Branch branch = branchDAO.getBranchBMstrLt(Integer.parseInt(currentUser.getUserBranchCode()));
		List<BankMstr> bankMstrList = branch.getBankMstrList();
		
		if (!(bankMstrList.isEmpty())) {
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("bankMstrList",bankMstrList);
			map.put("branchName", branch.getBranchName());
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;	
	}
	
	@RequestMapping(value = "/getBrBankIssuedChqBkList", method = RequestMethod.POST)
	public @ResponseBody Object getBrBankIssuedChqBkList(@RequestBody BankMstr bankMstr){
		
		Map<String,Object> map = new HashMap<String , Object>();
		System.out.println("bankName: "+bankMstr.getBnkName());
		System.out.println("bankFaCode: "+bankMstr.getBnkFaCode());
		
		List<ChequeBook> chequeBookList = chequeBookDAO.getIssuedChqBook(bankMstr);
		
		if (!chequeBookList.isEmpty()) {
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("issuedChqBkList",chequeBookList);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;	
	}
	
	@RequestMapping(value = "/rcvdChqBkByBranch", method = RequestMethod.POST)
	public @ResponseBody Object rcvdChqBkByBranch(@RequestBody List<ChequeBook> chequeBookList){
		
		Map<String,Object> map = new HashMap<String , Object>();
		
		User currentUser = (User)httpSession.getAttribute("currentUser");
		
		//here cheque book is receiving and entering cheque leaves in chequeLeaves table
		int temp = chequeBookDAO.updateChqBookRecvdBr(chequeBookList, currentUser);
		/*for (ChequeBook chequeBook : chequeBookList) {
			System.out.println("chequeBook id: "+chequeBook.getChqBkId());
			System.out.print("from no: "+chequeBook.getChqBkFromChqNo()+"\t");
			System.out.println("to no: "+chequeBook.getChqBkToChqNo());
			System.out.println("received cheque book by branch: "+chequeBook.isChqBkIsRecByBr());
		}*/
		
		if (temp>0) {
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;	
	}
	
	@RequestMapping(value = "/unUsedChq", method = RequestMethod.POST)
	public @ResponseBody Object unUsedChq(@RequestBody BankMstr bankMstr){
		
		System.out.println("unUsedChq called");
		
		Map<String,Object> map = new HashMap<String , Object>();
		
		List<ChequeLeaves> chequeLeaveList = chequeLeavesDAO.getUnUsedChqLeave(bankMstr);
		
		if (!chequeLeaveList.isEmpty()) {
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("unUsedChqLeaveList", chequeLeaveList);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;	
	}
	
	@RequestMapping(value = "/cancelChqLeave", method = RequestMethod.POST)
	public @ResponseBody Object rcvdChqBkByBranch(@RequestBody ChequeLeaves chequeLeave){
		
		Map<String,Object> map = new HashMap<String , Object>();
		
		int temp = chequeLeavesDAO.cancelChqLeave(chequeLeave);
		
		if (temp>0) {
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;	
	}
	
	@RequestMapping(value = "/getBranchBankMstr", method = RequestMethod.POST)
	public @ResponseBody Object getBranchBankMstr(){
		
		Map<String,Object> map = new HashMap<String , Object>();
		
		User currentUser = (User)httpSession.getAttribute("currentUser");
		Branch branch = branchDAO.getBranchBMstrLt(Integer.parseInt(currentUser.getUserBranchCode()));
		List<BankMstr> bankMstrList = branch.getBankMstrList();
		
		if (!(bankMstrList.isEmpty())) {
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("bankMstrList",bankMstrList);
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;	
	}
	
	@RequestMapping(value = "/getChqForUpdate" , method = RequestMethod.POST)
	public @ResponseBody Object getChqForUpdate(@RequestBody Map<String, Object> chqStatusService){
	
		/*System.out.println("bankId: "+chqStatusService.get("bnkId"));
		System.out.println("branchId: "+chqStatusService.get("branchId"));
		System.out.println("chqCancel: "+chqStatusService.get("chqCancel"));
		System.out.println("chqUsed: "+chqStatusService.get("chqUsed"));
		System.out.println("chqAll: "+chqStatusService.get("chqAll"));*/
		
		Map<String, Object> map = new HashMap<>();
		
		List<ChequeLeaves> chequeLeaveList = chequeLeavesDAO.getChqLeavesForUpdate(chqStatusService);
		
		if ((boolean) chqStatusService.get("chqAll")) {
			//all cheque of the bank
			//do nothing
		}else if((boolean) chqStatusService.get("chqunUsed")){
			
		for (int i = 0; i < chequeLeaveList.size(); i++) {
			// condition to check cheque is used or not on behalf of value coming from database and html page is equal or not
			if((boolean) chqStatusService.get("chqunUsed")==chequeLeaveList.get(i).isChqLUsed() || chequeLeaveList.get(i).isChqLCancel())
			{
				// removing ChequeLeave from the table if condition is successful
				chequeLeaveList.remove(i);
				i=i-1;	
			}	
		}			
		} else {
			//all cheque according to chqCancelFlag and chqUsedFlag
			for (int i = 0; i < chequeLeaveList.size(); i++) {
			
			if ((boolean)chqStatusService.get("chqCancel") == chequeLeaveList.get(i).isChqLCancel()
					&& (boolean)chqStatusService.get("chqUsed") == chequeLeaveList.get(i).isChqLUsed()) {
			} else {
				chequeLeaveList.remove(i);
				i = i-1;
			}
		}
		System.out.println("chequeLeaveList size: "+chequeLeaveList.size());
		}
		
		System.out.println("chq list: "+chequeLeaveList.isEmpty());
		System.out.println("chq list: "+chequeLeaveList.size());
		
		if (!chequeLeaveList.isEmpty()) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("chequeLeaveList", chequeLeaveList);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	@RequestMapping(value = "/updateChqLeave" , method = RequestMethod.POST)
	public @ResponseBody Object updateChqLeave(@RequestBody ChequeLeaves chequeLeaves){
		
		Map<String, Object> map = new HashMap<>();
		
		System.out.println("entered into updateChqLeave");
		
		int temp = chequeLeavesDAO.updateChqMaxLtNType(chequeLeaves);
		
		if (temp>0) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	@RequestMapping(value = "/removeChqLeave" , method = RequestMethod.POST)
	public @ResponseBody Object removeChqLeave(@RequestBody String chqLId){
		
		Map<String, Object> map = new HashMap<>();
		
		System.out.println("entered into deleteChqLeave");
		
		int temp = chequeLeavesDAO.removeChqLeave(chqLId);
		
		if (temp>0) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	@RequestMapping(value="/getUnusedChqByBnk", method=RequestMethod.POST)
	public @ResponseBody Object getUnusedChqByBnk(@RequestBody Integer bankId){
		System.out.println("getUnusedChqByBnk()");
		Map<String, Object> map = new HashMap<>();
		
		List<String> chqNoList = chequeLeavesDAO.getChqNoListByBankId(bankId);
		
		if (!chqNoList.isEmpty()) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("chqNoList", chqNoList);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}
	
}
