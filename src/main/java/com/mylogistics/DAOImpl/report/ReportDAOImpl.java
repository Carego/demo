package com.mylogistics.DAOImpl.report;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.math.BigDecimal;
import java.net.Inet4Address;
import java.sql.Blob;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;





import javax.persistence.criteria.CriteriaBuilder.In;
import javax.servlet.http.HttpServletRequest;
import javax.sql.rowset.serial.SerialBlob;
import javax.swing.text.DefaultEditorKit.CutAction;

import org.apache.commons.lang.NumberUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.CreateKeySecondPass;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.mapping.Array;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.core.EmitUtils;
import org.springframework.core.io.FileSystemResource;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.SerializationUtils;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.exc.IgnoredPropertyException;
import com.itextpdf.text.log.SysoCounter;
import com.itextpdf.text.pdf.PdfStructTreeController.returnType;
import com.mylogistics.DAO.report.ReportDAO;
import com.mylogistics.constants.AddressCNTS;
import com.mylogistics.constants.ArrivalReportCNTS;
import com.mylogistics.constants.BillCNTS;
import com.mylogistics.constants.BillDetailCNTS;
import com.mylogistics.constants.BranchCNTS;
import com.mylogistics.constants.BranchStockLeafDetCNTS;
import com.mylogistics.constants.BrokerCNTS;
import com.mylogistics.constants.CashStmtCNTS;
import com.mylogistics.constants.CashStmtStatusCNTS;
import com.mylogistics.constants.CashStmtTempCNTS;
import com.mylogistics.constants.ChallanCNTS;
import com.mylogistics.constants.ChallanDetailCNTS;
import com.mylogistics.constants.CnmtCNTS;
import com.mylogistics.constants.Cnmt_ChallanCNTS;
import com.mylogistics.constants.ContPersonCNTS;
import com.mylogistics.constants.CustomerCNTS;
import com.mylogistics.constants.CustomerGroupCNTS;
import com.mylogistics.constants.EmailReportCNTS;
import com.mylogistics.constants.LhpvBalCNTS;
import com.mylogistics.constants.LhpvCashSmryCNTS;
import com.mylogistics.constants.LhpvStatusCNTS;
import com.mylogistics.constants.MemoCNTS;
import com.mylogistics.constants.MoneyReceiptCNTS;
import com.mylogistics.constants.OwnerCNTS;
import com.mylogistics.constants.StationCNTS;
import com.mylogistics.constants.TempIntBrTvCNTS;
import com.mylogistics.controller.MrPrintCntlr;
import com.mylogistics.model.Address;
import com.mylogistics.model.ArrivalReport;
import com.mylogistics.model.BankCS;
import com.mylogistics.model.Bill;
import com.mylogistics.model.BillDetail;
import com.mylogistics.model.Branch;
import com.mylogistics.model.BranchStockLeafDet;
import com.mylogistics.model.Broker;
import com.mylogistics.model.CashStmt;
import com.mylogistics.model.CashStmtStatus;
import com.mylogistics.model.CashStmtTemp;
import com.mylogistics.model.Challan;
import com.mylogistics.model.ChallanDetail;
import com.mylogistics.model.Cnmt;
import com.mylogistics.model.Cnmt_Challan;
import com.mylogistics.model.ContPerson;
import com.mylogistics.model.Customer;
import com.mylogistics.model.EMailReport;
import com.mylogistics.model.EMailReportList;
import com.mylogistics.model.FAMaster;
import com.mylogistics.model.Memo;
import com.mylogistics.model.MoneyReceipt;
import com.mylogistics.model.Owner;
import com.mylogistics.model.RelianceCNStatus;
import com.mylogistics.model.Station;
import com.mylogistics.model.TempIntBrTv;
import com.mylogistics.model.User;
import com.mylogistics.model.bank.BankMstr;
import com.mylogistics.model.dataintegration.CnmtView;
import com.mylogistics.model.lhpv.LhpvBal;
import com.mylogistics.model.lhpv.LhpvCashSmry;
import com.mylogistics.model.lhpv.LhpvStatus;
import com.mylogistics.services.CnDetailService;
import com.mylogistics.services.CodePatternService;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.EmailThread;
import com.mylogistics.services.EmailUsingPhpService;
import com.mylogistics.services.ReconRptService;

public class ReportDAOImpl implements ReportDAO{

	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	public static Logger logger = Logger.getLogger(ReportDAOImpl.class);

	@Autowired
	public ReportDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Autowired
	private HttpServletRequest httpServletRequest;
	
	@Override
	@SuppressWarnings("unchecked")
	public List<String> getMisCnmt(String branchId, String fromCnmtNo,
			String toCnmtNo, String status) {
		List<String> misCnmtList = new ArrayList<>();
		List<BranchStockLeafDet> branchStockLeafDetList = new ArrayList<>();
		Session session=null;
		try{
			session = this.sessionFactory.openSession();
			
			Criteria cr = session.createCriteria(Cnmt.class);
			cr.add(Restrictions.eq(CnmtCNTS.BRANCH_CODE,branchId));
			ProjectionList projList = Projections.projectionList();
			projList.add(Projections.property(CnmtCNTS.CNMT_CODE));
			cr.setProjection(projList);
			List<String> usedCnmtList = cr.list();
			
			if (!usedCnmtList.isEmpty()) {
				Collections.sort(usedCnmtList);
				Collections.reverse(usedCnmtList);
				System.out.println("last cnmt: "+usedCnmtList.get(0));
				cr = session.createCriteria(BranchStockLeafDet.class);   
				cr.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_BRCODE,branchId));
				cr.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_STATUS,status));
				/*cr.add(Restrictions.ge(BranchStockLeafDetCNTS.BRSLD_SNO,fromCnmtNo)); 
				cr.add(Restrictions.le(BranchStockLeafDetCNTS.BRSLD_SNO,toCnmtNo));*/
				cr.add(Restrictions.le(BranchStockLeafDetCNTS.BRSLD_SNO,usedCnmtList.get(0)));
				branchStockLeafDetList =cr.list();
				for (BranchStockLeafDet branchStockLeafDet : branchStockLeafDetList) {
					misCnmtList.add(branchStockLeafDet.getBrsLeafDetSNo());
					System.out.print("branchId: "+branchStockLeafDet.getBrsLeafDetBrCode()+"\t");
					System.out.print("cnmtNo: "+branchStockLeafDet.getBrsLeafDetSNo()+"\t");
					System.out.print("cnmt Status: "+branchStockLeafDet.getBrsLeafDetStatus()+"\n");
				}
				Collections.sort(misCnmtList);
			}
						
			
			/*for (int i = 0; i < misCnmtList.size(); i++) {
				System.out.println("sorted list: "+misCnmtList.get(i));
			}*/
			session.flush();
		}catch (Exception e) {
			e.printStackTrace();
			logger.info("Excption in getMisCnmt"+e);
		}finally{
			session.clear();
			session.close();
		}
		return misCnmtList;
	}
	
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	@Override
	public List<String> getMisChln(String branchId, String status) {
		List<String> mischlnList =new ArrayList<String>();
	          List<BranchStockLeafDet> branchStockLeafDets=new ArrayList<BranchStockLeafDet>();
	         session = this.sessionFactory.openSession();
	         Criteria criteria= session.createCriteria(Challan.class);
	         criteria.add(Restrictions.eq(ChallanCNTS.BRANCH_CODE, branchId));
	     ProjectionList pl=Projections.projectionList();
	     pl.add(Projections.property(ChallanCNTS.CHALLAN_CODE));
	     criteria.setProjection(pl);
	   
	List<String> usedChlnList=criteria.list();
	   if(!usedChlnList.isEmpty())
	   {
		 Collections.sort(usedChlnList);
		 Collections.reverse(usedChlnList);
		 criteria=session.createCriteria(BranchStockLeafDet.class);
		 criteria.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_BRCODE, branchId));
		 criteria.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_STATUS, status));
		 criteria.add(Restrictions.le(BranchStockLeafDetCNTS.BRSLD_SNO, usedChlnList.get(0)));
		 branchStockLeafDets=criteria.list();
		 for (BranchStockLeafDet branchStockLeafDet : branchStockLeafDets) {
			mischlnList.add(branchStockLeafDet.getBrsLeafDetSNo());
			System.out.println(branchStockLeafDet.getBrsLeafDetSNo());
		}
	   }
		Collections.sort(mischlnList);
		session.flush();
		session.close();
		logger.info("in getMisChln");
		return mischlnList;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	@Override
	public List<String> getMisSedr(String branchId, String status) {
		List<String> missedrList =new ArrayList<String>();
	          List<BranchStockLeafDet> branchStockLeafDets=new ArrayList<BranchStockLeafDet>();
	         Session session = this.sessionFactory.openSession();
	         Criteria criteria= session.createCriteria(ArrivalReport.class);
	         criteria.add(Restrictions.eq(ArrivalReportCNTS.BRANCH_CODE, branchId));
	         criteria.add(Restrictions.eq(ArrivalReportCNTS.AR_REP_TYPE, "Manual"));
	         ProjectionList pl=Projections.projectionList();
	     pl.add(Projections.property(ArrivalReportCNTS.AR_CHLN_CODE));
	     criteria.setProjection(pl);
	   
	List<String> usedsedrList=criteria.list();
	   if(!usedsedrList.isEmpty())
	   {
		 Collections.sort(usedsedrList);
		 Collections.reverse(usedsedrList);
		 criteria=session.createCriteria(BranchStockLeafDet.class);
		 criteria.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_BRCODE, branchId));
		 criteria.add(Restrictions.eq(BranchStockLeafDetCNTS.BRSLD_STATUS, status));
		 criteria.add(Restrictions.le(BranchStockLeafDetCNTS.BRSLD_SNO, usedsedrList.get(0)));
		 branchStockLeafDets=criteria.list();
		 for (BranchStockLeafDet branchStockLeafDet : branchStockLeafDets) {
			missedrList.add(branchStockLeafDet.getBrsLeafDetSNo());
			System.out.println(branchStockLeafDet.getBrsLeafDetSNo());
		}
	   }
		Collections.sort(missedrList);
		session.flush();
		session.close();
		logger.info("in getMisSedr");
		return missedrList;
	}
	
	

	@Override
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getBrLedger(Map<String, Object> ledgerReportMap) {
		
		List<Map<String, Object>> ledgerMapList = new ArrayList<>();
		Map<String, String> faCodeNameMap = new HashMap<>();
		//Session session=null;
		try {
			session = this.sessionFactory.openSession();
			
			Criteria cr = session.createCriteria(FAMaster.class);
			List<FAMaster> faMasterList = cr.list();
			
			for (FAMaster faMaster : faMasterList) {
				faCodeNameMap.put(faMaster.getFaMfaCode(), faMaster.getFaMfaName());
			}
			
			/*cr = session.createCriteria(CashStmt.class);
			cr.addOrder(Order.asc(CashStmtCNTS.CS_DT));
			cr.add(Restrictions.eq(CashStmtCNTS.USER_BRANCH_CODE, String.valueOf(ledgerReportMap.get("branchId"))));
			cr.add(Restrictions.ge(CashStmtCNTS.CS_DT, CodePatternService.getSqlDate(String.valueOf(ledgerReportMap.get("fromDt")))));
			cr.add(Restrictions.le(CashStmtCNTS.CS_DT, CodePatternService.getSqlDate(String.valueOf(ledgerReportMap.get("toDt")))));
			cr.add(Restrictions.ge(CashStmtCNTS.CS_FA_CODE, String.valueOf(ledgerReportMap.get("fromFaCode"))));
			cr.add(Restrictions.le(CashStmtCNTS.CS_FA_CODE, String.valueOf(ledgerReportMap.get("toFaCode"))));
			List<CashStmt> cashStmtList = cr.list();*/
			
			cr = session.createCriteria(CashStmtStatus.class);
			cr.addOrder(Order.asc(CashStmtStatusCNTS.CSS_DT));
			cr.add(Restrictions.eq(CashStmtStatusCNTS.B_Code, String.valueOf(ledgerReportMap.get("branchId"))));
			cr.add(Restrictions.ge(CashStmtStatusCNTS.CSS_DT, CodePatternService.getSqlDate(String.valueOf(ledgerReportMap.get("fromDt")))));
			cr.add(Restrictions.le(CashStmtStatusCNTS.CSS_DT, CodePatternService.getSqlDate(String.valueOf(ledgerReportMap.get("toDt")))));
			
			List<CashStmtStatus> cashStmtStatusList = cr.list();
			
			List<CashStmt> cashStmtList = new ArrayList<>();
			
			if (!cashStmtStatusList.isEmpty()) {
				for (CashStmtStatus cashStmtStatus : cashStmtStatusList) {
					List<CashStmt> csList = cashStmtStatus.getCashStmtList();
					for (CashStmt cashStmt : csList) {
						if (Integer.parseInt(cashStmt.getCsFaCode()) >= Integer.parseInt(String.valueOf(ledgerReportMap.get("fromFaCode")))
								&& Integer.parseInt(cashStmt.getCsFaCode()) <= Integer.parseInt(String.valueOf(ledgerReportMap.get("toFaCode")))) {
							cashStmtList.add(cashStmt);
						}
					}
				}
			}
			
			//ignore bank type rows
			/*if (!cashStmtList.isEmpty()) {
				for(int i=0;i<cashStmtList.size();i++){
					String sub = cashStmtList.get(i).getCsFaCode().substring(0,2);
					if((cashStmtList.get(i).getCsFaCode().equalsIgnoreCase("1110131")) ||
							(sub.equalsIgnoreCase("07") &&	cashStmtList.get(i).getCsVouchType().equalsIgnoreCase("BANK") &&
									(cashStmtList.get(i).getCsType().equalsIgnoreCase("CashWithdraw(ATM)") ||
											cashStmtList.get(i).getCsType().equalsIgnoreCase("CashWithdraw(CHQ)") ||
											cashStmtList.get(i).getCsType().equalsIgnoreCase("CashDeposite") ||
											cashStmtList.get(i).getCsType().equalsIgnoreCase("CashWithdraw")))){
						cashStmtList.remove(i);
						i=i-1;
					}
				}
			}*/
			
			if (!cashStmtList.isEmpty()) {
				for(int i=0;i<cashStmtList.size();i++){
					String sub = cashStmtList.get(i).getCsFaCode().substring(0,2);
					if(cashStmtList.get(i).getCsFaCode().equalsIgnoreCase("1110131")){
						cashStmtList.remove(i);
						i=i-1;
					}
				}
			}
			
			
			/*for(int i=0;i<cashStmtList.size();i++){
				String sub = cashStmtList.get(i).getCsFaCode().substring(0,2);
				if((sub.equalsIgnoreCase("07") && cashStmtList.get(i).getCsVouchType().equalsIgnoreCase("BANK"))){
					cashStmtList.remove(i);
					i=i-1;
				}
			}*/
			
			System.out.println("listSize:==========>> "+cashStmtList.size());
			for (CashStmt cashStmt : cashStmtList) {
				Map<String, Object> map = new HashMap<>();
				map.put(CashStmtCNTS.CS_FA_CODE, cashStmt.getCsFaCode());
				map.put("csFaName", faCodeNameMap.get(cashStmt.getCsFaCode()));
				map.put(CashStmtCNTS.CS_DT, cashStmt.getCsDt());
				map.put(CashStmtCNTS.CS_VOUCH_NO, cashStmt.getCsVouchNo());				
				map.put(CashStmtCNTS.CS_DESCRIPTION, cashStmt.getCsDescription());				
				map.put(CashStmtCNTS.CS_DR_CR, cashStmt.getCsDrCr());				
				System.out.println("csAmt before: "+cashStmt.getCsAmt());
				map.put(CashStmtCNTS.CS_AMT, Math.round(cashStmt.getCsAmt()*100.0)/100.0);
				System.out.println("csAmt after: "+Math.round(cashStmt.getCsAmt()*100.0)/100.0);			
				map.put(CashStmtCNTS.CS_VOUCH_TYPE, cashStmt.getCsVouchType());
				map.put(CashStmtCNTS.CS_TYPE, cashStmt.getCsType());				
				map.put(CashStmtCNTS.CS_TV_NO, cashStmt.getCsTvNo());
				map.put(CashStmtCNTS.PAY_MODE, cashStmt.getPayMode());				
				
				map.put("branchId", String.valueOf(ledgerReportMap.get("branchId")));
				map.put("branchName", String.valueOf(ledgerReportMap.get("branchName")));	
				
				ledgerMapList.add(map);
			}
		session.flush();	
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Exception in branchLedger "+e);
		}finally{
			session.clear();
			session.close();
		}
		
		return ledgerMapList;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getConsolidateLedger(Map<String, Object> ledgerReportMap) {
		
		List<Map<String, Object>> ledgerMapList = new ArrayList<>();
		Map<String, String> faCodeNameMap = new HashMap<>();
		Map<Integer, String> brIdNameMap = new HashMap<>();
		//Session session=null;
		try {
			session = this.sessionFactory.openSession();
			
			Criteria cr = session.createCriteria(FAMaster.class);
			List<FAMaster> faMasterList = cr.list();
			
			for (FAMaster faMaster : faMasterList) {
				faCodeNameMap.put(faMaster.getFaMfaCode(), faMaster.getFaMfaName());
			}
			
			cr = session.createCriteria(Branch.class);
			List<Branch> branchList = cr.list();
			
			for (Branch branch : branchList) {
				brIdNameMap.put(branch.getBranchId(), branch.getBranchName());
			}
			
			/*cr = session.createCriteria(CashStmt.class);
			cr.addOrder(Order.asc(CashStmtCNTS.CS_DT));
			cr.add(Restrictions.ge(CashStmtCNTS.CS_DT, CodePatternService.getSqlDate(String.valueOf(ledgerReportMap.get("fromDt")))));
			cr.add(Restrictions.le(CashStmtCNTS.CS_DT, CodePatternService.getSqlDate(String.valueOf(ledgerReportMap.get("toDt")))));
			cr.add(Restrictions.ge(CashStmtCNTS.CS_FA_CODE, ledgerReportMap.get("fromFaCode")));
			cr.add(Restrictions.le(CashStmtCNTS.CS_FA_CODE, ledgerReportMap.get("toFaCode")));
			
			List<CashStmt> cashStmtList = cr.list();*/		
			
			cr = session.createCriteria(CashStmtStatus.class);
			cr.addOrder(Order.asc(CashStmtStatusCNTS.CSS_DT));
			cr.add(Restrictions.ge(CashStmtStatusCNTS.CSS_DT, CodePatternService.getSqlDate(String.valueOf(ledgerReportMap.get("fromDt")))));
			cr.add(Restrictions.le(CashStmtStatusCNTS.CSS_DT, CodePatternService.getSqlDate(String.valueOf(ledgerReportMap.get("toDt")))));
			
			List<CashStmtStatus> cashStmtStatusList = cr.list();
			
			System.out.println("CashStmtStatus List Size : "+cashStmtStatusList.size());
			
			List<CashStmt> cashStmtList = new ArrayList<>();
			
			System.out.println("===>>fromDt: "+CodePatternService.getSqlDate(String.valueOf(ledgerReportMap.get("fromDt"))));
			System.out.println("===>>toDt: "+CodePatternService.getSqlDate(String.valueOf(ledgerReportMap.get("toDt"))));
			
			if (!cashStmtStatusList.isEmpty()) {
				for (CashStmtStatus cashStmtStatus : cashStmtStatusList) {
					List<CashStmt> csList = new ArrayList<CashStmt>();
					//System.err.println("id of cashStmtStatus = "+cashStmtStatus.getCssId());
					
					csList = cashStmtStatus.getCashStmtList();
					/*List<CashStmt> csList = cashStmtStatus.getCashStmtList();
					for (CashStmt cashStmt : csList) {
						if (Integer.parseInt(cashStmt.getCsFaCode()) >= Integer.parseInt(String.valueOf(ledgerReportMap.get("fromFaCode")))
								&& Integer.parseInt(cashStmt.getCsFaCode()) <= Integer.parseInt(String.valueOf(ledgerReportMap.get("toFaCode")))) {
							cashStmtList.add(cashStmt);
						}
					}*/
					
					System.out.println("===>>csListSize(): "+csList.size());
					
					//TODO					
					if(!csList.isEmpty()){
						//System.out.println("size of csList = "+csList.size());
						for(int i=0;i<csList.size();i++){
							CashStmt cashStmt = csList.get(i);							
							String faCode = cashStmt.getCsFaCode();
							if(faCode != null){
								if(! faCode.equalsIgnoreCase("")){
									if (Integer.parseInt(cashStmt.getCsFaCode()) >= Integer.parseInt(String.valueOf(ledgerReportMap.get("fromFaCode")))
											&& Integer.parseInt(cashStmt.getCsFaCode()) <= Integer.parseInt(String.valueOf(ledgerReportMap.get("toFaCode")))) {
										cashStmtList.add(cashStmt);
									}
								}
							}							
						}
					}
				}
			}
			
		
			//ignore bank type rows
			/*if (!cashStmtList.isEmpty()) {
				for(int i=0;i<cashStmtList.size();i++){
					String sub = cashStmtList.get(i).getCsFaCode().substring(0,2);
					if((cashStmtList.get(i).getCsFaCode().equalsIgnoreCase("1110131")) ||
							(sub.equalsIgnoreCase("07") &&	cashStmtList.get(i).getCsVouchType().equalsIgnoreCase("BANK") &&
									(cashStmtList.get(i).getCsType().equalsIgnoreCase("CashWithdraw(ATM)") ||
											cashStmtList.get(i).getCsType().equalsIgnoreCase("CashWithdraw(CHQ)") ||
											cashStmtList.get(i).getCsType().equalsIgnoreCase("CashDeposite") ||
											cashStmtList.get(i).getCsType().equalsIgnoreCase("CashWithdraw")))){
						cashStmtList.remove(i);
						i=i-1;
					}
				}
			}*/
			
			System.out.println("==========>>cashStmtList.size 1: "+cashStmtList.size());
			
			if (!cashStmtList.isEmpty()) {
				for(int i=0;i<cashStmtList.size();i++){
					String sub = cashStmtList.get(i).getCsFaCode().substring(0,2);
					if(cashStmtList.get(i).getCsFaCode().equalsIgnoreCase("1110131")){
						cashStmtList.remove(i);
						i=i-1;
					}
				}
			}
			
			/*for(int i=0;i<cashStmtList.size();i++){
				String sub = cashStmtList.get(i).getCsFaCode().substring(0,2);
				if((sub.equalsIgnoreCase("07") && cashStmtList.get(i).getCsVouchType().equalsIgnoreCase("BANK"))){
					cashStmtList.remove(i);
					i=i-1;
				}
			}*/
			
			System.out.println("==========>>cashStmtList.size 2: "+cashStmtList.size());			
			for (CashStmt cashStmt : cashStmtList) {				
				Map<String, Object> map = new HashMap<>();
				map.put(CashStmtCNTS.CS_FA_CODE, cashStmt.getCsFaCode());
				map.put("csFaName", faCodeNameMap.get(cashStmt.getCsFaCode()));
				map.put(CashStmtCNTS.CS_DT, cashStmt.getCsDt());
				map.put(CashStmtCNTS.CS_VOUCH_NO, cashStmt.getCsVouchNo());
				map.put(CashStmtCNTS.CS_VOUCH_TYPE, cashStmt.getCsVouchType());
				map.put(CashStmtCNTS.CS_TYPE, cashStmt.getCsType());
				map.put(CashStmtCNTS.CS_DESCRIPTION, cashStmt.getCsDescription());	
				map.put(CashStmtCNTS.CS_DR_CR, cashStmt.getCsDrCr());		
				map.put(CashStmtCNTS.CS_AMT, Math.round(cashStmt.getCsAmt()*100.0)/100.0);
				map.put(CashStmtCNTS.CS_TV_NO, cashStmt.getCsTvNo());
				map.put(CashStmtCNTS.PAY_MODE, cashStmt.getPayMode());
				
				map.put("branchId", cashStmt.getbCode());
				map.put("branchName", brIdNameMap.get(cashStmt.getbCode()));		
				
				ledgerMapList.add(map);
			}
		session.flush();	
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Exception in consolidate ledger "+e);
		}finally{
			session.clear();
			session.close();				
		}
		return ledgerMapList;
		
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<String> getFinYears() {

		List<String> finYears = new ArrayList<>();
		Session session=null;
		try {
			session = this.sessionFactory.openSession();
			Query queryMaxDt = session.createQuery("Select max(csDt) from CashStmt");
			Query queryMinDt = session.createQuery("Select min(csDt) from CashStmt");
			
			Date maxDate = (Date) queryMaxDt.list().get(0);
			Date minDate = (Date) queryMinDt.list().get(0);
			
			System.out.println("Max Date: "+maxDate);
			System.out.println("Mix Date: "+minDate);
			
			Calendar cal = new GregorianCalendar();
			cal.setTime(maxDate);
			int maxDateYr = cal.get(Calendar.YEAR);
			cal.setTime(minDate);			
			int minDateYr = cal.get(Calendar.YEAR);
			
			System.out.println("maxYear: "+maxDateYr);
			System.out.println("minYear: "+minDateYr);
			
			for (int i = minDateYr; i <= maxDateYr; i++) {
				finYears.add(String.valueOf(i)+"-"+String.valueOf(i+1));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Exception in getfinancial year"+e);
		}finally{
			session.clear();
			session.close();
		}
		
		return finYears;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Map<String, Object> getBrTrial(Map<String, Object> trialReportMap) {
		
		Map<String, Object> trialMapNCloseBalMap = new HashMap<>();
		
		List<Map<String, Object>> trialMapList = new ArrayList<>();
		Map<String, String> faCodeNameMap = new HashMap<>();
		
		String str = String.valueOf(trialReportMap.get("finYr"));
		String[] parts = str.split("-");
		int startFinYr = Integer.parseInt(parts[0]);
		int endFinYr = Integer.parseInt(parts[1]);
		
		Calendar cal = new GregorianCalendar();
		cal.set(startFinYr, 4, 1);
		java.util.Date utilDate = cal.getTime();
		Date startFinYrDt = new Date(utilDate.getTime());
		
		try {
			session = this.sessionFactory.openSession();
			
			Criteria cr = session.createCriteria(FAMaster.class);
			List<FAMaster> faMasterList = cr.list();
			
			for (FAMaster faMaster : faMasterList) {
				faCodeNameMap.put(faMaster.getFaMfaCode(), faMaster.getFaMfaName());
			}
			
			/*cr = session.createCriteria(CashStmt.class);
			cr.addOrder(Order.asc(CashStmtCNTS.CS_DT));
			cr.add(Restrictions.eq(CashStmtCNTS.USER_BRANCH_CODE, String.valueOf(trialReportMap.get("branchId"))));
			cr.add(Restrictions.ge(CashStmtCNTS.CS_DT, startFinYrDt));
			cr.add(Restrictions.le(CashStmtCNTS.CS_DT, CodePatternService.getSqlDate(String.valueOf(trialReportMap.get("uptoDt")))));
			
			List<CashStmt> cashStmtList = cr.list();*/
			
			cr = session.createCriteria(CashStmtStatus.class);
			cr.addOrder(Order.asc(CashStmtStatusCNTS.CSS_DT));
			cr.add(Restrictions.eq(CashStmtStatusCNTS.B_Code, String.valueOf(trialReportMap.get("branchId"))));
			cr.add(Restrictions.ge(CashStmtStatusCNTS.CSS_DT, startFinYrDt));
			cr.add(Restrictions.le(CashStmtStatusCNTS.CSS_DT, CodePatternService.getSqlDate(String.valueOf(trialReportMap.get("uptoDt")))));
			
			List<CashStmtStatus> cashStmtStatusList = cr.list();
			
			List<CashStmt> cashStmtList = new ArrayList<>();
			
			if (!cashStmtStatusList.isEmpty()) {
				for (CashStmtStatus cashStmtStatus : cashStmtStatusList) {
					List<CashStmt> csList = cashStmtStatus.getCashStmtList();
					cashStmtList.addAll(csList);
					System.out.println("csListSize: "+cashStmtList.size());
				}
			}
			
			session.flush();
			session.clear();
			session.close();
			
			//ignore bank type rows
			/*if (!cashStmtList.isEmpty()) {
				for(int i=0;i<cashStmtList.size();i++){
					String sub = cashStmtList.get(i).getCsFaCode().substring(0,2);
					if((cashStmtList.get(i).getCsFaCode().equalsIgnoreCase("1110131")) ||
							(sub.equalsIgnoreCase("07") &&	cashStmtList.get(i).getCsVouchType().equalsIgnoreCase("BANK") &&
									(cashStmtList.get(i).getCsType().equalsIgnoreCase("CashWithdraw(ATM)") ||
											cashStmtList.get(i).getCsType().equalsIgnoreCase("CashWithdraw(CHQ)") ||
											cashStmtList.get(i).getCsType().equalsIgnoreCase("CashDeposite") ||
											cashStmtList.get(i).getCsType().equalsIgnoreCase("CashWithdraw")))){
						cashStmtList.remove(i);
						i=i-1;
					}
				}
			}*/
			
			//add sundry debtor code if not exist and remove 1110131
			boolean isSundryDebtorExist = false;
			if (!cashStmtList.isEmpty()) {
				for(int i=0;i<cashStmtList.size();i++){
					String sub = cashStmtList.get(i).getCsFaCode().substring(0,2);
					if(cashStmtList.get(i).getCsFaCode().equalsIgnoreCase("1110131")){
						cashStmtList.remove(i);
						i=i-1;
					}
					if(i>=0)//by kamal
					if (cashStmtList.get(i).getCsFaCode().equalsIgnoreCase("1110057")) {
						isSundryDebtorExist = true;
					}
				}
			}
			
			if (!isSundryDebtorExist) {
				//enter cash stmt temp object
				System.out.println("add 1110057");
				CashStmt cs = new CashStmt();
				cs.setCsAmt(0.0);
				cs.setCsChequeType('Z');
				cs.setCsDescription("sundry debtors");
				cs.setCsDrCr('D');
				cs.setCsDt(CodePatternService.getSqlDate("2015-04-01"));
				cs.setCsFaCode("1110057");
				cs.setCsIsClose(true);
				
				cashStmtList.add(cs);
			}
			
			System.out.println("listSize:==========>> "+cashStmtList.size());
			for (CashStmt cashStmt : cashStmtList) {
				Map<String, Object> map = new HashMap<>();
				map.put(CashStmtCNTS.CS_FA_CODE, cashStmt.getCsFaCode());
				map.put("csFaName", faCodeNameMap.get(cashStmt.getCsFaCode()));
				map.put(CashStmtCNTS.CS_DT, cashStmt.getCsDt());
				map.put(CashStmtCNTS.CS_VOUCH_NO, cashStmt.getCsVouchNo());
				map.put(CashStmtCNTS.CS_VOUCH_TYPE, cashStmt.getCsVouchType());
				map.put(CashStmtCNTS.CS_DESCRIPTION, cashStmt.getCsDescription());
				map.put(CashStmtCNTS.CS_TV_NO, cashStmt.getCsTvNo());
				map.put(CashStmtCNTS.CS_DR_CR, cashStmt.getCsDrCr());
				
				System.out.println("csAmt before: "+cashStmt.getCsAmt());
				map.put(CashStmtCNTS.CS_AMT, Math.round(cashStmt.getCsAmt()*100.0)/100.0);
				System.out.println("csAmt after: "+Math.round(cashStmt.getCsAmt()*100.0)/100.0);
				
				map.put("branchId", String.valueOf(trialReportMap.get("branchId")));
				map.put("branchName", String.valueOf(trialReportMap.get("branchName")));
				
				trialMapList.add(map);
			}
			
			/*session = this.sessionFactory.openSession();
			
			cr = session.createCriteria(CashStmtStatus.class);
			cr.addOrder(Order.asc(CashStmtStatusCNTS.CSS_DT));
			cr.add(Restrictions.ge(CashStmtStatusCNTS.CSS_DT, startFinYrDt));
			cr.add(Restrictions.eq(CashStmtStatusCNTS.B_Code, String.valueOf(trialReportMap.get("branchId"))));
			cr.add(Restrictions.le(CashStmtStatusCNTS.CSS_DT, CodePatternService.getSqlDate(String.valueOf(trialReportMap.get("uptoDt")))));
			
			List<CashStmtStatus> cashStmtStatusList = cr.list();
			
			session.flush();
			session.clear();
			session.close();*/
			
			List<Integer> branchIdList = new ArrayList<>();
			
			for (CashStmtStatus cashStmtStatus : cashStmtStatusList) {
				branchIdList.add(Integer.parseInt(cashStmtStatus.getbCode()));
			}
			
			//remove dupliacte from list
			Set<Integer> hs = new HashSet<>();
			hs.addAll(branchIdList);
			branchIdList.clear();
			branchIdList.addAll(hs);
			Collections.sort(branchIdList);
			
			double cashInClosing = 0.0;
			
			for (int i = 0; i < branchIdList.size(); i++) {
				System.out.println("Branch ID: =====>>"+branchIdList.get(i));
				
				List<CashStmtStatus> cashStmtStatusListTemp = new ArrayList<>();
				for (CashStmtStatus cashStmtStatus : cashStmtStatusList) {
					if (Integer.parseInt(cashStmtStatus.getbCode()) == branchIdList.get(i)) {
						cashStmtStatusListTemp.add(cashStmtStatus);
					}
				}
				
				for (CashStmtStatus cashStmtStatus : cashStmtStatusListTemp) {
					System.out.print(cashStmtStatus.getbCode()+"\t");
					System.out.println(cashStmtStatus.getCssDt());
				}
				
				//BigDecimal.ZERO.compareTo(BigDecimal.valueOf(0.0)) == 0
				
				if (BigDecimal.ZERO.compareTo(BigDecimal.valueOf(cashStmtStatusListTemp.get(cashStmtStatusListTemp.size()-1).getCssCloseBal())) == 0) {
					cashInClosing = cashInClosing + cashStmtStatusListTemp.get(cashStmtStatusListTemp.size()-1).getCssOpenBal();
				} else {
					cashInClosing = cashInClosing + cashStmtStatusListTemp.get(cashStmtStatusListTemp.size()-1).getCssCloseBal();
				}
				//cashInClosing = cashInClosing + cashStmtStatusListTemp.get(cashStmtStatusListTemp.size()-1).getCssCloseBal();
			}
			
			System.out.println("Closing Balance: "+cashInClosing);
			
			trialMapNCloseBalMap.put("cashInClosing", cashInClosing);
			trialMapNCloseBalMap.put("trialMapList", trialMapList);
		
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Exception in branch trial"+e);
		}
		
		if (session.isOpen()) {
			session.clear();
			session.close();
		}
		
		return trialMapNCloseBalMap;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Map<String, Object> getConsolidateTrial(Map<String, Object> trialReportMap) {
		
		Map<String, Object> trialMapNCloseBalMap = new HashMap();
		
		List<Map<String, Object>> trialMapList = new ArrayList<>();
		Map<String, String> faCodeNameMap = new HashMap<>();
		
		String str = String.valueOf(trialReportMap.get("finYr"));
		String[] parts = str.split("-");
		int startFinYr = Integer.parseInt(parts[0]);
		int endFinYr = Integer.parseInt(parts[1]);
		
		Calendar cal = new GregorianCalendar();
		cal.set(startFinYr, 4, 1);
		java.util.Date utilDate = cal.getTime();
		Date startFinYrDt = new Date(utilDate.getTime());
		
		try {
			session = this.sessionFactory.openSession();
			
			Criteria cr = session.createCriteria(FAMaster.class);
			List<FAMaster> faMasterList = cr.list();
			
			for (FAMaster faMaster : faMasterList) {
				faCodeNameMap.put(faMaster.getFaMfaCode(), faMaster.getFaMfaName());
			}
			
			/*cr = session.createCriteria(CashStmt.class);
			cr.addOrder(Order.asc(CashStmtCNTS.CS_DT));
			cr.add(Restrictions.ge(CashStmtCNTS.CS_DT, startFinYrDt));
			cr.add(Restrictions.le(CashStmtCNTS.CS_DT, CodePatternService.getSqlDate(String.valueOf(trialReportMap.get("uptoDt")))));
			
			List<CashStmt> cashStmtList = cr.list();*/
			
			cr = session.createCriteria(CashStmtStatus.class);
			cr.addOrder(Order.asc(CashStmtStatusCNTS.CSS_DT));
			cr.add(Restrictions.ge(CashStmtStatusCNTS.CSS_DT, startFinYrDt));
			cr.add(Restrictions.le(CashStmtStatusCNTS.CSS_DT, CodePatternService.getSqlDate(String.valueOf(trialReportMap.get("uptoDt")))));
			
			List<CashStmtStatus> cashStmtStatusList = cr.list();
			
			List<CashStmt> cashStmtList = new ArrayList<>();			
			
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			
			
			
			if (!cashStmtStatusList.isEmpty()) {
				for (CashStmtStatus cashStmtStatus : cashStmtStatusList) {					
					
					System.out.println("CashStmtStatus ID : "+cashStmtStatus.getCssId());
					
					//List<CashStmt> csList = cashStmtStatus.getCashStmtList();				
					
					/*System.err.println("cashStmtList1: "+cashStmtList.size());
					System.err.println("cashStmtStatus2: "+cashStmtStatus.getCssId());
					System.err.println("csList: "+csList.size());
					System.err.println("cashStmtStatus.getCashStmtList: "+cashStmtStatus.getCashStmtList());*/					
					
					//cashStmtList.addAll(csList);
					
					
					String d = dateFormat.format(cashStmtStatus.getCssDt());					
					//List<Object[]> list = session.createSQLQuery("SELECT * FROM cashstmt WHERE cssId  = "+cashStmtStatus.getCssId()+" AND csDt ='"+d+"'")
                      //      .setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP)
                        //    .list();			
					
					List<Object[]> list = session.createSQLQuery("SELECT cashstmt.csId AS csId, cashstmt.bCode AS bCode, cashstmt.csAmt AS csAmt, cashstmt.csChequeType AS csChequeType, cashstmt.csDescription AS csDescription, cashstmt.csDrCr AS csDrCr, cashstmt.csDt AS csDt, cashstmt.csFaCode AS csFaCode, cashstmt.csIsClose AS csIsClose, cashstmt.csIsVRev AS csIsVRev, cashstmt.csMrNo AS csMrNo, cashstmt.csPayTo AS csPayTo, cashstmt.csSFId AS csSFId, cashstmt.csTvNo AS csTvNo, cashstmt.csType AS csType, cashstmt.csVouchNo AS csVouchNo, cashstmt.csVouchType AS csVouchType, cashstmt.userCode AS userCode, cashstmt.cssId AS cssId, cashstmt.csLhpvTempNo AS csLhpvTempNo, cashstmt.csUpdate AS csUpdate" +
							" FROM cashstmt INNER JOIN cashstmtstatus_cashstmt ON cashstmt.csId=cashstmtstatus_cashstmt.csId AND cashstmtstatus_cashstmt.cssId = "+cashStmtStatus.getCssId())
							.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP)
							.list();					
					List tempList = new ArrayList();	
					for(Object obj : list){
						Map row = (Map)obj;
						CashStmt cashStmt = new CashStmt();
						cashStmt.setCsId((int)row.get("csId"));
						cashStmt.setbCode(String.valueOf(row.get("bCode")));						
						cashStmt.setCsAmt(Double.parseDouble(String.valueOf(row.get("csAmt"))));						
						cashStmt.setCsChequeType((char)row.get("csChequeType"));
						cashStmt.setCsDescription(String.valueOf(row.get("csDescription")));
						cashStmt.setCsDrCr((char)row.get("csDrCr"));
						cashStmt.setCsDt(new Date(dateFormat.parse(String.valueOf(row.get("csDt"))).getTime()));
						cashStmt.setCsFaCode(String.valueOf(row.get("csFaCode")));
						cashStmt.setCsIsClose((boolean)row.get("csIsClose"));
						cashStmt.setCsIsVRev((boolean)row.get("csIsVRev"));
						cashStmt.setCsMrNo(String.valueOf(row.get("csMrNo")));
						cashStmt.setCsPayTo(String.valueOf(row.get("csPayTo")));
						cashStmt.setCsSFId((int)row.get("csSFId"));
						cashStmt.setCsTvNo(String.valueOf(row.get("csTvNo")));
						cashStmt.setCsType(String.valueOf(row.get("csType")));
						cashStmt.setCsVouchNo((int)row.get("csVouchNo"));
						cashStmt.setCsVouchType(String.valueOf(row.get("csVouchType")));
						cashStmt.setUserCode(String.valueOf(row.get("userCode")));												
						cashStmt.setCsNo(cashStmtStatus);
						cashStmt.setCsLhpvTempNo(String.valueOf(row.get("csLhpvTempNo")));
						cashStmt.setCsUpdate((boolean)row.get("csUpdate"));											
						tempList.add(cashStmt);
					}			
					System.out.println("List Size : "+tempList.size());
					if(list.size() > 0){
						cashStmtList.addAll(tempList);						
					}
					
					
				}
			}
			
			session.flush();
			session.clear();
			session.close();
			
			System.out.println("listSize: "+cashStmtList.size());
			
			//ignore bank type rows
			/*if (!cashStmtList.isEmpty()) {
				for(int i=0;i<cashStmtList.size();i++){
					String sub = cashStmtList.get(i).getCsFaCode().substring(0,2);
					if((cashStmtList.get(i).getCsFaCode().equalsIgnoreCase("1110131")) ||
							(sub.equalsIgnoreCase("07") &&	cashStmtList.get(i).getCsVouchType().equalsIgnoreCase("BANK") &&
									(cashStmtList.get(i).getCsType().equalsIgnoreCase("CashWithdraw(ATM)") ||
											cashStmtList.get(i).getCsType().equalsIgnoreCase("CashWithdraw(CHQ)") ||
											cashStmtList.get(i).getCsType().equalsIgnoreCase("CashDeposite") ||
											cashStmtList.get(i).getCsType().equalsIgnoreCase("CashWithdraw")))){
						cashStmtList.remove(i);
						i=i-1;
					}
				}
			}*/
			
			//add sundry debtor code if not exist and remove 1110131
			boolean isSundryDebtorExist = false;
			if (!cashStmtList.isEmpty()) {
				for(int i=0;i<cashStmtList.size();i++){
					String sub = cashStmtList.get(i).getCsFaCode().substring(0,2);
					if(cashStmtList.get(i).getCsFaCode().equalsIgnoreCase("1110131")){
						cashStmtList.remove(i);
						i=i-1;
					}
					
					if (cashStmtList.get(i).getCsFaCode().equalsIgnoreCase("1110057")) {
						isSundryDebtorExist = true;
					}
				}
			}
			
			if (!isSundryDebtorExist) {
				//enter cash stmt temp object
				System.out.println("add 1110057");
				CashStmt cs = new CashStmt();
				cs.setCsAmt(0.0);
				cs.setCsChequeType('Z');
				cs.setCsDescription("sundry debtors");
				cs.setCsDrCr('D');
				cs.setCsDt(CodePatternService.getSqlDate("2015-04-01"));
				cs.setCsFaCode("1110057");
				cs.setCsIsClose(true);
				
				cashStmtList.add(cs);
			}
			
			System.out.println("listSize:==========>> "+cashStmtList.size());
			
			for (CashStmt cashStmt : cashStmtList) {
				
				System.err.println("Cash Stmt ID : "+cashStmt.getCsId()+ " : FaCode = "+cashStmt.getCsFaCode());
				
				
				Map<String, Object> map = new HashMap<>();
				map.put(CashStmtCNTS.CS_FA_CODE, cashStmt.getCsFaCode());
				map.put("csFaName", faCodeNameMap.get(cashStmt.getCsFaCode()));
				map.put(CashStmtCNTS.CS_DT, cashStmt.getCsDt());
				map.put(CashStmtCNTS.CS_VOUCH_NO, cashStmt.getCsVouchNo());
				map.put(CashStmtCNTS.CS_VOUCH_TYPE, cashStmt.getCsVouchType());
				map.put(CashStmtCNTS.CS_DESCRIPTION, cashStmt.getCsDescription());
				map.put(CashStmtCNTS.CS_TV_NO, cashStmt.getCsTvNo());
				map.put(CashStmtCNTS.CS_DR_CR, cashStmt.getCsDrCr());
				
				//System.out.println("csAmt before: "+cashStmt.getCsAmt());
				map.put(CashStmtCNTS.CS_AMT, Math.round(cashStmt.getCsAmt()*100.0)/100.0);
				//System.out.println("csAmt after: "+Math.round(cashStmt.getCsAmt()*100.0)/100.0);
				
				map.put("branchId", String.valueOf(trialReportMap.get("branchId")));
				map.put("branchName", String.valueOf(trialReportMap.get("branchName")));
				
				trialMapList.add(map);
			}
			
			/*session = this.sessionFactory.openSession();
			
			cr = session.createCriteria(CashStmtStatus.class);
			cr.addOrder(Order.asc(CashStmtStatusCNTS.CSS_DT));
			cr.add(Restrictions.ge(CashStmtStatusCNTS.CSS_DT, startFinYrDt));
			cr.add(Restrictions.le(CashStmtStatusCNTS.CSS_DT, CodePatternService.getSqlDate(String.valueOf(trialReportMap.get("uptoDt")))));
			
			List<CashStmtStatus> cashStmtStatusList = cr.list();
			
			session.flush();
			session.clear();
			session.close();*/
			
			List<Integer> branchIdList = new ArrayList<>();
			
			for (CashStmtStatus cashStmtStatus : cashStmtStatusList) {
				branchIdList.add(Integer.parseInt(cashStmtStatus.getbCode()));
			}
			
			//remove dupliacte from list
			Set<Integer> hs = new HashSet<>();
			hs.addAll(branchIdList);
			branchIdList.clear();
			branchIdList.addAll(hs);
			Collections.sort(branchIdList);
			
			double cashInClosing = 0.0;
			
			for (int i = 0; i < branchIdList.size(); i++) {
				//System.out.println("Branch ID: =====>>"+branchIdList.get(i));
				
				List<CashStmtStatus> cashStmtStatusListTemp = new ArrayList<>();
				for (CashStmtStatus cashStmtStatus : cashStmtStatusList) {
					if (Integer.parseInt(cashStmtStatus.getbCode()) == branchIdList.get(i)) {
						cashStmtStatusListTemp.add(cashStmtStatus);
					}
				}
				
				/*for (CashStmtStatus cashStmtStatus : cashStmtStatusListTemp) {
					System.out.print(cashStmtStatus.getbCode()+"\t");
					System.out.println(cashStmtStatus.getCssDt());
				}*/
				
				//BigDecimal.ZERO.compareTo(BigDecimal.valueOf(0.0)) == 0
				
				if (BigDecimal.ZERO.compareTo(BigDecimal.valueOf(cashStmtStatusListTemp.get(cashStmtStatusListTemp.size()-1).getCssCloseBal())) == 0) {
					cashInClosing = cashInClosing + cashStmtStatusListTemp.get(cashStmtStatusListTemp.size()-1).getCssOpenBal();
				} else {
					cashInClosing = cashInClosing + cashStmtStatusListTemp.get(cashStmtStatusListTemp.size()-1).getCssCloseBal();
				}
				//cashInClosing = cashInClosing + cashStmtStatusListTemp.get(cashStmtStatusListTemp.size()-1).getCssCloseBal();
			}
			
			//System.out.println("Closing Balance: "+cashInClosing);
			
			trialMapNCloseBalMap.put("trialMapList", trialMapList);
			trialMapNCloseBalMap.put("cashInClosing", cashInClosing);
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Exception in Consolidate trial"+e);
		}
		
		if (session.isOpen()) {
			session.clear();
			session.close();
		}
		
		return trialMapNCloseBalMap;
	}
	
	
	
	@SuppressWarnings("unchecked")
	public List<CnDetailService> getCnDetByDt(Date frDt, Date toDt){
		System.out.println("enter into getCnDetByDt funciton");
		List<CnDetailService> cnDetList = new ArrayList<>();
		List<Cnmt> cnmtList = new ArrayList<>();
		Session session=null;
		try{
			session = this.sessionFactory.openSession();
			
			Criteria cr = session.createCriteria(Cnmt.class);
			cr.add(Restrictions.between(CnmtCNTS.CNMT_DT, frDt, toDt));
			cnmtList = cr.list();
			System.out.println("size of cnmtList = "+cnmtList.size());
			if(!cnmtList.isEmpty()){
				//main cnmt loop start
				for(int i=0;i<cnmtList.size();i++){
					
					CnDetailService cnDetailService = new CnDetailService();
					cnDetailService.setCnmtNo(cnmtList.get(i).getCnmtCode());
					cnDetailService.setCnmtDt(cnmtList.get(i).getCnmtDt());
					cnDetailService.setBillNo(cnmtList.get(i).getCnmtBillNo());
					
					
					if(cnmtList.get(i).getCnmtBillNo() != null){
						List<Bill> blList = new ArrayList<>();
						cr = session.createCriteria(Bill.class);
						cr.add(Restrictions.eq(BillCNTS.Bill_NO,cnmtList.get(i).getCnmtBillNo()));
						blList = cr.list();
						if(!blList.isEmpty()){
							cnDetailService.setBlAmt(blList.get(0).getBlFinalTot());
						}
					}
					
					System.out.println(i+" cnmt ===> "+cnmtList.get(i).getCnmtId());
					Station frStn = (Station) session.get(Station.class, Integer.parseInt(cnmtList.get(i).getCnmtFromSt()));
					Station toStn = (Station) session.get(Station.class, Integer.parseInt(cnmtList.get(i).getCnmtToSt()));
					
					if(frStn != null){
						cnDetailService.setFrStn(frStn.getStnName());
					}
					
					if(toStn != null){
						cnDetailService.setToStn(toStn.getStnName());
					}
					
					if(cnmtList.get(i).getCnmtConsignor() != null){
						Customer consignor = (Customer) session.get(Customer.class, Integer.parseInt(cnmtList.get(i).getCnmtConsignor()));
						cnDetailService.setConsignor(consignor.getCustName());
					}
					
					if(cnmtList.get(i).getCnmtConsignee() != null){
						Customer consignee = (Customer) session.get(Customer.class, Integer.parseInt(cnmtList.get(i).getCnmtConsignee()));
						cnDetailService.setConsignee(consignee.getCustName());
					}
					
					System.out.println("cnmtList.get(i).getCustCode() = "+cnmtList.get(i).getCustCode());
					if(cnmtList.get(i).getCustCode() == "" || cnmtList.get(i).getCustCode() == null){
						cnDetailService.setBlngParty("");
					}else{
						Customer customer = (Customer) session.get(Customer.class, Integer.parseInt(cnmtList.get(i).getCustCode()));
						cnDetailService.setBlngParty(customer.getCustName());
					}
					
					
					cnDetailService.setNoOfPckg(cnmtList.get(i).getCnmtNoOfPkg());
					
					if(cnmtList.get(i).getCnmtActualWt() > 0){
						cnDetailService.setActWt(cnmtList.get(i).getCnmtActualWt() / ConstantsValues.kgInTon);
					}else{
						cnDetailService.setActWt(0);
					}
					
					if(cnmtList.get(i).getCnmtGuaranteeWt() > 0){
						cnDetailService.setWtChrg(cnmtList.get(i).getCnmtGuaranteeWt() / ConstantsValues.kgInTon);
					}else{
						cnDetailService.setWtChrg(0);
					}
						
					if(cnmtList.get(i).getCnmtRate() >= 0){
						cnDetailService.setPrMtRt(cnmtList.get(i).getCnmtRate() * 1000);
					}else{
						cnDetailService.setPrMtRt(0);
					}
					
					cnDetailService.setInvList(cnmtList.get(i).getCnmtInvoiceNo());
					cnDetailService.setVog(cnmtList.get(i).getCnmtVOG());
					cnDetailService.setExpDtOfDly(cnmtList.get(i).getCnmtDtOfDly());
					cnDetailService.setCnmtFreight(cnmtList.get(i).getCnmtFreight());
					
					
					List<Cnmt_Challan> ccList  = new ArrayList<>();
					cr = session.createCriteria(Cnmt_Challan.class);
					cr.add(Restrictions.eq(Cnmt_ChallanCNTS.Cnmt_Chln_cnmtId, cnmtList.get(i).getCnmtId()));
					ccList = cr.list();
					
					if(!ccList.isEmpty()){
						if(ccList.size() == 1){
							Challan challan = new Challan();
							challan = (Challan) session.get(Challan.class,ccList.get(0).getChlnId());
							
							if(challan.getChlnTimeAllow() != null){
								cnDetailService.setTnsTime(Integer.parseInt(challan.getChlnTimeAllow()));
							}
							
							cnDetailService.setChlnNo(challan.getChlnCode());
							cnDetailService.setChlnDt(challan.getChlnDt());
							
							if(challan.getChlnLryRate() != null){
								cnDetailService.setPrMtCost(Double.parseDouble(challan.getChlnLryRate()) * 1000);
							}else{
								cnDetailService.setPrMtCost(0);
							}
							
							if(challan.getChlnAdvance() != null){
								cnDetailService.setAdv(Double.parseDouble(challan.getChlnAdvance()));
							}
							
	 						cnDetailService.setBal(challan.getChlnBalance());
	 						
	 						if(challan.getChlnFreight() != null){
	 							cnDetailService.setAnyOthChrg(Float.parseFloat(challan.getChlnFreight()) - new Double(challan.getChlnTotalFreight()).floatValue());
	 						}
	 						
	 						
	 						if(challan.getChlnFreight() != null){
	 							cnDetailService.setLryHire(Double.parseDouble(challan.getChlnFreight()));
	 						}else{
	 							cnDetailService.setLryHire(0);
	 						}
	 						
	 						cnDetailService.setTotHireChrg(challan.getChlnTotalFreight());
	 						
	 						List<ChallanDetail> chdList = new ArrayList<>();
	 						cr = session.createCriteria(ChallanDetail.class);
	 						cr.add(Restrictions.eq(ChallanDetailCNTS.CHD_CHALLAN_CODE,challan.getChlnCode()));
	 						chdList = cr.list();
	 						
	 						if(!chdList.isEmpty()){
	 							
	 							List<Broker> brkList = new ArrayList<>();
	 							cr = session.createCriteria(Broker.class);
	 	 						cr.add(Restrictions.eq(BrokerCNTS.BRK_CODE,chdList.get(0).getChdBrCode()));
	 	 						brkList = cr.list();
	 	 						
	 	 						if(!brkList.isEmpty()){
	 	 							Broker broker = brkList.get(0);
	 	 							cnDetailService.setBrkName(broker.getBrkName());
	 	 							List<String> mobList = new ArrayList<String>();
	 	 							if(chdList.get(0).getChdBrMobNo() != null){
	 	 								mobList.add(chdList.get(0).getChdBrMobNo());
	 	 							}else{
	 	 								mobList = broker.getBrkPhNoList();
	 	 								if(mobList == null || mobList.isEmpty()){
	 	 									List<ContPerson> contPList = new ArrayList<>();
	 	 									cr = session.createCriteria(ContPerson.class);
	 	 		 	 						cr.add(Restrictions.eq(ContPersonCNTS.CP_REF_CODE,broker.getBrkCode()));
	 	 		 	 					    contPList = cr.list();
	 	 		 	 					    if(!contPList.isEmpty()){
	 	 		 	 					    	mobList = new ArrayList<>();
	 	 		 	 					    	//mobList.add(contPList.get(0).getCpMobile().get(0));
	 	 		 	 					    	mobList = contPList.get(0).getCpMobile();
	 	 		 	 					    }else{
	 	 		 	 					    mobList = new ArrayList<>();
	 	 		 	 					    }
	 	 								}
	 	 							}
	 	 							cnDetailService.setBrkCntList(mobList);
	 	 						}
	 							
	 	 						
	 	 						List<Owner> ownList = new ArrayList<>();
	 	 						cr = session.createCriteria(Owner.class);
	 	 						cr.add(Restrictions.eq(OwnerCNTS.OWN_CODE,chdList.get(0).getChdOwnCode()));
	 	 						ownList = cr.list();
	 	 						
	 	 						if(!ownList.isEmpty()){
	 	 							Owner owner = ownList.get(0);
	 	 							cnDetailService.setOwnName(owner.getOwnName());
	 	 							List<String> mobList = new ArrayList<String>();
	 	 							
	 	 							if(chdList.get(0).getChdOwnMobNo() != null){
	 	 								mobList.add(chdList.get(0).getChdOwnMobNo());
	 	 							}else{
	 	 								mobList = owner.getOwnPhNoList();
	 	 								if(mobList == null || mobList.isEmpty()){
	 	 									List<ContPerson> contPList = new ArrayList<>();
	 	 									cr = session.createCriteria(ContPerson.class);
	 	 		 	 						cr.add(Restrictions.eq(ContPersonCNTS.CP_REF_CODE,owner.getOwnCode()));
	 	 		 	 					    contPList = cr.list();
	 	 		 	 					    if(!contPList.isEmpty()){
	 	 		 	 					    	mobList = new ArrayList<>();
	 	 		 	 					    	//mobList.add(contPList.get(0).getCpMobile().get(0));
	 	 		 	 					    	mobList = contPList.get(0).getCpMobile();
	 	 		 	 					    }else{
	 	 		 	 					    	mobList = new ArrayList<>(); 
	 	 		 	 					    }
	 	 								}
	 	 							}
	 	 							
	 	 							cnDetailService.setOwnCntList(mobList);
	 	 							cnDetailService.setOwnPan(owner.getOwnPanNo());
	 	 							
	 	 							List<Address> addList = new ArrayList<>();
	 	 							cr = session.createCriteria(Address.class);   
	 	 					        cr.add(Restrictions.eq(AddressCNTS.REF_CODE,owner.getOwnCode()));
	 	 					        addList = cr.list();
	 	 					         
	 	 					        if(!addList.isEmpty()){
	 	 					        	String othAdd = addList.get(0).getCompleteAdd() +", "+ addList.get(0).getAddCity()
	 	 					        			+ "(" + addList.get(0).getAddPin() + "), " + addList.get(0).getAddState();
	 	 					        	cnDetailService.setOwnAdd(othAdd);
	 	 					        }
	 	 						}
	 	 						
	 	 						cnDetailService.setDriverName(chdList.get(0).getChdDvrName());
	 	 						cnDetailService.setDrvCntNo(chdList.get(0).getChdDvrMobNo());
	 	 						
	 						}
	 						
	 						cnDetList.add(cnDetailService);
	 						
						}else{
							
							Challan challan = new Challan();
							challan = (Challan) session.get(Challan.class,ccList.get(0).getChlnId());
							
							if(challan.getChlnTimeAllow() != null){
								cnDetailService.setTnsTime(Integer.parseInt(challan.getChlnTimeAllow()));
							}
							
							cnDetailService.setChlnNo(challan.getChlnCode());
							cnDetailService.setChlnDt(challan.getChlnDt());
							
							if(challan.getChlnLryRate() != null){
								cnDetailService.setPrMtCost(Double.parseDouble(challan.getChlnLryRate()) * 1000);
							}else{
								cnDetailService.setPrMtCost(0);
							}
							
							if(challan.getChlnAdvance() != null){
								cnDetailService.setAdv(Double.parseDouble(challan.getChlnAdvance()));
							}
							
	 						cnDetailService.setBal(challan.getChlnBalance());
	 						
	 						if(challan.getChlnFreight() != null){
	 							cnDetailService.setAnyOthChrg(Float.parseFloat(challan.getChlnFreight()) - new Double(challan.getChlnTotalFreight()).floatValue());
	 						}
	 						
	 						
	 						if(challan.getChlnFreight() != null){
	 							cnDetailService.setLryHire(Double.parseDouble(challan.getChlnFreight()));
	 						}else{
	 							cnDetailService.setLryHire(0);
	 						}
	 						
	 						cnDetailService.setTotHireChrg(challan.getChlnTotalFreight());
	 						
	 						List<ChallanDetail> chdList = new ArrayList<>();
	 						cr = session.createCriteria(ChallanDetail.class);
	 						cr.add(Restrictions.eq(ChallanDetailCNTS.CHD_CHALLAN_CODE,challan.getChlnCode()));
	 						chdList = cr.list();
	 						
	 						if(!chdList.isEmpty()){
	 							
	 							List<Broker> brkList = new ArrayList<>();
	 							cr = session.createCriteria(Broker.class);
	 	 						cr.add(Restrictions.eq(BrokerCNTS.BRK_CODE,chdList.get(0).getChdBrCode()));
	 	 						brkList = cr.list();
	 	 						
	 	 						if(!brkList.isEmpty()){
	 	 							Broker broker = brkList.get(0);
	 	 							cnDetailService.setBrkName(broker.getBrkName());
	 	 							List<String> mobList = new ArrayList<String>();
	 	 							if(chdList.get(0).getChdBrMobNo() != null){
	 	 								mobList.add(chdList.get(0).getChdBrMobNo());
	 	 							}else{
	 	 								mobList = broker.getBrkPhNoList();
	 	 								if(mobList == null || mobList.isEmpty()){
	 	 									List<ContPerson> contPList = new ArrayList<>();
	 	 									cr = session.createCriteria(ContPerson.class);
	 	 		 	 						cr.add(Restrictions.eq(ContPersonCNTS.CP_REF_CODE,broker.getBrkCode()));
	 	 		 	 					    contPList = cr.list();
	 	 		 	 					    if(!contPList.isEmpty()){
	 	 		 	 					    	mobList = new ArrayList<>();
	 	 		 	 					    	//mobList.add(contPList.get(0).getCpMobile().get(0));
	 	 		 	 					    	mobList = contPList.get(0).getCpMobile();
	 	 		 	 					    }else{
	 	 		 	 					    mobList = new ArrayList<>();
	 	 		 	 					    }
	 	 								}
	 	 							}
	 	 							cnDetailService.setBrkCntList(mobList);
	 	 						}
	 							
	 	 						
	 	 						List<Owner> ownList = new ArrayList<>();
	 	 						cr = session.createCriteria(Owner.class);
	 	 						cr.add(Restrictions.eq(OwnerCNTS.OWN_CODE,chdList.get(0).getChdOwnCode()));
	 	 						ownList = cr.list();
	 	 						
	 	 						if(!ownList.isEmpty()){
	 	 							Owner owner = ownList.get(0);
	 	 							cnDetailService.setOwnName(owner.getOwnName());
	 	 							List<String> mobList = new ArrayList<String>();
	 	 							
	 	 							if(chdList.get(0).getChdOwnMobNo() != null){
	 	 								mobList.add(chdList.get(0).getChdOwnMobNo());
	 	 							}else{
	 	 								mobList = owner.getOwnPhNoList();
	 	 								if(mobList == null || mobList.isEmpty()){
	 	 									List<ContPerson> contPList = new ArrayList<>();
	 	 									cr = session.createCriteria(ContPerson.class);
	 	 		 	 						cr.add(Restrictions.eq(ContPersonCNTS.CP_REF_CODE,owner.getOwnCode()));
	 	 		 	 					    contPList = cr.list();
	 	 		 	 					    if(!contPList.isEmpty()){
	 	 		 	 					    	mobList = new ArrayList<>();
	 	 		 	 					    	//mobList.add(contPList.get(0).getCpMobile().get(0));
	 	 		 	 					    	mobList = contPList.get(0).getCpMobile();
	 	 		 	 					    }else{
	 	 		 	 					    	mobList = new ArrayList<>(); 
	 	 		 	 					    }
	 	 								}
	 	 							}
	 	 							
	 	 							cnDetailService.setOwnCntList(mobList);
	 	 							cnDetailService.setOwnPan(owner.getOwnPanNo());
	 	 							
	 	 							List<Address> addList = new ArrayList<>();
	 	 							cr = session.createCriteria(Address.class);   
	 	 					        cr.add(Restrictions.eq(AddressCNTS.REF_CODE,owner.getOwnCode()));
	 	 					        addList = cr.list();
	 	 					         
	 	 					        if(!addList.isEmpty()){
	 	 					        	String othAdd = addList.get(0).getCompleteAdd() +", "+ addList.get(0).getAddCity()
	 	 					        			+ "(" + addList.get(0).getAddPin() + "), " + addList.get(0).getAddState();
	 	 					        	cnDetailService.setOwnAdd(othAdd);
	 	 					        }
	 	 						}
	 	 						
	 	 						cnDetailService.setDriverName(chdList.get(0).getChdDvrName());
	 	 						cnDetailService.setDrvCntNo(chdList.get(0).getChdDvrMobNo());
	 	 						
	 						}
	 						
	 						cnDetList.add(cnDetailService);
							
							for(int j=1;j<ccList.size();j++){
								
								CnDetailService cnDetailService1 = new CnDetailService();
								
								cnDetailService1.setCnmtNo(cnmtList.get(i).getCnmtCode());
								
								Challan challan1 = new Challan();
								challan1 = (Challan) session.get(Challan.class,ccList.get(j).getChlnId());
								
								if(challan1.getChlnTimeAllow() != null){
									cnDetailService1.setTnsTime(Integer.parseInt(challan1.getChlnTimeAllow()));
								}
								
								cnDetailService1.setChlnNo(challan1.getChlnCode());
								cnDetailService1.setChlnDt(challan1.getChlnDt());
								
								if(challan1.getChlnLryRate() != null){
									cnDetailService1.setPrMtCost(Double.parseDouble(challan1.getChlnLryRate()) * 1000);
								}else{
									cnDetailService1.setPrMtCost(0);
								}
								
								if(challan1.getChlnAdvance() != null){
									cnDetailService1.setAdv(Double.parseDouble(challan1.getChlnAdvance()));
								}
								
		 						cnDetailService1.setBal(challan1.getChlnBalance());
		 						
		 						if(challan1.getChlnFreight() != null){
		 							cnDetailService1.setAnyOthChrg(Float.parseFloat(challan1.getChlnFreight()) - new Double(challan.getChlnTotalFreight()).floatValue());
		 						}
		 						
		 						
		 						if(challan1.getChlnFreight() != null){
		 							cnDetailService1.setLryHire(Double.parseDouble(challan1.getChlnFreight()));
		 						}else{
		 							cnDetailService1.setLryHire(0);
		 						}
		 						
		 						cnDetailService1.setTotHireChrg(challan1.getChlnTotalFreight());
		 						
		 						List<ChallanDetail> chdList1 = new ArrayList<>();
		 						cr = session.createCriteria(ChallanDetail.class);
		 						cr.add(Restrictions.eq(ChallanDetailCNTS.CHD_CHALLAN_CODE,challan1.getChlnCode()));
		 						chdList1 = cr.list();
		 						
		 						if(!chdList1.isEmpty()){
		 							
		 							List<Broker> brkList = new ArrayList<>();
		 							cr = session.createCriteria(Broker.class);
		 	 						cr.add(Restrictions.eq(BrokerCNTS.BRK_CODE,chdList1.get(0).getChdBrCode()));
		 	 						brkList = cr.list();
		 	 						
		 	 						if(!brkList.isEmpty()){
		 	 							Broker broker = brkList.get(0);
		 	 							cnDetailService1.setBrkName(broker.getBrkName());
		 	 							List<String> mobList = new ArrayList<String>();
		 	 							if(chdList1.get(0).getChdBrMobNo() != null){
		 	 								mobList.add(chdList1.get(0).getChdBrMobNo());
		 	 							}else{
		 	 								mobList = broker.getBrkPhNoList();
		 	 								if(mobList == null || mobList.isEmpty()){
		 	 									List<ContPerson> contPList = new ArrayList<>();
		 	 									cr = session.createCriteria(ContPerson.class);
		 	 		 	 						cr.add(Restrictions.eq(ContPersonCNTS.CP_REF_CODE,broker.getBrkCode()));
		 	 		 	 					    contPList = cr.list();
		 	 		 	 					    if(!contPList.isEmpty()){
		 	 		 	 					    	mobList = new ArrayList<>();
		 	 		 	 					    	//mobList.add(contPList.get(0).getCpMobile().get(0));
		 	 		 	 					    	mobList = contPList.get(0).getCpMobile();
		 	 		 	 					    }else{
		 	 		 	 					    mobList = new ArrayList<>();
		 	 		 	 					    }
		 	 								}
		 	 							}
		 	 							cnDetailService1.setBrkCntList(mobList);
		 	 						}
		 							
		 	 						
		 	 						List<Owner> ownList = new ArrayList<>();
		 	 						cr = session.createCriteria(Owner.class);
		 	 						cr.add(Restrictions.eq(OwnerCNTS.OWN_CODE,chdList1.get(0).getChdOwnCode()));
		 	 						ownList = cr.list();
		 	 						
		 	 						if(!ownList.isEmpty()){
		 	 							Owner owner = ownList.get(0);
		 	 							cnDetailService1.setOwnName(owner.getOwnName());
		 	 							List<String> mobList = new ArrayList<String>();
		 	 							
		 	 							if(chdList1.get(0).getChdOwnMobNo() != null){
		 	 								mobList.add(chdList1.get(0).getChdOwnMobNo());
		 	 							}else{
		 	 								mobList = owner.getOwnPhNoList();
		 	 								if(mobList == null || mobList.isEmpty()){
		 	 									List<ContPerson> contPList = new ArrayList<>();
		 	 									cr = session.createCriteria(ContPerson.class);
		 	 		 	 						cr.add(Restrictions.eq(ContPersonCNTS.CP_REF_CODE,owner.getOwnCode()));
		 	 		 	 					    contPList = cr.list();
		 	 		 	 					    if(!contPList.isEmpty()){
		 	 		 	 					    	mobList = new ArrayList<>();
		 	 		 	 					    	//mobList.add(contPList.get(0).getCpMobile().get(0));
		 	 		 	 					    	mobList = contPList.get(0).getCpMobile();
		 	 		 	 					    }else{
		 	 		 	 					    	mobList = new ArrayList<>(); 
		 	 		 	 					    }
		 	 								}
		 	 							}
		 	 							
		 	 							cnDetailService1.setOwnCntList(mobList);
		 	 							cnDetailService1.setOwnPan(owner.getOwnPanNo());
		 	 							
		 	 							List<Address> addList = new ArrayList<>();
		 	 							cr = session.createCriteria(Address.class);   
		 	 					        cr.add(Restrictions.eq(AddressCNTS.REF_CODE,owner.getOwnCode()));
		 	 					        addList = cr.list();
		 	 					         
		 	 					        if(!addList.isEmpty()){
		 	 					        	String othAdd = addList.get(0).getCompleteAdd() +", "+ addList.get(0).getAddCity()
		 	 					        			+ "(" + addList.get(0).getAddPin() + "), " + addList.get(0).getAddState();
		 	 					        	cnDetailService1.setOwnAdd(othAdd);
		 	 					        }
		 	 						}
		 	 						
		 	 						cnDetailService1.setDriverName(chdList.get(0).getChdDvrName());
		 	 						cnDetailService1.setDrvCntNo(chdList.get(0).getChdDvrMobNo());
		 	 					
		 						}
		 						
		 						cnDetList.add(cnDetailService1);
							}
						}
					}else{
						cnDetList.add(cnDetailService);
					}	
				}
				//main cnmt loop end
			}else{
				System.out.println("There is no cnmt between "+frDt+" to "+toDt);
			}
			
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
			logger.info("Exception in getCnDetByDt"+e);
		}finally{
			session.clear();
			session.close();
		}
		return cnDetList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<CnDetailService> getCnDetByBrh(Date frDt, Date toDt, String brhCode){
		System.out.println("enter into getCnDetByBrh funciton");
		List<CnDetailService> cnDetList = new ArrayList<>();
		List<Cnmt> cnmtList = new ArrayList<>();
		Session session=null;
		try{
			session = this.sessionFactory.openSession();
			
			Criteria cr = session.createCriteria(Cnmt.class);
			cr.add(Restrictions.between(CnmtCNTS.CNMT_DT, frDt, toDt));
			cr.add(Restrictions.eq(CnmtCNTS.BRANCH_CODE,brhCode));
			cnmtList = cr.list();
			
			System.out.println("size of cnmtList = "+cnmtList.size());
			if(!cnmtList.isEmpty()){
				//main cnmt loop start
				for(int i=0;i<cnmtList.size();i++){
					
					CnDetailService cnDetailService = new CnDetailService();
					cnDetailService.setCnmtNo(cnmtList.get(i).getCnmtCode());
					cnDetailService.setCnmtDt(cnmtList.get(i).getCnmtDt());
					cnDetailService.setBillNo(cnmtList.get(i).getCnmtBillNo());
					
					if(cnmtList.get(i).getCnmtBillNo() != null){
						List<Bill> blList = new ArrayList<>();
						cr = session.createCriteria(Bill.class);
						cr.add(Restrictions.eq(BillCNTS.Bill_NO,cnmtList.get(i).getCnmtBillNo()));
						blList = cr.list();
						if(!blList.isEmpty()){
							cnDetailService.setBlAmt(blList.get(0).getBlFinalTot());
						}
					}
					
					Station frStn = (Station) session.get(Station.class, Integer.parseInt(cnmtList.get(i).getCnmtFromSt()));
					Station toStn = (Station) session.get(Station.class, Integer.parseInt(cnmtList.get(i).getCnmtToSt()));
					
					cnDetailService.setFrStn(frStn.getStnName());
					cnDetailService.setToStn(toStn.getStnName());
					
					if(cnmtList.get(i).getCnmtConsignor() != null){
						Customer consignor = (Customer) session.get(Customer.class, Integer.parseInt(cnmtList.get(i).getCnmtConsignor()));
						cnDetailService.setConsignor(consignor.getCustName());
					}
					
					if(cnmtList.get(i).getCnmtConsignee() != null){
						Customer consignee = (Customer) session.get(Customer.class, Integer.parseInt(cnmtList.get(i).getCnmtConsignee()));
						cnDetailService.setConsignee(consignee.getCustName());
					}
					
					System.out.println("cnmtList.get(i).getCustCode() = "+cnmtList.get(i).getCustCode());
					if(cnmtList.get(i).getCustCode() == "" || cnmtList.get(i).getCustCode() == null){
						cnDetailService.setBlngParty("");
					}else{
						Customer customer = (Customer) session.get(Customer.class, Integer.parseInt(cnmtList.get(i).getCustCode()));
						cnDetailService.setBlngParty(customer.getCustName());
					}
					
					
					cnDetailService.setNoOfPckg(cnmtList.get(i).getCnmtNoOfPkg());
					
					if(cnmtList.get(i).getCnmtActualWt() > 0){
						cnDetailService.setActWt(cnmtList.get(i).getCnmtActualWt() / ConstantsValues.kgInTon);
					}else{
						cnDetailService.setActWt(0);
					}
					
					if(cnmtList.get(i).getCnmtGuaranteeWt() > 0){
						cnDetailService.setWtChrg(cnmtList.get(i).getCnmtGuaranteeWt() / ConstantsValues.kgInTon);
					}else{
						cnDetailService.setWtChrg(0);
					}
						
					if(cnmtList.get(i).getCnmtRate() >= 0){
						cnDetailService.setPrMtRt(cnmtList.get(i).getCnmtRate() * 1000);
					}else{
						cnDetailService.setPrMtRt(0);
					}
					
					cnDetailService.setInvList(cnmtList.get(i).getCnmtInvoiceNo());
					cnDetailService.setVog(cnmtList.get(i).getCnmtVOG());
					cnDetailService.setExpDtOfDly(cnmtList.get(i).getCnmtDtOfDly());
					cnDetailService.setCnmtFreight(cnmtList.get(i).getCnmtFreight());
					
					
					List<Cnmt_Challan> ccList  = new ArrayList<>();
					cr = session.createCriteria(Cnmt_Challan.class);
					cr.add(Restrictions.eq(Cnmt_ChallanCNTS.Cnmt_Chln_cnmtId, cnmtList.get(i).getCnmtId()));
					ccList = cr.list();
					
					if(!ccList.isEmpty()){
						if(ccList.size() == 1){
							Challan challan = new Challan();
							challan = (Challan) session.get(Challan.class,ccList.get(0).getChlnId());
							
							if(challan.getChlnTimeAllow() != null){
								cnDetailService.setTnsTime(Integer.parseInt(challan.getChlnTimeAllow()));
							}
							
							cnDetailService.setChlnNo(challan.getChlnCode());
							cnDetailService.setChlnDt(challan.getChlnDt());
							
							if(challan.getChlnLryRate() != null){
								cnDetailService.setPrMtCost(Double.parseDouble(challan.getChlnLryRate()) * 1000);
							}else{
								cnDetailService.setPrMtCost(0);
							}
							
							if(challan.getChlnAdvance() != null){
								cnDetailService.setAdv(Double.parseDouble(challan.getChlnAdvance()));
							}
							
	 						cnDetailService.setBal(challan.getChlnBalance());
	 						
	 						if(challan.getChlnFreight() != null){
	 							cnDetailService.setAnyOthChrg(Float.parseFloat(challan.getChlnFreight()) - new Double(challan.getChlnTotalFreight()).floatValue());
	 						}
	 						
	 						
	 						if(challan.getChlnFreight() != null){
	 							cnDetailService.setLryHire(Double.parseDouble(challan.getChlnFreight()));
	 						}else{
	 							cnDetailService.setLryHire(0);
	 						}
	 						
	 						cnDetailService.setTotHireChrg(challan.getChlnTotalFreight());
	 						
	 						List<ChallanDetail> chdList = new ArrayList<>();
	 						cr = session.createCriteria(ChallanDetail.class);
	 						cr.add(Restrictions.eq(ChallanDetailCNTS.CHD_CHALLAN_CODE,challan.getChlnCode()));
	 						chdList = cr.list();
	 						
	 						if(!chdList.isEmpty()){
	 							
	 							List<Broker> brkList = new ArrayList<>();
	 							cr = session.createCriteria(Broker.class);
	 	 						cr.add(Restrictions.eq(BrokerCNTS.BRK_CODE,chdList.get(0).getChdBrCode()));
	 	 						brkList = cr.list();
	 	 						
	 	 						if(!brkList.isEmpty()){
	 	 							Broker broker = brkList.get(0);
	 	 							cnDetailService.setBrkName(broker.getBrkName());
	 	 							List<String> mobList = new ArrayList<String>();
	 	 							if(chdList.get(0).getChdBrMobNo() != null){
	 	 								mobList.add(chdList.get(0).getChdBrMobNo());
	 	 							}else{
	 	 								mobList = broker.getBrkPhNoList();
	 	 								if(mobList == null || mobList.isEmpty()){
	 	 									List<ContPerson> contPList = new ArrayList<>();
	 	 									cr = session.createCriteria(ContPerson.class);
	 	 		 	 						cr.add(Restrictions.eq(ContPersonCNTS.CP_REF_CODE,broker.getBrkCode()));
	 	 		 	 					    contPList = cr.list();
	 	 		 	 					    if(!contPList.isEmpty()){
	 	 		 	 					    	mobList = new ArrayList<>();
	 	 		 	 					    	//mobList.add(contPList.get(0).getCpMobile().get(0));
	 	 		 	 					    	mobList = contPList.get(0).getCpMobile();
	 	 		 	 					    }else{
	 	 		 	 					    mobList = new ArrayList<>();
	 	 		 	 					    }
	 	 								}
	 	 							}
	 	 							cnDetailService.setBrkCntList(mobList);
	 	 						}
	 							
	 	 						
	 	 						List<Owner> ownList = new ArrayList<>();
	 	 						cr = session.createCriteria(Owner.class);
	 	 						cr.add(Restrictions.eq(OwnerCNTS.OWN_CODE,chdList.get(0).getChdOwnCode()));
	 	 						ownList = cr.list();
	 	 						
	 	 						if(!ownList.isEmpty()){
	 	 							Owner owner = ownList.get(0);
	 	 							cnDetailService.setOwnName(owner.getOwnName());
	 	 							List<String> mobList = new ArrayList<String>();
	 	 							
	 	 							if(chdList.get(0).getChdOwnMobNo() != null){
	 	 								mobList.add(chdList.get(0).getChdOwnMobNo());
	 	 							}else{
	 	 								mobList = owner.getOwnPhNoList();
	 	 								if(mobList == null || mobList.isEmpty()){
	 	 									List<ContPerson> contPList = new ArrayList<>();
	 	 									cr = session.createCriteria(ContPerson.class);
	 	 		 	 						cr.add(Restrictions.eq(ContPersonCNTS.CP_REF_CODE,owner.getOwnCode()));
	 	 		 	 					    contPList = cr.list();
	 	 		 	 					    if(!contPList.isEmpty()){
	 	 		 	 					    	mobList = new ArrayList<>();
	 	 		 	 					    	//mobList.add(contPList.get(0).getCpMobile().get(0));
	 	 		 	 					    	mobList = contPList.get(0).getCpMobile();
	 	 		 	 					    }else{
	 	 		 	 					    	mobList = new ArrayList<>(); 
	 	 		 	 					    }
	 	 								}
	 	 							}
	 	 							
	 	 							cnDetailService.setOwnCntList(mobList);
	 	 							cnDetailService.setOwnPan(owner.getOwnPanNo());
	 	 							
	 	 							List<Address> addList = new ArrayList<>();
	 	 							cr = session.createCriteria(Address.class);   
	 	 					        cr.add(Restrictions.eq(AddressCNTS.REF_CODE,owner.getOwnCode()));
	 	 					        addList = cr.list();
	 	 					         
	 	 					        if(!addList.isEmpty()){
	 	 					        	String othAdd = addList.get(0).getCompleteAdd() +", "+ addList.get(0).getAddCity()
	 	 					        			+ "(" + addList.get(0).getAddPin() + "), " + addList.get(0).getAddState();
	 	 					        	cnDetailService.setOwnAdd(othAdd);
	 	 					        }
	 	 						}
	 	 						
	 	 						cnDetailService.setDriverName(chdList.get(0).getChdDvrName());
	 	 						cnDetailService.setDrvCntNo(chdList.get(0).getChdDvrMobNo());
	 	 						
	 						}
	 						
	 						cnDetList.add(cnDetailService);
	 						
						}else{
							
							Challan challan = new Challan();
							challan = (Challan) session.get(Challan.class,ccList.get(0).getChlnId());
							
							if(challan.getChlnTimeAllow() != null){
								cnDetailService.setTnsTime(Integer.parseInt(challan.getChlnTimeAllow()));
							}
							
							cnDetailService.setChlnNo(challan.getChlnCode());
							cnDetailService.setChlnDt(challan.getChlnDt());
							
							if(challan.getChlnLryRate() != null){
								cnDetailService.setPrMtCost(Double.parseDouble(challan.getChlnLryRate()) * 1000);
							}else{
								cnDetailService.setPrMtCost(0);
							}
							
							if(challan.getChlnAdvance() != null){
								cnDetailService.setAdv(Double.parseDouble(challan.getChlnAdvance()));
							}
							
	 						cnDetailService.setBal(challan.getChlnBalance());
	 						
	 						if(challan.getChlnFreight() != null){
	 							cnDetailService.setAnyOthChrg(Float.parseFloat(challan.getChlnFreight()) - new Double(challan.getChlnTotalFreight()).floatValue());
	 						}
	 						
	 						
	 						if(challan.getChlnFreight() != null){
	 							cnDetailService.setLryHire(Double.parseDouble(challan.getChlnFreight()));
	 						}else{
	 							cnDetailService.setLryHire(0);
	 						}
	 						
	 						cnDetailService.setTotHireChrg(challan.getChlnTotalFreight());
	 						
	 						List<ChallanDetail> chdList = new ArrayList<>();
	 						cr = session.createCriteria(ChallanDetail.class);
	 						cr.add(Restrictions.eq(ChallanDetailCNTS.CHD_CHALLAN_CODE,challan.getChlnCode()));
	 						chdList = cr.list();
	 						
	 						if(!chdList.isEmpty()){
	 							
	 							List<Broker> brkList = new ArrayList<>();
	 							cr = session.createCriteria(Broker.class);
	 	 						cr.add(Restrictions.eq(BrokerCNTS.BRK_CODE,chdList.get(0).getChdBrCode()));
	 	 						brkList = cr.list();
	 	 						
	 	 						if(!brkList.isEmpty()){
	 	 							Broker broker = brkList.get(0);
	 	 							cnDetailService.setBrkName(broker.getBrkName());
	 	 							List<String> mobList = new ArrayList<String>();
	 	 							if(chdList.get(0).getChdBrMobNo() != null){
	 	 								mobList.add(chdList.get(0).getChdBrMobNo());
	 	 							}else{
	 	 								mobList = broker.getBrkPhNoList();
	 	 								if(mobList == null || mobList.isEmpty()){
	 	 									List<ContPerson> contPList = new ArrayList<>();
	 	 									cr = session.createCriteria(ContPerson.class);
	 	 		 	 						cr.add(Restrictions.eq(ContPersonCNTS.CP_REF_CODE,broker.getBrkCode()));
	 	 		 	 					    contPList = cr.list();
	 	 		 	 					    if(!contPList.isEmpty()){
	 	 		 	 					    	mobList = new ArrayList<>();
	 	 		 	 					    	//mobList.add(contPList.get(0).getCpMobile().get(0));
	 	 		 	 					    	mobList = contPList.get(0).getCpMobile();
	 	 		 	 					    }else{
	 	 		 	 					    mobList = new ArrayList<>();
	 	 		 	 					    }
	 	 								}
	 	 							}
	 	 							cnDetailService.setBrkCntList(mobList);
	 	 						}
	 							
	 	 						
	 	 						List<Owner> ownList = new ArrayList<>();
	 	 						cr = session.createCriteria(Owner.class);
	 	 						cr.add(Restrictions.eq(OwnerCNTS.OWN_CODE,chdList.get(0).getChdOwnCode()));
	 	 						ownList = cr.list();
	 	 						
	 	 						if(!ownList.isEmpty()){
	 	 							Owner owner = ownList.get(0);
	 	 							cnDetailService.setOwnName(owner.getOwnName());
	 	 							List<String> mobList = new ArrayList<String>();
	 	 							
	 	 							if(chdList.get(0).getChdOwnMobNo() != null){
	 	 								mobList.add(chdList.get(0).getChdOwnMobNo());
	 	 							}else{
	 	 								mobList = owner.getOwnPhNoList();
	 	 								if(mobList == null || mobList.isEmpty()){
	 	 									List<ContPerson> contPList = new ArrayList<>();
	 	 									cr = session.createCriteria(ContPerson.class);
	 	 		 	 						cr.add(Restrictions.eq(ContPersonCNTS.CP_REF_CODE,owner.getOwnCode()));
	 	 		 	 					    contPList = cr.list();
	 	 		 	 					    if(!contPList.isEmpty()){
	 	 		 	 					    	mobList = new ArrayList<>();
	 	 		 	 					    	//mobList.add(contPList.get(0).getCpMobile().get(0));
	 	 		 	 					    	mobList = contPList.get(0).getCpMobile();
	 	 		 	 					    }else{
	 	 		 	 					    	mobList = new ArrayList<>(); 
	 	 		 	 					    }
	 	 								}
	 	 							}
	 	 							
	 	 							cnDetailService.setOwnCntList(mobList);
	 	 							cnDetailService.setOwnPan(owner.getOwnPanNo());
	 	 							
	 	 							List<Address> addList = new ArrayList<>();
	 	 							cr = session.createCriteria(Address.class);   
	 	 					        cr.add(Restrictions.eq(AddressCNTS.REF_CODE,owner.getOwnCode()));
	 	 					        addList = cr.list();
	 	 					         
	 	 					        if(!addList.isEmpty()){
	 	 					        	String othAdd = addList.get(0).getCompleteAdd() +", "+ addList.get(0).getAddCity()
	 	 					        			+ "(" + addList.get(0).getAddPin() + "), " + addList.get(0).getAddState();
	 	 					        	cnDetailService.setOwnAdd(othAdd);
	 	 					        }
	 	 						}
	 	 						
	 	 						cnDetailService.setDriverName(chdList.get(0).getChdDvrName());
	 	 						cnDetailService.setDrvCntNo(chdList.get(0).getChdDvrMobNo());
	 	 						
	 						}
	 						
	 						cnDetList.add(cnDetailService);
							
							for(int j=1;j<ccList.size();j++){
								
								CnDetailService cnDetailService1 = new CnDetailService();
								
								cnDetailService1.setCnmtNo(cnmtList.get(i).getCnmtCode());
								
								Challan challan1 = new Challan();
								challan1 = (Challan) session.get(Challan.class,ccList.get(j).getChlnId());
								
								if(challan1.getChlnTimeAllow() != null){
									cnDetailService1.setTnsTime(Integer.parseInt(challan1.getChlnTimeAllow()));
								}
								
								cnDetailService1.setChlnNo(challan1.getChlnCode());
								cnDetailService1.setChlnDt(challan1.getChlnDt());
								
								if(challan1.getChlnLryRate() != null){
									cnDetailService1.setPrMtCost(Double.parseDouble(challan1.getChlnLryRate()) * 1000);
								}else{
									cnDetailService1.setPrMtCost(0);
								}
								
								if(challan1.getChlnAdvance() != null){
									cnDetailService1.setAdv(Double.parseDouble(challan1.getChlnAdvance()));
								}
								
		 						cnDetailService1.setBal(challan1.getChlnBalance());
		 						
		 						if(challan1.getChlnFreight() != null){
		 							cnDetailService1.setAnyOthChrg(Float.parseFloat(challan1.getChlnFreight()) - new Double(challan.getChlnTotalFreight()).floatValue());
		 						}
		 						
		 						
		 						if(challan1.getChlnFreight() != null){
		 							cnDetailService1.setLryHire(Double.parseDouble(challan1.getChlnFreight()));
		 						}else{
		 							cnDetailService1.setLryHire(0);
		 						}
		 						
		 						cnDetailService1.setTotHireChrg(challan1.getChlnTotalFreight());
		 						
		 						List<ChallanDetail> chdList1 = new ArrayList<>();
		 						cr = session.createCriteria(ChallanDetail.class);
		 						cr.add(Restrictions.eq(ChallanDetailCNTS.CHD_CHALLAN_CODE,challan1.getChlnCode()));
		 						chdList1 = cr.list();
		 						
		 						if(!chdList1.isEmpty()){
		 							
		 							List<Broker> brkList = new ArrayList<>();
		 							cr = session.createCriteria(Broker.class);
		 	 						cr.add(Restrictions.eq(BrokerCNTS.BRK_CODE,chdList1.get(0).getChdBrCode()));
		 	 						brkList = cr.list();
		 	 						
		 	 						if(!brkList.isEmpty()){
		 	 							Broker broker = brkList.get(0);
		 	 							cnDetailService1.setBrkName(broker.getBrkName());
		 	 							List<String> mobList = new ArrayList<String>();
		 	 							if(chdList1.get(0).getChdBrMobNo() != null){
		 	 								mobList.add(chdList1.get(0).getChdBrMobNo());
		 	 							}else{
		 	 								mobList = broker.getBrkPhNoList();
		 	 								if(mobList == null || mobList.isEmpty()){
		 	 									List<ContPerson> contPList = new ArrayList<>();
		 	 									cr = session.createCriteria(ContPerson.class);
		 	 		 	 						cr.add(Restrictions.eq(ContPersonCNTS.CP_REF_CODE,broker.getBrkCode()));
		 	 		 	 					    contPList = cr.list();
		 	 		 	 					    if(!contPList.isEmpty()){
		 	 		 	 					    	mobList = new ArrayList<>();
		 	 		 	 					    	//mobList.add(contPList.get(0).getCpMobile().get(0));
		 	 		 	 					    	mobList = contPList.get(0).getCpMobile();
		 	 		 	 					    }else{
		 	 		 	 					    mobList = new ArrayList<>();
		 	 		 	 					    }
		 	 								}
		 	 							}
		 	 							cnDetailService1.setBrkCntList(mobList);
		 	 						}
		 							
		 	 						
		 	 						List<Owner> ownList = new ArrayList<>();
		 	 						cr = session.createCriteria(Owner.class);
		 	 						cr.add(Restrictions.eq(OwnerCNTS.OWN_CODE,chdList1.get(0).getChdOwnCode()));
		 	 						ownList = cr.list();
		 	 						
		 	 						if(!ownList.isEmpty()){
		 	 							Owner owner = ownList.get(0);
		 	 							cnDetailService1.setOwnName(owner.getOwnName());
		 	 							List<String> mobList = new ArrayList<String>();
		 	 							
		 	 							if(chdList1.get(0).getChdOwnMobNo() != null){
		 	 								mobList.add(chdList1.get(0).getChdOwnMobNo());
		 	 							}else{
		 	 								mobList = owner.getOwnPhNoList();
		 	 								if(mobList == null || mobList.isEmpty()){
		 	 									List<ContPerson> contPList = new ArrayList<>();
		 	 									cr = session.createCriteria(ContPerson.class);
		 	 		 	 						cr.add(Restrictions.eq(ContPersonCNTS.CP_REF_CODE,owner.getOwnCode()));
		 	 		 	 					    contPList = cr.list();
		 	 		 	 					    if(!contPList.isEmpty()){
		 	 		 	 					    	mobList = new ArrayList<>();
		 	 		 	 					    	//mobList.add(contPList.get(0).getCpMobile().get(0));
		 	 		 	 					    	mobList = contPList.get(0).getCpMobile();
		 	 		 	 					    }else{
		 	 		 	 					    	mobList = new ArrayList<>(); 
		 	 		 	 					    }
		 	 								}
		 	 							}
		 	 							
		 	 							cnDetailService1.setOwnCntList(mobList);
		 	 							cnDetailService1.setOwnPan(owner.getOwnPanNo());
		 	 							
		 	 							List<Address> addList = new ArrayList<>();
		 	 							cr = session.createCriteria(Address.class);   
		 	 					        cr.add(Restrictions.eq(AddressCNTS.REF_CODE,owner.getOwnCode()));
		 	 					        addList = cr.list();
		 	 					         
		 	 					        if(!addList.isEmpty()){
		 	 					        	String othAdd = addList.get(0).getCompleteAdd() +", "+ addList.get(0).getAddCity()
		 	 					        			+ "(" + addList.get(0).getAddPin() + "), " + addList.get(0).getAddState();
		 	 					        	cnDetailService1.setOwnAdd(othAdd);
		 	 					        }
		 	 						}
		 	 						
		 	 						cnDetailService1.setDriverName(chdList.get(0).getChdDvrName());
		 	 						cnDetailService1.setDrvCntNo(chdList.get(0).getChdDvrMobNo());
		 	 					
		 						}
		 						
		 						cnDetList.add(cnDetailService1);
							}
						}
					}else{
						cnDetList.add(cnDetailService);
					}	
				}
				//main cnmt loop end
			}else{
				System.out.println("There is no cnmt between "+frDt+" to "+toDt);
			}
			

			
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
			logger.info("Exception in getCnDetBybrh"+e);
		}finally{
			session.clear();
			session.close();	
		}
		return cnDetList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<CnDetailService> getCnDetByCust(Date frDt, Date toDt, String custCode){
		System.out.println("enter into getCnDetByBrh funciton");
		List<CnDetailService> cnDetList = new ArrayList<>();
		List<Cnmt> cnmtList = new ArrayList<>();
		Session session=null;
		try{
			session = this.sessionFactory.openSession();
			
			Criteria cr = session.createCriteria(Cnmt.class);
			cr.add(Restrictions.between(CnmtCNTS.CNMT_DT, frDt, toDt));
			cr.add(Restrictions.eq(CnmtCNTS.CUST_CODE,custCode));
			cnmtList = cr.list();
			
			System.out.println("size of cnmtList = "+cnmtList.size());
			if(!cnmtList.isEmpty()){
				//main cnmt loop start
				for(int i=0;i<cnmtList.size();i++){
					
					CnDetailService cnDetailService = new CnDetailService();
					cnDetailService.setCnmtNo(cnmtList.get(i).getCnmtCode());
					cnDetailService.setCnmtDt(cnmtList.get(i).getCnmtDt());
					cnDetailService.setBillNo(cnmtList.get(i).getCnmtBillNo());
					
					if(cnmtList.get(i).getCnmtBillNo() != null){
						List<Bill> blList = new ArrayList<>();
						cr = session.createCriteria(Bill.class);
						cr.add(Restrictions.eq(BillCNTS.Bill_NO,cnmtList.get(i).getCnmtBillNo()));
						blList = cr.list();
						if(!blList.isEmpty()){
							cnDetailService.setBlAmt(blList.get(0).getBlFinalTot());
						}
					}
					
					Station frStn = (Station) session.get(Station.class, Integer.parseInt(cnmtList.get(i).getCnmtFromSt()));
					Station toStn = (Station) session.get(Station.class, Integer.parseInt(cnmtList.get(i).getCnmtToSt()));
					
					cnDetailService.setFrStn(frStn.getStnName());
					cnDetailService.setToStn(toStn.getStnName());
					
					if(cnmtList.get(i).getCnmtConsignor() != null){
						Customer consignor = (Customer) session.get(Customer.class, Integer.parseInt(cnmtList.get(i).getCnmtConsignor()));
						cnDetailService.setConsignor(consignor.getCustName());
					}
					
					if(cnmtList.get(i).getCnmtConsignee() != null){
						Customer consignee = (Customer) session.get(Customer.class, Integer.parseInt(cnmtList.get(i).getCnmtConsignee()));
						cnDetailService.setConsignee(consignee.getCustName());
					}
					
					System.out.println("cnmtList.get(i).getCustCode() = "+cnmtList.get(i).getCustCode());
					if(cnmtList.get(i).getCustCode() == "" || cnmtList.get(i).getCustCode() == null){
						cnDetailService.setBlngParty("");
					}else{
						Customer customer = (Customer) session.get(Customer.class, Integer.parseInt(cnmtList.get(i).getCustCode()));
						cnDetailService.setBlngParty(customer.getCustName());
					}
					
					
					cnDetailService.setNoOfPckg(cnmtList.get(i).getCnmtNoOfPkg());
					
					if(cnmtList.get(i).getCnmtActualWt() > 0){
						cnDetailService.setActWt(cnmtList.get(i).getCnmtActualWt() / ConstantsValues.kgInTon);
					}else{
						cnDetailService.setActWt(0);
					}
					
					if(cnmtList.get(i).getCnmtGuaranteeWt() > 0){
						cnDetailService.setWtChrg(cnmtList.get(i).getCnmtGuaranteeWt() / ConstantsValues.kgInTon);
					}else{
						cnDetailService.setWtChrg(0);
					}
						
					if(cnmtList.get(i).getCnmtRate() >= 0){
						cnDetailService.setPrMtRt(cnmtList.get(i).getCnmtRate() * 1000);
					}else{
						cnDetailService.setPrMtRt(0);
					}
					
					cnDetailService.setInvList(cnmtList.get(i).getCnmtInvoiceNo());
					cnDetailService.setVog(cnmtList.get(i).getCnmtVOG());
					cnDetailService.setExpDtOfDly(cnmtList.get(i).getCnmtDtOfDly());
					cnDetailService.setCnmtFreight(cnmtList.get(i).getCnmtFreight());
					
					
					List<Cnmt_Challan> ccList  = new ArrayList<>();
					cr = session.createCriteria(Cnmt_Challan.class);
					cr.add(Restrictions.eq(Cnmt_ChallanCNTS.Cnmt_Chln_cnmtId, cnmtList.get(i).getCnmtId()));
					ccList = cr.list();
					
					if(!ccList.isEmpty()){
						if(ccList.size() == 1){
							Challan challan = new Challan();
							challan = (Challan) session.get(Challan.class,ccList.get(0).getChlnId());
							
							if(challan.getChlnTimeAllow() != null){
								cnDetailService.setTnsTime(Integer.parseInt(challan.getChlnTimeAllow()));
							}
							
							cnDetailService.setChlnNo(challan.getChlnCode());
							cnDetailService.setChlnDt(challan.getChlnDt());
							
							if(challan.getChlnLryRate() != null){
								cnDetailService.setPrMtCost(Double.parseDouble(challan.getChlnLryRate()) * 1000);
							}else{
								cnDetailService.setPrMtCost(0);
							}
							
							if(challan.getChlnAdvance() != null){
								cnDetailService.setAdv(Double.parseDouble(challan.getChlnAdvance()));
							}
							
	 						cnDetailService.setBal(challan.getChlnBalance());
	 						
	 						if(challan.getChlnFreight() != null){
	 							cnDetailService.setAnyOthChrg(Float.parseFloat(challan.getChlnFreight()) - new Double(challan.getChlnTotalFreight()).floatValue());
	 						}
	 						
	 						
	 						if(challan.getChlnFreight() != null){
	 							cnDetailService.setLryHire(Double.parseDouble(challan.getChlnFreight()));
	 						}else{
	 							cnDetailService.setLryHire(0);
	 						}
	 						
	 						cnDetailService.setTotHireChrg(challan.getChlnTotalFreight());
	 						
	 						List<ChallanDetail> chdList = new ArrayList<>();
	 						cr = session.createCriteria(ChallanDetail.class);
	 						cr.add(Restrictions.eq(ChallanDetailCNTS.CHD_CHALLAN_CODE,challan.getChlnCode()));
	 						chdList = cr.list();
	 						
	 						if(!chdList.isEmpty()){
	 							
	 							List<Broker> brkList = new ArrayList<>();
	 							cr = session.createCriteria(Broker.class);
	 	 						cr.add(Restrictions.eq(BrokerCNTS.BRK_CODE,chdList.get(0).getChdBrCode()));
	 	 						brkList = cr.list();
	 	 						
	 	 						if(!brkList.isEmpty()){
	 	 							Broker broker = brkList.get(0);
	 	 							cnDetailService.setBrkName(broker.getBrkName());
	 	 							List<String> mobList = new ArrayList<String>();
	 	 							if(chdList.get(0).getChdBrMobNo() != null){
	 	 								mobList.add(chdList.get(0).getChdBrMobNo());
	 	 							}else{
	 	 								mobList = broker.getBrkPhNoList();
	 	 								if(mobList == null || mobList.isEmpty()){
	 	 									List<ContPerson> contPList = new ArrayList<>();
	 	 									cr = session.createCriteria(ContPerson.class);
	 	 		 	 						cr.add(Restrictions.eq(ContPersonCNTS.CP_REF_CODE,broker.getBrkCode()));
	 	 		 	 					    contPList = cr.list();
	 	 		 	 					    if(!contPList.isEmpty()){
	 	 		 	 					    	mobList = new ArrayList<>();
	 	 		 	 					    	//mobList.add(contPList.get(0).getCpMobile().get(0));
	 	 		 	 					    	mobList = contPList.get(0).getCpMobile();
	 	 		 	 					    }else{
	 	 		 	 					    mobList = new ArrayList<>();
	 	 		 	 					    }
	 	 								}
	 	 							}
	 	 							cnDetailService.setBrkCntList(mobList);
	 	 						}
	 							
	 	 						
	 	 						List<Owner> ownList = new ArrayList<>();
	 	 						cr = session.createCriteria(Owner.class);
	 	 						cr.add(Restrictions.eq(OwnerCNTS.OWN_CODE,chdList.get(0).getChdOwnCode()));
	 	 						ownList = cr.list();
	 	 						
	 	 						if(!ownList.isEmpty()){
	 	 							Owner owner = ownList.get(0);
	 	 							cnDetailService.setOwnName(owner.getOwnName());
	 	 							List<String> mobList = new ArrayList<String>();
	 	 							
	 	 							if(chdList.get(0).getChdOwnMobNo() != null){
	 	 								mobList.add(chdList.get(0).getChdOwnMobNo());
	 	 							}else{
	 	 								mobList = owner.getOwnPhNoList();
	 	 								if(mobList == null || mobList.isEmpty()){
	 	 									List<ContPerson> contPList = new ArrayList<>();
	 	 									cr = session.createCriteria(ContPerson.class);
	 	 		 	 						cr.add(Restrictions.eq(ContPersonCNTS.CP_REF_CODE,owner.getOwnCode()));
	 	 		 	 					    contPList = cr.list();
	 	 		 	 					    if(!contPList.isEmpty()){
	 	 		 	 					    	mobList = new ArrayList<>();
	 	 		 	 					    	//mobList.add(contPList.get(0).getCpMobile().get(0));
	 	 		 	 					    	mobList = contPList.get(0).getCpMobile();
	 	 		 	 					    }else{
	 	 		 	 					    	mobList = new ArrayList<>(); 
	 	 		 	 					    }
	 	 								}
	 	 							}
	 	 							
	 	 							cnDetailService.setOwnCntList(mobList);
	 	 							cnDetailService.setOwnPan(owner.getOwnPanNo());
	 	 							
	 	 							List<Address> addList = new ArrayList<>();
	 	 							cr = session.createCriteria(Address.class);   
	 	 					        cr.add(Restrictions.eq(AddressCNTS.REF_CODE,owner.getOwnCode()));
	 	 					        addList = cr.list();
	 	 					         
	 	 					        if(!addList.isEmpty()){
	 	 					        	String othAdd = addList.get(0).getCompleteAdd() +", "+ addList.get(0).getAddCity()
	 	 					        			+ "(" + addList.get(0).getAddPin() + "), " + addList.get(0).getAddState();
	 	 					        	cnDetailService.setOwnAdd(othAdd);
	 	 					        }
	 	 						}
	 	 						
	 	 						cnDetailService.setDriverName(chdList.get(0).getChdDvrName());
	 	 						cnDetailService.setDrvCntNo(chdList.get(0).getChdDvrMobNo());
	 	 						
	 						}
	 						
	 						cnDetList.add(cnDetailService);
	 						
						}else{
							
							Challan challan = new Challan();
							challan = (Challan) session.get(Challan.class,ccList.get(0).getChlnId());
							
							if(challan.getChlnTimeAllow() != null){
								cnDetailService.setTnsTime(Integer.parseInt(challan.getChlnTimeAllow()));
							}
							
							cnDetailService.setChlnNo(challan.getChlnCode());
							cnDetailService.setChlnDt(challan.getChlnDt());
							
							if(challan.getChlnLryRate() != null){
								cnDetailService.setPrMtCost(Double.parseDouble(challan.getChlnLryRate()) * 1000);
							}else{
								cnDetailService.setPrMtCost(0);
							}
							
							if(challan.getChlnAdvance() != null){
								cnDetailService.setAdv(Double.parseDouble(challan.getChlnAdvance()));
							}
							
	 						cnDetailService.setBal(challan.getChlnBalance());
	 						
	 						if(challan.getChlnFreight() != null){
	 							cnDetailService.setAnyOthChrg(Float.parseFloat(challan.getChlnFreight()) - new Double(challan.getChlnTotalFreight()).floatValue());
	 						}
	 						
	 						
	 						if(challan.getChlnFreight() != null){
	 							cnDetailService.setLryHire(Double.parseDouble(challan.getChlnFreight()));
	 						}else{
	 							cnDetailService.setLryHire(0);
	 						}
	 						
	 						cnDetailService.setTotHireChrg(challan.getChlnTotalFreight());
	 						
	 						List<ChallanDetail> chdList = new ArrayList<>();
	 						cr = session.createCriteria(ChallanDetail.class);
	 						cr.add(Restrictions.eq(ChallanDetailCNTS.CHD_CHALLAN_CODE,challan.getChlnCode()));
	 						chdList = cr.list();
	 						
	 						if(!chdList.isEmpty()){
	 							
	 							List<Broker> brkList = new ArrayList<>();
	 							cr = session.createCriteria(Broker.class);
	 	 						cr.add(Restrictions.eq(BrokerCNTS.BRK_CODE,chdList.get(0).getChdBrCode()));
	 	 						brkList = cr.list();
	 	 						
	 	 						if(!brkList.isEmpty()){
	 	 							Broker broker = brkList.get(0);
	 	 							cnDetailService.setBrkName(broker.getBrkName());
	 	 							List<String> mobList = new ArrayList<String>();
	 	 							if(chdList.get(0).getChdBrMobNo() != null){
	 	 								mobList.add(chdList.get(0).getChdBrMobNo());
	 	 							}else{
	 	 								mobList = broker.getBrkPhNoList();
	 	 								if(mobList == null || mobList.isEmpty()){
	 	 									List<ContPerson> contPList = new ArrayList<>();
	 	 									cr = session.createCriteria(ContPerson.class);
	 	 		 	 						cr.add(Restrictions.eq(ContPersonCNTS.CP_REF_CODE,broker.getBrkCode()));
	 	 		 	 					    contPList = cr.list();
	 	 		 	 					    if(!contPList.isEmpty()){
	 	 		 	 					    	mobList = new ArrayList<>();
	 	 		 	 					    	//mobList.add(contPList.get(0).getCpMobile().get(0));
	 	 		 	 					    	mobList = contPList.get(0).getCpMobile();
	 	 		 	 					    }else{
	 	 		 	 					    mobList = new ArrayList<>();
	 	 		 	 					    }
	 	 								}
	 	 							}
	 	 							cnDetailService.setBrkCntList(mobList);
	 	 						}
	 							
	 	 						
	 	 						List<Owner> ownList = new ArrayList<>();
	 	 						cr = session.createCriteria(Owner.class);
	 	 						cr.add(Restrictions.eq(OwnerCNTS.OWN_CODE,chdList.get(0).getChdOwnCode()));
	 	 						ownList = cr.list();
	 	 						
	 	 						if(!ownList.isEmpty()){
	 	 							Owner owner = ownList.get(0);
	 	 							cnDetailService.setOwnName(owner.getOwnName());
	 	 							List<String> mobList = new ArrayList<String>();
	 	 							
	 	 							if(chdList.get(0).getChdOwnMobNo() != null){
	 	 								mobList.add(chdList.get(0).getChdOwnMobNo());
	 	 							}else{
	 	 								mobList = owner.getOwnPhNoList();
	 	 								if(mobList == null || mobList.isEmpty()){
	 	 									List<ContPerson> contPList = new ArrayList<>();
	 	 									cr = session.createCriteria(ContPerson.class);
	 	 		 	 						cr.add(Restrictions.eq(ContPersonCNTS.CP_REF_CODE,owner.getOwnCode()));
	 	 		 	 					    contPList = cr.list();
	 	 		 	 					    if(!contPList.isEmpty()){
	 	 		 	 					    	mobList = new ArrayList<>();
	 	 		 	 					    	//mobList.add(contPList.get(0).getCpMobile().get(0));
	 	 		 	 					    	mobList = contPList.get(0).getCpMobile();
	 	 		 	 					    }else{
	 	 		 	 					    	mobList = new ArrayList<>(); 
	 	 		 	 					    }
	 	 								}
	 	 							}
	 	 							
	 	 							cnDetailService.setOwnCntList(mobList);
	 	 							cnDetailService.setOwnPan(owner.getOwnPanNo());
	 	 							
	 	 							List<Address> addList = new ArrayList<>();
	 	 							cr = session.createCriteria(Address.class);   
	 	 					        cr.add(Restrictions.eq(AddressCNTS.REF_CODE,owner.getOwnCode()));
	 	 					        addList = cr.list();
	 	 					         
	 	 					        if(!addList.isEmpty()){
	 	 					        	String othAdd = addList.get(0).getCompleteAdd() +", "+ addList.get(0).getAddCity()
	 	 					        			+ "(" + addList.get(0).getAddPin() + "), " + addList.get(0).getAddState();
	 	 					        	cnDetailService.setOwnAdd(othAdd);
	 	 					        }
	 	 						}
	 	 						
	 	 						cnDetailService.setDriverName(chdList.get(0).getChdDvrName());
	 	 						cnDetailService.setDrvCntNo(chdList.get(0).getChdDvrMobNo());
	 	 						
	 						}
	 						
	 						cnDetList.add(cnDetailService);
							
							for(int j=1;j<ccList.size();j++){
								
								CnDetailService cnDetailService1 = new CnDetailService();
								
								cnDetailService1.setCnmtNo(cnmtList.get(i).getCnmtCode());
								
								Challan challan1 = new Challan();
								challan1 = (Challan) session.get(Challan.class,ccList.get(j).getChlnId());
								
								if(challan1.getChlnTimeAllow() != null){
									cnDetailService1.setTnsTime(Integer.parseInt(challan1.getChlnTimeAllow()));
								}
								
								cnDetailService1.setChlnNo(challan1.getChlnCode());
								cnDetailService1.setChlnDt(challan1.getChlnDt());
								
								if(challan1.getChlnLryRate() != null){
									cnDetailService1.setPrMtCost(Double.parseDouble(challan1.getChlnLryRate()) * 1000);
								}else{
									cnDetailService1.setPrMtCost(0);
								}
								
								if(challan1.getChlnAdvance() != null){
									cnDetailService1.setAdv(Double.parseDouble(challan1.getChlnAdvance()));
								}
								
		 						cnDetailService1.setBal(challan1.getChlnBalance());
		 						
		 						if(challan1.getChlnFreight() != null){
		 							cnDetailService1.setAnyOthChrg(Float.parseFloat(challan1.getChlnFreight()) - new Double(challan.getChlnTotalFreight()).floatValue());
		 						}
		 						
		 						
		 						if(challan1.getChlnFreight() != null){
		 							cnDetailService1.setLryHire(Double.parseDouble(challan1.getChlnFreight()));
		 						}else{
		 							cnDetailService1.setLryHire(0);
		 						}
		 						
		 						cnDetailService1.setTotHireChrg(challan1.getChlnTotalFreight());
		 						
		 						List<ChallanDetail> chdList1 = new ArrayList<>();
		 						cr = session.createCriteria(ChallanDetail.class);
		 						cr.add(Restrictions.eq(ChallanDetailCNTS.CHD_CHALLAN_CODE,challan1.getChlnCode()));
		 						chdList1 = cr.list();
		 						
		 						if(!chdList1.isEmpty()){
		 							
		 							List<Broker> brkList = new ArrayList<>();
		 							cr = session.createCriteria(Broker.class);
		 	 						cr.add(Restrictions.eq(BrokerCNTS.BRK_CODE,chdList1.get(0).getChdBrCode()));
		 	 						brkList = cr.list();
		 	 						
		 	 						if(!brkList.isEmpty()){
		 	 							Broker broker = brkList.get(0);
		 	 							cnDetailService1.setBrkName(broker.getBrkName());
		 	 							List<String> mobList = new ArrayList<String>();
		 	 							if(chdList1.get(0).getChdBrMobNo() != null){
		 	 								mobList.add(chdList1.get(0).getChdBrMobNo());
		 	 							}else{
		 	 								mobList = broker.getBrkPhNoList();
		 	 								if(mobList == null || mobList.isEmpty()){
		 	 									List<ContPerson> contPList = new ArrayList<>();
		 	 									cr = session.createCriteria(ContPerson.class);
		 	 		 	 						cr.add(Restrictions.eq(ContPersonCNTS.CP_REF_CODE,broker.getBrkCode()));
		 	 		 	 					    contPList = cr.list();
		 	 		 	 					    if(!contPList.isEmpty()){
		 	 		 	 					    	mobList = new ArrayList<>();
		 	 		 	 					    	//mobList.add(contPList.get(0).getCpMobile().get(0));
		 	 		 	 					    	mobList = contPList.get(0).getCpMobile();
		 	 		 	 					    }else{
		 	 		 	 					    mobList = new ArrayList<>();
		 	 		 	 					    }
		 	 								}
		 	 							}
		 	 							cnDetailService1.setBrkCntList(mobList);
		 	 						}
		 							
		 	 						
		 	 						List<Owner> ownList = new ArrayList<>();
		 	 						cr = session.createCriteria(Owner.class);
		 	 						cr.add(Restrictions.eq(OwnerCNTS.OWN_CODE,chdList1.get(0).getChdOwnCode()));
		 	 						ownList = cr.list();
		 	 						
		 	 						if(!ownList.isEmpty()){
		 	 							Owner owner = ownList.get(0);
		 	 							cnDetailService1.setOwnName(owner.getOwnName());
		 	 							List<String> mobList = new ArrayList<String>();
		 	 							
		 	 							if(chdList1.get(0).getChdOwnMobNo() != null){
		 	 								mobList.add(chdList1.get(0).getChdOwnMobNo());
		 	 							}else{
		 	 								mobList = owner.getOwnPhNoList();
		 	 								if(mobList == null || mobList.isEmpty()){
		 	 									List<ContPerson> contPList = new ArrayList<>();
		 	 									cr = session.createCriteria(ContPerson.class);
		 	 		 	 						cr.add(Restrictions.eq(ContPersonCNTS.CP_REF_CODE,owner.getOwnCode()));
		 	 		 	 					    contPList = cr.list();
		 	 		 	 					    if(!contPList.isEmpty()){
		 	 		 	 					    	mobList = new ArrayList<>();
		 	 		 	 					    	//mobList.add(contPList.get(0).getCpMobile().get(0));
		 	 		 	 					    	mobList = contPList.get(0).getCpMobile();
		 	 		 	 					    }else{
		 	 		 	 					    	mobList = new ArrayList<>(); 
		 	 		 	 					    }
		 	 								}
		 	 							}
		 	 							
		 	 							cnDetailService1.setOwnCntList(mobList);
		 	 							cnDetailService1.setOwnPan(owner.getOwnPanNo());
		 	 							
		 	 							List<Address> addList = new ArrayList<>();
		 	 							cr = session.createCriteria(Address.class);   
		 	 					        cr.add(Restrictions.eq(AddressCNTS.REF_CODE,owner.getOwnCode()));
		 	 					        addList = cr.list();
		 	 					         
		 	 					        if(!addList.isEmpty()){
		 	 					        	String othAdd = addList.get(0).getCompleteAdd() +", "+ addList.get(0).getAddCity()
		 	 					        			+ "(" + addList.get(0).getAddPin() + "), " + addList.get(0).getAddState();
		 	 					        	cnDetailService1.setOwnAdd(othAdd);
		 	 					        }
		 	 						}
		 	 						
		 	 						cnDetailService1.setDriverName(chdList.get(0).getChdDvrName());
		 	 						cnDetailService1.setDrvCntNo(chdList.get(0).getChdDvrMobNo());
		 	 					
		 						}
		 						
		 						cnDetList.add(cnDetailService1);
							}
						}
					}else{
						cnDetList.add(cnDetailService);
					}	
				}
				//main cnmt loop end
			}else{
				System.out.println("There is no cnmt between "+frDt+" to "+toDt);
			}
			
			
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
			logger.info("Exception in getCnDetBycust"+e);
		}finally{
			session.clear();
			session.close();
		}
		return cnDetList;
	}

	@Override
	public void test() {
		Session session=null;
		try {
			
			session = this.sessionFactory.openSession();
			
			/*SQLQuery sqlQuery = session.createSQLQuery("SELECT * From lhpvadv_challan where chlnId = :chlnId");
			sqlQuery.setParameter("chlnId", 2981);
			
			List<Object> query = sqlQuery.list();
			
			 for(Object rows : query){
				    Object[] row = (Object[]) rows;
				    int field1 = (int) row[0]; // contains field1
				    int field2 = (int) row[1];
				    System.out.println("query to third table: "+field1+" :"+field2);
			 }*/
			
			Criteria cr = session.createCriteria(CnmtView.class);
			
			List<CnmtView> cnmtViewList = cr.list();
			
			for (CnmtView cnmtView : cnmtViewList) {
				System.out.print("cnmtId: "+cnmtView.getCnmtId()+"\t");
				System.out.println("cnmtCode: "+cnmtView.getCnmtCode());
			}
			
			/*SQLQuery sqlQuery = session.createSQLQuery("SELECT cnmtId as cnmtId, cnmtCode as cnmtCode From cnmt_code");*/
			/*sqlQuery.addEntity(CnmtView.class);*/
			
			/*List<Object> query = sqlQuery.list();
			
			for (Object rows: query) {
				Object[] row = (Object[]) rows;
			    System.out.println("cnmtView: "+row[0]+" "+row[1]);
			}*/
			
			/*Query query = session.getNamedQuery("implicitSample");
			
			List<CnmtView> cnmtViews = query.list();
			
			for (CnmtView cnmtView : cnmtViews) {
				System.out.print("cnmtId: "+cnmtView.getCnmtId()+"\t");
				System.out.println("cnmtCode: "+cnmtView.getCnmtCode());
			}*/
			
			 /*for(Object rows : query){
				    Object[] row = (Object[]) rows;
				    int field1 = (int) row[0]; // contains field1
				    String field2 = (String) row[1];
				    System.out.println("data from veie: "+field1+" :"+field2);
			 }*/
			
			/*System.out.println("cnmtViewList Size: "+cnmtViewList.size());
			
			for (CnmtView cnmtView : cnmtViewList) {
				System.out.print("cnmtId: "+ cnmtView.getCnmtId()+"\t");
				System.out.println("cnmtCode: "+ cnmtView.getCnmtCode());
			}*/
			
			session.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.close();
	}
	
	
	@SuppressWarnings("unchecked")
	public int saveFirstBCS(Map<String,Object> clientMap){
		System.out.println("enter into saveFirstBCS funciton");
		int res = 0;
		
		int brhId = (int) clientMap.get("brhId");
		int bnkId = (int) clientMap.get("bnkId");
		Date date = Date.valueOf((String) clientMap.get("csDt")); 
		double openBal = Double.parseDouble(String.valueOf(clientMap.get("openBal")));
		
		System.out.println("brhId = "+brhId);
		System.out.println("bnkId = "+bnkId);
		System.out.println("date = "+date);
		System.out.println("openBal = "+openBal);
		Session session=null;
		Transaction transaction=null;
		try{ 
			
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			
			Branch branch = (Branch) session.get(Branch.class,brhId);
			BankMstr bank = (BankMstr) session.get(BankMstr.class,bnkId);
			
			List<CashStmtStatus> cssList = new ArrayList<>();
			Criteria cr = session.createCriteria(CashStmtStatus.class);
			cr.add(Restrictions.eq(CashStmtStatusCNTS.B_Code,String.valueOf(brhId)));
			cr.addOrder(Order.asc(CashStmtStatusCNTS.CSS_ID));
			cssList = cr.list();
			
			System.out.println("size of cssList = "+cssList.size());
			for(int k=0;k<cssList.size();k++){
				System.out.println("************"+cssList.get(k).getCssId());
			}
			
			if(!cssList.isEmpty()){
				
				BankCS newBankCS = new BankCS();
				newBankCS.setbCode(cssList.get(0).getbCode());
				newBankCS.setUserCode(cssList.get(0).getUserCode());
				newBankCS.setBcsBankCode(bank.getBnkFaCode());
				newBankCS.setBcsBankId(bank.getBnkId());
				newBankCS.setBcsBrhId(brhId);
				newBankCS.setBcsDt(cssList.get(0).getCssDt());
				newBankCS.setBcsOpenBal(openBal);
				
				int bcsId = (Integer) session.save(newBankCS);  
				double balAmt = 0.0;
				
				for(int i=0;i<cssList.size()-1;i++){
					CashStmtStatus cashStmtStatus = cssList.get(i);
					List<CashStmt> csList = new ArrayList<>();
					csList = cashStmtStatus.getCashStmtList();
					System.out.println("size of csList = "+csList.size());
					double crAmt = 0.0;
					double drAmt = 0.0;
					double newAmt = 0.0;
					if(!csList.isEmpty()){
						
						for(int j=0;j<csList.size();j++){
							if(csList.get(j).getCsFaCode().equalsIgnoreCase(bank.getBnkFaCode())){
								if(csList.get(j).getCsDrCr() == 'C'){
									crAmt =  crAmt + csList.get(j).getCsAmt() ;
								}else{
									drAmt = drAmt + csList.get(j).getCsAmt() ;
								}
							}
						}
						
						
						newAmt = (openBal + drAmt) - crAmt;
						
						BankCS prevBCS = (BankCS) session.get(BankCS.class,bcsId);
						prevBCS.setBcsCloseBal(newAmt);
						
						openBal = newAmt;
						
						session.merge(prevBCS);
						
						BankCS newBankCS1 = new BankCS();
						newBankCS1.setbCode(cashStmtStatus.getbCode());
						newBankCS1.setUserCode(cashStmtStatus.getUserCode());
						newBankCS1.setBcsBankCode(bank.getBnkFaCode());
						newBankCS1.setBcsBankId(bank.getBnkId());
						newBankCS1.setBcsBrhId(brhId);
						newBankCS1.setBcsDt(cssList.get(i+1).getCssDt());
						newBankCS1.setBcsOpenBal(newAmt);
						
						bcsId = (Integer) session.save(newBankCS1);
						
						if(i == cssList.size()-2){
							balAmt = newAmt;
						}
						
					}else{
						newAmt = openBal;
						
						BankCS prevBCS = (BankCS) session.get(BankCS.class,bcsId);
						prevBCS.setBcsCloseBal(newAmt);
						
						openBal = newAmt;
						
						session.merge(prevBCS);
						
						BankCS newBankCS1 = new BankCS();
						newBankCS1.setbCode(cashStmtStatus.getbCode());
						newBankCS1.setUserCode(cashStmtStatus.getUserCode());
						newBankCS1.setBcsBankCode(bank.getBnkFaCode());
						newBankCS1.setBcsBankId(bank.getBnkId());
						newBankCS1.setBcsBrhId(brhId);
						newBankCS1.setBcsDt(cssList.get(i+1).getCssDt());
						newBankCS1.setBcsOpenBal(newAmt);
						
						bcsId = (Integer) session.save(newBankCS1);
						
						if(i == cssList.size()-2){
							balAmt = newAmt;
						}
					}
				}
				
				bank.setBnkBalanceAmt(balAmt);
				session.merge(bank);
				
				transaction.commit();
				res = 1;
			}
			
		}catch(Exception e){
			e.printStackTrace();
			transaction.rollback();
			logger.info("Exception in saveFirstBCS "+e);
			res = 0;
		}finally{
			session.clear();
			session.close();
		}
		return res;
	}

	@Override
	public Map<String, Object> getReconReport(ReconRptService reconRptService) {
		System.out.println("gerReconReportDB()");
		Map<String, Object> reconRptMap = new HashMap<>();
		Session session=null;
		try {
			session = this.sessionFactory.openSession();
			Criteria cr = session.createCriteria(CashStmtTemp.class);
			cr.addOrder(Order.asc(CashStmtTempCNTS.CS_DT));
			cr.add(Restrictions.eq(CashStmtTempCNTS.CS_IS_VIEW, false));
			cr.add(Restrictions.ge(CashStmtTempCNTS.CS_DT, reconRptService.getFrmDt()));
			cr.add(Restrictions.le(CashStmtTempCNTS.CS_DT, reconRptService.getToDt()));
			if (reconRptService.getFrmBrId()>0) {
				cr.add(Restrictions.eq(CashStmtTempCNTS.USER_BRANCH_CODE, String.valueOf(reconRptService.getFrmBrId())));
			}
			if (reconRptService.getToBrId() > 0) {
				int toBrFaCode = 100000 + reconRptService.getToBrId();
				cr.add(Restrictions.eq(CashStmtTempCNTS.CS_BRH_FA_CODE, "0"+String.valueOf(toBrFaCode)));
			}
			
			List<CashStmtTemp> cashStmtTempList = cr.list();
			
			Set<Integer> csIdSet = new HashSet<>();
			
			//gat cashStmt on the basis of cashStmtTemp
			if (!cashStmtTempList.isEmpty()) {
				for (CashStmtTemp cashStmtTemp : cashStmtTempList) {
					cr = session.createCriteria(CashStmt.class);
					cr.add(Restrictions.eq(CashStmtCNTS.CS_AMT, cashStmtTemp.getCsAmt()));
					cr.add(Restrictions.eq(CashStmtCNTS.CS_DT, cashStmtTemp.getCsDt()));
					cr.add(Restrictions.eq(CashStmtCNTS.USER_BRANCH_CODE, cashStmtTemp.getbCode()));//from branch
					cr.add(Restrictions.eq(CashStmtCNTS.CS_FA_CODE, cashStmtTemp.getCsBrhFaCode()));//to branch
					cr.add(Restrictions.eq(CashStmtCNTS.CS_DESCRIPTION, cashStmtTemp.getCsDescription()));
					cr.add(Restrictions.eq(CashStmtCNTS.CS_TYPE, cashStmtTemp.getCsType()));
					//cr.add(Restrictions.eq(CashStmtCNTS.CS_VOUCH_TYPE, cashStmtTemp.getCsVouchType()));
					
					System.out.println(CashStmtCNTS.CS_AMT+" "+ cashStmtTemp.getCsAmt()+"\t"
							+CashStmtCNTS.CS_DT+" "+ cashStmtTemp.getCsDt()+"\t"
							+CashStmtCNTS.USER_BRANCH_CODE+" "+cashStmtTemp.getbCode()+"\t"
							+CashStmtCNTS.CS_FA_CODE+" "+cashStmtTemp.getCsFaCode()+"\t"
							+CashStmtCNTS.CS_DESCRIPTION+" "+cashStmtTemp.getCsDescription()+"\t"
							+CashStmtCNTS.CS_TYPE+" "+cashStmtTemp.getCsType()+"\t"
							+CashStmtCNTS.CS_VOUCH_TYPE+" "+cashStmtTemp.getCsVouchType());
					
					List<CashStmt> cashStmtList = cr.list();
					System.out.println("cashStmtList from temp: "+cashStmtList.size());
					if (!cashStmtList.isEmpty()) {
						for (CashStmt cashStmt : cashStmtList) {
							csIdSet.add(cashStmt.getCsId());
						} 
					}
				}
			}
			
			List<CashStmt> cashStmtList = new ArrayList<>();
			
			if (!csIdSet.isEmpty()) {
				for (Integer csId : csIdSet) {
					CashStmt cashStmt = (CashStmt) session.get(CashStmt.class, csId);
					
					//add fromBrName
					Branch br = (Branch) session.get(Branch.class, Integer.parseInt(cashStmt.getbCode()));
					cashStmt.setFromBrName(br.getBranchName().replaceAll("\\s", ""));
					
					//add toBrName
					br = (Branch) session.get(Branch.class, Integer.parseInt(cashStmt.getCsFaCode().substring(2)));
					cashStmt.setToBrName(br.getBranchName().replaceAll("\\s", ""));
					
					//add toBr CSS close Date
					cr = session.createCriteria(CashStmtStatus.class);
					cr.addOrder(Order.desc(CashStmtStatusCNTS.CSS_DT));
					cr.add(Restrictions.eq(CashStmtStatusCNTS.B_Code, String.valueOf(Integer.parseInt(cashStmt.getCsFaCode().substring(2)))));
					cr.setFirstResult(0);
					cr.setMaxResults(1);
					
					if (!cr.list().isEmpty()) {
						CashStmtStatus cashStmtStatus = (CashStmtStatus) cr.list().get(0);
						cashStmt.setToBrCsCloseDt(cashStmtStatus.getCssDt());
					}
					
					if (cashStmt.getCsVouchType().equalsIgnoreCase("bank") && cashStmt.getCsFaCode().substring(0,2).equalsIgnoreCase("07")) {
						//ignore this entry
					} else {
						cashStmtList.add(cashStmt);
					}
				}
			}
			System.err.println("csIdSet: "+csIdSet.size());
			System.err.println("cashStmtList: "+cashStmtList.size());
			
			reconRptMap.put("cashStmtList", cashStmtList);
			
			System.out.println("frmDt: "+reconRptService.getFrmDt());
			System.out.println("toDt: "+reconRptService.getToDt());
			
			Query queryCS = session.createQuery("select csId from CashStmt where csDt between :frmDt and :toDt"); 
			queryCS.setDate("frmDt", reconRptService.getFrmDt());
			queryCS.setDate("toDt", reconRptService.getToDt());
			List<Integer> csIdListFromCS = queryCS.list();
			System.out.println("csIdListFromCS: "+csIdListFromCS);
			
			List<Integer> csIdListFromTempCS = new ArrayList<>();
			if (reconRptService.getToBrId() > 0 && reconRptService.getFrmBrId()>0) {
				Query queryTempCS = session.createQuery("select tibCsId from TempIntBrTv where tibBranchCode = :frmBrFaCode and tibFaCode = :toBrFaCode and tibIsClear = :isClear");
				queryTempCS.setString("frmBrFaCode", "0"+String.valueOf(100000+reconRptService.getFrmBrId()));
				queryTempCS.setString("toBrFaCode", "0"+String.valueOf(100000+reconRptService.getToBrId()));
				queryTempCS.setBoolean("isClear", false);
				csIdListFromTempCS = queryTempCS.list();
				System.out.println("csIdListFromTempCS before: "+csIdListFromTempCS);
			} else if (reconRptService.getToBrId() > 0){
				Query queryTempCS = session.createQuery("select tibCsId from TempIntBrTv where tibFaCode = :toBrFaCode and tibIsClear = :isClear");
				queryTempCS.setString("toBrFaCode", "0"+String.valueOf(100000+reconRptService.getToBrId()));
				queryTempCS.setBoolean("isClear", false);
				csIdListFromTempCS = queryTempCS.list();
				System.out.println("csIdListFromTempCS before: "+csIdListFromTempCS);
			} else if (reconRptService.getFrmBrId() > 0) {
				Query queryTempCS = session.createQuery("select tibCsId from TempIntBrTv where tibBranchCode = :frmBrFaCode and tibIsClear = :isClear");
				queryTempCS.setString("frmBrFaCode", "0"+String.valueOf(100000+reconRptService.getFrmBrId()));
				queryTempCS.setBoolean("isClear", false);
				csIdListFromTempCS = queryTempCS.list();
				System.out.println("csIdListFromTempCS before: "+csIdListFromTempCS);
			}else {
				Query queryTempCS = session.createQuery("select tibCsId from TempIntBrTv where tibIsClear = :isClear");
				queryTempCS.setBoolean("isClear", false);
				csIdListFromTempCS = queryTempCS.list();
				System.out.println("csIdListFromTempCS before: "+csIdListFromTempCS);
			}
			
			csIdListFromTempCS.retainAll(csIdListFromCS);
			System.out.println("csIdListFromTempCS after: "+csIdListFromTempCS);
			
			List<TempIntBrTv> tempIntBrTvList = new ArrayList<>();
			if (!csIdListFromTempCS.isEmpty()) {
				for (Integer tibCsId : csIdListFromTempCS) {
					cr = session.createCriteria(TempIntBrTv.class);
					cr.add(Restrictions.eq(TempIntBrTvCNTS.TIBT_CS_ID, tibCsId));
					if (!cr.list().isEmpty()) {
						TempIntBrTv tempIntBrTv = (TempIntBrTv) cr.list().get(0);
						//add csDt to tempIntBrTv
						CashStmt cs = (CashStmt) session.get(CashStmt.class, tempIntBrTv.getTibCsId());
						tempIntBrTv.setTibDate(cs.getCsDt());
						
						//add drBranch Name
						Branch br = (Branch) session.get(Branch.class, Integer.parseInt(tempIntBrTv.getTibBranchCode().substring(2)));
						tempIntBrTv.setTibDrBrName(br.getBranchName());
						
						//add crBranch Name
						br = (Branch) session.get(Branch.class, Integer.parseInt(tempIntBrTv.getTibFaCode().substring(2)));
						tempIntBrTv.setTibCrBrName(br.getBranchName());
						
						tempIntBrTvList.add(tempIntBrTv);
					}
				}
			}
			
			reconRptMap.put("tempIntBrTvList", tempIntBrTvList);
			reconRptMap.put("temp", 1);
			session.flush();
		} catch (Exception e) {
			e.printStackTrace();
			reconRptMap.put("temp", -1);
			logger.info("Exception in getReconReport "+e);
		}finally{
			session.clear();
			session.close();		
		}
		
		return reconRptMap;
	}

	@Override
	public List<CashStmt> getCsListFrmCsTemp(CashStmtTemp cashStmtTemp) {
		System.out.println("ReportDAOImpl.getCsListFrmCsTemp()");
		List<CashStmt> cashStmtList = new ArrayList<>();
		Session session=null;
		try {
			session = this.sessionFactory.openSession();
			Criteria cr = session.createCriteria(CashStmt.class);
			cr.add(Restrictions.eq(CashStmtCNTS.CS_AMT, cashStmtTemp.getCsAmt()));
			cr.add(Restrictions.eq(CashStmtCNTS.CS_CDT, cashStmtTemp.getCsDt()));
			cr.add(Restrictions.eq(CashStmtCNTS.CS_DR_CR, 'D'));
			cr.add(Restrictions.eq(CashStmtCNTS.CS_DESCRIPTION, cashStmtTemp.getCsDescription()));
			cr.add(Restrictions.eq(CashStmtCNTS.CS_TYPE, cashStmtTemp.getCsType()));
			cr.add(Restrictions.eq(CashStmtCNTS.CS_VOUCH_TYPE, cashStmtTemp.getCsVouchType()));
			cr.add(Restrictions.eq(CashStmtCNTS.USER_BRANCH_CODE, cashStmtTemp.getbCode()));
			cr.add(Restrictions.eq(CashStmtCNTS.CS_FA_CODE, cashStmtTemp.getCsBrhFaCode()));
			cr.add(Restrictions.eq(CashStmtCNTS.CS_CTS, cashStmtTemp.getCreationTS()));
			
			cashStmtList = cr.list();
			
			System.out.println("cashStmtListSize: "+cashStmtList.size());
			
			session.flush();
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Exception in getCsListFrmCsTemp "+e);
		
		}finally{
			session.clear();
			session.close();
		}
		return cashStmtList;
	}

	//TODO OS and Stk rpt
	@Override
	public List<Map<String, Object>> getStockAndOsRpt(Map<String, Object> stockAndOsRptService) {
		System.out.println("ReportDAOImpl.getStockAndOsRpt()");
		List<Map<String, Object>> stockAndOsRptList = new ArrayList<>();
		
		Integer branchId = (Integer) stockAndOsRptService.get("branchId");
		Integer custId = (Integer) stockAndOsRptService.get("custId");
		Date upToDt = CodePatternService.getSqlDate(String.valueOf(stockAndOsRptService.get("upToDt")));
		
		System.out.println("branch: "+branchId);
		System.out.println("cust: "+custId);
		System.out.println("cust: "+upToDt);
		
		List<Customer> custList = new ArrayList<>();
		List<Bill> billListTemp = new ArrayList<>();
		List<Bill> billList = new ArrayList<>();
		List<Cnmt> cnmtListTemp = new ArrayList<>();
		List<Cnmt> cnmtList = new ArrayList<>();
		
		//for on account
		List<MoneyReceipt> mrList = new ArrayList<>();
		List<MoneyReceipt> mrListTemp = new ArrayList<>();
		Map<String,Double> mapBillId=new HashMap<String,Double>();
		Session session=null;
		try {
			session = this.sessionFactory.openSession();
			
			if (custId == null && branchId == null) {
				//extract bill
				/*Criteria cr = session.createCriteria(Bill.class);
				cr.add(Restrictions.le(BillCNTS.BILL_DT, upToDt));
				billListTemp = cr.list();*/
				
				Query query = session.createQuery("SELECT bill FROM Bill bill " +
						"WHERE bill.blBillDt <= :billDt and bill.blFinalTot = bill.blRemAmt and bill.blCancel = :blCancel");
				query.setDate("billDt", upToDt);
				query.setBoolean("blCancel", false);
				billList = query.list();
				
				Query queryTemp = session.createQuery("SELECT bill FROM Bill bill " +
						"WHERE bill.blBillDt <= :billDt and bill.blFinalTot > bill.blRemAmt and bill.blCancel = :blCancel");
				queryTemp.setDate("billDt", upToDt);
				queryTemp.setBoolean("blCancel", false);
				billListTemp = queryTemp.list();
				
				//extract customer
				Criteria custCriteria = session.createCriteria(Customer.class);
				custList = custCriteria.list();
				
				//extract cnmt
				Criteria cnmtCriteria = session.createCriteria(Cnmt.class);
				cnmtCriteria.add(Restrictions.le(CnmtCNTS.CNMT_DT, upToDt));
				cnmtCriteria.add(Restrictions.eq(CnmtCNTS.CNMT_CANCEL, false));
				cnmtListTemp = cnmtCriteria.list();
				
				//extract onAccount Mr
				Query mrQuery = session.createQuery("SELECT mr FROM MoneyReceipt mr " +
						"WHERE mr.mrType = :mrType and mr.mrDate <= :mrDate and mr.mrNetAmt = mr.mrRemAmt and mr.mrCancel = :mrCancel");
				mrQuery.setString(MoneyReceiptCNTS.MR_TYPE, "On Accounting");
				mrQuery.setDate(MoneyReceiptCNTS.MR_Date, upToDt);
				mrQuery.setBoolean("mrCancel", false);
				mrList = mrQuery.list();
				
				Query mrQueryTemp = session.createQuery("SELECT mr FROM MoneyReceipt mr " +
						"WHERE mr.mrType = :mrType and mr.mrDate <= :mrDate and mr.mrNetAmt > mr.mrRemAmt and mr.mrCancel = :mrCancel");
				mrQueryTemp.setString(MoneyReceiptCNTS.MR_TYPE, "On Accounting");
				mrQueryTemp.setDate(MoneyReceiptCNTS.MR_Date, upToDt);
				mrQueryTemp.setBoolean("mrCancel", false);
				mrListTemp = mrQueryTemp.list();
				
			} else if (custId == null) {//branch wise
				//extract bill
				/*Criteria cr = session.createCriteria(Bill.class);
				cr.add(Restrictions.eq(BillCNTS.USER_BRANCH_CODE, String.valueOf(branchId)));
				cr.add(Restrictions.le(BillCNTS.BILL_DT, upToDt));
				billListTemp = cr.list();*/
				
				//extract customer
				Criteria custCriteria = session.createCriteria(Customer.class);
				custCriteria.add(Restrictions.eq(CustomerCNTS.BRANCH_CODE, String.valueOf(branchId)));
				custList = custCriteria.list();
				
				//extract cnmt
				Criteria cnmtCriteria = session.createCriteria(Cnmt.class);
				cnmtCriteria.add(Restrictions.eq(CnmtCNTS.USER_BRANCH_CODE, String.valueOf(branchId)));
				cnmtCriteria.add(Restrictions.le(CnmtCNTS.CNMT_DT, upToDt));
				cnmtCriteria.add(Restrictions.eq(CnmtCNTS.CNMT_CANCEL,false));
				cnmtListTemp = cnmtCriteria.list();
				
				
				for (Customer cust : custList) {
					
					//extract bill
					Query query = session.createQuery("SELECT bill FROM Bill bill " +
							"WHERE bill.blBillDt <= :billDt and bill.blFinalTot = bill.blRemAmt and bill.blCustId = :blCustId and bill.blCancel = :blCancel");
					query.setDate("billDt", upToDt);
					query.setInteger("blCustId", cust.getCustId());
					query.setBoolean("blCancel", false);
					
					List<Bill> blList = new ArrayList<>();
					blList = query.list();
					
					if (!blList.isEmpty()) {
						billList.addAll(blList);
					}
					
					Query queryTemp = session.createQuery("SELECT bill FROM Bill bill " +
							"WHERE bill.blBillDt <= :billDt and bill.blFinalTot > bill.blRemAmt and bill.blCustId = :blCustId and bill.blCancel = :blCancel");
					queryTemp.setDate("billDt", upToDt);
					queryTemp.setInteger("blCustId", cust.getCustId());
					queryTemp.setBoolean("blCancel", false);
					
					List<Bill> blListTemp = queryTemp.list(); 
					
					if (!blListTemp.isEmpty()) {
						billListTemp.addAll(blListTemp);
					}
					 
					
					//extract onAccount Mr
					Query mrQuery = session.createQuery("SELECT mr FROM MoneyReceipt mr " +
							"WHERE mr.mrType = :mrType and mr.mrDate <= :mrDate and mr.mrNetAmt = mr.mrRemAmt and " +
							" mr.mrCustId = :mrCustId and mr.mrCancel = :mrCancel");
					mrQuery.setString(MoneyReceiptCNTS.MR_TYPE, "On Accounting");
					mrQuery.setDate(MoneyReceiptCNTS.MR_Date, upToDt);
					mrQuery.setInteger(MoneyReceiptCNTS.MR_CUST_ID, cust.getCustId());
					mrQuery.setBoolean("mrCancel", false);
					
					List<MoneyReceipt> moneyReceiptList = new ArrayList<>();
					moneyReceiptList = mrQuery.list();
					if (!moneyReceiptList.isEmpty()) {
						mrList.addAll(moneyReceiptList);
					}
					
					Query mrQueryTemp = session.createQuery("SELECT mr FROM MoneyReceipt mr " +
							"WHERE mr.mrType = :mrType and mr.mrDate <= :mrDate and mr.mrNetAmt > mr.mrRemAmt and " +
							" mr.mrCustId = :mrCustId and mr.mrCancel = :mrCancel");
					mrQueryTemp.setString(MoneyReceiptCNTS.MR_TYPE, "On Accounting");
					mrQueryTemp.setDate(MoneyReceiptCNTS.MR_Date, upToDt);
					mrQueryTemp.setInteger(MoneyReceiptCNTS.MR_CUST_ID, cust.getCustId());
					mrQueryTemp.setBoolean("mrCancel", false);
					
					List<MoneyReceipt> moneyReceiptListTemp = new ArrayList<>();
					moneyReceiptListTemp = mrQueryTemp.list();
					if (!moneyReceiptListTemp.isEmpty()) {
						mrListTemp.addAll(moneyReceiptListTemp);
					}
				}
				
			} else if (branchId == null) {//cust wise
				//extract bill
				/*Criteria cr = session.createCriteria(Bill.class);
				cr.add(Restrictions.eq(BillCNTS.BILL_CUST_ID, custId));
				cr.add(Restrictions.le(BillCNTS.BILL_DT, upToDt));
				billListTemp = cr.list();*/
				
				//extract bill
				Query query = session.createQuery("SELECT bill FROM Bill bill " +
						"WHERE bill.blBillDt <= :billDt and bill.blFinalTot = bill.blRemAmt and blCustId = :custId and bill.blCancel = :blCancel");
				query.setDate("billDt", upToDt);
				query.setInteger("custId", custId);
				query.setBoolean("blCancel", false);
				billList = query.list();
				
				Query queryTemp = session.createQuery("SELECT bill FROM Bill bill " +
						"WHERE bill.blBillDt <= :billDt and bill.blFinalTot > bill.blRemAmt and blCustId = :custId and bill.blCancel = :blCancel");
				queryTemp.setDate("billDt", upToDt);
				queryTemp.setInteger("custId", custId);
				queryTemp.setBoolean("blCancel", false);
				billListTemp = queryTemp.list();
				
				//extract customer
				Customer customer = (Customer) session.get(Customer.class, custId);
				custList.add(customer);
				
				//extract cnmt
				Criteria cnmtCriteria = session.createCriteria(Cnmt.class);
				cnmtCriteria.add(Restrictions.eq(CnmtCNTS.CUST_CODE, String.valueOf(custId)));
				cnmtCriteria.add(Restrictions.le(CnmtCNTS.CNMT_DT, upToDt));
				cnmtCriteria.add(Restrictions.eq(CnmtCNTS.CNMT_CANCEL,false));
						cnmtListTemp = cnmtCriteria.list();
				
				//extract onAccount Mr
				Query mrQuery = session.createQuery("SELECT mr FROM MoneyReceipt mr " +
						"WHERE mr.mrType = :mrType and mr.mrDate <= :mrDate and mr.mrNetAmt = mr.mrRemAmt and " +
						" mr.mrCustId = :mrCustId and mr.mrCancel = :mrCancel");
				mrQuery.setString(MoneyReceiptCNTS.MR_TYPE, "On Accounting");
				mrQuery.setDate(MoneyReceiptCNTS.MR_Date, upToDt);
				mrQuery.setInteger(MoneyReceiptCNTS.MR_CUST_ID, custId);
				mrQuery.setBoolean("mrCancel", false);
				mrList = mrQuery.list();
				
				Query mrQueryTemp = session.createQuery("SELECT mr FROM MoneyReceipt mr " +
						"WHERE mr.mrType = :mrType and mr.mrDate <= :mrDate and mr.mrNetAmt > mr.mrRemAmt and " +
						" mr.mrCustId = :mrCustId and mr.mrCancel = :mrCancel");
				mrQueryTemp.setString(MoneyReceiptCNTS.MR_TYPE, "On Accounting");
				mrQueryTemp.setDate(MoneyReceiptCNTS.MR_Date, upToDt);
				mrQueryTemp.setInteger(MoneyReceiptCNTS.MR_CUST_ID, custId);
				mrQueryTemp.setBoolean("mrCancel", false);
				mrListTemp = mrQueryTemp.list();
			} else {
				//extract bill
				/*Criteria cr = session.createCriteria(Bill.class);
				cr.add(Restrictions.eq(BillCNTS.USER_BRANCH_CODE, String.valueOf(branchId)));
				cr.add(Restrictions.eq(BillCNTS.BILL_CUST_ID, custId));
				cr.add(Restrictions.le(BillCNTS.BILL_DT, upToDt));
				billListTemp = cr.list();*/
				
				//extract bill
				Query query = session.createQuery("SELECT bill FROM Bill bill " +
						"WHERE bill.blBillDt <= :billDt and bill.blFinalTot = bill.blRemAmt and blCustId = :custId and bCode = :branchId " +
						"and bill.blCancel = :blCancel");
				query.setDate("billDt", upToDt);
				query.setInteger("custId", custId);
				query.setString("branchId", String.valueOf(branchId));
				query.setBoolean("blCancel", false);
				billList = query.list();
				
				Query queryTemp = session.createQuery("SELECT bill FROM Bill bill " +
						"WHERE bill.blBillDt <= :billDt and bill.blFinalTot > bill.blRemAmt and blCustId = :custId and bCode = :branchId " +
						"and bill.blCancel = :blCancel");
				queryTemp.setDate("billDt", upToDt);
				queryTemp.setInteger("custId", custId);
				queryTemp.setString("branchId", String.valueOf(branchId));
				queryTemp.setBoolean("blCancel", false);
				billListTemp = queryTemp.list();
				
				//extract customer
				Customer customer = (Customer) session.get(Customer.class, custId);
				custList.add(customer);
				
				//extract cnmt
				Criteria cnmtCriteria = session.createCriteria(Cnmt.class);
				cnmtCriteria.add(Restrictions.eq(CnmtCNTS.CUST_CODE, String.valueOf(custId)));
				cnmtCriteria.add(Restrictions.eq(CnmtCNTS.USER_BRANCH_CODE, String.valueOf(branchId)));
				cnmtCriteria.add(Restrictions.le(CnmtCNTS.CNMT_DT, upToDt));
				cnmtCriteria.add(Restrictions.eq(CnmtCNTS.CNMT_CANCEL,false));
				cnmtListTemp = cnmtCriteria.list();
				
				//extract onAccount Mr
				Query mrQuery = session.createQuery("SELECT mr FROM MoneyReceipt mr " +
						"WHERE mr.mrType = :mrType and mr.mrDate <= :mrDate and mr.mrNetAmt = mr.mrRemAmt and " +
						" mr.mrCustId = :mrCustId and mr.mrBrhId = :mrBrhId and mr.mrCancel = :mrCancel");
				mrQuery.setString(MoneyReceiptCNTS.MR_TYPE, "On Accounting");
				mrQuery.setDate(MoneyReceiptCNTS.MR_Date, upToDt);
				mrQuery.setInteger(MoneyReceiptCNTS.MR_CUST_ID, custId);
				mrQuery.setInteger(MoneyReceiptCNTS.MR_BRH_ID, branchId);
				mrQuery.setBoolean("mrCancel", false);
				mrList = mrQuery.list();
				
				Query mrQueryTemp = session.createQuery("SELECT mr FROM MoneyReceipt mr " +
						"WHERE mr.mrType = :mrType and mr.mrDate <= :mrDate and mr.mrNetAmt > mr.mrRemAmt and " +
						" mr.mrCustId = :mrCustId and mr.mrBrhId = :mrBrhId and mr.mrCancel = :mrCancel");
				mrQueryTemp.setString(MoneyReceiptCNTS.MR_TYPE, "On Accounting");
				mrQueryTemp.setDate(MoneyReceiptCNTS.MR_Date, upToDt);
				mrQueryTemp.setInteger(MoneyReceiptCNTS.MR_CUST_ID, custId);
				mrQueryTemp.setInteger(MoneyReceiptCNTS.MR_BRH_ID, branchId);
				mrQueryTemp.setBoolean("mrCancel", false);
				mrListTemp = mrQueryTemp.list();
			}
			
			
			logger.info("CNMT : "+cnmtList.size() + " : CnmtTemp = "+cnmtListTemp.size() + " : Cust = "+custList.size() + " : Bill = "+billList.size()+" : BillTemp = "+billListTemp.size()+" : MR = "+mrList.size()+" : MRTemp = "+mrListTemp.size());
			
			System.out.println("==========================BILL LIST=======================");
			for (Bill bill : billList) {
				System.out.print("billId: "+bill.getBlId()+ "\t");
				System.out.print("billRemAmt: "+bill.getBlRemAmt()+ "\t");
				System.out.println("bill.getBlFinalTot: "+bill.getBlFinalTot());
			}
			
			System.out.println("==========================BILL LIST TEMP=======================");
			for (Bill bill : billListTemp) {
				System.out.print("billId: "+bill.getBlId()+ "\t");
				System.out.print("billRemAmt: "+bill.getBlRemAmt()+ "\t");
				System.out.println("bill.getBlFinalTot: "+bill.getBlFinalTot());
			}
			
			if (!mrList.isEmpty()) {
				for (MoneyReceipt moneyReceipt : mrList) {
					if (moneyReceipt.getMrOthMrAmt() > 0) {
						moneyReceipt.setMrNetAmtForRpt(moneyReceipt.getMrNetAmt() - moneyReceipt.getMrOthMrAmt());
					} else {
						moneyReceipt.setMrNetAmtForRpt(moneyReceipt.getMrNetAmt());
					}
				}
			}
			
			if (!mrListTemp.isEmpty()) {
				for (MoneyReceipt moneyReceipt : mrListTemp) {
					if (moneyReceipt.getMrOthMrAmt() > 0) {
						moneyReceipt.setMrNetAmtForRpt(moneyReceipt.getMrNetAmt() - moneyReceipt.getMrOthMrAmt());
					} else {
						moneyReceipt.setMrNetAmtForRpt(moneyReceipt.getMrNetAmt());
					}
				}
			}
			//mr for on account
			if (!mrListTemp.isEmpty()) {
				for (MoneyReceipt mr : mrListTemp) {
					List<Map<String, Object>> pdList = new ArrayList<>();
					pdList = mr.getMrFrPDList();
					System.out.println("On Acc MrNo = "+mr.getMrNo());
					System.out.println("On Acc MrDt = "+mr.getMrDate());
					System.out.println("On Acc mrAmt = "+mr.getMrNetAmt());
					System.out.println("mrFrPdList==========>>: "+pdList.size());
					if (!pdList.isEmpty()) {
						for (Map<String, Object> pd : pdList) {
							System.out.println("mrFrPd: "+pd.entrySet());
							Criteria mrCriteria = session.createCriteria(MoneyReceipt.class);
							//mrCriteria.add(Restrictions.eq(MoneyReceiptCNTS.MR_CUST_ID, mr.getMrCustId()));
							if (pd.get("brhId") == null) {
								mrCriteria.add(Restrictions.eq(MoneyReceiptCNTS.MR_BRH_ID, mr.getMrBrhId()));
							} else {
								mrCriteria.add(Restrictions.eq(MoneyReceiptCNTS.MR_BRH_ID, Integer.parseInt(String.valueOf(pd.get("brhId")))));
							}
							mrCriteria.add(Restrictions.eq(MoneyReceiptCNTS.MR_NO, String.valueOf(pd.get("pdMr"))));
							mrCriteria.add(Restrictions.eq(MoneyReceiptCNTS.MR_TYPE, "Payment Detail"));
							System.out.println("mrPdListSize: "+mrCriteria.list().size());
							
							List<MoneyReceipt> mrPdList = new ArrayList<>();
							mrPdList = mrCriteria.list(); 

							if (!mrPdList.isEmpty()) {
								MoneyReceipt mrPd = (MoneyReceipt) mrPdList.get(0);
								if (mrPd.getMrDate().compareTo(upToDt) <= 0) {
									mr.setMrNetAmtForRpt(mr.getMrNetAmtForRpt() - Double.parseDouble(String.valueOf(pd.get("detAmt"))));
								}
							}
						}
					}
				}
			}
			
			if (!mrListTemp.isEmpty()) {
				for (MoneyReceipt moneyReceipt : mrListTemp) {
					if (moneyReceipt.getMrNetAmtForRpt() > 0) {
						mrList.add(moneyReceipt);
					}
				}
			}
			
			//get bill whose mr not prepared according to given condition
//			if (!billListTemp.isEmpty()) {
//				for (Bill bill : billListTemp) {
//					//bill.setBlFinalTotTemp(bill.getBlFinalTot());
//					SQLQuery sqlQuery = session.createSQLQuery("SELECT mrId FROM mr_bill WHERE blId = :blId");
//					sqlQuery.setInteger("blId", bill.getBlId());
//					List<Integer> mrIdList = sqlQuery.list();
					
//					/*if (!mrIdList.isEmpty()) {
//						for (Integer mrId : mrIdList) {
//							MoneyReceipt moneyReceipt = (MoneyReceipt) session.get(MoneyReceipt.class, mrId);
//							if (moneyReceipt.getMrDate() != null && moneyReceipt.getMrDate().compareTo(upToDt)>0) {
//								Hibernate.initialize(bill.getBillDetList());
//								billList.add(bill);
//							} else if (moneyReceipt.getMrDate() != null && moneyReceipt.getMrDate().compareTo(upToDt)<=0) {
//								bill.setBlFinalTotTemp(bill.getBlFinalTotTemp() - moneyReceipt.getMrFreight());
//								Hibernate.initialize(bill.getBillDetList());
//								billList.add(bill);
//							}
//							
//						}
//					}*/
					
//					if (!mrIdList.isEmpty()) {
//						for (Integer mrId : mrIdList) {
//							MoneyReceipt moneyReceipt = (MoneyReceipt) session.get(MoneyReceipt.class, mrId);
//							if (moneyReceipt.getMrDate() != null && moneyReceipt.getMrDate().compareTo(upToDt)>0) {
//								Hibernate.initialize(bill.getBillDetList());
//								billList.add(bill);
//								break;
//							}
//						}
//					}
//				}
//			}
			
			billList.addAll(billListTemp);
			
			//TODO remove part bill here
			if (!billList.isEmpty()) {
				for (Bill bill : billList) {
					bill.setBlFinalTotTemp(bill.getBlFinalTot());
					SQLQuery sqlQuery = session.createSQLQuery("SELECT mrId FROM mr_bill WHERE blId = :blId");
					sqlQuery.setInteger("blId", bill.getBlId());
					List<Integer> mrIdList = sqlQuery.list();
					
					if (!mrIdList.isEmpty()) {
						for (Integer mrId : mrIdList) {
							MoneyReceipt moneyReceipt = (MoneyReceipt) session.get(MoneyReceipt.class, mrId);
							/*System.err.print("##mrDate: "+moneyReceipt.getMrDate()+"\t");
							System.err.print("##mrId: "+moneyReceipt.getMrId()+"\t");
							System.err.print("##mrFreight: "+moneyReceipt.getMrFreight()+"\t");
							System.err.print("##BlDate: "+bill.getBlBillDt()+"\t");
							System.err.println("##BlFinalTotTemp: "+bill.getBlFinalTotTemp());*/
								
							
							if (moneyReceipt.getMrDate() != null && moneyReceipt.getMrDate().compareTo(upToDt)<=0) {
								System.err.print("mrDt: "+moneyReceipt.getMrDate());
								System.err.print("mrId: "+moneyReceipt.getMrId()+"\t");
								System.err.print("mrFreight: "+moneyReceipt.getMrFreight()+"\t");
								System.err.print("blDt: "+upToDt);
								System.err.print("blId: "+bill.getBlId()+"\t");
								System.err.print("BlFinalTot: "+bill.getBlFinalTot()+"\t");
								bill.setBlFinalTotTemp(bill.getBlFinalTotTemp() - moneyReceipt.getMrFreight());
								if(bill.getBlFinalTotTemp()<1){
									bill.setBlFinalTotTemp(0.0);
								}
									
								System.err.println("BlFinalTotTemp: "+bill.getBlFinalTotTemp());
							}
						}
					}
				}
			}
			
			for (Bill bill : billList) {
				System.out.println("blId: "+bill.getBlId());
			}
			
			System.out.println("==================Detail====================");
			double tempBlAmt = 0;
			for (Bill bill : billList) {
				System.out.print("blId: "+bill.getBlId()+"\t");
				System.out.print("blFinalTot: "+bill.getBlFinalTot()+"\t");
				System.out.println("blFinalTotTemp: "+bill.getBlFinalTotTemp());
				tempBlAmt += bill.getBlFinalTotTemp();
			}
			System.out.println("=======ARIF========== "+tempBlAmt);
			/*session.clear();
			session.close();*/
			//sort bill according to custId
			Collections.sort(billList, new Comparator<Bill>() {
				@Override
				public int compare(Bill b1, Bill b2) {
					if (b1.getBlCustId() > b2.getBlCustId()) {
						return 1;
					} else if (b1.getBlCustId() < b2.getBlCustId()) {
						return -1;
					} else {
						return 0;
					}
				}
			});
			
			//get cnmt whose bill not prepared according to the condition i.e. stock
			if (!cnmtListTemp.isEmpty()) {
				/*session = this.sessionFactory.openSession();*/
				for (Cnmt cnmt : cnmtListTemp) {
					if (cnmt.getCnmtBillNo() != null) {
						Criteria billCriteria = session.createCriteria(Bill.class);
						//TODO change criteria when bill no become uniques
						billCriteria.add(Restrictions.eq(BillCNTS.Bill_NO, cnmt.getCnmtBillNo()));
						billCriteria.add(Restrictions.eq(BillCNTS.BILL_CUST_ID, Integer.parseInt(cnmt.getCustCode())));
						
						if (!billCriteria.list().isEmpty()) {
							Bill bill = (Bill) billCriteria.list().get(0);
							if (bill.getBlBillDt().compareTo(upToDt) > 0) {
								cnmtList.add(cnmt);
							}
						}
					}else {
						cnmtList.add(cnmt);
					}
				}
				/*session.clear();
				session.close();*/
			}
			
			session.clear();
			session.close();
			
			
			for (Bill bl : billList) {
				System.out.println("blId: "+bl.getBlId());
				System.out.println("BlFinalTot: "+bl.getBlFinalTot());
				System.out.println("BlFinalTotTemp: "+bl.getBlFinalTotTemp());
			}
			
			
			if (!custList.isEmpty()) {
				for (Customer cust: custList) {
					Map<String, Object> custMap = new HashMap<>();
					
					custMap.put("custId", cust.getCustId());
					custMap.put("custName", cust.getCustName());
					
					//put cust branch code
					if (cust.getBranchCode().length() < 2) {
						custMap.put("custFaCode", "0"+cust.getBranchCode()+"-"+cust.getCustFaCode());
					} else {
						custMap.put("custFaCode", cust.getBranchCode()+"-"+cust.getCustFaCode());
					}
					
					double custOsTotal = 0.0;
					double custStkTotal = 0.0;
					
					if (!billList.isEmpty()) {
						for (Bill bill : billList) {
							if (bill.getBlCustId() == cust.getCustId()) {
								
								// calculate date difference
								long dateDiff = upToDt.getTime() - bill.getBlBillDt().getTime();
								dateDiff = dateDiff/(24 * 60 * 60 * 1000);
																
								if (dateDiff<=30) {
									if (custMap.get("OS0_30") == null) {
										custMap.put("OS0_30", bill.getBlFinalTotTemp());
									} else {
										custMap.put("OS0_30", Double.parseDouble(String.valueOf(custMap.get("OS0_30")))+bill.getBlFinalTotTemp());
									}
									custOsTotal += bill.getBlFinalTotTemp();
								} else if (dateDiff>30 && dateDiff<=60) {
									if (custMap.get("OS31_60") == null) {
										custMap.put("OS31_60", bill.getBlFinalTotTemp());
									} else {
										custMap.put("OS31_60", Double.parseDouble(String.valueOf(custMap.get("OS31_60")))+bill.getBlFinalTotTemp());
									}
									custOsTotal += bill.getBlFinalTotTemp();
								} else if (dateDiff>60 && dateDiff<=90) {
									if (custMap.get("OS61_90") == null) {
										custMap.put("OS61_90", bill.getBlFinalTotTemp());
									} else {
										custMap.put("OS61_90", Double.parseDouble(String.valueOf(custMap.get("OS61_90")))+bill.getBlFinalTotTemp());
									}
									custOsTotal += bill.getBlFinalTotTemp();
								} else if (dateDiff>90 && dateDiff<=120) {
									if (custMap.get("OS91_120") == null) {
										custMap.put("OS91_120", bill.getBlFinalTotTemp());
									} else {
										custMap.put("OS91_120", Double.parseDouble(String.valueOf(custMap.get("OS91_120")))+bill.getBlFinalTotTemp());
									}
									custOsTotal += bill.getBlFinalTotTemp();
								} else if (dateDiff>120) {
									if (custMap.get("OS121_more") == null) {
										custMap.put("OS121_more", bill.getBlFinalTotTemp());
									} else {
										custMap.put("OS121_more", Double.parseDouble(String.valueOf(custMap.get("OS121_more")))+bill.getBlFinalTotTemp());
									}
									custOsTotal += bill.getBlFinalTotTemp();
									mapBillId.put(bill.getBlBillNo(),bill.getBlFinalTotTemp());
								}
								custMap.put("OSTotal", custOsTotal);
							}
						}
					}
					
					//add stock
					if (!cnmtList.isEmpty()) {
						for (Cnmt cnmt : cnmtList) {
							if (cust.getCustId() == Integer.parseInt(cnmt.getCustCode())) {
								long dateDiff = upToDt.getTime() - cnmt.getCnmtDt().getTime();
								dateDiff = dateDiff/(24*60*60*1000);
								if (dateDiff<=30) {
									if (custMap.get("stkUpTo30") == null) {
										custMap.put("stkUpTo30", cnmt.getCnmtFreight());
									} else {
										custMap.put("stkUpTo30", Double.parseDouble(String.valueOf(custMap.get("stkUpTo30")))+cnmt.getCnmtFreight());
									}
									custStkTotal += cnmt.getCnmtFreight();
								}
								if (dateDiff>30) {
									if (custMap.get("stkAbove30") == null) {
										custMap.put("stkAbove30", cnmt.getCnmtFreight());
									} else {
										custMap.put("stkAbove30", Double.parseDouble(String.valueOf(custMap.get("stkAbove30")))+cnmt.getCnmtFreight());
									}
									custStkTotal += cnmt.getCnmtFreight();
								}
							}
							custMap.put("stkTotal", custStkTotal);
						}
					}
					
					//add onAcc here
					/*if (custMap.get("custId") != null) {
						double mrNetAmtForRpt = 0.0;
						for (MoneyReceipt mrOnAcc : mrList) {
							if (mrOnAcc.getMrCustId() == Integer.parseInt(String.valueOf(custMap.get("custId")))) {
								mrNetAmtForRpt += mrOnAcc.getMrNetAmtForRpt();
							}
						}
						
						custMap.put("onAC", mrNetAmtForRpt);
						if (custMap.get("OSTotal") != null) {
							custMap.put("OSTotal", Double.parseDouble(String.valueOf(custMap.get("OSTotal"))) - mrNetAmtForRpt);
						}
						
						stockAndOsRptList.add(custMap);
					}*/
					
					
					double mrNetAmtForRpt = 0.0;
					for (MoneyReceipt mrOnAcc : mrList) {
						if (mrOnAcc.getMrCustId() == cust.getCustId()) {
							mrNetAmtForRpt += mrOnAcc.getMrNetAmtForRpt();
						}
					}
						
					custMap.put("onAC", mrNetAmtForRpt);
					if (custMap.get("OSTotal") == null) {
						custMap.put("OSTotal", 0.0 - mrNetAmtForRpt);
					} else {
						custMap.put("OSTotal", Double.parseDouble(String.valueOf(custMap.get("OSTotal"))) - mrNetAmtForRpt);
					}
					
					if ((custMap.get("onAC") != null && Double.parseDouble(String.valueOf(custMap.get("onAC"))) > 0)
							|| (custMap.get("OSTotal") != null && Double.parseDouble(String.valueOf(custMap.get("OSTotal"))) > 0)
							|| (custMap.get("stkTotal") != null && Double.parseDouble(String.valueOf(custMap.get("stkTotal"))) > 0)) {

						stockAndOsRptList.add(custMap);
					} else if ((custMap.get("onAC") != null && Double.parseDouble(String.valueOf(custMap.get("onAC"))) < 0)
							|| (custMap.get("OSTotal") != null && Double.parseDouble(String.valueOf(custMap.get("OSTotal"))) < 0)
							|| (custMap.get("stkTotal") != null && Double.parseDouble(String.valueOf(custMap.get("stkTotal"))) < 0)) {
						
						stockAndOsRptList.add(custMap);
					}
					
					/*if ((custMap.get("onAC") != null) || (custMap.get("OSTotal") != null) || (custMap.get("stkTotal") != null)) {
						stockAndOsRptList.add(custMap);
					}*/
				}
			}
			/*session.clear();
			session.close();*/
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Exception in getStockAndOsRpt "+e);
		} finally {
			if (session.isOpen()) {
				session.clear();
				session.close();
			}
		}
		System.out.println("billid size="+mapBillId.size());
		  Set<Entry<String, Double>> set=mapBillId.entrySet();
		   Iterator<Entry<String, Double>> iterator=set.iterator();
		     
		while (iterator.hasNext()) {
            Entry<String,Double> entry=iterator.next();
            if(entry.getValue()>0)
           System.out.println(entry.getKey()+"   "+entry.getValue());
		}
		
	System.out.println("mrLlistTempSize="+mrListTemp.size());
	Iterator<MoneyReceipt> iterat=mrList.iterator();
	   while(iterat.hasNext()){
		   MoneyReceipt mr=iterat.next();
		   System.out.println(mr.getMrNo()+" "+mr.getMrRemAmt()+" "+mr.getMrNetAmtForRpt()+" "+mr.getMrFrPDList());
	   }
		return stockAndOsRptList;
	}

	@Override
	public Map<String, Object> getStockRpt(Map<String, Object> stockRptService) {
		System.out.println("ReportDAOImpl.getStockRpt()");
		Map<String, Object> stockRpt = new HashMap<>();
		
		Integer branchId = (Integer) stockRptService.get("branchId");
		Integer custId = (Integer) stockRptService.get("custId");
		String custGroupId=(String) stockRptService.get("custGroupId");
		Date upToDt = CodePatternService.getSqlDate(String.valueOf(stockRptService.get("upToDt")));
		
		List<Cnmt> cnmtList = new ArrayList<>();
		Session session=null;
		try {
			session = this.sessionFactory.openSession();
			
			List<Cnmt> cnmtListTemp1 = new ArrayList<>();
			List<Cnmt> cnmtListTemp2 = new ArrayList<>();
			
			List<Customer> custList = new ArrayList<>();
			List<Branch> brList = new ArrayList<>();
			
			if (custId == null && branchId == null && custGroupId==null) {
				//extract cnmt
				double cnmtFright = 0;
				Criteria cr = session.createCriteria(Cnmt.class);
				cr.add(Restrictions.isNull(CnmtCNTS.CNMT_BILL_NO));
				cr.add(Restrictions.le(CnmtCNTS.CNMT_DT, upToDt));
				cr.add(Restrictions.eq(CnmtCNTS.CNMT_CANCEL, false));
				cr.add(Restrictions.gt(CnmtCNTS.CNMT_FREIGHT, cnmtFright));
				
				cnmtListTemp1 = cr.list();
				
				Query query = session.createQuery("SELECT cnmt FROM Cnmt cnmt, Bill bill WHERE cnmt.cnmtBillNo = bill.blBillNo and " +
						"bill.blBillDt > :upToDt and cnmt.cnmtDt <= :upToDt ");
				query.setDate("upToDt", upToDt);
				cnmtListTemp2 = query.list();
				
				//extract customer
				Criteria custCriteria = session.createCriteria(Customer.class);
				custList = custCriteria.list();
				 
				 
			} else if (custId == null && custGroupId==null) {
				//extract cnmt
				double cnmtFright = 0;
				Criteria cr = session.createCriteria(Cnmt.class);
				cr.add(Restrictions.isNull(CnmtCNTS.CNMT_BILL_NO));
				cr.add(Restrictions.le(CnmtCNTS.CNMT_DT, upToDt));
				cr.add(Restrictions.eq(CnmtCNTS.BRANCH_CODE, String.valueOf(branchId)));
				cr.add(Restrictions.eq(CnmtCNTS.CNMT_CANCEL,false));
				cr.add(Restrictions.gt(CnmtCNTS.CNMT_FREIGHT, cnmtFright));
				
				cnmtListTemp1 = cr.list();
				
				Query query = session.createQuery("SELECT cnmt FROM Cnmt cnmt, Bill bill WHERE cnmt.cnmtBillNo = bill.blBillNo and " +
						"bill.blBillDt > :upToDt and cnmt.cnmtDt <= :upToDt and branchCode = :branchId");
				query.setDate("upToDt", upToDt);
				query.setString("branchId", String.valueOf(branchId));
				cnmtListTemp2 = query.list();
				
				//extract customer
				Criteria custCriteria = session.createCriteria(Customer.class);
				custCriteria.add(Restrictions.eq(CustomerCNTS.BRANCH_CODE, String.valueOf(branchId)));
				custList = custCriteria.list();
				
			} else if (branchId == null && custGroupId == null) {
				//extract cnmt
				double cnmtFright = 0;
				Criteria cr = session.createCriteria(Cnmt.class);
				cr.add(Restrictions.isNull(CnmtCNTS.CNMT_BILL_NO));
				cr.add(Restrictions.le(CnmtCNTS.CNMT_DT, upToDt));
				cr.add(Restrictions.eq(CnmtCNTS.CUST_CODE, String.valueOf(custId)));
				cr.add(Restrictions.eq(CnmtCNTS.CNMT_CANCEL,false));
				cr.add(Restrictions.gt(CnmtCNTS.CNMT_FREIGHT, cnmtFright));
				cnmtListTemp1 = cr.list();
				
				Query query = session.createQuery("SELECT cnmt FROM Cnmt cnmt, Bill bill WHERE cnmt.cnmtBillNo = bill.blBillNo and " +
						"bill.blBillDt > :upToDt and cnmt.cnmtDt <= :upToDt and custCode = :custId");
				query.setDate("upToDt", upToDt);
				query.setString("custId", String.valueOf(custId));
				cnmtListTemp2 = query.list();
				
				//extract customer
				Customer customer = (Customer) session.get(Customer.class, custId);
				custList.add(customer);
				
			}else if(custGroupId!=null){
				double cnmtFright = 0;
				System.out.println("custGroupId.."+custGroupId);
				Criteria criteria=session.createCriteria(Customer.class);
				 criteria.add(Restrictions.eq(CustomerCNTS.CUST_GROUP_ID,custGroupId));
				 custList=criteria.list();
				List<String> custCodeList=new ArrayList<String>();
				  for(Customer cust:custList){
					 custCodeList.add(cust.getCustCode());  
				  }
				if(!custCodeList.isEmpty()){
				Criteria cr = session.createCriteria(Cnmt.class);
				cr.add(Restrictions.isNull(CnmtCNTS.CNMT_BILL_NO));
				cr.add(Restrictions.le(CnmtCNTS.CNMT_DT, upToDt));
				cr.add(Restrictions.in(CnmtCNTS.CUST_CODE,custCodeList));
				cr.add(Restrictions.eq(CnmtCNTS.CNMT_CANCEL,false));
				cr.add(Restrictions.gt(CnmtCNTS.CNMT_FREIGHT, cnmtFright));
				cnmtListTemp1 = cr.list();
				
				Query query = session.createQuery("SELECT cnmt FROM Cnmt cnmt, Bill bill WHERE cnmt.cnmtBillNo = bill.blBillNo and " +
						"bill.blBillDt > :upToDt and cnmt.cnmtDt <= :upToDt and custCode in(:custListId)");
				query.setDate("upToDt", upToDt);
				query.setParameterList("custListId",custCodeList);
				cnmtListTemp2 = query.list();
				}
				//extract customer
				//Customer customer = (Customer) session.get(Customer.class, custId);
				//custList.add(customer);
						System.out.println("custSize="+custList.size());
			}else {
				//extract cnmt
				double cnmtFright = 0;
				Criteria cr = session.createCriteria(Cnmt.class);
				cr.add(Restrictions.isNull(CnmtCNTS.CNMT_BILL_NO));
				cr.add(Restrictions.le(CnmtCNTS.CNMT_DT, upToDt));
				cr.add(Restrictions.eq(CnmtCNTS.CUST_CODE, String.valueOf(custId)));
				cr.add(Restrictions.eq(CnmtCNTS.BRANCH_CODE, String.valueOf(branchId)));
				cr.add(Restrictions.eq(CnmtCNTS.CNMT_CANCEL,false));
				cr.add(Restrictions.gt(CnmtCNTS.CNMT_FREIGHT, cnmtFright));
				
				cnmtListTemp1 = cr.list();
				
				Query query = session.createQuery("SELECT cnmt FROM Cnmt cnmt, Bill bill WHERE cnmt.cnmtBillNo = bill.blBillNo and " +
						"bill.blBillDt > :upToDt and cnmt.cnmtDt <= :upToDt and custCode = :custId and branchCode = :branchId");
				query.setDate("upToDt", upToDt);
				query.setString("custId", String.valueOf(custId));
				query.setString("branchId", String.valueOf(branchId));
				cnmtListTemp2 = query.list();
				
				//extract customer
				Customer customer = (Customer) session.get(Customer.class, custId);
				custList.add(customer);
				
			}
			
			Criteria stnCriteria = session.createCriteria(Station.class);
			List<Station> stationList = stnCriteria.list();
			
			Criteria brCriteria = session.createCriteria(Branch.class);
			brCriteria.add(Restrictions.eq(BranchCNTS.IS_OPEN, "yes"));
			brList = brCriteria.list();
			
			session.clear();
			session.close();
			
			//add stock
			cnmtList.addAll(cnmtListTemp1);
			cnmtList.addAll(cnmtListTemp2);
			System.out.println("final List Size: "+cnmtList.size());
			
			//sorting
			Collections.sort(cnmtList, new Comparator<Cnmt>() {

				@Override
				public int compare(Cnmt cnmt1, Cnmt cnmt2) {
					return cnmt1.getCustCode().compareTo(cnmt2.getCustCode());
				}
			});
			
			List<Map<String, Object>> cnmtMapList = new ArrayList<>();
			List<Map<String, Object>> custMapList = new ArrayList<>();
			
			//double totalFrt = 0.0;
			if (!cnmtList.isEmpty()) {
				session = this.sessionFactory.openSession();
				for (Cnmt cnmt : cnmtList) {
							
					//if (customer.getCustId() == Integer.parseInt(cnmt.getCustCode())) {
								
					Map<String, Object> cnmtMap = new HashMap<>();
								
					//totalFrt += (cnmt.getCnmtFreight()*100.0)/100.0;
								
					Query cnmtChlnQuery = session.createQuery("SELECT chlnId FROM Cnmt_Challan WHERE cnmtId = :cnmtId");
					cnmtChlnQuery.setInteger("cnmtId", cnmt.getCnmtId());
					List<Integer> chlnIdList = cnmtChlnQuery.list();
					System.err.println("chlnIdList size: "+chlnIdList.size());
					if (chlnIdList.isEmpty()) {//no challan for corresponding cnmt
						cnmtMap = getCnmtDetail(cnmt, brList, stationList, session, false, upToDt);
						//cnmtMap.put("cumFrt", totalFrt);
					} else if (chlnIdList.size() == 1) {//one challan for corresponding cnmt 
						cnmtMap = getCnmtDetail(cnmt, brList, stationList, session, false, upToDt);
						//cnmtMap.put("cumFrt", totalFrt);
						
						//add Challan Detail
						Challan challan = (Challan) session.get(Challan.class, chlnIdList.get(0));
						System.err.println("challanId: "+challan.getChlnId());
						cnmtMap.put(ChallanCNTS.CHLN_ID, challan.getChlnId());
						cnmtMap.put(ChallanCNTS.CHALLAN_CODE, challan.getChlnCode());
						cnmtMap.put(ChallanCNTS.CHLN_DT, challan.getChlnDt());
						
						//add own/brk
						Criteria chdCriteria = session.createCriteria(ChallanDetail.class);
						chdCriteria.add(Restrictions.eq(ChallanDetailCNTS.CHD_CHALLAN_CODE, challan.getChlnCode()));
						ChallanDetail chd = new ChallanDetail();
						if (!chdCriteria.list().isEmpty()) {
							chd = (ChallanDetail) chdCriteria.list().get(0);
							
							Criteria brkCriteria = session.createCriteria(Broker.class);
							brkCriteria.add(Restrictions.eq(BrokerCNTS.BRK_CODE, chd.getChdBrCode()));
							if (!brkCriteria.list().isEmpty()) {
								Broker broker = (Broker) brkCriteria.list().get(0);
								cnmtMap.put("ownBrkName", broker.getBrkName());
								cnmtMap.put("ownBrkPhNo", chd.getChdBrMobNo());
							} 
							/* 
							if (chd.getChdPanHdrType() == null || chd.getChdPanHdrType().equalsIgnoreCase("owner")) {
								//consider owner
								Criteria ownCriteria = session.createCriteria(Owner.class);
								ownCriteria.add(Restrictions.eq(OwnerCNTS.OWN_CODE, chd.getChdOwnCode()));
								if (!ownCriteria.list().isEmpty()) {
									Owner owner = (Owner) ownCriteria.list().get(0);
									cnmtMap.put("ownBrkName", owner.getOwnName());
									cnmtMap.put("ownBrkPhNo", chd.getChdOwnMobNo());
								}
							} else {
								//consider broker
								Criteria brkCriteria = session.createCriteria(Broker.class);
								brkCriteria.add(Restrictions.eq(BrokerCNTS.BRK_CODE, chd.getChdBrCode()));
								if (!brkCriteria.list().isEmpty()) {
									Broker broker = (Broker) brkCriteria.list().get(0);
									cnmtMap.put("ownBrkName", broker.getBrkName());
									cnmtMap.put("ownBrkPhNo", chd.getChdBrMobNo());
								}
							}
							*/
						}
						
						if (challan.getChlnLryNo() != null && !challan.getChlnLryNo().equalsIgnoreCase("")) {
							//get lry no from challan
							cnmtMap.put(ChallanCNTS.CHLN_LRY_NO, challan.getChlnLryNo());
						} else {
							//get lry no from challan detail
							cnmtMap.put(ChallanCNTS.CHLN_LRY_NO, chd.getChdRcNo());
						}
									
						//get arrival report
						if (challan.getChlnArId() > 0) {
							System.out.println("arId================>>>>>: "+challan.getChlnArId());
							ArrivalReport ar = (ArrivalReport) session.get(ArrivalReport.class, challan.getChlnArId());
							if (ar != null) {
								cnmtMap.put(ArrivalReportCNTS.AR_ID, ar.getArId());
								cnmtMap.put(ArrivalReportCNTS.AR_CODE, ar.getArCode());
								Date issueDt = ar.getArIssueDt();
								if(issueDt == null)									
									cnmtMap.put(ArrivalReportCNTS.AR_DATE, ar.getArDt());
								else
									cnmtMap.put(ArrivalReportCNTS.AR_DATE, issueDt);
								Memo memo = ar.getMemo();
								if(memo != null){
									cnmtMap.put(MemoCNTS.MEMO_MEMO_NO, memo.getMemoNo());
									cnmtMap.put(MemoCNTS.MEMO_MEMO_DATE, memo.getMemoDate());
									cnmtMap.put(MemoCNTS.MEMO_FROM_BRANCH, memo.getFromBranch());
									cnmtMap.put(MemoCNTS.MEMO_TO_BRANCH, memo.getToBranch());
								}
							}
						}
						
						//get lhpv bal
						SQLQuery lhpvBalQuery = session.createSQLQuery("SELECT lbId FROM lhpvbal_challan WHERE chlnId = :chlnId");
						lhpvBalQuery.setInteger("chlnId", challan.getChlnId());
						
						List<Integer> lhpvBalIdList = lhpvBalQuery.list();
						
						if (!lhpvBalIdList.isEmpty()) {
							LhpvBal lhpvBal = (LhpvBal) session.get(LhpvBal.class, lhpvBalIdList.get(lhpvBalIdList.size()-1));//get last lhpv balance
							
							if (lhpvBal.getLbPayMethod() == 'C' || lhpvBal.getLbPayMethod() == 'C') {
								Criteria cr = session.createCriteria(LhpvCashSmry.class);
								cr.add(Restrictions.eq(LhpvCashSmryCNTS.LB_ID, lhpvBal.getLbId()));
								List<LhpvCashSmry> lhpvCashSmryList = cr.list();
								
								if (!lhpvCashSmryList.isEmpty()) {
									LhpvCashSmry lhpvCashSmry = lhpvCashSmryList.get(0);
									cr = session.createCriteria(LhpvStatus.class);
									cr.add(Restrictions.eq(LhpvStatusCNTS.B_CODE, lhpvCashSmry.getbCode()));
									cr.add(Restrictions.eq(LhpvStatusCNTS.LS_DT, lhpvCashSmry.getLcsLhpvDt()));
									List<LhpvStatus> lhpvStatuList = cr.list();
									
									if (!lhpvStatuList.isEmpty()) {
										//add lhpv no and date
										LhpvStatus lhpvStatus = lhpvStatuList.get(0);
										cnmtMap.put(LhpvStatusCNTS.LS_NO, lhpvStatus.getLsNo()+"("+lhpvStatus.getbCode()+")");
										cnmtMap.put(LhpvStatusCNTS.LS_DT, lhpvStatus.getLsDt());
									}
								}
							} else {
								//add lhpv no and date
								LhpvStatus lhpvStatus  = lhpvBal.getLhpvStatus();
								if(lhpvStatus != null){
									cnmtMap.put(LhpvStatusCNTS.LS_NO, lhpvStatus.getLsNo()+"("+lhpvStatus.getbCode()+")");
									cnmtMap.put(LhpvStatusCNTS.LS_DT, lhpvStatus.getLsDt());
								}
							}
						}
						
					} else if (chlnIdList.size()>1) {//many challan for corresponding cnmt
						
						cnmtMap = getCnmtDetail(cnmt, brList, stationList, session, false, upToDt);
						//cnmtMap.put("cumFrt", totalFrt);
						
						//add Challan Detail
						Challan challan = (Challan) session.get(Challan.class, chlnIdList.get(0));
						cnmtMap.put(ChallanCNTS.CHLN_ID, challan.getChlnId());
						cnmtMap.put(ChallanCNTS.CHALLAN_CODE, challan.getChlnCode());
						cnmtMap.put(ChallanCNTS.CHLN_DT, challan.getChlnDt());
						
						//add own/brk
						Criteria chdCriteria = session.createCriteria(ChallanDetail.class);
						chdCriteria.add(Restrictions.eq(ChallanDetailCNTS.CHD_CHALLAN_CODE, challan.getChlnCode()));
						ChallanDetail chd = new ChallanDetail();
						if (!chdCriteria.list().isEmpty()) {
							chd = (ChallanDetail) chdCriteria.list().get(0);
							if (chd.getChdPanHdrType() == null || chd.getChdPanHdrType().equalsIgnoreCase("owner")) {
								//consider owner
								Criteria ownCriteria = session.createCriteria(Owner.class);
								ownCriteria.add(Restrictions.eq(OwnerCNTS.OWN_CODE, chd.getChdOwnCode()));
								if (!ownCriteria.list().isEmpty()) {
									Owner owner = (Owner) ownCriteria.list().get(0);
									cnmtMap.put("ownBrkName", owner.getOwnName());
									cnmtMap.put("ownBrkPhNo", chd.getChdOwnMobNo());
								}
							} else {
								//consider broker
								Criteria brkCriteria = session.createCriteria(Broker.class);
								brkCriteria.add(Restrictions.eq(BrokerCNTS.BRK_CODE, chd.getChdBrCode()));
								if (!brkCriteria.list().isEmpty()) {
									Broker broker = (Broker) brkCriteria.list().get(0);
									cnmtMap.put("ownBrkName", broker.getBrkName());
									cnmtMap.put("ownBrkPhNo", chd.getChdBrMobNo());
								}
							}
						}
						
						if (challan.getChlnLryNo() != null && !challan.getChlnLryNo().equalsIgnoreCase("")) {
							//get lry no from challan
							cnmtMap.put(ChallanCNTS.CHLN_LRY_NO, challan.getChlnLryNo());
						} else {
							//get lry no from challan detail
							cnmtMap.put(ChallanCNTS.CHLN_LRY_NO, chd.getChdRcNo());
						}
									
						//get arrival report
						if (challan.getChlnArId() > 0) {
							System.out.println("arId================>>>>>: "+challan.getChlnArId());
							ArrivalReport ar = (ArrivalReport) session.get(ArrivalReport.class, challan.getChlnArId());
							if (ar != null) {
								cnmtMap.put(ArrivalReportCNTS.AR_ID, ar.getArId());
								cnmtMap.put(ArrivalReportCNTS.AR_CODE, ar.getArCode());
								cnmtMap.put(ArrivalReportCNTS.AR_DATE, ar.getArDt());
							}
						}
						
						//get lhpv bal
						SQLQuery lhpvBalQuery = session.createSQLQuery("SELECT lbId FROM lhpvbal_challan WHERE chlnId = :chlnId");
						lhpvBalQuery.setInteger("chlnId", challan.getChlnId());
						
						List<Integer> lhpvBalIdList = lhpvBalQuery.list();
						
						if (!lhpvBalIdList.isEmpty()) {
							LhpvBal lhpvBal = (LhpvBal) session.get(LhpvBal.class, lhpvBalIdList.get(lhpvBalIdList.size()-1));//get last lhpv balance
							
							if (lhpvBal.getLbPayMethod() == 'C' || lhpvBal.getLbPayMethod() == 'C') {
								Criteria cr = session.createCriteria(LhpvCashSmry.class);
								cr.add(Restrictions.eq(LhpvCashSmryCNTS.LB_ID, lhpvBal.getLbId()));
								List<LhpvCashSmry> lhpvCashSmryList = cr.list();
								
								if (!lhpvCashSmryList.isEmpty()) {
									LhpvCashSmry lhpvCashSmry = lhpvCashSmryList.get(0);
									cr = session.createCriteria(LhpvStatus.class);
									cr.add(Restrictions.eq(LhpvStatusCNTS.B_CODE, lhpvCashSmry.getbCode()));
									cr.add(Restrictions.eq(LhpvStatusCNTS.LS_DT, lhpvCashSmry.getLcsLhpvDt()));
									List<LhpvStatus> lhpvStatuList = cr.list();
									
									if (!lhpvStatuList.isEmpty()) {
										//add lhpv no and date
										LhpvStatus lhpvStatus  = lhpvStatuList.get(0);
										cnmtMap.put(LhpvStatusCNTS.LS_NO, lhpvStatus.getLsNo()+"("+lhpvStatus.getbCode()+")");
										cnmtMap.put(LhpvStatusCNTS.LS_DT, lhpvStatus.getLsDt());
									}
								}
							} else {
								//add lhpv no and date
								LhpvStatus lhpvStatus  = lhpvBal.getLhpvStatus();
								if(lhpvStatus != null){
									cnmtMap.put(LhpvStatusCNTS.LS_NO, lhpvStatus.getLsNo()+"("+lhpvStatus.getbCode()+")");
									cnmtMap.put(LhpvStatusCNTS.LS_DT, lhpvStatus.getLsDt());
								}
							}
						}
						
						for (int i = 1; i < chlnIdList.size(); i++) {
							Map<String, Object> cnmtTempMap = getCnmtDetail(cnmt, brList, stationList, session, true, upToDt);
							//cnmtTempMap.put("cumFrt", 0.0);
							
							//add Challan Detail
							Challan chln = (Challan) session.get(Challan.class, chlnIdList.get(i));
							cnmtTempMap.put(ChallanCNTS.CHLN_ID, chln.getChlnId());
							cnmtTempMap.put(ChallanCNTS.CHALLAN_CODE, chln.getChlnCode());
							cnmtTempMap.put(ChallanCNTS.CHLN_DT, chln.getChlnDt());
							
							//add own/brk
							Criteria chdCr = session.createCriteria(ChallanDetail.class);
							chdCr.add(Restrictions.eq(ChallanDetailCNTS.CHD_CHALLAN_CODE, chln.getChlnCode()));
							ChallanDetail chlnDetail = new ChallanDetail();
							if (!chdCr.list().isEmpty()) {
								chlnDetail = (ChallanDetail) chdCr.list().get(0);
								if (chlnDetail.getChdPanHdrType() == null || chlnDetail.getChdPanHdrType().equalsIgnoreCase("owner")) {
									//consider owner
									Criteria ownCriteria = session.createCriteria(Owner.class);
									ownCriteria.add(Restrictions.eq(OwnerCNTS.OWN_CODE, chlnDetail.getChdOwnCode()));
									if (!ownCriteria.list().isEmpty()) {
										Owner owner = (Owner) ownCriteria.list().get(0);
										cnmtTempMap.put("ownBrkName", owner.getOwnName());
										cnmtTempMap.put("ownBrkPhNo", chlnDetail.getChdOwnMobNo());
									}
								} else {
									//consider broker
									Criteria brkCriteria = session.createCriteria(Broker.class);
									brkCriteria.add(Restrictions.eq(BrokerCNTS.BRK_CODE, chlnDetail.getChdBrCode()));
									if (!brkCriteria.list().isEmpty()) {
										Broker broker = (Broker) brkCriteria.list().get(0);
										cnmtTempMap.put("ownBrkName", broker.getBrkName());
										cnmtTempMap.put("ownBrkPhNo", chlnDetail.getChdBrMobNo());
									}
								}
							}
							
							if (chln.getChlnLryNo() != null && !chln.getChlnLryNo().equalsIgnoreCase("")) {
								//get lry no from challan
								cnmtTempMap.put(ChallanCNTS.CHLN_LRY_NO, chln.getChlnLryNo());
							} else {
								//get lry no from challan detail
								cnmtTempMap.put(ChallanCNTS.CHLN_LRY_NO, chlnDetail.getChdRcNo());
								
							}
							
							//get arrival report
							if (chln.getChlnArId() > 0) {
								System.out.println("arId================>>>>>: "+chln.getChlnArId());
								ArrivalReport ar = (ArrivalReport) session.get(ArrivalReport.class, chln.getChlnArId());
								if (ar != null) {
									cnmtTempMap.put(ArrivalReportCNTS.AR_ID, ar.getArId());
									cnmtTempMap.put(ArrivalReportCNTS.AR_CODE, ar.getArCode());
									cnmtTempMap.put(ArrivalReportCNTS.AR_DATE, ar.getArDt());
								}
							}
							
							//get lhpv bal
							lhpvBalQuery = session.createSQLQuery("SELECT lbId FROM lhpvbal_challan WHERE chlnId = :chlnId");
							lhpvBalQuery.setInteger("chlnId", chln.getChlnId());
							
							lhpvBalIdList = lhpvBalQuery.list();
							
							if (!lhpvBalIdList.isEmpty()) {
								LhpvBal lhpvBal = (LhpvBal) session.get(LhpvBal.class, lhpvBalIdList.get(lhpvBalIdList.size()-1));//get last lhpv balance
								
								if (lhpvBal.getLbPayMethod() == 'C' || lhpvBal.getLbPayMethod() == 'C') {
									Criteria cr = session.createCriteria(LhpvCashSmry.class);
									cr.add(Restrictions.eq(LhpvCashSmryCNTS.LB_ID, lhpvBal.getLbId()));
									List<LhpvCashSmry> lhpvCashSmryList = cr.list();
									
									if (!lhpvCashSmryList.isEmpty()) {
										LhpvCashSmry lhpvCashSmry = lhpvCashSmryList.get(0);
										cr = session.createCriteria(LhpvStatus.class);
										cr.add(Restrictions.eq(LhpvStatusCNTS.B_CODE, lhpvCashSmry.getbCode()));
										cr.add(Restrictions.eq(LhpvStatusCNTS.LS_DT, lhpvCashSmry.getLcsLhpvDt()));
										List<LhpvStatus> lhpvStatuList = cr.list();
										
										if (!lhpvStatuList.isEmpty()) {
											//add lhpv no and date
											LhpvStatus lhpvStatus  = lhpvStatuList.get(0);
											cnmtTempMap.put(LhpvStatusCNTS.LS_NO, lhpvStatus.getLsNo()+"("+lhpvStatus.getbCode()+")");
											cnmtTempMap.put(LhpvStatusCNTS.LS_DT, lhpvStatus.getLsDt());
										}
									}
								} else {
									//add lhpv no and date
									LhpvStatus lhpvStatus  = lhpvBal.getLhpvStatus();
									if(lhpvStatus != null){
										cnmtTempMap.put(LhpvStatusCNTS.LS_NO, lhpvStatus.getLsNo()+"("+lhpvStatus.getbCode()+")");
										cnmtTempMap.put(LhpvStatusCNTS.LS_DT, lhpvStatus.getLsDt());
									}
								}
							}
							
							cnmtMapList.add(cnmtTempMap);
						}
					}
					cnmtMapList.add(cnmtMap);
				}
					
			}
				
			stockRpt.put("temp", 1);
			stockRpt.put("cnmtMapList", cnmtMapList);
			stockRpt.put("custList", custList);
			stockRpt.put("brList", brList);
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Exception in getStockRpt "+e);
			stockRpt.put("temp", -1);
		}finally{
			session.clear();
			session.close();
		}
		
		return stockRpt;
	}
	
	private Map<String, Object> getCnmtDetail(Cnmt cnmt, List<Branch> brList, List<Station> stationList, Session session, boolean isMultiChln, Date upToDt){
		Map<String, Object> map = new HashMap<>();
		map.put(CnmtCNTS.CNMT_ID, cnmt.getCnmtId());
		map.put(CnmtCNTS.CUST_CODE, cnmt.getCustCode());
		map.put(CnmtCNTS.CNMT_TPNO, cnmt.getCnmtTpNo());
		
		
		
		if (!brList.isEmpty()) {
			for (Branch branch : brList) {
				if (branch.getBranchId() == Integer.valueOf(cnmt.getBranchCode())) {
					map.put("cnmtBrName", branch.getBranchName());
					break;
				}		
			}
			
			for (Branch branch : brList) {
				if (branch.getBranchId() == Integer.valueOf(cnmt.getBranchCode())) {
					map.put("cnmtBillAt", branch.getBranchName());
					break;
				}		
			}
		}
		map.put(CnmtCNTS.CNMT_CODE, cnmt.getCnmtCode());
		if (!stationList.isEmpty()) {
			for (Station station : stationList) {
				if (station.getStnId() == Integer.parseInt(cnmt.getCnmtFromSt())) {
					map.put("cnmtFrmStnName", station.getStnName());
					break;
				}
			}
			
			for (Station station : stationList) {
				if (station.getStnId() == Integer.parseInt(cnmt.getCnmtToSt())) {
					map.put("cnmtToStnName", station.getStnName());
					break;
				}
			}
		}
		map.put(CnmtCNTS.CNMT_ACTUAL_WT, cnmt.getCnmtActualWt()/1000.0);
		map.put(CnmtCNTS.CNMT_GUARANTEE_WT, cnmt.getCnmtGuaranteeWt()/1000.0);
		map.put(CnmtCNTS.CNMT_NO_OF_PKG, cnmt.getCnmtNoOfPkg());
		map.put(CnmtCNTS.CNMT_DC, cnmt.getCnmtDC());
		
		long dateDiff = upToDt.getTime() - cnmt.getCnmtDt().getTime();
		dateDiff = dateDiff/(1000 * 60 * 60 * 24);
		
		if (isMultiChln) {
			map.put(CnmtCNTS.CNMT_FREIGHT, 0.0);
		} else {
			map.put(CnmtCNTS.CNMT_FREIGHT, Math.round(cnmt.getCnmtFreight()*100.0)/100.0);
			if (dateDiff <= 30) {
				map.put(CnmtCNTS.CNMT_FREIGHT+"UpTo30", Math.round(cnmt.getCnmtFreight()*100.0)/100.0);
			} else {
				map.put(CnmtCNTS.CNMT_FREIGHT+"Above30", Math.round(cnmt.getCnmtFreight()*100.0)/100.0);
			}
			
		}
		
		map.put(CnmtCNTS.CNMT_DT, cnmt.getCnmtDt());
		
		if (cnmt.getCnmtConsignor() != null && !cnmt.getCnmtConsignor().equalsIgnoreCase("")) {
			Customer cngr = (Customer) session.get(Customer.class, Integer.parseInt(cnmt.getCnmtConsignor()));
			map.put("cnmtCngorName", cngr.getCustName());
		}
		
		if (cnmt.getCnmtConsignee() != null && !cnmt.getCnmtConsignee().equalsIgnoreCase("")) {
			Customer cngr = (Customer) session.get(Customer.class, Integer.parseInt(cnmt.getCnmtConsignee()));
			map.put("cnmtCngeeName", cngr.getCustName());
		}
		
		return map;
	}

	//TODO OS Rpt
	@Override
	public Map<String, Object> getOsRpt(Map<String, Object> osRptService) {
		System.out.println("ReportDAOImpl.getOsRpt()");
		Map<String, Object> osRpt = new HashMap<>();
		int temp = 0;
		
		//List<Integer> listSelectCust=(List)osRptService.get("selectCust");
		 String custGroupId=(String)osRptService.get("custGroupId");
		Integer branchId = (Integer) osRptService.get("branchId");
		Integer custId = (Integer) osRptService.get("custId");
		Date upToDt = CodePatternService.getSqlDate(String.valueOf(osRptService.get("upToDt")));
	//	System.out.println("checkOrder="+osRptService.get("checkOrder"));
	//	String checkOrder=String.valueOf(osRptService.get("checkOrder"));
		System.out.println("branch: "+branchId);
		System.out.println("cust: "+custId);
		System.out.println("upToDT: "+upToDt);
	//	System.out.println("custList:"+checkOrder);
		System.out.println("custGroupId="+custGroupId);
	//	System.out.println("chekOrder="+checkOrder);
		List<Customer> custList = new ArrayList<>();
		List<Bill> billListTemp = new ArrayList<>();
		List<Bill> billList = new ArrayList<>();
		
		//for on account
		List<MoneyReceipt> mrList = new ArrayList<>();
		List<MoneyReceipt> mrListTemp = new ArrayList<>();
		
		Map<Integer, String> branchMap = new HashMap<>();
		Session session=null;
		try {
			session = this.sessionFactory.openSession();
			
			if (custId == null && branchId == null&&custGroupId==null/*(listSelectCust==null||listSelectCust.isEmpty())*/) {
				
			/*	if(checkOrder!=null&&checkOrder!=""){
				Query query = session.createQuery("SELECT bill FROM Bill bill " +
						"WHERE bill.blBillDt <= :billDt and bill.blFinalTot = bill.blRemAmt " +
						"and bill.blCancel = :blCancel ORDER BY bill."+checkOrder);
				query.setDate("billDt", upToDt);
				query.setBoolean("blCancel", false);
				billList = query.list();
				
				Query queryTemp = session.createQuery("SELECT bill FROM Bill bill " +
						"WHERE bill.blBillDt <= :billDt and bill.blFinalTot > bill.blRemAmt " +
						"and bill.blCancel = :blCancel ORDER BY bill."+checkOrder);
				queryTemp.setDate("billDt", upToDt);
				queryTemp.setBoolean("blCancel", false);
				billListTemp = queryTemp.list();
				}else{ */
					Query query = session.createQuery("SELECT bill FROM Bill bill " +
							"WHERE bill.blBillDt <= :billDt and bill.blFinalTot = bill.blRemAmt " +
							"and bill.blCancel = :blCancel");
					query.setDate("billDt", upToDt);
					query.setBoolean("blCancel", false);
					billList = query.list();
					
					Query queryTemp = session.createQuery("SELECT bill FROM Bill bill " +
							"WHERE bill.blBillDt <= :billDt and bill.blFinalTot > bill.blRemAmt " +
							"and bill.blCancel = :blCancel");
					queryTemp.setDate("billDt", upToDt);
					queryTemp.setBoolean("blCancel", false);
					billListTemp = queryTemp.list();
					
				//}
				//extract customer
				Criteria custCriteria = session.createCriteria(Customer.class);
				custList = custCriteria.list();
				
				//extract branch
				Query brQuery = session.createQuery("SELECT new map(branchId as branchId, branchName as branchName) FROM Branch");
				List<Map<String, Object>> brMapList = brQuery.list();
				
				if (!brMapList.isEmpty()) {
					for (Map<String, Object> brMap : brMapList) {
						branchMap.put(Integer.parseInt(String.valueOf(brMap.get("branchId"))), String.valueOf(brMap.get("branchName")));
					}
				}
				
				//extract onAccount Mr
				Query mrQuery = session.createQuery("SELECT mr FROM MoneyReceipt mr " +
						"WHERE mr.mrType = :mrType and mr.mrDate <= :mrDate and mr.mrNetAmt = mr.mrRemAmt and mr.mrCancel = :mrCancel");
				mrQuery.setString(MoneyReceiptCNTS.MR_TYPE, "On Accounting");
				mrQuery.setDate(MoneyReceiptCNTS.MR_Date, upToDt);
				mrQuery.setBoolean("mrCancel", false);
				mrList = mrQuery.list();
				
				Query mrQueryTemp = session.createQuery("SELECT mr FROM MoneyReceipt mr " +
						"WHERE mr.mrType = :mrType and mr.mrDate <= :mrDate and mr.mrNetAmt > mr.mrRemAmt and mr.mrCancel = :mrCancel");
				mrQueryTemp.setString(MoneyReceiptCNTS.MR_TYPE, "On Accounting");
				mrQueryTemp.setDate(MoneyReceiptCNTS.MR_Date, upToDt);
				mrQueryTemp.setBoolean("mrCancel", false);
				mrListTemp = mrQueryTemp.list();
				
			} else if (custId == null&&custGroupId==null/*(listSelectCust==null||listSelectCust.isEmpty())*/) {//branch wise
				//extract customer
				Criteria custCriteria = session.createCriteria(Customer.class);
				custCriteria.add(Restrictions.eq(CustomerCNTS.BRANCH_CODE, String.valueOf(branchId)));
				custList = custCriteria.list();
				
				Branch branch = (Branch) session.get(Branch.class, branchId);
				if (branch != null) {
					branchMap.put(branch.getBranchId(), branch.getBranchName());
				}
				
				for (Customer cust : custList) {
					//extract bill
					Query query = session.createQuery("SELECT bill FROM Bill bill " +
							"WHERE bill.blBillDt <= :billDt and bill.blFinalTot = bill.blRemAmt and " +
							"bill.blCustId = :blCustId and bill.blCancel = :blCancel");
					query.setDate("billDt", upToDt);
					query.setInteger("blCustId", cust.getCustId());
					query.setBoolean("blCancel", false);

					List<Bill> blList = new ArrayList<>();
					blList = query.list();
					
					if (!blList.isEmpty()) {
						billList.addAll(blList);
					}
					
					Query queryTemp = session.createQuery("SELECT bill FROM Bill bill " +
							"WHERE bill.blBillDt <= :billDt and bill.blFinalTot > bill.blRemAmt and " +
							"bill.blCustId = :blCustId and bill.blCancel = :blCancel");
					queryTemp.setDate("billDt", upToDt);
					queryTemp.setInteger("blCustId", cust.getCustId());
					queryTemp.setBoolean("blCancel", false);
					
					List<Bill> blListTemp = queryTemp.list(); 
					
					if (!blListTemp.isEmpty()) {
						billListTemp.addAll(blListTemp);
					}
					
					//extract onAccount Mr
					Query mrQuery = session.createQuery("SELECT mr FROM MoneyReceipt mr " +
							"WHERE mr.mrType = :mrType and mr.mrDate <= :mrDate and mr.mrNetAmt = mr.mrRemAmt and " +
							" mr.mrCustId = :mrCustId and mr.mrCancel = :mrCancel");
					mrQuery.setString(MoneyReceiptCNTS.MR_TYPE, "On Accounting");
					mrQuery.setDate(MoneyReceiptCNTS.MR_Date, upToDt);
					mrQuery.setInteger(MoneyReceiptCNTS.MR_CUST_ID, cust.getCustId());
					mrQuery.setBoolean("mrCancel", false);
					
					List<MoneyReceipt> moneyReceiptList = new ArrayList<>();
					moneyReceiptList = mrQuery.list();
					if (!moneyReceiptList.isEmpty()) {
						mrList.addAll(moneyReceiptList);
					}
					
					Query mrQueryTemp = session.createQuery("SELECT mr FROM MoneyReceipt mr " +
							"WHERE mr.mrType = :mrType and mr.mrDate <= :mrDate and mr.mrNetAmt > mr.mrRemAmt and " +
							" mr.mrCustId = :mrCustId and mr.mrCancel = :mrCancel");
					mrQueryTemp.setString(MoneyReceiptCNTS.MR_TYPE, "On Accounting");
					mrQueryTemp.setDate(MoneyReceiptCNTS.MR_Date, upToDt);
					mrQueryTemp.setInteger(MoneyReceiptCNTS.MR_CUST_ID, cust.getCustId());
					mrQueryTemp.setBoolean("mrCancel", false);
					
					List<MoneyReceipt> moneyReceiptListTemp = new ArrayList<>();
					moneyReceiptListTemp = mrQueryTemp.list();
					if (!moneyReceiptListTemp.isEmpty()) {
						mrListTemp.addAll(moneyReceiptListTemp);
					}
				}
				
			} else if (branchId == null&&custGroupId==null/*(listSelectCust==null||listSelectCust.isEmpty())*/) {
				//extract bill
				Query query = session.createQuery("SELECT bill FROM Bill bill " +
						"WHERE bill.blBillDt <= :billDt and bill.blFinalTot = bill.blRemAmt and " +
						"blCustId = :custId and bill.blCancel = :blCancel");
				query.setDate("billDt", upToDt);
				query.setInteger("custId", custId);
				query.setBoolean("blCancel", false);
				billList = query.list();
				
				Query queryTemp = session.createQuery("SELECT bill FROM Bill bill " +
						"WHERE bill.blBillDt <= :billDt and bill.blFinalTot > bill.blRemAmt and " +
						"blCustId = :custId and bill.blCancel = :blCancel");
				queryTemp.setDate("billDt", upToDt);
				queryTemp.setInteger("custId", custId);
				queryTemp.setBoolean("blCancel", false);
				billListTemp = queryTemp.list();
				
				//extract customer
				Customer customer = (Customer) session.get(Customer.class, custId);
				custList.add(customer);
				
				//extract branch
				Query brQuery = session.createQuery("SELECT new map(branchId as branchId, branchName as branchName) FROM Branch");
				List<Map<String, Object>> brMapList = brQuery.list();
				
				if (!brMapList.isEmpty()) {
					for (Map<String, Object> brMap : brMapList) {
						branchMap.put(Integer.parseInt(String.valueOf(brMap.get("branchId"))), String.valueOf(brMap.get("branchName")));
					}
				}
				
				//extract onAccount Mr
				Query mrQuery = session.createQuery("SELECT mr FROM MoneyReceipt mr " +
						"WHERE mr.mrType = :mrType and mr.mrDate <= :mrDate and mr.mrNetAmt = mr.mrRemAmt and " +
						" mr.mrCustId = :mrCustId and mr.mrCancel = :mrCancel");
				mrQuery.setString(MoneyReceiptCNTS.MR_TYPE, "On Accounting");
				mrQuery.setDate(MoneyReceiptCNTS.MR_Date, upToDt);
				mrQuery.setInteger(MoneyReceiptCNTS.MR_CUST_ID, custId);
				mrQuery.setBoolean("mrCancel", false);
				mrList = mrQuery.list();
				
				Query mrQueryTemp = session.createQuery("SELECT mr FROM MoneyReceipt mr " +
						"WHERE mr.mrType = :mrType and mr.mrDate <= :mrDate and mr.mrNetAmt > mr.mrRemAmt and " +
						" mr.mrCustId = :mrCustId and mr.mrCancel = :mrCancel");
				mrQueryTemp.setString(MoneyReceiptCNTS.MR_TYPE, "On Accounting");
				mrQueryTemp.setDate(MoneyReceiptCNTS.MR_Date, upToDt);
				mrQueryTemp.setInteger(MoneyReceiptCNTS.MR_CUST_ID, custId);
				mrQueryTemp.setBoolean("mrCancel", false);
				mrListTemp = mrQueryTemp.list();
				
			} else if(custId == null && branchId == null&&custGroupId!=null/*(!listSelectCust.isEmpty())*/){
				 List<Integer> custListId=new ArrayList<Integer>();
				 
				 Query brQuery = session.createQuery("SELECT new map(branchId as branchId, branchName as branchName) FROM Branch");
					List<Map<String, Object>> brMapList = brQuery.list();
					
					if (!brMapList.isEmpty()) {
						for (Map<String, Object> brMap : brMapList) {
							branchMap.put(Integer.parseInt(String.valueOf(brMap.get("branchId"))), String.valueOf(brMap.get("branchName")));
						}
					}
				 
				/*   Iterator<Integer> listIterator=listSelectCust.iterator();
				   while (listIterator.hasNext()) {
					              custListId.add(listIterator.next());
					              	}
				*/
				  // String hql ="FROM Customer C WHERE C.custId  IN (:custIds)"; 
					String hql ="FROM Customer C WHERE C.custGroupId IN (:custGroupId)";
				   Query custQuery = session.createQuery(hql);
				    custQuery.setParameter("custGroupId",custGroupId);
				  // custQuery.setParameterList("custIds",custListId);
				   custList = custQuery.list();
				   
				for (Customer cust : custList) {
					//extract bill
					Query query = session.createQuery("SELECT bill FROM Bill bill " +
							"WHERE bill.blBillDt <= :billDt and bill.blFinalTot = bill.blRemAmt and " +
							"bill.blCustId = :blCustId and bill.blCancel = :blCancel ORDER BY bill.blBillDt ASC ");
					query.setDate("billDt", upToDt);
					query.setInteger("blCustId",cust.getCustId());
					query.setBoolean("blCancel", false);

					List<Bill> blList = new ArrayList<>();
					blList = query.list();
					
					if (!blList.isEmpty()) {
						billList.addAll(blList);
					}
					
					Query queryTemp = session.createQuery("SELECT bill FROM Bill bill " +
							"WHERE bill.blBillDt <= :billDt and bill.blFinalTot > bill.blRemAmt and " +
							"bill.blCustId = :blCustId and bill.blCancel = :blCancel ORDER BY bill.blBillDt ASC");
					queryTemp.setDate("billDt", upToDt);
					queryTemp.setInteger("blCustId",cust.getCustId());
					queryTemp.setBoolean("blCancel", false);
					
					List<Bill> blListTemp = queryTemp.list(); 
					
					if (!blListTemp.isEmpty()) {
						billListTemp.addAll(blListTemp);
					}
					
					//extract onAccount Mr
					Query mrQuery = session.createQuery("SELECT mr FROM MoneyReceipt mr " +
							"WHERE mr.mrType = :mrType and mr.mrDate <= :mrDate and mr.mrNetAmt = mr.mrRemAmt and " +
							" mr.mrCustId = :mrCustId and mr.mrCancel = :mrCancel");
					mrQuery.setString(MoneyReceiptCNTS.MR_TYPE, "On Accounting");
					mrQuery.setDate(MoneyReceiptCNTS.MR_Date, upToDt);
					mrQuery.setInteger(MoneyReceiptCNTS.MR_CUST_ID,cust.getCustId());
					mrQuery.setBoolean("mrCancel", false);
					
					List<MoneyReceipt> moneyReceiptList = new ArrayList<>();
					moneyReceiptList = mrQuery.list();
					if (!moneyReceiptList.isEmpty()) {
						mrList.addAll(moneyReceiptList);
					}
					
					Query mrQueryTemp = session.createQuery("SELECT mr FROM MoneyReceipt mr " +
							"WHERE mr.mrType = :mrType and mr.mrDate <= :mrDate and mr.mrNetAmt > mr.mrRemAmt and " +
							" mr.mrCustId = :mrCustId and mr.mrCancel = :mrCancel");
					mrQueryTemp.setString(MoneyReceiptCNTS.MR_TYPE, "On Accounting");
					mrQueryTemp.setDate(MoneyReceiptCNTS.MR_Date, upToDt);
					mrQueryTemp.setInteger(MoneyReceiptCNTS.MR_CUST_ID,cust.getCustId());
					mrQueryTemp.setBoolean("mrCancel", false);
					
					List<MoneyReceipt> moneyReceiptListTemp = new ArrayList<>();
					moneyReceiptListTemp = mrQueryTemp.list();
					if (!moneyReceiptListTemp.isEmpty()) {
						mrListTemp.addAll(moneyReceiptListTemp);
					}
				}
				
				
			}else {
				//extract bill
				Query query = session.createQuery("SELECT bill FROM Bill bill " +
						"WHERE bill.blBillDt <= :billDt and bill.blFinalTot = bill.blRemAmt and " +
						"blCustId = :custId and bCode = :branchId and bill.blCancel = :blCancel");
				query.setDate("billDt", upToDt);
				query.setInteger("custId", custId);
				query.setString("branchId", String.valueOf(branchId));
				query.setBoolean("blCancel", false);
				billList = query.list();
				
				Query queryTemp = session.createQuery("SELECT bill FROM Bill bill " +
						"WHERE bill.blBillDt <= :billDt and bill.blFinalTot > bill.blRemAmt and " +
						"blCustId = :custId and bCode = :branchId and bill.blCancel = :blCancel");
				queryTemp.setDate("billDt", upToDt);
				queryTemp.setInteger("custId", custId);
				queryTemp.setString("branchId", String.valueOf(branchId));
				queryTemp.setBoolean("blCancel", false);
				billListTemp = queryTemp.list();
				
				//extract customer
				Customer customer = (Customer) session.get(Customer.class, custId);
				custList.add(customer);
				
				//extract branch
				Branch branch = (Branch) session.get(Branch.class, branchId);
				if (branch != null) {
					branchMap.put(branch.getBranchId(), branch.getBranchName());
				}
				
				//extract onAccount Mr
				Query mrQuery = session.createQuery("SELECT mr FROM MoneyReceipt mr " +
						"WHERE mr.mrType = :mrType and mr.mrDate <= :mrDate and mr.mrNetAmt = mr.mrRemAmt and " +
						" mr.mrCustId = :mrCustId and mr.mrBrhId = :mrBrhId and mr.mrCancel = :mrCancel");
				mrQuery.setString(MoneyReceiptCNTS.MR_TYPE, "On Accounting");
				mrQuery.setDate(MoneyReceiptCNTS.MR_Date, upToDt);
				mrQuery.setInteger(MoneyReceiptCNTS.MR_CUST_ID, custId);
				mrQuery.setInteger(MoneyReceiptCNTS.MR_BRH_ID, branchId);
				mrQuery.setBoolean("mrCancel", false);
				mrList = mrQuery.list();
				
				Query mrQueryTemp = session.createQuery("SELECT mr FROM MoneyReceipt mr " +
						"WHERE mr.mrType = :mrType and mr.mrDate <= :mrDate and mr.mrNetAmt > mr.mrRemAmt and " +
						" mr.mrCustId = :mrCustId and mr.mrBrhId = :mrBrhId and mr.mrCancel = :mrCancel");
				mrQueryTemp.setString(MoneyReceiptCNTS.MR_TYPE, "On Accounting");
				mrQueryTemp.setDate(MoneyReceiptCNTS.MR_Date, upToDt);
				mrQueryTemp.setInteger(MoneyReceiptCNTS.MR_CUST_ID, custId);
				mrQueryTemp.setInteger(MoneyReceiptCNTS.MR_BRH_ID, branchId);
				mrQueryTemp.setBoolean("mrCancel", false);
				mrListTemp = mrQueryTemp.list();
			}
			
			for (Bill bill : billList) {
				Hibernate.initialize(bill.getBillDetList());
				bill.setBlFinalTotTemp(bill.getBlFinalTot()); //manoj
			}
			
			if (!mrList.isEmpty()) {
				for (MoneyReceipt moneyReceipt : mrList) {
					if (moneyReceipt.getMrOthMrAmt() > 0) {
						moneyReceipt.setMrNetAmtForRpt(moneyReceipt.getMrNetAmt() - moneyReceipt.getMrOthMrAmt());
					} else {
						moneyReceipt.setMrNetAmtForRpt(moneyReceipt.getMrNetAmt());
					}
				}
			}
			
			if (!mrListTemp.isEmpty()) {
				for (MoneyReceipt moneyReceipt : mrListTemp) {
					if (moneyReceipt.getMrOthMrAmt() > 0) {
						moneyReceipt.setMrNetAmtForRpt(moneyReceipt.getMrNetAmt() - moneyReceipt.getMrOthMrAmt());
					} else {
						moneyReceipt.setMrNetAmtForRpt(moneyReceipt.getMrNetAmt());
					}
				}
			}
			
			//mr for on account
			if (!mrListTemp.isEmpty()) {
				for (MoneyReceipt mr : mrListTemp) {
					List<Map<String, Object>> pdList = new ArrayList<>();
					pdList = mr.getMrFrPDList();
					System.out.println("On Acc MrNo = "+mr.getMrNo());
					System.out.println("On Acc MrDt = "+mr.getMrDate());
					System.out.println("mrFrPdList==========>>: "+pdList.size());
					if (!pdList.isEmpty()) {
						for (Map<String, Object> pd : pdList) {
							System.out.println("mrFrPd: "+pd.entrySet());
							Criteria mrCriteria = session.createCriteria(MoneyReceipt.class);
							//mrCriteria.add(Restrictions.eq(MoneyReceiptCNTS.MR_CUST_ID, mr.getMrCustId()));
							mrCriteria.add(Restrictions.eq(MoneyReceiptCNTS.MR_BRH_ID, Integer.parseInt(String.valueOf(pd.get("brhId")))));
							mrCriteria.add(Restrictions.eq(MoneyReceiptCNTS.MR_NO, String.valueOf(pd.get("pdMr"))));
							mrCriteria.add(Restrictions.eq(MoneyReceiptCNTS.MR_TYPE, "Payment Detail"));
							System.out.println("mrPdListSize: "+mrCriteria.list().size());
							
							List<MoneyReceipt> mrPdList = new ArrayList<>();
							mrPdList = mrCriteria.list(); 

							if (!mrPdList.isEmpty()) {
								MoneyReceipt mrPd = (MoneyReceipt) mrPdList.get(0);
								if (mrPd.getMrDate().compareTo(upToDt) <= 0) {
								mr.setMrNetAmtForRpt(mr.getMrNetAmtForRpt() - Double.parseDouble(String.valueOf(pd.get("detAmt"))));
							   }
							}
						}
					}
				}
			}
			
			if (!mrListTemp.isEmpty()) {
				for (MoneyReceipt moneyReceipt : mrListTemp) {
					if (moneyReceipt.getMrNetAmtForRpt() > 0) {
						mrList.add(moneyReceipt);
					}
				}
			}
			
			//get bill whose mr not prepared according to given condition
		/* manoj	if (!billListTemp.isEmpty()) {
				for (Bill bill : billListTemp) {
					
					SQLQuery sqlQuery = session.createSQLQuery("SELECT mrId FROM mr_bill WHERE blId = :blId");
					sqlQuery.setInteger("blId", bill.getBlId());
					List<Integer> mrIdList = sqlQuery.list();
					   
					if (!mrIdList.isEmpty()) {
						for (Integer mrId : mrIdList) {
							MoneyReceipt moneyReceipt = (MoneyReceipt) session.get(MoneyReceipt.class, mrId);
							if (moneyReceipt.getMrDate() != null &&moneyReceipt.getMrDate().compareTo(upToDt)>0) {
								Hibernate.initialize(bill.getBillDetList());
								billList.add(bill);
								break;
							}
						}
					}
					
				}
			}*/
			int sameBillDetail=0;
			if (!billListTemp.isEmpty()) {
				for (Bill bill : billListTemp) {
					
					SQLQuery sqlQuery = session.createSQLQuery("SELECT mrId FROM mr_bill WHERE blId = :blId");
					sqlQuery.setInteger("blId", bill.getBlId());
					List<Integer> mrIdList = sqlQuery.list();
					 sameBillDetail=0;
					if (!mrIdList.isEmpty()) {
						bill.setBlFinalTotTemp(bill.getBlFinalTot());
						for (Integer mrId : mrIdList) {
							MoneyReceipt moneyReceipt = (MoneyReceipt) session.get(MoneyReceipt.class, mrId);
							  
							if ((moneyReceipt.getMrDate() != null) && (bill.getBlFinalTot()>bill.getBlRemAmt()&&bill.getBlRemAmt()>0.9)) {
								if(!(moneyReceipt.getMrDate().compareTo(upToDt)>0)){
									bill.setBlFinalTotTemp(bill.getBlFinalTotTemp()-moneyReceipt.getMrFreight());
									}
								 Hibernate.initialize(bill.getBillDetList());
								 if(sameBillDetail==0){
								     billList.add(bill);
								     sameBillDetail=1;    
								 }
								 if(bill.getBlFinalTotTemp()<0.9){
									 billList.remove(bill);
								 }
							
							}
							else if(bill.getBlRemAmt()==0.0){
								if((moneyReceipt.getMrDate().compareTo(upToDt)<0)){
								bill.setBlFinalTotTemp(bill.getBlFinalTotTemp()-moneyReceipt.getMrFreight());
								}
								else if((moneyReceipt.getMrDate().compareTo(upToDt)>0)){
									Hibernate.initialize(bill.getBillDetList());
									billList.add(bill);
								}
								
							}
						}
						//Hibernate.initialize(bill.getBillDetList());
						//billList.add(bill);
					}
					else{
						bill.setBlFinalTotTemp(bill.getBlRemAmt());
					}
				}
			} 

			//sort bill according to custId
			Collections.sort(billList, new Comparator<Bill>() {
				@Override
				public int compare(Bill b1, Bill b2) {
					if (b1.getBlCustId() > b2.getBlCustId()) {
						return 1;
					} else if (b1.getBlCustId() < b2.getBlCustId()) {
						return -1;
					} else {
						return 0;
					}
				}
			});

			List<Map<String, Object>> osBillRptList = new ArrayList<>();
			
			if (!billList.isEmpty()) {
				for (Bill bill : billList) {
					
					List<BillDetail> billDetailList = bill.getBillDetList();
					
					for (BillDetail billDetail : billDetailList) {
						Map<String, Object> osBillRpt = new HashMap<>();
						
						osBillRpt.put(BillCNTS.BILL_ID, bill.getBlId());
						osBillRpt.put(BillCNTS.Bill_NO, bill.getBlBillNo());
						osBillRpt.put(BillCNTS.BILL_TYPE, bill.getBlType());
						osBillRpt.put(BillCNTS.BILL_DT, bill.getBlBillDt());
						osBillRpt.put(BillCNTS.BILL_FINAL_TOT, bill.getBlFinalTot());
						osBillRpt.put(BillCNTS.BILL_CUST_ID, bill.getBlCustId());
						
						osBillRpt.put(BillDetailCNTS.BD_ID, billDetail.getBdId());
						osBillRpt.put(BillDetailCNTS.BD_TOT_AMT, billDetail.getBdTotAmt());
						
						//aging of bill
						long dateDiff = upToDt.getTime() - bill.getBlBillDt().getTime();
						dateDiff = dateDiff/(1000 * 60 * 60 * 24);
						/* Manoj...
						if (dateDiff <= 30) {
							osBillRpt.put(BillCNTS.BILL_FINAL_TOT+"os0-30", bill.getBlFinalTot());
						} else if (dateDiff > 30 && dateDiff <= 60) {
							osBillRpt.put(BillCNTS.BILL_FINAL_TOT+"os31-60", bill.getBlFinalTot());
						} else if (dateDiff > 60 && dateDiff <= 90) {
							osBillRpt.put(BillCNTS.BILL_FINAL_TOT+"os61-90", bill.getBlFinalTot());
						} else if (dateDiff > 90 && dateDiff <= 120) {
							osBillRpt.put(BillCNTS.BILL_FINAL_TOT+"os91-120", bill.getBlFinalTot());
						} else if (dateDiff>120) {
							osBillRpt.put(BillCNTS.BILL_FINAL_TOT+"os121-more", bill.getBlFinalTot());
						}
						*/
						if (dateDiff <= 30) {
							osBillRpt.put(BillCNTS.BILL_FINAL_TOT+"os0-30", bill.getBlFinalTotTemp());
						} else if (dateDiff > 30 && dateDiff <= 60) {
							osBillRpt.put(BillCNTS.BILL_FINAL_TOT+"os31-60", bill.getBlFinalTotTemp());
						} else if (dateDiff > 60 && dateDiff <= 90) {
							osBillRpt.put(BillCNTS.BILL_FINAL_TOT+"os61-90", bill.getBlFinalTotTemp());
						} else if (dateDiff > 90 && dateDiff <= 120) {
							osBillRpt.put(BillCNTS.BILL_FINAL_TOT+"os91-120", bill.getBlFinalTotTemp());
						} else if (dateDiff>120) {
							osBillRpt.put(BillCNTS.BILL_FINAL_TOT+"os121-more",bill.getBlFinalTotTemp());
						}
						double othCharge = 0.0;
						if (billDetail.getBdOthChgList() != null && !billDetail.getBdOthChgList().isEmpty()) {
							for (Map<String, Object> othChgargeMap: billDetail.getBdOthChgList()) {
								othCharge += Double.parseDouble(String.valueOf(othChgargeMap.get("otChgValue")));
							}
						}
						
						osBillRpt.put("othChg", othCharge);
						
						//get corresponding cnmt Detail
						Cnmt cnmt = (Cnmt) session.get(Cnmt.class, billDetail.getBdCnmtId());
						if (cnmt != null) {
							System.out.println("****************************");
							System.out.println(cnmt.getCnmtCode());
							System.out.println("****************************");
							osBillRpt.put(CnmtCNTS.CNMT_ID, cnmt.getCnmtId());
							osBillRpt.put(CnmtCNTS.CNMT_CODE, cnmt.getCnmtCode());
							osBillRpt.put(CnmtCNTS.CNMT_DT, cnmt.getCnmtDt());
							osBillRpt.put(CnmtCNTS.CNMT_FREIGHT, cnmt.getCnmtFreight());
							
							//add invoice
							String invoiceNo = "";
							if (cnmt.getCnmtInvoiceNo() != null && !cnmt.getCnmtInvoiceNo().isEmpty()) {
								for (Map<String, Object> cnmtInvoiceNo: cnmt.getCnmtInvoiceNo()) {
									invoiceNo = invoiceNo +" "+cnmtInvoiceNo.get("invoiceNo");
								}
							}
							osBillRpt.put(CnmtCNTS.CNMT_INVOICE_NO, invoiceNo.trim());
							
							//add From station name
							Station frmStn = (Station) session.get(Station.class, Integer.parseInt(cnmt.getCnmtFromSt()));
							if (frmStn != null) {
								osBillRpt.put("frmStnName", frmStn.getStnName());
							} else {
								osBillRpt.put("frmStnName", "");
							}
							
							//add To station name
							Station toStn = (Station) session.get(Station.class, Integer.parseInt(cnmt.getCnmtToSt()));
							if (toStn != null) {
								osBillRpt.put("toStnName", toStn.getStnName());
							} else {
								osBillRpt.put("toStnName", "");
							}
							
							//add delivery date
							Query cnmtChlnQuery = session.createQuery("SELECT chlnId FROM Cnmt_Challan WHERE cnmtId = :cnmtId");
							cnmtChlnQuery.setInteger("cnmtId", cnmt.getCnmtId());
							List<Integer> chlnIdList = cnmtChlnQuery.list();
							
							if (!chlnIdList.isEmpty()) {
								Challan challan = (Challan) session.get(Challan.class, chlnIdList.get(chlnIdList.size() - 1));
								Criteria cr = session.createCriteria(ArrivalReport.class);
								//System.out.println(challan.getChlnCode());
								cr.add(Restrictions.eq(ArrivalReportCNTS.AR_CHLN_CODE, challan.getChlnCode()));
								
								if (!cr.list().isEmpty()) {
									ArrivalReport arrivalReport = (ArrivalReport) cr.list().get(0);
									if (arrivalReport != null) {
										osBillRpt.put(ArrivalReportCNTS.AR_ID, arrivalReport.getArId());
										osBillRpt.put(ArrivalReportCNTS.AR_DATE, arrivalReport.getArDt());
									}
								}
							} 
						}
						osBillRptList.add(osBillRpt);
					}
				}
			}
			
			osRpt.put("osBillRptList", osBillRptList);
			osRpt.put("custList", custList);
			osRpt.put("branchMap", branchMap);
			osRpt.put("mrList", mrList);
			
			System.out.println("mrListsize="+mrList.size());
			System.out.println("custList"+custList.size());
			 for (Customer customer:custList) {
				System.out.println(customer.getbCode());
			}
			for(MoneyReceipt mr:mrList)
				System.out.println(mr.getMrNo()+" "+mr.getMrRemAmt()+" "+mr.getMrNetAmtForRpt()+" "+mr.getMrFrPDList());
			
			
			temp = 1;
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Exception in outstanding report "+e);
			temp = -1;
		}finally{
			session.clear();
			session.close();
		}
		
		osRpt.put("temp", temp);
		return osRpt;
	}
	
	public List<Map<String, Object>> getConsolidateLedgerFinalList(List<Map<String, Object>> ledgerMapListTemp){
		List<Map<String, Object>> finalList = new ArrayList();
		Session session=null;
		session = this.sessionFactory.openSession();
		try{
			Iterator it = ledgerMapListTemp.iterator();
			while(it.hasNext()){
				Map map = (Map)it.next();
				
				String csType = String.valueOf(map.get("csType"));
				String csVouchType = String.valueOf(map.get("csVouchType"));
				String csTvNo = String.valueOf(map.get("csTvNo"));
				String payMode = null;
				Object payObj = map.get("payMode");
				if(payObj != null)
					payMode = String.valueOf(payObj);				
				if(csType.equalsIgnoreCase("Bill") && csVouchType.equalsIgnoreCase("CONTRA")){					
					MoneyReceipt realMr = null;
					if(! NumberUtils.isNumber(csTvNo)){
						Criteria mrCriteria = session.createCriteria(MoneyReceipt.class);
						mrCriteria.createAlias("bill", "bl");
						mrCriteria.add(Restrictions.eq("bl.blBillNo", csTvNo));
						List mrList = mrCriteria.list();
						if(! mrList.isEmpty())
							realMr =(MoneyReceipt)mrCriteria.list().get(0);					
						if(realMr != null){							
							if(realMr.getMrPayBy() == 'Q'){
								map.put("chq", realMr.getMrChqNo());
								map.put("csPayMode", "CHEQUE");
							}else if(realMr.getMrPayBy() == 'C'){
								map.put("chq", "");
								map.put("csPayMode", "CASH");
							}else if(realMr.getMrPayBy() == 'R'){
								map.put("chq", realMr.getMrRtgsRefNo());								
								map.put("csPayMode", "RTGS");
							}else{
								map.put("chq", "");
								map.put("csPayMode", "");
							}
						}else{
							map.put("chq", "");
							map.put("csPayMode", "");							
						}
					}else{
						map.put("chq", "");
						map.put("csPayMode", "");					
					}
				}else{
					if(payMode != null){					
						if(payMode.equalsIgnoreCase("C")){						
							map.put("chq", "");
							map.put("csPayMode", "CASH");
						}else if(payMode.equalsIgnoreCase("Q")){							
							map.put("csPayMode", "CHEQUE");
							if(csTvNo.contains("(")){						
								map.put(CashStmtCNTS.CS_TV_NO, csTvNo.substring(0, csTvNo.indexOf("(")));
								map.put("chq",csTvNo.substring(csTvNo.indexOf("(")+1, csTvNo.length()-1));							
							}else{
								map.put(CashStmtCNTS.CS_TV_NO, csTvNo);//by kamal changed from (CashStmtCNTS.CS_TV_NO, "");
								map.put("chq", csTvNo);
							}
						}else if(payMode.equalsIgnoreCase("R")){							
							map.put("csPayMode", "RTGS");							
							String cstvNo = String.valueOf(map.get("csTvNo"));
							if(csTvNo.contains("(")){
								map.put("chq", cstvNo.substring(cstvNo.indexOf("(")+1, cstvNo.length()-1));
								map.put(CashStmtCNTS.CS_TV_NO, cstvNo.substring(0, csTvNo.indexOf("(")));
							}else{
								map.put("chq", "");
							}
						}
					}else{					
						map.put("chq", "");
						map.put("csPayMode", "");
					}
				}
				finalList.add(map);
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.info("Exception in getConsolidateLedgerFinalList "+e);
		}finally{
			session.clear();
			session.close();
		}
		return finalList;
	}
	
	public List<Map<String, Object>> getBrLedgerFinalList(List<Map<String, Object>> ledgerMapListTemp){
		List<Map<String, Object>> finalList = new ArrayList();
		Session session=null;
		session = this.sessionFactory.openSession();
		try{
			Iterator it = ledgerMapListTemp.iterator();
			while(it.hasNext()){
				Map map = (Map)it.next();
				
				String csType = String.valueOf(map.get("csType"));
				String csVouchType = String.valueOf(map.get("csVouchType"));
				String csTvNo = String.valueOf(map.get("csTvNo"));
				String payMode = null;
				Object payObj = map.get("payMode");
				if(payObj != null)
					payMode = String.valueOf(payObj);				
				if(csType.equalsIgnoreCase("Bill") && csVouchType.equalsIgnoreCase("CONTRA")){					
					MoneyReceipt realMr = null;
					if(! NumberUtils.isNumber(csTvNo)){
						Criteria mrCriteria = session.createCriteria(MoneyReceipt.class);
						mrCriteria.createAlias("bill", "bl");
						mrCriteria.add(Restrictions.eq("bl.blBillNo", csTvNo));
						List mrList = mrCriteria.list();
						if(! mrList.isEmpty())
							realMr =(MoneyReceipt)mrCriteria.list().get(0);					
						if(realMr != null){							
							if(realMr.getMrPayBy() == 'Q'){
								map.put("chq", realMr.getMrChqNo());
								map.put("csPayMode", "CHEQUE");
							}else if(realMr.getMrPayBy() == 'C'){
								map.put("chq", "");
								map.put("csPayMode", "CASH");
							}else if(realMr.getMrPayBy() == 'R'){								
								map.put("chq", realMr.getMrRtgsRefNo());								
								map.put("csPayMode", "RTGS");
							}else{
								map.put("chq", "");
								map.put("csPayMode", "");
							}
						}else{
							map.put("chq", "");
							map.put("csPayMode", "");							
						}
					}else{
						map.put("chq", "");
						map.put("csPayMode", "");					
					}
				}else{
					if(payMode != null){					
						if(payMode.equalsIgnoreCase("C")){						
							map.put("chq", "");
							map.put("csPayMode", "CASH");
						}else if(payMode.equalsIgnoreCase("Q")){							
							map.put("csPayMode", "CHEQUE");
							if(csTvNo.contains("(")){						
								map.put(CashStmtCNTS.CS_TV_NO, csTvNo.substring(0, csTvNo.indexOf("(")));
								map.put("chq",csTvNo.substring(csTvNo.indexOf("(")+1, csTvNo.length()-1));							
							}else{
								map.put(CashStmtCNTS.CS_TV_NO, csTvNo);//by kamal changed from (CashStmtCNTS.CS_TV_NO, "");
								map.put("chq", csTvNo);
							}
						}else if(payMode.equalsIgnoreCase("R")){							
							map.put("csPayMode", "RTGS");							
							String cstvNo = String.valueOf(map.get("csTvNo"));
							if(csTvNo.contains("(")){
								map.put("chq", cstvNo.substring(cstvNo.indexOf("(")+1, cstvNo.length()-1));
								map.put(CashStmtCNTS.CS_TV_NO, cstvNo.substring(0, csTvNo.indexOf("(")));
							}else{
								map.put("chq", cstvNo);
							}
								
						}else if(payMode.equalsIgnoreCase("O")){
							map.put("csPayMode", "O");							
							map.put("chq", "");
						}
					}else{					
						map.put("chq", "");
						map.put("csPayMode", "");
					}
				}
				finalList.add(map);
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.info("Exception in getBrLedgerFinalList "+e);
		}finally{
			session.clear();
			session.close();
		}
		return finalList;
	}
	
/****************************************** get All branch excess rpt detail****************************************/
	
	@Override
	public  List<Map<String, Object>> getShortExcesRpt(Map<String, Object> shortExcessRptMap) {
		System.out.println("enter shortExcesss...");
		
		List<Map<String,Object>> shrtExRptList=new ArrayList<Map<String,Object>>();
		Integer branchId=(Integer)shortExcessRptMap.get("branchId");
		//String custName=String.valueOf(shortExcessRptMap.get("custName"));
		Integer custId=(Integer)shortExcessRptMap.get("custNameId");
		boolean isConsolidate=(Boolean)shortExcessRptMap.get("isConsolidate");
		boolean isBranch=(Boolean)shortExcessRptMap.get("isBranch");
		Date fromDt=CodePatternService.getSqlDate(String.valueOf(shortExcessRptMap.get("fromDt")));
		Date toDt=CodePatternService.getSqlDate(String.valueOf(shortExcessRptMap.get("toDt")));
		List<Map<String,Object>> mapCust=(List)shortExcessRptMap.get(("custList"));
		Session session=null;					
		System.out.println("custid="+custId);
		System.out.println("shortExcessRptMap="+shortExcessRptMap);
		String contentCol[]={"custName","blCustId","blBillNo","blBillDt","blFinalTot",
							  "mrNo","mrDate","mrBrhId","mrNetAmt","mrAccessAmt","mrDedAmt"};
		 try{
			 session=sessionFactory.openSession();
			
		Criteria criteria=session.createCriteria(MoneyReceipt.class);
		 
		// criteria.addOrder(Order.asc(MoneyReceiptCNTS.MR_Date));
		 criteria.add(Restrictions.ge(MoneyReceiptCNTS.MR_Date,fromDt));
		 criteria.add(Restrictions.le(MoneyReceiptCNTS.MR_Date,toDt));
		 if(isConsolidate){
			 if(custId!=null){
		 criteria.add(Restrictions.eq(MoneyReceiptCNTS.MR_CUST_ID,custId));
			 }
		 }else if(isBranch){
			 criteria.add(Restrictions.eq(MoneyReceiptCNTS.MR_BRH_ID,branchId));
			 if(custId!=null){
				 criteria.add(Restrictions.eq(MoneyReceiptCNTS.MR_CUST_ID,custId));
					 }
		 }
		 criteria.add(Restrictions.ne(MoneyReceiptCNTS.MR_CANCEL, true));
		 criteria.add(Restrictions.ne(MoneyReceiptCNTS.MR_SRT_EXCESS_DISALLOW, true));
		 
		 List<MoneyReceipt>  mrList =criteria.list();
		  System.out.println("sizemrLIst="+mrList.size());
		 	
		  	 //String dedDetList[]={"dedAmt","dedType","cnmtNo"};	
		 	 //String mrCustName="";
		 	Map<String,Object> custTemp=null;
		     MoneyReceipt mr=null;
		     Bill bill=null;
		     int j=1;
              Collections.sort(mrList,new Comparator<MoneyReceipt>(){
            	  @Override
            	public int compare(MoneyReceipt mr1, MoneyReceipt mr2) {
            		// TODO Auto-generated method stub
            		  Bill bill1=mr1.getBill();
            		  String blBillNo1=null; 
            		  if(bill1!=null){
            			   blBillNo1=bill1.getBlBillNo();
            		   }else if(bill1==null){
            			   blBillNo1=null;   
            		   }
            		  
            		  Bill bill2=mr2.getBill();
            		  String blBillNo2=null;
            		  if(bill2!=null){
           			   blBillNo2=bill2.getBlBillNo();
            		  }else if(bill2==null){
           			   blBillNo2=null;   
           		   }
            		  
            		    if(blBillNo1==blBillNo2){
            		    	return 0;
            		    }
            		    if(blBillNo1==null){
            		    	return -1;
            		    }
            		    if(blBillNo2==null){
            		    	return 1;
            		    }
            		  return blBillNo1.compareTo(blBillNo2);
            	  }
  
            	});
          /*    Collections.sort(billList, new Comparator<Bill>() {
  				@Override
  				public int compare(Bill b1, Bill b2) {
  					if (b1.getBlCustId() > b2.getBlCustId()) {
  						return 1;
  					} else if (b1.getBlCustId() < b2.getBlCustId()) {
  						return -1;
  					} else {
  						return 0;
  					}
  				}
  			});*/

 		 	 Iterator<MoneyReceipt> iterator= mrList.iterator();
              
              while(iterator.hasNext()){
		    	Map<String,Object> rptMap=new HashMap<String,Object>();
		    	 mr=iterator.next();
		     if(mr.getMrDedAmt()>1.0||mr.getMrAccessAmt()>=1.0){
		    	
		    	 Iterator<Map<String,Object>> custList=mapCust.iterator();
		    	 while(custList.hasNext()){
	    	    	   Map<String,Object> cust=custList.next();
	    	    	     if((Integer)cust.get("custId")==mr.getMrCustId()){
	    	    			  custTemp=cust;
	    	    		   }
	    	    	   }
	    	      
		    	  bill=mr.getBill();
		    	 if(bill!=null){
		    		 if(j==1){
				    	 rptMap.put(CustomerCNTS.CUST_NAME,custTemp.get("custName"));
				    	 j=0;
				    	 }else if(isBranch&&custId==null){
				    		 rptMap.put(CustomerCNTS.CUST_NAME,custTemp.get("custName"));
				    	 }else if(isConsolidate&&custId==null){
				    		 rptMap.put(CustomerCNTS.CUST_NAME,custTemp.get("custName"));
				    	 }else{
				    		 rptMap.put(CustomerCNTS.CUST_NAME," ");
				    	 }
				    	
		    	 
		    	 rptMap.put(BillCNTS.BILL_CUST_ID,bill.getBlCustId());
		    	 rptMap.put(BillCNTS.Bill_NO,bill.getBlBillNo());
		    	 rptMap.put(BillCNTS.BILL_DT,bill.getBlBillDt());
		    	 rptMap.put(BillCNTS.BILL_FINAL_TOT,bill.getBlFinalTot());
		    	 }
	
		    	 rptMap.put(MoneyReceiptCNTS.MR_NO,mr.getMrNo());
		    	  rptMap.put(MoneyReceiptCNTS.MR_Date,mr.getMrDate());
		    	  rptMap.put(MoneyReceiptCNTS.MR_BRH_ID,mr.getMrBrhId());
		    	  rptMap.put(MoneyReceiptCNTS.MR_NET_AMT,mr.getMrNetAmt());
		    	  rptMap.put(MoneyReceiptCNTS.MR_Exce_MR_AMT,mr.getMrAccessAmt());
		    	  rptMap.put(MoneyReceiptCNTS.MR_Ded_Amt,mr.getMrDedAmt());
		    	  List<Map<String,Object>> mrDetList=mr.getMrDedDetList();
		    	  Iterator<Map<String,Object>> iteMrDedDet=mrDetList.iterator();
		    	  int m=0;
	    		    while (iteMrDedDet.hasNext()) {
	    		    	Map<String,Object> map=iteMrDedDet.next();
		    	  rptMap.put("dedAmt",map.get("dedAmt"));
		    	  rptMap.put("dedType",map.get("dedType"));
		    	  rptMap.put("cnmtNo",map.get("cnmtNo"));
		    	  m=1;
		    	  break;
	    		    }
	    		     if(m!=1){
	    		    	rptMap.put("dedAmt"," ");
	  		    	  rptMap.put("dedType"," ");
	  		    	  rptMap.put("cnmtNo"," ");
	    		    }
		    	  shrtExRptList.add(rptMap);
		    	  if(mrDetList!=null&&!mrDetList.isEmpty()){
		    		    while (iteMrDedDet.hasNext()) {
		    		    	Map<String,Object> map=iteMrDedDet.next();
		    		    	for(int i=0;i<contentCol.length;i++){
		    		    		 map.put(contentCol[i]," ");
		    		    	}
		    		    	     shrtExRptList.add(map);
					 		  	}		    	  
				             
		    	  }
						
		     }
		  }
	  
		  
		 }
		 catch(Exception e){
			 e.printStackTrace();
			 shrtExRptList=null;
			 logger.info("Exception in getShortExcesRpt "+e);
		 }finally{
			  session.clear();
			  session.close();
		 }
		return shrtExRptList;
	}
	
	
	public Map<String, Object> sendStockAndOsRpt(MultipartFile attachmentFile){
		logger.error("Enter into sendStockAndOsRpt() : Attachment = "+attachmentFile);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		Session session=null;
		try{
			byte imgInBytes[] = attachmentFile.getBytes();
			String path = httpServletRequest.getRealPath("/")+"/StockAndOs.xls";
			FileOutputStream out = new FileOutputStream(path);
			out.write(imgInBytes);
			out.close();
			session = this.sessionFactory.openSession();
			List<EMailReport> emrList = session.createCriteria(EMailReport.class)
					.add(Restrictions.eq(EmailReportCNTS.EMR_REPORT_NAME, "Outstanding and Stock"))
					.setFetchMode("emrEMailReportList", FetchMode.EAGER)
					.list();
			if(! emrList.isEmpty()){	
				
				
				EMailReport eMailReport = (EMailReport)emrList.get(0);
				
				List<EMailReportList> emrSendList = eMailReport.getEmrEMailReportList();
				List<String> emailList = new ArrayList<String>();
				
				if(! emrSendList.isEmpty()){
					for(int i=0; i<emrSendList.size(); i++){
						String email = emrSendList.get(i).getEmrlEmail();
						emailList.add(email);						
					}
					
					
					EmailThread emailThread = new EmailThread(emailList, eMailReport.getEmrSubject(), eMailReport.getEmrBody(), path, "StockAndOs.xls");
					Thread thread = new Thread(emailThread);
					thread.run();
					
			        logger.info("Attachment Sent....");
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
					resultMap.put("msg", ConstantsValues.SUCCESS);
				}else{
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);					
					resultMap.put("msg", "Receipts not found !");
					logger.info("Receipts not found !");
				}
			}else{
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				resultMap.put("msg", "Report details not found !");
				logger.info("Report not found !");
			}		
			
		}catch(Exception e){
			logger.error("Exception : "+e);
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", ConstantsValues.ERROR);
		}finally{
			session.clear();
			session.close();
		}
		logger.error("Return : "+resultMap.get(ConstantsValues.RESULT));
		logger.error("Exit from sendStockAndOsRpt() ");
		return resultMap;
	}
	
	// By Mohd. Furkan
	
	public List<Map<String, Object>> getHoStockAndOsRpt(User user, Map<String, Object> initParam){
		logger.info("UserID = "+user.getUserId()+" : Enter ento getHoStockAndOsRpt() : InitParam = "+initParam);
		List<Map<String, Object>> stockAndOsRptList = new ArrayList<>();
		
		Integer branchId = (Integer) initParam.get("branchId");
		Integer custId = (Integer) initParam.get("custId");
		Date upToDt = CodePatternService.getSqlDate(String.valueOf(initParam.get("upToDt")));
		
		// Customer List
		List<Customer> custList = new ArrayList<>();
		List<Integer> tempCustId = new ArrayList<>();

		// Bill List
		List<Bill> billListTemp = new ArrayList<>();
		List<Bill> billList = new ArrayList<>();
		
		// Cnmt List
		List<Cnmt> cnmtList = new ArrayList<>();
		
		//for on account
		List<MoneyReceipt> mrList = new ArrayList<>();
		List<MoneyReceipt> mrListTemp = new ArrayList<>();
		Map<String,Double> mapBillId = new HashMap<String,Double>();
		Session session=null;
		try {
			session = this.sessionFactory.openSession();
			
			// Fetch CNMT
			String qu = "SELECT c.custCode AS custCode, c.cnmtDt AS cnmtDt, c.cnmtFreight AS cnmtFreight " +
					"FROM cnmt AS c " +
					"INNER JOIN bill AS b ON c.cnmtBillNo = b.blBillNo " +
					//"AND c.custCode = b.blCustId " +
					"AND c.cnmtDt <= '"+upToDt+"' " +
					"AND c.isCancel = false " +
					"AND b.blBillDt > '"+upToDt+"' ";
	
			Criteria cCriteria = session.createCriteria(Cnmt.class);
			cCriteria.add(Restrictions.le(CnmtCNTS.CNMT_DT, upToDt));
			cCriteria.add(Restrictions.eq(CnmtCNTS.CNMT_CANCEL, false));
			cCriteria.add(Restrictions.isNull(CnmtCNTS.CNMT_BILL_NO));
			cCriteria.setProjection(
					Projections.projectionList()
					.add(Projections.property(CnmtCNTS.CUST_CODE), CnmtCNTS.CUST_CODE)
					.add(Projections.property(CnmtCNTS.CNMT_DT), CnmtCNTS.CNMT_DT)
					.add(Projections.property(CnmtCNTS.CNMT_FREIGHT), CnmtCNTS.CNMT_FREIGHT)
				);
			cCriteria.setResultTransformer(Transformers.aliasToBean(Cnmt.class));
			
			if(branchId != null && custId != null){
				qu = qu + " AND c.bCode = '"+String.valueOf(branchId)+"' AND c.custCode = '"+String.valueOf(custId)+"' ";
				cCriteria.add(Restrictions.eq(CnmtCNTS.USER_BRANCH_CODE, String.valueOf(branchId)));
				if(custId==1754 || custId==1755 || custId==1756){
					cCriteria.add(Restrictions.eq(CnmtCNTS.PR_BL_CODE, String.valueOf(custId)));	
				}else {
					cCriteria.add(Restrictions.isNull(CnmtCNTS.PR_BL_CODE));
					cCriteria.add(Restrictions.eq(CnmtCNTS.CUST_CODE, String.valueOf(custId)));
				}
				
				
			}else if(branchId != null){
				qu = qu + " AND c.bCode = '"+String.valueOf(branchId)+"' ";
				cCriteria.add(Restrictions.eq(CnmtCNTS.USER_BRANCH_CODE, String.valueOf(branchId)));
			}else if(custId != null){
				qu = qu + " c.custCode = '"+String.valueOf(custId)+"' ";
				if(custId==1754 || custId==1755 || custId==1756){
					cCriteria.add(Restrictions.eq(CnmtCNTS.PR_BL_CODE, String.valueOf(custId)));	
				}else {
					cCriteria.add(Restrictions.isNull(CnmtCNTS.PR_BL_CODE));
					cCriteria.add(Restrictions.eq(CnmtCNTS.CUST_CODE, String.valueOf(custId)));
				}
			}
			cnmtList = cCriteria.list();	
			cnmtList.addAll(
					session.createSQLQuery(qu)
					.setResultTransformer(Transformers.aliasToBean(Cnmt.class)).list()
					);
			
			// Fetch Customer
			Criteria custCriteria = session.createCriteria(Customer.class);
			if(branchId != null && custId != null){				
				custCriteria.add(Restrictions.eq(CustomerCNTS.CUST_ID, custId));
			}else if(branchId != null)
				custCriteria.add(Restrictions.eq(CustomerCNTS.BRANCH_CODE, String.valueOf(branchId)));
			else if(custId != null)
				custCriteria.add(Restrictions.eq(CustomerCNTS.CUST_ID, custId));
			
			custList = custCriteria.list();
			
			if(branchId != null && custId == null)
				for(int i=0; i<custList.size(); i++)
					tempCustId.add(custList.get(i).getCustId());
			
			//	Fetch Bill					
			Criteria billCriteria = session.createCriteria(Bill.class)
					.add(Restrictions.le(BillCNTS.BILL_DT, upToDt))
					.add(Restrictions.eq(BillCNTS.BILL_CANCEL, false))
					.add(Restrictions.sqlRestriction("blFinalTot = blRemAmt"));	
			if(branchId != null && custId != null){
				billCriteria.add(Restrictions.eq(BillCNTS.BILL_CUST_ID, custId));
				billCriteria.add(Restrictions.eq(BillCNTS.USER_BRANCH_CODE, String.valueOf(branchId)));
			}
			if(branchId != null && custId == null)
				billCriteria.add(Restrictions.in(BillCNTS.BILL_CUST_ID, tempCustId));
			if(custId != null)
				billCriteria.add(Restrictions.eq(BillCNTS.BILL_CUST_ID, custId));
			
			billList = billCriteria.list();
				
			//	Fetch Bill Temp
			billCriteria = session.createCriteria(Bill.class)
					.add(Restrictions.le(BillCNTS.BILL_DT, upToDt))
					.add(Restrictions.eq(BillCNTS.BILL_CANCEL, false))
					.add(Restrictions.sqlRestriction("blFinalTot > blRemAmt"));
			if(branchId != null && custId != null){
				billCriteria.add(Restrictions.eq(BillCNTS.BILL_CUST_ID, custId));
				billCriteria.add(Restrictions.eq(BillCNTS.USER_BRANCH_CODE, String.valueOf(branchId)));
			}
			if(branchId != null && custId == null)					
				billCriteria.add(Restrictions.in(BillCNTS.BILL_CUST_ID, tempCustId));
			if(custId != null)
				billCriteria.add(Restrictions.eq(BillCNTS.BILL_CUST_ID, custId));
			
			billListTemp = billCriteria.list();
			
			// Fetch MR - OnAccount
			Criteria mrCriteria = session.createCriteria(MoneyReceipt.class)
					.add(Restrictions.eq(MoneyReceiptCNTS.MR_TYPE, "On Accounting"))
					.add(Restrictions.le(MoneyReceiptCNTS.MR_Date, upToDt))
					.add(Restrictions.eq(MoneyReceiptCNTS.MR_CANCEL, false))
					.add(Restrictions.sqlRestriction("mrNetAmt = mrRemAmt"));
			if(branchId != null && custId != null){
				mrCriteria.add(Restrictions.eq(MoneyReceiptCNTS.MR_CUST_ID, custId));				
				mrCriteria.add(Restrictions.eq(MoneyReceiptCNTS.MR_BRH_ID, branchId));
			}
			if(branchId != null && custId == null)
				mrCriteria.add(Restrictions.in(MoneyReceiptCNTS.MR_CUST_ID, tempCustId));
			if(custId != null)
				mrCriteria.add(Restrictions.eq(MoneyReceiptCNTS.MR_CUST_ID, custId));				
			mrList = mrCriteria.list();
				
			// Fetch MR Temp - OnAccount
			mrCriteria = session.createCriteria(MoneyReceipt.class)
					.add(Restrictions.eq(MoneyReceiptCNTS.MR_TYPE, "On Accounting"))
					.add(Restrictions.le(MoneyReceiptCNTS.MR_Date, upToDt))
					.add(Restrictions.eq(MoneyReceiptCNTS.MR_CANCEL, false))
					.add(Restrictions.sqlRestriction("mrNetAmt > mrRemAmt"));
			if(branchId != null && custId != null){
				mrCriteria.add(Restrictions.eq(MoneyReceiptCNTS.MR_CUST_ID, custId));				
				mrCriteria.add(Restrictions.eq(MoneyReceiptCNTS.MR_BRH_ID, branchId));
			}
			if(branchId != null && custId == null)
				mrCriteria.add(Restrictions.in(MoneyReceiptCNTS.MR_CUST_ID, tempCustId));
			if(custId != null)
				mrCriteria.add(Restrictions.eq(MoneyReceiptCNTS.MR_CUST_ID, custId));
			
			mrListTemp = mrCriteria.list();
			
			logger.info("UserID = "+user.getUserId()+" : Cnmt = "+cnmtList.size()+" : Customer = "+custList.size()+" : Bill = "+billList.size()+" : BillTemp = "+billListTemp.size()+" : MR = "+mrList.size()+" : MR Temp = "+mrListTemp.size());
			
			logger.info("UserID = "+user.getUserId()+" : Going to set up mrNetAmtForRpt on MonyeRecript ");
			if (!mrList.isEmpty()) {
				for (MoneyReceipt moneyReceipt : mrList) {
					if (moneyReceipt.getMrOthMrAmt() > 0) {
						moneyReceipt.setMrNetAmtForRpt(moneyReceipt.getMrNetAmt() - moneyReceipt.getMrOthMrAmt());
					} else {
						moneyReceipt.setMrNetAmtForRpt(moneyReceipt.getMrNetAmt());
					}
				}
			}
			
			if (!mrListTemp.isEmpty()) {
				for (MoneyReceipt moneyReceipt : mrListTemp) {
					if (moneyReceipt.getMrOthMrAmt() > 0) {
						moneyReceipt.setMrNetAmtForRpt(moneyReceipt.getMrNetAmt() - moneyReceipt.getMrOthMrAmt());
					} else {
						moneyReceipt.setMrNetAmtForRpt(moneyReceipt.getMrNetAmt());
					}
				}
			}
			
			List<Integer> mrBrhIdList = new ArrayList<>();
			List<String> mrNoList = new ArrayList<>();
			
			if (!mrListTemp.isEmpty()) {
				for (MoneyReceipt mr : mrListTemp) {
					List<Map<String, Object>> pdList = mr.getMrFrPDList();					
					if (!pdList.isEmpty()) {
						for (Map<String, Object> pd : pdList) {
							int brhId = 0;
							if (pd.get("brhId") == null) {
								brhId = mr.getMrBrhId();
							} else {
								brhId = Integer.parseInt(String.valueOf(pd.get("brhId")));
							}
							mrBrhIdList.add(brhId);
							mrNoList.add(String.valueOf(pd.get("pdMr")));
						}
					}
				}
			}
			
			List<MoneyReceipt> mrPdList = session.createCriteria(MoneyReceipt.class)
					.add(Restrictions.in(MoneyReceiptCNTS.MR_BRH_ID, mrBrhIdList))
					.add(Restrictions.in(MoneyReceiptCNTS.MR_NO, mrNoList))
					.add(Restrictions.eq(MoneyReceiptCNTS.MR_TYPE, "Payment Detail"))
					.list();
			
			//mr for on account
			logger.info("UserID = "+user.getUserId()+" : Going to set up mrNetAmtForRpt on MonyeRecript (Payment Detail) ");
			if (!mrListTemp.isEmpty()) {
				for (MoneyReceipt mr : mrListTemp) {
					List<Map<String, Object>> pdList = mr.getMrFrPDList();					
					if (!pdList.isEmpty()) {
						for (Map<String, Object> pd : pdList) {
							int brhId = 0;
							if (pd.get("brhId") == null) {
								brhId = mr.getMrBrhId();
							} else {
								brhId = Integer.parseInt(String.valueOf(pd.get("brhId")));
							}
							String mrNo = String.valueOf(pd.get("pdMr"));
							if (!mrPdList.isEmpty()) {
								for (MoneyReceipt mrPd : mrPdList) {
									if (mrPd.getMrBrhId() == brhId && mrPd.getMrNo().equalsIgnoreCase(mrNo)) {
										if (mrPd.getMrDate().compareTo(upToDt) <= 0) {
											mr.setMrNetAmtForRpt(mr.getMrNetAmtForRpt() - Double.parseDouble(String.valueOf(pd.get("detAmt"))));
										}
										break;
									}
								}
							}
						}
					}
				}
			}
			
			if (!mrListTemp.isEmpty()) {
				for (MoneyReceipt moneyReceipt : mrListTemp) {
					if (moneyReceipt.getMrNetAmtForRpt() > 0) {
						mrList.add(moneyReceipt);
					}
				}
			}
			
			billList.addAll(billListTemp);
			
			//TODO remove part bill here
			logger.info("UserID = "+user.getUserId()+" : Going to find MrDetail according to Bill");
			
			if(! billList.isEmpty()){
				String billIds = new String();
				// Calculating all bill id
				for(int i=0; i<billList.size(); i++)
					billIds = billIds+","+billList.get(i).getBlId();				
				billIds = billIds.substring(1, billIds.length());
				
				// Find all mr/bill relationship
				List<Integer> mrBillIds = session.createSQLQuery("SELECT mrId FROM mr_bill WHERE blId IN("+billIds+") ORDER BY mrId").list();
				List<MoneyReceipt> mrrList = session.createCriteria(MoneyReceipt.class)
						.add(Restrictions.in(MoneyReceiptCNTS.MR_ID, mrBillIds))
						.addOrder(Order.asc(MoneyReceiptCNTS.MR_ID))
						.setFetchMode("bill", FetchMode.SELECT)
						.list();

				// Going to set blFinalTotTemp amount of Bill
				if(! mrrList.isEmpty()){
					for(Bill bill : billList){
						bill.setBlFinalTotTemp(bill.getBlFinalTot());
						for(MoneyReceipt mmr : mrrList){
							if(bill.getBlId() == mmr.getBill().getBlId()){
								if (mmr.getMrDate() != null && mmr.getMrDate().compareTo(upToDt)<=0) {							
									bill.setBlFinalTotTemp(bill.getBlFinalTotTemp() - mmr.getMrFreight());
									if(bill.getBlFinalTotTemp()<1){
										bill.setBlFinalTotTemp(0.0);
									}										
								}
							}
						}
					}
				}
			}
			
					
			//sort bill according to custId
			Collections.sort(billList, new Comparator<Bill>() {
				@Override
				public int compare(Bill b1, Bill b2) {
					if (b1.getBlCustId() > b2.getBlCustId()) {
						return 1;
					} else if (b1.getBlCustId() < b2.getBlCustId()) {
						return -1;
					} else {
						return 0;
					}
				}
			});
			
			session.clear();
			session.close();
			
			// Going to create table row
			logger.info("UserID = "+user.getUserId()+ " : Going to create table row ");
			if (!custList.isEmpty()) {
				for (Customer cust: custList) {
					Map<String, Object> custMap = new HashMap<>();
					
					custMap.put("custId", cust.getCustId());
					custMap.put("custName", cust.getCustName());
					
					//put cust branch code
					if (cust.getBranchCode().length() < 2) {
						custMap.put("custFaCode", "0"+cust.getBranchCode()+"-"+cust.getCustFaCode());
					} else {
						custMap.put("custFaCode", cust.getBranchCode()+"-"+cust.getCustFaCode());
					}
					
					double custOsTotal = 0.0;
					double custStkTotal = 0.0;
					
					if (!billList.isEmpty()) {
						for (Bill bill : billList) {
							if (bill.getBlCustId() == cust.getCustId()) {
								
								// calculate date difference
								long dateDiff = upToDt.getTime() - bill.getBlBillDt().getTime();
								dateDiff = dateDiff/(24 * 60 * 60 * 1000);
																
								if (dateDiff<=30) {
									if (custMap.get("OS0_30") == null) {
										custMap.put("OS0_30", bill.getBlFinalTotTemp());
									} else {
										custMap.put("OS0_30", Double.parseDouble(String.valueOf(custMap.get("OS0_30")))+bill.getBlFinalTotTemp());
									}
									custOsTotal += bill.getBlFinalTotTemp();
								} else if (dateDiff>30 && dateDiff<=60) {
									if (custMap.get("OS31_60") == null) {
										custMap.put("OS31_60", bill.getBlFinalTotTemp());
									} else {
										custMap.put("OS31_60", Double.parseDouble(String.valueOf(custMap.get("OS31_60")))+bill.getBlFinalTotTemp());
									}
									custOsTotal += bill.getBlFinalTotTemp();
								} else if (dateDiff>60 && dateDiff<=90) {
									if (custMap.get("OS61_90") == null) {
										custMap.put("OS61_90", bill.getBlFinalTotTemp());
									} else {
										custMap.put("OS61_90", Double.parseDouble(String.valueOf(custMap.get("OS61_90")))+bill.getBlFinalTotTemp());
									}
									custOsTotal += bill.getBlFinalTotTemp();
								} else if (dateDiff>90 && dateDiff<=120) {
									if (custMap.get("OS91_120") == null) {
										custMap.put("OS91_120", bill.getBlFinalTotTemp());
									} else {
										custMap.put("OS91_120", Double.parseDouble(String.valueOf(custMap.get("OS91_120")))+bill.getBlFinalTotTemp());
									}
									custOsTotal += bill.getBlFinalTotTemp();
								} else if (dateDiff>120) {
									if (custMap.get("OS121_more") == null) {
										custMap.put("OS121_more", bill.getBlFinalTotTemp());
									} else {
										custMap.put("OS121_more", Double.parseDouble(String.valueOf(custMap.get("OS121_more")))+bill.getBlFinalTotTemp());
									}
									custOsTotal += bill.getBlFinalTotTemp();
									mapBillId.put(bill.getBlBillNo(),bill.getBlFinalTotTemp());
								}
								custMap.put("OSTotal", custOsTotal);
							}
						}
					}
					
					//add stock
					if (!cnmtList.isEmpty()) {
						for (Cnmt cnmt : cnmtList) {
							if (cust.getCustId() == Integer.parseInt(cnmt.getCustCode())) {
								long dateDiff = upToDt.getTime() - cnmt.getCnmtDt().getTime();
								dateDiff = dateDiff/(24*60*60*1000);
								if (dateDiff<=30) {
									if (custMap.get("stkUpTo30") == null) {
										custMap.put("stkUpTo30", cnmt.getCnmtFreight());
									} else {
										custMap.put("stkUpTo30", Double.parseDouble(String.valueOf(custMap.get("stkUpTo30")))+cnmt.getCnmtFreight());
									}
									custStkTotal += cnmt.getCnmtFreight();
								}
								if (dateDiff>30) {
									if (custMap.get("stkAbove30") == null) {
										custMap.put("stkAbove30", cnmt.getCnmtFreight());
									} else {
										custMap.put("stkAbove30", Double.parseDouble(String.valueOf(custMap.get("stkAbove30")))+cnmt.getCnmtFreight());
									}
									custStkTotal += cnmt.getCnmtFreight();
								}
							}
							custMap.put("stkTotal", custStkTotal);
						}
					}
					
					double mrNetAmtForRpt = 0.0;
					for (MoneyReceipt mrOnAcc : mrList) {
						if (mrOnAcc.getMrCustId() == cust.getCustId()) {
							mrNetAmtForRpt += mrOnAcc.getMrNetAmtForRpt();
						}
					}
						
					custMap.put("onAC", mrNetAmtForRpt);
					if (custMap.get("OSTotal") == null) {
						custMap.put("OSTotal", 0.0 - mrNetAmtForRpt);
					} else {
						custMap.put("OSTotal", Double.parseDouble(String.valueOf(custMap.get("OSTotal"))) - mrNetAmtForRpt);
					}
					
					if ((custMap.get("onAC") != null && Double.parseDouble(String.valueOf(custMap.get("onAC"))) > 0)
							|| (custMap.get("OSTotal") != null && Double.parseDouble(String.valueOf(custMap.get("OSTotal"))) > 0)
							|| (custMap.get("stkTotal") != null && Double.parseDouble(String.valueOf(custMap.get("stkTotal"))) > 0)) {

						stockAndOsRptList.add(custMap);
					} else if ((custMap.get("onAC") != null && Double.parseDouble(String.valueOf(custMap.get("onAC"))) < 0)
							|| (custMap.get("OSTotal") != null && Double.parseDouble(String.valueOf(custMap.get("OSTotal"))) < 0)
							|| (custMap.get("stkTotal") != null && Double.parseDouble(String.valueOf(custMap.get("stkTotal"))) < 0)) {
						
						stockAndOsRptList.add(custMap);
					}
					
				}
			}
			
		} catch (Exception e) {
			logger.error("UserID = "+user.getUserId()+ " : Exception = "+e);
		} finally {
			if (session.isOpen()) {
				session.clear();
				session.close();
			}
		}
		
		return stockAndOsRptList;
	}
	
	//TODO OS Rpt
	@Override
	public Map<String, Object> getOsRptN(Map<String, Object> osRptService) {
		System.out.println("ReportDAOImpl.getOsRpt()");
		Map<String, Object> osRpt = new HashMap<>();
		int temp = 0;
		String custGroupId=(String)osRptService.get("custGroupId");
		Integer branchId = (Integer) osRptService.get("branchId");
		Integer custId = (Integer) osRptService.get("custId");
		String osby=(String)osRptService.get("OSBY");
		Date upToDt = CodePatternService.getSqlDate(String.valueOf(osRptService.get("upToDt")));
		System.out.println("branch: "+branchId);
		System.out.println("cust: "+custId);
		System.out.println("upToDT: "+upToDt);
		System.out.println("custGroupId="+custGroupId);
		List<Customer> custList = new ArrayList<>();
		List<Bill> billListTemp = new ArrayList<>();
		List<Bill> billList = new ArrayList<>();
		
		//for on account
		List<MoneyReceipt> mrList = new ArrayList<>();
		List<MoneyReceipt> mrListTemp = new ArrayList<>();
		
		Map<Integer, String> branchMap = new HashMap<>();
		Session session=null;
		try {
			session = this.sessionFactory.openSession();
			
			if (custId == null && branchId == null&&custGroupId==null) {
				Criteria billCriteria = session.createCriteria(Bill.class)
						.add(Restrictions.le(BillCNTS.BILL_DT, upToDt))
						.add(Restrictions.eq(BillCNTS.BILL_CANCEL, false))
						.add(Restrictions.eqProperty(BillCNTS.BILL_FINAL_TOT, BillCNTS.BILL_REM_AMT));
						//.setFetchMode("billDetList", FetchMode.EAGER);
				billList.addAll(billCriteria.list());
				
				billCriteria = session.createCriteria(Bill.class)
					.add(Restrictions.le(BillCNTS.BILL_DT, upToDt))
					.add(Restrictions.eq(BillCNTS.BILL_CANCEL, false))
					.add(Restrictions.gtProperty(BillCNTS.BILL_FINAL_TOT, BillCNTS.BILL_REM_AMT));
					//.setFetchMode("billDetList", FetchMode.EAGER);
				billListTemp.addAll(billCriteria.list());
					
				//extract customer
				Criteria custCriteria = session.createCriteria(Customer.class);
				custList = custCriteria.list();
				
				//extract branch
				Query brQuery = session.createQuery("SELECT new map(branchId as branchId, branchName as branchName) FROM Branch");
				List<Map<String, Object>> brMapList = brQuery.list();
				
				if (!brMapList.isEmpty()) {
					for (Map<String, Object> brMap : brMapList) {
						branchMap.put(Integer.parseInt(String.valueOf(brMap.get("branchId"))), String.valueOf(brMap.get("branchName")));
					}
				}
				
				//extract onAccount Mr
				Criteria mrCriteria = session.createCriteria(MoneyReceipt.class)
						.add(Restrictions.eq(MoneyReceiptCNTS.MR_TYPE, "On Accounting"))
						.add(Restrictions.le(MoneyReceiptCNTS.MR_Date, upToDt))
						.add(Restrictions.eq(MoneyReceiptCNTS.MR_CANCEL, false))
						.add(Restrictions.eqProperty(MoneyReceiptCNTS.MR_NET_AMT, MoneyReceiptCNTS.MR_REM_AMT));
				mrList.addAll(mrCriteria.list());
				
				mrCriteria = session.createCriteria(MoneyReceipt.class)
						.add(Restrictions.eq(MoneyReceiptCNTS.MR_TYPE, "On Accounting"))
						.add(Restrictions.le(MoneyReceiptCNTS.MR_Date, upToDt))
						.add(Restrictions.eq(MoneyReceiptCNTS.MR_CANCEL, false))
						.add(Restrictions.gtProperty(MoneyReceiptCNTS.MR_NET_AMT, MoneyReceiptCNTS.MR_REM_AMT));
				mrListTemp.addAll(mrCriteria.list());
				
			} else if (custId == null&&custGroupId==null) {//branch wise
				//extract customer
				Criteria custCriteria = session.createCriteria(Customer.class);
				custCriteria.add(Restrictions.eq(CustomerCNTS.BRANCH_CODE, String.valueOf(branchId)));
				custList = custCriteria.list();
				
				Branch branch = (Branch) session.get(Branch.class, branchId);
				if (branch != null) {
					branchMap.put(branch.getBranchId(), branch.getBranchName());
				}
				
				List<Integer> customerIdList = new ArrayList<>();
				for(Customer cust : custList) {
					customerIdList.add(cust.getCustId());
				}
				
				Criteria billCriteria = session.createCriteria(Bill.class)
						.add(Restrictions.le(BillCNTS.BILL_DT, upToDt))
						.add(Restrictions.in(BillCNTS.BILL_CUST_ID, customerIdList))
						.add(Restrictions.eq(BillCNTS.BILL_CANCEL, false))
						.add(Restrictions.eqProperty(BillCNTS.BILL_FINAL_TOT, BillCNTS.BILL_REM_AMT));	
						//.setFetchMode("billDetList", FetchMode.EAGER);
				billList.addAll(billCriteria.list());
				
				billCriteria = session.createCriteria(Bill.class)
					.add(Restrictions.le(BillCNTS.BILL_DT, upToDt))
					.add(Restrictions.in(BillCNTS.BILL_CUST_ID, customerIdList))
					.add(Restrictions.eq(BillCNTS.BILL_CANCEL, false))
					.add(Restrictions.gtProperty(BillCNTS.BILL_FINAL_TOT, BillCNTS.BILL_REM_AMT));
					//.setFetchMode("billDetList", FetchMode.EAGER);
				billListTemp.addAll(billCriteria.list());
				
				Criteria mrCriteria = session.createCriteria(MoneyReceipt.class)
						.add(Restrictions.eq(MoneyReceiptCNTS.MR_TYPE, "On Accounting"))
						.add(Restrictions.le(MoneyReceiptCNTS.MR_Date, upToDt))
						.add(Restrictions.in(MoneyReceiptCNTS.MR_CUST_ID, customerIdList))
						.add(Restrictions.eq(MoneyReceiptCNTS.MR_CANCEL, false))
						.add(Restrictions.eqProperty(MoneyReceiptCNTS.MR_NET_AMT, MoneyReceiptCNTS.MR_REM_AMT));
				mrList.addAll(mrCriteria.list());
				
				mrCriteria = session.createCriteria(MoneyReceipt.class)
						.add(Restrictions.eq(MoneyReceiptCNTS.MR_TYPE, "On Accounting"))
						.add(Restrictions.le(MoneyReceiptCNTS.MR_Date, upToDt))
						.add(Restrictions.in(MoneyReceiptCNTS.MR_CUST_ID, customerIdList))
						.add(Restrictions.eq(MoneyReceiptCNTS.MR_CANCEL, false))
						.add(Restrictions.gtProperty(MoneyReceiptCNTS.MR_NET_AMT, MoneyReceiptCNTS.MR_REM_AMT));
				mrListTemp.addAll(mrCriteria.list());
			} else if (branchId == null&&custGroupId==null) {
				//extract bill
				Criteria billCriteria = session.createCriteria(Bill.class)
						.add(Restrictions.le(BillCNTS.BILL_DT, upToDt))
						.add(Restrictions.eq(BillCNTS.BILL_CUST_ID, custId))
						.add(Restrictions.eq(BillCNTS.BILL_CANCEL, false))
						.add(Restrictions.eqProperty(BillCNTS.BILL_FINAL_TOT, BillCNTS.BILL_REM_AMT));
						//.setFetchMode("billDetList", FetchMode.EAGER);
				billList.addAll(billCriteria.list());
				
				billCriteria = session.createCriteria(Bill.class)
					.add(Restrictions.le(BillCNTS.BILL_DT, upToDt))
					.add(Restrictions.eq(BillCNTS.BILL_CUST_ID, custId))
					.add(Restrictions.eq(BillCNTS.BILL_CANCEL, false))
					.add(Restrictions.gtProperty(BillCNTS.BILL_FINAL_TOT, BillCNTS.BILL_REM_AMT));
					//.setFetchMode("billDetList", FetchMode.EAGER);
				billListTemp.addAll(billCriteria.list());
				
				Criteria mrCriteria = session.createCriteria(MoneyReceipt.class)
						.add(Restrictions.eq(MoneyReceiptCNTS.MR_TYPE, "On Accounting"))
						.add(Restrictions.le(MoneyReceiptCNTS.MR_Date, upToDt))
						.add(Restrictions.eq(MoneyReceiptCNTS.MR_CUST_ID, custId))
						.add(Restrictions.eq(MoneyReceiptCNTS.MR_CANCEL, false))
						.add(Restrictions.eqProperty(MoneyReceiptCNTS.MR_NET_AMT, MoneyReceiptCNTS.MR_REM_AMT));
				mrList.addAll(mrCriteria.list());
				
				mrCriteria = session.createCriteria(MoneyReceipt.class)
						.add(Restrictions.eq(MoneyReceiptCNTS.MR_TYPE, "On Accounting"))
						.add(Restrictions.le(MoneyReceiptCNTS.MR_Date, upToDt))
						.add(Restrictions.eq(MoneyReceiptCNTS.MR_CUST_ID, custId))
						.add(Restrictions.eq(MoneyReceiptCNTS.MR_CANCEL, false))
						.add(Restrictions.gtProperty(MoneyReceiptCNTS.MR_NET_AMT, MoneyReceiptCNTS.MR_REM_AMT));
				mrListTemp.addAll(mrCriteria.list());
				
				//extract customer
				Customer customer = (Customer) session.get(Customer.class, custId);
				custList.add(customer);
				
				//extract branch
				Query brQuery = session.createQuery("SELECT new map(branchId as branchId, branchName as branchName) FROM Branch");
				List<Map<String, Object>> brMapList = brQuery.list();
				
				if (!brMapList.isEmpty()) {
					for (Map<String, Object> brMap : brMapList) {
						branchMap.put(Integer.parseInt(String.valueOf(brMap.get("branchId"))), String.valueOf(brMap.get("branchName")));
					}
				}
			} else if(custId == null && branchId == null&&custGroupId!=null){
				 List<Integer> custListId=new ArrayList<Integer>();
				 
				 Query brQuery = session.createQuery("SELECT new map(branchId as branchId, branchName as branchName) FROM Branch");
					List<Map<String, Object>> brMapList = brQuery.list();
					
					if (!brMapList.isEmpty()) {
						for (Map<String, Object> brMap : brMapList) {
							branchMap.put(Integer.parseInt(String.valueOf(brMap.get("branchId"))), String.valueOf(brMap.get("branchName")));
						}
					}
					String hql ="FROM Customer C WHERE C.custGroupId IN (:custGroupId)";
				   Query custQuery = session.createQuery(hql);
				    custQuery.setParameter("custGroupId",custGroupId);
				   custList = custQuery.list();
				   
				   List<Integer> customerIdList = new ArrayList<>();
				   for(Customer cust : custList)
					   customerIdList.add(cust.getCustId());
				   
				   Criteria billCriteria = session.createCriteria(Bill.class)
							.add(Restrictions.le(BillCNTS.BILL_DT, upToDt))
							.add(Restrictions.in(BillCNTS.BILL_CUST_ID, customerIdList))
							.add(Restrictions.eq(BillCNTS.BILL_CANCEL, false))
							.add(Restrictions.eqProperty(BillCNTS.BILL_FINAL_TOT, BillCNTS.BILL_REM_AMT));
//				   			.setFetchMode("billDetList", FetchMode.EAGER);
					billList.addAll(billCriteria.list());
					
					billCriteria = session.createCriteria(Bill.class)
						.add(Restrictions.le(BillCNTS.BILL_DT, upToDt))
						.add(Restrictions.in(BillCNTS.BILL_CUST_ID, customerIdList))
						.add(Restrictions.eq(BillCNTS.BILL_CANCEL, false))
						.add(Restrictions.gtProperty(BillCNTS.BILL_FINAL_TOT, BillCNTS.BILL_REM_AMT));
//						.setFetchMode("billDetList", FetchMode.EAGER);
					billListTemp.addAll(billCriteria.list());
					
					Criteria mrCriteria = session.createCriteria(MoneyReceipt.class)
							.add(Restrictions.eq(MoneyReceiptCNTS.MR_TYPE, "On Accounting"))
							.add(Restrictions.le(MoneyReceiptCNTS.MR_Date, upToDt))
							.add(Restrictions.in(MoneyReceiptCNTS.MR_CUST_ID, customerIdList))
							.add(Restrictions.eq(MoneyReceiptCNTS.MR_CANCEL, false))
							.add(Restrictions.eqProperty(MoneyReceiptCNTS.MR_NET_AMT, MoneyReceiptCNTS.MR_REM_AMT));				
					mrList.addAll(mrCriteria.list());
					
					mrCriteria = session.createCriteria(MoneyReceipt.class)
							.add(Restrictions.eq(MoneyReceiptCNTS.MR_TYPE, "On Accounting"))
							.add(Restrictions.le(MoneyReceiptCNTS.MR_Date, upToDt))
							.add(Restrictions.in(MoneyReceiptCNTS.MR_CUST_ID, customerIdList))
							.add(Restrictions.eq(MoneyReceiptCNTS.MR_CANCEL, false))
							.add(Restrictions.gtProperty(MoneyReceiptCNTS.MR_NET_AMT, MoneyReceiptCNTS.MR_REM_AMT));
					mrListTemp.addAll(mrCriteria.list());
			} else {
				Criteria billCriteria = session.createCriteria(Bill.class)
						.add(Restrictions.le(BillCNTS.BILL_DT, upToDt))
						.add(Restrictions.eq(BillCNTS.BILL_CUST_ID, custId))
						.add(Restrictions.eq(BillCNTS.BILL_CANCEL, false))
						.add(Restrictions.eq(BillCNTS.USER_BRANCH_CODE, Integer.valueOf(branchId)))
						.add(Restrictions.eqProperty(BillCNTS.BILL_FINAL_TOT, BillCNTS.BILL_REM_AMT));
						//.setFetchMode("billDetList", FetchMode.EAGER);
				billList.addAll(billCriteria.list());
				
				billCriteria = session.createCriteria(Bill.class)
					.add(Restrictions.le(BillCNTS.BILL_DT, upToDt))
					.add(Restrictions.eq(BillCNTS.BILL_CUST_ID, custId))
					.add(Restrictions.eq(BillCNTS.BILL_CANCEL, false))
					.add(Restrictions.eq(BillCNTS.USER_BRANCH_CODE, Integer.valueOf(branchId)))
					.add(Restrictions.gtProperty(BillCNTS.BILL_FINAL_TOT, BillCNTS.BILL_REM_AMT));
					//.setFetchMode("billDetList", FetchMode.EAGER);
				billListTemp.addAll(billCriteria.list());
				
				Criteria mrCriteria = session.createCriteria(MoneyReceipt.class)
						.add(Restrictions.eq(MoneyReceiptCNTS.MR_TYPE, "On Accounting"))
						.add(Restrictions.le(MoneyReceiptCNTS.MR_Date, upToDt))
						.add(Restrictions.eq(MoneyReceiptCNTS.MR_CUST_ID, custId))
						.add(Restrictions.eq(MoneyReceiptCNTS.MR_CANCEL, false))
						.add(Restrictions.eq(MoneyReceiptCNTS.MR_BRH_ID, branchId))
						.add(Restrictions.eqProperty(MoneyReceiptCNTS.MR_NET_AMT, MoneyReceiptCNTS.MR_REM_AMT));
				mrList.addAll(mrCriteria.list());
				
				mrCriteria = session.createCriteria(MoneyReceipt.class)
						.add(Restrictions.eq(MoneyReceiptCNTS.MR_TYPE, "On Accounting"))
						.add(Restrictions.le(MoneyReceiptCNTS.MR_Date, upToDt))
						.add(Restrictions.eq(MoneyReceiptCNTS.MR_CUST_ID, custId))
						.add(Restrictions.eq(MoneyReceiptCNTS.MR_CANCEL, false))
						.add(Restrictions.eq(MoneyReceiptCNTS.MR_BRH_ID, branchId))
						.add(Restrictions.gtProperty(MoneyReceiptCNTS.MR_NET_AMT, MoneyReceiptCNTS.MR_REM_AMT));
				mrListTemp.addAll(mrCriteria.list());
				
				//extract customer
				Customer customer = (Customer) session.get(Customer.class, custId);
				custList.add(customer);
				
				//extract branch
				Branch branch = (Branch) session.get(Branch.class, branchId);
				if (branch != null) {
					branchMap.put(branch.getBranchId(), branch.getBranchName());
				}
			}
			
			for (Bill bill : billList) {
				bill.setBlFinalTotTemp(bill.getBlFinalTot()); //manoj
			}
			
			if (!mrList.isEmpty()) {
				for (MoneyReceipt moneyReceipt : mrList) {
					if (moneyReceipt.getMrOthMrAmt() > 0) {
						moneyReceipt.setMrNetAmtForRpt(moneyReceipt.getMrNetAmt() - moneyReceipt.getMrOthMrAmt());
					} else {
						moneyReceipt.setMrNetAmtForRpt(moneyReceipt.getMrNetAmt());
					}
				}
			}
			
			if (!mrListTemp.isEmpty()) {
				for (MoneyReceipt moneyReceipt : mrListTemp) {
					if (moneyReceipt.getMrOthMrAmt() > 0) {
						moneyReceipt.setMrNetAmtForRpt(moneyReceipt.getMrNetAmt() - moneyReceipt.getMrOthMrAmt());
					} else {
						moneyReceipt.setMrNetAmtForRpt(moneyReceipt.getMrNetAmt());
					}
				}
			}
			
			List<Integer> mrBrhId  = new ArrayList<>();
			List<String> mrNoList = new ArrayList<>();
			
			for (MoneyReceipt mr : mrListTemp) {
				List<Map<String, Object>> pdList = mr.getMrFrPDList();
				for (Map<String, Object> pd : pdList) {
					mrBrhId.add(Integer.parseInt(String.valueOf(pd.get("brhId"))));
					mrNoList.add(String.valueOf(pd.get("pdMr")));
				}
			}
			
			List<MoneyReceipt> mrPdList = new ArrayList<>();
			if (!mrListTemp.isEmpty())
					mrPdList.addAll(session.createCriteria(MoneyReceipt.class)
						.add(Restrictions.in(MoneyReceiptCNTS.MR_BRH_ID, mrBrhId))
						.add(Restrictions.in(MoneyReceiptCNTS.MR_NO, mrNoList))
						.add(Restrictions.eq(MoneyReceiptCNTS.MR_TYPE, "Payment Detail"))
						.list());
			
			Map<String, MoneyReceipt> mrPdMap = new HashMap<>();
			for (MoneyReceipt mr : mrPdList)
				mrPdMap.put(mr.getMrBrhId()+":"+mr.getMrNo(), mr);
			
			//mr for on account
			if (!mrListTemp.isEmpty()) {
				for (MoneyReceipt mr : mrListTemp) {
					List<Map<String, Object>> pdList = mr.getMrFrPDList();
					System.out.println("On Acc MrNo = "+mr.getMrNo());
					if (!pdList.isEmpty()) {
						for (Map<String, Object> pd : pdList) {
							MoneyReceipt mrPd = mrPdMap.get(Integer.parseInt(String.valueOf(pd.get("brhId")))+":"+String.valueOf(pd.get("pdMr")));
							if (mrPd != null) {
								if (mrPd.getMrDate().compareTo(upToDt) <= 0) {
								mr.setMrNetAmtForRpt(mr.getMrNetAmtForRpt() - Double.parseDouble(String.valueOf(pd.get("detAmt"))));
							   }
							}
						}
					}
				}
			}
			
			if (!mrListTemp.isEmpty()) {
				for (MoneyReceipt moneyReceipt : mrListTemp) {
					if (moneyReceipt.getMrNetAmtForRpt() > 0) {
						mrList.add(moneyReceipt);
					}
				}
			}

			Set<Integer> blIdSet = new HashSet<>();
			for(Bill bill : billListTemp){
				blIdSet.add(bill.getBlId());
			}
			
			List<MoneyReceipt> mrBillList = new ArrayList<>();
			if (!billListTemp.isEmpty())
				mrBillList.addAll(
					session.createCriteria(MoneyReceipt.class)
					.createAlias("bill", "bill")
					.add(Restrictions.in("bill.blId", blIdSet))
					.setFetchMode("bill", FetchMode.JOIN)
					.list());
			
			Map<Integer, List<MoneyReceipt>> billMrMap = new HashMap<>();
			for (MoneyReceipt mr : mrBillList) {
				Bill bill = mr.getBill();
				if (billMrMap.get(bill.getBlId()) == null) {
					List<MoneyReceipt> list = new ArrayList<>();
					list.add(mr);
					billMrMap.put(bill.getBlId(), list);
				} else {
					List<MoneyReceipt> list = billMrMap.get(bill.getBlId());
					list.add(mr);
				}
			}
			
			int sameBillDetail=0;
			if (!billListTemp.isEmpty()) {
				for (Bill bill : billListTemp) {
					 sameBillDetail=0;
					if (billMrMap.get(bill.getBlId()) !=null && !billMrMap.get(bill.getBlId()).isEmpty()) {
						bill.setBlFinalTotTemp(bill.getBlFinalTot());
						for (MoneyReceipt moneyReceipt : billMrMap.get(bill.getBlId())) {
							if ((moneyReceipt.getMrDate() != null) && (bill.getBlFinalTot()>bill.getBlRemAmt()&&bill.getBlRemAmt()>0.9)) {
								if(!(moneyReceipt.getMrDate().compareTo(upToDt)>0)){
									bill.setBlFinalTotTemp(bill.getBlFinalTotTemp()-moneyReceipt.getMrFreight());
									}
								 Hibernate.initialize(bill.getBillDetList());
								 if(sameBillDetail==0){
								     billList.add(bill);
								     sameBillDetail=1;    
								 }
								 if(bill.getBlFinalTotTemp()<0.9){
									 billList.remove(bill);
								 }
							
							}
							else if(bill.getBlRemAmt()==0.0){
								if((moneyReceipt.getMrDate().compareTo(upToDt)<0)){
								bill.setBlFinalTotTemp(bill.getBlFinalTotTemp()-moneyReceipt.getMrFreight());
								}
								else if((moneyReceipt.getMrDate().compareTo(upToDt)>0)){
									Hibernate.initialize(bill.getBillDetList());
									billList.add(bill);
								}
								
							}
						}
					}
					else{
						bill.setBlFinalTotTemp(bill.getBlRemAmt());
					}
				}
			} 

			//sort bill according to custId
			Collections.sort(billList, new Comparator<Bill>() {
				@Override
				public int compare(Bill b1, Bill b2) {
					if (b1.getBlCustId() > b2.getBlCustId()) {
						return 1;
					} else if (b1.getBlCustId() < b2.getBlCustId()) {
						return -1;
					} else {
						return 0;
					}
				}
			});

			List<Map<String, Object>> osBillRptList = new ArrayList<>();
			
			Set<Integer> billIdSets = new HashSet<>();
			for (Bill bill : billList)  {
				billIdSets.add(bill.getBlId());
			}
			
			List<BillDetail> billDetailList = session.createCriteria(BillDetail.class)
					.setFetchMode("bill", FetchMode.EAGER)
					.createAlias("bill", "bl")
					.add(Restrictions.in("bl.blId", billIdSets))
					.list();
			
			Set<Integer> cnmtIdList = new HashSet<>();
			Map<Integer, List<BillDetail>> billBillDetailMap = new HashMap<Integer, List<BillDetail>>();
			for (BillDetail billDetail : billDetailList) {
				Bill bill = billDetail.getBill();
				cnmtIdList.add(billDetail.getBdCnmtId());
				if (billBillDetailMap.get(bill.getBlId()) == null) {
					List<BillDetail> list = new ArrayList<BillDetail>();
					list.add(billDetail);
					billBillDetailMap.put(bill.getBlId(), list);
				} else {
					billBillDetailMap.get(bill.getBlId()).add(billDetail);
				}
			}
			
			List<Cnmt> cnmtList = new ArrayList<>();
			if (!cnmtIdList.isEmpty())
				cnmtList.addAll(
					session.createCriteria(Cnmt.class)
					.add(Restrictions.in(CnmtCNTS.CNMT_ID, cnmtIdList))
					.list());
			
			Map<Integer, Cnmt> cnmtMap = new HashMap<>();
			Set<Integer> stationIdList = new HashSet<>();
			for (Cnmt cnmt : cnmtList) {
				cnmtMap.put(cnmt.getCnmtId(), cnmt);
				stationIdList.add(Integer.parseInt(cnmt.getCnmtFromSt()));
				stationIdList.add(Integer.parseInt(cnmt.getCnmtToSt()));
			}
			
			Map<Integer, Station> stationMap = new HashMap<>();
			List<Station> stationList = new ArrayList<>(); 
			if (!stationIdList.isEmpty())
				stationList.addAll(
						session.createCriteria(Station.class)
							.add(Restrictions.in(StationCNTS.STN_ID, stationIdList))
							.setProjection(Projections.projectionList()
							.add(Projections.property(StationCNTS.STN_ID), StationCNTS.STN_ID)
							.add(Projections.property(StationCNTS.STN_NAME), StationCNTS.STN_NAME))
							.setResultTransformer(Transformers.aliasToBean(Station.class))
							.list());
			
			for (Station station : stationList)
				stationMap.put(station.getStnId(), station);
			
			List<Cnmt_Challan> cnmtChallanList  = new ArrayList<>();
			if (!cnmtIdList.isEmpty())
					cnmtChallanList.addAll(
							session.createCriteria(Cnmt_Challan.class)
								.add(Restrictions.in(Cnmt_ChallanCNTS.Cnmt_Chln_cnmtId, cnmtIdList))
								.setProjection(Projections.projectionList()
										.add(Projections.property(Cnmt_ChallanCNTS.Cnmt_Chln_chlnId), Cnmt_ChallanCNTS.Cnmt_Chln_chlnId)
										.add(Projections.property(Cnmt_ChallanCNTS.Cnmt_Chln_cnmtId),  Cnmt_ChallanCNTS.Cnmt_Chln_cnmtId))
										.setResultTransformer(Transformers.aliasToBean(Cnmt_Challan.class))
										.list());
			
			Map<Integer, Integer> cnmtChallanIdMap = new HashMap<>();
			Set<Integer> challanIdSet = new HashSet<>();
			for(Cnmt_Challan cnmtChallan : cnmtChallanList) {
				cnmtChallanIdMap.put(cnmtChallan.getCnmtId(), cnmtChallan.getChlnId());
				challanIdSet.add(cnmtChallan.getChlnId());
			}
			
			List<Map<String, Object>> arrivalReportList = new ArrayList<Map<String,Object>>();
			if (!challanIdSet.isEmpty())
				arrivalReportList.addAll(
					session.createSQLQuery("select chlnId as chlnId, arId as arId, arDt as arDt FROM arrivalreport"
					+ " inner join challan on chlnCode = archlnCode and chlnId in (:chlnIdList)")
					.setParameter("chlnIdList", challanIdSet)
					.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP)
					.list());
			
			Map<Integer, Object> arrivalReportMap = new HashMap<>();
			
			for (Map<String, Object> map : arrivalReportList) {
				ArrivalReport arrivalReport = new ArrivalReport();
				arrivalReport.setArId(Integer.parseInt(map.get("arId").toString()));
				arrivalReport.setArDt(CodePatternService.getSqlDate(map.get("arDt").toString()));
				arrivalReportMap.put(Integer.parseInt(map.get("chlnId").toString()), arrivalReport);
			}
				
			if (!billList.isEmpty()) {
				for (Bill bill : billList) {
					
					List<BillDetail> billDetails = billBillDetailMap.get(bill.getBlId());
					
					for (BillDetail billDetail : billDetails) {
						Map<String, Object> osBillRpt = new HashMap<>();
						
						osBillRpt.put(BillCNTS.BILL_ID, bill.getBlId());
						osBillRpt.put(BillCNTS.Bill_NO, bill.getBlBillNo());
						osBillRpt.put(BillCNTS.BILL_TYPE, bill.getBlType());
						osBillRpt.put(BillCNTS.BILL_DT, bill.getBlBillDt());
						osBillRpt.put(BillCNTS.BILL_FINAL_TOT, bill.getBlFinalTot());
						osBillRpt.put(BillCNTS.BILL_CUST_ID, bill.getBlCustId());
						if(bill.getBlBillSubDt()!=null)
						{
							osBillRpt.put(BillCNTS.BILL_SUB_DT, bill.getBlBillSubDt());	
						}
						
						
						osBillRpt.put(BillDetailCNTS.BD_ID, billDetail.getBdId());
						osBillRpt.put(BillDetailCNTS.BD_TOT_AMT, billDetail.getBdTotAmt());
						
						if(osby.equalsIgnoreCase("Bill Submission") && bill.getBlBillSubDt()!=null){
						System.out.println(" i am doing aging in Bill Submission for "+bill.getBlBillNo());
						//aging of bill
						long dateDiff = upToDt.getTime() - bill.getBlBillSubDt().getTime();
						dateDiff = dateDiff/(1000 * 60 * 60 * 24);
						if (dateDiff <= 30) {
							osBillRpt.put(BillCNTS.BILL_FINAL_TOT+"os0-30", bill.getBlFinalTotTemp());
						} else if (dateDiff > 30 && dateDiff <= 60) {
							osBillRpt.put(BillCNTS.BILL_FINAL_TOT+"os31-60", bill.getBlFinalTotTemp());
						} else if (dateDiff > 60 && dateDiff <= 90) {
							osBillRpt.put(BillCNTS.BILL_FINAL_TOT+"os61-90", bill.getBlFinalTotTemp());
						} else if (dateDiff > 90 && dateDiff <= 120) {
							osBillRpt.put(BillCNTS.BILL_FINAL_TOT+"os91-120", bill.getBlFinalTotTemp());
						} else if (dateDiff>120) {
							osBillRpt.put(BillCNTS.BILL_FINAL_TOT+"os121-more",bill.getBlFinalTotTemp());
						}
						}
						else
						{
							//aging of bill
							System.out.println(" i am doing aging in Bill Date for "+bill.getBlBillNo());
							long dateDiff = upToDt.getTime() - bill.getBlBillDt().getTime();
							dateDiff = dateDiff/(1000 * 60 * 60 * 24);
							if (dateDiff <= 30) {
								osBillRpt.put(BillCNTS.BILL_FINAL_TOT+"os0-30", bill.getBlFinalTotTemp());
							} else if (dateDiff > 30 && dateDiff <= 60) {
								osBillRpt.put(BillCNTS.BILL_FINAL_TOT+"os31-60", bill.getBlFinalTotTemp());
							} else if (dateDiff > 60 && dateDiff <= 90) {
								osBillRpt.put(BillCNTS.BILL_FINAL_TOT+"os61-90", bill.getBlFinalTotTemp());
							} else if (dateDiff > 90 && dateDiff <= 120) {
								osBillRpt.put(BillCNTS.BILL_FINAL_TOT+"os91-120", bill.getBlFinalTotTemp());
							} else if (dateDiff>120) {
								osBillRpt.put(BillCNTS.BILL_FINAL_TOT+"os121-more",bill.getBlFinalTotTemp());
							}
						}



						double othCharge = 0.0;
						if (billDetail.getBdOthChgList() != null && !billDetail.getBdOthChgList().isEmpty()) {
							for (Map<String, Object> othChgargeMap: billDetail.getBdOthChgList()) {
								othCharge += Double.parseDouble(String.valueOf(othChgargeMap.get("otChgValue")));
							}
						}
						
						osBillRpt.put("othChg", othCharge);
						
						//get corresponding cnmt Detail
						Cnmt cnmt = (Cnmt) cnmtMap.get(billDetail.getBdCnmtId());
						//Cnmt cnmt = (Cnmt) session.get(Cnmt.class, billDetail.getBdCnmtId());
						
						if (cnmt != null) {
							
							System.out.println("********************************");
							System.out.println(cnmt.getCnmtCode());
							System.out.println("********************************");
							osBillRpt.put(CnmtCNTS.CNMT_ID, cnmt.getCnmtId());
							osBillRpt.put(CnmtCNTS.CNMT_CODE, cnmt.getCnmtCode());
							osBillRpt.put(CnmtCNTS.CNMT_DT, cnmt.getCnmtDt());
							osBillRpt.put(CnmtCNTS.CNMT_FREIGHT, cnmt.getCnmtFreight());
							
							//add invoice
							String invoiceNo = "";
							if (cnmt.getCnmtInvoiceNo() != null && !cnmt.getCnmtInvoiceNo().isEmpty()) {
								for (Map<String, Object> cnmtInvoiceNo: cnmt.getCnmtInvoiceNo()) {
									invoiceNo = invoiceNo +" "+cnmtInvoiceNo.get("invoiceNo");
								}
							}
							osBillRpt.put(CnmtCNTS.CNMT_INVOICE_NO, invoiceNo.trim());
							
							//add From station name
							//Station frmStn = (Station) session.get(Station.class, Integer.parseInt(cnmt.getCnmtFromSt()));
							Station frmStn = (Station) stationMap.get(Integer.parseInt(cnmt.getCnmtFromSt()));
							if (frmStn != null) {
								osBillRpt.put("frmStnName", frmStn.getStnName());
							} else {
								osBillRpt.put("frmStnName", "");
							}
							
							//add To station name
							Station toStn = (Station) stationMap.get(Integer.parseInt(cnmt.getCnmtToSt()));
							if (toStn != null) {
								osBillRpt.put("toStnName", toStn.getStnName());
							} else {
								osBillRpt.put("toStnName", "");
							}
							
							Integer chlnId = cnmtChallanIdMap.get(cnmt.getCnmtId());
							ArrivalReport arrivalReport = (ArrivalReport) arrivalReportMap.get(chlnId);
							
							if (arrivalReport != null) {
								osBillRpt.put(ArrivalReportCNTS.AR_ID, arrivalReport.getArId());
								osBillRpt.put(ArrivalReportCNTS.AR_DATE, arrivalReport.getArDt());
							}
						}
						osBillRptList.add(osBillRpt);
					}
				}
			}
			
			osRpt.put("osBillRptList", osBillRptList);
			osRpt.put("custList", custList);
			osRpt.put("branchMap", branchMap);
			osRpt.put("mrList", mrList);
			
			System.out.println("mrListsize="+mrList.size());
			System.out.println("custList"+custList.size());
			 for (Customer customer:custList) {
				System.out.println(customer.getbCode());
			}
			for(MoneyReceipt mr:mrList)
				System.out.println(mr.getMrNo()+" "+mr.getMrRemAmt()+" "+mr.getMrNetAmtForRpt()+" "+mr.getMrFrPDList());
			
			
			temp = 1;
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Exception in outstanding report "+e);
			temp = -1;
		}finally{
			session.clear();
			session.close();
		}
		
		osRpt.put("temp", temp);
		return osRpt;
	}
		
		
	
	@Override
	public Map<String, Object> getRelOsRpt(Map<String, Object> osRptService) {
		// TODO Auto-generated method stub
		Map<String, Object> relOsRpt = new HashMap<>();
		int temp = 0;
	String custgroupId=osRptService.get("custGroupId").toString();
	Date date=Date.valueOf(osRptService.get("upToDt").toString());
	System.out.println("Customer Group Id is  "+custgroupId);
	System.out.println("Up to Date is "+date);
	List<Customer> customerList=new ArrayList<Customer>();
	List<Integer> custId=new ArrayList<Integer>();
	List<Bill> billList=new ArrayList<Bill>();
/*	List<String> billNoList=new ArrayList<String>();
	List<Date> blDateList=new ArrayList<Date>();*/
	Session session=null;
	try {
		 session=this.sessionFactory.openSession();
	
	if(custgroupId!=null)
	{
	
	Criteria criteria=	session.createCriteria(Customer.class,"customer");
	criteria.add(Restrictions.eq(CustomerCNTS.CUST_GROUP_ID, custgroupId));
	customerList=criteria.list();	
	if(!customerList.isEmpty())
	{
		int count=0;
		for (Customer customer : customerList) {
			custId.add(customer.getCustId());
			count++;
		}
		System.out.println(count);
	}
	
	Criteria blcriteria =session.createCriteria(Bill.class,"bill");
	blcriteria.add(Restrictions.in(BillCNTS.BILL_CUST_ID, custId));
	blcriteria.add(Restrictions.eq(BillCNTS.BILL_CANCEL, false));
	blcriteria.add(Restrictions.eqProperty(BillCNTS.BILL_FINAL_TOT, BillCNTS.BILL_REM_AMT));
	blcriteria.add(Restrictions.le(BillCNTS.BILL_DT, date));
	blcriteria.setFetchMode("billDetList", FetchMode.JOIN);
	billList=blcriteria.list();
	
/*	if(!billList.isEmpty())
	{
		int count=0;
		for (Bill bill : billList) {
	billNoList.add(bill.getBlBillNo());
	blDateList.add(bill.getBlBillDt());
			count++;
		}
		System.out.println(count);
	}*/

	}
	Set<Bill> billset=new HashSet<>(billList);
	Set<Integer> cnmtIdList = new HashSet<>();

	for(Bill bill : billset)
	{
	List<BillDetail> billDetails=bill.getBillDetList();
	for (BillDetail billDetail : billDetails) {
	cnmtIdList.add(billDetail.getBdCnmtId());
	}
	}
	System.out.println("Cnmt ID "+cnmtIdList);
	List<Map<String, Object>> resosBillRptList = new ArrayList<>();
	List<Cnmt> cnmtList=new ArrayList<Cnmt>();
	Criteria cnmtcriteria=session.createCriteria(Cnmt.class,"cnmt");
	cnmtcriteria.add(Restrictions.in(CnmtCNTS.CNMT_ID, cnmtIdList));
	cnmtList=cnmtcriteria.list();
	Map<Integer, Cnmt> cnmtMap = new HashMap<>();
	Set<Integer> stationIdList = new HashSet<>();
	Map<Integer, Station> stationMap = new HashMap<>();
	if(!cnmtList.isEmpty())
	{
		for (Cnmt cnmt : cnmtList) {
			cnmtMap.put(cnmt.getCnmtId(), cnmt);
			stationIdList.add(Integer.parseInt(cnmt.getCnmtToSt()));
		}

		List<Station> stationList = new ArrayList<>(); 
		if (!stationIdList.isEmpty())
		{
			stationList.addAll(
					session.createCriteria(Station.class)
						.add(Restrictions.in(StationCNTS.STN_ID, stationIdList))
						.setProjection(Projections.projectionList()
						.add(Projections.property(StationCNTS.STN_ID), StationCNTS.STN_ID)
						.add(Projections.property(StationCNTS.STN_NAME), StationCNTS.STN_NAME))
						.setResultTransformer(Transformers.aliasToBean(Station.class))
						.list());
		}
		
		for (Station station : stationList)
		{
			stationMap.put(station.getStnId(), station);
		}
	}
	
	for (Bill bill : billset) {
	
	List<BillDetail> billDetails=   bill.getBillDetList();

	System.out.println("Bill No "+bill.getBlBillNo());
	for (BillDetail billDetail : billDetails) {
		Map<String, Object> resosBillRpt = new HashMap<>();
		resosBillRpt.put(BillCNTS.Bill_NO, bill.getBlBillNo());
		resosBillRpt.put(BillCNTS.BILL_DT, bill.getBlBillDt());
		resosBillRpt.put(BillCNTS.BILL_CUST_ID, bill.getBlCustId());
		resosBillRpt.put(BillDetailCNTS.BD_TOT_AMT, billDetail.getBdTotAmt());
	
		
	Cnmt cnmt=cnmtMap.get(billDetail.getBdCnmtId());
	if(cnmt!=null)
	{
		resosBillRpt.put(CnmtCNTS.CNMT_CODE, cnmt.getCnmtCode());
		resosBillRpt.put(CnmtCNTS.CNMT_DT, cnmt.getCnmtDt());
		//resosBillRpt.put(CnmtCNTS.CNMT_FREIGHT, Math.round(cnmt.getCnmtFreight()));
		//add To station name
		Station toStn = (Station) stationMap.get(Integer.parseInt(cnmt.getCnmtToSt()));
		if (toStn != null) {
			resosBillRpt.put("toStnName", toStn.getStnName());
		} else {
			resosBillRpt.put("toStnName", "");
		}
	}
	resosBillRptList.add(resosBillRpt);
	}	
	}
	
	
	Collections.sort(resosBillRptList,new Comparator<Map<String, Object>>() {
		@Override
		public int compare(Map<String, Object> o1, Map<String, Object> o2) {
			// TODO Auto-generated method stub
			Date date=Date.valueOf(o1.get(BillCNTS.BILL_DT).toString());
			Date date2=Date.valueOf(o2.get(BillCNTS.BILL_DT).toString());
			return date.compareTo(date2);
		}
	});
	
	
	
	
	System.out.println("Cnmt Id List Size "+cnmtIdList.size());
	System.out.println("Cnmt list size  "+cnmtList.size());
	System.out.println("Station Id List size "+stationIdList.size());
	System.out.println("Station Map Size  "+stationMap.size());
	relOsRpt.put("resosBillRptList", resosBillRptList);	
	
	relOsRpt.put("custList", customerList);
	

	
	
	temp = 1;
	}
	catch (Exception e) {
		// TODO: handle exception
		e.printStackTrace();
		logger.info("Exception in outstanding report "+e);
		temp = -1;
	}
	finally{
		session.clear();
		session.close();
	}
	System.out.println("Cust List Size  "+customerList.size());

	
	relOsRpt.put("temp", temp);
	
	List<Map<String, Object>> maps=(List<Map<String, Object>>)	relOsRpt.get("resosBillRptList");
	
	Iterator<Map<String, Object>> map=	maps.iterator();
	while(map.hasNext())
	{
	Map<String,Object>	 map2=map.next();
	System.out.println(map2.get(CnmtCNTS.CNMT_CODE));
	}	
	


		return relOsRpt;
	}

	
	
	
	@Override
	public Map<String, Object> getRelStockRpt(Map<String, Object> osRptService) {
		// TODO Auto-generated method stub
		System.out.println(osRptService.get("upToDt"));
		System.out.println(osRptService.get("custGroupId"));
		
		Date uptoDate=Date.valueOf(osRptService.get("upToDt").toString());
		String groupId=String.valueOf(osRptService.get("custGroupId"));
		
		List<Map<String, Object>> list=new ArrayList<>();
		Map<String,Object> map=new HashMap<>();
		Session session=null;
		try
		{
			session=this.sessionFactory.openSession();
			
		Criteria custcriteria=	session.createCriteria(Customer.class);
		custcriteria.add(Restrictions.eq(CustomerCNTS.CUST_GROUP_ID,groupId));
		List<Customer> customerList=custcriteria.list();
		
		List<String> custCodeList=new ArrayList<>();
		if(!customerList.isEmpty())
		{
		for (Customer customer :customerList) {
			custCodeList.add(customer.getCustCode());
		}	
		}
		System.out.println(custCodeList);
		System.out.println("Customer Group List "+customerList.size());
		System.out.println("Customer Cust Code Size "+custCodeList.size());
		
			
		Criteria cnmtCriteria=	session.createCriteria(Cnmt.class);
		cnmtCriteria.add(Restrictions.le(CnmtCNTS.CNMT_DT,uptoDate));
		cnmtCriteria.add(Restrictions.isNull(CnmtCNTS.CNMT_BILL_NO));
		cnmtCriteria.add(Restrictions.eq(CnmtCNTS.CNMT_CANCEL, false));
		cnmtCriteria.add(Restrictions.in(CnmtCNTS.CUST_CODE, custCodeList));
	List<Cnmt> cnmtList=	cnmtCriteria.list();
	
	
	Set<String> stationSet=new HashSet<>();
	Map<String, Object> stationMap=new HashMap<>();
	List<Station> stationList = new ArrayList<>();
	if(!cnmtList.isEmpty())
	{
    for (Cnmt cnmt : cnmtList) {
	stationSet.add(cnmt.getCnmtToSt());
    }
    if(!stationSet.isEmpty())
    {
    	Criteria stationcriteria=session.createCriteria(Station.class);
    	stationcriteria.add(Restrictions.in(StationCNTS.STN_CODE, stationSet));
    	stationList=stationcriteria.list();
    	if(!stationList.isEmpty())
    	{
    for (Station station : stationList) {
		stationMap.put(station.getStnCode(), station);
	}
    
    	}	
    } 
	}
	
	Comparator<Cnmt> comparator=new Comparator<Cnmt>() {

		@Override
		public int compare(Cnmt cnmt, Cnmt cnmt2) {
			// TODO Auto-generated method stub
		
		return cnmt.getCnmtDt().compareTo(cnmt2.getCnmtDt());
		}
	};
	Collections.sort(cnmtList,comparator);
	
	for (Cnmt cnmt : cnmtList) {
		Map<String, Object> stockRpt = new HashMap<>();
		if(cnmt!=null){
		stockRpt.put(CnmtCNTS.CNMT_CODE, cnmt.getCnmtCode());
		stockRpt.put(CnmtCNTS.CNMT_DT, cnmt.getCnmtDt());
		stockRpt.put(CnmtCNTS.CNMT_TOT, cnmt.getCnmtTOT());
		Station station=(Station) stationMap.get(cnmt.getCnmtToSt());
		stockRpt.put(StationCNTS.STN_CODE, station.getStnName());
		list.add(stockRpt);
		}	
	}
	map.put("cnmtList", list);
	map.put("temp", 1);
	map.put("customerList", customerList);
	
/*	for(Map<String, Object> map2:list)
	{
		System.out.println(map2.get(CnmtCNTS.CNMT_CODE)+"        "+map2.get(StationCNTS.STN_CODE));
	}*/

	System.out.println("Size of Full List  "+list.size());
	
	System.out.println("Cnmt List Size "+cnmtList.size());
			
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
			map.put("temp", 0);
		
		}
		finally {
			session.clear();
			session.close();
		}
		
	
		return map;
	}
	
	
	@Transactional
	public List<Integer> getRelianceCustomer()
	{
		Session session=this.sessionFactory.openSession();
	Criteria criteria=	session.createCriteria(Customer.class);
	criteria.add(Restrictions.eq(CustomerCNTS.CUST_GROUP_ID, "00001"));
	criteria.setProjection(Projections.property(CustomerCNTS.CUST_ID));
	List<Integer> custId=criteria.list();
	System.out.println("Size of Cust Id "+custId.size());
	System.out.println("Cust Id  "+custId);
		session.close();
		return custId;
	}
	
	@Transactional
	@Override
	public List<Cnmt> getCnmtCodes(String cnmtCode)
	{
		Session session=null;

		List<Cnmt> list=new ArrayList<>();
		try
		{
		session=this.sessionFactory.openSession();
	    List<Integer> custIdList=getRelianceCustomer();
	    List<String> strings=new ArrayList<>();
	    for (Integer integer : custIdList) {
			strings.add(String.valueOf(integer));
		}
	    strings.add("1185");
	
		Criteria criteria=session.createCriteria(Cnmt.class);
		criteria.add(Restrictions.in(CnmtCNTS.CUST_CODE, strings));
		criteria.add(Restrictions.ilike(CnmtCNTS.CNMT_CODE, cnmtCode,MatchMode.ANYWHERE));
        list=criteria.list();
        System.out.println("Size fo cnmt List  "+list.size());
		}
		catch(Exception exception)
		{
		exception.printStackTrace();	
		}
		finally {
		
			session.close();
		}
		
		return list;
			
	}


	@Transactional
	@Override
	public List<Station> getStation()
	{
		Session session=this.sessionFactory.openSession();
		Criteria criteria=session.createCriteria(Station.class);
	    List<Station> stations=	criteria.list();
	    session.close();
		return stations;
		
	}

	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public List<Integer> getChallanData(String cnmtCode) {
		System.out.println("Cnmt COde in CnmtDaoImpl "+cnmtCode);
		Session session=this.sessionFactory.openSession();
     Criteria criteria=  session.createCriteria(Cnmt.class);
     criteria.add(Restrictions.eq(CnmtCNTS.CNMT_CODE, cnmtCode));
     criteria.setProjection(Projections.property(CnmtCNTS.CNMT_ID));
   List<Integer>   cnmtId= criteria.list();
     session.close();
    return cnmtId;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public List<Integer> getChallanId(List<Integer> integers)
	{
		Session session=this.sessionFactory.openSession();
	Criteria  criteria=	session.createCriteria(Cnmt_Challan.class);
	criteria.add(Restrictions.in(Cnmt_ChallanCNTS.Cnmt_Chln_cnmtId, integers));
	criteria.setProjection(Projections.property(Cnmt_ChallanCNTS.Cnmt_Chln_chlnId));
    List<Integer> cnmt_challans=	criteria.list();
    session.close();
	return cnmt_challans;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public List<Challan> getChallanData(List<Integer> cnmt_challans_Id)
	{
		Session session=this.sessionFactory.openSession();
		Criteria criteria=session.createCriteria(Challan.class);
		criteria.add(Restrictions.in(ChallanCNTS.CHLN_ID, cnmt_challans_Id));
		List<Challan> challans=criteria.list();
		session.close();
		return challans;
	}


	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public List<ArrivalReport> getArrivalReoprtData(String challanno) {
		System.out.println("Challan No "+challanno);
		Session session=this.sessionFactory.openSession();
		Criteria criteria=session.createCriteria(ArrivalReport.class);
		criteria.add(Restrictions.eq(ArrivalReportCNTS.AR_CHLN_CODE, challanno));
		List<ArrivalReport> arrivalReports=criteria.list();
		session.close();
		return arrivalReports;
	}


	@Transactional
	@Override
	public void saveRelianceCNStatus(RelianceCNStatus cnStatus)
	{
		System.out.println(cnStatus.getIndentDate()+"    "+cnStatus.getPlacementDate()+"   "+cnStatus.getTripStartDate());
		System.out.println(cnStatus.getCnmtCode()+"      "+cnStatus.getToStation()+"     "+cnStatus.getFromStation());
	Session session=this.sessionFactory.openSession();
	Transaction transaction=session.beginTransaction();
	session.saveOrUpdate(cnStatus);
	transaction.commit();
	session.close();
	}

	@Transactional
	@Override
	public java.util.List<RelianceCNStatus> fetchCNStatus(Date fromdate, Date todate) {
		Session session=this.sessionFactory.openSession();
	Criteria criteria=	session.createCriteria(RelianceCNStatus.class);
	criteria.add(Restrictions.ge("indentDate", fromdate));
	criteria.add(Restrictions.le("indentDate", todate));
	List<RelianceCNStatus> relianceCNStatus=criteria.list();
	System.out.println("reliance cn Status Size is "+relianceCNStatus.size());
	session.close();
	return relianceCNStatus;
	}

	@Transactional
	@Override
	public java.util.List<RelianceCNStatus> getDataFromRelianceCNStatus(String cnmtCode) {
		Session session=this.sessionFactory.openSession();
		Criteria criteria=session.createCriteria(RelianceCNStatus.class);
		criteria.add(Restrictions.eq("cnmtCode",cnmtCode));
	List<RelianceCNStatus>  cnStatus=	criteria.list();
	session.close();
	return cnStatus;	
	}
		
	
	//TODO by manoj
		@SuppressWarnings("unchecked")
		@Override
		public Map<String, Object> getStockRptN(Map<String, Object> stockRptService)
		{
			System.out.println("StockImpl.getStockRpt()");
			Map<String, Object> stockRpt = new HashMap<>();
			Integer branchId = (Integer) stockRptService.get("branchId");
			Integer custId = (Integer) stockRptService.get("custId");
			String custGroupId=(String) stockRptService.get("custGroupId");
			Date upToDt = CodePatternService.getSqlDate(String.valueOf(stockRptService.get("upToDt")));
			System.out.println("Branch Id "+branchId+" custId "+custId+"   custgroupId "+custGroupId+"   uptoDt   "+upToDt);
			
			
			List<Cnmt> cnmtList = new ArrayList<>();
			List<Cnmt> cnmtListTemp1 = new ArrayList<>();
			List<Cnmt> cnmtListTemp2 = new ArrayList<>();
			
			List<Customer> custList = new ArrayList<>();
			List<Branch> brList = new ArrayList<>();
			
		    Session session=null;
		    try {
		    session=this.sessionFactory.openSession();
		    if (custId == null && branchId == null && custGroupId==null) {
				//extract cnmt
				double cnmtFright = 0;
				Criteria cr = session.createCriteria(Cnmt.class);
				cr.add(Restrictions.isNull(CnmtCNTS.CNMT_BILL_NO));
				cr.add(Restrictions.le(CnmtCNTS.CNMT_DT, upToDt));
				cr.add(Restrictions.ne(CnmtCNTS.CNMT_DC, "0 bill"));
				cr.add(Restrictions.eq(CnmtCNTS.CNMT_CANCEL, false));
				cr.add(Restrictions.gt(CnmtCNTS.CNMT_FREIGHT, cnmtFright));
				
				cnmtListTemp1 = cr.list();
				
				Query query = session.createQuery("SELECT cnmt FROM Cnmt cnmt, Bill bill WHERE cnmt.cnmtBillNo = bill.blBillNo and " +
						"bill.blBillDt > :upToDt and cnmt.cnmtDC!=:cnmtDC and cnmt.cnmtDt <= :upToDt ");
				query.setDate("upToDt", upToDt);
				query.setString("cnmtDC", "0 bill");
				cnmtListTemp2 = query.list();
				
				//extract customer
				Criteria custCriteria = session.createCriteria(Customer.class);
				custList = custCriteria.list();
				 
				 
			} else if (custId == null && custGroupId==null) {
				//extract cnmt
				double cnmtFright = 0;
				Criteria cr = session.createCriteria(Cnmt.class);
				cr.add(Restrictions.isNull(CnmtCNTS.CNMT_BILL_NO));
				cr.add(Restrictions.le(CnmtCNTS.CNMT_DT, upToDt));
				cr.add(Restrictions.eq(CnmtCNTS.BRANCH_CODE, String.valueOf(branchId)));
				cr.add(Restrictions.eq(CnmtCNTS.CNMT_CANCEL,false));
				cr.add(Restrictions.ne(CnmtCNTS.CNMT_DC, "0 bill"));
				cr.add(Restrictions.gt(CnmtCNTS.CNMT_FREIGHT, cnmtFright));
				
				cnmtListTemp1 = cr.list();

				Query query = session.createQuery("SELECT cnmt FROM Cnmt cnmt, Bill bill                                 where cnmt.cnmtBillNo = bill.blBillNo and " +
						"bill.blBillDt > :upToDt and cnmt.cnmtDC!=:cnmtDC and cnmt.cnmtDt <= :upToDt and branchCode = :branchId");
				query.setDate("upToDt", upToDt);
				query.setString("cnmtDC", "0 bill");
				query.setString("branchId", String.valueOf(branchId));
				cnmtListTemp2 = query.list();
				
				//extract customer
				Criteria custCriteria = session.createCriteria(Customer.class);
				custCriteria.add(Restrictions.eq(CustomerCNTS.BRANCH_CODE, String.valueOf(branchId)));
				custList = custCriteria.list();
				System.out.println("Size of Customer List "+custList.size());
				
			} else if (branchId == null && custGroupId == null) {
				//extract cnmt
				System.out.println("**********************************************");
				double cnmtFright = 0;
			
				if(custId==1754 || custId==1755 || custId==1756){
					System.out.println("i am in "+custId);
					Criteria cr = session.createCriteria(Cnmt.class);
					cr.add(Restrictions.isNull(CnmtCNTS.CNMT_BILL_NO));
					cr.add(Restrictions.le(CnmtCNTS.CNMT_DT, upToDt));
					cr.add(Restrictions.eq(CnmtCNTS.PR_BL_CODE, String.valueOf(custId)));	
					cr.add(Restrictions.eq(CnmtCNTS.CNMT_CANCEL,false));
					cr.add(Restrictions.ne(CnmtCNTS.CNMT_DC, "0 bill"));
					cr.add(Restrictions.gt(CnmtCNTS.CNMT_FREIGHT, cnmtFright));
					cnmtListTemp1 = cr.list();
				}
				else
				{
					Criteria cr = session.createCriteria(Cnmt.class);
					cr.add(Restrictions.isNull(CnmtCNTS.CNMT_BILL_NO));
					cr.add(Restrictions.le(CnmtCNTS.CNMT_DT, upToDt));
					cr.add(Restrictions.eq(CnmtCNTS.CUST_CODE, String.valueOf(custId)));	
					cr.add(Restrictions.isNull(CnmtCNTS.PR_BL_CODE));
					cr.add(Restrictions.ne(CnmtCNTS.CNMT_DC, "0 bill"));
					System.out.println(" i am in criteria else condition");
					cr.add(Restrictions.eq(CnmtCNTS.CNMT_CANCEL,false));
					cr.add(Restrictions.gt(CnmtCNTS.CNMT_FREIGHT, cnmtFright));
					cnmtListTemp1 = cr.list();
				}
			

				if(custId==1754 || custId==1755 || custId==1756)
				{
					System.out.println("If condition of hql query");
					Query query = session.createQuery("SELECT cnmt FROM Cnmt cnmt, Bill bill WHERE cnmt.cnmtBillNo = bill.blBillNo and " +
							"bill.blBillDt > :upToDt and cnmt.cnmtDC!=:cnmtDC and cnmt.cnmtDt <= :upToDt and prBlCode = :custId and branchCode = :branchId");
					query.setDate("upToDt", upToDt);
					query.setString("custId", String.valueOf(custId));
					query.setString("cnmtDC", "0 bill");
					query.setString("branchId", String.valueOf(branchId));
					cnmtListTemp2 = query.list();	
				}
				else
				{
					System.out.println("Else condition of hql query");
					Query query = session.createQuery("SELECT cnmt FROM Cnmt cnmt, Bill bill WHERE cnmt.cnmtBillNo = bill.blBillNo and " +
							"bill.blBillDt > :upToDt and cnmt.cnmtDC!=:cnmtDC and cnmt.cnmtDt <= :upToDt and custCode = :custId and prBlCode is null and branchCode = :branchId");
					query.setDate("upToDt", upToDt);
					query.setString("custId", String.valueOf(custId));
					query.setString("cnmtDC", "0 bill");
					query.setString("branchId", String.valueOf(branchId));
					cnmtListTemp2 = query.list();
					
				}
				
				//extract customer
				Customer customer = (Customer) session.get(Customer.class, custId);
				custList.add(customer);
				
			}else if(custGroupId!=null){
				double cnmtFright = 0;
				System.out.println("custGroupId.."+custGroupId);
				Criteria criteria=session.createCriteria(Customer.class);
				 criteria.add(Restrictions.eq(CustomerCNTS.CUST_GROUP_ID,custGroupId));
				 custList=criteria.list();
				List<String> custCodeList=new ArrayList<String>();
				  for(Customer cust:custList){
					 custCodeList.add(cust.getCustCode());  
				  }
				  
	           List<String> custCodeList2=new ArrayList<>();
				if(!custCodeList.isEmpty()){
					
					for (String string : custCodeList) {
						if(string.equals("1754") || string.equals("1755") || string.equals("1756"))
						{
							custCodeList2.add(string);	
						}
					}
					if(!custCodeList2.isEmpty())
					{
						Criteria cr = session.createCriteria(Cnmt.class);
						cr.add(Restrictions.isNull(CnmtCNTS.CNMT_BILL_NO));
						cr.add(Restrictions.le(CnmtCNTS.CNMT_DT, upToDt));
						cr.add(Restrictions.in(CnmtCNTS.PR_BL_CODE,custCodeList2));
						cr.add(Restrictions.eq(CnmtCNTS.CNMT_CANCEL,false));
						cr.add(Restrictions.ne(CnmtCNTS.CNMT_DC, "0 bill"));
						cr.add(Restrictions.gt(CnmtCNTS.CNMT_FREIGHT, cnmtFright));
						cnmtListTemp1 = cr.list();
						
						Query query = session.createQuery("SELECT cnmt FROM Cnmt cnmt, Bill bill WHERE cnmt.cnmtBillNo = bill.blBillNo and " +
								"bill.blBillDt > :upToDt and  cnmt.cnmtDC!=:cnmtDC and cnmt.cnmtDt <= :upToDt and cnmt.prBlCode in(:custListId)");
						query.setDate("upToDt", upToDt);
						query.setString("cnmtDC", "0 bill");
						query.setParameterList("custListId",custCodeList2);
						cnmtListTemp2 = query.list();
						custCodeList.removeAll(custCodeList2);
					}
					
					
				Criteria cr = session.createCriteria(Cnmt.class);
				cr.add(Restrictions.isNull(CnmtCNTS.CNMT_BILL_NO));
				cr.add(Restrictions.le(CnmtCNTS.CNMT_DT, upToDt));
				cr.add(Restrictions.in(CnmtCNTS.CUST_CODE,custCodeList));
				cr.add(Restrictions.isNull(CnmtCNTS.PR_BL_CODE));
				cr.add(Restrictions.eq(CnmtCNTS.CNMT_CANCEL,false));
				cr.add(Restrictions.ne(CnmtCNTS.CNMT_DC, "0 bill"));
				cr.add(Restrictions.gt(CnmtCNTS.CNMT_FREIGHT, cnmtFright));
				cnmtListTemp1.addAll(cr.list());
				
				Query query = session.createQuery("SELECT cnmt FROM Cnmt cnmt, Bill bill WHERE cnmt.cnmtBillNo = bill.blBillNo and " +
						"bill.blBillDt > :upToDt and cnmt.prBlCode is null and  cnmt.cnmtDC!=:cnmtDC and cnmt.cnmtDt <= :upToDt and custCode in(:custListId)");
				query.setDate("upToDt", upToDt);
				query.setString("cnmtDC", "0 bill");
				query.setParameterList("custListId",custCodeList);
				cnmtListTemp2.addAll(query.list());
				}
				//extract customer
				//Customer customer = (Customer) session.get(Customer.class, custId);
				//custList.add(customer);
					
			}else {
				//extract cnmt
				double cnmtFright = 0;
				if(custId==1754 || custId==1755 || custId==1756)
				{
					System.out.println("In if condition **********");
				Criteria cr = session.createCriteria(Cnmt.class);
				cr.add(Restrictions.isNull(CnmtCNTS.CNMT_BILL_NO));
				cr.add(Restrictions.le(CnmtCNTS.CNMT_DT, upToDt));
				cr.add(Restrictions.eq(CnmtCNTS.PR_BL_CODE, String.valueOf(custId)));
				cr.add(Restrictions.eq(CnmtCNTS.BRANCH_CODE, String.valueOf(branchId)));
				cr.add(Restrictions.eq(CnmtCNTS.CNMT_CANCEL,false));
				cr.add(Restrictions.ne(CnmtCNTS.CNMT_DC, "0 bill"));
				cr.add(Restrictions.gt(CnmtCNTS.CNMT_FREIGHT, cnmtFright));
				cnmtListTemp1 = cr.list();
				}
				else
				{
					System.out.println("In else condition **********");
					Criteria cr = session.createCriteria(Cnmt.class);
					cr.add(Restrictions.isNull(CnmtCNTS.CNMT_BILL_NO));
					cr.add(Restrictions.le(CnmtCNTS.CNMT_DT, upToDt));
					cr.add(Restrictions.eq(CnmtCNTS.CUST_CODE, String.valueOf(custId)));
					cr.add(Restrictions.isNull(CnmtCNTS.PR_BL_CODE));
					cr.add(Restrictions.ne(CnmtCNTS.CNMT_DC, "0 bill"));
					cr.add(Restrictions.eq(CnmtCNTS.BRANCH_CODE, String.valueOf(branchId)));
					cr.add(Restrictions.eq(CnmtCNTS.CNMT_CANCEL,false));
					cr.add(Restrictions.gt(CnmtCNTS.CNMT_FREIGHT, cnmtFright));
					cnmtListTemp1 = cr.list();
					
				}
				
				if(custId==1754 || custId==1755 || custId==1756)
				{
					System.out.println("In if condition **********");
					Query query = session.createQuery("SELECT cnmt FROM Cnmt cnmt, Bill bill WHERE cnmt.cnmtBillNo = bill.blBillNo and " +
							"bill.blBillDt > :upToDt and cnmt.cnmtDC!=:cnmtDC and cnmt.cnmtDt <= :upToDt and prBlCode = :custId and branchCode = :branchId");
					query.setDate("upToDt", upToDt);
					query.setString("custId", String.valueOf(custId));
					query.setString("cnmtDC", "0 bill");
					query.setString("branchId", String.valueOf(branchId));
					cnmtListTemp2 = query.list();
				}
				else
				{
					System.out.println("In else condition **********");
					Query query = session.createQuery("SELECT cnmt FROM Cnmt cnmt, Bill bill WHERE cnmt.cnmtBillNo = bill.blBillNo and " +
							"bill.blBillDt > :upToDt and cnmt.cnmtDC!=:cnmtDC and cnmt.cnmtDt <= :upToDt and custCode = :custId and prBlCode is null and branchCode = :branchId");
					query.setDate("upToDt", upToDt);
					query.setString("cnmtDC", "0 bill");
					query.setString("custId", String.valueOf(custId));
			
					query.setString("branchId", String.valueOf(branchId));
					cnmtListTemp2 = query.list();
					
				}
					
			
				//extract customer
				Customer customer = (Customer) session.get(Customer.class, custId);
				custList.add(customer);
				
			}
	 
		    //      ***********  Fetching Records from Station table ********//
			Criteria stnCriteria = session.createCriteria(Station.class);
			List<Station> stationList = stnCriteria.list();	
			
		    //      ***********  Fetching Records from Branch table ********//
			Criteria brCriteria = session.createCriteria(Branch.class);
			brCriteria.add(Restrictions.eq(BranchCNTS.IS_OPEN, "yes"));
			brList = brCriteria.list();
			
			
			System.out.println("Station List "+stationList.size());
			System.out.println("Size of Br List "+brList.size());
			System.out.println("custSize="+custList.size());
			System.out.println("Size of CnmtLIstTemp1 is "+cnmtListTemp1.size());
			System.out.println("Size of CnmtLIstTemp2 is "+cnmtListTemp2.size());
			
			
			// Adding data in Cnmt List 
			cnmtList.addAll(cnmtListTemp1);
			cnmtList.addAll(cnmtListTemp2);
			
			
			System.out.println("final List Size: "+cnmtList.size());
			
			//sorting
			Collections.sort(cnmtList, new Comparator<Cnmt>() {

				@Override
				public int compare(Cnmt cnmt1, Cnmt cnmt2) {
					return cnmt1.getCnmtDt().compareTo(cnmt2.getCnmtDt());
				}
			});
			
			if(cnmtList.size()>0)
			{
			
			List<Customer> customerList=new ArrayList<>();
			Map<Integer, Object> map=new HashMap<>();
			List<Map<String, Object>> cnmtMapList = new ArrayList<>();
			
			// *********************** Fetching Data from Customer Table ********************//
			Criteria custcriteria=session.createCriteria(Customer.class);
	        customerList=  custcriteria.list();
	        System.out.println("Customer List Size  "+customerList.size());
	        if(!customerList.isEmpty())
	        {
	        	//********************** Putting data into Map from customer ***************//
	        for (Customer customer : customerList) {
				map.put(customer.getCustId(), customer);
			}
	        System.out.println("Customer Map  "+map.size());
	        }
	        
			
			
			List<Cnmt_Challan> chlnList=new ArrayList<Cnmt_Challan>();
			List<Integer> cnmtIdList=new ArrayList<Integer>();
		
			Map<Object, Object> challanmap=new HashMap<Object,Object>();
			Map<String, Object> cnmtMap2 = new HashMap<>();
			
			// ************ if Cnmt list is not empty than adding Cnmt Id into cnmtId List ***************
			if(!cnmtList.isEmpty())
			{
				
				for (Cnmt cnmt : cnmtList) {
					// adding in cnmtId List
					cnmtIdList.add(cnmt.getCnmtId());
					
				}
			}
				System.out.println(cnmtIdList);
				System.out.println("Cnmt Id List Size "+cnmtIdList.size());
				
				// ************** Fetching Data From Cnmt_Challan Table ************************//
			Criteria chlncriteria=	session.createCriteria(Cnmt_Challan.class,"cnmt_challan");
			chlncriteria.add(Restrictions.in(Cnmt_ChallanCNTS.Cnmt_Chln_cnmtId, cnmtIdList));
			chlnList=chlncriteria.list();
			
			System.out.println("Cnmt_Challan List Size   "+chlnList.size());
			
			
			List<Integer> challanId=new ArrayList<>();
			Map<Object, Object> cnmtchallanmap=new HashMap<Object,Object>();
			MultiValueMap<Object, Object> multiValueMap=new LinkedMultiValueMap<>();
			

			for (Cnmt_Challan cnmt_Challan : chlnList) {
				// adding challan id into challanId List from challan cnmt list
				challanId.add(cnmt_Challan.getChlnId());
				// adding data into cnmtchallan Map   
				cnmtchallanmap.put(cnmt_Challan.getCnmtId(), cnmt_Challan.getChlnId());
				multiValueMap.add(cnmt_Challan.getCnmtId(), cnmt_Challan.getChlnId());

			
			}
			System.out.println(challanId);
			System.out.println("MultiValue Map  "+multiValueMap);
		System.out.println("Challan Id lIst size from CNmt_Challan "+challanId.size());

			// ************************* Fetching Data From Challan Table **********//
			Criteria chlncriteria2=session.createCriteria(Challan.class);
			chlncriteria2.add(Restrictions.in(ChallanCNTS.CHLN_ID, challanId));
		    List<Challan> challanList=chlncriteria2.list();
			System.out.println("Challan List  "+challanList.size());
			
			
			List<String> challancodeList=new ArrayList<>();
			List<Integer> challanIdList=new ArrayList<>();
			List<Integer> chlnArId=new ArrayList<>();
		    for (Challan challan : challanList) {
		    	// Putting challan into challan Map 
				challanmap.put(challan.getChlnId(), challan);
				// adding challancode into challancode list
				challancodeList.add(challan.getChlnCode());
				
				// adding challan id into challan Id List
				challanIdList.add(challan.getChlnId());
				
				// adding chlnArId in chlnArId list
				chlnArId.add(challan.getChlnArId());
			}
		    System.out.println("Challan Id List Size "+challanIdList.size());
		

		    
		    // **************** Fetching Data from ChallanDetail Table ***************//
		    Criteria chdCriteria = session.createCriteria(ChallanDetail.class);
			List<ChallanDetail> cdlist=chdCriteria.add(Restrictions.in(ChallanDetailCNTS.CHD_CHALLAN_CODE,challancodeList)).list();
		System.out.println("CDLIst  "+cdlist.size());
		    List<String> chdbrcodeList=new ArrayList<>();
			Map<Object,Object> chdMap=new HashMap<>();
			List<String> chdowncodeList=new ArrayList<>();
			for (ChallanDetail challanDetail : cdlist) {
				// putting data into ChallanDetail Map 
				chdMap.put(challanDetail.getChdChlnCode(), challanDetail);
				
				// adding ChallanDetail BrCode into chdbrCode List
				chdbrcodeList.add(challanDetail.getChdBrCode());
				
				// Adding challanDetail Own Code into chdownCode List
				chdowncodeList.add(challanDetail.getChdOwnCode());
			}
			
			
			
			// Fetching Data From Broker Table
			Criteria brkCriteria = session.createCriteria(Broker.class);
			brkCriteria.add(Restrictions.in(BrokerCNTS.BRK_CODE, chdbrcodeList));
			List<Broker> brokerList=new ArrayList<>();
			brokerList=brkCriteria.list();
			Map<String, Object> brkMap=new HashMap<>();
			for(Broker broker:brokerList)
			{
				// Putting Data Into Broker Map
			brkMap.put(broker.getBrkCode(), broker);
			}
			
			// Fetching Data From Owner Table
			Criteria ownCriteria = session.createCriteria(Owner.class);
			ownCriteria.add(Restrictions.in(OwnerCNTS.OWN_CODE, chdowncodeList));
			List<Owner> ownList=ownCriteria.list();
			Map<String,Object> ownMap=new HashMap<>();
			for (Owner owner : ownList) {
				// Putting Data into ownMap form ownerList
				ownMap.put(owner.getOwnCode(), owner);
			}
			
			//**************   fetching data from Arrival Report table **************//
		    Criteria arcriteria=session.createCriteria(ArrivalReport.class);
		    arcriteria.add(Restrictions.in(ArrivalReportCNTS.AR_ID, chlnArId));
		    List<ArrivalReport> arrivalReportList=arcriteria.list();
		    
		    
		    Map<Integer,Object> arMap=new HashMap<>();
		    for (ArrivalReport arrivalReport : arrivalReportList) {
		    	// putting data into ArrivalReport Map
				arMap.put(arrivalReport.getArId(), arrivalReport);
			}
		    
		    // **************  Fetching Data From lhpvbal_challan  ***************//
		SQLQuery lhpvBalQuery = session.createSQLQuery("SELECT chlnId,lbId FROM lhpvbal_challan WHERE chlnId in :chlnId");
			lhpvBalQuery.setParameterList("chlnId", challanIdList);
			List<Object[]> lhpvBalIdList = lhpvBalQuery.list();

		    Map<Object,Object> lhpvBalChlnId=new HashMap<>();
		    List<Integer> lhpvBalChlnLbId=new ArrayList<>();
		    for (Object[] objects : lhpvBalIdList) {
				lhpvBalChlnId.put(objects[0], objects[1]);
				lhpvBalChlnLbId.add(Integer.parseInt(String.valueOf(objects[1])));
			}

		    
		    
		    // fetching LhpvBal
		    List<LhpvBal> lhpvBalList =new ArrayList<>();
		    Map<Integer, Object> lhpvMap=new HashMap<>();
		    List<Integer> lhpvIdList=new ArrayList<>();
		    if(!lhpvBalIdList.isEmpty())
		    {
		    Criteria criteria=session.createCriteria(LhpvBal.class);
		    criteria.add(Restrictions.in(LhpvBalCNTS.LHPV_BAL_ID, lhpvBalChlnLbId));
		    lhpvBalList=criteria.list();

		    if(!lhpvBalList.isEmpty())
		    {
		for (LhpvBal lhpvBal : lhpvBalList) {
			lhpvMap.put(lhpvBal.getLbId(), lhpvBal);
			lhpvIdList.add(lhpvBal.getLbId());
		    }
		    }
		    }

		    Map<Integer, Object> lhpvCashStmtMap=new HashMap<>();
		    List<String> bcodeList=new ArrayList<>();
		    List<Date> lcsLhpvDtList=new ArrayList<>();
		    if(!lhpvIdList.isEmpty())
		    {
		    Criteria criteria=session.createCriteria(LhpvCashSmry.class);
		    criteria.add(Restrictions.in(LhpvCashSmryCNTS.LB_ID, lhpvIdList));
		   List<LhpvCashSmry> cashSmries= criteria.list();

		   if(!cashSmries.isEmpty())
		   {
			 for (LhpvCashSmry lhpvCashSmry : cashSmries) {
				lhpvCashStmtMap.put(lhpvCashSmry.getLbId(), lhpvCashSmry);
				bcodeList.add(lhpvCashSmry.getbCode());
				lcsLhpvDtList.add(lhpvCashSmry.getLcsLhpvDt());
			}  
		   }
		    }
		 
	         Map<String,Object> lhpvStatusMap=new HashMap<>();
		    if(!bcodeList.isEmpty() && !lcsLhpvDtList.isEmpty())
		    {
		
		    Criteria criteria=session.createCriteria(LhpvStatus.class);
		    criteria.add(Restrictions.in(LhpvStatusCNTS.B_CODE, bcodeList));
		    criteria.add(Restrictions.in(LhpvStatusCNTS.LS_DT, lcsLhpvDtList));
		  List<LhpvStatus>  lhpvStatusList=  criteria.list();
		  if(!lhpvStatusList.isEmpty())
		  {
			for (LhpvStatus lhpvStatus : lhpvStatusList) {
				lhpvStatusMap.put(lhpvStatus.getbCode()+","+lhpvStatus.getLsDt(), lhpvStatus);
			}  
		  }
		    }
			
			
	       
				for(Cnmt cnmt:cnmtList)
				{
				List<Object> chlnIDLits=multiValueMap.get(cnmt.getCnmtId());
	 
				if(chlnIDLits==null)
				{
				cnmtMap2=getCnmtDetailN(cnmt, brList, map,stationList, session, false, upToDt);	
	   System.out.println(" i am in chlnIDLits== null "+chlnIDLits);

				}
				else if(chlnIDLits.size()==1)
				{	
					System.out.println(" i am in chlnIDLits size "+chlnIDLits.size());
		
					cnmtMap2 = getCnmtDetailN(cnmt, brList,map, stationList, session, false, upToDt);	

	            
			       Object chlnId=    cnmtchallanmap.get(cnmt.getCnmtId());
			    	List<Object> list=multiValueMap.get(cnmt.getCnmtId());

					if(chlnId!=null)
					{
				        System.out.println("ChlnId     ***********  "+chlnId);
						Challan challan=(Challan) challanmap.get(chlnId);
						System.out.println("Challan COde    "+challan.getChlnCode());
						cnmtMap2.put(ChallanCNTS.CHLN_ID, challan.getChlnId());
						cnmtMap2.put(ChallanCNTS.CHALLAN_CODE, challan.getChlnCode());
						cnmtMap2.put(ChallanCNTS.CHLN_DT, challan.getChlnDt());
						
						ChallanDetail challanDetail=	(ChallanDetail) chdMap.get(challan.getChlnCode());
						System.out.println("ChallanDetail Broker Code     "+challanDetail.getChdBrCode());
						Broker broker=(Broker) brkMap.get(challanDetail.getChdBrCode());
						/*if(broker!=null)
						{
							System.out.println("Broker Name   "+broker.getBrkName());
							cnmtMap2.put("ownBrkName", broker.getBrkName());
							cnmtMap2.put("ownBrkPhNo", challanDetail.getChdBrMobNo());	
						}*/
						
						//if (challanDetail.getChdPanHdrType() == null || challanDetail.getChdPanHdrType().equalsIgnoreCase("owner")) {
							//consider owner
							Owner owner=(Owner)ownMap.get(challanDetail.getChdOwnCode());
							if (owner!=null) {
								System.out.println("Owner Name  "+owner.getOwnName());
								cnmtMap2.put("ownName", owner.getOwnName());
								cnmtMap2.put("ownPhNo", challanDetail.getChdOwnMobNo());
							}
						//}
						/*else
						{*/
							Broker broker2=(Broker) brkMap.get(challanDetail.getChdBrCode());
							if(broker2!=null)
							{
								cnmtMap2.put("brkName", broker2.getBrkName());
								cnmtMap2.put("brkPhNo", challanDetail.getChdBrMobNo());	
							}	
						//}
						if (challan.getChlnLryNo() != null && !challan.getChlnLryNo().equalsIgnoreCase("")) {
							//get lry no from challan
						
							cnmtMap2.put(ChallanCNTS.CHLN_LRY_NO, challan.getChlnLryNo());
						} else {
							//get lry no from challan detail
							cnmtMap2.put(ChallanCNTS.CHLN_LRY_NO, challanDetail.getChdRcNo());
						}
						
						//get arrival report
			System.out.println("Challan Ar Id    "+challan.getChlnArId());
						if (challan.getChlnArId() > 0) {
										ArrivalReport ar=(ArrivalReport) arMap.get(challan.getChlnArId());
							if (ar != null) {
								cnmtMap2.put(ArrivalReportCNTS.AR_ID, ar.getArId());
								cnmtMap2.put(ArrivalReportCNTS.AR_CODE, ar.getArCode());
								Date issueDt = ar.getArIssueDt();
								if(issueDt == null)									
									cnmtMap2.put(ArrivalReportCNTS.AR_DATE, ar.getArDt());
								else
									cnmtMap2.put(ArrivalReportCNTS.AR_DATE, issueDt);
								Memo memo = ar.getMemo();
								if(memo != null){
									cnmtMap2.put(MemoCNTS.MEMO_MEMO_NO, memo.getMemoNo());
									cnmtMap2.put(MemoCNTS.MEMO_MEMO_DATE, memo.getMemoDate());
									cnmtMap2.put(MemoCNTS.MEMO_FROM_BRANCH, memo.getFromBranch());
									cnmtMap2.put(MemoCNTS.MEMO_TO_BRANCH, memo.getToBranch());
								}
							}
						}
						
						Object lbid=lhpvBalChlnId.get(challan.getChlnId());
					System.out.println("LbID  "+lbid);
						if(lbid!=null)
						{
						LhpvBal lhpvBal=(LhpvBal) lhpvMap.get(lbid);
							if (lhpvBal.getLbPayMethod() == 'C' || lhpvBal.getLbPayMethod() == 'C') {
		
								LhpvCashSmry lhpvCashSmryList=(LhpvCashSmry) lhpvCashStmtMap.get(lhpvBal.getLbId());
								
								if (lhpvCashSmryList!=null) {

									LhpvStatus lhpvStatuList=(LhpvStatus) lhpvStatusMap.get(lhpvCashSmryList.getbCode()+","+lhpvCashSmryList.getLcsLhpvDt());
									
									if (lhpvStatuList!=null) {
						
										cnmtMap2.put(LhpvStatusCNTS.LS_NO, lhpvStatuList.getLsNo()+"("+lhpvStatuList.getbCode()+")");
										cnmtMap2.put(LhpvStatusCNTS.LS_DT, lhpvStatuList.getLsDt());
									}
								}
							} else {
								//add lhpv no and date
								LhpvStatus lhpvStatus  = lhpvBal.getLhpvStatus();
								if(lhpvStatus != null){
									cnmtMap2.put(LhpvStatusCNTS.LS_NO, lhpvStatus.getLsNo()+"("+lhpvStatus.getbCode()+")");
									cnmtMap2.put(LhpvStatusCNTS.LS_DT, lhpvStatus.getLsDt());
								}
							}
						}
					}
			
					
				// ******************************************************* Done ******************************
				

			    	
				}
				
				else if(chlnIDLits.size()>1)
				{
					//System.out.println("i am here");
				
					cnmtMap2 = getCnmtDetailN(cnmt, brList,map, stationList, session, false, upToDt);	
				
					//System.out.println(chlnIDLits.get(0));
						Challan challan=(Challan) challanmap.get(chlnIDLits.get(0));
						cnmtMap2.put(ChallanCNTS.CHLN_ID, challan.getChlnId());
						cnmtMap2.put(ChallanCNTS.CHALLAN_CODE, challan.getChlnCode());
						cnmtMap2.put(ChallanCNTS.CHLN_DT, challan.getChlnDt());
						
						ChallanDetail challanDetail=	(ChallanDetail) chdMap.get(challan.getChlnCode());
						Broker broker=(Broker) brkMap.get(challanDetail.getChdBrCode());
						if(broker!=null)
						{
						//	System.out.println(" I am in broker ");
							cnmtMap2.put("brkName", broker.getBrkName());
							cnmtMap2.put("brkPhNo", challanDetail.getChdBrMobNo());	
						}
						
						//if (challanDetail.getChdPanHdrType() == null || challanDetail.getChdPanHdrType().equalsIgnoreCase("owner")) {
							//consider owner
							Owner owner=(Owner)ownMap.get(challanDetail.getChdOwnCode());
							if (owner!=null) {
				//System.out.println(" I am in Owner");
								cnmtMap2.put("ownName", owner.getOwnName());
								cnmtMap2.put("ownPhNo", challanDetail.getChdOwnMobNo());
							}
						/*}
						else
						{
							Broker broker2=(Broker) brkMap.get(challanDetail.getChdBrCode());
							if(broker2!=null)
							{
								cnmtMap2.put("brkName", broker2.getBrkName());
								cnmtMap2.put("brkPhNo", challanDetail.getChdBrMobNo());	
							}	
						}*/
						if (challan.getChlnLryNo() != null && !challan.getChlnLryNo().equalsIgnoreCase("")) {
							//get lry no from challan
							//System.out.println(" I am in Lorry no ");
							cnmtMap2.put(ChallanCNTS.CHLN_LRY_NO, challan.getChlnLryNo());
						} else {
							//get lry no from challan detail
							cnmtMap2.put(ChallanCNTS.CHLN_LRY_NO, challanDetail.getChdRcNo());
						}
						//get arrival report
						if (challan.getChlnArId() > 0) {
							//System.out.println("arId================>>>>>: "+challan.getChlnArId());
							//ArrivalReport ar = (ArrivalReport) session.get(ArrivalReport.class, challan.getChlnArId());
							ArrivalReport ar=(ArrivalReport) arMap.get(challan.getChlnArId());
							if (ar != null) {
								cnmtMap2.put(ArrivalReportCNTS.AR_ID, ar.getArId());
								cnmtMap2.put(ArrivalReportCNTS.AR_CODE, ar.getArCode());
								cnmtMap2.put(ArrivalReportCNTS.AR_DATE, ar.getArDt());
					
							}
						}
						
						Object lbid=lhpvBalChlnId.get(challan.getChlnId());
						//System.out.println(lbid);
						if(lbid!=null)
						{
							//LhpvBal lhpvBal = (LhpvBal) session.get(LhpvBal.class, lhpvBalIdList.get(lhpvBalIdList.size()-1));//get last lhpv balance
							LhpvBal lhpvBal=(LhpvBal) lhpvMap.get(lbid);
							if (lhpvBal.getLbPayMethod() == 'C' || lhpvBal.getLbPayMethod() == 'C') {
					
								LhpvCashSmry lhpvCashSmryList=(LhpvCashSmry) lhpvCashStmtMap.get(lhpvBal.getLbId());
								
								if (lhpvCashSmryList!=null) {

									LhpvStatus lhpvStatuList=(LhpvStatus) lhpvStatusMap.get(lhpvCashSmryList.getbCode()+","+lhpvCashSmryList.getLcsLhpvDt());
									
									if (lhpvStatuList!=null) {
						
										cnmtMap2.put(LhpvStatusCNTS.LS_NO, lhpvStatuList.getLsNo()+"("+lhpvStatuList.getbCode()+")");
										cnmtMap2.put(LhpvStatusCNTS.LS_DT, lhpvStatuList.getLsDt());
									}
								}
							} else {
								//add lhpv no and date
								LhpvStatus lhpvStatus  = lhpvBal.getLhpvStatus();
								if(lhpvStatus != null){
									cnmtMap2.put(LhpvStatusCNTS.LS_NO, lhpvStatus.getLsNo()+"("+lhpvStatus.getbCode()+")");
									cnmtMap2.put(LhpvStatusCNTS.LS_DT, lhpvStatus.getLsDt());
								}
							}
						}
				
				
					
						for(int i = 1; i < chlnIDLits.size(); i++)
						{
					
							Map<String, Object> cnmtTempMap = getCnmtDetailN(cnmt, brList,map, stationList, session, true, upToDt);
						
							
							//add Challan Detail
						//	Challan chln = (Challan) session.get(Challan.class, chlnIdList.get(i));
							Challan chln=(Challan) challanmap.get(chlnIDLits.get(i));
							cnmtTempMap.put(ChallanCNTS.CHLN_ID, chln.getChlnId());
							cnmtTempMap.put(ChallanCNTS.CHALLAN_CODE, chln.getChlnCode());
							cnmtTempMap.put(ChallanCNTS.CHLN_DT, chln.getChlnDt());
							
							//add own/brk
							ChallanDetail chlnDetail = (ChallanDetail) chdMap.get(chln.getChlnCode());
								//if (chlnDetail.getChdPanHdrType() == null || chlnDetail.getChdPanHdrType().equalsIgnoreCase("owner")) {
									//consider owner
								
									Owner owner2=(Owner)ownMap.get(chlnDetail.getChdOwnCode());
									if (owner2!=null) {
						//System.out.println(" I am in Owner");
										cnmtTempMap.put("ownName", owner2.getOwnName());
										cnmtTempMap.put("ownPhNo", chlnDetail.getChdOwnMobNo());
									}
								//} else {
									//consider broker
									Broker broker3=(Broker) brkMap.get(chlnDetail.getChdBrCode());
									if(broker3!=null)
									{
										cnmtTempMap.put("brkName", broker3.getBrkName());
										cnmtTempMap.put("brkPhNo", chlnDetail.getChdBrMobNo());	
									}
							//	}
						
							
							if (chln.getChlnLryNo() != null && ! "".equalsIgnoreCase(chln.getChlnLryNo())) {
								//get lry no from challan
								cnmtTempMap.put(ChallanCNTS.CHLN_LRY_NO, chln.getChlnLryNo());
							} else {
								//get lry no from challan detail
								cnmtTempMap.put(ChallanCNTS.CHLN_LRY_NO, chlnDetail.getChdRcNo());
								
							}
							
							//get arrival report
							if (chln.getChlnArId() > 0) {
								ArrivalReport ar=(ArrivalReport) arMap.get(chln.getChlnArId());
								if (ar != null) {
									cnmtTempMap.put(ArrivalReportCNTS.AR_ID, ar.getArId());
									cnmtTempMap.put(ArrivalReportCNTS.AR_CODE, ar.getArCode());
									cnmtTempMap.put(ArrivalReportCNTS.AR_DATE, ar.getArDt());

								}
							}
							
							//get lhpv bal
							Object lbid2=lhpvBalChlnId.get(chln.getChlnId());
						
							if (lbid2!=null) {
								LhpvBal lhpvBal = (LhpvBal) lhpvMap.get(lbid2);
								
								if (lhpvBal.getLbPayMethod() == 'C' || lhpvBal.getLbPayMethod() == 'C') {
						
									LhpvCashSmry lhpvCashSmryList=(LhpvCashSmry) lhpvCashStmtMap.get(lhpvBal.getLbId());
								//	System.out.println("LhpvCashSmry Chln Code     "+lhpvCashSmryList.getLcsChlnCode()+"     bcode      "+lhpvCashSmryList.getbCode()+"       lbid   "+lhpvCashSmryList.getLcsId()+"   dt    "+lhpvCashSmryList.getLcsLhpvDt());
									if (lhpvCashSmryList!=null) {
									
										LhpvStatus lhpvStatuList=(LhpvStatus) lhpvStatusMap.get(lhpvCashSmryList.getbCode()+","+lhpvCashSmryList.getLcsLhpvDt());
									//	System.out.println("LhpvStatus LsNo   "+lhpvStatuList.getLsNo()+"     LSDt   "+lhpvStatuList.getLsDt());
										if (lhpvStatuList!=null) {
											//add lhpv no and date
											cnmtTempMap.put(LhpvStatusCNTS.LS_NO, lhpvStatuList.getLsNo()+"("+lhpvStatuList.getbCode()+")");
											cnmtTempMap.put(LhpvStatusCNTS.LS_DT, lhpvStatuList.getLsDt());
										}
									}
								} else {
									//add lhpv no and date
									LhpvStatus lhpvStatus  = lhpvBal.getLhpvStatus();
									if(lhpvStatus !=  null){
										cnmtTempMap.put(LhpvStatusCNTS.LS_NO, lhpvStatus.getLsNo()+"("+lhpvStatus.getbCode()+")");
										cnmtTempMap.put(LhpvStatusCNTS.LS_DT, lhpvStatus.getLsDt());
									}
								}
							}
						
							cnmtMapList.add(cnmtTempMap);

							break;
						}
		
					}
				System.out.println("Cnmt Map 2 Size is    ///////////   "+cnmtMap2.size());
	          Collection<Object> collection= cnmtMap2.values();
	     
	          System.out.println(collection);
				cnmtMapList.add(cnmtMap2);
				}
		
		    
		    stockRpt.put("temp", 1);
			stockRpt.put("custList", custList);
			stockRpt.put("brList", brList);
			stockRpt.put("cnmtMapList", cnmtMapList);
			}	
			else
			{
				stockRpt.put("temp", 0);
			}
		    }
		    
		    catch (Exception e) {
		    	stockRpt.put("temp", 0);
				// TODO: handle exception
		     e.printStackTrace();
			}
		    finally {
		       	session.clear();
		    	session.close();	
			}
			
			return stockRpt;
		}
	
	

	private Map<String, Object> getCnmtDetailN(Cnmt cnmt, List<Branch> brList,Map<Integer,Object> custmap, List<Station> stationList, Session session, boolean isMultiChln, Date upToDt){
		Map<String, Object> map = new HashMap<>();
		map.put(CnmtCNTS.CNMT_ID, cnmt.getCnmtId());
		map.put(CnmtCNTS.CUST_CODE, cnmt.getCustCode());
		map.put(CnmtCNTS.CNMT_TPNO, cnmt.getCnmtTpNo());
		map.put(CnmtCNTS.PR_BL_CODE, cnmt.getPrBlCode());
		
		if(cnmt.getCnmtInvoiceNo()!=null) {
			String invNo="";
			if(!cnmt.getCnmtInvoiceNo().isEmpty()) {
				for(int i=0;i<cnmt.getCnmtInvoiceNo().size();i++) {
					if(i>0)
						invNo=invNo+", ";
					
					invNo=invNo+""+cnmt.getCnmtInvoiceNo().get(i).get("invoiceNo");
				}
				
			}
			map.put(CnmtCNTS.CNMT_INVOICE_NO, invNo);
		}else {
			map.put(CnmtCNTS.CNMT_INVOICE_NO, "");
		}
		
		if (!brList.isEmpty()) {
			for (Branch branch : brList) {
				if (branch.getBranchId() == Integer.valueOf(cnmt.getBranchCode())) {
					map.put("cnmtBrName", branch.getBranchName());
					break;
				}		
			}
			
			for (Branch branch : brList) {
				if (branch.getBranchId() == Integer.valueOf(cnmt.getBranchCode())) {
					map.put("cnmtBillAt", branch.getBranchName());
					break;
				}		
			}
		}
		map.put(CnmtCNTS.CNMT_CODE, cnmt.getCnmtCode());
		if (!stationList.isEmpty()) {
			for (Station station : stationList) {
				if (station.getStnId() == Integer.parseInt(cnmt.getCnmtFromSt())) {
					map.put("cnmtFrmStnName", station.getStnName());
					break;
				}
			}
			
			for (Station station : stationList) {
				if (station.getStnId() == Integer.parseInt(cnmt.getCnmtToSt())) {
					map.put("cnmtToStnName", station.getStnName());
					break;
				}
			}
		}
		map.put(CnmtCNTS.CNMT_ACTUAL_WT, cnmt.getCnmtActualWt()/1000.0);
		map.put(CnmtCNTS.CNMT_GUARANTEE_WT, cnmt.getCnmtGuaranteeWt()/1000.0);
		map.put(CnmtCNTS.CNMT_NO_OF_PKG, cnmt.getCnmtNoOfPkg());
		map.put(CnmtCNTS.CNMT_DC, cnmt.getCnmtDC());
		
		long dateDiff = upToDt.getTime() - cnmt.getCnmtDt().getTime();
		dateDiff = dateDiff/(1000 * 60 * 60 * 24);
		
		if (isMultiChln) {
			map.put(CnmtCNTS.CNMT_FREIGHT, 0.0);
		} else {
			map.put(CnmtCNTS.CNMT_FREIGHT, Math.round(cnmt.getCnmtFreight()*100.0)/100.0);
			if (dateDiff <= 30) {
				map.put(CnmtCNTS.CNMT_FREIGHT+"UpTo30", Math.round(cnmt.getCnmtFreight()*100.0)/100.0);
			} else {
				map.put(CnmtCNTS.CNMT_FREIGHT+"Above30", Math.round(cnmt.getCnmtFreight()*100.0)/100.0);
			}
			
		}
		
		map.put(CnmtCNTS.CNMT_DT, cnmt.getCnmtDt());
		
		if (cnmt.getCnmtConsignor() != null && !"".equalsIgnoreCase(cnmt.getCnmtConsignor())) {
			//Customer cngr = (Customer) session.get(Customer.class, Integer.parseInt(cnmt.getCnmtConsignor()));
			Customer cngr=(Customer) custmap.get(Integer.parseInt(cnmt.getCnmtConsignor()));
			map.put("cnmtCngorName", cngr.getCustName());
		//	System.out.println("Cnmt Id "+cnmt.getCnmtId());
			//System.out.println("For Consignor");
			//System.out.println(cnmt.getCnmtConsignor()+"   from customer   "+cngr.getCustName());
			
		}
		
		if (cnmt.getCnmtConsignee() != null && !"".equalsIgnoreCase(cnmt.getCnmtConsignee())) {
			//Customer cngr = (Customer) session.get(Customer.class, Integer.parseInt(cnmt.getCnmtConsignee()));
			Customer cngr=(Customer) custmap.get(Integer.parseInt(cnmt.getCnmtConsignee()));
			map.put("cnmtCngeeName", cngr.getCustName());
			//System.out.println("For consignee");
			//System.out.println(cnmt.getCnmtConsignee()+"   from customer   "+cngr.getCustName());
		}
		
		
		return map;
	
	}

	
	
	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getRelOs(Map<String, Object> osRptService) {
		// TODO Auto-generated method stub
		Map<String, Object> relOsRpt = new HashMap<>();
		int temp = 0;
	String custgroupId=osRptService.get("custGroupId").toString();
	Date date=Date.valueOf(osRptService.get("upToDt").toString());
	System.out.println("Customer Group Id is  "+custgroupId);
	System.out.println("Up to Date is "+date);
	List<Customer> customerList=new ArrayList<Customer>();
	List<Integer> custId=new ArrayList<Integer>();
	List<Bill> billList=new ArrayList<Bill>();
/*	List<String> billNoList=new ArrayList<String>();
	List<Date> blDateList=new ArrayList<Date>();*/
	Session session=null;
	try {
		 session=this.sessionFactory.openSession();
	
	if(custgroupId!=null)
	{
	
	Criteria criteria=	session.createCriteria(Customer.class,"customer");
	criteria.add(Restrictions.eq(CustomerCNTS.CUST_GROUP_ID, custgroupId));
	customerList=criteria.list();	
	if(!customerList.isEmpty())
	{
		int count=0;
		for (Customer customer : customerList) {
			custId.add(customer.getCustId());
			count++;
		}
		System.out.println(count);
	}
	
	Criteria blcriteria =session.createCriteria(Bill.class,"bill");
	blcriteria.add(Restrictions.in(BillCNTS.BILL_CUST_ID, custId));
	blcriteria.add(Restrictions.eq(BillCNTS.BILL_CANCEL, false));
	blcriteria.add(Restrictions.eqProperty(BillCNTS.BILL_FINAL_TOT, BillCNTS.BILL_REM_AMT));
	blcriteria.add(Restrictions.le(BillCNTS.BILL_DT, date));
	blcriteria.setFetchMode("billDetList", FetchMode.JOIN);
	billList=blcriteria.list();
	

	}
	Set<Bill> billset=new HashSet<>(billList);
	Set<Integer> cnmtIdList = new HashSet<>();

	for(Bill bill : billset)
	{
	List<BillDetail> billDetails=bill.getBillDetList();
	for (BillDetail billDetail : billDetails) {
	cnmtIdList.add(billDetail.getBdCnmtId());
	}
	}
	
	List<Map<String, Object>> resosBillRptList = new ArrayList<>();
	List<Cnmt> cnmtList=new ArrayList<Cnmt>();
	
	Criteria cnmtcriteria=session.createCriteria(Cnmt.class,"cnmt");
	cnmtcriteria.add(Restrictions.in(CnmtCNTS.CNMT_ID, cnmtIdList));
	cnmtList=cnmtcriteria.list();
	Map<Integer, Cnmt> cnmtMap = new HashMap<>();
	List<Integer> cnIdList=new ArrayList<>();
/*	Set<Integer> stationIdList = new HashSet<>();
	Set<Integer> stationFromList=new HashSet<>();*/
	Map<Integer, Station> stationMap = new HashMap<>();
	if(!cnmtList.isEmpty())
	{
		for (Cnmt cnmt : cnmtList) {
			cnmtMap.put(cnmt.getCnmtId(), cnmt);
			cnIdList.add(cnmt.getCnmtId());
/*			stationIdList.add(Integer.parseInt(cnmt.getCnmtToSt()));
			stationFromList.add(Integer.parseInt(cnmt.getCnmtFromSt()));*/
		}

		List<Station> stationList = new ArrayList<>(); 
		
			stationList.addAll(
					session.createCriteria(Station.class).list());
	

		for (Station station : stationList)
		{
			stationMap.put(station.getStnId(), station);
		}
	}
	
	List<Cnmt_Challan> cnmtchlnList=new ArrayList<>();
	Map<Object, Object> cnmtChlnMap=new HashMap<>();
	List<Object> chlnId=new ArrayList<>();
	if(!cnIdList.isEmpty())
	{
		Criteria criteria=session.createCriteria(Cnmt_Challan.class);
		criteria.add(Restrictions.in(Cnmt_ChallanCNTS.Cnmt_Chln_cnmtId, cnIdList));
	   cnmtchlnList=criteria.list();
	   if(!cnmtchlnList.isEmpty())
	   {
		 for (Cnmt_Challan cnmt_Challan : cnmtchlnList) {
			cnmtChlnMap.put(cnmt_Challan.getCnmtId(), cnmt_Challan.getChlnId());
			chlnId.add(cnmt_Challan.getChlnId());
		}  
	   }
	}
	Map<Integer, Object> chlnMap=new HashMap<>();
	if(!chlnId.isEmpty())
	{
	Criteria criteria=session.createCriteria(Challan.class);
	criteria.add(Restrictions.in(ChallanCNTS.CHLN_ID, chlnId));
	List<Challan> chlnList=criteria.list();
	if(!chlnList.isEmpty())
	{
	for (Challan challan : chlnList) {
		chlnMap.put(challan.getChlnId(), challan);
	}	
	}
	}
	
	
	for (Bill bill : billset) {
	
	List<BillDetail> billDetails=   bill.getBillDetList();

	System.out.println("Bill No "+bill.getBlBillNo());
	for (BillDetail billDetail : billDetails) {
		Map<String, Object> resosBillRpt = new HashMap<>();
		resosBillRpt.put(BillCNTS.Bill_NO, bill.getBlBillNo());
		resosBillRpt.put(BillCNTS.BILL_DT, bill.getBlBillDt());
	//	resosBillRpt.put(BillCNTS.BILL_CUST_ID, bill.getBlCustId());
		double bdothAmt=(billDetail.getBdBonusAmt()+billDetail.getBdDetAmt()+billDetail.getBdLoadAmt()+billDetail.getBdOthChgAmt()+billDetail.getBdUnloadAmt()+billDetail.getBdUnloadDetAmt());
		resosBillRpt.put(BillDetailCNTS.BD_OTH_CHG_AMT,bdothAmt);
		resosBillRpt.put(BillDetailCNTS.BD_FREIGHT,billDetail.getBdFreight());
		resosBillRpt.put(BillDetailCNTS.BD_TOT_AMT, billDetail.getBdTotAmt());
	
		
	Cnmt cnmt=cnmtMap.get(billDetail.getBdCnmtId());
	if(cnmt!=null)
	{
		resosBillRpt.put(CnmtCNTS.CNMT_CODE, cnmt.getCnmtCode());
		resosBillRpt.put(CnmtCNTS.CNMT_DT, cnmt.getCnmtDt());
		//resosBillRpt.put(CnmtCNTS.CNMT_VEHICLE_TYPE, cnmt.getCnmtVehicleType());
		List<Map<String, Object>> invoiceList=cnmt.getCnmtInvoiceNo();
		if(invoiceList!=null)
		{
		for (Map<String, Object> map : invoiceList) {
			System.out.println("Inovice No      "+map.get("invoiceNo"));
			resosBillRpt.put("invoiceNo", map.get("invoiceNo"));
		}
		}
		else
		{
			resosBillRpt.put("invoiceNo", "");	
		}
	
		
				
		
		//add To station name
		Station toStn = (Station) stationMap.get(Integer.parseInt(cnmt.getCnmtToSt()));
		if (toStn != null) {
			resosBillRpt.put("toStnName", toStn.getStnName());
		} else {
			resosBillRpt.put("toStnName", "");
		}
		Station fromStn = (Station) stationMap.get(Integer.parseInt(cnmt.getCnmtFromSt()));
		if (fromStn != null) {
			resosBillRpt.put("fromStnName", fromStn.getStnName());
		} else {
			resosBillRpt.put("fromStnName", "");
		}
		
	Object challanId=	cnmtChlnMap.get(cnmt.getCnmtId());
	if(challanId!=null)
	{
	Challan challan=(Challan) chlnMap.get(challanId);
		if(challan!=null)
		{
		resosBillRpt.put("truckNo", challan.getChlnLryNo());	
		resosBillRpt.put(ChallanCNTS.CHLN_VEHICLE_TYPE, challan.getChlnVehicleType());
		}
	}
		
	}
	
	resosBillRptList.add(resosBillRpt);
	}	
	}
	
	Collections.sort(resosBillRptList,new Comparator<Map<String, Object>>() {
		@Override
		public int compare(Map<String, Object> o1, Map<String, Object> o2) {
			// TODO Auto-generated method stub
			Date date=Date.valueOf(o1.get(BillCNTS.BILL_DT).toString());
			Date date2=Date.valueOf(o2.get(BillCNTS.BILL_DT).toString());
			return date.compareTo(date2);
		}
	});
	
	
	relOsRpt.put("resosBillRptList", resosBillRptList);	
	
	relOsRpt.put("custList", customerList);
	temp = 1;
	}
	catch (Exception e) {
		// TODO: handle exception
		e.printStackTrace();
		logger.info("Exception in outstanding report "+e);
		temp = -1;
	}
	finally{
		session.clear();
		session.close();
	}
	System.out.println("Cust List Size  "+customerList.size());

	
	relOsRpt.put("temp", temp);

		return relOsRpt;
	}
	
	
	@Override
	public Map<String, Object> getRelStockReport(Map<String, Object> osRptService) {
		// TODO Auto-generated method stub
		System.out.println(osRptService.get("upToDt"));
		System.out.println(osRptService.get("custGroupId"));
		
		Date uptoDate=Date.valueOf(osRptService.get("upToDt").toString());
		String groupId=String.valueOf(osRptService.get("custGroupId"));
		
		List<Map<String, Object>> list=new ArrayList<>();
		Map<String,Object> map=new HashMap<>();
		Session session=null;
		try
		{
			session=this.sessionFactory.openSession();
			
		Criteria custcriteria=	session.createCriteria(Customer.class);
		custcriteria.add(Restrictions.eq(CustomerCNTS.CUST_GROUP_ID,groupId));
		List<Customer> customerList=custcriteria.list();
		
		List<String> custCodeList=new ArrayList<>();
		if(!customerList.isEmpty())
		{
		for (Customer customer :customerList) {
			custCodeList.add(customer.getCustCode());
		}	
		}
		System.out.println(custCodeList);
		System.out.println("Customer Group List "+customerList.size());
		System.out.println("Customer Cust Code Size "+custCodeList.size());
		
			
		Criteria cnmtCriteria=	session.createCriteria(Cnmt.class);
		cnmtCriteria.add(Restrictions.le(CnmtCNTS.CNMT_DT,uptoDate));
		cnmtCriteria.add(Restrictions.isNull(CnmtCNTS.CNMT_BILL_NO));
		cnmtCriteria.add(Restrictions.eq(CnmtCNTS.CNMT_CANCEL, false));
		cnmtCriteria.add(Restrictions.in(CnmtCNTS.CUST_CODE, custCodeList));
	    List<Cnmt> cnmtList=	cnmtCriteria.list();
	
	    
	

	Map<String, Object> stationMap=new HashMap<>();
	List<Station> stationList = new ArrayList<>();
	List<Integer> cnmtIdList=new ArrayList<>();
	Map<Object, Object> cnmtChlnMap=new HashMap<>();
	List<Integer> chlnidList=new ArrayList<>();
	Map<Object, Object> challanMap=new HashMap<>();
	  Map<Object, Object> customerMap=new HashMap<>();
	if(!cnmtList.isEmpty())
	{
    	Criteria stationcriteria=session.createCriteria(Station.class);
    	stationList=stationcriteria.list();
    	if(!stationList.isEmpty())
    	{
    for (Station station : stationList) {
		stationMap.put(station.getStnCode(), station);
	}
    	}	
    	
    	for (Cnmt cnmt: cnmtList) {
    		cnmtIdList.add(cnmt.getCnmtId());
		}
    	if(!cnmtIdList.isEmpty())
    	{
    		Criteria criteria=session.createCriteria(Cnmt_Challan.class);
    	 List<Cnmt_Challan> cnmt_Challans=	criteria.add(Restrictions.in(Cnmt_ChallanCNTS.Cnmt_Chln_cnmtId, cnmtIdList)).list();
    		for (Cnmt_Challan cnmt_Challan : cnmt_Challans) {
				cnmtChlnMap.put(cnmt_Challan.getCnmtId(), cnmt_Challan.getChlnId());
			chlnidList.add(cnmt_Challan.getChlnId());
    		}
           Criteria criteria2=session.createCriteria(Challan.class);
           List<Challan> challans=   criteria2.add(Restrictions.in(ChallanCNTS.CHLN_ID,chlnidList)).list();
       	for (Challan challan : challans) {
			challanMap.put(challan.getChlnId(), challan);
		}
    	}
    	
    	Criteria criteria=session.createCriteria(Customer.class);
    List<Customer>  customers=	criteria.list();
  
    for (Customer customer : customers) {
		customerMap.put(customer.getCustCode(),customer);
	}
    	
    	
    	
	}
	
	Comparator<Cnmt> comparator=new Comparator<Cnmt>() {

		@Override
		public int compare(Cnmt cnmt, Cnmt cnmt2) {
			// TODO Auto-generated method stub
		
		return cnmt.getCnmtDt().compareTo(cnmt2.getCnmtDt());
		}
	};
	Collections.sort(cnmtList,comparator);
	
	for (Cnmt cnmt : cnmtList) {
		Map<String, Object> stockRpt = new HashMap<>();
		if(cnmt!=null){
        ArrayList<Map<String, Object>> arrayList= cnmt.getCnmtInvoiceNo();
        if(arrayList!=null){
        for(Map<String, Object> map2:arrayList)
        {
        	if(map2!=null)
        	{
        stockRpt.put(CnmtCNTS.CNMT_INVOICE_NO, map2.get("invoiceNo"));
        	}
        }
        }
       Object chlnId= cnmtChlnMap.get(cnmt.getCnmtId());
        System.out.println("Challan Id  "+chlnId);
        if(chlnId!=null)
        {
      Challan challan=(Challan)challanMap.get(chlnId);
      System.out.println("Truck No.  "+challan.getChlnLryNo());
      stockRpt.put(ChallanCNTS.CHLN_LRY_NO, challan.getChlnLryNo());
      stockRpt.put(ChallanCNTS.CHLN_VEHICLE_TYPE, challan.getChlnVehicleType());
        }
        if(cnmt.getPrBlCode()!=null)
        {
        	Customer customer=(Customer)customerMap.get(cnmt.getPrBlCode());
        	stockRpt.put(CnmtCNTS.CNMT_CONSIGNEE,customer.getCustName());
        }
        else
        {
        	Customer customer=(Customer)customerMap.get(cnmt.getCustCode());
        	stockRpt.put(CnmtCNTS.CNMT_CONSIGNEE,customer.getCustName());

        }
		stockRpt.put(CnmtCNTS.CNMT_CODE, cnmt.getCnmtCode());
		stockRpt.put(CnmtCNTS.CNMT_DT, cnmt.getCnmtDt());
		stockRpt.put(CnmtCNTS.CNMT_FREIGHT, cnmt.getCnmtFreight());
		Station station=(Station) stationMap.get(cnmt.getCnmtToSt());
		stockRpt.put("toStation", station.getStnName());
		Station station2=(Station) stationMap.get(cnmt.getCnmtFromSt());
		stockRpt.put("fromStation", station2.getStnName());
		list.add(stockRpt);
		}	
	}
	map.put("cnmtList", list);
	map.put("temp", 1);
	map.put("customerList", customerList);
	
/*	for(Map<String, Object> map2:list)
	{
		System.out.println(map2.get(CnmtCNTS.CNMT_CODE)+"        "+map2.get(StationCNTS.STN_CODE));
	}*/

	System.out.println("Size of Full List  "+list.size());
	
	System.out.println("Cnmt List Size "+cnmtList.size());
			
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
			map.put("temp", 0);
		
		}
		finally {
			session.clear();
			session.close();
		}
		
	
		return map;
	}
	
	
	@Override
	public List<Challan> getPayableReport(Date fromDt, Date toDt) {
		// TODO Auto-generated method stub
		Session session=null;
		List<Challan> challans=null;
		try {
			session =this.sessionFactory.openSession();
			Criteria criteria=session.createCriteria(Challan.class);
			criteria.add(Restrictions.eq(ChallanCNTS.CHLN_CANCEL, false));
			criteria.add(Restrictions.or(Restrictions.gt(ChallanCNTS.CHLN_REM_ADV, 0.0d),Restrictions.gt(ChallanCNTS.CHLN_REM_BAL, 0.0d)));
			criteria.add(Restrictions.between(ChallanCNTS.CHLN_DT, fromDt, toDt));
			criteria.addOrder(Order.asc(ChallanCNTS.CHLN_DT));
			challans=criteria.list();
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}finally {
			session.close();
		}
		return challans;
	}
	
	
	
}