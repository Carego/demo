'use strict';

var app = angular.module('application');

app.controller('EditVehicleVendorCntlr',['$scope','$location','$http','$filter','$window',
                                 function($scope,$location,$http,$filter,$window){

	console.log("EditVehicleVendorCntlr Started");
	
	$scope.vehVendorList = [];
	$scope.ownerList = [];
	$scope.brokerList = [];
	$scope.vtList = [];
	$scope.stateList = [];
	
	$scope.owner = {};
	$scope.broker = {};
	$scope.vehVenTemp = {};
	$scope.vehVenMstr = {};
	
	$scope.vehVenMstrFormFlag = false;
	
	$scope.vehVenDBFlag = true;
	$scope.ownerDBFlag = true;
	$scope.brokerDBFlag = true;
	$scope.vehTypeDBFlag = true;
	$scope.stateDBFlag = true;
	$scope.fitStateDBFlag = true;
	
	
	$scope.getOwnBrkDriRC = function() {
		console.log("getOwnBrkDriRC Entered");
		var response = $http.post($scope.projectName+'/getOwnBrkDriRC');
		response.success(function(data, status, headers, config){
			
			if (data.result === "success") {
				console.log("getOwnBrkDriRC Success");
				$scope.vehVendorList = data.vehVendorList;
			}else {
				$scope.alertToast(data.result);
				$scope.alertToast("Or you have no vehicle to edit");
				console.log("getOwnBrkDriRC Error");
			}
		});
		response.error(function(data, status, headers, config){
			console.log("getOwnBrkDriRC Error: "+data);
		});
	}
	
	$scope.openVehVenDB = function() {
		console.log("Entered into openVehVenDB");
		$scope.vehVenDBFlag = false;
		$('div#vehVenDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Vehicle Detail",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.vehVenDBFlag = true;
			}
		});

		$('div#vehVenDB').dialog('open');
	}
	
	$scope.saveVehVen = function(vehVenTemp){
		$scope.vehVenTemp = vehVenTemp;
	}
	
	$scope.closeVehVenDb =  function(){
		$('div#vehVenDB').dialog('close');
	}
	
	$scope.vehVenSubmit = function() {
		console.log("vehVenSubmit");
		
		var response = $http.post($scope.projectName+'/getVehicleVendor', $scope.vehVenTemp.vvId);
		response.success(function(data, status, headers, config){
			
			if (data.result === "success") {
				$scope.vehVenMstrFormFlag = true;
				console.log("getVehicleVendor Success");
				$scope.vehVenMstr = data.vehVendor;
				
				$scope.owner.ownName = $scope.vehVenMstr.owner.ownName;
				$scope.owner.ownId = $scope.vehVenMstr.owner.ownId;
				$scope.owner.ownCode = $scope.vehVenMstr.owner.ownCode;
				
				$scope.broker.brkName = $scope.vehVenMstr.broker.brkName;
				$scope.broker.brkId = $scope.vehVenMstr.broker.brkId;
				$scope.broker.brkCode = $scope.vehVenMstr.broker.brkCode;
				
				if ($scope.ownerList.length < 1) {
					$scope.getOwnNameCodeId();
					console.log("getOwnNameCodeId() called: ")
				}else{
					console.log("getOwnNameCodeId() not called: ")
				}
			}else {
				$scope.alertToast(data.result);
				console.log("getVehicleVendor Error");
			}
		});
		response.error(function(data, status, headers, config){
			console.log("getVehicleVendor Error: "+data);
		});
	}
	
	$scope.getOwnNameCodeId = function(){
		console.log("getOwnNameCodeId Entered");
		var response = $http.post($scope.projectName+'/getOwnNameCodeId');
		response.success(function(data, status, headers, config){
			
			if (data.result === "success") {
				console.log("getOwnNameCodeId Success");
				$scope.ownerList = data.ownerList;
				if ($scope.brokerList.length < 1) {
					$scope.getBrkNameCodeId();
					console.log("getBrkNameCodeId() called: ")
				}else{
					console.log("getBrkNameCodeId() not called: ")
				}
				
			}else {
				$scope.alertToast("you have no owner");
				console.log("getOwnNameCodeId Error");
			}
		});
		response.error(function(data, status, headers, config){
			console.log("getOwnNameCodeId Error: "+data);
		});
	}
	
	$scope.openOwnerDB = function(){
		console.log("Entered into openOwnerDB");
		$scope.ownerDBFlag = false;
		$('div#ownerDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Owner",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.ownerDBFlag = true;
			}
		});

		$('div#ownerDB').dialog('open');
	}
	
	$scope.saveOwner = function(owner){
		$scope.owner = owner;
	}
	
	$scope.closeOwnDb =  function(){
		console.log("OwnerName----->"+$scope.owner.ownName);
		$('div#ownerDB').dialog('close');
	}
	
	$scope.getBrkNameCodeId = function(){
		console.log("getBrkNameCodeId Entered");
		var response = $http.post($scope.projectName+'/getBrkNameCodeId');
		response.success(function(data, status, headers, config){
			
			if (data.result === "success") {
				console.log("getBrkNameCodeId Success");
				$scope.brokerList = data.brokerList;
				if ($scope.vtList.length < 1) {
					$scope.getVehicleType();
					console.log("getVehicleType() called: ")
				}else{
					console.log("getVehicleType() not called: ")
				}
			}else {
				$scope.alertToast("you have no broker");
				console.log("getBrkNameCodeId Error");
			}
		});
		response.error(function(data, status, headers, config){
			console.log("getBrkNameCodeId Error: "+data);
		});
	}
	
	$scope.openBrokerDB = function(){
		console.log("Entered into openBrokerDB");
		$scope.brokerDBFlag = false;
		$('div#brokerDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			position: UDPos,
			show: UDShow,
			hide: UDHide,
			title: "Broker",
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy');
				$(this).hide();
				$scope.brokerDBFlag = true;
			}
		});

		$('div#brokerDB').dialog('open');
	}
	
	$scope.saveBroker = function(broker){
		$scope.broker = broker;
	}
	
	$scope.closeBrkDb =  function(){
		console.log("brokerName----->"+$scope.broker.brkName);
		$('div#brokerDB').dialog('close');
	}
	
	$scope.getVehicleType = function(){
		console.log("getVehicleType()");
		var response = $http.post($scope.projectName+'/getVehicleTypeCodeForOwner');
		response.success(function(data, status, headers, config){
			if(data.result==="success"){
				$scope.vtList = data.list;
				
				if ($scope.stateList.length < 1) {
					$scope.getStateList();
					console.log("getStateList() called: ")
				}else{
					console.log("getStateList() not called: ")
				}
				
				
			}else{
				console.log(data);   
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
		});
	}
	
	$scope.openVehicleTypeDB = function(){
		$scope.vehTypeDBFlag = false;
    	$('div#vehTypeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Vehicle Type",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.vehTypeDBFlag = true;
		    }
			});
		
		$('div#vehTypeDB').dialog('open');
		}
	
	$scope.saveVehicleType = function(vt){
		console.log("saveVehicleType()");
		$scope.vehVenMstr.vvType = vt.vtCode;
	}
	
	$scope.closeVehicleTypeDb = function(){
		console.log("closeVehicleTypeDb()");
		$('div#vehTypeDB').dialog('close');
	}
	
	$scope.getStateList = function() {
		console.log("getStateList()");
		var response = $http.post($scope.projectName+'/getStateList');
		response.success(function(data, status, headers, config){
			if(data.result==="success"){
				$scope.stateList = data.stateList;
			}else{
				console.log(data);   
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data.result);
		});
	}
	
	$scope.openStateDB = function(){
		console.log("openStateDB()");
		$scope.stateDBFlag = false;
    	$('div#stateDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "States",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.stateDBFlag = true;
		    }
			});
		
		$('div#stateDB').dialog('open');
		}
	
	$scope.saveState = function(state){
		console.log("saveState()");
		$scope.vehVenMstr.vvPerState = state;
	}
	
	$scope.closeStateDb = function(){
		console.log("closeStateDb()");
		$('div#stateDB').dialog('close');
		
	}
	
	$scope.openFitStateDB = function(){
		console.log("openFitStateDB()");
		$scope.fitStateDBFlag = false;
    	$('div#fitStateDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "States",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.fitStateDBFlag = true;
		    }
			});
		
		$('div#fitStateDB').dialog('open');
		}
	
	$scope.saveFitState = function(state){
		console.log("saveFitState()");
		$scope.vehVenMstr.vvFitState = state;
	}
	
	$scope.closeFitStateBb = function(){
		console.log("closeFitStateBb()");
		$('div#fitStateDB').dialog('close');
	}
	
	$scope.updateVehVenMstr = function(vehVenMstrForm){
		console.log("updateVehVenMstr");
		
		if(vehVenMstrForm.$invalid){
			if (vehVenMstrForm.vvDriverName.$invalid) {
				$scope.alertToast("Please Enter Driver Name");
			} else if (vehVenMstrForm.vvDriverDLNoName.$invalid) {
				$scope.alertToast("Please Enter Valid DL No");
			} else if (vehVenMstrForm.vvDriverMobNoName.$invalid) {
				$scope.alertToast("Please Enter Valid Mobile No");
			}
		}else {
			var vehVendorService = {
					"ownId"			:	$scope.owner.ownId,
					"brkId"			:	$scope.broker.brkId,
					"vehVenMstr"	:	$scope.vehVenMstr
			}
			
			$('#updateVehVenMstrId').attr("disabled","disabled");
			
			var response = $http.post($scope.projectName+'/updateVehVenMstr', vehVendorService);
			response.success(function(data, status, headers, config){
				
				if (data.result === "success") {
					$scope.alertToast("Updated Successfully");
					
					$('#updateVehVenMstrId').removeAttr("disabled");
					
					//empty the resources
					
					$scope.owner = {};
					$scope.broker = {};
					$scope.vehVenMstr = {};
					$scope.vehVenTemp = {};
					
					$scope.vehVenMstrFormFlag = false;
					
				}else {
					$scope.alertToast(data.errorResult);
				}
			});
			response.error(function(data, status, headers, config){
				console.log("saveVehVenMstr Error: "+data);
			});
		}
	}
	
	$('#vvDriverMobNoId').keypress(function(key) {
		if(key.charCode < 48 || key.charCode > 57)
			return false;
	});

	$('#vvDriverMobNoId').keypress(function(e) {
		if (this.value.length == 10) {
			e.preventDefault();
		}
	});
	
	$('#rcNoId').keypress(function(e) {
		if (this.value.length == 40) {
			e.preventDefault();
		}
	});
	
	$('#vvDriverNameId').keypress(function(e) {
		if (this.value.length == 40) {
			e.preventDefault();
		}
	});
	
	$('#vvDriverDLNoId').keypress(function(e) {
		if (this.value.length == 40) {
			e.preventDefault();
		}
	});
	
	$('#vvPerNoId').keypress(function(e) {
		if (this.value.length == 40) {
			e.preventDefault();
		}
	});
	
	$('#vvFitNoId').keypress(function(e) {
		if (this.value.length == 40) {
			e.preventDefault();
		}
	});
	
	$('#vvFinanceBankId').keypress(function(e) {
		if (this.value.length == 40) {
			e.preventDefault();
		}
	});
	
	$('#vvPolicyNoId').keypress(function(e) {
		if (this.value.length == 40) {
			e.preventDefault();
		}
	});
	
	$('#vvPolicyCompId').keypress(function(e) {
		if (this.value.length == 40) {
			e.preventDefault();
		}
	});
	
	$('#vvTransitPassNoId').keypress(function(e) {
		if (this.value.length == 40) {
			e.preventDefault();
		}
	});
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		$scope.getOwnBrkDriRC();
	}else if($scope.logoutStatus === true){
		$location.path("/");
	}else{
		console.log("****************");
	}
	
	console.log("EditVehicleVendorCntlr Ended");
}]);