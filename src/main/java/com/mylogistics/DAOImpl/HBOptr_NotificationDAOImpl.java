package com.mylogistics.DAOImpl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.HBOptr_NotificationDAO;
import com.mylogistics.constants.HBOptr_NotificationCNTS;
import com.mylogistics.model.HBOptr_Notification;

public class HBOptr_NotificationDAOImpl implements HBOptr_NotificationDAO{

	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	public HBOptr_NotificationDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Transactional
	public int saveHBON(HBOptr_Notification hBON){
		int temp;
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 session.save(hBON);
			 transaction.commit();
			 temp = 1;
			 session.flush();
		 }catch(Exception e){
			 e.printStackTrace();
			 temp = -1;
		 }
		 session.clear();
		 session.close();
		 return temp;
	}
	
		@Transactional
		@SuppressWarnings("unchecked")
		public List<HBOptr_Notification> getHBOptr_NotificationData(String operatorCode){
			List<HBOptr_Notification> hbOptr_Notifications = new ArrayList<HBOptr_Notification>();
			try{
				session = this.sessionFactory.openSession();
				
				Criteria cr=session.createCriteria(HBOptr_Notification.class);
				cr.add(Restrictions.eq(HBOptr_NotificationCNTS.HBON_ISVIEW, false));
				cr.add(Restrictions.eq(HBOptr_NotificationCNTS.HBON_OPCODE, operatorCode));
				
				hbOptr_Notifications=cr.list();				
			}
			catch(Exception e){
				e.printStackTrace();
			}
			session.clear();
			session.close();
			return hbOptr_Notifications;
	}
		

		@Transactional
		@SuppressWarnings("unchecked")
		public List<HBOptr_Notification> getTotalHBON_Data(String operatorCode){
			System.out.println("operator code inside getTotalHBON_Data "+operatorCode);
			List<HBOptr_Notification> hbOptr_Notifications = new ArrayList<HBOptr_Notification>();
			try{
				session = this.sessionFactory.openSession();
				transaction =  session.beginTransaction();
				Criteria cr=session.createCriteria(HBOptr_Notification.class);
				cr.add(Restrictions.eq(HBOptr_NotificationCNTS.HBON_OPCODE,operatorCode));
				hbOptr_Notifications=cr.list();
				transaction.commit();
				session.flush();
			}
			catch(Exception e){
				e.printStackTrace();
			}
			session.clear();
			session.close();
			return hbOptr_Notifications;
	}
		
		@Transactional
		@SuppressWarnings("unchecked")
		public List<HBOptr_Notification> getHBOptr_NotificationDataWithDate(String userCode,Date date){
			List<HBOptr_Notification> hbOptr_Notifications = new ArrayList<HBOptr_Notification>();
			try{
				session = this.sessionFactory.openSession();
				transaction =  session.beginTransaction();
				Criteria cr=session.createCriteria(HBOptr_Notification.class);
				cr.add(Restrictions.eq(HBOptr_NotificationCNTS.HBON_OPCODE,userCode));
				cr.add(Restrictions.eq(HBOptr_NotificationCNTS.HBON_DISDT,date));
				//cr.add(Restrictions.eq(BranchStockDispatchCNTS.HBON_DT,date));
				hbOptr_Notifications=cr.list();
				transaction.commit();
				session.flush();
			}
			catch(Exception e){
				e.printStackTrace();
			}
			session.clear();
			session.close();
			return hbOptr_Notifications;
	}
		
		
		@Transactional
		public int updateHBON_Data(String dispatchCode){
			System.out.println("enter into updateHBON_Data function");
			try{
				session = this.sessionFactory.openSession();
				transaction =  session.beginTransaction();
				Criteria cr=session.createCriteria(HBOptr_Notification.class);
				cr.add(Restrictions.eq(HBOptr_NotificationCNTS.HBON_DISCODE,dispatchCode));
				//cr.add(Restrictions.eq(BranchStockDispatchCNTS.HBON_DT,date));
				HBOptr_Notification hbOptr_Notification = (HBOptr_Notification) cr.list().get(0);
				hbOptr_Notification.setView(true);
				session.update(hbOptr_Notification);
				transaction.commit();
				session.flush();
			}
			catch(Exception e){
				e.printStackTrace();
			}
			session.clear();
			session.close();
			return 1;
	}
		
		@SuppressWarnings("unchecked")
		 @Transactional
		 public int updateIsViewYes(int id){
			System.out.println("enter into updateIsViewYes function");
			int temp;
			 try{
				 session = this.sessionFactory.openSession();
				 transaction = session.beginTransaction();
				 HBOptr_Notification hbOptr_Notification = new HBOptr_Notification();
				 List<HBOptr_Notification> hbOptr_Notifications = new ArrayList<HBOptr_Notification>();	
			 	 Criteria cr=session.createCriteria(HBOptr_Notification.class);
				 cr.add(Restrictions.eq(HBOptr_NotificationCNTS.HBON_ID,id));
				 hbOptr_Notifications =cr.list();
				 hbOptr_Notification = hbOptr_Notifications.get(0);
				 hbOptr_Notification.setView(true);
				 session.update(hbOptr_Notification);
				 transaction.commit();
				 session.flush();
				
				 temp = 1;
				 }
			 
			 catch(Exception e){
				 e.printStackTrace();
				 temp = -1;
			 }
			 session.clear();
			 session.close();
			 return temp;
		 }
}
