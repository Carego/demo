package org.apache.jsp.WEB_002dINF.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class footer_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\n");
      out.write("<div style=\"background-image: url('resources/img/footer.jpg'); background-size:cover; position: fixed; bottom: -20px; right: 0px ; left: 0px;\" >\n");
      out.write("   <div class=\"row\">\n");
      out.write("       <div class=\"col s11\">\n");
      out.write("           <div style=\"margin: 10px 0px\">\n");
      out.write("           <a class=\"grey-text lighten-3\"> License </a>\n");
      out.write("           <a class=\"grey-text lighten-3 right\" style=\"margin-right: -50px\" href=\"http://www.matteroftech.in/\"> © 2014-2015 Matter of Tech, All rights reserved. </a>\n");
      out.write("           </div>\n");
      out.write("         </div>\n");
      out.write("   </div>\n");
      out.write("</div>\n");
      out.write("\n");
      out.write("<!-- content visible if login true -->\n");
      out.write("<!-- <div ng-if=\"operatorLogin || adminLogin || superAdminLogin\">\n");
      out.write("\n");
      out.write("<div class=\"red\" style=\"margin-bottom: -20px;\">\n");
      out.write("    <div class=\"row\">\n");
      out.write("    \t<div class=\"col s11\">\n");
      out.write("        \t<div style=\"margin: 10px 10px\">\n");
      out.write("          \t© 2014-2015 Matter of Tech, All rights reserved.\n");
      out.write("        \t<a class=\"grey-text text-lighten-4 right\">License</a>\n");
      out.write("        \t</div>\n");
      out.write("      \t</div>\n");
      out.write("    </div>\n");
      out.write("</div>\n");
      out.write("</div>\n");
      out.write("\n");
      out.write(" -->");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
