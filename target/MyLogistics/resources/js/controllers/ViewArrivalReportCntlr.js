'use strict';

var app = angular.module('application');

app.controller('ViewArrivalReportCntlr',['$scope','$location','$http','FileUploadService','$filter',
                                         function($scope,$location,$http,FileUploadService,$filter){

	$scope.ArCodeDBFlag = true;
	$scope.BranchCodeDBFlag=true;
	$scope.showARDetailsFlag=true;
	$scope.show = "true";
	$scope.ar= {};

	$('#arRcvWt').keypress(function(key) {
		if(key.charCode < 46 || key.charCode > 57)
			return false;
	});


	$('#arRcvWt').keypress(function(e) {
		if (this.value.length == 9) {
			e.preventDefault();
		}
	});

	$('#arPkg').keypress(function(key) {
		if(key.charCode < 48 || key.charCode > 57)
			return false;
	});


	$('#arPkg').keypress(function(e) {
		if (this.value.length == 9) {
			e.preventDefault();
		}
	});

	$('#arUnloading').keypress(function(key) {
		if(key.charCode < 48 || key.charCode > 57)
			return false;
	});


	$('#arUnloading').keypress(function(e) {
		if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 9)){
			e.preventDefault();
		}
	});


	$('#arClaim').keypress(function(key) {
		if(key.charCode < 48 || key.charCode > 57)
			return false;
	});


	$('#arClaim').keypress(function(e) {
		if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 9)){
			e.preventDefault();
		}
	});

	$('#arDetention').keypress(function(key) {
		if(key.charCode < 48 || key.charCode > 57)
			return false;
	});


	$('#arDetention').keypress(function(e) {
		if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 9)){
			e.preventDefault();
		}
	});

	$('#arOther').keypress(function(key) {
		if(key.charCode < 48 || key.charCode > 57)
			return false;
	});


	$('#arOther').keypress(function(e) {
		if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 9)){
			e.preventDefault();
		}
	});

	$('#arLateDelivery').keypress(function(key) {
		if(key.charCode < 48 || key.charCode > 57)
			return false;
	});


	$('#arLateDelivery').keypress(function(e) {
		if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 9)){
			e.preventDefault();
		}
	});

	$('#arLateAck').keypress(function(key) {
		if(key.charCode < 48 || key.charCode > 57)
			return false;
	});


	$('#arLateAck').keypress(function(e) {
		if((/\d*\.\d\d$/.test(this.value)) ||  (this.value.length == 9)){
			e.preventDefault();
		}
	});

	$scope.OpenARCodevDB = function(){
		$scope.ArCodeDBFlag = false;
		$('div#arCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#arCodeDB').dialog('open');
	}

	$scope.OpenBranchCodeDB = function(){
		$scope.BranchCodeDBFlag=false;
		$('div#branchCodeDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Branch Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#branchCodeDB').dialog('open');
	}

	$scope.saveBranchCode = function(branch){
		$scope.ar.branchCode = branch.branchCode;
		$('div#branchCodeDB').dialog('close');
		$scope.BranchCodeDBFlag=true;
	}

	$scope.saveArCode = function(arCode){
		$scope.arCode = arCode;
		$('div#arCodeDB').dialog('close');
		$scope.ArCodeDBFlag = true;
	}

	$scope.getArList = function(){
		console.log(" entered into getArList------>");
		var response = $http.post($scope.projectName+'/getArrivalRepList');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.arList = data.list;
				$scope.getBranchData();
			}else{
				$scope.getARCodeList();
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {
			$scope.errorToast(data);
		});
	}

	$scope.getBranchData = function(){
		console.log("getBranchData------>");
		var response = $http.post($scope.projectName+'/getBranchDataForAR');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.branchList = data.list;
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers,config) {
			$scope.errorToast(data);
		});
	}

	$scope.ViewAr = function(arCode){
		$scope.code=$('#arrRepCode').val();
		if($scope.code === ""){
			$scope.alertToast("Please enter arrival report code....");
		}else{
			$scope.showARDetailsFlag = false;
			var response = $http.post($scope.projectName+'/viewar',arCode);
			response.success(function(data, status, headers, config) {
				if(data.result === "success"){
					console.log(data);
					console.log("from server -----data.ar.branchCode = "+data.ar.branchCode);
					$scope.ar = data.ar;
					console.log("ar.arUnloading ----> "+$scope.ar.arUnloading);
					console.log("ar.arClaim ----> "+$scope.ar.arClaim);
					console.log("ar.arDetention ----> "+$scope.ar.arDetention);
					console.log("ar.arOther ----> "+$scope.ar.arOther);
					$scope.arDt =  $filter('date')($scope.ar.arDt, 'MM/dd/yyyy hh:mm:ss');
					$scope.arRepDt =  $filter('date')($scope.ar.arRepDt, 'MM/dd/yyyy hh:mm:ss');
					$scope.arRepTime =  $filter('date')($scope.ar.arRepTime, 'MM/dd/yyyy hh:mm:ss');
					$scope.successToast(data.result);
				}else{
					console.log(data);
				}
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data);
			});
		}
	}


	$scope.uploadArImage = function(){
		console.log("enter into uploadArImage function");
		var file = $scope.arImage;
		if(angular.isUndefined(file) || file === null || file === ""){
			$scope.alertToast("First choose the file----->");
		}else{
			console.log('file is ' + JSON.stringify(file));
			var uploadUrl = $scope.projectName+"/uploadarImage";
			FileUploadService.uploadFileToUrl(file, uploadUrl);
			console.log("file save on server");
		}
	}


	$scope.updateAR = function(ARForm){
		console.log("Entered into updateAR function--- ");

		if(ARForm.$invalid){
			if(ARForm.arChlnCode.$invalid){
				$scope.alertToast("please enter 7 digit AR Chln Code..");
			}else if(ARForm.arCode.$invalid){
				$scope.alertToast("please enter 7 digit AR Code..");
			}else if(ARForm.arDt.$invalid){
				$scope.alertToast("please enter AR Date..");
			}else if(ARForm.arRcvWt.$invalid){
				$scope.alertToast("please enter Received Weight in integer..");
			}else if(ARForm.arPkg.$invalid){
				$scope.alertToast("please enter Package in integer..");
			}else if(ARForm.arUnloading.$invalid){
				$scope.alertToast("Please enter Unloading..");
			}else if(ARForm.arClaim.$invalid){
				$scope.alertToast("please enter Claim..");
			}else if(ARForm.arDetention.$invalid){
				$scope.alertToast("please enter Detention..");
			}else if(ARForm.arOther.$invalid){
				$scope.alertToast("Please enter Other..");
			}else if(v.arLateDelivery.$invalid){
				$scope.alertToast("Please enter Late Delivery..");
			}else if(v.arLateAck.$invalid){
				$scope.alertToast("Please enter Late Acknowledgement..");
			}else if(ARForm.arRepDt.$invalid){
				$scope.alertToast("Please enter Reporting Date..");
			}else if(ARForm.arRepTime.$invalid){
				$scope.alertToast("Please enter Reporting Time..");
			}
		}else{				

			var response = $http.post($scope.projectName+'/editArrRep',$scope.ar);
			console.log("********************");
			response.success(function(data, status, headers, config) {
				if(data.result==="success"){
					console.log("success");
					$scope.successToast(data.result);
					$scope.ar="";
					$scope.arCode="";
					$scope.showARDetailsFlag = true;
				}else{
					console.log(data);
				}		
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data);
			});

		}
	}

	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		$scope.getArList();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 } 
	

	
}]);