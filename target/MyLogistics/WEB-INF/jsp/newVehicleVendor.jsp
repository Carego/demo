<div ng-show="operatorLogin || superAdminLogin">
	
	<div class="row">
		<form name="vehVenValidForm" ng-submit=vehVenValid(vehVenValidForm) class="col s12 card" style="align: center; padding-top: 40px; background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
				<div class="col s3 input-field">
					<input class="validate" type="text" id="ownerId" name="ownerNm" ng-model="owner.ownName" ng-keyup="getOwnerByNameCode($event.keyCode)" ng-required="true"> 
					<label for="code">Owner Name/FaCode</label>
				</div>

				<div class="col s3 input-field">
					<input class="validate" type="text" id="brokerId" name="brokerNm" ng-model="broker.brkName" ng-keyup="getBrokerByNameCode($event.keyCode)" ng-required="true"> 
					<label for="code">Broker Name/FaCode</label>
				</div>
				
				<div class="col s3 input-field">
					<input class="validate" type="text" id="rcNoId" name="rcNoNm" ng-model="vehVenMstr.vvRcNo" ng-required="true"> 
					<label for="code">Rc No.</label>
				</div>
				
				<div class="col s3 input-field">
					<input class="col s12 btn teal white-text" type="submit" value="submit">
				</div>
				
			</div>
		</form>
	</div>
	
	<div class="row" ng-show="vehVenMstrFormFlag">
		<form name="vehVenMstrForm" ng-submit=vehVenMstrSubmit(vehVenMstrForm) class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
		     		
		       		<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="vvTypeId" name ="vvTypeName" ng-model="vehVenMstr.vvType" ng-click="openVehicleTypeDB()" readonly="readonly">
		       			<label for="code">Vehcle Type</label>	
		       		</div>
		       		
		       		<div class="col s3 input-field">
		       				<input class="validate" type ="date" id="vvRcIssueDtId" name ="vvRcIssueDtName" ng-model="vehVenMstr.vvRcIssueDt" >
		       		<label for="code">RC Issue Date</label>	
		       		</div>
		       		
		       		<div class="col s3 input-field">
		       				<input class="validate" type ="date" id="vvRcValidDtId" name ="vvRcValidDtName" ng-model="vehVenMstr.vvRcValidDt" >
		       		<label for="code">RC Valid Date</label>	
		       		</div>
		       		
		       		<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="vvDriverNameId" name ="vvDriverName" ng-model="vehVenMstr.vvDriverName" >
		       			<label for="code">Driver Name</label>	
		       		</div>
		    </div>
		    
		    <div class="row">
		     	
		     		<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="vvDriverDLNoId" name ="vvDriverDLNoName" ng-model="vehVenMstr.vvDriverDLNo" >
		       			<label for="code">Driver DL No.</label>
		       		</div>
		       		
		       		<div class="col s3 input-field">
		       				<input class="validate" type ="date" id="vvDriverDLIssueDtId" name ="vvDriverDLIssueDtName" ng-model="vehVenMstr.vvDriverDLIssueDt" >
		       		<label for="code">Driver DL Issue Date</label>	
		       		</div>
		       		
		       		<div class="col s3 input-field">
		       				<input class="validate" type ="date" id="vvDriverDLValidDtId" name ="vvDriverDLValidDtName" ng-model="vehVenMstr.vvDriverDLValidDt" >
		       		<label for="code">Driver DL Valid Date</label>	
		       		</div>
		       		
		       		<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="vvDriverMobNoId" name ="vvDriverMobNoName" ng-model="vehVenMstr.vvDriverMobNo" ng-minlength="10" ng-maxlength="10">
		       			<label for="code">Driver Mob No.</label>
		       		</div>
	
			</div>
		    
		    <div class="row">
		     	
		     		<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="vvPerNoId" name ="vvPerNoName" ng-model="vehVenMstr.vvPerNo" >
		       			<label for="code">Permit No.</label>
		       		</div>
		       		
					<div class="col s3 input-field">
		       				<input class="validate" type ="date" id="vvPerIssueDtId" name ="vvPerIssueDtName" ng-model="vehVenMstr.vvPerIssueDt" >
		       		<label for="code">Permit Issue Date</label>	
		       		</div>
		       		
		       		<div class="col s3 input-field">
		       				<input class="validate" type ="date" id="vvPerValidDtId" name ="vvPerValidDtName" ng-model="vehVenMstr.vvPerValidDt" >
		       		<label for="code">Permit Valid Date</label>	
		       		</div>
		       		
		       		<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="vvPerStateId" name ="vvPerStateName" ng-model="vehVenMstr.vvPerState" readonly="readonly" ng-click="openStateDB()">
		       			<label for="code">Permit State</label>
		       		</div>
		    </div>
		    
		    <div class="row">
		     	
		     		<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="vvFitNoId" name ="vvFitNoName" ng-model="vehVenMstr.vvFitNo" >
		       			<label for="code">Fit Doc No.</label>
		       		</div>
		       		
					<div class="col s3 input-field">
		       				<input class="validate" type ="date" id="vvFitIssueDtId" name ="vvFitIssueDtName" ng-model="vehVenMstr.vvFitIssueDt" >
		       		<label for="code">Fit Doc Issue Date</label>	
		       		</div>
		       		
		       		<div class="col s3 input-field">
		       				<input class="validate" type ="date" id="vvFitValidDtId" name ="vvFitValidDtName" ng-model="vehVenMstr.vvFitValidDt" >
		       		<label for="code">Fit Doc Valid Date</label>	
		       		</div>
		       		
		       		<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="vvFitStateId" name ="vvFitStateName" ng-model="vehVenMstr.vvFitState" readonly="readonly" ng-click="openFitStateDB()">
		       			<label for="code">Fit Doc State</label>
		       		</div>
		    </div>
		    
		    <div class="row">
		     	
		     		<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="vvFinanceBankId" name ="vvFinanceBankName" ng-model="vehVenMstr.vvFinanceBankName" >
		       			<label for="code">Finance Bank Name</label>
		       		</div>
		       		
					<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="vvPolicyNoId" name ="vvPolicyNoName" ng-model="vehVenMstr.vvPolicyNo" >
		       		<label for="code">Policy No.</label>	
		       		</div>
		       		
		       		<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="vvPolicyCompId" name ="vvPolicyCompName" ng-model="vehVenMstr.vvPolicyComp" >
		       		<label for="code">Policy Company</label>	
		       		</div>
		       		
		       		<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="vvTransitPassNoId" name ="vvTransitPassNoName" ng-model="vehVenMstr.vvTransitPassNo" >
		       			<label for="code">Transit Pass No.</label>
		       		</div>
		    </div>
		    
		        <div class="row">
		     	
		     		<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="vvEngineNoId" name ="vvEngineNo" ng-model="vehVenMstr.vvEngineNo" >
		       			<label for="code">Engine No.</label>
		       		</div>
		       		
					<div class="col s3 input-field">
		       				<input class="validate" type ="text" id="vvChassisNoId" name ="vvChassisNoName" ng-model="vehVenMstr.vvChassisNo" >
		       		<label for="code">Chassis No.</label>	
		       		</div>
		       		
		       		<div class="col s3 input-field">
		       				<input class="validate" type ="number" id="vvModelId" name ="vvModelName" step="1" min="1990" max="2050" ng-model="vehVenMstr.vvModel" >
		       		<label for="code">Model Year</label>	
		       		</div>
		       		
		    </div>
		    	
			<div class="row">
      		 	<div class="col s12 center">
      		 		<input type="submit" value="Submit">
      		 	</div>
      		</div>
	       	 
		  </form>
    </div>
	
	<div id ="ownerDB" ng-hide="ownerDBFlag">
		<input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Name </th>
 	  	  	  <th> Pan No </th> 
 	  	  	  <th> Code </th>
 	  	  </tr>
 	  
		  <tr ng-repeat="owner in ownerList | filter:filterTextbox">
		 	  <td><input type="radio"  name="owner" value="{{ owner }}" ng-focus="saveOwner(owner)"></td>
              <td>{{owner.ownName}}</td>
              <td>{{owner.ownPanNo}}</td> 
              <td>{{owner.ownCode}}</td>
          </tr>
          
          <!-- <tr>
          	<td colspan="3">
	          	<div class="col s12 center">
	          		<input type="button" value="Submit" ng-click="closeOwnDb()">
	          	</div>
          	</td>
          </tr> -->
      	  
      </table> 
	</div>
	
	<div id ="brokerDB" ng-hide="brokerDBFlag">
		<input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	    <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Name </th>
 	  	  	  <th> Code </th>
 	  	  </tr>
 	  
		  <tr ng-repeat="broker in brokerList | filter:filterTextbox">
		 	  <td><input type="radio"  name="broker" value="{{ broker }}" ng-focus="saveBroker(broker)"></td>
              <td>{{broker.brkName}}</td>
              <td>{{broker.brkCode}}</td>
          </tr>
          
         <!--  <tr>
          	<td colspan="3">
	          	<div class="col s12 center">
	          		<input type="button" value="Submit" ng-click="closeBrkDb()">
	          	</div>
          	</td>
          </tr> -->
      	  
      </table> 
	</div>
	
	<div id ="vehTypeDB" ng-hide = "vehTypeDBFlag">
		<input type="text" name="filterVehicleType" ng-model="filterVehicleType" placeholder="Search">
 	  
 	   	  <table>
 	 		<tr>
				<th></th>
				<th>Vehicle Type Code</th>
				<th>Vehicle Type</th>
				<th>Service Type</th>
			</tr>
 	  
		  <tr ng-repeat="vt in vtList  | filter:filterVehicleType">
		 	  <td><input type="radio"  name="vtCode" id="vtCode" class="vtCode"  value="{{ vt }}" ng-model="vtCode" ng-click="saveVehicleType(vt)"></td>
              <td>{{ vt.vtCode }}</td>
              <td>{{ vt.vtVehicleType }}</td>
              <td>{{vt.vtServiceType}}</td>
          </tr>
         </table>       
	</div>
	
	<div id ="stateDB" ng-hide = "stateDBFlag">
		<input type="text" name="filterVehicleType" ng-model="filterVehicleType" placeholder="Search">
 	  
 	   	  <table>
 	 		<tr>
				<th></th>
				<th>State</th>
			</tr>
 	  
		  <tr ng-repeat="state in stateList | filter:filterVehicleType">
		 	  <td><input type="radio"  name="stateName" id="stateId" ng-model="vtCode" ng-click="saveState(state)"></td>
              <td>{{ state }}</td>
              
          </tr>
         </table>       
	</div>
	
	<div id ="fitStateDB" ng-hide = "fitStateDBFlag">
		<input type="text" name="filterVehicleType" ng-model="filterVehicleType" placeholder="Search">
 	  
 	   	  <table>
 	 		<tr>
				<th></th>
				<th>State</th>
			</tr>
 	  
		  <tr ng-repeat="state in stateList | filter:filterVehicleType">
		 	  <td><input type="radio"  name="fitStateName" id="fitStateId" ng-model="vtCode" ng-click="saveFitState(state)"></td>
              <td>{{ state }}</td>
              
          </tr>
         </table>       
	</div>
	
	<div id="vehVenMstrDB" ng-hide="vehVenMstrDBFlag"> 
		<div class="col s6 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >
			<table class="table-hover table-bordered table-bordered">
				
				<tr>
					<td>Owner Name</td>
					<td>{{owner.ownName}}</td>
				</tr>
				
				<tr>
					<td>Broker Name</td>
					<td>{{broker.brkName}}</td>
				</tr>
				
				<tr>
					<td>RC No.</td>
					<td>{{vehVenMstr.vvRcNo}}</td>
				</tr>
				
				<tr>
					<td>RC Issue Date</td>
					<td>{{vehVenMstr.vvRcIssueDt | date:'dd/MM/yyyy'}}</td>
				</tr>
				
				<tr>
					<td>RC Valid Date</td>
					<td>{{vehVenMstr.vvRcValidDt | date:'dd/MM/yyyy'}}</td>
				</tr>
				
				<tr>
					<td>Vehcle Type</td>
					<td>{{vehVenMstr.vvType}}</td>
				</tr>
				
				<tr>
					<td>Driver Name</td>
					<td>{{vehVenMstr.vvDriverName}}</td>
				</tr>
				
				<tr>
					<td>Driver DL No.</td>
					<td>{{vehVenMstr.vvDriverDLNo}}</td>
				</tr>
				
				<tr>
					<td>Driver Mob No.</td>
					<td>{{vehVenMstr.vvDriverMobNo}}</td>
				</tr>
				
				<tr>
					<td>Driver DL Issue Date</td>
					<td>{{vehVenMstr.vvDriverDLIssueDt | date:'dd/MM/yyyy'}}</td>
				</tr>
				
				<tr>
					<td>Driver DL Valid Date</td>
					<td>{{vehVenMstr.vvDriverDLValidDt | date:'dd/MM/yyyy'}}</td>
				</tr>
				
				<tr>
					<td>Permit No.</td>
					<td>{{vehVenMstr.vvPerNo}}</td>
				</tr>
				
				<tr>
					<td>Permit Issue Date</td>
					<td>{{vehVenMstr.vvPerIssueDt | date:'dd/MM/yyyy'}}</td>
				</tr>
				
				<tr>
					<td>Permit Valid Date</td>
					<td>{{vehVenMstr.vvPerValidDt | date:'dd/MM/yyyy'}}</td>
				</tr>
				
				<tr>
					<td>Permit State</td>
					<td>{{vehVenMstr.vvPerState}}</td>
				</tr>
					
		       	<tr>
					<td>Fit Doc No.</td>
					<td>{{vehVenMstr.vvFitNo}}</td>
				</tr>
				
				<tr>
					<td>Fit Doc Issue Date</td>
					<td>{{vehVenMstr.vvFitIssueDt | date:'dd/MM/yyyy'}}</td>
				</tr>
				
				<tr>
					<td>Fit Doc Valid Date</td>
					<td>{{vehVenMstr.vvFitValidDt | date:'dd/MM/yyyy'}}</td>
				</tr>
				
				<tr>
					<td>Fit Doc State</td>
					<td>{{vehVenMstr.vvFitState}}</td>
				</tr>
				
				<tr>
					<td>Finance Bank Name</td>
					<td>{{vehVenMstr.vvFinanceBankName}}</td>
				</tr>
				
				<tr>
					<td>Policy No.</td>
					<td>{{vehVenMstr.vvPolicyNo}}</td>
				</tr>
				
				<tr>
					<td>Policy Company</td>
					<td>{{vehVenMstr.vvPolicyComp}}</td>
				</tr>
				
				<tr>
					<td>Transit Pass No.</td>
					<td>{{vehVenMstr.vvTransitPassNo}}</td>
				</tr>
					
				<tr>
					<td>Engine No.</td>
					<td>{{vehVenMstr.vvEngineNo}}</td>
				</tr>
				
				<tr>
					<td>Chassis No.</td>
					<td>{{vehVenMstr.vvChassisNo}}</td>
				</tr>
				
				<tr>
					<td>Model</td>
					<td>{{vehVenMstr.vvModel}}</td>
				</tr>
				
			</table>
			<input type="button" value="Cancel" ng-click="back()"/>
			<input type="button" id="saveVehVenMstrId" value="Save" ng-click="saveVehVenMstr()"/>
		</div>
	</div>
	
</div>
