<div ng-show="operatorLogin || superAdminLogin">
	<div class="row">
		<form name="newVoucherForm" ng-submit=voucherSubmit(newVoucherForm) class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="branchFaCodeId" name ="branchFaCode" ng-model="vs.branch.branchFaCode"  readonly ng-required="true" >
		       			<label for="code">Branch Code</label>	
		       		</div>

		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="branchNameId" name ="branchName" ng-model="vs.branch.branchName" readonly ng-required="true" >
		       			<label for="code">Branch Name</label>	
		       		</div>
		       		
		       			<div class="col s4 input-field">
		       				<input class="validate" type ="date" id="dateId" name ="dateTemp" ng-model="dateTemp"   readonly  >
		       			<label for="code">Date</label>	
		       		</div>
		    </div>
		    
		    
		    <div class="row">
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="sheetNoId" name ="sheetNo" ng-model="vs.cashStmtStatus.cssCsNo" readonly ng-required="true" >
		       			<label for="code">Sheet No</label>	
		       		</div>
		       		
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="voucherTypeId" name ="voucherType" ng-model="vs.voucherType" readonly ng-required="true" >
		       			<label for="code">Voucher Type</label>	
		       		</div>
		       		
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="lhpvNoId" name ="lhpvNoName" ng-model="vs.lhpvTemp.ltLhpvNo">
		       			<label for="code">Lhpv No.</label>	
		       		</div>
		    </div>
		    
		    
		    <div class="row">
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="chlnNoId" name ="chlnNoName" ng-model="chlnNo" ng-click="openChlnDB()" readonly>
		       			<label for="code">Challan No</label>	
		       		</div>
		       		
		       		<!-- <div class="col s4 input-field">
		       				<input class="validate" type ="text" id="tranTypeId" name ="tranTypeName" ng-model="vs.lhpvTemp.ltTransType">
		       			<label for="code">Transaction Type</label>	
		       		</div> -->
		       		
		       		<div class="col s4 input-field">
				    		<select name="tranTypeName" id="tranTypeId" ng-model="vs.lhpvTemp.ltTransType" ng-init="vs.lhpvTemp.ltTransType = 'CASH'">
								<option value='CASH'>CASH</option>
								<option value='BANK'>BANK</option>
								<option value='CONTRA'>CONTRA</option>
							</select>
							<label>Transaction Type</label>
					</div>
		       		
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="bnkId" name ="bnkName" ng-model="vs.lhpvTemp.ltBankCode">
		       			<label for="code">Bank Code</label>	
		       		</div>
		       		
		    </div>
		    
		    
		    <div class="row">
		    
		    		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="chqNoId" name ="chqNoName" ng-model="vs.lhpvTemp.ltChqNo">
		       			<label for="code">Cheque No.</label>	
		       		</div>
		       		
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="number" id="advId" name ="advName" ng-model="vs.lhpvTemp.ltAdv">
		       			<label for="code">Advance</label>	
		       		</div>
		       		
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="number" id="balId" name ="balName" ng-model="vs.lhpvTemp.ltBal">
		       			<label for="code">Balance</label>	
		       		</div>
		       		
		    </div>
		    
		    
		     <div class="row">
		     		
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="number" id="detId" name ="detName" ng-model="vs.lhpvTemp.ltDet">
		       			<label for="code">Detention</label>	
		       		</div>
		       		
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="number" id="unldId" name ="unldName" ng-model="vs.lhpvTemp.ltUnload">
		       			<label for="code">Unloading</label>	
		       		</div>
		       		
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="number" id="othId" name ="othName" ng-model="vs.lhpvTemp.ltOth">
		       			<label for="code">Others</label>	
		       		</div>
		       		
		    </div>
		    
		    
		    <div class="row">
		    		
		    		<div class="col s4 input-field">
		       				<input class="validate" type ="number" id="tdsId" name ="tdsName" ng-model="vs.lhpvTemp.ltTds">
		       			<label for="code">TDS</label>	
		       		</div>
		       		
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="number" id="cdId" name ="cdName" ng-model="vs.lhpvTemp.ltCashDis">
		       			<label for="code">Cash Discount</label>	
		       		</div>
		       		
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="number" id="munsId" name ="munsName" ng-model="vs.lhpvTemp.ltMuns">
		       			<label for="code">Munsiana</label>	
		       		</div>
		       		
		    </div>
		  
		  
			  <div class="row"> 
			  	
			  	<div class="col s4 input-field">
		       				<input class="validate" type ="number" id="claimId" name ="claimName" ng-model="vs.lhpvTemp.ltClaim">
		       			<label for="code">Claim</label>	
		       		</div>
		       		
			  </div>
			  
			  
	         <div class="row"> 		
				 <div ng-show="reqChlnList.length > 0">
					<table class="tblrow">
					<caption class="coltag tblrow">Selected Challan</caption>
						<tr class="rowclr">
							<th class="colclr">Challan No</th>
							<th class="colclr">Action</th>
						</tr>
						<tr class="tbl" ng-repeat="chln in reqChlnList">
							<td class="rowcel">{{chln}}</td>
							<td class="rowcel"><input type="button" value="remove" ng-click="removeChln($index)" /></td>
						</tr>
					</table>
				</div>
			</div>	
			
			<div class="row">
				<div class="input-field col s12">
					<i class="mdi-editor-mode-edit prefix"></i>
 					<textarea id="textarea" class="materialize-textarea"  rows="3" cols="92"  name ="desc" ng-model="vs.desc" ng-required="true"></textarea>
 					<label>Description</label>
				</div>
    		 </div>	
    		 
	   		  <div class="row">
		     		<div class="col s12 center">
		       				<input class="validate" type ="submit" value="submit" >
		       		</div>
	   		 </div>
		  </form>
    </div>
    
  
 <div id="chlnCodeDB" ng-hide="chlnCodeDBFlag">

	<input type="text" name="filterChln"
		ng-model="filterChln" placeholder="Search....">
	<table>
		<tr ng-repeat="chln in chlnList | filter:filterChln">
			<td><input type="radio" name="chlnName" id="chlnId"
				 value="{{ chln }}" ng-model="chlnCodde"
				ng-click="saveChln(chln)"></td>
			<td>{{ chln }}</td>
		</tr>
	</table>
</div>
  
  
  
  
    <div id="saveVsDB" ng-hide="saveVsFlag"> 
		<div class="col s6 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >
			<table class="table-hover table-bordered table-bordered">
				<!-- <h2>Voucher Details</h2> -->
				<tr>
					<td>Branch Code</td>
					<td>{{vs.branch.branchFaCode}}</td>
				</tr>
				
				<tr>
					<td>Branch Name</td>
					<td>{{vs.branch.branchName}}</td>
				</tr>
				
				<tr>
					<td>Date</td>
					<td>{{dateTemp}}</td>
				</tr>
				
				<tr>
					<td>Sheet No</td>
					<td>{{vs.cashStmtStatus.cssCsNo}}</td>
				</tr>
				
				<tr>
					<td>Voucher Type</td>
					<td>{{vs.voucherType}}</td>
				</tr>
								
				<tr>
					<td>LHPV NO.</td>
					<td>{{vs.lhpvTemp.ltLhpvNo}}</td>
				</tr>
											
				<tr>
					<td>Transaction Type</td>
					<td>{{vs.lhpvTemp.ltTransType}}</td>
				</tr>
				
				<tr>
					<td>Cheque No.</td>
					<td>{{vs.lhpvTemp.ltChqNo}}</td>
				</tr>
						
				<tr>
					<td>Advance</td>
					<td>{{vs.lhpvTemp.ltAdv}}</td>
				</tr>
				
				<tr>
					<td>Balance</td>
					<td>{{vs.lhpvTemp.ltBal}}</td>
				</tr>
				
				<tr>
					<td>Detention</td>
					<td>{{vs.lhpvTemp.ltDet}}</td>
				</tr>
				
				<tr>
					<td>Unloading</td>
					<td>{{vs.lhpvTemp.ltUnload}}</td>
				</tr>
				
				<tr>
					<td>Others</td>
					<td>{{vs.lhpvTemp.ltOth}}</td>
				</tr>
				
				<tr>
					<td>TDS</td>
					<td>{{vs.lhpvTemp.ltTds}}</td>
				</tr>
				
				<tr>
					<td>Cash Discount</td>
					<td>{{vs.lhpvTemp.ltCashDis}}</td>
				</tr>
				
				<tr>
					<td>Munsiana</td>
					<td>{{vs.lhpvTemp.ltMuns}}</td>
				</tr>
				
				<tr>
					<td>Claim</td>
					<td>{{vs.lhpvTemp.ltClaim}}</td>
				</tr>
			</table>
			<input type="button" value="Save" id="saveId" ng-click="saveVS()"/>
			<input type="button" value="Cancel" ng-click="back()"/>
		</div>
	</div>

</div>
