package com.mylogistics.model.webservice;

import java.sql.Date;
import java.util.Calendar;

import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "challanapp")
public class ChallanApp {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "chlnId")
	private Integer chlnId;
	
	@Column(name = "chlnCode")
	private String chlnCode;
	
	@Column(name = "branchCode")
	private String branchCode;

	@Column(name = "chlnChdCode")
	private String chlnChdCode;
	
	@Column(name = "chlnFromStnCode")
	private String chlnFromStnCode;
	
	@Column(name = "chlnFromStnName")
	private String chlnFromStnName;
	
	@Column(name = "chlnToStnCode")
	private String chlnToStnCode;
	
	@Column(name = "chlnToStnName")
	private String chlnToStnName;
	
	
	@Column(name = "chlnEmpCode")
	private String chlnEmpCode;
		
	@Column(name = "chlnNoOfPkg")
	private String chlnNoOfPkg;
	
	@Column(name = "chlnLryRate")
	private String chlnLryRate;
	
	@Column(name = "chlnLryType")
	private String chlnLryType;
		
	@Column(name = "chlnChgWt")
	private String chlnChgWt;
	
	@Column(name = "chlnChgWtType")
	private String chlnChgWtType;
		
	@Column(name = "chlnFreight")
	private String chlnFreight;
	
	@Column(name = "chlnLoadingAmount")
	private String chlnLoadingAmount;
	
	@Column(name = "chlnExtra")
	private String chlnExtra;
	
	@Column(name = "chlnStatisticalChg")
	private String chlnStatisticalChg;
	
	@Column(name = "chlnTdsAmt")
	private String chlnTdsAmt;
	
	@Column(name = "chlnTotalFreight")
	private String chlnTotalFreight;
	
	@Column(name = "chlnAdvance")
	private String chlnAdvance;
	
	@Column(name = "chlnBalance")
	private String chlnBalance;
	
	@Column(name = "chlnPayAt")
	private String chlnPayAt;
	
	@Column(name = "chlnPayAtCode")
	private String chlnPayAtCode;
	
	@Column(name = "chlnTimeAllow")
	private String chlnTimeAllow;
		
	@Column(name = "chlnDt")
	private String chlnDt;
	
	@Column(name = "chlnBrRate")
	private String chlnBrRate;
	
	@Column(name = "chlnTotalWt")
	private String chlnTotalWt;
		
	@Column(name = "chlnWtSlip")
	private String chlnWtSlip;
	
	@Column(name = "chlnLryRepDT")
	private String chlnLryRepDT;
	
	@Column(name = "chlnVehicleType")
	private String chlnVehicleType;
	
	@Column(name = "chlnVehicleCode")
	private String chlnVehicleCode;
	
	@Column(name = "chlnRrNo")
	private String chlnRrNo;
	
	@Column(name = "chlnTrainNo")
	private String chlnTrainNo;
	
	@Column(name = "chlnLryLoadTime")
	private String chlnLryLoadTime;
	
	@Column(name = "chlnLryRptTime")
	private String chlnLryRptTime;
	
	@Column(name = "chlnLryNo")
	private String chlnLryNo;
	
	@Column(name = "userCode")
	private String userCode;
	
	@Column(name = "isPending")
	private Boolean isPending;
	
	@Column(name = "isCancel")
	private Boolean isCancel;
	
	@Column(name = "creationTS", insertable=false, updatable=true)
	private Date creationTS;
	
	public Integer getChlnId() {
		return chlnId;
	}
	public void setChlnId(Integer chlnId) {
		this.chlnId = chlnId;
	}
	public String getChlnCode() {
		return chlnCode;
	}
	public void setChlnCode(String chlnCode) {
		this.chlnCode = chlnCode;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getChlnChdCode() {
		return chlnChdCode;
	}
	public void setChlnChdCode(String chlnChdCode) {
		this.chlnChdCode = chlnChdCode;
	}
	public String getChlnFromStnCode() {
		return chlnFromStnCode;
	}
	public void setChlnFromStnCode(String chlnFromStnCode) {
		this.chlnFromStnCode = chlnFromStnCode;
	}
	public String getChlnFromStnName() {
		return chlnFromStnName;
	}
	public void setChlnFromStnName(String chlnFromStnName) {
		this.chlnFromStnName = chlnFromStnName;
	}
	public String getChlnToStnCode() {
		return chlnToStnCode;
	}
	public void setChlnToStnCode(String chlnToStnCode) {
		this.chlnToStnCode = chlnToStnCode;
	}
	public String getChlnToStnName() {
		return chlnToStnName;
	}
	public void setChlnToStnName(String chlnToStnName) {
		this.chlnToStnName = chlnToStnName;
	}
	public String getChlnEmpCode() {
		return chlnEmpCode;
	}
	public void setChlnEmpCode(String chlnEmpCode) {
		this.chlnEmpCode = chlnEmpCode;
	}
	public String getChlnNoOfPkg() {
		return chlnNoOfPkg;
	}
	public void setChlnNoOfPkg(String chlnNoOfPkg) {
		this.chlnNoOfPkg = chlnNoOfPkg;
	}
	public String getChlnLryRate() {
		return chlnLryRate;
	}
	public void setChlnLryRate(String chlnLryRate) {
		this.chlnLryRate = chlnLryRate;
	}
	public String getChlnChgWt() {
		return chlnChgWt;
	}
	public void setChlnChgWt(String chlnChgWt) {
		this.chlnChgWt = chlnChgWt;
	}
	public String getChlnFreight() {
		return chlnFreight;
	}
	public void setChlnFreight(String chlnFreight) {
		this.chlnFreight = chlnFreight;
	}
	public String getChlnLoadingAmount() {
		return chlnLoadingAmount;
	}
	public void setChlnLoadingAmount(String chlnLoadingAmount) {
		this.chlnLoadingAmount = chlnLoadingAmount;
	}
	public String getChlnExtra() {
		return chlnExtra;
	}
	public void setChlnExtra(String chlnExtra) {
		this.chlnExtra = chlnExtra;
	}
	public String getChlnStatisticalChg() {
		return chlnStatisticalChg;
	}
	public void setChlnStatisticalChg(String chlnStatisticalChg) {
		this.chlnStatisticalChg = chlnStatisticalChg;
	}
	public String getChlnTdsAmt() {
		return chlnTdsAmt;
	}
	public void setChlnTdsAmt(String chlnTdsAmt) {
		this.chlnTdsAmt = chlnTdsAmt;
	}
	public String getChlnTotalFreight() {
		return chlnTotalFreight;
	}
	public void setChlnTotalFreight(String chlnTotalFreight) {
		this.chlnTotalFreight = chlnTotalFreight;
	}
	public String getChlnAdvance() {
		return chlnAdvance;
	}
	public void setChlnAdvance(String chlnAdvance) {
		this.chlnAdvance = chlnAdvance;
	}
	public String getChlnBalance() {
		return chlnBalance;
	}
	public void setChlnBalance(String chlnBalance) {
		this.chlnBalance = chlnBalance;
	}
	public String getChlnPayAt() {
		return chlnPayAt;
	}
	public void setChlnPayAt(String chlnPayAt) {
		this.chlnPayAt = chlnPayAt;
	}
	public String getChlnTimeAllow() {
		return chlnTimeAllow;
	}
	public void setChlnTimeAllow(String chlnTimeAllow) {
		this.chlnTimeAllow = chlnTimeAllow;
	}
	public String getChlnDt() {
		return chlnDt;
	}
	public void setChlnDt(String chlnDt) {
		this.chlnDt = chlnDt;
	}
	public String getChlnBrRate() {
		return chlnBrRate;
	}
	public void setChlnBrRate(String chlnBrRate) {
		this.chlnBrRate = chlnBrRate;
	}
	public String getChlnTotalWt() {
		return chlnTotalWt;
	}
	public void setChlnTotalWt(String chlnTotalWt) {
		this.chlnTotalWt = chlnTotalWt;
	}
	public String getChlnWtSlip() {
		return chlnWtSlip;
	}
	public void setChlnWtSlip(String chlnWtSlip) {
		this.chlnWtSlip = chlnWtSlip;
	}
	public String getChlnLryRepDT() {
		return chlnLryRepDT;
	}
	public void setChlnLryRepDT(String chlnLryRepDT) {
		this.chlnLryRepDT = chlnLryRepDT;
	}
	public String getChlnVehicleType() {
		return chlnVehicleType;
	}
	public void setChlnVehicleType(String chlnVehicleType) {
		this.chlnVehicleType = chlnVehicleType;
	}
	public String getChlnRrNo() {
		return chlnRrNo;
	}
	public void setChlnRrNo(String chlnRrNo) {
		this.chlnRrNo = chlnRrNo;
	}
	public String getChlnTrainNo() {
		return chlnTrainNo;
	}
	public void setChlnTrainNo(String chlnTrainNo) {
		this.chlnTrainNo = chlnTrainNo;
	}
	public String getChlnLryLoadTime() {
		return chlnLryLoadTime;
	}
	public void setChlnLryLoadTime(String chlnLryLoadTime) {
		this.chlnLryLoadTime = chlnLryLoadTime;
	}
	
	public String getChlnLryRptTime() {
		return chlnLryRptTime;
	}
	public void setChlnLryRptTime(String chlnLryRptTime) {
		this.chlnLryRptTime = chlnLryRptTime;
	}
	public String getChlnLryNo() {
		return chlnLryNo;
	}
	public void setChlnLryNo(String chlnLryNo) {
		this.chlnLryNo = chlnLryNo;
	}
	public String getUserCode() {
		return userCode;
	}
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
	public Date getCreationTS() {
		return creationTS;
	}
	public void setCreationTS(Date creationTS) {
		this.creationTS = creationTS;
	}
	public String getChlnLryType() {
		return chlnLryType;
	}
	public void setChlnLryType(String chlnLryType) {
		this.chlnLryType = chlnLryType;
	}
	public String getChlnChgWtType() {
		return chlnChgWtType;
	}
	public void setChlnChgWtType(String chlnChgWtType) {
		this.chlnChgWtType = chlnChgWtType;
	}
	public String getChlnVehicleCode() {
		return chlnVehicleCode;
	}
	public void setChlnVehicleCode(String chlnVehicleCode) {
		this.chlnVehicleCode = chlnVehicleCode;
	}
	public Boolean getIsPending() {
		return isPending;
	}
	public void setIsPending(Boolean isPending) {
		this.isPending = isPending;
	}
	public Boolean getIsCancel() {
		return isCancel;
	}
	public void setIsCancel(Boolean isCancel) {
		this.isCancel = isCancel;
	}
	public String getChlnPayAtCode() {
		return chlnPayAtCode;
	}
	public void setChlnPayAtCode(String chlnPayAtCode) {
		this.chlnPayAtCode = chlnPayAtCode;
	}
	
	
}