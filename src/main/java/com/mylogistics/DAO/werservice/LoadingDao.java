package com.mylogistics.DAO.werservice;

import java.util.List;
import java.util.Map;

import org.hibernate.Session;

import com.mylogistics.model.webservice.LoadingModel;
import com.mylogistics.model.webservice.VendorDetail;

public interface LoadingDao {
	
	public Map<String, Object> getOwnBrkWs(Session session, String branchCode);
	
	public Map<String, Object> saveLoadingDetail(Session session, LoadingModel loadingModel);
	
	public Map<String, Object> saveLoadingDetail1(Session session, LoadingModel loadingModel);
	
	public Map<String, Object> saveVendorDetail(Session session, LoadingModel loadingModel);
	
	public List<Map<String, Object>> getLoadingDt(Session session, String branchCode, String fromDate, String toDate, String officerName);
	
	public List<String> getTrafficOfficers(Session session);
	
	public Map<String, Object> getPendingOrder(Session session, String bCode, Map<String, Object> resultMap);
	
	public Map<String, Object> getVendorDt(Session session, Integer ldId, Map<String, Object> resultMap);
	
	public Map<String, Object> saveFinalOrder(Session session, VendorDetail vDetail, Map<String, Object> resultMap);

}