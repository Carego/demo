package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.TempIntBrTvDAO;
import com.mylogistics.constants.CashStmtCNTS;
import com.mylogistics.constants.TempIntBrTvCNTS;
import com.mylogistics.model.CashStmt;
import com.mylogistics.model.TempIntBrTv;

public class TempIntBrTvDAOImpl implements TempIntBrTvDAO{

	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	private HttpSession httpSession;
	
	
	@Autowired
	public TempIntBrTvDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	

	@Transactional
	@SuppressWarnings("unchecked")
	public List<TempIntBrTv> getOpenIBTV(String brFaCode){
		System.out.println("enter into getOpenIBTV function");
		List<TempIntBrTv> tibtList = new ArrayList<TempIntBrTv>();
		if(brFaCode != null){
			try{
				session = this.sessionFactory.openSession();
				transaction = session.beginTransaction();
				Criteria cr = session.createCriteria(TempIntBrTv.class);
				cr.add(Restrictions.eq(TempIntBrTvCNTS.TIBT_FA_CODE,brFaCode));
				cr.add(Restrictions.eq(TempIntBrTvCNTS.TIBT_IS_CLEAR,false));
				cr.addOrder(Order.asc(TempIntBrTvCNTS.TIBT_BR_CODE));
				tibtList = cr.list();
				
				if(!tibtList.isEmpty()){
					for(int i=0;i<tibtList.size();i++){
						CashStmt cashStmt = (CashStmt) session.get(CashStmt.class,tibtList.get(i).getTibCsId());
						tibtList.get(i).setTibDate(cashStmt.getCsDt());
					}
				}
				
				session.flush();
			}catch(Exception e){
				e.printStackTrace();
			}
			session.clear();
			session.close();
		}else{
			System.out.println("brFaCode is empty");
		}
		
		return tibtList;
	}
}
