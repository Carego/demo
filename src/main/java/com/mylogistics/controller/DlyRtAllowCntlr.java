package com.mylogistics.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.BranchStockLeafDetDAO;
import com.mylogistics.model.Branch;
import com.mylogistics.model.BranchStockLeafDet;
import com.mylogistics.services.ConstantsValues;

@Controller
public class DlyRtAllowCntlr {

	@Autowired
	private BranchDAO branchDAO;
	
	@Autowired
	private BranchStockLeafDetDAO branchStockLeafDetDAO;
	
	
	@RequestMapping(value = "/getBrhFrDlyRt", method = RequestMethod.POST)
	public @ResponseBody Object getBrhFrDlyRt(){
		System.out.println("Inside getBrhFrDlyRt function-------");
		Map<String, Object> map = new HashMap<String, Object>();
		
		List<Branch> brhList = branchDAO.getAllActiveBranches();
		List<Map<String,Object>> actBrhList = new ArrayList<>();
		
		if(!brhList.isEmpty()){
			for(int i=0;i<brhList.size();i++){
				Map<String,Object> brhMap = new HashMap<>();
				brhMap.put("branchId",brhList.get(i).getBranchId());
				brhMap.put("branchName",brhList.get(i).getBranchName());
				brhMap.put("branchFaCode",brhList.get(i).getBranchFaCode());
				
				actBrhList.add(brhMap);
			}
			
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("list",actBrhList);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}

	
	
	@RequestMapping(value = "/getCnmtFrDlyRt", method = RequestMethod.POST)
	public @ResponseBody Object getCnmtFrDlyRt(@RequestBody Map<String,Object> clientMap){
		System.out.println("Inside getCnmtFrDlyRt function-------");
		Map<String, Object> map = new HashMap<String, Object>();
		
		int brhId = Integer.parseInt(String.valueOf(clientMap.get("brhId")));
		if(brhId > 0){
			List<BranchStockLeafDet> cnmtCodeList = branchStockLeafDetDAO.getCodeList(String.valueOf(brhId),"cnmt");
			if(!cnmtCodeList.isEmpty()){
				List<String> actCnList = new ArrayList<>();
				for(int i=0;i<cnmtCodeList.size();i++){
					actCnList.add(cnmtCodeList.get(i).getBrsLeafDetSNo());
				}
				if(!actCnList.isEmpty()){
					map.put("list",actCnList);
					map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
				}else{
					map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
				}
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			}
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		
		return map;
	}	
	
	@RequestMapping(value = "/submitDlyRt", method = RequestMethod.POST)
	public @ResponseBody Object submitDlyRt(@RequestBody Map<String,Object> clientMap){
		System.out.println("Inside submitDlyRt function-------");
		Map<String, Object> map = new HashMap<String, Object>();
		
		String brhCode = String.valueOf(clientMap.get("brhId"));
		String cnmtCode = String.valueOf(clientMap.get("cnmtCode"));
		boolean allow = false;
		if(String.valueOf(clientMap.get("allow")).equalsIgnoreCase("true")){
			allow = true;
		}else{
			allow = false;
		}
		
		System.out.println("value of allow = "+allow);
		
		int res = branchStockLeafDetDAO.updateCnmt(brhCode , cnmtCode , allow);
		if(res > 0){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}	
	
}
