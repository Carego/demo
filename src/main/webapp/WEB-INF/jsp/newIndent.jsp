<!-- <style>
table, th, td {
  border: 1px solid white;
}


</style> -->

		<div>
			<form ng-submit="saveIndent()" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
			
			</div>
			
			<div class="row">
			
			<div class="col s3 input-field">
			<input class="validate" type ="text" id="orderId" name ="orderIdName" ng-model="orderId" ng-blur="verifyOrderid()" ng-required="true">
				<label for="code">Order ID</label>	
		    </div>
			
			
			<!-- <div class="col s2 input-field">
			<input class="validate" type ="text" id="indentId" name ="indentIdName" ng-model="indentId" ng-required="true" readonly>
				<label for="code">Indent ID</label>	
		    </div> -->
			
				<div class="col s3 input-field">
			<input class="validate" type ="datetime-local" id="indentDateTimeId" name ="indentDateTimeName" ng-model="indentDateTime" ng-required="true" >
				<label for="code">Indent Date Time</label>	
		    </div>
		    
		    <div class="col s3 input-field">
			<input class="validate" type ="datetime-local" id="placeDateTimeId" name ="placeDateTimeName" ng-model="placeDateTime" ng-required="true" >
				<label for="code">Placement Date Time</label>	
		    </div>
		    
		    <div class="col s3 input-field">
			<input class="validate" type ="text" id="branchNameId" name ="branchName" ng-model="branchName" ng-click="getBranch()" ng-required="true" readonly >
				<label for="code">Branch</label>	
		    </div>
		    
			<!-- <div class="col s3 input-field">
			<input class="validate" type ="date" id="indentDateId" name ="indentDateName" ng-model="indentDate" ng-required="true" >
				<label for="code">Indent Date</label>	
		    </div>
		    
		    <div class="col s2 input-field">
		       		<input class="validate" type ="time" id="indentTimeId" name ="indentTimeName" ng-model="indentTime" ng-required="true" >
		       		<label for="code">Indent Time</label>	
		       		</div>
			
		    
		    <div class="col s3 input-field">
			<input class="validate" type ="date" id="placeDateId" name ="placeDateName" ng-model="placeDate" ng-required="true" >
				<label for="code">Placement Date</label>	
		    </div>
		    
		    <div class="col s2 input-field">
		       		<input class="validate" type ="time" id="placeTimeId" name ="placeTimeName" ng-model="placeTime" ng-required="true" >
		       		<label for="code">Placement Time</label>	
		       		</div> -->
			</div>
		    
		       		<div class="row">
			<div class="col s5 input-field">
		       				<input class="validate" type ="text" id="custId" name ="custName" ng-model="custName" ng-click="getCustomer()" ng-required="true" readonly>
		       			<label for="code">Customer Name</label>	
		       		</div>
		    
		    <div class="col s2 input-field">
				    		<select name="custTypeName" id="custTypeId"  ng-model="custType" ng-change="selectCustType()" ng-init="custType = 'Consignor'" ng-required="true">
								<option value="Consignor">Consignor</option>
            					<option value="Consignee">Consignee</option>
            					<option value="Both">Both</option>
							</select>
							<label>Customer Type</label>
					</div>
		    <div  class="col s5 input-field">
		    	<input class="validate" type ="text" id="conNameId" name ="conName" ng-model="conName" ng-readonly="conNameReadOnlyFlag" ng-keyup="getConsignorList($event.keyCode)" required >
		   		 <label ng-if="custType=='Both'">Customer Name</label>
		   		 <label ng-if="custType=='Consignee'">Consignor Name</label>
		   		 <label ng-if="custType=='Consignor'">Consignee Name</label>
		    </div>
		    
		    </div>
			
			<div class="row">
			
			<div  class="col s3 input-field">
		    	<input class="validate" type ="text" id="fromStnId" name ="fromStnName" ng-model="fromStn" ng-click="getFromLocation()" required readonly>
		    	<label>From Location</label>
		    </div>
		    
		    <div  class="col s3 input-field">
		   		 <input class="validate" type ="text" id="toStnId" name ="toStnName" ng-model="toStn" ng-click="getToLocation()" required readonly>
		    	 <label>To Location</label>
		    </div>
		    		
		    		<div  class="col s3 input-field">
		    	<input class="validate" type ="text" id="vehicleTypeId" name ="vehicleTypeName" ng-model="vehicleType"  required readonly>
		    	<label>Vehicle type</label>
		    </div>
		    
		     <div  class="col s3 input-field">
		   		 <input class="validate" type ="text" id="materialTypeId" name ="materialTypeName" ng-model="materialType"  required >
		    	 <label>Material</label>
		    </div>
		    <!-- <div  class="col s3 input-field">
		    	<input class="validate" type ="number" id="kmId" name ="kmName" ng-model="km"  required >
		    	<label>Kilometer</label>
		    </div>
		    
		    <div  class="col s3 input-field">
		   		 <input class="validate" type ="text" id="transitTimeId" name ="transitTimeName" ng-model="transitTime" required >
		    	 <label>Transit time</label>
		    </div> -->
		    			
			</div>
			
			
			
			<div class="row">
			
			
		    <div  class="col s3 input-field">
		    	<input class="validate" type ="number" id="totalWeightId" name ="totalWeightName" ng-model="totalWeight"  required >
		    	<label>Total Weight</label>
		    </div>
		    
		    <div  class="col s3 input-field">
		    	<input class="validate" type ="number" id="minWeightId" name ="minWeightName" ng-model="minWeight" ng-change="calculateTrgtRt()" required >
		    	<label>Minimum Weight Guarantee</label>
		    </div>
		    
		   <div  class="col s3 input-field">
		   		<select name="Name" id="cnmtDimensionTypeId"  ng-model="cnmtDimensionType"  ng-init="cnmtDimensionType = 'NORMAL'" ng-required="true">
								<option value="NORMAL">Normal</option>
            					<option value="ODC">ODC</option>
							</select>
		    <label>Consignment Dimension</label>
		    </div>
		    <div  class="col s3 input-field" ng-show="cnmtDimensionType == 'ODC'">
		    	<input class="validate" type ="text" id="dimensionId" name ="dimensionName" ng-model="dimension"  ng-required="cnmtDimensionType == 'ODC'" >
		    	<label>Dimension</label>
		    </div>
		    
		    <div  class="col s3 input-field">
		   		 <input class="validate" type ="number" id="totalVehicleId" name ="totalVehicleName" ng-model="totalVehicle" required >
		    	 <label>No. of Vehicles Required</label>
		    </div>
		    			
			</div>
			
			
			<div class="row">
			
			 <div  class="col s3 input-field">
		    	<input class="validate" type ="text" id="toolsId" name ="toolsName" ng-model="tools"  required >
		    	<label>Tools required</label>
		    </div>
		    
		    <!-- <div  class="col s3 input-field">
		    <select name="custTypeName" id="gpsId"  ng-model="gps"  ng-init="gps = 'No'" ng-required="true">
								<option value="Yes">Yes</option>
            					<option value="No">No</option>
							</select>
		    <label>GPS required</label>
		   		 <input class="validate" type ="text" id="gpsId" name ="gpsName" ng-model="gps"  required readonly>
		    	 <label>GPS required</label>
		    </div> -->
		    		
		    <div  class="col s3 input-field">
		    	<input class="validate" type ="number" id="ratePrTnId" name ="ratePrTnName" ng-model="ratePrTn" ng-change="calculateTrgtRt()" required >
		    	<label>Per Ton Rate</label>
		    </div>
		    
		    <div  class="col s3 input-field">
		   		 <input class="validate" type ="number" id="targetRateId" name ="targetRateName" ng-model="targetRate" ng-change="calculatePrTnRt()" required >
		    	 <label>Target Rate</label>
		    </div>
		    
		    </div>
			
			
				<div class="row">
		    
		     <div  class="col s3 input-field">
		   		 <input class="validate" type ="number" id="advancePercentId" name ="advancePercentName" ng-model="advancePercent" required >
		    	 <label>Advance %</label>
		    </div>
		    
		     <div  class="col s3 input-field">
		   		<select name="advPayModeName" id="advPayModeId"  ng-model="advPayMode"  ng-init="advPayMode = 'RTGS'" ng-required="true">
								<option value="RTGS">RTGS</option>
            					<option value="NEFT">NEFT</option>
            					<option value="PETRO">Petro Card</option>
							</select>
		    <label>Advance Payment Mode</label>
		    </div>
		    		
		   
		     <div  class="col s3 input-field">
		   		<select name="balPayModeName" id="balPayModeId"  ng-model="balPayMode"  ng-init="balPayMode = 'RTGS'" ng-required="true">
								<option value="RTGS">RTGS</option>
            					<option value="NEFT">NEFT</option>
            					<option value="PETRO">Petro Card</option>
							</select>
		    <label>Balance Payment Mode</label>
		    </div>
		    		
		    			
			</div>
			
			
			<div class="row">
				<div class="input-field col s12">
					<i class="mdi-editor-mode-edit prefix"></i>
 					<textarea id="textarea" class="materialize-textarea"  rows="3" cols="92"  name ="balTermsName" ng-model="balTerms" ng-required="true"></textarea>
 					<label>Balance Payment terms</label>
				</div>
    		 </div>	
			
			
				<div class="row">
	     		<div class="input-field col s12 center">
	       				<input class="validate" type ="submit" id="saveId" value="submit" >
	       		</div>
	        </div>
				
				</form>	
	</div>
			
			
			
	<div>
	<div id ="selCustId" ng-hide="selCustFlag">
		  <input type="text" name="filterCust" ng-model="filterCust" placeholder="Search....">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Branch Code</th>
 	  	  	  <th>Customer Name</th>
 	  	  	  <th>FaCode</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="cust in custList | filter:filterCust">
		 	  <td><input type="radio"  name="cust"   value="{{ cust }}" ng-model="custCode" ng-click="saveCustomer(cust)"></td>
              <td>{{cust.bCode}}</td>
              <td>{{cust.custName}}</td>
              <td>{{cust.custFaCode}}</td>
          </tr>
      </table> 
	</div>
	
	</div>
	
	
	<div id="consigDB" ng-hide="consignDBFlag">

	<input type="text" name="filterConsign"
		ng-model="filterConsign" placeholder="Search by Code">

	<table>
		<tr>
			<th></th>
			<th>Customer Code</th>
			<th>Branch Code</th>
			<th>Customer Name</th>
			<th>Daily Contract Allowed</th>
		</tr>

		<tr ng-repeat="consignor in customerList | filter:filterCNMTConsignor">
			<td><input type="radio" name="custCode" id="custId"
				value="{{ consignor.custCode }}" ng-model="consignorTmpCode"
				ng-click="saveConsignorCustomerCode(consignor)"></td>
			<td>{{ consignor.custCode }}</td>
			<td>{{ consignor.branchCode }}</td>
			<td>{{ consignor.custName }}</td>
			<td>{{ consignor.custIsDailyContAllow }}</td>
		</tr>
	</table>

</div>
	
	
	<div>
	<div id ="selBrhId" ng-hide="selBranchFlag">
		  <input type="text" name="filterBrh" ng-model="filterBrh" placeholder="Search....">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Branch Code</th>
 	  	  	  <th>Branch Name</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="brh in branchList | filter:filterBrh">
		 	  <td><input type="radio"  name="brhName"   value="{{ brh.branchName }}" ng-model="brh.BranchName" ng-click="saveBranch(brh)"></td>
              <td>{{brh.branchCode}}</td>
              <td>{{brh.branchName}}</td>
          </tr>
      </table> 
	</div>
	
	</div>
	
	
	<div>
	<div id ="fromStationDBId" ng-hide="fromStationDBFlag">
		  <input type="text" name="filterFrmStn" ng-model="filterFrmStn" placeholder="Search....">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Station Name</th>
 	  	  	  <th>Pin Code</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="frmStn in frmStnList | filter:filterFrmStn">
		 	  <td><input type="radio"  name="frmStn"   value="{{ frmStn.stnName }}" ng-model="frmStn.stnName" ng-click="saveFromStn(frmStn)"></td>
              <td>{{frmStn.stnName}}</td>
              <td>{{frmStn.stnPin}}</td>
          </tr>
      </table> 
	</div>
	
	</div>
	
   <div>
	<div id ="toStationDBId" ng-hide="toStationDBFlag">
		  <input type="text" name="filterToStn" ng-model="filterToStn" placeholder="Search....">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Station Name</th>
 	  	  	  <th>Pin Code</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="toStn in toStnList | filter:filterToStn">
		 	  <td><input type="radio"  name="toStn"   value="{{ toStn.stnName }}" ng-model="toStn.stnName" ng-click="saveToStn(toStn)"></td>
              <td>{{toStn.stnName}}</td>
              <td>{{toStn.stnPin}}</td>
          </tr>
      </table> 
	</div>
	
	</div>
	
	
	
	 <div>
	<div id ="toVehicleTypeDBId" ng-hide="toVehicleTypeDBFlag">
		  <input type="text" name="filterVehicleType" ng-model="filterVehicleType" placeholder="Search....">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Vehicle Type</th>
 	  	  	  <th>Weight Limit</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="veh in vehicleTypeList | filter:filterVehicleType">
		 	  <td><input type="radio"  name="vehicleType"   value="{{ veh }}" ng-model="veh.vtVehicleType" ng-click="saveVehicleType(veh)"></td>
              <td>{{veh.vtVehicleType}}</td>
              <td>{{veh.vtCode}}</td>
          </tr>
      </table> 
	</div>
	
	</div>
	
	
	<div id ="toProductTypeDBId" ng-hide="toProductTypeDBFlag">
		  <input type="text" name="filterProductType" ng-model="filterProductType" placeholder="Search....">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Material Type</th>
<!--  	  	  	  <th>Weight Limit</th> -->
 	  	  </tr>
 	  
		  <tr ng-repeat="cts in ctsList | filter:filterProductType">
		 	  <td><input type="radio"  name="productType"   value="{{ cts.ctsId }}" ng-model="cts" ng-click="saveProductType(cts)"></td>
              <td>{{cts.ctsProductType}}</td>
<!--               <td>{{veh.vtCode}}</td> -->
          </tr>
      </table> 
	</div>
	
	
	<!-- <div id ="toOwnBrkDBId" ng-hide="toOwnBrkDBFlag">
		  <input type="text" name="filterOwnBrk" ng-model="filterOwnBrk" placeholder="Search....">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>index</th>
 	  	  	  <th>ID</th>
 	  	  	  <th>Company Name</th>
 	  	  	  <th>Company Owner Name</th>
 	  	  	  <th>Address</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="ownBrk in ownBrkList | filter:filterOwnBrk">
		 	  <td><input type="radio"  name="ownBrk"   value="{{ ownBrk }}" ng-model="ownBrk.master_Id" ng-click="sendSelectedListToAPI(ownBrk)"></td>
              <td>{{$index + 1}}</td>
              <td>{{ownBrk.master_Id}}</td>
              <td>{{ownBrk.company_Name}}</td>
              <td>{{ownBrk.company_Owner_Name}}</td>
              <td>{{ownBrk.company_Office_Address}}</td>
          </tr>
      </table> 
	</div> -->
	
	<div id ="toOwnBrkDBId" ng-hide="toOwnBrkDBFlag">
		  <input type="text" name="filterOwnBrk" ng-model="filterOwnBrk" placeholder="Search....">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>index</th>
 	  	  	  <th>ID</th>
 	  	  	  <th>Company Name</th>
 	  	  	  <th>Company Owner Name</th>
 	  	  	  <th>Address</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="ownBrk in ownBrkList | filter:filterOwnBrk">
		 	  <td><input type="checkbox"   ng-model="ownBrk.Selected"  ng-init="ownBrk.Selected=true"  ></td>
              <td>{{$index + 1}}</td>
              <td>{{ownBrk.master_Id}}</td>
              <td>{{ownBrk.company_Name}}</td>
              <td>{{ownBrk.company_Owner_Name}}</td>
              <td>{{ownBrk.company_Office_Address}}</td>
          </tr>
        
      </table> 
       <input type="button" ng-click="submitList()" value="submit">
       
       <div class="col s12 center" ng-show="excelButtonFlag">
			<form method="post" action="downloadIndentAllExcel" enctype="multipart/form-data">
				<input type="submit" id="printXlsId" value="Print XLS">
		  	</form>
		</div>
      <!-- <ul>
      	<li><input type="checkbox" name="colors[]" value="red" />Red</li>
      <ul> -->
	</div>
	
	