package com.mylogistics.DAOImpl.webservice;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.mapping.Array;
import org.hibernate.sql.Alias;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;

import com.mylogistics.DAO.FAParticularDAO;
import com.mylogistics.DAO.FaMasterDAO;
import com.mylogistics.DAO.werservice.LoadingDao;
import com.mylogistics.constants.BrokerCNTS;
import com.mylogistics.constants.OwnerCNTS;
import com.mylogistics.constants.webservice.LoadingModelCNTS;
import com.mylogistics.model.Broker;
import com.mylogistics.model.CashStmt;
import com.mylogistics.model.CashStmt1516;
import com.mylogistics.model.CashStmtStatus;
import com.mylogistics.model.FAMaster;
import com.mylogistics.model.FAParticular;
import com.mylogistics.model.Owner;
import com.mylogistics.model.webservice.LoadingModel;
import com.mylogistics.model.webservice.VendorDetail;
import com.mylogistics.model.webservice.VendorDetail.SelectedVendor;
import com.mylogistics.model.webservice.VendorDetail.UnSelectedVendor;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.ValidatorFactoryUtil;

public class LoadingDaoImpl implements LoadingDao {
	
	public static Logger logger = Logger.getLogger(LoadingDaoImpl.class);
	
	@Autowired
	private FAParticularDAO faParticularDAO;
	
	@Autowired
	private FaMasterDAO faMasterDAO;

	public Map<String, Object> getOwnBrkWs(Session session, String branchCode){
		logger.info("Enter into getOwnBrkWs() : BranchCode = "+branchCode);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try{			
			Criteria cr = session.createCriteria(Owner.class)
					.add(Restrictions.eq(OwnerCNTS.BRANCH_CODE, branchCode));
			
			ProjectionList projectionList = Projections.projectionList();
			projectionList.add(Projections.property(OwnerCNTS.OWN_NAME), "name");
			projectionList.add(Projections.property(OwnerCNTS.OWN_CODE), "code");
			cr.setProjection(projectionList);
			
			List<Map<String, Object>> ownList = cr.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP)
					.list();
			
			cr = session.createCriteria(Broker.class)
					.add(Restrictions.eq(BrokerCNTS.BRANCH_CODE, branchCode));
			
			projectionList = Projections.projectionList();
			projectionList.add(Projections.property(BrokerCNTS.BRK_NAME), "name");
			projectionList.add(Projections.property(BrokerCNTS.BRK_CODE), "code");
			cr.setProjection(projectionList);
			
			List<Map<String, Object>> brkList = cr.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP)
					.list();
			
			List<Map<String, Object>> finalList = new ArrayList<Map<String,Object>>();
			finalList.addAll(ownList);
			finalList.addAll(brkList);
			
			ownList.clear();
			brkList.clear();
			
			resultMap.put("ownBrkList", finalList);
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
					
		}catch(Exception e){
			logger.error("Exception : "+e);
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		logger.info("Exit from getOwnBrkWs() : BranchCode = "+branchCode);
		return resultMap;
	}
	
	public Map<String, Object> saveLoadingDetail(Session session, LoadingModel loadingModel){
		logger.info("Enter into saveLoadingDetail() ");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		List<String> msg = new ArrayList<String>();
		try{
			List<VendorDetail> vendorList = loadingModel.getVendorDetails();
			
			Validator validator = ValidatorFactoryUtil.getValidator();			
			Set<ConstraintViolation<LoadingModel>> errMsgLoading = validator.validate(loadingModel);
			Set<ConstraintViolation<VendorDetail>> errMsgVDetail = null;
			
			if(errMsgLoading.isEmpty()){
				
				Integer id = (Integer) session.save(loadingModel);
				
				Iterator<VendorDetail> it = vendorList.iterator();
				Boolean status = true;
				while(it.hasNext()){
					logger.info("Enter into VendorDetailList()");
					VendorDetail vDetail = it.next();
					if(vDetail.getIsSelected()){
						logger.info("Enter into Selected()");
						errMsgVDetail = validator.validate(vDetail, SelectedVendor.class);
						if(! errMsgVDetail.isEmpty()){
							Iterator<ConstraintViolation<VendorDetail>> vv = errMsgVDetail.iterator();
							while(vv.hasNext()){
								ConstraintViolation<VendorDetail> err = vv.next();
								msg.add(err.getMessage());								
							}							
							status = false;
						}
					}else{
						logger.info("Enter into not Selected()");
						errMsgVDetail = validator.validate(vDetail, UnSelectedVendor.class);
						if(! errMsgVDetail.isEmpty()){
							Iterator<ConstraintViolation<VendorDetail>> vv = errMsgVDetail.iterator();
							while(vv.hasNext()){
								ConstraintViolation<VendorDetail> err = vv.next();
								msg.add(err.getMessage());								
							}					
							status = false;
						}
					}
				}
				if(status){
					createOwnerOrBroker(session, vendorList, loadingModel.getBranchCode(), loadingModel.getUserCode());
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
					msg.add("Record is saved !");
					resultMap.put("msg", msg);
				}else{					
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					resultMap.put("msg", msg);
				}					
			}else{
				Iterator<ConstraintViolation<LoadingModel>> it = errMsgLoading.iterator();
				while(it.hasNext()){
					ConstraintViolation<LoadingModel> err = it.next();
					msg.add(err.getMessage());
					logger.info("Validation Error = "+err.getMessage());
				}
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				resultMap.put("msg", msg);				
			}
			
		}catch(Exception e){
			logger.error("Error : "+e);
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", "Please try again !");
		}
		logger.info("Exit from saveLoadingDetail()");
		return resultMap;		
	}
	
	public Map<String, Object> saveLoadingDetail1(Session session, LoadingModel loadingModel){
		logger.info("Enter into saveLoadingDetail() ");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		List<String> msg = new ArrayList<String>();
		String orderNo = "";
		try{
			
			List<LoadingModel> list = session.createCriteria(LoadingModel.class)
					.addOrder(Order.desc(LoadingModelCNTS.LD_ID))
					.setMaxResults(1)
					.list();
			
			if(list.isEmpty())
				orderNo = "01";
			else{
				orderNo = list.get(0).getOrderNo();
				orderNo = "0" +String.valueOf(Integer.parseInt(orderNo)+1);				
			}						
			loadingModel.setOrderNo(orderNo);
			loadingModel.setIsPending(true);
			
			Validator validator = ValidatorFactoryUtil.getValidator();			
			Set<ConstraintViolation<LoadingModel>> errMsgLoading = validator.validate(loadingModel);
			
			if(errMsgLoading.isEmpty()){
				Integer id = (Integer) session.save(loadingModel);
				if(id > 0){
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
					resultMap.put("orderNo", orderNo);
					resultMap.put("msg", "Record is saved !");
				}else{
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					resultMap.put("msg", "Please try again !");
				}
			}else{
				Iterator<ConstraintViolation<LoadingModel>> it = errMsgLoading.iterator();
				while(it.hasNext()){
					ConstraintViolation<LoadingModel> err = it.next();
					msg.add(err.getMessage());
					logger.info("Validation Error = "+err.getMessage());
				}
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				resultMap.put("msg", "Please try again !");		
			}			
		}catch(Exception e){
			logger.error("Error : "+e);
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", "Please try again !");
		}
		logger.info("Exit from saveLoadingDetail()");
		return resultMap;		
	}
	
	@Override
	public Map<String, Object> saveVendorDetail(Session session, LoadingModel loadingModel){
		logger.info("Enter into saveVendorDetail() ");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		List<String> msg = new ArrayList<String>();
		String orderNo = "";
		try{
			Validator validator = ValidatorFactoryUtil.getValidator();		
			Iterator<VendorDetail> it = loadingModel.getVendorDetails().iterator();
			Set<ConstraintViolation<VendorDetail>> errMsgVDetail = null;
			
			Boolean status = true;
			
			while(it.hasNext()){			
				VendorDetail vDetail = it.next();								
				errMsgVDetail = validator.validate(vDetail, UnSelectedVendor.class);
				if(! errMsgVDetail.isEmpty()){
					Iterator<ConstraintViolation<VendorDetail>> vv = errMsgVDetail.iterator();
					while(vv.hasNext()){
						ConstraintViolation<VendorDetail> err = vv.next();
						logger.info(err.getMessage());								
					}							
					status = false;
				}
			}
			
			if(status){
				LoadingModel realLoadingModel = (LoadingModel) session.get(LoadingModel.class, loadingModel.getLdId());
				if(realLoadingModel != null){
					realLoadingModel.getVendorDetails().addAll(loadingModel.getVendorDetails());
					session.update(realLoadingModel);
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
					resultMap.put("orderNo", orderNo);
					resultMap.put("msg", "Record is saved !");
					
					createOwnerOrBroker(session, loadingModel.getVendorDetails(), loadingModel.getBranchCode(), loadingModel.getUserCode());
					
				}else{
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					resultMap.put("msg", "Please try again !");
				}	
			}else{
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				resultMap.put("msg", "Please try again !");
			}
					
		}catch(Exception e){
			logger.error("Error : "+e);
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", "Please try again !");
		}
		logger.info("Exit from saveVendorDetail()");
		return resultMap;		
	}
	
	@Override
	public Map<String, Object> getPendingOrder(Session session, String bCode, Map<String, Object> resultMap){
		try{
			Criteria cr = session.createCriteria(LoadingModel.class)
					.add(Restrictions.eq(LoadingModelCNTS.BRANCH_CODE, bCode))
					.add(Restrictions.eq(LoadingModelCNTS.IS_PENDING, true));
			
			ProjectionList projectionList = Projections.projectionList();
			
			projectionList.add(Projections.property(LoadingModelCNTS.LD_ID), LoadingModelCNTS.LD_ID);
			projectionList.add(Projections.property(LoadingModelCNTS.CUSTOMER_NAME), LoadingModelCNTS.CUSTOMER_NAME);
			projectionList.add(Projections.property(LoadingModelCNTS.SOURCE_NAME), LoadingModelCNTS.SOURCE_NAME);
			projectionList.add(Projections.property(LoadingModelCNTS.DESTINATION_NAME), LoadingModelCNTS.DESTINATION_NAME);
			projectionList.add(Projections.property(LoadingModelCNTS.ORDER_NO), LoadingModelCNTS.ORDER_NO);
			
			cr.setProjection(projectionList);
			cr.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
			
			List<Map<String, Object>> loadingModelList = cr.list();
			if(loadingModelList.isEmpty()){
				logger.error("No such records !");
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				resultMap.put("msg", "No such records !");
			}else{
				logger.error("Records found = "+loadingModelList.size()+" !");
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				resultMap.put("loadingModel", loadingModelList);
			}
		}catch(Exception e){
			logger.error("Error : "+e);
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", "Please try again !");
		}
		return resultMap;
	}
	
	public void createOwnerOrBroker(Session session, List<VendorDetail> list, String branchCode, String userCode){
		logger.info("Enter into createOwnerOrBroker()");
		try{
			Iterator<VendorDetail> it = list.iterator();
			while(it.hasNext()){
				VendorDetail vd = (VendorDetail) it.next();
				
				if(vd.getTypeOfVendor().equalsIgnoreCase("Owner") || vd.getTypeOfVendor().equalsIgnoreCase("Broker")){
					
					if(vd.getVendorCode().equalsIgnoreCase("") && vd.getTypeOfVendor().equalsIgnoreCase("Owner")){
						
							List<FAParticular> faParticulars = faParticularDAO.getFAParticular(ConstantsValues.FAP_OWN);
							if(! faParticulars.isEmpty()){
								Owner owner = new Owner();
								FAParticular fAParticular = faParticulars.get(0);
								int fapId = fAParticular.getFaPerId();
								String fapIdStr = String.valueOf(fapId); 
								if(fapId < 10)
									fapIdStr="0"+fapIdStr;
								owner.setOwnName(vd.getVendorName());
								owner.setfAParticular(fAParticular);
								owner.setView(false);
								owner.setUserCode(userCode);
								owner.setbCode(branchCode);
								owner.setBranchCode(branchCode);
								owner.setOwnPanIntRt(0);
								// Save Owner
								int ownId = (Integer) session.save(owner);
								owner.setOwnId(ownId);
								// Owner Code
								String ownerCode = "own"+String.valueOf(ownId);
								owner.setOwnCode(ownerCode);
								// Owner FaCode
								ownerCode = "own"+String.valueOf(ownId); 
								ownId=ownId+100000;								
								String ownIdStr = String.valueOf(ownId).substring(1,6);
								String faCode=fapIdStr+ownIdStr;
								owner.setOwnFaCode(faCode);
								
								session.update(owner);
								
								FAMaster faMaster = new FAMaster();
								faMaster.setFaMfaCode(faCode);
								faMaster.setFaMfaType("owner");
								faMaster.setFaMfaName(owner.getOwnName());
								faMaster.setbCode(branchCode);
								faMaster.setUserCode(userCode);
								
								faMasterDAO.saveFaMaster(faMaster);
							}							
						}
					
					if(vd.getVendorCode().equalsIgnoreCase("") && vd.getTypeOfVendor().equalsIgnoreCase("Broker")){						
							List<FAParticular> faParticulars = faParticularDAO.getFAParticular(ConstantsValues.FAP_BRK);
							if(! faParticulars.isEmpty()){
								Broker broker  = new Broker();
								FAParticular fAParticular = faParticulars.get(0);
								int fapId = fAParticular.getFaPerId();
								String fapIdStr = String.valueOf(fapId); 
								if(fapId < 10)
									fapIdStr="0"+fapIdStr;
								broker.setBrkName(vd.getVendorName());
								broker.setfAParticular(fAParticular);
								broker.setView(false);
								broker.setUserCode(userCode);
								broker.setbCode(branchCode);
								broker.setBranchCode(branchCode);
								broker.setBrkPanIntRt(0);
								// Save Broker
								int brkId = (Integer) session.save(broker);
								broker.setBrkId(brkId);
								// Broker Code
								String brokerCode = "brk"+String.valueOf(brkId);
								broker.setBrkCode(brokerCode);
								// Owner FaCode
								brokerCode = "brk"+String.valueOf(brkId); 
								brkId = brkId+100000;								
								String brkIdStr = String.valueOf(brkId).substring(1,6);
								String faCode = fapIdStr + brkIdStr;
								broker.setBrkFaCode(faCode);
								
								session.update(broker);
								
								FAMaster faMaster = new FAMaster();
								faMaster.setFaMfaCode(faCode);
								faMaster.setFaMfaType("broker");
								faMaster.setFaMfaName(broker.getBrkName());
								faMaster.setbCode(branchCode);
								faMaster.setUserCode(userCode);
								
								faMasterDAO.saveFaMaster(faMaster);
						}
					}			
					
				if(! vd.getVendorCode().equalsIgnoreCase("") && vd.getTypeOfVendor().equalsIgnoreCase("Owner")){
				List<Owner> ownList = session.createCriteria(Owner.class)
						.add(Restrictions.eq(OwnerCNTS.OWN_CODE, vd.getVendorCode()))
						.list();
				if(! ownList.isEmpty()){
					Owner owner = ownList.get(0);
					ArrayList<String> phoneList = owner.getOwnPhNoList();
					if(phoneList != null){
						if(! phoneList.contains(vd.getMobileNo())){
							phoneList.add(vd.getMobileNo());
							owner.setOwnPhNoList(phoneList);
							session.update(owner);
						}
					}else{
						phoneList = new ArrayList<String>();
						phoneList.add(vd.getMobileNo());
						owner.setOwnPhNoList(phoneList);
						session.update(owner);
					}
				}
			}
				
			if(! vd.getVendorCode().equalsIgnoreCase("") && vd.getTypeOfVendor().equalsIgnoreCase("Broker")){
				System.err.println("Code = "+vd.getVendorCode());				
				List<Broker> brkList = session.createCriteria(Broker.class)
						.add(Restrictions.eq(BrokerCNTS.BRK_CODE, vd.getVendorCode()))
						.list();
				System.err.println("Size = "+brkList.size());
				if(! brkList.isEmpty()){
					Broker broker = brkList.get(0);
					ArrayList<String> phoneList = broker.getBrkPhNoList();
					if(phoneList != null){
						System.err.println("$$$$$");
						if(! phoneList.contains(vd.getMobileNo())){
							phoneList.add(vd.getMobileNo());
							broker.setBrkPhNoList(phoneList);
							session.update(broker);
						}
					}else{
						System.err.println("#####");
						phoneList = new ArrayList<String>();
						phoneList.add(vd.getMobileNo());
						broker.setBrkPhNoList(phoneList);
						session.update(broker);
					}
				}
			}
				}
		}
		}catch(Exception e){
			logger.error("Error in createOwnerOrBroker : Exception = "+e);
		}
	}
	
	@Override
	public List<String> getTrafficOfficers(Session session){
		logger.info("Enter into getTrafficOfficers()");
		List<String> list = new ArrayList<String>();
		Transaction transaction = session.beginTransaction();
		try{
			list = session.createQuery("SELECT DISTINCT(officerName) FROM LoadingModel AS lm")					
					.list();
			
		}catch(Exception e){
			logger.error("Exception = "+e);
		}
		transaction.commit();
		logger.info("Exit from getTrafficOfficers() : Size = "+list.size());
		return list;
	}
	
	@Override
	public List<Map<String, Object>> getLoadingDt(Session session, String branchCode, String fromDate, String toDate, String officerName){		
		logger.info("Enter into getLoadingDt()");
		List<Map<String, Object>> finalList = new ArrayList<Map<String,Object>>();
		try{
			Criteria cr = session.createCriteria(LoadingModel.class);
			
			if(! branchCode.equalsIgnoreCase(""))
				cr.add(Restrictions.eq(LoadingModelCNTS.BRANCH_CODE, branchCode));
			if(! officerName.equalsIgnoreCase(""))
				cr.add(Restrictions.eq(LoadingModelCNTS.OFFICER_NAME, officerName));
			if(! fromDate.equalsIgnoreCase(""))
				cr.add(Restrictions.sqlRestriction("creationTS >= '"+fromDate+"'"));			
			if(! toDate.equalsIgnoreCase(""))
				cr.add(Restrictions.sqlRestriction("creationTS <= '"+toDate+"'"));		
			
			cr.setFetchMode(LoadingModelCNTS.VENDOR_DETAILS, FetchMode.SELECT);
			cr.addOrder(Order.asc(LoadingModelCNTS.OFFICER_NAME));			
			
			List<LoadingModel> list = cr.list();			
			if(! list.isEmpty()){				
				Iterator<LoadingModel> it = list.iterator();
				while(it.hasNext()){
					
					Map map = new HashMap<String, Object>();
					LoadingModel lModel = it.next();
					
					map.put("branchName", lModel.getBranchName());
					map.put("officerName", lModel.getOfficerName());
					map.put("customerName", lModel.getCustomerName());
					map.put("sourceName", lModel.getSourceName());
					map.put("destinationName", lModel.getDestinationName());
					map.put("quantity", lModel.getQuantity());
					map.put("metricOrTon", lModel.getMetricOrTon());
					map.put("vehicleType", lModel.getVehicleType());
					
					List<VendorDetail> vList = lModel.getVendorDetails();
					Iterator<VendorDetail> vIterator = vList.iterator();
					List<Map<String, Object>> vvList = new ArrayList<Map<String, Object>>();
					while(vIterator.hasNext()){
						VendorDetail vd = (VendorDetail) vIterator.next();
						Map vMap = new HashMap<String, Object>();
						vMap.put("vendorName", vd.getVendorName());
						vMap.put("vendorType", vd.getTypeOfVendor());
						vMap.put("mobile", vd.getMobileNo());
						vMap.put("rate", vd.getRate());
						vMap.put("isSelected", vd.getIsSelected());
						vMap.put("reason", vd.getReason());
						vvList.add(vMap);						
					}					
					map.put("vendorDetails", vvList);
					finalList.add(map);
				}				
			}					
		}catch(Exception e){
			logger.error("Exception - "+e);
		}
		logger.info("Exit from getLoadingDt()");
		return finalList;
	}
	
	public Map<String, Object> getVendorDt(Session session, Integer ldId, Map<String, Object> resultMap){
		logger.info("Enter into getVendorDt()");
		try{
			LoadingModel loadingModel = (LoadingModel) session.get(LoadingModel.class, ldId);
			if(loadingModel == null){
				resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				resultMap.put("msg", "Order detail not found !");
			}else{
				if(loadingModel.getVendorDetails().isEmpty()){
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					resultMap.put("msg", "Vendor detail not found !");
				}else{
					resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
					resultMap.put("vendorDt", loadingModel.getVendorDetails());
				}
			}
		}catch(Exception e){
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", "Vendor detail not found !");
			logger.error("Exception : "+e);
		}
		logger.info("Exit from getVendorDt()");
		return resultMap;
	}
	
	public Map<String, Object> saveFinalOrder(Session session, VendorDetail vDetail, Map<String, Object> resultMap){
		logger.info("Enter into saveFinalOrder()");
		try{
			VendorDetail realVDt = (VendorDetail) session.get(VendorDetail.class, vDetail.getVdId());
			LoadingModel ldModel = realVDt.getLoadingModel();
			
			realVDt.setIsSelected(vDetail.getIsSelected());
			realVDt.setReason(vDetail.getReason());
			
			session.update(realVDt);
			
			ldModel.setIsPending(false);
			session.update(ldModel);
			
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			resultMap.put("msg", "Vendor detail is updated !");
			
		}catch(Exception e){
			resultMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			resultMap.put("msg", "Vendor detail not found !");
			logger.error("Exception : "+e);
		}
		logger.info("Exit from saveFinalOrder()");
		return resultMap;
	}
	
}