package com.mylogistics.mobileApi.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.mylogistics.DAO.AddressDAO;
import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.BrokerDAO;
import com.mylogistics.DAO.EmployeeDAO;
import com.mylogistics.DAO.FaMasterDAO;
import com.mylogistics.DAO.OwnBrkKycBusinessDetailsDAO;
import com.mylogistics.DAO.OwnerDAO;
import com.mylogistics.DAO.StateDAO;
import com.mylogistics.DAO.StationDAO;
import com.mylogistics.DAO.UserDAO;
import com.mylogistics.DAO.VehicleTypeDAO;
import com.mylogistics.mobileApi.controller.model.OwnerKYCBankAccountDetails;
import com.mylogistics.mobileApi.controller.model.OwnerKYCPersonalDetails;
import com.mylogistics.model.Address;
import com.mylogistics.model.AllStation;
import com.mylogistics.model.Broker;
import com.mylogistics.model.BrokerImg;
import com.mylogistics.model.Employee;
import com.mylogistics.model.FAMaster;
import com.mylogistics.model.FAParticular;
import com.mylogistics.model.Owner;
import com.mylogistics.model.OwnerImg;
import com.mylogistics.model.State;
import com.mylogistics.model.VehicleType;
import com.mylogistics.model.app.AppLoginOTP;
import com.mylogistics.services.OwnBrkKycBusinessDetails;

@RestController
public class OwnerKYCCntlr {
	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private OwnerDAO ownerDAO;

	@Autowired
	private AddressDAO addressDAO;

	@Autowired
	private HttpSession httpSession;

	@Autowired
	private UserDAO userDAO;

	@Autowired
	private EmployeeDAO employeeDAO;

	@Autowired
	private BranchDAO branchDAO;

	@Autowired
	private StationDAO stationDAO;

	@Autowired
	private StateDAO stateDAO;
	
	@Autowired
	private FaMasterDAO faMasterDAO;
	
	@Autowired
	private BrokerDAO brokerDAO;
	
	@Autowired
	private OwnBrkKycBusinessDetailsDAO ownBrkKycBusinessDetailsDAO ;

	@Autowired
	private VehicleTypeDAO vehicleTypeDAO;
	/*private String supplierPanImgPath = "/var/www/html/Erp_Image/App/Supplier/Pan";
	private String supplierDecImgPath = "/var/www/html/Erp_Image/App/Supplier/Dec";
	private String supplierBankDetImgPath = "/var/www/html/Erp_Image/App/Supplier/BankDetail";
	private String supplierAdrsPrfImgPath = "/var/www/html/Erp_Image/App/Supplier/AdrsPrf";
*/
	private String ownerKYCDocs = "/var/www/html/Erp_Image/App/OwnerKycDocs";

	
	
	@RequestMapping(value = "/saveOwnerKYCPersonalDetail", method = RequestMethod.POST)
	public Object saveOwnerKYCPersonalDetail(@RequestBody OwnerKYCPersonalDetails ownerKYCPersonalDetails) {
		Map<String, Object> resultMap = new HashMap<String, Object>();

		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try {
			 if( ownerKYCPersonalDetails.getEmpId()>0) {
				 
				 if(ownerKYCPersonalDetails.getOwnBrkType().equalsIgnoreCase("Owner")) {

						Employee employee = (Employee) session.get(Employee.class, ownerKYCPersonalDetails.getEmpId());
						List<Owner> ownList = ownerDAO.getPhoneNo();

						for (Owner own : ownList) {
							if (own.getOwnPhNoList() != null && !own.getOwnPhNoList().isEmpty())
								for (String phone : own.getOwnPhNoList())
									if (ownerKYCPersonalDetails.getPhoneNo().equals(phone)) {

										System.out.println("Yes= " + phone + " " + own.getOwnCode());
										resultMap.put("data", own.getOwnCode());
										resultMap.put("isSuccess", false);
										resultMap.put("message", "Phone No. already exist");
										return resultMap;
									}
						}

						Owner owner = new Owner();
						ArrayList<String> phoneNoList = new ArrayList<>();

						phoneNoList.add(ownerKYCPersonalDetails.getPhoneNo());
						if (ownerKYCPersonalDetails.getAlternateContactNo() != null)
							phoneNoList.add(ownerKYCPersonalDetails.getAlternateContactNo());

						owner.setOwnPhNoList(phoneNoList);
						owner.setOwnName(ownerKYCPersonalDetails.getOwnName());
						owner.setOwnEmailId(ownerKYCPersonalDetails.getEmailAddress());
						owner.setOwnPanNo(ownerKYCPersonalDetails.getPanNo());
						owner.setOwnValidPan(false);
						int ownId = ownerDAO.saveOwner(session, owner);
						
						owner.setOwnCode("own" + ownId);
						int ownerFaCode = 800000 + ownId;
						owner.setOwnFaCode("0" + ownerFaCode);
						owner.setbCode(employee.getbCode());
						owner.setBranchCode(employee.getBranchCode());
						owner.setUserCode(employee.getEmpCode());
						owner.setEmpId(ownerKYCPersonalDetails.getEmpId());

						FAParticular fAParticular = (FAParticular) session.get(FAParticular.class, 8);

						owner.setfAParticular(fAParticular);
						owner.setView(false);
						
						FAMaster fAMaster=new FAMaster();
						fAMaster.setbCode(employee.getbCode());
						fAMaster.setFaMfaCode("0" + ownerFaCode);
						fAMaster.setFaMfaName(ownerKYCPersonalDetails.getOwnName());
						fAMaster.setFaMfaType("owner");
						fAMaster.setUserCode(employee.getEmpCode());
						faMasterDAO.saveFaMaster(fAMaster,session);
						
						
						OwnerImg ownerImg=new OwnerImg();
						ownerImg.setOwnAddressFImgPath(ownerKYCPersonalDetails.getAddressProofUrlFront());
						ownerImg.setOwnAddressBImgPath(ownerKYCPersonalDetails.getAddressProofUrlBack());
						ownerImg.setOwnId(ownId);
						ownerImg.setOwnPanImgPath(ownerKYCPersonalDetails.getPanImageUrl());
						owner.setOwnIsPanImg(true);
						int ownImgId = ownerDAO.saveOwnImg(session, ownerImg);
						owner.setOwnImgId(ownImgId);

						ownerDAO.updateOwner(session, owner);

						Address address = new Address();

						address.setAddCity(ownerKYCPersonalDetails.getAddCity());
						address.setAddDist(ownerKYCPersonalDetails.getAddDist());
						address.setAddPin(ownerKYCPersonalDetails.getAddPin());
						address.setAddPost(ownerKYCPersonalDetails.getAddArea());
						address.setAddState(ownerKYCPersonalDetails.getAddState());
						address.setAddRefCode(owner.getOwnCode());
						address.setAddType("Current Address");
						address.setCompleteAdd(ownerKYCPersonalDetails.getPlotNo());
						address.setbCode(employee.getBranchCode());
						address.setUserCode(employee.getEmpCode());

						addressDAO.saveAddress(address, session);

						session.flush();
						session.clear();
						transaction.commit();
						resultMap.put("isSuccess", true);
						resultMap.put("data", owner.getOwnCode());
						resultMap.put("message", "Detail succesfully Saved");
						
						  
				 }else if(ownerKYCPersonalDetails.getOwnBrkType().equalsIgnoreCase("Broker")) {
					//Broker
					 


						Employee employee = (Employee) session.get(Employee.class, ownerKYCPersonalDetails.getEmpId());
						List<Broker> brkList = brokerDAO.getPhoneNo();

						for (Broker brk : brkList) {
							if (brk.getBrkPhNoList() != null && !brk.getBrkPhNoList().isEmpty())
								for (String phone : brk.getBrkPhNoList())
									if (ownerKYCPersonalDetails.getPhoneNo().equals(phone)) {

										System.out.println("Yes= " + phone + " " + brk.getBrkCode());
										resultMap.put("data", brk.getBrkCode());
										resultMap.put("isSuccess", false);
										resultMap.put("message", "Phone No. already exist");
										return resultMap;
									}
						}

						Broker broker = new Broker();
						ArrayList<String> phoneNoList = new ArrayList<>();

						phoneNoList.add(ownerKYCPersonalDetails.getPhoneNo());
						if (ownerKYCPersonalDetails.getAlternateContactNo() != null)
							phoneNoList.add(ownerKYCPersonalDetails.getAlternateContactNo());

						broker.setBrkPhNoList(phoneNoList);
						broker.setBrkName(ownerKYCPersonalDetails.getOwnName());
						broker.setBrkEmailId(ownerKYCPersonalDetails.getEmailAddress());
						broker.setBrkFirmName(ownerKYCPersonalDetails.getCompanyName());
						broker.setBrkFirmType(ownerKYCPersonalDetails.getFirmType());
						broker.setBrkPanNo(ownerKYCPersonalDetails.getPanNo());
						broker.setBrkValidPan(false);
						int brkId = brokerDAO.saveBroker(broker,session);
						
						broker.setBrkCode("brk" + brkId);
						int brokerFaCode = 900000 + brkId;
						broker.setBrkFaCode("0" + brokerFaCode);
						broker.setbCode(employee.getbCode());
						broker.setBranchCode(employee.getBranchCode());
						broker.setUserCode(employee.getEmpCode());
//						broker.setEmpId(ownerKYCPersonalDetails.getEmpId());

						FAParticular fAParticular = (FAParticular) session.get(FAParticular.class, 9);

						broker.setfAParticular(fAParticular);
						broker.setView(false);
						
						FAMaster fAMaster=new FAMaster();
						fAMaster.setbCode(employee.getbCode());
						fAMaster.setFaMfaCode("0" + brokerFaCode);
						fAMaster.setFaMfaName(ownerKYCPersonalDetails.getOwnName());
						fAMaster.setFaMfaType("broker");
						fAMaster.setUserCode(employee.getEmpCode());
						faMasterDAO.saveFaMaster(fAMaster,session);
						
						
						BrokerImg brokerImg=new BrokerImg();
						brokerImg.setBrkAddressFImgPath(ownerKYCPersonalDetails.getAddressProofUrlFront());
						brokerImg.setBrkAddressBImgPath(ownerKYCPersonalDetails.getAddressProofUrlBack());
						brokerImg.setBrkId(brkId);
						brokerImg.setBrkPanImgPath(ownerKYCPersonalDetails.getPanImageUrl());
						broker.setBrkIsPanImg(true);
						int brkImgId = brokerDAO.saveBrkImg(session, brokerImg);
						broker.setBrkImgId(brkImgId);

						brokerDAO.updateBroker(session, broker);

						Address address = new Address();

						address.setAddCity(ownerKYCPersonalDetails.getAddCity());
						address.setAddDist(ownerKYCPersonalDetails.getAddDist());
						address.setAddPin(ownerKYCPersonalDetails.getAddPin());
						address.setAddPost(ownerKYCPersonalDetails.getAddArea());
						address.setAddState(ownerKYCPersonalDetails.getAddState());
						address.setAddRefCode(broker.getBrkCode());
						address.setAddType("Current Address");
						address.setCompleteAdd(ownerKYCPersonalDetails.getPlotNo());
						address.setbCode(employee.getBranchCode());
						address.setUserCode(employee.getEmpCode());

						addressDAO.saveAddress(address, session);

						session.flush();
						session.clear();
						transaction.commit();
						resultMap.put("isSuccess", true);
						resultMap.put("data", broker.getBrkCode());
						resultMap.put("message", "Detail succesfully Saved");
						
					 
					 //Broker
				 }
				 
				 
				 
			 }else if(ownerKYCPersonalDetails.getOwnBrkType().equalsIgnoreCase("Owner")){
					Owner owner = ownerDAO.getOwnByCode(ownerKYCPersonalDetails.getOwnCode());
					
					if(owner !=null) {
						ArrayList<String> phoneNoList = owner.getOwnPhNoList();
					//	if(phoneNoList==null) {
						//	phoneNoList=new ArrayList<>();
						//	phoneNoList.add(ownerKYCPersonalDetails.getPhoneNo());
						//}
						
						
						System.out.println("Phone No.="+ownerKYCPersonalDetails.getAlternateContactNo());
						if (ownerKYCPersonalDetails.getAlternateContactNo() != null)
							phoneNoList.add(ownerKYCPersonalDetails.getAlternateContactNo());

						owner.setOwnPhNoList(phoneNoList);
						owner.setOwnName(ownerKYCPersonalDetails.getOwnName());
						owner.setOwnEmailId(ownerKYCPersonalDetails.getEmailAddress());
						owner.setbCode("1");
						owner.setBranchCode("1");
						owner.setUserCode("1");
						owner.setEmpId(ownerKYCPersonalDetails.getEmpId());
						owner.setOwnPanNo(ownerKYCPersonalDetails.getPanNo());
						owner.setOwnValidPan(false);
						if (owner.getOwnImgId() != null && owner.getOwnImgId()>0) {
							
							OwnerImg ownerImg = (OwnerImg) session.get(OwnerImg.class, owner.getOwnImgId());
							
							ownerImg.setOwnAddressFImgPath(ownerKYCPersonalDetails.getAddressProofUrlFront());
							ownerImg.setOwnAddressBImgPath(ownerKYCPersonalDetails.getAddressProofUrlBack());
							ownerImg.setOwnPanImgPath(ownerKYCPersonalDetails.getPanImageUrl());
							owner.setOwnIsPanImg(true);
							ownerDAO.updateOwnImg(session, ownerImg);
						}else {
							OwnerImg ownerImg=new OwnerImg();
							ownerImg.setOwnAddressFImgPath(ownerKYCPersonalDetails.getAddressProofUrlFront());
							ownerImg.setOwnAddressBImgPath(ownerKYCPersonalDetails.getAddressProofUrlBack());
							ownerImg.setOwnId(owner.getOwnId());
							ownerImg.setOwnPanImgPath(ownerKYCPersonalDetails.getPanImageUrl());
							owner.setOwnIsPanImg(true);
							int ownImgId = ownerDAO.saveOwnImg(session, ownerImg);
							owner.setOwnImgId(ownImgId);
							
						}
						
						
						ownerDAO.updateOwner(session, owner);
						
						
						FAMaster fAMaster=new FAMaster();
						fAMaster.setbCode("1");
						fAMaster.setFaMfaCode(owner.getOwnFaCode());
						fAMaster.setFaMfaName(ownerKYCPersonalDetails.getOwnName());
						fAMaster.setFaMfaType("owner");
						fAMaster.setUserCode("1");
						faMasterDAO.saveFaMaster(fAMaster,session);
						

						Address address = new Address();

						address.setAddCity(ownerKYCPersonalDetails.getAddCity());
						address.setAddDist(ownerKYCPersonalDetails.getAddDist());
						address.setAddPin(ownerKYCPersonalDetails.getAddPin());
						address.setAddPost(ownerKYCPersonalDetails.getAddArea());
						address.setAddState(ownerKYCPersonalDetails.getAddState());
						address.setAddRefCode(owner.getOwnCode());
						address.setAddType("Current Address");
						address.setCompleteAdd(ownerKYCPersonalDetails.getPlotNo());
						address.setbCode("1");
						address.setUserCode("1");

						addressDAO.saveAddress(address, session);

						session.flush();
						session.clear();
						transaction.commit();
						resultMap.put("isSuccess", true);
						resultMap.put("data", owner.getOwnCode());
						resultMap.put("message", "Detail succesfully Saved");
					}else {
						resultMap.put("isSuccess", false);
						resultMap.put("data", null);
						resultMap.put("message", "Please enter valid owner code");
					}
			  }else if(ownerKYCPersonalDetails.getOwnBrkType().equalsIgnoreCase("Broker")) {
				  //Broker
				  

					Broker broker = brokerDAO.getBrkByCode(ownerKYCPersonalDetails.getBrkCode());
					
					if(broker !=null) {
						ArrayList<String> phoneNoList = broker.getBrkPhNoList();
					//	if(phoneNoList==null) {
						//	phoneNoList=new ArrayList<>();
						//	phoneNoList.add(ownerKYCPersonalDetails.getPhoneNo());
						//}
						
						
						System.out.println("Phone No.="+ownerKYCPersonalDetails.getAlternateContactNo());
						if (ownerKYCPersonalDetails.getAlternateContactNo() != null)
							phoneNoList.add(ownerKYCPersonalDetails.getAlternateContactNo());

						broker.setBrkPhNoList(phoneNoList);
						broker.setBrkName(ownerKYCPersonalDetails.getOwnName());
						broker.setBrkEmailId(ownerKYCPersonalDetails.getEmailAddress());
						broker.setBrkFirmName(ownerKYCPersonalDetails.getCompanyName());
						broker.setBrkFirmType(ownerKYCPersonalDetails.getFirmType());
						broker.setbCode("1");
						broker.setBranchCode("1");
						broker.setUserCode("1");
						broker.setBrkPanNo(ownerKYCPersonalDetails.getPanNo());
						broker.setBrkValidPan(false);
						
						if (broker.getBrkImgId() != null && broker.getBrkImgId()>0) {
							
							BrokerImg brokerImg = (BrokerImg) session.get(BrokerImg.class, broker.getBrkImgId());
							
							brokerImg.setBrkAddressFImgPath(ownerKYCPersonalDetails.getAddressProofUrlFront());
							brokerImg.setBrkAddressBImgPath(ownerKYCPersonalDetails.getAddressProofUrlBack());
							brokerImg.setBrkPanImgPath(ownerKYCPersonalDetails.getPanImageUrl());
							broker.setBrkIsPanImg(true);
							brokerDAO.updateBrkImg(session, brokerImg);
						}else {
							BrokerImg brokerImg=new BrokerImg();
							brokerImg.setBrkAddressFImgPath(ownerKYCPersonalDetails.getAddressProofUrlFront());
							brokerImg.setBrkAddressBImgPath(ownerKYCPersonalDetails.getAddressProofUrlBack());
							brokerImg.setBrkId(broker.getBrkId());
							brokerImg.setBrkPanImgPath(ownerKYCPersonalDetails.getPanImageUrl());
							broker.setBrkIsPanImg(true);
							int brkImgId = brokerDAO.saveBrkImg(session, brokerImg);
							broker.setBrkImgId(brkImgId);
							
						}
						
						
						brokerDAO.updateBroker(session, broker);
						
						
						FAMaster fAMaster=new FAMaster();
						fAMaster.setbCode("1");
						fAMaster.setFaMfaCode(broker.getBrkFaCode());
						fAMaster.setFaMfaName(ownerKYCPersonalDetails.getOwnName());
						fAMaster.setFaMfaType("broker");
						fAMaster.setUserCode("1");
						faMasterDAO.saveFaMaster(fAMaster,session);
						

						Address address = new Address();

						address.setAddCity(ownerKYCPersonalDetails.getAddCity());
						address.setAddDist(ownerKYCPersonalDetails.getAddDist());
						address.setAddPin(ownerKYCPersonalDetails.getAddPin());
						address.setAddPost(ownerKYCPersonalDetails.getAddArea());
						address.setAddState(ownerKYCPersonalDetails.getAddState());
						address.setAddRefCode(broker.getBrkCode());
						address.setAddType("Current Address");
						address.setCompleteAdd(ownerKYCPersonalDetails.getPlotNo());
						address.setbCode("1");
						address.setUserCode("1");

						addressDAO.saveAddress(address, session);

						session.flush();
						session.clear();
						transaction.commit();
						resultMap.put("isSuccess", true);
						resultMap.put("data", broker.getBrkCode());
						resultMap.put("message", "Detail succesfully Saved");
					}else {
						resultMap.put("isSuccess", false);
						resultMap.put("data", null);
						resultMap.put("message", "Please enter valid broker code");
					}
			  
				  
				  //Broker
			  }
			 

		} catch (Exception e) {
			transaction.rollback();
			e.printStackTrace();
			resultMap.put("isSuccess", false);
			resultMap.put("message", "Something is going wrong !");
		} finally {
			session.close();
		}

		return resultMap;
	}

	@RequestMapping(value = "/saveOwnerKYCBankAccountsDetail", method = RequestMethod.POST)
	public Object saveOwnerKYCBankAccountsDetail(@RequestBody OwnerKYCBankAccountDetails ownerKYCBankAccountDetails) {
		Map<String, Object> resultMap = new HashMap<String, Object>();

		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try {
			Employee employee=null;
			 if( ownerKYCBankAccountDetails.getEmpId()>0) {
				 employee = (Employee) session.get(Employee.class, ownerKYCBankAccountDetails.getEmpId());
			 }

			if(ownerKYCBankAccountDetails.getOwnBrkCode() != null && !ownerKYCBankAccountDetails.getOwnBrkCode().equalsIgnoreCase("")) {
				
				System.out.println(ownerKYCBankAccountDetails.getOwnBrkCode().substring(0, 3));
				if(ownerKYCBankAccountDetails.getOwnBrkCode().substring(0, 3).equalsIgnoreCase("own")) {
					
					Owner owner = ownerDAO.getOwnByCode(ownerKYCBankAccountDetails.getOwnBrkCode());

					if(owner==null) {
						
							resultMap.put("ownBrkCode", null);
							resultMap.put("isSuccess", false);
							resultMap.put("message", "Owner not found!");
							return resultMap;
						
						
						}else {
						owner.setOwnAccntNo(ownerKYCBankAccountDetails.getAccountNo());
						owner.setOwnAcntHldrName(ownerKYCBankAccountDetails.getAccountHolderName());
						owner.setOwnBnkDet(true);
						owner.setOwnIfsc(ownerKYCBankAccountDetails.getIfscCode());
						owner.setOwnBnkBranch(ownerKYCBankAccountDetails.getBankName());
						/*owner.setOwnPanNo(ownerKYCBankAccountDetails.getPanNo());
						owner.setOwnValidPan(false);
						*/owner.setOwnBnkDetValid(false);
						owner.setOwnBnkDetInValid(false);
						
						
						if (owner.getOwnImgId() != null && owner.getOwnImgId()>0) {
							
							OwnerImg ownerImg = (OwnerImg) session.get(OwnerImg.class, owner.getOwnImgId());
							
							//ownerImg.setOwnPanImgPath(ownerKYCBankAccountDetails.getPanImageUrl());
							ownerImg.setOwnBnkDetImgPath(ownerKYCBankAccountDetails.getAccountProofImageUrl());
							owner.setOwnIsChqImg(true);
							//owner.setOwnIsPanImg(true);
							ownerDAO.updateOwnImg(session, ownerImg);
							
						}else {
							OwnerImg ownerImg=new OwnerImg();
							//ownerImg.setOwnPanImgPath(ownerKYCBankAccountDetails.getPanImageUrl());
							ownerImg.setOwnBnkDetImgPath(ownerKYCBankAccountDetails.getAccountProofImageUrl());
							owner.setOwnIsChqImg(true);
							//owner.setOwnIsPanImg(true);
							int ownImgId = ownerDAO.saveOwnImg(session, ownerImg);
							owner.setOwnImgId(ownImgId);
							
						}
						
						ownerDAO.updateOwner(session, owner);
						resultMap.put("ownBrkCode", owner.getOwnCode());

					}
					
				}else {
					//Broker
					

					
					Broker broker = brokerDAO.getBrkByCode(ownerKYCBankAccountDetails.getOwnBrkCode());

					if(broker==null) {
						
							resultMap.put("ownBrkCode", null);
							resultMap.put("isSuccess", false);
							resultMap.put("message", "Broker not found!");
							return resultMap;
						
						}else {
						broker.setBrkAccntNo(ownerKYCBankAccountDetails.getAccountNo());
						broker.setBrkAcntHldrName(ownerKYCBankAccountDetails.getAccountHolderName());
						broker.setBrkBnkDet(true);
						broker.setBrkIfsc(ownerKYCBankAccountDetails.getIfscCode());
						broker.setBrkBnkBranch(ownerKYCBankAccountDetails.getBankName());
						/*broker.setBrkPanNo(ownerKYCBankAccountDetails.getPanNo());
						broker.setBrkValidPan(false);
						*/broker.setBrkBnkDetValid(false);
						broker.setBrkBnkDetInValid(false);
						
						
						if (broker.getBrkImgId() != null && broker.getBrkImgId()>0) {
							
							BrokerImg brokerImg = (BrokerImg) session.get(BrokerImg.class, broker.getBrkImgId());
							
							//brokerImg.setBrkPanImgPath(ownerKYCBankAccountDetails.getPanImageUrl());
							brokerImg.setBrkBnkDetImgPath(ownerKYCBankAccountDetails.getAccountProofImageUrl());
							//broker.setBrkIsChqImg(true);
							//broker.setBrkIsPanImg(true);
							brokerDAO.updateBrkImg(session, brokerImg);
							
						}else {
							BrokerImg brokerImg=new BrokerImg();
							//brokerImg.setBrkPanImgPath(ownerKYCBankAccountDetails.getPanImageUrl());
							brokerImg.setBrkBnkDetImgPath(ownerKYCBankAccountDetails.getAccountProofImageUrl());
							//broker.setBrkIsChqImg(true);
							//broker.setBrkIsPanImg(true);
							int brkImgId = brokerDAO.saveBrkImg(session, brokerImg);
							broker.setBrkImgId(brkImgId);
							
						}
						
						brokerDAO.updateBroker(session, broker);
						resultMap.put("ownBrkCode", broker.getBrkCode());

					}
					
				
					
					//Broker
				}
				
				
			}
			
			
			session.flush();
			session.clear();
			transaction.commit();
			resultMap.put("isSuccess", true);
			resultMap.put("message", "Detail successfully Saved");
		} catch (Exception e) {
			transaction.rollback();
			resultMap.put("ownBrkCode", null);
			resultMap.put("isSuccess", false);
			resultMap.put("message", "Please retry!");
		} finally {
			session.close();
		}

		return resultMap;
	}

	@RequestMapping(value = "/getStnByPinFrApp", method = RequestMethod.POST)
	public @ResponseBody Object getStnByPinFrApp(@RequestBody Map<String, String> pinCode) {
		Map<String, Object> resultMap = new HashMap<String, Object>();

		List<AllStation> list = stationDAO.getAStnByPin(pinCode.get("pinCode"));
		if (list.isEmpty()) {
			resultMap.put("message", "No record found!");
			resultMap.put("isSuccess", false);
		} else {
			
			String stName=null;
			List<State> stateList = stateDAO.getStateNameByCode(list.get(0).getStateCode().toString());
			if (!stateList.isEmpty()) {
				stName=stateList.get(0).getStateName();
			}
			
			
			
			List<Map<String, String>> areaList=new ArrayList<>();
			
			for(AllStation alStn:list) {
				Map<String, String> map=new HashMap<>();
				map.put("pinCode", alStn.getPinCode().toString());
				map.put("area", alStn.getStationName());
				map.put("district", alStn.getDistrict());
				map.put("city", alStn.getCity());
				map.put("state", stName);
				
				areaList.add(map);
				
			}
			

			
			resultMap.put("data", areaList);
			resultMap.put("isSuccess", true);
		}
		return resultMap;
	}

	@RequestMapping(value = "/employeeLogin", method = RequestMethod.POST)
	public Object employeeLogin(@RequestBody Employee employee) {

		Map<String, Object> map = new HashMap<String, Object>();
		
		try {
			String userName = employee.getEmpFaCode();
			String pass = employee.getEmpPassword();

			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(pass.getBytes());
			byte[] digest = md.digest();
			StringBuffer sb = new StringBuffer();
			for (byte b : digest) {
				sb.append(String.format("%02x", b & 0xff));
			}
			System.out.println("original:" + pass);
			System.out.println("digested(hex):" + sb.toString());
			String password = sb.toString();

			System.out.println("inside loginUser ----->" + userName);

			List<Employee> empList = employeeDAO.getEmployeeByUN(userName, password);

			if (empList.isEmpty()) {
				map.put("empId", null);
				map.put("isSuccess", false);
				map.put("message", "Please enter valid credentials!");
				return map;
			} else {

				Employee emp = empList.get(0);
				System.out.println(empList.size() + "empName=" + emp.getEmpName());
				map.put("empName", emp.getEmpName());
				map.put("empId", emp.getEmpId());
				map.put("isSuccess", true);

				return map;
			}
		}catch(Exception e) {
			map.put("empId", null);
			map.put("isSuccess", false);
			map.put("message", "Please enter valid credentials!");
			return map;
		}
		
		

	}

	/*@RequestMapping(value = "/uploadVendorKYCDocuments", method = RequestMethod.POST)
	public @ResponseBody Object uploadVendorKYCDocuments(@RequestParam("file") MultipartFile file,
			@RequestParam("ownCode") String ownCode, @RequestParam("doc_name") String docName,
			@RequestParam("image_side") String imageSide) {
		System.out.println("file name = " + file.getContentType());
		System.out.println("original file name = " + file.getOriginalFilename());
		String fileType = file.getOriginalFilename().split("\\.")[file.getOriginalFilename().split("\\.").length - 1];
		System.out.println("file type = " + fileType);
		// String filType=fileType[fileType.length-1];
		Map<String, Object> resultMap = new HashMap<>();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try {

			Owner own = ownerDAO.getOwnByCode(ownCode);
			if (own != null) {

				if (docName.equalsIgnoreCase("PAN")) {

					byte[] fileInBytes = file.getBytes();

					Path path = Paths.get(supplierPanImgPath);
					if (!Files.exists(path))
						Files.createDirectories(path);// make new directory if not exist

					

					if (own.getOwnImgId() != null) {
						
						OwnerImg ownImg = (OwnerImg) session.get(OwnerImg.class, own.getOwnImgId());
						
						if (ownImg.getOwnPanImgPath() != null) {
							File panFile = new File(supplierPanImgPath + "/own" + own.getOwnId() + "." + fileType);
							if (panFile.exists())
								panFile.delete();

							FileOutputStream out = new FileOutputStream(panFile);
							out.write(fileInBytes);
							out.close();
							 ownImg.setOwnBnkDetImgPath(supplierPanImgPath + "/own" + own.getOwnId() + "." + fileType);
							// ownImg.setUserCode(crnUser.getUserCode());
							ownerDAO.updateOwnImg(session, ownImg);
						} else {
							File panFile = new File(supplierPanImgPath + "/own" + own.getOwnId() + "." + fileType);
							if (panFile.exists())
								panFile.delete();

							FileOutputStream out = new FileOutputStream(panFile);
							out.write(fileInBytes);
							out.close();

							ownImg.setOwnPanImgPath(supplierPanImgPath + "/own" + own.getOwnId() + "." + fileType);
							// ownImg.setUserCode(crnUser.getUserCode());
							ownerDAO.updateOwnImg(session, ownImg);
						}

					} else {

						OwnerImg ownImgN = new OwnerImg();
						File panFile = new File(supplierPanImgPath + "/own" + own.getOwnId() + "." + fileType);
						if (panFile.exists())
							panFile.delete();

						FileOutputStream out = new FileOutputStream(panFile);
						out.write(fileInBytes);
						out.close();
						 ownImgN.setbCode(own.getbCode());
						 ownImgN.setOwnId(own.getOwnId());
						// ownImgN.setUserCode(crnUser.getUserCode());

						ownImgN.setOwnPanImgPath(supplierPanImgPath + "/own" + own.getOwnId() + "." + fileType);
						int ownImgId = ownerDAO.saveOwnImg(session, ownImgN);

						own.setOwnImgId(ownImgId);

					}

					own.setOwnIsPanImg(true);
					own.setOwnValidPan(false);

					ownerDAO.updateOwner(session, own);

				} else if (docName.equalsIgnoreCase("DEC")) {

					byte[] fileInBytes = file.getBytes();

					Path path = Paths.get(supplierDecImgPath);
					if (!Files.exists(path))
						Files.createDirectories(path);// make new directory if not exist

					
					if (own.getOwnImgId() != null) {
						OwnerImg ownImg = (OwnerImg) session.get(OwnerImg.class, own.getOwnImgId());
						
						if (ownImg.getOwnDecImgPath() != null) {
							System.out.println("image path=" + ownImg.getOwnDecImgPath());
							File decFile = new File(supplierDecImgPath + "/own" + own.getOwnId() + "." + fileType);
							System.out.println("is file="+decFile.exists());
							if (decFile.exists())
								decFile.delete();

							FileOutputStream out = new FileOutputStream(decFile);
							out.write(fileInBytes);
							out.close();
							 ownImg.setOwnBnkDetImgPath(supplierDecImgPath + "/own" + own.getOwnId() + "." + fileType);
							// ownImg.setUserCode(crnUser.getUserCode());
							ownerDAO.updateOwnImg(session, ownImg);
						} else {
							File decFile = new File(supplierDecImgPath + "/own" + own.getOwnId() + "." + fileType);
							if (decFile.exists())
								decFile.delete();

							FileOutputStream out = new FileOutputStream(decFile);
							out.write(fileInBytes);
							out.close();

							ownImg.setOwnDecImgPath(supplierDecImgPath + "/own" + own.getOwnId() + "." + fileType);
							// ownImg.setUserCode(crnUser.getUserCode());
							ownerDAO.updateOwnImg(session, ownImg);
						}

					} else {

						OwnerImg ownImgN = new OwnerImg();
						File decFile = new File(supplierDecImgPath + "/own" + own.getOwnId() + "." + fileType);
						if (decFile.exists())
							decFile.delete();

						FileOutputStream out = new FileOutputStream(decFile);
						out.write(fileInBytes);
						out.close();
						// ownImgN.setbCode(crnUser.getUserBranchCode());
						 ownImgN.setOwnId(own.getOwnId());
						// ownImgN.setUserCode(crnUser.getUserCode());

						ownImgN.setOwnDecImgPath(supplierDecImgPath + "/own" + own.getOwnId() + "." + fileType);
						int ownImgId = ownerDAO.saveOwnImg(session, ownImgN);

						own.setOwnImgId(ownImgId);

					}

					own.setOwnIsDecImg(true);
					own.setOwnPanIntRt(0);

					ownerDAO.updateOwner(session, own);

				} else if (docName.equalsIgnoreCase("ACCOUNTPROOF")) {

					byte[] fileInBytes = file.getBytes();

					Path path = Paths.get(supplierBankDetImgPath);
					if (!Files.exists(path))
						Files.createDirectories(path);// make new directory if not exist

					
					if (own.getOwnImgId() != null) {
						OwnerImg ownImg = (OwnerImg) session.get(OwnerImg.class, own.getOwnImgId());

						
						if (ownImg.getOwnBnkDetImgPath() != null) {
							File chqFile = new File(supplierBankDetImgPath + "/own" + own.getOwnId() + "." + fileType);
							
							if (chqFile.exists())
								chqFile.delete();

							FileOutputStream out = new FileOutputStream(chqFile);
							out.write(fileInBytes);
							out.close();
							 ownImg.setOwnBnkDetImgPath(supplierBankDetImgPath + "/own" + own.getOwnId() + "." + fileType);
							// ownImg.setUserCode(crnUser.getUserCode());
							ownerDAO.updateOwnImg(session, ownImg);
						} else {
							File chqFile = new File(supplierBankDetImgPath + "/own" + own.getOwnId() + "." + fileType);
							if (chqFile.exists())
								chqFile.delete();

							FileOutputStream out = new FileOutputStream(chqFile);
							out.write(fileInBytes);
							out.close();

							ownImg.setOwnBnkDetImgPath(
									supplierBankDetImgPath + "/own" + own.getOwnId() + "." + fileType);
							// ownImg.setUserCode(crnUser.getUserCode());
							ownerDAO.updateOwnImg(session, ownImg);
						}

					} else {

						OwnerImg ownImgN = new OwnerImg();
						File chqFile = new File(supplierBankDetImgPath + "/own" + own.getOwnId() + "." + fileType);
						if (chqFile.exists())
							chqFile.delete();

						FileOutputStream out = new FileOutputStream(chqFile);
						out.write(fileInBytes);
						out.close();
						// ownImgN.setbCode(crnUser.getUserBranchCode());
						 ownImgN.setOwnId(own.getOwnId());
						// ownImgN.setUserCode(crnUser.getUserCode());

						ownImgN.setOwnBnkDetImgPath(supplierBankDetImgPath + "/own" + own.getOwnId() + "." + fileType);
						int ownImgId = ownerDAO.saveOwnImg(session, ownImgN);

						own.setOwnImgId(ownImgId);

					}

					own.setOwnIsChqImg(true);
					own.setOwnBnkDetValid(false);
					own.setOwnBnkDetInValid(false);

					ownerDAO.updateOwner(session, own);

				} else if (docName.equalsIgnoreCase("ADDRESSPROOF")) {
					if (imageSide.equalsIgnoreCase("FRONT")) {

						byte[] fileInBytes = file.getBytes();

						Path path = Paths.get(supplierAdrsPrfImgPath);
						if (!Files.exists(path))
							Files.createDirectories(path);// make new directory if not exist

						
						if (own.getOwnImgId() != null) {
							OwnerImg ownImg = (OwnerImg) session.get(OwnerImg.class, own.getOwnImgId());

							
							if (ownImg.getOwnAddressFImgPath() != null) {
								File addressFile = new File(supplierAdrsPrfImgPath + "/Fown" + own.getOwnId() + "." + fileType);
								if (addressFile.exists())
									addressFile.delete();

								FileOutputStream out = new FileOutputStream(addressFile);
								out.write(fileInBytes);
								out.close();
								 ownImg.setOwnBnkDetImgPath(supplierAdrsPrfImgPath + "/Fown" + own.getOwnId() + "." + fileType);
								// ownImg.setUserCode(crnUser.getUserCode());
								ownerDAO.updateOwnImg(session, ownImg);
							} else {
								File addressFile = new File(
										supplierAdrsPrfImgPath + "/Fown" + own.getOwnId() + "." + fileType);
								if (addressFile.exists())
									addressFile.delete();

								FileOutputStream out = new FileOutputStream(addressFile);
								out.write(fileInBytes);
								out.close();

								ownImg.setOwnAddressFImgPath(
										supplierAdrsPrfImgPath + "/Fown" + own.getOwnId() + "." + fileType);
								// ownImg.setUserCode(crnUser.getUserCode());
								ownerDAO.updateOwnImg(session, ownImg);
							}

						} else {

							OwnerImg ownImgN = new OwnerImg();
							File addressFile = new File(supplierAdrsPrfImgPath + "/Fown" + own.getOwnId() + "." + fileType);
							if (addressFile.exists())
								addressFile.delete();

							FileOutputStream out = new FileOutputStream(addressFile);
							out.write(fileInBytes);
							out.close();
							// ownImgN.setbCode(crnUser.getUserBranchCode());
							 ownImgN.setOwnId(own.getOwnId());
							// ownImgN.setUserCode(crnUser.getUserCode());

							ownImgN.setOwnAddressFImgPath(
									supplierAdrsPrfImgPath + "/Fown" + own.getOwnId() + "." + fileType);
							int ownImgId = ownerDAO.saveOwnImg(session, ownImgN);

							own.setOwnImgId(ownImgId);

						}


						ownerDAO.updateOwner(session, own);

					} else {

						byte[] fileInBytes = file.getBytes();

						Path path = Paths.get(supplierAdrsPrfImgPath);
						if (!Files.exists(path))
							Files.createDirectories(path);// make new directory if not exist

						
						if (own.getOwnImgId() != null) {
							OwnerImg ownImg = (OwnerImg) session.get(OwnerImg.class, own.getOwnImgId());

							
							if (ownImg.getOwnAddressBImgPath() != null) {
								File addressFile = new File(supplierAdrsPrfImgPath + "/Bown" + own.getOwnId() + "." + fileType);
								if (addressFile.exists())
									addressFile.delete();

								FileOutputStream out = new FileOutputStream(addressFile);
								out.write(fileInBytes);
								out.close();
								 ownImg.setOwnBnkDetImgPath(supplierAdrsPrfImgPath + "/Bown" + own.getOwnId() + "." + fileType);
								// ownImg.setUserCode(crnUser.getUserCode());
								ownerDAO.updateOwnImg(session, ownImg);
							} else {
								File addressFile = new File(
										supplierAdrsPrfImgPath + "/Bown" + own.getOwnId() + "." + fileType);
								if (addressFile.exists())
									addressFile.delete();

								FileOutputStream out = new FileOutputStream(addressFile);
								out.write(fileInBytes);
								out.close();

								ownImg.setOwnAddressBImgPath(
										supplierAdrsPrfImgPath + "/Bown" + own.getOwnId() + "." + fileType);
								// ownImg.setUserCode(crnUser.getUserCode());
								ownerDAO.updateOwnImg(session, ownImg);
							}

						} else {

							OwnerImg ownImgN = new OwnerImg();
							File addressFile = new File(supplierAdrsPrfImgPath + "/Bown" + own.getOwnId() + "." + fileType);
							if (addressFile.exists())
								addressFile.delete();

							FileOutputStream out = new FileOutputStream(addressFile);
							out.write(fileInBytes);
							out.close();
							// ownImgN.setbCode(crnUser.getUserBranchCode());
							 ownImgN.setOwnId(own.getOwnId());
							// ownImgN.setUserCode(crnUser.getUserCode());

							ownImgN.setOwnAddressBImgPath(
									supplierAdrsPrfImgPath + "/Bown" + own.getOwnId() + "." + fileType);
							int ownImgId = ownerDAO.saveOwnImg(session, ownImgN);

							own.setOwnImgId(ownImgId);

						}


						ownerDAO.updateOwner(session, own);

					

					}

				}

				session.flush();
				session.clear();
				transaction.commit();
				resultMap.put("isSuccess", true);
				resultMap.put("message", "Image successfully uploaded");

			} else {
				resultMap.put("isSuccess", false);
				resultMap.put("message", "Vendor not found");
			}

		} catch (Exception e) {
			transaction.rollback();
			e.printStackTrace();
			resultMap.put("isSuccess", false);
			resultMap.put("message", "Problem in uploading file on server");
		} finally {
			session.close();
		}
		return resultMap;
	}
	*/
	
	
	@RequestMapping(value = "/uploadVendorKYCDocuments", method = RequestMethod.POST)
	public @ResponseBody Object uploadVendorKYCDocuments(@RequestParam("file") MultipartFile file) {
		System.out.println("file name = " + file.getContentType());
		System.out.println("original file name = " + file.getOriginalFilename());
		String fileType = file.getOriginalFilename().split("\\.")[file.getOriginalFilename().split("\\.").length - 1];
		System.out.println("file type = " + fileType);
		// String filType=fileType[fileType.length-1];
		Map<String, Object> resultMap = new HashMap<>();
		try {

			

					byte[] fileInBytes = file.getBytes();

					Path path = Paths.get(ownerKYCDocs);
					if (!Files.exists(path))
						Files.createDirectories(path);// make new directory if not exist

							FileOutputStream out = new FileOutputStream(new File(ownerKYCDocs + "/"+file.getOriginalFilename()));
							out.write(fileInBytes);
							out.close();
				
							resultMap.put("imagePath", ownerKYCDocs + "/"+file.getOriginalFilename());
							resultMap.put("isSuccess", true);
							resultMap.put("message", "Image successfully uploaded");


		} catch (Exception e) {
			e.printStackTrace();
			resultMap.put("isSuccess", false);
			resultMap.put("imagePath",null);
			resultMap.put("message", "Problem in uploading file on server");
		} 
		return resultMap;
	}
	
	
	@RequestMapping(value = "/verifyOTPForOwner", method = RequestMethod.POST)
	public @ResponseBody Object verifyOTPForOwner(@RequestBody Map<String, Object> initParam){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		
		String phoneNo=initParam.get("phone_no").toString();
		String otpNo=initParam.get("otp").toString();
		System.out.println("otp="+otpNo+" Phone No.="+phoneNo);
		if(otpNo!=null && !"".equalsIgnoreCase(otpNo.trim())) {
			try {
			List<AppLoginOTP>userList = ownerDAO.verifyOTPForOwner(phoneNo, otpNo);
			
			if(!userList.isEmpty()) {
				
				
				if(initParam.get("userType").toString().equalsIgnoreCase("Owner")) {
					List<Owner> ownList = ownerDAO.getPhoneNo();

					for (Owner own : ownList) {
						if (own.getOwnPhNoList() != null && !own.getOwnPhNoList().isEmpty())
							for (String phone : own.getOwnPhNoList())
								if (phoneNo.equals(phone)) {

									System.out.println("Yes= " + phone + " " + own.getOwnCode());
									resultMap.put("isSuccess", true);
									resultMap.put("message", "Phone No. already exist");
									resultMap.put("ownCode", own.getOwnCode());
									return resultMap;
								}
					}
					
					
					ArrayList<String> phoneNoList = new ArrayList<>();

					phoneNoList.add(phoneNo);
					
					Owner owner=new Owner();
					
					owner.setOwnPhNoList(phoneNoList);
					int ownId = ownerDAO.saveOwner(session, owner);

					owner.setOwnCode("own" + ownId);
					int ownerFaCode = 800000 + ownId;
					owner.setOwnFaCode("0" + ownerFaCode);
					owner.setUserCode("1");

					FAParticular fAParticular = (FAParticular) session.get(FAParticular.class, 8);

					owner.setfAParticular(fAParticular);
					owner.setView(false);

					ownerDAO.updateOwner(session, owner);
					
					session.flush();
					session.clear();
					transaction.commit();
					resultMap.put("isSuccess", true);
					resultMap.put("message", "Owner Code successfully generated");
					resultMap.put("ownCode", owner.getOwnCode());
				
				}else if(initParam.get("userType").toString().equalsIgnoreCase("Broker")){
					//Broker 
					

					List<Broker> brkList = brokerDAO.getPhoneNo();

					for (Broker brk : brkList) {
						if (brk.getBrkPhNoList() != null && !brk.getBrkPhNoList().isEmpty())
							for (String phone : brk.getBrkPhNoList())
								if (phoneNo.equals(phone)) {

									System.out.println("Yes= " + phone + " " + brk.getBrkCode());
									resultMap.put("isSuccess", true);
									resultMap.put("message", "Phone No. already exist");
									resultMap.put("brkCode", brk.getBrkCode());
									return resultMap;
								}
					}
					
					
					ArrayList<String> phoneNoList = new ArrayList<>();

					phoneNoList.add(phoneNo);
					
					Broker broker=new Broker();
					
					broker.setBrkPhNoList(phoneNoList);
					int brkId = brokerDAO.saveBroker( broker,session);

					broker.setBrkCode("brk" + brkId);
					int brokerFaCode = 900000 + brkId;
					broker.setBrkFaCode("0" + brokerFaCode);
					broker.setUserCode("1");

					FAParticular fAParticular = (FAParticular) session.get(FAParticular.class, 9);

					broker.setfAParticular(fAParticular);
					broker.setView(false);

					brokerDAO.updateBroker(session, broker);
					
					session.flush();
					session.clear();
					transaction.commit();
					resultMap.put("isSuccess", true);
					resultMap.put("message", "Broker Code successfully generated");
					resultMap.put("brkCode", broker.getBrkCode());
				
				//Broker
					
				}else {
					resultMap.put("isSuccess", false);
					resultMap.put("message", "Please select valid user type");
					resultMap.put("ownCode", null);
					resultMap.put("brkCode", null);
				}
				
			}else {
				resultMap.put("isSuccess", false);
				resultMap.put("message", "OTP doesn't match");
				resultMap.put("ownCode", null);
				resultMap.put("brkCode", null);
			}
		}catch(Exception e) {
			resultMap.put("isSuccess", false);
			resultMap.put("message", "Something going wrong");
			resultMap.put("ownCode", null);
			resultMap.put("brkCode", null);
			e.printStackTrace();
			transaction.rollback();
		}finally {
			session.close();
		}
			
		}else {
			resultMap.put("isSuccess", false);
			resultMap.put("message", "Enter OTP");
		}
		
		return resultMap;
	}
	
	
	@RequestMapping(value = "/saveOwnerKYCTruckOwnershipDetail", method = RequestMethod.POST)
	public Object saveOwnerKYCTruckOwnershipDetail(@RequestBody Map<String, Object> clientMap) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		
		Owner owner = ownerDAO.getOwnByCode(clientMap.get("ownCode").toString());

		if(owner !=null) {
			Session session=sessionFactory.openSession();
			Transaction transaction=session.beginTransaction();
			try {
				
				if(clientMap.get("deductTds").toString().equalsIgnoreCase("Yes")) {
					if (owner.getOwnPanNo().charAt(3) == 'P' ||	owner.getOwnPanNo().charAt(3) == 'H' ||	owner.getOwnPanNo().charAt(3) == 'J') {
						owner.setOwnPanIntRt(1);
					}else {
						owner.setOwnPanIntRt(2);
					}
				}else {
					owner.setOwnPanIntRt(0);
				}
				
				ownerDAO.updateOwner(session, owner);
				
				session.flush();
				session.clear();
				transaction.commit();
				
				resultMap.put("isSuccess", true);
				resultMap.put("message", "successfully saved");
				
			}catch(Exception e) {
				e.printStackTrace();
				transaction.rollback();
				resultMap.put("isSuccess", false);
				resultMap.put("message", "Something going wrong");
			}finally {
				if(session.isOpen())
					session.close();
			}
			
		}else {
			resultMap.put("isSuccess", false);
			resultMap.put("message", "Owner not found");
		
		}
		
		
		return resultMap;
	}
	
	
	/*@RequestMapping(value = "/getStateAndVehicleTypeFrApp", method = RequestMethod.GET)
	public @ResponseBody Object getStateAndVehicleTypeFrApp() {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		
		List<String> stateList=stateDAO.getStateName();
		List<String> vtList=vehicleTypeDAO.getVehTypeName();
		List<State>stateList=stateDAO.getStateData();
		List<VehicleType> vtList=vehicleTypeDAO.getVehicleType();
		if(!stateList.isEmpty() && !vtList.isEmpty()) {
			Map<String, String> stateMap=new HashMap<>();
			Map<String, String> vtMap=new HashMap<>();
			
			for(State state:stateList) {
				stateMap.put(state.getStateCode(), state.getStateName());
			}
			
			for(VehicleType vt:vtList) {
				vtMap.put(vt.getVtCode(), vt.getVtVehicleType());
			}
			
			resultMap.put("stateList", stateList);
			resultMap.put("vehicleTypeList", vtList);
			resultMap.put("stateList", stateMap);
			resultMap.put("vehicleTypeList", vtMap);
			resultMap.put("isSuccess", true);
		}else {
			resultMap.put("isSuccess", false);
			resultMap.put("message", "There is some problem to fetching details.");
		}
		
		return resultMap;
	}*/

	
	
	@RequestMapping(value = "/getStateAndVehicleTypeFrApp", method = RequestMethod.GET)
	public @ResponseBody Object getStateAndVehicleTypeFrApp() {
		Map<String, Object> resultMap = new HashMap<String, Object>();

		/*
		 * List<String> stateList=stateDAO.getStateName(); List<String>
		 * vtList=vehicleTypeDAO.getVehTypeName();
		 */
		try {
			State pstate = new State();
			List<State> stateList = stateDAO.getStateData();
			List<VehicleType> vtList = vehicleTypeDAO.getVehicleType();
			//List<State> listOfState = new ArrayList<>();
			/*for (State state : stateList) {
				pstate.setStateName(state.getStateName());
				listOfState.add(pstate);
				System.out.println("state "+pstate.getStateName());
			}*/
			System.out.println("list of sttaes   " + stateList);
			if (!stateList.isEmpty() && !vtList.isEmpty()) {
				Map<String, String> stateMap = new HashMap<>();
				Map<String, String> vtMap = new HashMap<>();
				
				JSONArray stateArray = new JSONArray();
				for (State state : stateList) {
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("name", state.getStateName());
					stateArray.put(jsonObject);
					System.out.println("json object   " + jsonObject.get("name"));
				}
				
				JSONArray vtArray = new JSONArray();
				for (VehicleType vt : vtList) {
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("name", vt.getVtVehicleType());
					vtArray.put(jsonObject);
					System.out.println("json object   " + jsonObject.get("name"));
				}
				resultMap.put("stateList", stateArray.toString());

				System.out.println("result map   " + resultMap);
				resultMap.put("vehicleTypeList", vtArray.toString());
				resultMap.put("isSuccess", true);
			} else {
				resultMap.put("isSuccess", false);
				resultMap.put("message", "There is some problem to fetching details.");
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return resultMap;
	}
	
	
	
	@RequestMapping(value = "/saveOwnBrkKYCBusinessDetail", method = RequestMethod.POST)
	public Object saveOwnBrkKYCBusinessDetail(@RequestBody OwnBrkKycBusinessDetails brkKycBusinessDetails) {
		Map<String, Object> resultMap = new HashMap<String, Object>();

		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try {
			
			List<com.mylogistics.model.OwnBrkKycBusinessDetails> ownBrkList=ownBrkKycBusinessDetailsDAO.getOwnBrkBusinessDetailByCode(brkKycBusinessDetails.getOwnBrkCode());
			
			if(!ownBrkList.isEmpty()) {
				
				for(com.mylogistics.model.OwnBrkKycBusinessDetails obk:ownBrkList) {
					if(obk.getRoutes()!=null)
					if(brkKycBusinessDetails.getRouteList().size()>0) {
						
						for(int i=0;i<brkKycBusinessDetails.getRouteList().size();i++) {
							
							if(brkKycBusinessDetails.getRouteList().get(i).equalsIgnoreCase(obk.getRoutes())) {
								brkKycBusinessDetails.getRouteList().remove(i);
								i--;
							}
							
						}
						
					}
					
					if(obk.getVehTypes()!=null)
					if(brkKycBusinessDetails.getVehilceTypeList().size()>0) {
						
						for(int i=0;i<brkKycBusinessDetails.getVehilceTypeList().size();i++) {
							
							if(brkKycBusinessDetails.getVehilceTypeList().get(i).equalsIgnoreCase(obk.getVehTypes())) {
								brkKycBusinessDetails.getVehilceTypeList().remove(i);
								i--;
							}
							
						}
						
					}
					
				}
				
			}
			
			
			if(brkKycBusinessDetails.getRouteList().size()>=brkKycBusinessDetails.getVehilceTypeList().size()) {
				for(int i=0;i<brkKycBusinessDetails.getRouteList().size();i++) {
					com.mylogistics.model.OwnBrkKycBusinessDetails ownBrk=new com.mylogistics.model.OwnBrkKycBusinessDetails();
					ownBrk.setOwnBrkCode(brkKycBusinessDetails.getOwnBrkCode());
					ownBrk.setRoutes(brkKycBusinessDetails.getRouteList().get(i));
				
					if(i<brkKycBusinessDetails.getVehilceTypeList().size()) {
						ownBrk.setVehTypes(brkKycBusinessDetails.getVehilceTypeList().get(i));
					}
					
					ownBrkKycBusinessDetailsDAO.saveOwnBrkBusinessDetail(session, ownBrk);
				}
				
				
			}else {
				
				for(int i=0;i<brkKycBusinessDetails.getVehilceTypeList().size();i++) {
					com.mylogistics.model.OwnBrkKycBusinessDetails ownBrk=new com.mylogistics.model.OwnBrkKycBusinessDetails();
					ownBrk.setOwnBrkCode(brkKycBusinessDetails.getOwnBrkCode());
					ownBrk.setVehTypes(brkKycBusinessDetails.getVehilceTypeList().get(i));
					
					if(i<brkKycBusinessDetails.getRouteList().size()) {
						ownBrk.setRoutes(brkKycBusinessDetails.getRouteList().get(i));
					}
					
					ownBrkKycBusinessDetailsDAO.saveOwnBrkBusinessDetail(session, ownBrk);
				}
				
				
			}
			
			session.flush();
			session.clear();
			transaction.commit();
			resultMap.put("isSuccess", true);
			resultMap.put("message", "Details successfully Saved!");
		} catch (Exception e) {
			transaction.rollback();
			resultMap.put("isSuccess", false);
			resultMap.put("message", "Please retry!");
		} finally {
			session.close();
		}

		return resultMap;
	}

	

}
