package com.mylogistics.DAO;

import java.sql.Date;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.mylogistics.model.ContToStn;

@Repository
public interface ContToStnDAO {

	//Sanjana: saves contToStn to the database
	public int saveContToStn(ContToStn contToStn);

	public List<ContToStn> getContToStnRateW(String contCode , String stnCode , String vehicleType);

	public List<ContToStn> getContToStnRateQ(String contCode , String stnCode , String productType ,String vehicleType , Date cnmtDt);
	
	//Kamal: get Rate according weight By matching Date
	public List<ContToStn> getContToStnRateCnmtW(String contCode , String stnCode , String vehicleType , Date cnmtDt);

	//Kamal: get rate according quantity By matching Date
	public List<ContToStn> getContToStnRateCnmtQ(String contCode , String stnCode , String productType , Date cnmtDt);


	//Sanjana:get cont to stations list with matching code
	public List<ContToStn> getContToStn(String code);

	//Sanjana: delete CTS
	public ContToStn deleteCTS(String code);

	//Shikha:get cont to stations list with matching codes 
	public List<ContToStn> getContForView(String contCode , String stnCode);
	
	//get the list of unregistered station in db
	public List<String> getUnregStnList();
	
	public int chgStnRate(Date renDt , List<Map<String, Object>> chgRateList);
	
	public int chgDlyStnRate(Date renDt , List<Map<String, Object>> chgRateList, String contCode);
	
	public double getRateFrBbl(Map<String, Object> rateService);
	
	public int updateToWt(ContToStn contToStn);
	
	public List<ContToStn> getContToStn(String code,Date date);

	public List<ContToStn> getContToStnFrIndent(String code, String toStn, String vehicleType, Date date);
	
	public ContToStn getContToStnById(int ctsId);
	
}
