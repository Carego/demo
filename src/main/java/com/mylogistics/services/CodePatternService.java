package com.mylogistics.services;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Random;
import java.util.TimeZone;

import com.mylogistics.model.Broker;
import com.mylogistics.model.Customer;
import com.mylogistics.model.CustomerRepresentative;
import com.mylogistics.model.DailyContract;
import com.mylogistics.model.Employee;
import com.mylogistics.model.Owner;
import com.mylogistics.model.RegularContract;
import com.mylogistics.model.State;
import com.mylogistics.model.Station;
import com.mylogistics.model.StationarySupplier;


public class CodePatternService {
	
	private static String custCode = null;
	private static String custRepCode = null;
	
	public static String empCodeGen(Employee employee,String lastFiveChar){
		
		System.out.println("Entered into empCodeGen function of CodePatternService---"+employee.getEmpName());
		String empName = employee.getEmpName();
		String subempName=empName.substring(0,3);
		Date dateEmpDOAppointment=employee.getEmpDOAppointment();
	    SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        String formattedDate = formatter.format(dateEmpDOAppointment);
        String subempDOAppointment=formattedDate.substring(0,4);
      // 	String subbranchName=branchName.substring(0,3);
		
       	String empCode = subempName+subempDOAppointment+lastFiveChar;
		return empCode;
	}
	
	public static String custCodeGen(Customer customer,String lastFiveChar,String brTempCode){
		
		String s=customer.getCustName();
		String custname=s.substring(0,3);
		String b=customer.getBranchCode();
		
		String branchname=brTempCode.substring(3,5);
		custCode= custname+branchname+lastFiveChar;
		return custCode;
	}
	
   public static String custRepCodeGen(CustomerRepresentative custrep,String lastFiveChar){
	    
	    String s=custrep.getCrName();
		String custname=s.substring(0,3);
		
		Random rnd = new Random();
        int n = 1000 + rnd.nextInt(8900);
        
        custRepCode= custname+n+lastFiveChar;
		
		return custRepCode;
	}
	
   public static String dlyContCodeGen(DailyContract dlyCnt,String last){
		System.out.println("Enter into dlyContCodeGen function-----");
		
		String dlyContBLPMCode = dlyCnt.getDlyContBLPMCode();
		String custName = dlyContBLPMCode.substring(0, 3);
		String dlyContCode = "dly"+custName+last;
		
		return dlyContCode;
	}
	
	public static String regContCodeGen(RegularContract regCnt,String last){
		System.out.println("Enter into regContCodeGen function-----");
		
		String regContBLPMCode = regCnt.getRegContBLPMCode();
		String custName= regContBLPMCode.substring(0, 3);
		String regContCode = "reg"+custName+last;
		
		return regContCode;
	}
	
	public static String brokerCodeGen(Broker broker,String last){
		System.out.println("Enter into brokerCodeGen function-----");
		
		/*String branchCode=broker.getBranchCode();
		String brCode=branchCode.substring(0,3);*/
		String brokerNAme =broker.getBrkName();
		String brkCode=brokerNAme.substring(0, 3);
		
		String brokerCode = "brk"+brkCode+last;
		
		return brokerCode;
	}
	
	public static String ownerCodeGen(Owner owner,String last){
		System.out.println("Enter into ownerCodeGen function-----");
		
		/*String branchCode=owner.getBranchCode();
		String brCode=branchCode.substring(0,3);*/
		String ownerNAme =owner.getOwnName();
		String owCode=ownerNAme.substring(0, 3);
		
		String ownerCode = "own"+owCode+last;
		
		return ownerCode;
	}
	
	
	public static String branchCodeGen(String branchName,String lastFiveChar){
		
		System.out.println("Entered into branchCodeGen function of CodePatternService---"+branchName);
		
		String subBranchName=branchName.substring(0,3);
		String branchCode= subBranchName+"00"+lastFiveChar;
		return branchCode;
	}
	
	public static String stSupplierCodeGen(StationarySupplier stsupplier,String lastFiveChar){
		
		System.out.println("Entered into stSupplierCodeGen function of CodePatternService---"+stsupplier.getStSupName());
		
		String subSupplierName = stsupplier.getStSupName().substring(0,3);
		/*String subStateName = stsupplier.getStSupState().substring(0,3);
		String stSupplierCode = subSupplierName+subStateName+lastFiveChar;*/
		String stSupplierCode = subSupplierName+lastFiveChar;
		return stSupplierCode;
		
	}
	
	public static String stateCodeGen(State state){
		
		System.out.println("Entered into stateCodeGen function of CodePatternService---"+state.getStateName());
		
		String subStateName = state.getStateName().substring(0,3);
        String subStateSTD = state.getStateSTD();
        String stateCode=subStateName+subStateSTD;
		return stateCode;
	}
	
	public static String stationCodeGen(Station station){
		
		System.out.println("Entered into stationCodeGen function of CodePatternService---"+station.getStnName());
		
		String subStationName = station.getStnName().substring(0,3);
		String subStationPin = station.getStnPin();
		String stationCode=subStationName+subStationPin;
        return stationCode;
	}
	
	public static String generateFaCode(int id, int faPerId){
		
		String faPerIdStr = null;
		if (faPerId<10) {
			faPerIdStr = "0"+String.valueOf(faPerId);
		} else {
			faPerIdStr = String.valueOf(faPerId);
		}
		String bnkIdStr = String.valueOf(id+100000).substring(1);
		
		return faPerIdStr+bnkIdStr;
	}
	
	public static String get7DigitCode(int id){
		
		String str = null;
		if (id>0) {
			str = String.valueOf(10000000+id).substring(1);
		} else {
			str = null;
		}
		
		return str;
	}
	
	public static String get10DigitCode(long id){
		
		String str = null;
		
		
		if (id>0) {
			str = String.valueOf(10000000000L+id).substring(1);
		} else {
			str = null;
		}
		
		return str;
	}
	
	public static Date getSqlDate(String stringDate){
		
		Date sqlDate = null;
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			java.util.Date utilDate = formatter.parse(stringDate);
			sqlDate = new Date(utilDate.getTime());
			System.out.println("sqlDate : "+sqlDate);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sqlDate;
	}
	
	public static String getFormatedDateString(String dateString){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String formateDateString = null;
		try {
			java.util.Date d = sdf.parse(String.valueOf(dateString));
			sdf.applyPattern("d/M/yy");
			formateDateString = sdf.format(d);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return formateDateString;
	}
	
	public static Date getNextDt(Date date){
		System.out.println("enter into getNextDate function = "+date);
		Date reqDt = date;
		Calendar c = Calendar.getInstance();
		c.setTime(reqDt);
		c.add(Calendar.DATE, 1);
		reqDt = new Date(c.getTime().getTime());
		c.setTime(reqDt);
		int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
		if (dayOfWeek == 1) {
			c.add(Calendar.DATE, 1);
			System.out.println("sunday = " + reqDt);
			reqDt = new Date(c.getTime().getTime());
		}else{
			System.out.println("Not sunday = " + reqDt);
		}
		return reqDt;
	}
	
	public static Date getCurrentSqlDateFromUTC(Calendar currentTS){
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(currentTS.getTimeInMillis()+ConstantsValues.TIME_UTC_GMT_DIFFERENCE);
		Date myDate = null;
		try {
			myDate = new Date((simpleDateFormat.parse(simpleDateFormat.format(calendar.getTime()))).getTime());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return myDate;
	}
	
	public static int get4DigitRanNo() {
	    Random r = new Random( System.currentTimeMillis() );
	    return 1000 + r.nextInt(8999);
	}
		
}
