package com.mylogistics.DAO;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.mylogistics.model.OwnerMaster;

@Repository
public interface OwnerMasterDAO {

	public int saveOwnerMaster(Session session, OwnerMaster ownerMaster);
	
}
