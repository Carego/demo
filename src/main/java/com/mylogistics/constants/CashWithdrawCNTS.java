package com.mylogistics.constants;

public class CashWithdrawCNTS {

	public static final String CW_ID = "cwId";
	
	public static final String CW_CHQ_TYPE = "cwChqType";
	
	public static final String CHEQUE_LEAVES = "chequeLeaves";
	
	public static final String CW_DESC = "cwDesc";
	
}
