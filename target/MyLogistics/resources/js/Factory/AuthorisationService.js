'use strict';

angular.module('application').factory('AuthorisationService',
		[ '$http', '$resource', function($http, $resource) {
			var authorised = {};
			
			authorised.reset = function(){
				authorised.currentUser = {};
			}
			
			authorised.setUser = function(user){
				authorised.currentUser = user;
			}
			
			authorised.getUser = function(){
				return authorised.currentUser;
			}

			authorised.setUserRole = function(role){
				authorised.currentUser.userRole = role;
			}
			
			authorised.getUserRole = function(){
				return authorised.currentUser.userRole;
			}
			
			authorised.getUserBrhCode = function(){
				return authorised.currentUser.userBranchCode;
			}
			
			authorised.setUserCode = function(code){
				authorised.currentUser.userCode = code;
			}
			
			authorised.getUserCode = function(){
				return authorised.currentUser.userCode;
			}
			
			authorised.reset();
			
			return authorised;
		} ]);