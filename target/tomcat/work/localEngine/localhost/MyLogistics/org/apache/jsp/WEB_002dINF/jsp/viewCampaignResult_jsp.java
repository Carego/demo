package org.apache.jsp.WEB_002dINF.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class viewCampaignResult_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("\r\n");
      out.write("<!-- <div class=\"row\" >\r\n");
      out.write("<div class=\"col s4\">\r\n");
      out.write("\t       \t\t\t\t<input class=\"validate\" type =\"button\" value=\"View Indents\" ng-click=\"getIndentList()\" >\r\n");
      out.write("\t       \t\t</div>\r\n");
      out.write("\t       \t\t\r\n");
      out.write("\t       \t\t\r\n");
      out.write("</div> -->\r\n");
      out.write("<div class=\"row\" ng-show=\"allIndentShowDB\">\r\n");
      out.write("\t<table class=\"tblrow\">\r\n");
      out.write("\r\n");
      out.write("\t\t<caption class=\"coltag tblrow\">Campaign Result List</caption>\r\n");
      out.write("\r\n");
      out.write("\t\t<tr class=\"rowclr\">\r\n");
      out.write("\t\t\t<th class=\"colclr\">Indent No.</th>\r\n");
      out.write("\t\t\t<th class=\"colclr\">Branch</th>\r\n");
      out.write("\t\t\t<th class=\"colclr\">Customer</th>\r\n");
      out.write("\t\t\t<th class=\"colclr\">From</th>\r\n");
      out.write("\t\t\t<th class=\"colclr\">To</th>\r\n");
      out.write("\t\t\t<th class=\"colclr\">Placement Date and time</th>\r\n");
      out.write("\t\t\t<th class=\"colclr\">Vehicle Type</th>\r\n");
      out.write("\t\t</tr>\r\n");
      out.write("\r\n");
      out.write("\t\t<tr ng-repeat=\"indent in indentList\">\r\n");
      out.write("\t\t\t<td class=\"rowcel\" ng-click=\"getCampaignByIndent(indent,$index)\"><a>{{indent.indentId}}</a></td>\r\n");
      out.write("\t\t\t<td class=\"rowcel\">{{indent.branchName}}</td>\r\n");
      out.write("\t\t\t<td class=\"rowcel\">{{indent.cutomerName}}</td>\r\n");
      out.write("\t\t\t<td class=\"rowcel\">{{indent.fromStn}}</td>\r\n");
      out.write("\t\t\t<td class=\"rowcel\">{{indent.toStn}}</td>\r\n");
      out.write("\t\t\t<td class=\"rowcel\">{{indent.placementDateTime | date : 'M/d/yy\r\n");
      out.write("\t\t\t\th:mm a'}}</td>\r\n");
      out.write("\t\t\t<td class=\"rowcel\">{{indent.vehicleType}}</td>\r\n");
      out.write("\r\n");
      out.write("\t\t</tr>\r\n");
      out.write("\t</table>\r\n");
      out.write("\r\n");
      out.write("</div>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<div class=\"row\" ng-show=\"singleIndentShowDB\">\r\n");
      out.write("\t<table class=\"tblrow\">\r\n");
      out.write("\r\n");
      out.write("\t\t<caption class=\"coltag tblrow\">Campaign Result List</caption>\r\n");
      out.write("\r\n");
      out.write("\t\t<tr class=\"rowclr\">\r\n");
      out.write("\t\t\t<th class=\"colclr\">Indent No.</th>\r\n");
      out.write("\t\t\t<th class=\"colclr\">Branch</th>\r\n");
      out.write("\t\t\t<th class=\"colclr\">Customer</th>\r\n");
      out.write("\t\t\t<th class=\"colclr\">From</th>\r\n");
      out.write("\t\t\t<th class=\"colclr\">To</th>\r\n");
      out.write("\t\t\t<th class=\"colclr\">Placement Date and time</th>\r\n");
      out.write("\t\t\t<th class=\"colclr\">Vehicle Type</th>\r\n");
      out.write("\t\t</tr>\r\n");
      out.write("\r\n");
      out.write("\t\t<tr>\r\n");
      out.write("\t\t\t<td class=\"rowcel\">{{indentList[tempIndex].indentId}}</td>\r\n");
      out.write("\t\t\t<td class=\"rowcel\">{{indentList[tempIndex].branchName}}</td>\r\n");
      out.write("\t\t\t<td class=\"rowcel\">{{indentList[tempIndex].cutomerName}}</td>\r\n");
      out.write("\t\t\t<td class=\"rowcel\">{{indentList[tempIndex].fromStn}}</td>\r\n");
      out.write("\t\t\t<td class=\"rowcel\">{{indentList[tempIndex].toStn}}</td>\r\n");
      out.write("\t\t\t<td class=\"rowcel\">{{indentList[tempIndex].placementDateTime |\r\n");
      out.write("\t\t\t\tdate : 'M/d/yy h:mm a'}}</td>\r\n");
      out.write("\t\t\t<td class=\"rowcel\">{{indentList[tempIndex].vehicleType}}</td>\r\n");
      out.write("\r\n");
      out.write("\t\t</tr>\r\n");
      out.write("\t</table>\r\n");
      out.write("\r\n");
      out.write("</div>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<div class=\"row\" ng-show=\"ownerListDBFlag\">\r\n");
      out.write("\t<table class=\"tblrow\">\r\n");
      out.write("\r\n");
      out.write("\t\t<caption class=\"coltag tblrow\">Available Vendor List</caption>\r\n");
      out.write("\r\n");
      out.write("\t\t<tr class=\"rowclr\">\r\n");
      out.write("\t\t\t<th class=\"colclr\">Company Name</th>\r\n");
      out.write("\t\t\t<th class=\"colclr\">Company Owner Name</th>\r\n");
      out.write("\t\t\t<th class=\"colclr\">Address</th>\r\n");
      out.write("\t\t\t<th class=\"colclr\">Contact No.</th>\r\n");
      out.write("\t\t\t<th class=\"colclr\">Vehicle Available</th>\r\n");
      out.write("\t\t\t<th class=\"colclr\">Rate</th>\r\n");
      out.write("\t\t\t<th class=\"colclr\">Remarks</th>\r\n");
      out.write("\t\t\t<th class=\"colclr\">Add Vehicle and Driver detail</th>\r\n");
      out.write("\t\t</tr>\r\n");
      out.write("\r\n");
      out.write("\t\t<tr ng-repeat=\"ownMstr in ownerMstrList\">\r\n");
      out.write("\t\t\t<td class=\"rowcel\">{{ownMstr.company_Name}}</td>\r\n");
      out.write("\t\t\t<td class=\"rowcel\">{{ownMstr.company_Owner_Name}}</td>\r\n");
      out.write("\t\t\t<td class=\"rowcel\">{{ownMstr.company_Office_Address}}</td>\r\n");
      out.write("\t\t\t<td class=\"rowcel\">{{ownMstr.phone_No}}</td>\r\n");
      out.write("\t\t\t<td class=\"rowcel\">{{campMap[ownMstr.master_Id].noOfVehAvail}}</td>\r\n");
      out.write("\t\t\t<td class=\"rowcel\"><div contenteditable>{{campMap[ownMstr.master_Id].rates}}</div></td>\r\n");
      out.write("\t\t\t<td class=\"rowcel\"><div contenteditable>{{campMap[ownMstr.master_Id].agentRemark}}</div></td>\r\n");
      out.write("\t\t\t<td class=\"rowcel\"\r\n");
      out.write("\t\t\t\tng-click=\"addVehAndDrvrDet(ownMstr,campMap[ownMstr.master_Id])\"><a>Add\r\n");
      out.write("\t\t\t\t\tDetails</a></td>\r\n");
      out.write("\r\n");
      out.write("\t\t</tr>\r\n");
      out.write("\t</table>\r\n");
      out.write("\r\n");
      out.write("\t<div class=\"row\">\r\n");
      out.write("\t\t<div class=\"col s4\">\r\n");
      out.write("\t\t\t<input class=\"validate\" type=\"button\" value=\"Save Placement\"\r\n");
      out.write("\t\t\t\tng-click=\"savePlacement()\">\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t</div>\r\n");
      out.write("\r\n");
      out.write("</div>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<div id=\"addDrvrDetId\" ng-hide=\"addDrvrDetFlag\">\r\n");
      out.write("\t<br> <br>\r\n");
      out.write("\t<div class=\"input-field col s4\">\r\n");
      out.write("\t\t<input type=\"number\" id=\"noOfVehicle\" name=\"noOfVehicle\"\r\n");
      out.write("\t\t\tng-model=\"noOfVehicle\" ng-blur=\"make(noOfVehicle)\" required>\r\n");
      out.write("\t\t<label>No. of Vehicle</label>\r\n");
      out.write("\t</div>\r\n");
      out.write("\t<br> <br>\r\n");
      out.write("\t<div ng-repeat=\"x in vehList\">\r\n");
      out.write("\t\t<div>\r\n");
      out.write("\t\t\t<h4>Add detail of Vehicle {{x+1}}</h4>\r\n");
      out.write("\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t\t<br>\r\n");
      out.write("\t\t<div class=\"input-field col s4\">\r\n");
      out.write("\t\t\t<input type=\"text\" id=\"vehicleNoId\" name=\"vehicleNoName\"\r\n");
      out.write("\t\t\t\tng-model=\"vehicleNo\" required> <label>Vehicle No</label>\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t\t\r\n");
      out.write("\t\t<div class=\"input-field col s4\">\r\n");
      out.write("\t\t\t<input type=\"text\" id=\"driverNameId\" name=\"driverName\"\r\n");
      out.write("\t\t\t\tng-model=\"driverName\" required> <label>Driver Name</label>\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t\t<div class=\"input-field col s4\">\r\n");
      out.write("\t\t\t<input type=\"text\" id=\"driverContactNoId\" name=\"driverContactNoName\"\r\n");
      out.write("\t\t\t\tng-model=\"driverNo\" required> <label>Driver\r\n");
      out.write("\t\t\t\tContact No.</label>\r\n");
      out.write("\t\t</div >\r\n");
      out.write("\t\t\r\n");
      out.write("\t\r\n");
      out.write("\t\r\n");
      out.write("\t<div class=\"input-field col s12 center\">\r\n");
      out.write("\t\t\t<input type=\"button\" value=\"Submit\" ng-click=\"addDrvrDet(vehicleNo,driverName,driverNo)\">\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t</div>\r\n");
      out.write("</div>\r\n");
      out.write("\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
