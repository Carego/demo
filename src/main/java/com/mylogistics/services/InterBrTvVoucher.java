package com.mylogistics.services;

public class InterBrTvVoucher {

	private String brFaCode;
	
	private double amt;
	
	private char dOrC;
	
	private String desc;

	public String getBrFaCode() {
		return brFaCode;
	}

	public void setBrFaCode(String brFaCode) {
		this.brFaCode = brFaCode;
	}

	public double getAmt() {
		return amt;
	}

	public void setAmt(double amt) {
		this.amt = amt;
	}

	public char getdOrC() {
		return dOrC;
	}

	public void setdOrC(char dOrC) {
		this.dOrC = dOrC;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	
	
}
