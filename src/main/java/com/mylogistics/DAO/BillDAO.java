package com.mylogistics.DAO;

import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.mylogistics.model.Bill;
import com.mylogistics.model.BillForwarding;
import com.mylogistics.model.User;
import com.mylogistics.services.BillService;

@Repository
public interface BillDAO {

	
	public List<Bill> getCustBill(int custId);
	
	public List<Bill> getPendingBillFrMr(int custId);
	
	public List<Map<String,Object>> getBlFrPrint(int brhId, String frBlNo, String toBlNo);
	
	public List<Map<String,Object>> getSingleBlPrint(int brhId, String blNo);
	
	public List<Map<String,Object>> getSingleBlPrint(int brhId, String blNo,Session session);
	
	public Map<String,Object> getBillByBlNo(String blNo,int brhId);
	
	public int getBillByBlNo(String blNo);
	
	public int billCancel(String blNo, int brhId);
	
	//generate new bill
	public String getNewBillByBr(int brhId);
	
	public Map<String,Object> getNewBillByBrh(int brhId);
	
	//	Transaction Management
	public Map<String, Object> saveAdvBill(Session session, User user, BillService billService);
	public Map<String, Object> saveBill(Session session, User user, BillService billService);
	
	//manual bill and gst bill 
	public String saveAdvBillM(BillService billService,Session session);
	//reliance bill
	public String saveRelGstBill(BillService billService,Session session);
	//manual bill and gst bill
	public String saveBillM(BillService billService,Session session);
	
	
	public String saveBill(BillService billService);//currently not in use
	public String saveAdvBill(BillService billService);//currently not in use

	public String saveAdvanceBillM(BillService billService, Session session);
	
}
