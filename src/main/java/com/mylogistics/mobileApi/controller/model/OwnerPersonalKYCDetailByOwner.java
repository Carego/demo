package com.mylogistics.mobileApi.controller.model;

public class OwnerPersonalKYCDetailByOwner {
	
	private String ownName;
	
	private String plotNo;
	
	private String addCity;
	
	private String addState;
	
	private String addPin;
	
	private String addType;
	
	private String addDist;
	
	private String addArea;
	
	private String phoneNo;
	
	private String alternateContactNo;
	
	private String emailAddress;
	
	private String addressProofUrl;

	private String ownCode;
	
	public String getOwnName() {
		return ownName;
	}

	public void setOwnName(String ownName) {
		this.ownName = ownName;
	}

	public String getPlotNo() {
		return plotNo;
	}

	public void setPlotNo(String plotNo) {
		this.plotNo = plotNo;
	}

	public String getAddCity() {
		return addCity;
	}

	public void setAddCity(String addCity) {
		this.addCity = addCity;
	}

	public String getAddState() {
		return addState;
	}

	public void setAddState(String addState) {
		this.addState = addState;
	}

	public String getAddPin() {
		return addPin;
	}

	public void setAddPin(String addPin) {
		this.addPin = addPin;
	}

	public String getAddType() {
		return addType;
	}

	public void setAddType(String addType) {
		this.addType = addType;
	}

	public String getAddDist() {
		return addDist;
	}

	public void setAddDist(String addDist) {
		this.addDist = addDist;
	}

	public String getAddArea() {
		return addArea;
	}

	public void setAddArea(String addArea) {
		this.addArea = addArea;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getAlternateContactNo() {
		return alternateContactNo;
	}

	public void setAlternateContactNo(String alternateContactNo) {
		this.alternateContactNo = alternateContactNo;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getAddressProofUrl() {
		return addressProofUrl;
	}

	public void setAddressProofUrl(String addressProofUrl) {
		this.addressProofUrl = addressProofUrl;
	}

	public String getOwnCode() {
		return ownCode;
	}

	public void setOwnCode(String ownCode) {
		this.ownCode = ownCode;
	}



}
