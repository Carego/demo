'use strict';

var app = angular.module('application');

app.controller('LhpvTempCntlr',['$scope','$location','$http','$filter',
                                  function($scope,$location,$http,$filter){
	
	$scope.vs = {};
	$scope.chlnList = [];
	//$scope.vs.lhpvTemp.ltChlnList = [];
	$scope.reqChlnList = [];
	
	$scope.chlnCodeDBFlag = true;
	$scope.saveVsFlag = true;
	
	$scope.getVoucherDetails = function(){
		console.log("Enter into getDetails");
		$scope.saveVsFlag = true;
		var response = $http.post($scope.projectName+'/getLhpvTempDetails');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.vs.branch = data.branch;
				$scope.vs.cashStmtStatus = data.cashStmtStatus;
				$scope.vs.voucherType = $scope.LHPV_TEMP_VOUCHER;
				$scope.dateTemp = $filter('date')(data.cashStmtStatus.cssDt, "yyyy-MM-dd'");
				$scope.getChlnNo();
			}else{
				console.log("Error in bringing data");
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
	
	$scope.getChlnNo = function(){
		console.log("enter into getChlnNo funciton");
		var response = $http.post($scope.projectName+'/getChlnNoFrLT');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.chlnList = data.list;
			}else{
				console.log("Error in bringing data");
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});	
	}
	
	
	$scope.openChlnDB = function(){
		console.log("enter into openChlnDB funciton");
		$scope.chlnCodeDBFlag = false;
		$('div#chlnCodeDB').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title : "Select Challan No.",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.chlnCodeDBFlag = true;
		    }
			});
		$('div#chlnCodeDB').dialog('open');
	}
	
	
	$scope.saveChln = function(chln){
		console.log("enter into saveChln funciton");
		$scope.reqChlnList.push(chln);
		$scope.chlnNo = chln;
		$('div#chlnCodeDB').dialog('close');
	}
	
	
	$scope.removeChln = function(index){
		console.log("enter into removeChln funciton");
		$scope.reqChlnList.splice(index,1);
	}
	
	
	$scope.voucherSubmit = function(newVoucherForm){
		console.log("enter into vouchersubmit funciton");
		if(newVoucherForm.$invalid){
			$scope.alertToast("please fill correct form");
		}else{
			console.log("final submittion");
			$scope.saveVsFlag = false;
	    	$('div#saveVsDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			        $scope.saveVsFlag = true;
			    }
				});
			$('div#saveVsDB').dialog('open');
		}
	}
	
	
	$scope.back = function(){
		console.log("enter into back function");
		$('div#saveVsDB').dialog('close');
	}
	
	$scope.saveVS = function(){
		console.log("enter into saveVS funciton");
		$('#saveId').attr("disabled","disabled"); 
		if($scope.reqChlnList.length > 0){
			$scope.vs.lhpvTemp.ltChlnList = $scope.reqChlnList;
		}
		var response = $http.post($scope.projectName+'/saveLhpvTemp',$scope.vs);
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$('#saveId').removeAttr("disabled");
				$('div#saveVsDB').dialog('close');
				$scope.chlnNo = "";
				$scope.vs = {};
				$scope.chlnList = [];
				$scope.reqChlnList = [];
				$scope.getVoucherDetails();
			}else{
				console.log("Error in bringing data");
			}
		});
		response.error(function(data, status, headers, config){
			console.log(data);
		});	
	}
	
	
	
	
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		 $scope.getVoucherDetails();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 }
}]);