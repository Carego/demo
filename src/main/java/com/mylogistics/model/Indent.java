package com.mylogistics.model;


import java.sql.Date;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="indent")
public class Indent {

	@Id
	@Column(name="indentId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int indentId;
	
	@Column(name="orderId",unique=true)
	private String orderId;
	
	@Column(name="branchName",length=20)
	private String branchName;
	
	@Column
	private int ctsId;
	
	@Column(name="indentDateTime")
	private Calendar indentDateTime;
	
//	@Column(name="indentTime",length=16)
//	private String indentTime; 
	
	@Column(name="placementDateTime")
	private Calendar placementDateTime;
	
//	@Column(name="placementTime",length=16)
//	private String placementTime; 
	
	@Column(name="custCode",length=5)
	private String custCode;
	
	@Column(name="cutomerName",length=64)
	private String cutomerName;
	
	@Column(name="consignorName",length=64)
	private String consignorName;
	
	@Column(name="consigneeName",length=64)
	private String consigneeName;
	
	@Column(name="fromStn",length=8)
	private String fromStn;
	
	@Column(name="toStn",length=8)
	private String toStn;
	
	@Column(name="kilometer")
	private int kilometer;
	
	@Column(name="transitTime")
	private int transitTime;
	
	@Column(name="vehicleType",length=64)
	private String vehicleType;
	
	@Column(name="material",length=64)
	private String material;
	
	@Column(name="totalWeight")
	private double totalWeight;
	
	@Column
	private double minGuaranteeWeight;
	
	@Column
	private String cnmtDimensionType;
	
	@Column
	private String dimension;
	
	@Column
	private int advancePercent;
	
	@Column
	private String advPayMode;
	
	@Column
	private String balPayMode;
	
	@Column
	private String balTerms;
	
	@Column(name="vehicleRqr")
	private int vehicleRqr;
	
	@Column(name="tools",length=64)
	private String tools;
	
	@Column(name="gps",length=5)
	private String gps;
	
	@Column(name="indentStatus",length=20)
	private String indentStatus;
	
	@Column(name="indentStage")
	private String indentStage;
	
	@Column(name="prTonRate")
	private double prTonRate;
	
	@Column(name="trgtrate")
	private double trgtRate;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=true)
	private Calendar creationTS;

	public int getIndentId() {
		return indentId;
	}

	public void setIndentId(int indentId) {
		this.indentId = indentId;
	}

	public Calendar getIndentDateTime() {
		return indentDateTime;
	}

	public void setIndentDateTime(Calendar indentDateTime) {
		this.indentDateTime = indentDateTime;
	}

	public Calendar getPlacementDateTime() {
		return placementDateTime;
	}

	public void setPlacementDateTime(Calendar placementDateTime) {
		this.placementDateTime = placementDateTime;
	}

	
	public int getCtsId() {
		return ctsId;
	}

	public void setCtsId(int ctsId) {
		this.ctsId = ctsId;
	}

	public String getCutomerName() {
		return cutomerName;
	}

	public void setCutomerName(String cutomerName) {
		this.cutomerName = cutomerName;
	}

	public String getConsignorName() {
		return consignorName;
	}

	public void setConsignorName(String consignorName) {
		this.consignorName = consignorName;
	}

	public String getConsigneeName() {
		return consigneeName;
	}

	public void setConsigneeName(String consigneeName) {
		this.consigneeName = consigneeName;
	}

	public String getFromStn() {
		return fromStn;
	}

	public void setFromStn(String fromStn) {
		this.fromStn = fromStn;
	}

	public String getToStn() {
		return toStn;
	}

	public void setToStn(String toStn) {
		this.toStn = toStn;
	}

	public int getKilometer() {
		return kilometer;
	}

	public void setKilometer(int kilometer) {
		this.kilometer = kilometer;
	}

	public int getTransitTime() {
		return transitTime;
	}

	public void setTransitTime(int transitTime) {
		this.transitTime = transitTime;
	}

	public String getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}

	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

	public double getTotalWeight() {
		return totalWeight;
	}

	public void setTotalWeight(double totalWeight) {
		this.totalWeight = totalWeight;
	}

	public int getVehicleRqr() {
		return vehicleRqr;
	}

	public void setVehicleRqr(int vehicleRqr) {
		this.vehicleRqr = vehicleRqr;
	}

	public String getTools() {
		return tools;
	}

	public void setTools(String tools) {
		this.tools = tools;
	}

	public String getGps() {
		return gps;
	}

	public void setGps(String gps) {
		this.gps = gps;
	}

	public double getPrTonRate() {
		return prTonRate;
	}

	public void setPrTonRate(double prTonRate) {
		this.prTonRate = prTonRate;
	}

	public double getTrgtRate() {
		return trgtRate;
	}

	public void setTrgtRate(double trgtRate) {
		this.trgtRate = trgtRate;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public double getMinGuaranteeWeight() {
		return minGuaranteeWeight;
	}

	public void setMinGuaranteeWeight(double minGuaranteeWeight) {
		this.minGuaranteeWeight = minGuaranteeWeight;
	}

	public String getCnmtDimensionType() {
		return cnmtDimensionType;
	}

	public void setCnmtDimensionType(String cnmtDimensionType) {
		this.cnmtDimensionType = cnmtDimensionType;
	}

	public String getDimension() {
		return dimension;
	}

	public void setDimension(String dimension) {
		this.dimension = dimension;
	}

	public int getAdvancePercent() {
		return advancePercent;
	}

	public void setAdvancePercent(int advancePercent) {
		this.advancePercent = advancePercent;
	}

	public String getAdvPayMode() {
		return advPayMode;
	}

	public void setAdvPayMode(String advPayMode) {
		this.advPayMode = advPayMode;
	}

	public String getBalPayMode() {
		return balPayMode;
	}

	public void setBalPayMode(String balPayMode) {
		this.balPayMode = balPayMode;
	}

	public String getBalTerms() {
		return balTerms;
	}

	public void setBalTerms(String balTerms) {
		this.balTerms = balTerms;
	}

	public String getCustCode() {
		return custCode;
	}

	public void setCustCode(String custCode) {
		this.custCode = custCode;
	}

	public String getIndentStatus() {
		return indentStatus;
	}

	public void setIndentStatus(String indentStatus) {
		this.indentStatus = indentStatus;
	}

	public String getIndentStage() {
		return indentStage;
	}

	public void setIndentStage(String indentStage) {
		this.indentStage = indentStage;
	}
	
	
}
