'use strict';

var app = angular.module('application');

app.controller('CustBillPrmsnCntlr',['$scope','$location','$http','$window',
                                 function($scope,$location,$http,$window){

	
	$scope.custList = [];
	$scope.actCust = {};
	$scope.selCust = {};
	
	$scope.setCustFlag = true;
	$scope.showCust = false;
	
	$scope.getCustomer = function(){
		console.log("enter into $scope.getCustomer function");
		var response = $http.post($scope.projectName+'/getCustFrBillPr');
		response.success(function(data, status, headers, config){
			if (data.result === "success") {
				$scope.custList = data.list;
			}else{
				$scope.alertToast("Server Error");
			}
		});
		response.error(function(data, status, headers, config){
			console.log("getOwnNameCodeId Error: "+data);
		});
	}
	
	
	
	$scope.openCustDB = function(){
		console.log("enter into openCustDB function");
		$scope.setCustFlag = false;
		$('div#setCustId').dialog({
			autoOpen: false,
			modal:true,
			resizable: false,
			title: "Select Customer",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			draggable: true,
			close: function(event, ui) { 
		        $(this).dialog('destroy');
		        $(this).hide();
		        $scope.setCustFlag = true;
		    }
			});
		
		$('div#setCustId').dialog('open');	
	}
	
	

	$scope.saveCustomer = function(cust){
		console.log("enter into saveCustomer function");
		$('div#setCustId').dialog('close');	
		$scope.customer = cust.custName;
		$scope.selCust = cust;
	}
	
	
	$scope.viewCustomer = function(custForm){
		console.log("enter into viewCustomer function = "+custForm.$invalid);
		if(custForm.$invalid){
			$scope.alertToast("Please select a customer");
		}else{
			var req = {
					"custId" : $scope.selCust.custId 	
				};
				
				var response = $http.post($scope.projectName+'/getCustBillPDt',req);
				  response.success(function(data, status, headers, config){
					  if(data.result === "success"){
						  $scope.actCust = data.cust;
						  $scope.showCust = true;
					  }else{
						 $scope.alertToast("Customer "+$scope.customer+" not exist");
						 $scope.showCust = false;
					  }
				   });
				   response.error(function(data, status, headers, config) {
						$scope.errorToast(data.result);
				   });	
		}
	}
	
	
	$scope.saveCustBill = function(){
		$scope.showCust = false;
		var response = $http.post($scope.projectName+'/saveCustBillPer',$scope.actCust);
		  response.success(function(data, status, headers, config){
			  if(data.result === "success"){
				  $scope.alertToast("SUCCESS");
				  $scope.actCust = {};
				  $scope.selCust = {};
				  $scope.setCustFlag = true;
			  }else{
				 $scope.alertToast("ERROR")
			  }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
		   });	
	}
	
    
	 if($scope.adminLogin === true || $scope.superAdminLogin === true){
		  $scope.getCustomer();
	 }else if($scope.logoutStatus === true){
			 $location.path("/");
	 }else{
			 console.log("****************");
	 } 
}]);