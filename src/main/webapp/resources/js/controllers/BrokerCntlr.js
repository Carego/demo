'use strict';

var app = angular.module('application');

app.controller('BrokerCntlr',['$scope','$location','$http','FileUploadService',
                                 function($scope,$location,$http,FileUploadService){
	
	$scope.mobileList = [];
	$scope.broker = {};
	$scope.add = {};
	$scope.docUpldFlag=false;
	 $('#saveBtnId').attr("disabled","disabled");
	
	$scope.panCardDBFlag = true;
	$scope.existBrokerDB=true;
	var panImgSize = 0;
	var decImgSize = 0;
	var chq=0;
	var pan=0;
	var dec=0;
	
	

	
	    
	   /* $scope.SubmitBroker = function(BrokerForm,CurrentAddressForm,ContactPersonForm,broker,contPerson,currentAddress){
			 console.log("enter into SubmitBroker function");
			 if(BrokerForm.$invalid){
				 if(BrokerForm.branchCode.$invalid){
					 $scope.alertToast("please Enter Branch Code");
				 }else if(BrokerForm.brkName.$invalid){
					 $scope.alertToast("please Enter Owner Name");
				 }else if(BrokerForm.brkVehicleType.$invalid){
					 $scope.alertToast("please Enter vehicle type");
				 }else if(BrokerForm.brkVoterId.$invalid){
					 $scope.alertToast("please Enter voterID number of 10 digits");
				 }else if(BrokerForm.brkPPNo.$invalid){
					 $scope.alertToast("please Enter PassPort No number within 8-25 digits");
				 }else if(BrokerForm.brkRegPlace.$invalid){
					 $scope.alertToast("please Enter Reg Place within 40 digits");
				 }else if(BrokerForm.brkEmailId.$invalid){
					 $scope.alertToast("please Enter correct email-Id");
				 }else if(BrokerForm.brkSrvTaxNo.$invalid){
					 $scope.alertToast("please Enter service tax number within 15 digits");
				 }else if(BrokerForm.brkFirmType.$invalid){
					 $scope.alertToast("please Enter Firm Type");
				 } else if(BrokerForm.brkBsnCard.$invalid){
					 $scope.alertToast("please Enter Business card ");
				 } 
			 }
			 
			
			
					
			 }
	    }
	  
			 $scope.back= function(){
					$scope.brokerFlag = true;
					$('div#brokerDB').dialog('close');
				}
			 */
			 
			 $scope.saveBroker = function(BrokerForm,add,broker){
				 console.log("Enter into save Broker Function");
				// $('div#brokerDB').dialog('close');
				 if(BrokerForm.phNoName.$invalid){
					 $scope.alertToast("please Enter valid phone no.");
					 return;
				 }
				 if(BrokerForm.$invalid){
					 $scope.alertToast("please Fill valid details");
					 return;
				 }
				 
				 $scope.broker.brkPhNoList = [];
				 console.log("Phone no.="+$scope.phNo);
				 $scope.broker.brkPhNoList.push($scope.phNo);
				 
				 var finalObject = {
						 "currentAddress" 		: $scope.add,
					 	 "broker"               : $scope.broker
					 };
				 
				 
				 $('#saveBtnId').attr("disabled","disabled");
					 var response = $http.post($scope.projectName+'/SubmitBrokerN',finalObject);
					   response.success(function(data, status, headers, config){
						   if(data.result==="success"){
							  // $('#saveBtnId').removeAttr("disabled");
							   $scope.successToast(data.msg+"="+data.brkCode);
							   $scope.newBrokerDetailDB=false;
							   $scope.broker = {};
							   $scope.add={};
							   $scope.phNo = "";
						   }else{
							   $scope.alertToast(data.msg);
							   $('#saveBtnId').removeAttr("disabled");
							   console.log(data);   
							   
						  }
					   });
					  response.error(function(data, status, headers, config) {
							$scope.errorToast(data.result);
							$scope.alertToast(data.msg);
					});

			 }
			 
	
		
				$scope.openUploadDB = function(){
					$scope.docUpldFlag=true;
					$('div#docUploadDBId').dialog({
						autoOpen: false,
						modal:true,
						title: "Documents Upload",
						show: UDShow,
						hide: UDHide,
						position: UDPos,
						resizable: false,
						draggable: true,
						close: function(event, ui) { 
							$(this).dialog('destroy') ;
							$(this).hide();
						}
					});

					$('div#docUploadDBId').dialog('open');
				}
		
		$scope.uploadPanImg = function(){
			console.log("enter into uploadPanImg function");
			var file = $scope.panImg;
			if(angular.isUndefined(file) || file === null || file === ""){
				$scope.alertToast("First choose the file----->");
			}else if(file.size > $scope.maxFileSize){
				$scope.alertToast("image size must be less than or equal to 1mb");
			}else{
				panImgSize = file.size;
				console.log('file is ' + JSON.stringify(file));
				var uploadUrl = $scope.projectName+"/upldBrkPanImg";
				FileUploadService.uploadFileToUrl(file, uploadUrl);
				console.log("file save on server");
				pan=1;
			}
		}
		
		
		
		
		$scope.uploadDecImg = function(){
			console.log("enter into uploadDecImg function");
			var file = $scope.decImg;
			if(angular.isUndefined(file) || file === null || file === ""){
				$scope.alertToast("First choose the file----->");
			}else if(file.size > $scope.maxFileSize){
				$scope.alertToast("image size must be less than or equal to 1mb");
			}else{
				decImgSize = file.size;
				console.log('file is ' + JSON.stringify(file));
				var uploadUrl = $scope.projectName+"/upldBrkDecImg";
				FileUploadService.uploadFileToUrl(file, uploadUrl);
				console.log("file save on server");
				dec=1;
			}	
		}
		
		
		$scope.uploadCCImage = function(ccImage){
			console.log("enter into uploadDecImg function");
			var file = ccImage;
			
			if(angular.isUndefined(file) || file === null || file === ""){
				$scope.alertToast("First choose the file----->");
			}else{
				console.log('file is ' + JSON.stringify(file));
				var uploadUrl = $scope.projectName+"/upldBrkChqImg";
				FileUploadService.uploadFileToUrl(file, uploadUrl);
				console.log("file save on server");
			       chq=1;
			}	
		}
	
		
		$scope.uploadImage=function(docUploadForm){
			
			if($scope.panImg == null || pan==0 ){
				$scope.alertToast("Please upload PAN image");
				return;
			}
			if($scope.ccImage == null || chq==0 ){
				$scope.alertToast("Please upload Cheque image");
				return;
			}
			if($scope.decImg != null && dec==0 ){
				$scope.alertToast("Please upload Declaration image");
				return;
			}
			
		      $('#saveBtnId').removeAttr("disabled");
		      $('div#docUploadDBId').dialog('close');
				
		}
		
	    
	    
	    $scope.verifyPan=function(brokerPanForm,panNo){
			console.log("Enter into verifyPan()"+panNo);
			if(brokerPanForm.$invalid){
				$scope.alertToast("Please Enter valid Pan No.");
			}else{
				var response = $http.post($scope.projectName+'/checkBrokerPan',panNo);
				response.success(function(data, status, headers, config) {
					
					if(data.result=="success"){
						$scope.existBrokerDB=false;
						$scope.newBrokerDetailDB=false;
						$scope.successToast("Pan No Already exist");
						console.log(data.list);
						$scope.brkList=data.list;
				    	$('div#existBrokerId').dialog({
							autoOpen: false,
							modal:true,
							resizable: false,
							show: UDShow,
							hide: UDHide,
							position: UDPos,
							draggable: true,
							close: function(event, ui) { 
						        $(this).dialog('destroy');
						        $(this).hide();
						        //$scope.existBrokerId = true;
						    }
							});
					$('div#existBrokerId').dialog('open');
						
					}else{
						$scope.newBrokerDetailDB=true;
						$scope.broker.brkPanNo=panNo;
					}
				});
				response.error(function(data, status, headers, config) {
					console.log(data.result);
					$scope.newBrokerDetailDB=false;
					$scope.errorToast("Please retry");
				});
			}
			
								
		}
		
	    
	    $scope.panNoChange=function(panNo){
	    	$scope.newBrokerDetailDB=false;
	    	$scope.broker.brkPanNo=panNo;
	    }
	    
	    
	    $scope.editExistBroker=function(brk){
			
			
			
		}
	    
	    
	    
	    
	    $scope.add={};
	    $scope.stateDBFlag=true;
		$scope.distDBFlag=true;
		$scope.cityDBFlag=true;
		$scope.stnDBFlag=true;
		
		
		$('#addPinId').keypress(function(key) {
			if(key.charCode < 48 || key.charCode > 57)
				return false;
		});
		
		$('#phNoId').keypress(function(key) {
			if(key.charCode < 48 || key.charCode > 57)
				return false;
		});
		
		$scope.getState=function(){
			console.log("getState()");
			 var response= $http.post($scope.projectName+'/getStateDetails');
				response.success(function(data, status, headers, config){
					if(data.result === "success"){
						//console.log(data.list);
						$scope.stateList=data.list;
						$scope.OpenStateDB();
					}else{
						$scope.alertToast("state not found in database");
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.alertToast("There is some problem in fetching state");
				});
			
		}
		
		
		
		$scope.OpenStateDB = function(){
			$scope.stateDBFlag=false;
			$('div#stateDB').dialog({
				autoOpen: false,
				modal:true,
				title: "State",
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				resizable: false,
				draggable: true,
				close: function(event, ui) { 
					$(this).dialog('destroy') ;
					$(this).hide();
				}
			});

			$('div#stateDB').dialog('open');
		}

		$scope.saveStateCode = function(state){
			$scope.stateCode = state.stateCode;
			$scope.stateName = state.stateName;
			$scope.stateGST=state.stateGST;
			$('div#stateDB').dialog('close');
			$scope.stateDBFlag=true;
			
			
			 var response= $http.post($scope.projectName+'/getADistByStateCode',state.stateCode);
				response.success(function(data, status, headers, config){
					if(data.result === "success"){
						//console.log(data.list);
						$scope.distList=data.list;
						$scope.OpenDistDB();
					}else{
						$scope.alertToast("District not found in database");
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.alertToast("There is some problem in fetching District");
				});
			
			
		}
		
		
		$scope.OpenDistDB = function(){
			$scope.distDBFlag=false;
			$('div#distDB').dialog({
				autoOpen: false,
				modal:true,
				title: "District",
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				resizable: false,
				draggable: true,
				close: function(event, ui) { 
					$(this).dialog('destroy') ;
					$(this).hide();
				}
			});

			$('div#distDB').dialog('open');
		}
		
		
		
		
		$scope.saveDist = function(dist){
			$scope.distName = dist;
			$('div#distDB').dialog('close');
			$scope.distDBFlag=true;
			var map={
					"dist":$scope.distName,
					"state":$scope.stateCode
			};
			
			 var response= $http.post($scope.projectName+'/getACityByDistName',map);
				response.success(function(data, status, headers, config){
					if(data.result === "success"){
						//console.log(data.list);
						$scope.cityList=data.list;
						$scope.OpenCityDB();
					}else{
						$scope.alertToast("city not found in database");
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.alertToast("There is some problem in fetching city");
				});
			
			
		}
		
		
		
		$scope.OpenCityDB = function(){
			$scope.cityDBFlag=false;
			$('div#cityDB').dialog({
				autoOpen: false,
				modal:true,
				title: "City",
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				resizable: false,
				draggable: true,
				close: function(event, ui) { 
					$(this).dialog('destroy') ;
					$(this).hide();
				}
			});

			$('div#cityDB').dialog('open');
		}
		
		
		$scope.saveCity = function(city){
			$scope.cityName = city;
			$('div#cityDB').dialog('close');
			$scope.cityDBFlag=true;
			var map={
					"city":$scope.cityName,
					"dist":$scope.distName,
					"state":$scope.stateCode
			};
			
			 var response= $http.post($scope.projectName+'/getAStnByCityDistName',map);
				response.success(function(data, status, headers, config){
					if(data.result === "success"){
						//console.log(data.list);
						$scope.stnList=data.list;
						$scope.OpenStnDB();
					}else{
						$scope.alertToast("station not found in database");
					}
				});
				response.error(function(data, status, headers, config) {
					$scope.alertToast("There is some problem in fetching station");
				});
			
			
		}
		
		
		$scope.OpenStnDB = function(){
			$scope.stnDBFlag=false;
			$('div#stnDB').dialog({
				autoOpen: false,
				modal:true,
				title: "Station",
				show: UDShow,
				hide: UDHide,
				position: UDPos,
				resizable: false,
				draggable: true,
				close: function(event, ui) { 
					$(this).dialog('destroy') ;
					$(this).hide();
				}
			});

			$('div#stnDB').dialog('open');
		}
		
		
		
		
		$scope.saveStn = function(stn){
						
			$scope.add.addPost=stn.stationName;
			$scope.add.addDist=stn.district;
			$scope.add.addState=$scope.stateName;
			$scope.add.addPin=stn.pinCode;
			$scope.add.addCity=stn.city;
			
			$('div#stnDB').dialog('close');
			console.log("Station"+stn.stationName);
			$scope.stnDBFlag=true;
		}
		
		
		
		
		$scope.getStnByPin=function(){
			
			if($scope.add.addPin.length < 6)
				return;
			
			var response= $http.post($scope.projectName+'/getAStnByPin',$scope.add.addPin);
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					//console.log(data.list +""+data.stateName);
					$scope.stnList=data.list;
					$scope.stateName=data.stateName;
					//$scope.stateGST=data.stateGST;
					//console.log("$scope.stateGST"+$scope.stateGST);
					$scope.OpenStnDB();
				}else{
					$scope.alertToast("station not found in database");
				}
			});
			response.error(function(data, status, headers, config) {
				$scope.alertToast("There is some problem in fetching station");
			});
			
			
		}
		
	    
		$scope.BranchCodeDBFlag=true;
		
		 $scope.getBranch= function(){
			  console.log("getBranchData------>");
			  var response = $http.post($scope.projectName+'/getBranchDataForChallan');
			  response.success(function(data, status, headers, config){
				  if(data.result === "success"){
					  $scope.branchList = data.list;
					  $scope.BranchCodeDBFlag=false;
					  $('div#branchCodeDB').dialog({
						  autoOpen: false,
						  modal:true,
						  title: "Branch Code",
						  show: UDShow,
						  hide: UDHide,
						  position: UDPos,
						  resizable: false,
						  draggable: true,
						  close: function(event, ui) { 
							  $(this).dialog('destroy') ;
							  $(this).hide();
						  }
					  });

					  $('div#branchCodeDB').dialog('open');
				  }else{
					  $scope.alertToast("you don't have any active branch");
					  console.log(data);
				  }
			  });
			  response.error(function(data, status, headers,config) {
				  $scope.errorToast(data);
			  });
		  }
		
		
		 $scope.saveBranchCode=function(branch){
			 $scope.broker.bCode=branch.branchCode;
			 $('div#branchCodeDB').dialog('close');
		 }
	    
	    
	    if($scope.operatorLogin === true || $scope.superAdminLogin === true){
	    	 //$scope.fetch();
		 }else if($scope.logoutStatus === true){
			 $location.path("/");
		 }else{
			 console.log("****************");
		 } 
	   
	   
}]);