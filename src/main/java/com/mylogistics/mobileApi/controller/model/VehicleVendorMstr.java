package com.mylogistics.mobileApi.controller.model;

import java.io.File;
import java.sql.Date;

public class VehicleVendorMstr {

	private int vvId;
	
	private String vvRcNo;
	
	private Date vvRcIssueDt;
	
	private Date vvRcValidDt;
	
	private String vvPerNo;
	
	private Date vvPerIssueDt;
	
	private Date vvPerValidDt;
	
	private String vvPerState;
	
	private Date vvFitValidDt;
	
	private String vvPolicyNo;
	
	private Date vvPolicyValidDt;
	
	private String vvPolicyComp;
	
	private String vvTransitPassNo;
	
	private String vvPollutionNorms;
	
	private String vvPermitType;
	
	private String vvPollutionImgPath;
	
	private Date vvPollutionUptoDate;
	
	private String vvType;
	
	private String ownCode;
	
	private String vvEngineNo;
	
	private String vvTransferId;
	
	private Date vvTransferDt;
	
	private String vvChassisNo;
	
	private int vvModel;
	
	private String vvModelNo;
	
	private String vvHypoTo;
	
	/*private File rcImg;
	
	private File prmitSlipImg;
	
	private File insuranceImg;
	
	private File pollutionImg;*/
	private String vvRcImgPath;
	
	private String vvPerSlpImgPath;
	
	private String vvPolicyImgPath;
	
	public int getVvId() {
		return vvId;
	}

	public void setVvId(int vvId) {
		this.vvId = vvId;
	}

	public String getVvRcNo() {
		return vvRcNo;
	}

	public void setVvRcNo(String vvRcNo) {
		this.vvRcNo = vvRcNo;
	}

	public Date getVvRcIssueDt() {
		return vvRcIssueDt;
	}

	public void setVvRcIssueDt(Date vvRcIssueDt) {
		this.vvRcIssueDt = vvRcIssueDt;
	}

	public Date getVvRcValidDt() {
		return vvRcValidDt;
	}

	public void setVvRcValidDt(Date vvRcValidDt) {
		this.vvRcValidDt = vvRcValidDt;
	}

	public String getVvPerNo() {
		return vvPerNo;
	}

	public void setVvPerNo(String vvPerNo) {
		this.vvPerNo = vvPerNo;
	}

	public Date getVvPerIssueDt() {
		return vvPerIssueDt;
	}

	public void setVvPerIssueDt(Date vvPerIssueDt) {
		this.vvPerIssueDt = vvPerIssueDt;
	}

	public Date getVvPerValidDt() {
		return vvPerValidDt;
	}

	public void setVvPerValidDt(Date vvPerValidDt) {
		this.vvPerValidDt = vvPerValidDt;
	}

	public String getVvPerState() {
		return vvPerState;
	}

	public void setVvPerState(String vvPerState) {
		this.vvPerState = vvPerState;
	}


	public Date getVvFitValidDt() {
		return vvFitValidDt;
	}

	public void setVvFitValidDt(Date vvFitValidDt) {
		this.vvFitValidDt = vvFitValidDt;
	}


	public String getVvPolicyNo() {
		return vvPolicyNo;
	}

	public void setVvPolicyNo(String vvPolicyNo) {
		this.vvPolicyNo = vvPolicyNo;
	}

	public String getVvPolicyComp() {
		return vvPolicyComp;
	}

	public void setVvPolicyComp(String vvPolicyComp) {
		this.vvPolicyComp = vvPolicyComp;
	}

	public String getVvTransitPassNo() {
		return vvTransitPassNo;
	}

	public void setVvTransitPassNo(String vvTransitPassNo) {
		this.vvTransitPassNo = vvTransitPassNo;
	}

	public String getVvType() {
		return vvType;
	}

	public void setVvType(String vvType) {
		this.vvType = vvType;
	}


	public String getOwnCode() {
		return ownCode;
	}

	public void setOwnCode(String ownCode) {
		this.ownCode = ownCode;
	}

	/*public File getRcImg() {
		return rcImg;
	}

	public void setRcImg(File rcImg) {
		this.rcImg = rcImg;
	}

	public File getPrmitSlipImg() {
		return prmitSlipImg;
	}

	public void setPrmitSlipImg(File prmitSlipImg) {
		this.prmitSlipImg = prmitSlipImg;
	}

	public File getInsuranceImg() {
		return insuranceImg;
	}

	public void setInsuranceImg(File insuranceImg) {
		this.insuranceImg = insuranceImg;
	}

	public File getPollutionImg() {
		return pollutionImg;
	}

	public void setPollutionImg(File pollutionImg) {
		this.pollutionImg = pollutionImg;
	}*/


	public String getVvEngineNo() {
		return vvEngineNo;
	}

	public void setVvEngineNo(String vvEngineNo) {
		this.vvEngineNo = vvEngineNo;
	}

	public String getVvChassisNo() {
		return vvChassisNo;
	}

	public void setVvChassisNo(String vvChassisNo) {
		this.vvChassisNo = vvChassisNo;
	}

	public int getVvModel() {
		return vvModel;
	}

	public void setVvModel(int vvModel) {
		this.vvModel = vvModel;
	}

	public Date getVvPolicyValidDt() {
		return vvPolicyValidDt;
	}

	public void setVvPolicyValidDt(Date vvPolicyValidDt) {
		this.vvPolicyValidDt = vvPolicyValidDt;
	}


	public String getVvHypoTo() {
		return vvHypoTo;
	}

	public void setVvHypoTo(String vvHypoTo) {
		this.vvHypoTo = vvHypoTo;
	}

	public String getVvRcImgPath() {
		return vvRcImgPath;
	}

	public void setVvRcImgPath(String vvRcImgPath) {
		this.vvRcImgPath = vvRcImgPath;
	}

	public String getVvPerSlpImgPath() {
		return vvPerSlpImgPath;
	}

	public void setVvPerSlpImgPath(String vvPerSlpImgPath) {
		this.vvPerSlpImgPath = vvPerSlpImgPath;
	}

	public String getVvPolicyImgPath() {
		return vvPolicyImgPath;
	}

	public void setVvPolicyImgPath(String vvPolicyImgPath) {
		this.vvPolicyImgPath = vvPolicyImgPath;
	}

	public String getVvModelNo() {
		return vvModelNo;
	}

	public void setVvModelNo(String vvModelNo) {
		this.vvModelNo = vvModelNo;
	}

	public String getVvTransferId() {
		return vvTransferId;
	}

	public void setVvTransferId(String vvTransferId) {
		this.vvTransferId = vvTransferId;
	}

	public Date getVvTransferDt() {
		return vvTransferDt;
	}

	public void setVvTransferDt(Date vvTransferDt) {
		this.vvTransferDt = vvTransferDt;
	}

	public String getVvPollutionNorms() {
		return vvPollutionNorms;
	}

	public void setVvPollutionNorms(String vvPollutionNorms) {
		this.vvPollutionNorms = vvPollutionNorms;
	}

	public String getVvPermitType() {
		return vvPermitType;
	}

	public void setVvPermitType(String vvPermitType) {
		this.vvPermitType = vvPermitType;
	}

	public String getVvPollutionImgPath() {
		return vvPollutionImgPath;
	}

	public void setVvPollutionImgPath(String vvPollutionImgPath) {
		this.vvPollutionImgPath = vvPollutionImgPath;
	}

	public Date getVvPollutionUptoDate() {
		return vvPollutionUptoDate;
	}

	public void setVvPollutionUptoDate(Date vvPollutionUptoDate) {
		this.vvPollutionUptoDate = vvPollutionUptoDate;
	}


}
