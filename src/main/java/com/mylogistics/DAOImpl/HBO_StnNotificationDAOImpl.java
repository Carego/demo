package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpSession;
import javax.validation.Constraint;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.validator.internal.engine.ConstraintViolationImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.HBO_StnNotificationDAO;
import com.mylogistics.constants.HBO_StnNotificationCNTS;
import com.mylogistics.constants.StationaryCNTS;
import com.mylogistics.model.ChCnSeDetail;
import com.mylogistics.model.HBO_StnNotification;
import com.mylogistics.model.MasterStationaryStk;
import com.mylogistics.model.MasterStationaryStk.ValidationForInsert;
import com.mylogistics.model.Stationary;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;

public class HBO_StnNotificationDAOImpl implements HBO_StnNotificationDAO{
	
	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	public HBO_StnNotificationDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	
	@Transactional
	public int saveHBO_StnNot(HBO_StnNotification hBO_StnNotification){
		System.out.println("enter into saveHBO_StnNot function");
		int temp;
		hBO_StnNotification.sethBOSN_hasRec("not received");
		hBO_StnNotification.sethBOSN_isCreate("no");
		try{
		    session = this.sessionFactory.openSession();
			transaction =  session.beginTransaction();
			session.save(hBO_StnNotification);
			transaction.commit();
			session.flush();
			temp= 1;
		}
		catch(Exception e){
			e.printStackTrace();
			temp= -1;
		}
		session.clear();
		session.close();
		return temp;		
	}
	
	
	@Transactional
	public List<HBO_StnNotification> getHBOSNData(String opCode){
		System.out.println("enter into getHBOSNData function");
		List<HBO_StnNotification> HBOSNList = new ArrayList<HBO_StnNotification>();
		try{
		    session = this.sessionFactory.openSession();
			transaction =  session.beginTransaction();
			Criteria cr=session.createCriteria(HBO_StnNotification.class);
			cr.add(Restrictions.eq(HBO_StnNotificationCNTS.HBOSN_OPCODE,opCode));
			cr.add(Restrictions.eq(HBO_StnNotificationCNTS.HBOSN_HASREC,"not received"));
			HBOSNList = cr.list();
			transaction.commit();
			session.flush();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return HBOSNList;		
	}
	
	
	@Transactional
	public int updateHBOSN(HBO_StnNotification HBOSN){
		System.out.println("enter into updateHBOSN function---->"+HBOSN.gethBOSN_Id());
		int temp;
		//HBOSN.sethBOSN_hasRec("received");
		try{
		    session = this.sessionFactory.openSession();
			transaction =  session.beginTransaction();
			Criteria cr=session.createCriteria(HBO_StnNotification.class);
			cr.add(Restrictions.eq(HBO_StnNotificationCNTS.HBOSN_ID,HBOSN.gethBOSN_Id()));
			List<HBO_StnNotification> HBOSNList = cr.list();
			HBO_StnNotification hbosn = HBOSNList.get(0);
			hbosn.sethBOSN_hasRec("received");
			session.saveOrUpdate(hbosn);
			transaction.commit();
			session.flush();
			temp =1;
		}
		catch(Exception e){
			e.printStackTrace();
			temp = -1;
		}
		session.clear();
		session.close();
		return temp;		
	}
	
	
	
	@Transactional
	public List<HBO_StnNotification> getHBOSNForSA(){
		System.out.println("enter into getHBOSNForSA function");
		List<HBO_StnNotification> HBOSNList = new ArrayList<HBO_StnNotification>();
		try{
		    session = this.sessionFactory.openSession();
			transaction =  session.beginTransaction();
			Criteria cr=session.createCriteria(HBO_StnNotification.class);
			cr.add(Restrictions.eq(HBO_StnNotificationCNTS.HBOSN_ISVIEW,"no"));
			HBOSNList = cr.list();
			transaction.commit();
			session.flush();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return HBOSNList;		
	}
	
	
	
	@Transactional
	public int confirmOrder(int id){
		System.out.println("enter into confirmOrder function");
		int temp;
		try{
		    session = this.sessionFactory.openSession();
			transaction =  session.beginTransaction();
			Criteria cr=session.createCriteria(HBO_StnNotification.class);
			cr.add(Restrictions.eq(HBO_StnNotificationCNTS.HBOSN_ID,id));
			HBO_StnNotification hBO_StnNotification = (HBO_StnNotification) cr.list().get(0);
			hBO_StnNotification.sethBOSN_isView("yes");
			session.saveOrUpdate(hBO_StnNotification);
			transaction.commit();
			session.flush();
			temp = 1;
		}
		catch(Exception e){
			temp = -1;
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return temp;		
	}
	
	
	@Transactional
	public int updateIsCreate(HBO_StnNotification hBO_StnNotification){
		System.out.println("enter into updateIsCreate function");
		int temp;
		try{
		    session = this.sessionFactory.openSession();
			transaction =  session.beginTransaction();
			Criteria cr=session.createCriteria(HBO_StnNotification.class);
			cr.add(Restrictions.eq(HBO_StnNotificationCNTS.HBOSN_ID,hBO_StnNotification.gethBOSN_Id()));
			HBO_StnNotification hbosn = (HBO_StnNotification) cr.list().get(0);
			hbosn.sethBOSN_isCreate("yes");
			hbosn.sethBOSN_stnId(hBO_StnNotification.gethBOSN_stnId());
			session.merge(hbosn);
			session.saveOrUpdate(hbosn);
			transaction.commit();
			session.flush();
			temp = 1;
		}
		catch(Exception e){
			temp = -1;
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return temp;		
	}
	
	 @Transactional
	 @SuppressWarnings("unchecked")
	public int updateHBO(Map<String , Object> clientMap){
		 System.out.println("entre into updateHBO function");
		 int temp = 0;
		 String cnmtSeq = (String) clientMap.get("nowCnmtSeq");
		 String chlnSeq = (String) clientMap.get("nowChlnSeq");
		 String sedrSeq = (String) clientMap.get("nowSedrSeq");
		 int hbosnId = (int) clientMap.get("hbosnId");
		 User currentUser = (User) httpSession.getAttribute("currentUser");
		 try{
			session = this.sessionFactory.openSession();
			transaction =  session.beginTransaction();
			HBO_StnNotification hbosn = (HBO_StnNotification) session.get(HBO_StnNotification.class, hbosnId);
			if(hbosn != null){
				hbosn.sethBOSN_hasRec("received");
				session.update(hbosn);
				
				Stationary stationary = (Stationary) session.get(Stationary.class, hbosn.gethBOSN_stnId());
				if(stationary != null){
						String stOdrCode = stationary.getStCode();
					
					int nobCnmt = hbosn.gethBOSN_cnmtNo();
					int nobChln = hbosn.gethBOSN_chlnNo();
					int nobSedr = hbosn.gethBOSN_sedrNo();
					
					
					if (nobCnmt>0) {
						
						String chCnSeCode = null;			
						ChCnSeDetail chCnSeDetail  = new ChCnSeDetail();			
						chCnSeDetail.setChCnSeNob(nobCnmt);
						chCnSeDetail.setChCnSeStOdrCode(stOdrCode);
						chCnSeDetail.setChCnSeStatus("cnmt");
						chCnSeDetail.setUserCode(currentUser.getUserCode());
						chCnSeDetail.setbCode(currentUser.getUserBranchCode());
						long endNo = 50*nobCnmt;
						long strtNo = Long.parseLong(cnmtSeq);
						endNo = strtNo + endNo;
						chCnSeDetail.setChCnSeStartNo(cnmtSeq);
						chCnSeDetail.setChCnSeEndNo(String.valueOf(endNo));
						chCnSeCode = "cn"+cnmtSeq;
						chCnSeDetail.setChCnSeCode(chCnSeCode);
						session.save(chCnSeDetail);
						
						Long startNo = strtNo;
						
						for(int i=0; i<nobCnmt; i++){
							List<MasterStationaryStk> mstrStnStkList = session.createCriteria(MasterStationaryStk.class)
									.add(Restrictions.eq("mstrStnStkStartNo", startNo))
									.list();
							if(mstrStnStkList.isEmpty()){
								MasterStationaryStk stk = new MasterStationaryStk();
								stk.setMstrStnStkbCode(1);
								stk.setMstrStnStkStartNo(startNo);
								stk.setMstrStnStkStatus("cnmt");
								stk.setUserCode(Integer.parseInt(currentUser.getUserCode()));
								stk.setIsIssued("no");
								stk.setStationary(stationary);
								stk.setIsDirty("no");
								
								ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
								Validator validator = factory.getValidator();
								Set<ConstraintViolation<MasterStationaryStk>> validMsg = validator.validate(stk, ValidationForInsert.class);
								
								if(validMsg.isEmpty()){
									session.save(stk);
								}else{
									System.out.println("Error");
								}
								startNo = startNo+50;
							}else{
								System.err.println("Already exits ;");
							}
						}				
					}
					if (nobChln>0) {
						ChCnSeDetail chCnSeDetail  = new ChCnSeDetail();				
						String chCnSeCode = null;				
						chCnSeDetail.setChCnSeNob(nobChln);
						chCnSeDetail.setChCnSeStOdrCode(stOdrCode);
						chCnSeDetail.setChCnSeStatus("chln");
						chCnSeDetail.setUserCode(currentUser.getUserCode());
						chCnSeDetail.setbCode(currentUser.getUserBranchCode());
						long endNo = 50*nobChln;
						long strtNo = Long.parseLong(chlnSeq);
						endNo = strtNo + endNo;
						System.out.println("The End Number for CHLN in if case is------<<>>>---"+endNo);
						chCnSeDetail.setChCnSeStartNo(chlnSeq);
						chCnSeDetail.setChCnSeEndNo(String.valueOf(endNo));
						chCnSeCode = "ch"+chlnSeq;
						chCnSeDetail.setChCnSeCode(chCnSeCode);
						session.save(chCnSeDetail);
						
						
						Long startNo = strtNo;
						
						for(int i=0; i<nobChln; i++){
							List<MasterStationaryStk> mstrStnStkList = session.createCriteria(MasterStationaryStk.class)
									.add(Restrictions.eq("mstrStnStkStartNo", startNo))
									.list();
							if(mstrStnStkList.isEmpty()){
								MasterStationaryStk stk = new MasterStationaryStk();
								stk.setMstrStnStkbCode(1);
								stk.setMstrStnStkStartNo(startNo);
								stk.setMstrStnStkStatus("chln");
								stk.setUserCode(Integer.parseInt(currentUser.getUserCode()));
								stk.setIsIssued("no");
								stk.setIsDirty("no");
								stk.setStationary(stationary);
								
								ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
								Validator validator = factory.getValidator();
								Set<ConstraintViolation<MasterStationaryStk>> validMsg = validator.validate(stk, ValidationForInsert.class);
								
								if(validMsg.isEmpty()){
									session.save(stk);
								}else{
									System.out.println("Validation Error !");
								}
								startNo = startNo+50;
							}else{
								System.err.println("Already exits ;");
							}
						}			
					}
					
					
					if (nobSedr>0) {
						ChCnSeDetail chCnSeDetail  = new ChCnSeDetail();
						String chCnSeCode = null;
						chCnSeDetail.setChCnSeNob(nobSedr);
						chCnSeDetail.setChCnSeStOdrCode(stOdrCode);
						chCnSeDetail.setChCnSeStatus("sedr");
						chCnSeDetail.setUserCode(currentUser.getUserCode());
						long endNo = 50*nobSedr;
						long strtNo = Long.parseLong(sedrSeq);
						endNo = strtNo + endNo;
						chCnSeDetail.setChCnSeStartNo(sedrSeq);
						chCnSeDetail.setChCnSeEndNo(String.valueOf(endNo));
						chCnSeCode = "se"+sedrSeq;
						chCnSeDetail.setChCnSeCode(chCnSeCode);
						
						session.save(chCnSeDetail);
						
						Long startNo = strtNo;
						
						for(int i=0; i<nobSedr; i++){
							List<MasterStationaryStk> mstrStnStkList = session.createCriteria(MasterStationaryStk.class)
									.add(Restrictions.eq("mstrStnStkStartNo", startNo))
									.list();
							if(mstrStnStkList.isEmpty()){
								MasterStationaryStk stk = new MasterStationaryStk();
								stk.setMstrStnStkbCode(1);
								stk.setMstrStnStkStartNo(startNo);
								stk.setMstrStnStkStatus("sedr");
								stk.setUserCode(Integer.parseInt(currentUser.getUserCode()));
								stk.setIsIssued("no");
								stk.setIsDirty("no");
								stk.setStationary(stationary);
								
								ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
								Validator validator = factory.getValidator();
								Set<ConstraintViolation<MasterStationaryStk>> validMsg = validator.validate(stk, ValidationForInsert.class);
								
								if(validMsg.isEmpty()){
									session.save(stk);
								}else{
									System.out.println("Validation Error !");
								}
								startNo = startNo+50;
							}else{
								System.err.println("Already exits ;");
							}
						}		
						
					}
				}
			}
			
			
			
			
			transaction.commit();
			session.flush();
			temp = 1;
			
			System.out.println("HI>>> : "+temp);
		 }catch(Exception e){
			e.printStackTrace();
			temp = -1;
		 }finally{
			 session.clear();
			 session.close();
		 }
		 return temp;
	 }
}
