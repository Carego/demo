package com.mylogistics.DAOImpl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.FaTravDivisionDAO;
import com.mylogistics.DAO.bank.BankMstrDAO;
import com.mylogistics.constants.BranchCNTS;
import com.mylogistics.constants.CashStmtCNTS;
import com.mylogistics.constants.CashStmtStatusCNTS;
import com.mylogistics.constants.EmployeeCNTS;
import com.mylogistics.constants.FAMasterCNTS;
import com.mylogistics.constants.FaTravDivisionCNTS;
import com.mylogistics.constants.bank.BankMstrCNTS;
import com.mylogistics.constants.bank.ChequeLeavesCNTS;
import com.mylogistics.model.Branch;
import com.mylogistics.model.CashStmt;
import com.mylogistics.model.CashStmtStatus;
import com.mylogistics.model.CashStmtTemp;
import com.mylogistics.model.Employee;
import com.mylogistics.model.FAMaster;
import com.mylogistics.model.FaTravDivision;
import com.mylogistics.model.User;
import com.mylogistics.model.bank.BankMstr;
import com.mylogistics.model.bank.ChequeLeaves;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.TravDivService;
import com.mylogistics.services.TravVoucherService;
import com.mylogistics.services.VoucherService;
//import java.util.Date;

public class FaTravDivisionDAOImpl implements FaTravDivisionDAO{

	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private BankMstrDAO bankMstrDAO;
	
	
	@Autowired
	public FaTravDivisionDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Transactional
	@SuppressWarnings("unchecked")
	@Override
	public Map<String,Object> saveTrvlVoucByCash(VoucherService voucherService , TravVoucherService travVoucherService){
		System.out.println("enter into saveTrvlVoucByCash function");
		Map<String,Object> map = new HashMap<String,Object>();
		User currentUser = (User) httpSession.getAttribute("currentUser");
		String brFaCode = voucherService.getBranch().getBranchFaCode();
		CashStmtStatus cashStmtStatus = voucherService.getCashStmtStatus();
		//java.sql.Date sqlDate = new java.sql.Date(new Date().getTime());
		List<TravDivService> tdSerList = travVoucherService.getAllTDService();
		List<CashStmtStatus> cssList = new ArrayList<CashStmtStatus>();
		List<FAMaster> faMList = new ArrayList<FAMaster>();
		int voucherNo = 0;
		String payMode = "C";
		if(!tdSerList.isEmpty()){
			
			try{
				session = this.sessionFactory.openSession();
				transaction = session.beginTransaction();
				System.out.println("*************"+transaction.isActive());
				
				java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
				
				Criteria crr = session.createCriteria(FAMaster.class);
				crr.add(Restrictions.eq(FAMasterCNTS.FA_MFA_NAME,ConstantsValues.TRAV_EXP_FANAME));
				faMList =crr.list();
				String trFaCode = faMList.get(0).getFaMfaCode();
				
				Criteria cr = session.createCriteria(CashStmt.class);
				cr.add(Restrictions.eq(CashStmtCNTS.CS_CDT,sqlDate));
				cr.add(Restrictions.eq(CashStmtCNTS.USER_BRANCH_CODE,currentUser.getUserBranchCode()));
				ProjectionList proList = Projections.projectionList();
				proList.add(Projections.property("csVouchNo"));
				cr.setProjection(proList);
				
				List<Integer>  voucherNoList = cr.list();
				
				if(!voucherNoList.isEmpty()){
					//int vNo = voucherNoList.get(voucherNoList.size() - 1);
					for(int i=0;i<voucherNoList.size();i++){
						if(voucherNo < voucherNoList.get(i)){
							voucherNo = voucherNoList.get(i);
						}
					}
					voucherNo = voucherNo + 1;
				}else{
					voucherNo = 1;
				}
				
				String tdsCode = voucherService.getTdsCode();
				double tdsAmt = voucherService.getTdsAmt();
				System.out.println("tdsCode = "+tdsCode+" --- tdsAmt = "+tdsAmt);
	/*			if(tdsCode != null && tdsAmt > 0){
					
					CashStmt csWTds = new CashStmt();
					csWTds.setbCode(currentUser.getUserBranchCode());
					csWTds.setUserCode(currentUser.getUserCode());
					csWTds.setCsDescription(voucherService.getDesc());
					csWTds.setCsDrCr('C');
					csWTds.setCsAmt(tdsAmt);
					csWTds.setCsType(voucherService.getVoucherType());
					csWTds.setCsVouchType("cash");
					csWTds.setCsPayTo(voucherService.getPayTo());
					csWTds.setCsTvNo(ConstantsValues.CONST_TVNO);
					csWTds.setCsFaCode(tdsCode);
					csWTds.setCsVouchNo(voucherNo);
					csWTds.setCsDt(sqlDate);
					
					Criteria cr1 = session.createCriteria(CashStmtStatus.class);
					cr1.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
					cssList = cr1.list();
					
					if(!cssList.isEmpty()){
						CashStmtStatus csStatus = cssList.get(0);
						csWTds.setCsNo(csStatus);
						csStatus.getCashStmtList().add(csWTds);
						System.out.println("tds case successfully saved");
						session.update(csStatus);
						//transaction.commit();
					}	
				}*/
				
				
				for(int i=0 ; i<tdSerList.size() ; i++){
					
					TravDivService travDivService = tdSerList.get(i);
					Employee employee = travDivService.getEmployee();
					double totAmt = travDivService.getTotAmt();
					List<FaTravDivision> ftdList = travDivService.getFtdList();
					
					List<Branch> brList = new ArrayList<Branch>();
					int brId = Integer.parseInt(employee.getBranchCode());
					Criteria cr2 = session.createCriteria(Branch.class);
					cr2.add(Restrictions.eq(BranchCNTS.BRANCH_ID,brId));
					brList = cr2.list();
					
					String trBrFaCode = brList.get(0).getBranchFaCode();					
					
					java.util.Date date = new java.util.Date();
					Calendar calendar = Calendar.getInstance();
					calendar.setTime(date);
					long timeMilles = calendar.getTimeInMillis();
					
					String tvNo = String.valueOf(timeMilles);	
									
					
					if(brFaCode.equalsIgnoreCase(trBrFaCode)){
						
						CashStmt cashStmt = new CashStmt();
						cashStmt.setbCode(currentUser.getUserBranchCode());
						cashStmt.setUserCode(currentUser.getUserCode());
						cashStmt.setCsDescription(voucherService.getDesc());
						cashStmt.setCsDrCr('D');
						cashStmt.setCsAmt(totAmt);
						cashStmt.setCsType(voucherService.getVoucherType());
						cashStmt.setCsVouchType("cash");
						cashStmt.setCsPayTo(voucherService.getPayTo());
						cashStmt.setCsTvNo(tvNo);
						cashStmt.setCsFaCode(trFaCode);
						cashStmt.setCsVouchNo(voucherNo);
						cashStmt.setCsDt(sqlDate);
						cashStmt.setPayMode(payMode);
						
						Criteria cr3 = session.createCriteria(CashStmtStatus.class);
						cr3.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
						cssList = cr3.list();
						int csId = 0;
						
						if(!cssList.isEmpty()){
							CashStmtStatus csStatus = cssList.get(0);
							cashStmt.setCsNo(csStatus);
							csId = (Integer) session.save(cashStmt);
							csStatus.getCashStmtList().add(cashStmt);
							session.update(csStatus);
							//transaction.commit();
						}	
						
						for(int k=0;k<ftdList.size();k++){
							
							System.out.println("csId ====> "+csId);
							System.out.println("employee id = "+employee.getEmpId());
							List<Employee> empList = new ArrayList<Employee>();
							Criteria cr4 = session.createCriteria(Employee.class);
							cr4.add(Restrictions.eq(EmployeeCNTS.EMP_ID,employee.getEmpId()));
							empList = cr4.list();
							Employee actEmp = empList.get(0);
							
							FaTravDivision faTravDivision = ftdList.get(k);
							faTravDivision.setbCode(currentUser.getUserBranchCode());
							faTravDivision.setUserCode(currentUser.getUserCode());
							faTravDivision.setFtdCsId(csId);
							
							faTravDivision.setEmployee(actEmp);
							int ftdId = (Integer) session.save(faTravDivision);
														
							cashStmt.getCsTravIdList().add(ftdId);
							
							actEmp.getFtdList().add(faTravDivision);
							
							session.update(actEmp);
							session.update(cashStmt);
							//transaction.commit();
						}
						
						if(tdsCode != null && tdsAmt > 0){
							
							CashStmt csWTds = new CashStmt();
							csWTds.setbCode(currentUser.getUserBranchCode());
							csWTds.setUserCode(currentUser.getUserCode());
							csWTds.setCsDescription(voucherService.getDesc());
							csWTds.setCsDrCr('C');
							csWTds.setCsAmt(tdsAmt);
							csWTds.setCsType(voucherService.getVoucherType());
							csWTds.setCsVouchType("cash");
							csWTds.setCsPayTo(voucherService.getPayTo());
							csWTds.setCsTvNo(tvNo);
							csWTds.setCsFaCode(tdsCode);
							csWTds.setCsVouchNo(voucherNo);
							csWTds.setCsDt(sqlDate);
							csWTds.setPayMode(payMode);
							
							Criteria cr1 = session.createCriteria(CashStmtStatus.class);
							cr1.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
							cssList = cr1.list();
							
							if(!cssList.isEmpty()){
								CashStmtStatus csStatus = cssList.get(0);
								csWTds.setCsNo(csStatus);
								csStatus.getCashStmtList().add(csWTds);
								System.out.println("tds case successfully saved");
								session.update(csStatus);
								//transaction.commit();
							}	
						}
						
					}else{
						
						CashStmt cashStmt1 = new CashStmt();
						cashStmt1.setbCode(currentUser.getUserBranchCode());
						cashStmt1.setUserCode(currentUser.getUserCode());
						cashStmt1.setCsDescription(voucherService.getDesc());
						cashStmt1.setCsDrCr('D');
						cashStmt1.setCsAmt(totAmt - tdsAmt);
						cashStmt1.setCsType(voucherService.getVoucherType());
						cashStmt1.setCsVouchType("cash");
						cashStmt1.setCsTvNo(tvNo);
						cashStmt1.setCsPayTo(voucherService.getPayTo());
						cashStmt1.setCsFaCode(trBrFaCode);
						cashStmt1.setCsVouchNo(voucherNo);
						cashStmt1.setCsDt(sqlDate);
						cashStmt1.setPayMode(payMode);
						
						Criteria cr5 = session.createCriteria(CashStmtStatus.class);
						cr5.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
						cssList = cr5.list();
						
						int csId = 0;
						if(!cssList.isEmpty()){
							CashStmtStatus csStatus = cssList.get(0);
							cashStmt1.setCsNo(csStatus);
							csId = (Integer) session.save(cashStmt1);
							csStatus.getCashStmtList().add(cashStmt1);
							session.update(csStatus);
							//transaction.commit();
						}
						
						for(int k=0;k<ftdList.size();k++){
							
							System.out.println("csId = "+csId);
							
							List<Employee> empList = new ArrayList<Employee>();
							Criteria cr6 = session.createCriteria(Employee.class);
							cr6.add(Restrictions.eq(EmployeeCNTS.EMP_ID,employee.getEmpId()));
							empList = cr6.list();
							Employee actEmp = empList.get(0);
							
							FaTravDivision faTravDivision = ftdList.get(k);
							faTravDivision.setbCode(currentUser.getUserBranchCode());
							faTravDivision.setUserCode(currentUser.getUserCode());
							faTravDivision.setFtdCsId(csId);
							
							faTravDivision.setEmployee(actEmp);
							int ftdId = (Integer) session.save(faTravDivision);
							cashStmt1.getCsTravIdList().add(ftdId);
							
							actEmp.getFtdList().add(faTravDivision);
							session.update(actEmp);
							session.update(cashStmt1);
							//transaction.commit();
						}
						
						CashStmtTemp cashStmt2 = new CashStmtTemp();
						cashStmt2.setbCode(currentUser.getUserBranchCode());
						cashStmt2.setUserCode(currentUser.getUserCode());
						cashStmt2.setCsDescription(voucherService.getDesc());
						cashStmt2.setCsDrCr('C');
						cashStmt2.setCsAmt(totAmt - tdsAmt);
						cashStmt2.setCsType(voucherService.getVoucherType());
						cashStmt2.setCsVouchType("contra");
						cashStmt2.setCsPayTo(voucherService.getPayTo());
						cashStmt2.setCsTvNo(tvNo);
						cashStmt2.setCsFaCode(brFaCode);
						cashStmt2.setCsBrhFaCode(trBrFaCode);
						//cashStmt2.setCsVouchNo(voucherNo);
						cashStmt2.setCsDt(sqlDate);
						cashStmt2.setPayMode(payMode);
						
						session.save(cashStmt2);
						/*Criteria cr7 = session.createCriteria(CashStmtStatus.class);
						cr7.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
						cssList = cr7.list();
						if(!cssList.isEmpty()){
							CashStmtStatus csStatus = cssList.get(0);
							cashStmt2.setCsNo(csStatus);
							csStatus.getCashStmtList().add(cashStmt2);
							session.update(csStatus);
							//transaction.commit();
						}*/
						
						
						
						CashStmtTemp cashStmt3 = new CashStmtTemp();
						cashStmt3.setbCode(currentUser.getUserBranchCode());
						cashStmt3.setUserCode(currentUser.getUserCode());
						cashStmt3.setCsDescription(voucherService.getDesc());
						cashStmt3.setCsDrCr('D');
						cashStmt3.setCsAmt(totAmt);
						cashStmt3.setCsType(voucherService.getVoucherType());
						cashStmt3.setCsVouchType("contra");
						cashStmt3.setCsPayTo(voucherService.getPayTo());
						cashStmt3.setCsTvNo(tvNo);
						cashStmt3.setCsBrhFaCode(trBrFaCode);
						cashStmt3.setCsFaCode(trFaCode);
						//cashStmt3.setCsVouchNo(voucherNo);
						cashStmt3.setCsDt(sqlDate);
						cashStmt3.setPayMode(payMode);
						session.save(cashStmt3);
						/*Criteria cr8 = session.createCriteria(CashStmtStatus.class);
						cr8.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
						cssList = cr8.list();
						if(!cssList.isEmpty()){
							CashStmtStatus csStatus = cssList.get(0);
							cashStmt3.setCsNo(csStatus);
							csStatus.getCashStmtList().add(cashStmt3);
							session.update(csStatus);
							//transaction.commit();
						}*/
						
						if(tdsCode != null && tdsAmt > 0){
							
							CashStmtTemp csWTds = new CashStmtTemp();
							csWTds.setbCode(currentUser.getUserBranchCode());
							csWTds.setUserCode(currentUser.getUserCode());
							csWTds.setCsDescription(voucherService.getDesc());
							csWTds.setCsDrCr('C');
							csWTds.setCsAmt(tdsAmt);
							csWTds.setCsType(voucherService.getVoucherType());
							csWTds.setCsVouchType("contra");
							csWTds.setCsPayTo(voucherService.getPayTo());
							csWTds.setCsTvNo(tvNo);
							csWTds.setCsFaCode(tdsCode);
							csWTds.setCsBrhFaCode(trBrFaCode);
							//csWTds.setCsVouchNo(voucherNo);
							csWTds.setCsDt(sqlDate);
							csWTds.setPayMode(payMode);
							
							session.save(csWTds);
							/*Criteria cr1 = session.createCriteria(CashStmtStatus.class);
							cr1.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
							cssList = cr1.list();
							
							if(!cssList.isEmpty()){
								CashStmtStatus csStatus = cssList.get(0);
								csWTds.setCsNo(csStatus);
								csStatus.getCashStmtList().add(csWTds);
								System.out.println("tds case successfully saved");
								session.update(csStatus);
								//transaction.commit();
							}	*/
						}

					}
				}
				map.put("vhNo",voucherNo);
				transaction.commit();
				session.flush();
			}catch(Exception e){
				e.printStackTrace();
			}
			session.clear();
			session.close();
		}else{
			System.out.println("tdSerList is empty");
			map.put("vhNo",0);
		}
		return map;
	}
	
	
	
	@Transactional
	@SuppressWarnings("unchecked")
	@Override
	public Map<String,Object> saveTrvlVouchByChq(VoucherService voucherService , TravVoucherService travVoucherService){
		System.out.println("enter into saveTrvlVouchByChq function");
		Map<String,Object> map = new HashMap<String,Object>();
		User currentUser = (User) httpSession.getAttribute("currentUser");
		String brFaCode = voucherService.getBranch().getBranchFaCode();
		CashStmtStatus cashStmtStatus = voucherService.getCashStmtStatus();
		//java.sql.Date sqlDate = new java.sql.Date(new Date().getTime());
		List<TravDivService> tdSerList = travVoucherService.getAllTDService();
		List<CashStmtStatus> cssList = new ArrayList<CashStmtStatus>();
		List<FAMaster> faMList = new ArrayList<FAMaster>();
		List<BankMstr> bankMList = new ArrayList<BankMstr>();
		char chequeType = voucherService.getChequeType();
		//List<BankMstr> bankMList = bankMstrDAO.getBankByBankCode(voucherService.getBankCode());
		int voucherNo = 0;
		String payMode = "Q";
		if(!tdSerList.isEmpty()){
			
			try{
				session = this.sessionFactory.openSession();
				transaction = session.beginTransaction();
				
				java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
				
				Criteria crr=session.createCriteria(FAMaster.class);
				crr.add(Restrictions.eq(FAMasterCNTS.FA_MFA_NAME,ConstantsValues.TRAV_EXP_FANAME));
				faMList =crr.list();
				String trFaCode = faMList.get(0).getFaMfaCode();
				
				Criteria cr = session.createCriteria(CashStmt.class);
				cr.add(Restrictions.eq(CashStmtCNTS.CS_CDT,sqlDate));
				cr.add(Restrictions.eq(CashStmtCNTS.USER_BRANCH_CODE,currentUser.getUserBranchCode()));
				ProjectionList proList = Projections.projectionList();
				proList.add(Projections.property("csVouchNo"));
				cr.setProjection(proList);
				
				List<Integer>  voucherNoList = cr.list();
				
				if(!voucherNoList.isEmpty()){
					//int vNo = voucherNoList.get(voucherNoList.size() - 1);
					for(int i=0;i<voucherNoList.size();i++){
						if(voucherNo < voucherNoList.get(i)){
							voucherNo = voucherNoList.get(i);
						}
					}
					voucherNo = voucherNo + 1;
				}else{
					voucherNo = 1;
				}
				
				String tdsCode = voucherService.getTdsCode();
				double tdsAmt = voucherService.getTdsAmt();
				/*if(tdsCode != null && tdsAmt > 0){
					Map<String,Object> newMap = new HashMap<String,Object>();
					
					CashStmt csWTds = new CashStmt();
					csWTds.setbCode(currentUser.getUserBranchCode());
					csWTds.setUserCode(currentUser.getUserCode());
					csWTds.setCsDescription(voucherService.getDesc());
					csWTds.setCsDrCr('C');
					csWTds.setCsAmt(tdsAmt);
					csWTds.setCsType(voucherService.getVoucherType());
					csWTds.setCsVouchType("bank");
					csWTds.setCsPayTo(voucherService.getPayTo());
					csWTds.setCsTvNo(ConstantsValues.CONST_TVNO);
					csWTds.setCsFaCode(tdsCode);
					csWTds.setCsVouchNo(voucherNo);
					csWTds.setCsDt(sqlDate);
					
					Criteria cr1 = session.createCriteria(CashStmtStatus.class);
					cr1.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
					cssList = cr1.list();
					
					if(!cssList.isEmpty()){
						CashStmtStatus csStatus = cssList.get(0);
						csWTds.setCsNo(csStatus);
						csStatus.getCashStmtList().add(csWTds);
						session.update(csStatus);
						//transaction.commit();
					}	
				}
				*/
				List<ChequeLeaves> chqList = new ArrayList<ChequeLeaves>();
				ChequeLeaves chequeLeaves = voucherService.getChequeLeaves();
				System.out.println("chequeLeaves id = "+chequeLeaves.getChqLId());
				Criteria crr1 = session.createCriteria(ChequeLeaves.class);
				crr1.add(Restrictions.eq(ChequeLeavesCNTS.CHQ_L_ID,chequeLeaves.getChqLId()));
				chqList = crr1.list();
				System.out.println("size of chqList = "+chqList.size());
				ChequeLeaves chql = chqList.get(0);
				double totAmtFQ = 0.0;
				
				for(int i=0;i<tdSerList.size();i++){
					totAmtFQ = totAmtFQ + tdSerList.get(i).getTotAmt();
				}
				
				chql.setChqLChqAmt(totAmtFQ);
				chql.setChqLUsedDt(new Date(new java.util.Date().getTime()));
				chql.setChqLUsed(true);
				String tvNo = chql.getChqLChqNo();
				
				session.update(chql);
				
				BankMstr bank = chql.getBankMstr();
				double balAmt = bank.getBnkBalanceAmt();
				double newBalAmt = balAmt - chequeLeaves.getChqLChqAmt();
				bank.setBnkBalanceAmt(newBalAmt);
				session.merge(bank);
				
				// InterBranch Case Main Cash Stmt Start
				double amount = 0.0;
				int count = 0;
				for(int i=0;i<tdSerList.size();i++){

					TravDivService travDivService = tdSerList.get(i);
					Employee employee = travDivService.getEmployee();
					List<FaTravDivision> ftdList = travDivService.getFtdList();
						
					List<Branch> brList = new ArrayList<Branch>();
					int brId = Integer.parseInt(employee.getBranchCode());
					Criteria crr2 = session.createCriteria(Branch.class);
					crr2.add(Restrictions.eq(BranchCNTS.BRANCH_ID,brId));
					brList = crr2.list();
					
					String trBrFaCode = brList.get(0).getBranchFaCode();
					amount = amount + travDivService.getTotAmt();
					/*if(brFaCode.equalsIgnoreCase(trBrFaCode)){
						System.out.println("************** "+i);
					}else{
						amount = amount + travDivService.getTotAmt();
						count = count + 1;
					}	*/
				}
				
				System.out.println(count+" EmpCode are from other branches--");
				CashStmt mainCashStmt = new CashStmt();
				
				//if(count > 0){
					
					mainCashStmt.setbCode(currentUser.getUserBranchCode());
					mainCashStmt.setUserCode(currentUser.getUserCode());
					mainCashStmt.setCsDescription(voucherService.getDesc());
					mainCashStmt.setCsDrCr('C');
					mainCashStmt.setCsAmt(amount - tdsAmt);
					mainCashStmt.setCsType(voucherService.getVoucherType());
					mainCashStmt.setCsVouchType("bank");
					mainCashStmt.setCsChequeType(chequeType);
					mainCashStmt.setCsPayTo(voucherService.getPayTo());
					mainCashStmt.setCsFaCode(voucherService.getBankCode());
					mainCashStmt.setCsTvNo(tvNo);
					mainCashStmt.setCsDt(sqlDate);
					mainCashStmt.setCsVouchNo(voucherNo);
					mainCashStmt.setPayMode(payMode);
					
					Criteria crr3 = session.createCriteria(CashStmtStatus.class);
					crr3.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
					cssList = crr3.list();
					if(!cssList.isEmpty()){
						CashStmtStatus csStatus = cssList.get(0);
						mainCashStmt.setCsNo(csStatus);
						csStatus.getCashStmtList().add(mainCashStmt);
						session.update(csStatus);
						//transaction.commit();
					}
				//}
				
				// InterBranch Case Main Cash Stmt End
				
				

				for(int i=0 ; i<tdSerList.size() ; i++){
					
					TravDivService travDivService = tdSerList.get(i);
					Employee employee = travDivService.getEmployee();
					double totAmt = travDivService.getTotAmt();
					List<FaTravDivision> ftdList = travDivService.getFtdList();
					
					List<Branch> brList = new ArrayList<Branch>();
					int brId = Integer.parseInt(employee.getBranchCode());
					Criteria cr2 = session.createCriteria(Branch.class);
					cr2.add(Restrictions.eq(BranchCNTS.BRANCH_ID,brId));
					brList = cr2.list();
					
					String trBrFaCode = brList.get(0).getBranchFaCode();
					/*String tvNo = employee.getEmpFaCode();
					int count = tvNo.length();
					if(count < 13){
						count = 13 - count;
						for(int j=0;j<count;j++){
							tvNo = "0" + tvNo;
						}
					}*/
					
					
					if(brFaCode.equalsIgnoreCase(trBrFaCode)){
						
						CashStmt cashStmt = new CashStmt();
						cashStmt.setbCode(currentUser.getUserBranchCode());
						cashStmt.setUserCode(currentUser.getUserCode());
						cashStmt.setCsDescription(voucherService.getDesc());
						cashStmt.setCsDrCr('D');
						cashStmt.setCsAmt(totAmt);
						cashStmt.setCsType(voucherService.getVoucherType());
						cashStmt.setCsVouchType("bank");
						cashStmt.setCsPayTo(voucherService.getPayTo());
						cashStmt.setCsTvNo(tvNo);
						cashStmt.setCsFaCode(trFaCode);
						cashStmt.setCsVouchNo(voucherNo);
						cashStmt.setCsDt(sqlDate);
						cashStmt.setPayMode(payMode);
						
						Criteria cr3 = session.createCriteria(CashStmtStatus.class);
						cr3.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
						cssList = cr3.list();
						
						int csId = 0;
						if(!cssList.isEmpty()){
							CashStmtStatus csStatus = cssList.get(0);
							cashStmt.setCsNo(csStatus);
							csId = (Integer) session.save(cashStmt);
							csStatus.getCashStmtList().add(cashStmt);
							session.update(csStatus);
							//transaction.commit();
						}	
						
						for(int k=0;k<ftdList.size();k++){
							
							List<Employee> empList = new ArrayList<Employee>();
							Criteria cr4 = session.createCriteria(Employee.class);
							cr4.add(Restrictions.eq(EmployeeCNTS.EMP_ID,employee.getEmpId()));
							empList = cr4.list();
							Employee actEmp = empList.get(0);
							
							FaTravDivision faTravDivision = ftdList.get(k);
							faTravDivision.setbCode(currentUser.getUserBranchCode());
							faTravDivision.setUserCode(currentUser.getUserCode());
							faTravDivision.setFtdCsId(csId);
							
							faTravDivision.setEmployee(actEmp);
							int ftdId = (Integer) session.save(faTravDivision);
							cashStmt.getCsTravIdList().add(ftdId);
							
							actEmp.getFtdList().add(faTravDivision);
							
							session.update(actEmp);
							session.update(cashStmt);
							//transaction.commit();
						}
						
						
						/*CashStmt cashStmt1 = new CashStmt();
						cashStmt1.setbCode(currentUser.getUserBranchCode());
						cashStmt1.setUserCode(currentUser.getUserCode());
						cashStmt1.setCsDescription(voucherService.getDesc());
						cashStmt1.setCsDrCr('C');
						cashStmt1.setCsAmt(totAmt);
						cashStmt1.setCsType(voucherService.getVoucherType());
						cashStmt1.setCsVouchType("bank");
						cashStmt1.setCsChequeType(chequeType);
						cashStmt1.setCsPayTo(voucherService.getPayTo());
						cashStmt1.setCsTvNo(tvNo);
						cashStmt1.setCsFaCode(voucherService.getBankCode());
						cashStmt1.setCsVouchNo(voucherNo);
						cashStmt1.setCsDt(sqlDate);
		
						Criteria cr33 = session.createCriteria(CashStmtStatus.class);
						cr33.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
						cssList = cr33.list();
						if(!cssList.isEmpty()){
							CashStmtStatus csStatus = cssList.get(0);
							cashStmt1.setCsNo(csStatus);
							csStatus.getCashStmtList().add(cashStmt1);
							session.update(csStatus);
							//transaction.commit();
						}*/
						
						if(tdsCode != null && tdsAmt > 0){
							Map<String,Object> newMap = new HashMap<String,Object>();
							
							CashStmt csWTds = new CashStmt();
							csWTds.setbCode(currentUser.getUserBranchCode());
							csWTds.setUserCode(currentUser.getUserCode());
							csWTds.setCsDescription(voucherService.getDesc());
							csWTds.setCsDrCr('C');
							csWTds.setCsAmt(tdsAmt);
							csWTds.setCsType(voucherService.getVoucherType());
							csWTds.setCsVouchType("bank");
							csWTds.setCsPayTo(voucherService.getPayTo());
							csWTds.setCsTvNo(tvNo);
							csWTds.setCsFaCode(tdsCode);
							csWTds.setCsVouchNo(voucherNo);
							csWTds.setCsDt(sqlDate);
							csWTds.setPayMode(payMode);
							
							Criteria cr1 = session.createCriteria(CashStmtStatus.class);
							cr1.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
							cssList = cr1.list();
							
							if(!cssList.isEmpty()){
								CashStmtStatus csStatus = cssList.get(0);
								csWTds.setCsNo(csStatus);
								csStatus.getCashStmtList().add(csWTds);
								session.update(csStatus);
								//transaction.commit();
							}	
						}
						
						
					}else{
						
						CashStmt cashStmt1 = new CashStmt();
						cashStmt1.setbCode(currentUser.getUserBranchCode());
						cashStmt1.setUserCode(currentUser.getUserCode());
						cashStmt1.setCsDescription(voucherService.getDesc());
						cashStmt1.setCsDrCr('D');
						cashStmt1.setCsAmt(totAmt - tdsAmt);
						cashStmt1.setCsType(voucherService.getVoucherType());
						cashStmt1.setCsVouchType("bank");
						cashStmt1.setCsTvNo(tvNo);
						cashStmt1.setCsPayTo(voucherService.getPayTo());
						cashStmt1.setCsFaCode(trBrFaCode);
						cashStmt1.setCsVouchNo(voucherNo);
						cashStmt1.setCsDt(sqlDate);
						cashStmt1.setPayMode(payMode);
						
						Criteria cr5 = session.createCriteria(CashStmtStatus.class);
						cr5.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
						cssList = cr5.list();
						
						int csId = 0;
						if(!cssList.isEmpty()){
							CashStmtStatus csStatus = cssList.get(0);
							cashStmt1.setCsNo(csStatus);
							csId = (Integer) session.save(cashStmt1);
							csStatus.getCashStmtList().add(cashStmt1);
							session.update(csStatus);
							//transaction.commit();
						}
						
						for(int k=0;k<ftdList.size();k++){
							List<Employee> empList = new ArrayList<Employee>();
							Criteria cr6 = session.createCriteria(Employee.class);
							cr6.add(Restrictions.eq(EmployeeCNTS.EMP_ID,employee.getEmpId()));
							empList = cr6.list();
							Employee actEmp = empList.get(0);
							
							FaTravDivision faTravDivision = ftdList.get(k);
							faTravDivision.setbCode(currentUser.getUserBranchCode());
							faTravDivision.setUserCode(currentUser.getUserCode());
							faTravDivision.setFtdCsId(csId);
							
							faTravDivision.setEmployee(actEmp);
							int ftdId = (Integer) session.save(faTravDivision);
							cashStmt1.getCsTravIdList().add(ftdId);
							
							actEmp.getFtdList().add(faTravDivision);
							
							session.update(actEmp);
							session.update(cashStmt1);
							//transaction.commit();
						}
						
						CashStmtTemp cashStmt2 = new CashStmtTemp();
						cashStmt2.setbCode(currentUser.getUserBranchCode());
						cashStmt2.setUserCode(currentUser.getUserCode());
						cashStmt2.setCsDescription(voucherService.getDesc());
						cashStmt2.setCsDrCr('C');
						cashStmt2.setCsAmt(totAmt);
						cashStmt2.setCsType(voucherService.getVoucherType());
						cashStmt2.setCsVouchType("contra");
						cashStmt2.setCsPayTo(voucherService.getPayTo());
						cashStmt2.setCsTvNo(tvNo);
						cashStmt2.setCsFaCode(brFaCode);
						cashStmt2.setCsBrhFaCode(trBrFaCode);
						//cashStmt2.setCsVouchNo(voucherNo);
						cashStmt2.setCsDt(sqlDate);
						cashStmt2.setPayMode(payMode);
						
						session.save(cashStmt2);
						/*Criteria cr7 = session.createCriteria(CashStmtStatus.class);
						cr7.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
						cssList = cr7.list();
						if(!cssList.isEmpty()){
							CashStmtStatus csStatus = cssList.get(0);
							cashStmt2.setCsNo(csStatus);
							csStatus.getCashStmtList().add(cashStmt2);
							session.update(csStatus);
							//transaction.commit();
						}*/
						
						
						
						CashStmtTemp cashStmt3 = new CashStmtTemp();
						cashStmt3.setbCode(currentUser.getUserBranchCode());
						cashStmt3.setUserCode(currentUser.getUserCode());
						cashStmt3.setCsDescription(voucherService.getDesc());
						cashStmt3.setCsDrCr('D');
						cashStmt3.setCsAmt(totAmt);
						cashStmt3.setCsType(voucherService.getVoucherType());
						cashStmt3.setCsVouchType("contra");
						cashStmt3.setCsPayTo(voucherService.getPayTo());
						cashStmt3.setCsTvNo(tvNo);
						cashStmt3.setCsFaCode(trFaCode);
						cashStmt3.setCsBrhFaCode(trBrFaCode);
						//cashStmt3.setCsVouchNo(voucherNo);
						cashStmt3.setCsDt(sqlDate);
						cashStmt3.setPayMode(payMode);
						
						session.save(cashStmt3);
						/*Criteria cr8 = session.createCriteria(CashStmtStatus.class);
						cr8.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
						cssList = cr8.list();
						if(!cssList.isEmpty()){
							CashStmtStatus csStatus = cssList.get(0);
							cashStmt3.setCsNo(csStatus);
							csStatus.getCashStmtList().add(cashStmt3);
							session.update(csStatus);
							//transaction.commit();
						}*/
						
						if(tdsCode != null && tdsAmt > 0){	
							CashStmtTemp csWTds = new CashStmtTemp();
							csWTds.setbCode(currentUser.getUserBranchCode());
							csWTds.setUserCode(currentUser.getUserCode());
							csWTds.setCsDescription(voucherService.getDesc());
							csWTds.setCsDrCr('C');
							csWTds.setCsAmt(tdsAmt);
							csWTds.setCsType(voucherService.getVoucherType());
							csWTds.setCsVouchType("contra");
							csWTds.setCsPayTo(voucherService.getPayTo());
							csWTds.setCsTvNo(tvNo);
							csWTds.setCsFaCode(tdsCode);
							csWTds.setCsBrhFaCode(trBrFaCode);
							//csWTds.setCsVouchNo(voucherNo);
							csWTds.setCsDt(sqlDate);
							csWTds.setPayMode(payMode);
							session.save(csWTds);
							/*Criteria cr1 = session.createCriteria(CashStmtStatus.class);
							cr1.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
							cssList = cr1.list();
							
							if(!cssList.isEmpty()){
								CashStmtStatus csStatus = cssList.get(0);
								csWTds.setCsNo(csStatus);
								csStatus.getCashStmtList().add(csWTds);
								session.update(csStatus);
								//transaction.commit();
							}	*/
						}
					}
				}
				map.put("vhNo",voucherNo);
				transaction.commit();
				session.flush();
			}catch(Exception e){
				e.printStackTrace();
			}
			session.clear();
			session.close();
		}else{
			System.out.println("tdSerList is empty");
			map.put("vhNo",0);
		}
		
		return map;

	}
	
	
	
	
	@Transactional
	@SuppressWarnings("unchecked")
	public Map<String,Object> saveTrvlVouchByOnline(VoucherService voucherService , TravVoucherService travVoucherService){
		System.out.println("enter into saveTrvlVouchByChq function");
		Map<String,Object> map = new HashMap<String,Object>();
		User currentUser = (User) httpSession.getAttribute("currentUser");
		String brFaCode = voucherService.getBranch().getBranchFaCode();
		CashStmtStatus cashStmtStatus = voucherService.getCashStmtStatus();
		List<TravDivService> tdSerList = travVoucherService.getAllTDService();
		List<CashStmtStatus> cssList = new ArrayList<CashStmtStatus>();
		List<FAMaster> faMList = new ArrayList<FAMaster>();
		char chequeType = voucherService.getChequeType();
		int voucherNo = 0;
		String payMode = "O";
		Date date = new Date(new java.util.Date().getTime());
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		System.out.println("unique id =====>" + calendar.getTimeInMillis());
		long milliSec = calendar.getTimeInMillis();
		String tvNo = String.valueOf(milliSec);
		
		if(!tdSerList.isEmpty()){
			
			try{
				session = this.sessionFactory.openSession();
				transaction = session.beginTransaction();
				
				
				CashStmtStatus actCashSS = (CashStmtStatus) session.get(CashStmtStatus.class,cashStmtStatus.getCssId());
				
				java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus.getCssDt().getTime());
				
				Criteria crr=session.createCriteria(FAMaster.class);
				crr.add(Restrictions.eq(FAMasterCNTS.FA_MFA_NAME,ConstantsValues.TRAV_EXP_FANAME));
				faMList =crr.list();
				String trFaCode = faMList.get(0).getFaMfaCode();
								
				List<CashStmt> csList = actCashSS.getCashStmtList();
				if(!csList.isEmpty()){
					//int vNo  = csList.get(csList.size() - 1).getCsVouchNo();
					for(int i=0;i<csList.size();i++){
						if(voucherNo < csList.get(i).getCsVouchNo()){
							voucherNo = csList.get(i).getCsVouchNo();
						}
					}
					voucherNo = voucherNo + 1;
				}else{
					voucherNo = 1;
				}
				
				String tdsCode = voucherService.getTdsCode();
				double tdsAmt = voucherService.getTdsAmt();

				double totAmtFQ = 0.0;
				
				for(int i=0;i<tdSerList.size();i++){
					totAmtFQ = totAmtFQ + tdSerList.get(i).getTotAmt();
				}
				
				List<BankMstr> bnkList = new ArrayList<>();
				crr = session.createCriteria(BankMstr.class);
				crr.add(Restrictions.eq(BankMstrCNTS.BNK_FA_CODE,voucherService.getBankCode()));
				bnkList = crr.list();
				
				if(!bnkList.isEmpty()){
					BankMstr bank = bnkList.get(0);
					double balAmt = bank.getBnkBalanceAmt();
					double newBalAmt = balAmt - totAmtFQ;
					bank.setBnkBalanceAmt(newBalAmt);
					session.merge(bank);
				}
				
				
				// InterBranch Case Main Cash Stmt Start
				double amount = 0.0;
				int count = 0;
				for(int i=0;i<tdSerList.size();i++){

					TravDivService travDivService = tdSerList.get(i);
					Employee employee = travDivService.getEmployee();
					List<FaTravDivision> ftdList = travDivService.getFtdList();
						
					List<Branch> brList = new ArrayList<Branch>();
					int brId = Integer.parseInt(employee.getBranchCode());
					Criteria crr2 = session.createCriteria(Branch.class);
					crr2.add(Restrictions.eq(BranchCNTS.BRANCH_ID,brId));
					brList = crr2.list();
					
					String trBrFaCode = brList.get(0).getBranchFaCode();
					amount = amount + travDivService.getTotAmt();
					
				}
				
				System.out.println(count+" EmpCode are from other branches--");
				CashStmt mainCashStmt = new CashStmt();
				
					
					mainCashStmt.setbCode(currentUser.getUserBranchCode());
					mainCashStmt.setUserCode(currentUser.getUserCode());
					mainCashStmt.setCsDescription(voucherService.getDesc());
					mainCashStmt.setCsDrCr('C');
					mainCashStmt.setCsAmt(amount - tdsAmt);
					mainCashStmt.setCsType(voucherService.getVoucherType());
					mainCashStmt.setCsVouchType("bank");
					mainCashStmt.setCsChequeType(chequeType);
					mainCashStmt.setCsPayTo(voucherService.getPayTo());
					mainCashStmt.setCsFaCode(voucherService.getBankCode());
					mainCashStmt.setCsTvNo(tvNo);
					mainCashStmt.setCsDt(sqlDate);
					mainCashStmt.setCsVouchNo(voucherNo);
					mainCashStmt.setPayMode(payMode);
					
					Criteria crr3 = session.createCriteria(CashStmtStatus.class);
					crr3.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
					cssList = crr3.list();
					if(!cssList.isEmpty()){
						CashStmtStatus csStatus = cssList.get(0);
						mainCashStmt.setCsNo(csStatus);
						csStatus.getCashStmtList().add(mainCashStmt);
						session.update(csStatus);
						
					}
				
				// InterBranch Case Main Cash Stmt End
				
				

				for(int i=0 ; i<tdSerList.size() ; i++){
					
					TravDivService travDivService = tdSerList.get(i);
					Employee employee = travDivService.getEmployee();
					double totAmt = travDivService.getTotAmt();
					List<FaTravDivision> ftdList = travDivService.getFtdList();
					
					List<Branch> brList = new ArrayList<Branch>();
					int brId = Integer.parseInt(employee.getBranchCode());
					Criteria cr2 = session.createCriteria(Branch.class);
					cr2.add(Restrictions.eq(BranchCNTS.BRANCH_ID,brId));
					brList = cr2.list();
					
					String trBrFaCode = brList.get(0).getBranchFaCode();
					
					
					if(brFaCode.equalsIgnoreCase(trBrFaCode)){
						
						CashStmt cashStmt = new CashStmt();
						cashStmt.setbCode(currentUser.getUserBranchCode());
						cashStmt.setUserCode(currentUser.getUserCode());
						cashStmt.setCsDescription(voucherService.getDesc());
						cashStmt.setCsDrCr('D');
						cashStmt.setCsAmt(totAmt);
						cashStmt.setCsType(voucherService.getVoucherType());
						cashStmt.setCsVouchType("bank");
						cashStmt.setCsPayTo(voucherService.getPayTo());
						cashStmt.setCsTvNo(tvNo);
						cashStmt.setCsFaCode(trFaCode);
						cashStmt.setCsVouchNo(voucherNo);
						cashStmt.setCsDt(sqlDate);
						cashStmt.setPayMode(payMode);
						
						Criteria cr3 = session.createCriteria(CashStmtStatus.class);
						cr3.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
						cssList = cr3.list();
						
						int csId = 0;
						if(!cssList.isEmpty()){
							CashStmtStatus csStatus = cssList.get(0);
							cashStmt.setCsNo(csStatus);
							csId = (Integer) session.save(cashStmt);
							csStatus.getCashStmtList().add(cashStmt);
							session.update(csStatus);
						}	
						
						for(int k=0;k<ftdList.size();k++){
							
							List<Employee> empList = new ArrayList<Employee>();
							Criteria cr4 = session.createCriteria(Employee.class);
							cr4.add(Restrictions.eq(EmployeeCNTS.EMP_ID,employee.getEmpId()));
							empList = cr4.list();
							Employee actEmp = empList.get(0);
							
							FaTravDivision faTravDivision = ftdList.get(k);
							faTravDivision.setbCode(currentUser.getUserBranchCode());
							faTravDivision.setUserCode(currentUser.getUserCode());
							faTravDivision.setFtdCsId(csId);
							
							faTravDivision.setEmployee(actEmp);
							int ftdId = (Integer) session.save(faTravDivision);
							cashStmt.getCsTravIdList().add(ftdId);
							
							actEmp.getFtdList().add(faTravDivision);
							
							session.update(actEmp);
							session.update(cashStmt);
							
						}
						
						
						if(tdsCode != null && tdsAmt > 0){
							Map<String,Object> newMap = new HashMap<String,Object>();
							
							CashStmt csWTds = new CashStmt();
							csWTds.setbCode(currentUser.getUserBranchCode());
							csWTds.setUserCode(currentUser.getUserCode());
							csWTds.setCsDescription(voucherService.getDesc());
							csWTds.setCsDrCr('C');
							csWTds.setCsAmt(tdsAmt);
							csWTds.setCsType(voucherService.getVoucherType());
							csWTds.setCsVouchType("bank");
							csWTds.setCsPayTo(voucherService.getPayTo());
							csWTds.setCsTvNo(tvNo);
							csWTds.setCsFaCode(tdsCode);
							csWTds.setCsVouchNo(voucherNo);
							csWTds.setCsDt(sqlDate);
							csWTds.setPayMode(payMode);
							
							Criteria cr1 = session.createCriteria(CashStmtStatus.class);
							cr1.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
							cssList = cr1.list();
							
							if(!cssList.isEmpty()){
								CashStmtStatus csStatus = cssList.get(0);
								csWTds.setCsNo(csStatus);
								csStatus.getCashStmtList().add(csWTds);
								session.update(csStatus);
							}	
						}
						
						
					}else{
						
						CashStmt cashStmt1 = new CashStmt();
						cashStmt1.setbCode(currentUser.getUserBranchCode());
						cashStmt1.setUserCode(currentUser.getUserCode());
						cashStmt1.setCsDescription(voucherService.getDesc());
						cashStmt1.setCsDrCr('D');
						cashStmt1.setCsAmt(totAmt - tdsAmt);
						cashStmt1.setCsType(voucherService.getVoucherType());
						cashStmt1.setCsVouchType("bank");
						cashStmt1.setCsTvNo(tvNo);
						cashStmt1.setCsPayTo(voucherService.getPayTo());
						cashStmt1.setCsFaCode(trBrFaCode);
						cashStmt1.setCsVouchNo(voucherNo);
						cashStmt1.setCsDt(sqlDate);
						cashStmt1.setPayMode(payMode);
						
						Criteria cr5 = session.createCriteria(CashStmtStatus.class);
						cr5.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
						cssList = cr5.list();
						
						int csId = 0;
						if(!cssList.isEmpty()){
							CashStmtStatus csStatus = cssList.get(0);
							cashStmt1.setCsNo(csStatus);
							csId = (Integer) session.save(cashStmt1);
							csStatus.getCashStmtList().add(cashStmt1);
							session.update(csStatus);
						}
						
						for(int k=0;k<ftdList.size();k++){
							List<Employee> empList = new ArrayList<Employee>();
							Criteria cr6 = session.createCriteria(Employee.class);
							cr6.add(Restrictions.eq(EmployeeCNTS.EMP_ID,employee.getEmpId()));
							empList = cr6.list();
							Employee actEmp = empList.get(0);
							
							FaTravDivision faTravDivision = ftdList.get(k);
							faTravDivision.setbCode(currentUser.getUserBranchCode());
							faTravDivision.setUserCode(currentUser.getUserCode());
							faTravDivision.setFtdCsId(csId);
							
							faTravDivision.setEmployee(actEmp);
							int ftdId = (Integer) session.save(faTravDivision);
							cashStmt1.getCsTravIdList().add(ftdId);
							
							actEmp.getFtdList().add(faTravDivision);
							
							session.update(actEmp);
							session.update(cashStmt1);
						}
						
						CashStmtTemp cashStmt2 = new CashStmtTemp();
						cashStmt2.setbCode(currentUser.getUserBranchCode());
						cashStmt2.setUserCode(currentUser.getUserCode());
						cashStmt2.setCsDescription(voucherService.getDesc());
						cashStmt2.setCsDrCr('C');
						cashStmt2.setCsAmt(totAmt);
						cashStmt2.setCsType(voucherService.getVoucherType());
						cashStmt2.setCsVouchType("contra");
						cashStmt2.setCsPayTo(voucherService.getPayTo());
						cashStmt2.setCsTvNo(tvNo);
						cashStmt2.setCsFaCode(brFaCode);
						cashStmt2.setCsBrhFaCode(trBrFaCode);
						cashStmt2.setCsDt(sqlDate);
						cashStmt2.setPayMode(payMode);
						
						session.save(cashStmt2);
						
						CashStmtTemp cashStmt3 = new CashStmtTemp();
						cashStmt3.setbCode(currentUser.getUserBranchCode());
						cashStmt3.setUserCode(currentUser.getUserCode());
						cashStmt3.setCsDescription(voucherService.getDesc());
						cashStmt3.setCsDrCr('D');
						cashStmt3.setCsAmt(totAmt);
						cashStmt3.setCsType(voucherService.getVoucherType());
						cashStmt3.setCsVouchType("contra");
						cashStmt3.setCsPayTo(voucherService.getPayTo());
						cashStmt3.setCsTvNo(tvNo);
						cashStmt3.setCsFaCode(trFaCode);
						cashStmt3.setCsBrhFaCode(trBrFaCode);
						cashStmt3.setCsDt(sqlDate);
						cashStmt3.setPayMode(payMode);
						
						session.save(cashStmt3);
						
						if(tdsCode != null && tdsAmt > 0){	
							CashStmtTemp csWTds = new CashStmtTemp();
							csWTds.setbCode(currentUser.getUserBranchCode());
							csWTds.setUserCode(currentUser.getUserCode());
							csWTds.setCsDescription(voucherService.getDesc());
							csWTds.setCsDrCr('C');
							csWTds.setCsAmt(tdsAmt);
							csWTds.setCsType(voucherService.getVoucherType());
							csWTds.setCsVouchType("contra");
							csWTds.setCsPayTo(voucherService.getPayTo());
							csWTds.setCsTvNo(tvNo);
							csWTds.setCsFaCode(tdsCode);
							csWTds.setCsBrhFaCode(trBrFaCode);
							csWTds.setCsDt(sqlDate);
							csWTds.setPayMode(payMode);
							
							session.save(csWTds);
						}
					}
				}
				map.put("vhNo",voucherNo);
				transaction.commit();
				session.flush();
			}catch(Exception e){
				e.printStackTrace();
			}
			session.clear();
			session.close();
		}else{
			System.out.println("tdSerList is empty");
			map.put("vhNo",0);
		}
		
		return map;

	}
	
	
	
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<TravDivService> getSubFileDetails(List<CashStmt> reqCsList){
		System.out.println("enter into getSubFileDetails function");
		List<TravDivService> tDetList = new ArrayList<TravDivService>();
		if(!reqCsList.isEmpty()){
			try{
				session = this.sessionFactory.openSession();
				transaction = session.beginTransaction();
				for(int i=0;i<reqCsList.size();i++){
					List<Integer> csTravIdList = new ArrayList<Integer>();
					csTravIdList = reqCsList.get(i).getCsTravIdList();
					if(!csTravIdList.isEmpty()){
						List<FaTravDivision> actList = new ArrayList<FaTravDivision>();
						for(int j=0;j<csTravIdList.size();j++){
							List<FaTravDivision> tempList = new ArrayList<FaTravDivision>();
							Criteria cr = session.createCriteria(FaTravDivision.class);
							cr.add(Restrictions.eq(FaTravDivisionCNTS.FTD_ID,csTravIdList.get(j)));
							tempList = cr.list(); 
							if(!tempList.isEmpty()){
								actList.add(tempList.get(0));
							}
						}
						
						if(!actList.isEmpty()){
							double totAmt = 0.0;
							
							for(int k=0;k<actList.size();k++){
								totAmt = totAmt + actList.get(k).getFtdTravAmt();
							}
							
							TravDivService travDivService = new TravDivService();
							travDivService.setTotAmt(totAmt);
							travDivService.setFtdList(actList);
							travDivService.setEmployee(actList.get(0).getEmployee());
							
							tDetList.add(travDivService);
						}
					}
				} 
				transaction.commit();
				session.flush();
			}catch(Exception e){
				e.printStackTrace();
			}
			session.clear();
			session.close();
		}else{
			System.out.println("reqCsList is empty");
		}
		return tDetList;
	}
}
