'use strict';

var app = angular.module('application');

app.controller('LhpvBalPayCntlr',['$scope','$location','$http','$filter','$window',
                                  function($scope,$location,$http,$filter,$window){

	$scope.vs = {};
	$scope.lhpvBal = {};
//	$scope.bnkList = [];
	$scope.chlnList = [];
	$scope.actBOList = [];
	$scope.lhpvBalList = [];
	
	$scope.lhpvBalBankDBFlag=true;
	$scope.lhpvBalCardDBFlag=true;
	$scope.bankCodeDBFlag = true;
//	$scope.chqNoDBFlag = true;
	$scope.brkOwnDBFlag = true;
	$scope.chlnDBFlag = true;
	$scope.lhpvBalDBFlag = true;
	$scope.saveVsFlag = true;
	$scope.vs.cardCode="";
	
	$scope.arHoAlw = false;
	
	$scope.tdsPerFlag = false;
	$scope.lodingFlag = false;
	$scope.cardButtonDB = true;
	$scope.bankButtonDB = true;
	$scope.cardDetailDB=true;
	$scope.bankDetailDB=true;

	//$scope.Math = window.Math;
	
	var faCodeTemp = '';
	
	
	

	 $scope.selectDetailType = function(){
		if($scope.detailType==="bank"){
			$scope.bankDetailDB=false;
			$scope.cardDetailDB=true;
			$scope.cardButtonDB = true;
			$scope.bankButtonDB = false;
			$scope.vs.cardCode="";
		}else if($scope.detailType==="card"){
			$scope.bankDetailDB=true;
			$scope.cardDetailDB=false;
			$scope.cardButtonDB = false;
			$scope.bankButtonDB = true;
		}
		 
	 }
	
	
	
	
	$scope.getLhpvDet = function(){
		console.log("enter into getLhpvDet function");
		var response = $http.post($scope.projectName+'/getLhpvBalDet');
		  response.success(function(data, status, headers, config){
			   if(data.result === "success"){
				   $scope.vs.branch = data.branch;
				   $scope.vs.lhpvStatus = data.lhpvStatus;
				   $scope.vs.voucherType = $scope.LHPV_BAL_VOUCHER;
				   $scope.dateTemp = $filter('date')(data.lhpvStatus.lsDt, "yyyy-MM-dd'");
				   $scope.bnkList = data.bnkList;
				   
				   if($scope.vs.lhpvStatus.lsClose === true){
					   $scope.alertToast("You already close the LHPV of "+$scope.dateTemp);
					   $('#verfChln').attr("disabled","disabled");
				   }else{
				   }
			   }else{
				   console.log("error in fetching getLhpvDet data");
				   $scope.alertToast("Server Error");
			   }
		   });
		   response.error(function(data, status, headers, config) {
				$scope.errorToast(data.result);
		   });
	}
	
	
	
	
	
	 $scope.saveChln = function(chlnCode){
		 console.log("enter into saveChln fucniton");
		 $scope.challan = chlnCode;
		 
		 var duplicate = false;
		 if($scope.lhpvBalList.length > 0){
			 for(var i=0;i<$scope.lhpvBalList.length;i++){
				if($scope.lhpvBalList[i].challan.chlnCode === chlnCode){
					duplicate = true;
					break;
				}
			 }
		 }
		 
		 if(duplicate === false){
			 var data = {
					 "chlnCode"     : $scope.challan,
					 "brkOwnFaCode" : $scope.vs.brkOwnFaCode
			 };
			 
			 var response = $http.post($scope.projectName+'/getChlnFrLB',data);
				response.success(function(data, status, headers, config){
					if(data.result === "success"){
						
						if ($scope.vs.branch.branchFaCode === '0100023') {
							console.log(data);
							//mohsin
							$scope.actOthExtKmP  = data.exKm;
							$scope.actOthOvrHgtP = data.ovrHgt;
							$scope.actUnpDetP    = data.det;
							$scope.actUnLoadingP = data.unLdg;
							
							console.log("data.arHoAlw === "+data.arHoAlw);
							console.log("data.munsAmt = "+data.munsAmt);
							$scope.actChln = data.chln;
							$scope.lhpvBal.challan = $scope.actChln;
							$scope.lhpvBal.lbLryBalP    = data.chlnBal;
							
							$scope.actBal = data.chlnBal;
							if(data.isCal === true){
								$scope.lhpvBal.lbCashDiscR = data.csDis;
								$scope.lhpvBal.lbMunsR      = data.munsAmt;
								$scope.lhpvBal.lbTdsR       = window.Math.round(data.tdsAmt);
							}else{
								$scope.lhpvBal.lbCashDiscR  = 0;
								$scope.lhpvBal.lbMunsR      = 0;
								$scope.lhpvBal.lbTdsR       = 0;
							}
							
							//lhpv tds calculation
							$scope.tdsPer = data.tdsPer;
							if ($scope.tdsPer == 1) {
								console.log("tdsPer if: "+$scope.tdsPer);
								//$scope.tdsPerFlag = true;
							} else {
								console.log("tdsPer else: "+$scope.tdsPer);
								$scope.tdsPerFlag = false;
							}
							
							$scope.actCashDisR = $scope.lhpvBal.lbCashDiscR;
							$scope.actMunsR = $scope.lhpvBal.lbMunsR;
							$scope.actTdsR = $scope.lhpvBal.lbTdsR;
							
								
								$scope.lhpvBal.lbWtShrtgCR  = 0;
								$scope.lhpvBal.lbDrRcvrWtCR = 0;
								$scope.lhpvBal.lbLateDelCR  = 0;
								$scope.lhpvBal.lbLateAckCR  = 0;
								$scope.lhpvBal.lbOthExtKmP  = 0;
								$scope.lhpvBal.lbOthOvrHgtP = 0;
								$scope.lhpvBal.lbOthPnltyP  = 0;
								$scope.lhpvBal.lbOthMiscP   = 0;
								$scope.lhpvBal.lbUnpDetP    = 0;
								$scope.lhpvBal.lbUnLoadingP = 0;
							
								
								
								$scope.actWtShrtgCR = $scope.lhpvBal.lbWtShrtgCR;
								$scope.actDrRcvrWtCR = $scope.lhpvBal.lbDrRcvrWtCR;
								
								
								$scope.lhpvBal.lbTotPayAmt = $scope.lhpvBal.lbLryBalP + $scope.lhpvBal.lbOthExtKmP + $scope.lhpvBal.lbOthOvrHgtP 
																+ $scope.lhpvBal.lbOthPnltyP + $scope.lhpvBal.lbUnpDetP + $scope.lhpvBal.lbUnLoadingP;
		
								$scope.lhpvBal.lbTotRcvrAmt = $scope.lhpvBal.lbCashDiscR + $scope.lhpvBal.lbMunsR + $scope.lhpvBal.lbTdsR 
																+ $scope.lhpvBal.lbWtShrtgCR + $scope.lhpvBal.lbDrRcvrWtCR + $scope.lhpvBal.lbLateDelCR 
																+ $scope.lhpvBal.lbLateAckCR + $scope.lhpvBal.lbOthMiscP;
		
		
								$scope.lhpvBal.lbFinalTot = $scope.lhpvBal.lbTotPayAmt - $scope.lhpvBal.lbTotRcvrAmt;
								
						} else {
							console.log(data);
							//mohsin
							$scope.actOthExtKmP  = data.exKm;
							$scope.actOthOvrHgtP = data.ovrHgt;
							$scope.actUnpDetP    = data.det;
							$scope.actUnLoadingP = data.unLdg;
							
							console.log("data.arHoAlw === "+data.arHoAlw);
							console.log("data.munsAmt = "+data.munsAmt);
							$scope.actChln = data.chln;
							$scope.lhpvBal.challan = $scope.actChln;
							$scope.lhpvBal.lbLryBalP    = data.chlnBal;
							
							$scope.actBal = data.chlnBal;
							if(data.isCal === true){
								$scope.lhpvBal.lbCashDiscR = data.csDis;
								$scope.lhpvBal.lbMunsR      = data.munsAmt;
								$scope.lhpvBal.lbTdsR       = window.Math.round(data.tdsAmt);
							}else{
								$scope.lhpvBal.lbCashDiscR  = 0;
								$scope.lhpvBal.lbMunsR      = 0;
								$scope.lhpvBal.lbTdsR       = 0;
							}
							
							//lhpv tds calculation
							$scope.tdsPer = data.tdsPer;
							if ($scope.tdsPer == 1) {
								console.log("tdsPer if: "+$scope.tdsPer);
								//$scope.tdsPerFlag = true;
							} else {
								console.log("tdsPer else: "+$scope.tdsPer);
								$scope.tdsPerFlag = false;
							}
							
							$scope.actCashDisR = $scope.lhpvBal.lbCashDiscR;
							$scope.actMunsR = $scope.lhpvBal.lbMunsR;
							$scope.actTdsR = $scope.lhpvBal.lbTdsR;
							
							if(data.arHoAlw === true){
								
								$scope.arHoAlw = true;
								
								if(data.isCal === true){
									$scope.lhpvBal.lbWtShrtgCR  = data.wtShrtg;
									$scope.lhpvBal.lbDrRcvrWtCR = data.drRcvrWt;
									$scope.lhpvBal.lbLateDelCR  = data.ltDel;
									$scope.lhpvBal.lbLateAckCR  = data.ltAck;
									$scope.lhpvBal.lbOthExtKmP  = data.exKm;
									$scope.lhpvBal.lbOthOvrHgtP = data.ovrHgt;
									$scope.lhpvBal.lbOthPnltyP  = data.penalty;
									$scope.lhpvBal.lbOthMiscP   = data.oth;
									$scope.lhpvBal.lbUnpDetP    = data.det;
									$scope.lhpvBal.lbUnLoadingP = data.unLdg;
									
									
								}else{
									$scope.lhpvBal.lbWtShrtgCR  = 0;
									$scope.lhpvBal.lbDrRcvrWtCR = 0;
									$scope.lhpvBal.lbLateDelCR  = 0;
									$scope.lhpvBal.lbLateAckCR  = 0;
									$scope.lhpvBal.lbOthExtKmP  = 0;
									$scope.lhpvBal.lbOthOvrHgtP = 0;
									$scope.lhpvBal.lbOthPnltyP  = 0;
									$scope.lhpvBal.lbOthMiscP   = 0;
									$scope.lhpvBal.lbUnpDetP    = 0;
									$scope.lhpvBal.lbUnLoadingP = 0;
								}
								
								
								$scope.actWtShrtgCR = $scope.lhpvBal.lbWtShrtgCR;
								$scope.actDrRcvrWtCR = $scope.lhpvBal.lbDrRcvrWtCR;
								
								
								$scope.lhpvBal.lbTotPayAmt = $scope.lhpvBal.lbLryBalP + $scope.lhpvBal.lbOthExtKmP + $scope.lhpvBal.lbOthOvrHgtP 
																+ $scope.lhpvBal.lbOthPnltyP + $scope.lhpvBal.lbUnpDetP + $scope.lhpvBal.lbUnLoadingP;
		
								$scope.lhpvBal.lbTotRcvrAmt = $scope.lhpvBal.lbCashDiscR + $scope.lhpvBal.lbMunsR + $scope.lhpvBal.lbTdsR 
																+ $scope.lhpvBal.lbWtShrtgCR + $scope.lhpvBal.lbDrRcvrWtCR + $scope.lhpvBal.lbLateDelCR 
																+ $scope.lhpvBal.lbLateAckCR + $scope.lhpvBal.lbOthMiscP;
		
		
								$scope.lhpvBal.lbFinalTot = $scope.lhpvBal.lbTotPayAmt - $scope.lhpvBal.lbTotRcvrAmt;
								
							}else{
								
								$scope.arHoAlw = false;
								$scope.lhpvBal.lbTotPayAmt = $scope.lhpvBal.lbLryBalP;
								$scope.lhpvBal.lbTotRcvrAmt = $scope.lhpvBal.lbCashDiscR + $scope.lhpvBal.lbMunsR + $scope.lhpvBal.lbTdsR;
								$scope.lhpvBal.lbFinalTot = $scope.lhpvBal.lbTotPayAmt - $scope.lhpvBal.lbTotRcvrAmt;
							
							
								$scope.lhpvBal.lbWtShrtgCR  = data.wtShrtg;
								$scope.lhpvBal.lbDrRcvrWtCR = data.drRcvrWt;
								$scope.lhpvBal.lbLateDelCR  = data.ltDel;
								$scope.lhpvBal.lbLateAckCR  = data.ltAck;
								$scope.lhpvBal.lbOthExtKmP  = data.exKm;
								$scope.lhpvBal.lbOthOvrHgtP = data.ovrHgt;
								$scope.lhpvBal.lbOthPnltyP  = data.penalty;
								$scope.lhpvBal.lbOthMiscP   = data.oth;
								$scope.lhpvBal.lbUnpDetP    = data.det;
								$scope.lhpvBal.lbUnLoadingP = data.unLdg;
							}
						}
						
						
						
						
						
						$scope.lhpvBalDBFlag = false;
					    	$('div#lhpvBalDB').dialog({
								autoOpen: false,
								modal:true,
								resizable: false,
								show: UDShow,
								title:"LHPV BALANCE FOR CHALLAN ("+$scope.challan+")",
								hide: UDHide,
								position: UDPos,
								draggable: true,
								close: function(event, ui) { 
							        $(this).dialog('destroy');
							        $(this).hide();
							        $scope.lhpvBalDBFlag = true;
							    }
								});
						$('div#lhpvBalDB').dialog('open'); 
					}else if(data.result==="SRTG"){
						$scope.alertToast("There is some Sortage/Damage in challan");
					}else if(data.result==="hold"){
						$scope.alertToast("This challan is on hold for payment");
					}else{
						$scope.alertToast("server error");
						console.log("error in fetching getChlnFrLA data");
					}
				});
				response.error(function(data, status, headers, config){
					console.log(data);
				});
		 }else{
			 $scope.alertToast("Already create the lhpv Balance for challan "+$scope.challan);
		 }
	 }
	 
	 
	 $scope.submitLhpvBal = function(lhpvBalForm,lhBal){
		 console.log("enter into submitLhpvBal function = "+lhpvBalForm.$invalid);
		 if(lhpvBalForm.$invalid){
			$scope.alertToast("Please fill correct form");
		 }else{
			 if($scope.lhpvBal.lbLryBalP < 0 && $scope.actBal > 0){
				 $scope.alertToast("please fill the amount <= "+$scope.actBal);
			 }else{
				 $scope.lbFinalTotSum=$scope.lbFinalTotSum+$scope.lhpvBal.lbFinalTot;
				 $scope.lhpvBalList.push(lhBal);
				 $scope.lhpvBal = {};
				 $('div#lhpvBalDB').dialog('close'); 
			 }
		 }
	 }
	 
	
	 $scope.removeLhpvBal = function(index,lhpvbal){
		 console.log("enter into removeLhpvBal function");
		 $scope.lbFinalTotSum=$scope.lbFinalTotSum-lhpvbal.lbFinalTot;
		 $scope.lhpvBalList.splice(index,1);
	 }
	 
	 
	 $scope.voucherSubmit = function(voucherForm){
		 console.log("enter into voucherForm function = "+voucherForm.$invalid);
		 if(voucherForm.$invalid){
			$scope.alertToast("Please enter correct form");
		 }else{
			 console.log("final submittion");
			    $scope.saveVsFlag = false;
		    	$('div#saveVsDB').dialog({
					autoOpen: false,
					modal:true,
					resizable: false,
					show: UDShow,
					title:"LHPV BALANCE FINAL SUBMISSION",
					hide: UDHide,
					position: UDPos,
					draggable: true,
					close: function(event, ui) { 
				        $(this).dialog('destroy');
				        $(this).hide();
				        $scope.saveVsFlag = true;
				    }
					});
				$('div#saveVsDB').dialog('open');
		 }
	 }
	 
	 
	 $scope.back = function(){
		 console.log("enter into back function");
		 $('div#saveVsDB').dialog('close');
	 } 
	 
	 
	 $scope.saveVS = function(){
		 console.log("enter into saveVS function ");
			$scope.vs.lhpvBalList = $scope.lhpvBalList;
			$scope.lbFinalTotSum=0;
			faCodeTemp='';
			
			$('#saveId').attr("disabled","disabled");
			var response = $http.post($scope.projectName+'/submitLhpvBalBrh',$scope.vs);
			response.success(function(data, status, headers, config){
				if(data.result === "success"){
					$('div#saveVsDB').dialog('close');
					$('#saveId').removeAttr("disabled");
					
					$scope.alertToast("Successfully save Bank Details -->> ");
					
					$scope.vs = {};
					$scope.lhpvBal = {};
					$scope.bnkList = [];
					$scope.chlnList = [];
					$scope.actBOList = [];
					$scope.lhpvBalList = [];
					$scope.vs.payToStf='';
					$scope.vs.cardCode="";
					$scope.challan = "";
					$scope.vs.chequeType = '';
					$('#chequeTypeId').attr("disabled","disabled");
					$scope.vs.bankCode = "";
					$('#bankCodeId').attr("disabled","disabled");
					$scope.vs.chequeLeaves = {};
					$('#chequeNoId').attr("disabled","disabled");
					
					$scope.getLhpvDet();
				}else{
					console.log("Error in submitVoucher");
				}
			});
			response.error(function(data, status, headers, config){
				console.log(data);
			});
	 }
	 
	 
	 $scope.resetAllAmt = function() {
			console.log("resetAllAmt");
			
			 $scope.lhpvBal.lbTotPayAmt = $scope.lhpvBal.lbLryBalP + $scope.lhpvBal.lbOthExtKmP + $scope.lhpvBal.lbOthOvrHgtP 
			 + $scope.lhpvBal.lbOthPnltyP + $scope.lhpvBal.lbUnpDetP + $scope.lhpvBal.lbUnLoadingP;

			 $scope.lhpvBal.lbTotRcvrAmt = $scope.lhpvBal.lbCashDiscR + $scope.lhpvBal.lbMunsR + $scope.lhpvBal.lbTdsR 
			 + $scope.lhpvBal.lbWtShrtgCR + $scope.lhpvBal.lbDrRcvrWtCR + $scope.lhpvBal.lbLateDelCR 
			 + $scope.lhpvBal.lbLateAckCR + $scope.lhpvBal.lbOthMiscP;
			 
			 $scope.chngLryBal();
	}
	 
	 $scope.verifyChallan = function() {
		console.log("verifyChallan()");
		if (angular.isUndefined($scope.challan) || $scope.challan === "" || $scope.challan === null) {
			$scope.alertToast("Please Enter Challan No");
		} else {
			
			$scope.lodingFlag = true;
			//$scope.detailType==="bank";
			var payMap={
				"type":$scope.detailType,
				"chlnNo":$scope.challan,
				"acntHldr":$scope.vs.acntHldr
					
			};
			
			var res = $http.post($scope.projectName+'/verifyChlnForLhpvBalN',payMap);
			res.success(function(data, status, headers, config) {
				if (data.result === "success") {
					if (angular.isUndefined(faCodeTemp) || faCodeTemp === "" || faCodeTemp === null) {
						faCodeTemp = data.faCode;
						$scope.lbFinalTotSum=0;
						$scope.vs.brkOwnFaCode = data.faCode;
						$scope.lhpvBalList = [];
					} if($scope.detailType==="bank"){ // this is for same owner broker
						if (faCodeTemp != data.faCode) {
									faCodeTemp = data.faCode;
									$scope.lbFinalTotSum=0;
									$scope.vs.brkOwnFaCode = data.faCode;
									$scope.lhpvBalList = [];
						}
					}
					
					if($scope.detailType==="bank"){
						if(data.validAc){
							$scope.vs.actBankName=data.bankName;
							$scope.vs.actIfscCode=data.ifsc;
							$scope.vs.actAccountNo=data.acNo;
							$scope.vs.actPayeeName=data.acName;
						}else{
							$scope.alertToast($scope.vs.acntHldr+" does not have valid bank details");
							$scope.lodingFlag = false;
							return;
						}
					}
					$scope.saveChln($scope.challan);
				} else {
					$scope.alertToast(data.msg);
				}
				$scope.lodingFlag = false;
			});
			res.error(function(data, status, headers, config) {
				console.log("error in response of verifyChlnForLhpvBal: "+data);
				$scope.lodingFlag = false;
			});
		}
	}
	 
	 
	 $scope.addBankDetail=function(){
		 $scope.lhpvBalBankDBFlag=false;
		 
		   	$('div#lhpvBalBankDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				show: UDShow,
				title:"Bank Detail",
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			        $scope.saveVsFlag = true;
			    }
				});
			$('div#lhpvBalBankDB').dialog('open');
	
		 
	 }
	 
	 
	 
	 $scope.submitBankDetail = function(lhpvBankForm){
		 console.log("Enter into submitBankDetail");
		 
		 
				 $scope.vs.actBankName=$scope.bankName;
			 
				 $scope.vs.actPayeeName=$scope.payeeName;
			 if($scope.ifscCode.length === 11  ){
				 if($scope.ifscCode ===$scope.ifscVCode){
					 lhpvBankForm.ifscCodeName.$invalid=false;
					 $scope.vs.actIfscCode=$scope.ifscCode;
				 }else{
					 $scope.ifscCode="";
					 $scope.alertToast("IFSC Code not match");
					 lhpvBankForm.ifscCodeName.$invalid=true;
			 	}
			 }else{
				 lhpvBankForm.ifscCodeName.$invalid=true;
				 $scope.alertToast("IFSC Code should be 11 characters");
			 }
			
			 if($scope.accountNo.length<=20 && $scope.accountNo.length>1 ){
				 if($scope.accountNo ===$scope.accountVNo){
					 $scope.vs.actAccountNo=$scope.accountNo;
					 lhpvBankForm.accountNoName.$invalid=false;
				 } else{
					 $scope.accountNo="";
					 $scope.alertToast("A/C No. not match");
					 lhpvBankForm.accountNoName.$invalid=true;
				 } 
			 }else{
				 lhpvBankForm.accountNoName.$invalid=true;
				 $scope.alertToast("Account No. can't be greater 20 character and empty");
			 }
			 
				 
			 
			 if(lhpvBankForm.$invalid){
				 $scope.alertToast("Enter valid detail");
			 }else if(lhpvBankForm.accountNoName.$invalid){
				// $scope.alertToast("Enter valid Account No");
			 }else if(lhpvBankForm.ifscCodeName.$invalid){
				 //$scope.alertToast("Enter valid IFSC Code");
			 }else{
				 $('div#lhpvBalBankDB').dialog('close');
			 }
		 
		 
	 }
	 
	
	 
	 
	 $scope.addCardDetail=function(){
		 $scope.lhpvBalCardDBFlag=false;
		 
		   	$('div#lhpvBalCardDB').dialog({
				autoOpen: false,
				modal:true,
				resizable: false,
				show: UDShow,
				title:"Card Detail",
				hide: UDHide,
				position: UDPos,
				draggable: true,
				close: function(event, ui) { 
			        $(this).dialog('destroy');
			        $(this).hide();
			        $scope.saveVsFlag = true;
			    }
				});
			$('div#lhpvBalCardDB').dialog('open');
	
		 
	 }
	 
	 
	 
	 
	 $scope.submitCardDetail = function(lhpvCardForm){
		 console.log("Enter into submitCardDetail");
		 $scope.vs.actIfscCode=null;
		 $scope.vs.actPayeeName=null;

		 
				 $scope.vs.actBankName=$scope.cardName;
				 $scope.vs.cardCode =$scope.cardVNo;
				 $scope.vs.actAccountNo=$scope.cardNo;
			/* if($scope.cardNo.length<=20 && $scope.cardNo.length>1 ){
				 if($scope.cardNo ===$scope.cardVNo){
					 $scope.vs.actAccountNo=$scope.cardNo;
					 lhpvCardForm.cardNoName.$invalid=false;
				 } else{
					 $scope.cardNo="";
					 $scope.alertToast("Card No. not match");
					 lhpvCardForm.cardNoName.$invalid=true;
				 } 
			 }else{
				 lhpvCardForm.cardNoName.$invalid=true;
				 $scope.alertToast("Card No. can't be greater 20 character and empty");
			 }*/
			 
				 
			 
			 if(lhpvCardForm.$invalid){
				 $scope.alertToast("Enter valid detail");
			 }else if(lhpvCardForm.cardNoName.$invalid){
				// $scope.alertToast("Enter valid Account No");
			 }else{
				 $('div#lhpvBalCardDB').dialog('close');
			 }
	 }

	 
	 $scope.selectCardName=function(){
		 $scope.cardNo="";
		 $scope.cardVNo="";
	 }
	 
	 
 $scope.petroCardDBFlag=true;
	 
	 $scope.getCardNo=function(){
		 console.log("Enter into getCardNo()");
		 var res = $http.post($scope.projectName+'/getPetroCardNo',$scope.cardName);
			res.success(function(data, status, headers, config) {
				if (data.result === "success") {
					console.log(data);
					$scope.petroList=data.list;
					
					$scope.petroCardDBFlag = false;
			    	$('div#petroCardDB').dialog({
						autoOpen: false,
						modal:true,
						resizable: false,
						show: UDShow,
						title:"Petro Card FaCode",
						hide: UDHide,
						position: UDPos,
						draggable: true,
						close: function(event, ui) { 
					        $(this).dialog('destroy');
					        $(this).hide();
					        $scope.saveVsFlag = true;
					    }
						});
					$('div#petroCardDB').dialog('open');
					
					//$scope.alertToast("Successs");
					
				} else {
					$scope.alertToast("There is no petro card");
				}
			});
			res.error(function(data, status, headers, config) {
				console.log("error in response of getPetroCardNo: "+data);
				$scope.lodingFlag = false;
			});
			
	 }
	 
	 $scope.savePetroCode = function(petro){
		 console.log(petro);
		 $scope.cardNo=petro.cardNo;
		 $scope.cardVNo = petro.cardFaCode;
		// $scope.vs.cardCode = petro.cardFaCode;
		 $('div#petroCardDB').dialog('close');
	 } 
	 
	
	 $scope.notifyMsg = function() {
		console.log("notifyMsg");
		if (angular.isUndefined($scope.vs.brkOwnFaCode) || $scope.vs.brkOwnFaCode === "" || $scope.vs.brkOwnFaCode === null) {
			$scope.alertToast("Please Entre Challan No and Verify Challan");
		}
	}
	 
	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		 $scope.getLhpvDet();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 } 
}]);