package org.apache.jsp.WEB_002dINF.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class superAdmin_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("<div ng-show=\"superAdminLogin\">\r\n");
      out.write("<div class=\"container\">\r\n");
      out.write("\t<div class=\"row\">\r\n");
      out.write("\t<div class=\"col s3 hide-on-med-and-down\"> &nbsp; </div>\r\n");
      out.write("\t<div class=\"col s12 m12 l6\" ng-if=\"superAdminLogin\">\r\n");
      out.write("\t\r\n");
      out.write("\t\t\t\r\n");
      out.write("\t<div class=\"card blue-grey darken-1\">\r\n");
      out.write("            <div class=\"card-content white-text\">\r\n");
      out.write("              <span class=\"card-title\">Welcome Super Admin</span>\r\n");
      out.write("              <p></p>\r\n");
      out.write("            </div>\r\n");
      out.write("            <!-- <div class=\"card-action center\">\r\n");
      out.write("            <a href=\"#/superAdmin\" class=\"btn white-text\" ng-click=\"superAdmin()\">superAdmin</a>\r\n");
      out.write("\t\t\t<a href=\"#/admin\" class=\"btn white-text\" ng-click=\"admin()\">admin</a>\r\n");
      out.write("\t\t\t<a href=\"#/operator\" class=\"btn white-text\" ng-click=\"operator()\">operator</a>\r\n");
      out.write("            </div> -->\r\n");
      out.write("     </div>\r\n");
      out.write("\t</div>\r\n");
      out.write("\t</div>\r\n");
      out.write("</div>\r\n");
      out.write("\t<div class=\"row\">\r\n");
      out.write("       <div class=\"col s12\">\r\n");
      out.write("         <div class=\"card blue-grey darken-1\">\r\n");
      out.write("           <div class=\"card-content white-text\">\r\n");
      out.write("             <span class=\"card-title\">Stationary Notification</span>\r\n");
      out.write("             \t<table>\r\n");
      out.write("\t\t \t \t\t<tr>\r\n");
      out.write("\t\t \t \t\t\t<th>CnmtDaysLast</th>\r\n");
      out.write("\t\t \t \t\t    <th>CnmtAvgMonUsg</th>\t\t\t\t\r\n");
      out.write("\t\t\t\t\t\t<th>ChlnDaysLast</th>\r\n");
      out.write("\t\t\t\t\t\t<th>ChlnAvgMonUsg</th>\r\n");
      out.write("\t\t\t\t\t\t<th>SedrDaysLast</th>\r\n");
      out.write("\t\t\t\t\t\t<th>SedrAvgMonUsg</th>\r\n");
      out.write("\t\t\t\t\t</tr>\r\n");
      out.write("\t\t \t  \t  \r\n");
      out.write("\t\t\t\t  <tr>\r\n");
      out.write("\t\t              <td>{{cnmtMonthsLast}} month {{cnmtDaysLast}} days</td>\r\n");
      out.write("\t\t              <td>{{cnmtAvgMonUsg | number:2}}</td>\r\n");
      out.write("\t\t              <td>{{chlnMonthsLast}} month {{chlnDaysLast}} days</td>\r\n");
      out.write("\t\t              <td>{{chlnAvgMonUsg | number:2}}</td>\r\n");
      out.write("\t\t              <td>{{sedrMonthsLast}} month {{sedrDaysLast}} days</td>\r\n");
      out.write("\t\t              <td>{{sedrAvgMonUsg | number:2}}</td>\r\n");
      out.write("\t\t          </tr>\r\n");
      out.write("         \t  </table>\r\n");
      out.write("             <input type=\"button\" value=\"Create Stationary Order\" ng-click=\"createStnOrder()\"/>\r\n");
      out.write("           </div>\r\n");
      out.write("         </div>\r\n");
      out.write("       </div>\r\n");
      out.write("     </div> \r\n");
      out.write("     \r\n");
      out.write("    <div class=\"row\" ng-if=\"HBOSNList.length > 0\">\r\n");
      out.write("       <div class=\"col s12\">\r\n");
      out.write("         <div class=\"card blue-grey darken-1\">\r\n");
      out.write("           <div class=\"card-content white-text\">\r\n");
      out.write("             <span class=\"card-title\">Stationary Order</span>\r\n");
      out.write("              \t\r\n");
      out.write("\t        \t<table>\r\n");
      out.write("              \t\r\n");
      out.write("\t              <tr>\r\n");
      out.write("\t              \t<td></td>\r\n");
      out.write("\t              \t<td>id</td>\r\n");
      out.write("\t\t\t\t\t<td>Cnmt Number</td>\r\n");
      out.write("\t\t\t\t\t<td>Chln Number</td>\r\n");
      out.write("\t\t\t\t\t<td>Sedr Number</td>\r\n");
      out.write("\t\t\t\t\t<td>Stationary Status</td>\r\n");
      out.write("\t              </tr>\r\n");
      out.write("\t              \r\n");
      out.write("\t              <tr ng-repeat=\"HBOSN in HBOSNList\">\r\n");
      out.write("\t              <td><input type=\"checkbox\" name=\"check\" ></td>\r\n");
      out.write("\t              \t<td>{{HBOSN.hBOSN_Id}}</td>\r\n");
      out.write("\t\t\t\t\t<td>{{HBOSN.hBOSN_cnmtNo}}</td>\r\n");
      out.write("\t\t\t\t\t<td>{{HBOSN.hBOSN_chlnNo}}</td>\r\n");
      out.write("\t\t\t\t\t<td>{{HBOSN.hBOSN_sedrNo}}</td>\r\n");
      out.write("\t\t\t\t\t<td>{{HBOSN.hBOSN_hasRec}}</td>\r\n");
      out.write("\t\t\t\t\t<td><input type=\"button\" value=\"confirm order\" ng-click=\"confirmOrder(HBOSN)\"></td>\r\n");
      out.write("\t\t\t\t\t<!-- <td>{{HBOSN.creationTS | date : format : timezone}}</td> -->\r\n");
      out.write("\t\t\t\t\t<!-- <td><input type=\"button\" value=\"Create Stationary Order\" ng-click=\"createStnOrder(HBOSN)\"></td>\r\n");
      out.write("\t\t\t\t\t<td><input type=\"button\" value=\"Receive Stationary Order\" ng-click=\"recStnOrder()\"></td> -->\r\n");
      out.write("\t\t\t\t\t<!-- <td><input type=\"button\" value=\"Create Dispatch Order\" ng-click=\"createOrder(hbo.bCode)\"></td> -->\r\n");
      out.write("\t              </tr>\r\n");
      out.write("\t              \r\n");
      out.write("\t            <!--   <tr>\r\n");
      out.write("\t              \t<td><input type =\"date\" ng-model=\"dispatchDate\"></td>\r\n");
      out.write("\t              \t <td><input type=\"button\" ng-click=\"getListWithDate(dispatchDate)\"></td>\r\n");
      out.write("\t              </tr> -->\r\n");
      out.write("\t              \r\n");
      out.write("           \t </table>\r\n");
      out.write("        \r\n");
      out.write("             <!-- <input type=\"button\" value=\"Create Stationary Order\" ng-click=\"createStnOrder()\"/>  -->     \r\n");
      out.write("           </div>\r\n");
      out.write("         </div>\r\n");
      out.write("       </div>\r\n");
      out.write("     </div> \r\n");
      out.write("     \r\n");
      out.write("     \r\n");
      out.write("     \r\n");
      out.write("    <div id=\"openStnOrderDB\" ng-hide=\"createStnOrderFlag\">\r\n");
      out.write("    \t\r\n");
      out.write("    \t <form name=\"stnOrderForm\" ng-submit=\"saveStnOrder(hbosn,stnOrderForm)\">\r\n");
      out.write("\t \t  \t<div class=\"row\" style=\"margin-top: 40px;\">\r\n");
      out.write("\t \t  \t<div class=\"input-field col s6\">\r\n");
      out.write("\t        \t<input type =\"text\" name =\"hbon.operatorCode\" ng-model=\"hbosn.hBOSN_opCode\" ng-click=\"selectOperatorCode()\" required readOnly>\r\n");
      out.write("\t      \t\t\t<label>Operator Code</label>\r\n");
      out.write("\t      \t</div>\r\n");
      out.write("\t      \t\r\n");
      out.write("\t       \t<div class=\"input-field col s6\">\r\n");
      out.write("\t        \t<input type =\"number\" name =\"hbon.noCnmt\" ng-model=\"hbosn.hBOSN_cnmtNo\" required>\r\n");
      out.write("\t      \t\t\t<label>No. Of CNMT</label>\r\n");
      out.write("\t      \t</div>\r\n");
      out.write("\t      \t<div class=\"input-field col s6\">\r\n");
      out.write("\t        \t<input type =\"number\" name =\"hbon.noChln\" ng-model=\"hbosn.hBOSN_chlnNo\" required>\r\n");
      out.write("\t      \t\t\t<label>No. Of Challan</label>\r\n");
      out.write("\t      \t</div>\r\n");
      out.write("\t      \t<div class=\"input-field col s6\">\r\n");
      out.write("\t        \t<input type =\"number\" name =\"hbon.noSedr\" ng-model=\"hbosn.hBOSN_sedrNo\">\r\n");
      out.write("\t      \t\t\t<label>No. Of SEDR</label>\r\n");
      out.write("\t      \t</div>\r\n");
      out.write("\t      \t<div class=\"col s12 center\">\r\n");
      out.write("\t      \t<input type=\"submit\">\r\n");
      out.write("\t      \t</div>\r\n");
      out.write("\t      \t</div>\r\n");
      out.write(" \t  </form>    \r\n");
      out.write("    \r\n");
      out.write("    </div> \r\n");
      out.write(" \r\n");
      out.write(" \r\n");
      out.write("<div id =\"selectOperatorCodeModel\" ng-hide = \"operatorCodeFlag\">\r\n");
      out.write(" \t  <table>\r\n");
      out.write(" \t \t\t<tr>\r\n");
      out.write("\t\t\t\t<th></th>\r\n");
      out.write("\t\t\t\t\r\n");
      out.write("\t\t\t\t<th>Operator Code</th>\r\n");
      out.write("\t\t\t\t\r\n");
      out.write("\t\t\t\t<th>User Name</th>\t\t\t\r\n");
      out.write("\t\t\t\t\r\n");
      out.write("\t\t\t</tr>\r\n");
      out.write("\t\t\t\r\n");
      out.write("\t\t\t <tr ng-repeat=\"operator in operatorList\">\r\n");
      out.write("\t\t \t  <td><input type=\"radio\"  name=\"operatorName\" value=\"{{ operator.userCode }}\"  ng-click=\"selectOperator(operator.userCode)\"></td>\r\n");
      out.write("              <td>{{ operator.userCode }}</td>\r\n");
      out.write("              <td>{{ operator.userName }}</td>\r\n");
      out.write("          </tr>\r\n");
      out.write("         </table>\r\n");
      out.write("</div>\t\r\n");
      out.write("</div>       ");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
