package com.mylogistics.DAOImpl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;

import com.mylogistics.DAO.OwnerMasterDAO;
import com.mylogistics.model.OwnerMaster;

public class OwnerMasterDAOImpl implements OwnerMasterDAO{

	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	public OwnerMasterDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Override
	public int saveOwnerMaster(Session session,OwnerMaster ownerMaster) {
		
		return (int) session.save(ownerMaster);
	}
	
	
}
