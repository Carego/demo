package com.mylogistics.constants;

public class CnmtCNTS {

	public static final String 	CNMT_ID = "cnmtId";
	
	public static final String CNMT_CODE  = "cnmtCode";
	
	public static final String BRANCH_CODE = "branchCode";
	
	public static final String CNMT_DT = "cnmtDt";
	
	public static final String CUST_CODE = "custCode";
	
	public static final String CNMT_BILL_NO = "cnmtBillNo";
		
	public static final String CNMT_CONSIGNOR = "cnmtConsignor";
	
	public static final String CNMT_CONSIGNEE = "cnmtConsignee";
	
	public static final String CNMT_DC = "cnmtDC";
	
	public static final String CNMT_FrSt = "cnmtFromSt";
	
	public static final String CNMT_TSt = "cnmtToSt";
	
	public static final String CNMT_PROD_TYP = "cnmtProductType";
	
	public static final String CNMT_NO_OF_PKG = "cnmtNoOfPkg";
	
	public static final String CNMT_ACTUAL_WT = "cnmtActualWt";
	
	public static final String CNMT_GUARANTEE_WT = "cnmtGuaranteeWt";
	
	public static final String CNMT_DDL = "cnmtDDL";
	
	public static final String CNMT_PAY_AT = "cnmtPayAt";
	
	public static final String CNMT_BILL_AT = "cnmtBillAt";
	
	public static final String CNMT_RATE = "cnmtRate";
	
	public static final String CNMT_FREIGHT = "cnmtFreight";
	
	public static final String CNMT_VOG = "cnmtVOG";
	
	public static final String CNMT_EXTRA_EXP = "cnmtExtraExp";
	
	public static final String CNMT_COST_GRADE = "cnmtCostGrade";
	
	public static final String CNMT_DT_OF_DLY = "cnmtDtOfDly";
	
	public static final String CNMT_EMP_CODE = "cnmtEmpCode";
	
	public static final String CNMT_KM = "cnmtKm";
	
	public static final String CNMT_VEHICLE_TYPE = "cnmtVehicleType";
	
	public static final String CNMT_STATE = "cnmtState";
	
	public static final String CNMT_TOT = "cnmtTOT";
	
	public static final String CONTRACT_CODE= "contractCode";
	
	public static final String CNMT_INVOICE_NO = "cnmtInvoiceNo";
	
	public static final String USER_BRANCH_CODE = "bCode";
	
	public static final String CNMT_IS_TRANS = "isTrans";
	
	public static final String CREATION_TS = "creationTS";
	
	public static final String IS_VIEW = "isView";
	
	public static final String USER_CODE = "userCode";
	
	public static final String CNMT_TPNO = "cnmtTpNo";
	
	public static final String CNMT_CCA = "isCCA";
	
	public static final String CNMT_CANCEL = "isCancel";
	
	public static final String CNMT_BILL_AMT = "bilAmt";
	
	public static final String CNMT_LHPV_AMT = "lhpvAmt";
	
	public static final String CNMT_COST = "cnmtCost";
	
	public static final String PR_BL_CODE = "prBlCode";
	
	public static final String WH_CODE = "whCode";
	
	public static final String REL_TYPE = "relType";
	
	public static final String REL_CHLN = "relChln";
	
}
