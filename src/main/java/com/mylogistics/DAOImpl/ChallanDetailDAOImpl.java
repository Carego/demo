package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.ChallanDetailDAO;
import com.mylogistics.constants.ChallanCNTS;
import com.mylogistics.constants.ChallanDetailCNTS;
import com.mylogistics.model.Challan;
import com.mylogistics.model.ChallanDetail;

public class ChallanDetailDAOImpl implements ChallanDetailDAO{

	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;

	@Autowired
	public ChallanDetailDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}


	@Transactional//done
	public int saveChlnDetailToDB(ChallanDetail chd){
		int temp;
		try{
			session = this.sessionFactory.openSession();
			transaction =  session.beginTransaction();
			session.save(chd);
			transaction.commit();
			System.out.println("user data inserted successfully");
			session.flush();
			temp= 1;
		}
		catch(Exception e){
			e.printStackTrace();
			temp= -1;
		}
		session.clear();
		session.close();
		return temp;
	}

	@Transactional
	public int updateChallanDetail(ChallanDetail challanDetail){
		int temp;
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		try{
			session = this.sessionFactory.openSession();
			transaction = session.beginTransaction();
			Criteria cr=session.createCriteria(Challan.class);
			challanDetail.setCreationTS(calendar);
			session.saveOrUpdate(challanDetail);
			transaction.commit();
			session.flush();
			temp= 1;
		}
		catch(Exception e){
			e.printStackTrace();
			temp= -1;
		}		
		session.clear();
		session.close();
		return temp;
	}

	 @Transactional
	 @SuppressWarnings("unchecked")
	 public List<ChallanDetail> getChlnDet(){
		 System.out.println("enter into getChlnDet function");
		 List<ChallanDetail> chldList = new ArrayList<>();
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 Criteria cr=session.createCriteria(ChallanDetail.class);
			 chldList = cr.list();
			
			 session.flush();
		 }catch(Exception e){
			 e.printStackTrace();
		 }
		 session.clear();
		 session.close();
		 return chldList;
	 }
	 
	 
	 @Transactional
	 @SuppressWarnings("unchecked")
	 public String getLryNoByChCode(String chCode){
		 System.out.println("enter into getLryNoByChdId function");
		 String lryNo = "";
		 List<ChallanDetail> chdList = new ArrayList<>();
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 Criteria cr=session.createCriteria(ChallanDetail.class);
			 cr.add(Restrictions.eq(ChallanDetailCNTS.CHD_CHALLAN_CODE,chCode));
			 chdList = cr.list();
			 if(!chdList.isEmpty()){
				 ChallanDetail chd = chdList.get(0);
				 lryNo = chd.getChdRcNo();
			 }
			 session.flush();
		 }catch(Exception e){
			 e.printStackTrace();
		 }
		 session.clear();
		 session.close();
		 return lryNo;
	 }
	 
	 
	 @Transactional
	 @SuppressWarnings("unchecked")
	 public ChallanDetail getChdbyChlnCode(String chlnCode){
		 System.out.println("enter into getChdbyChlnCode function ="+chlnCode);
		 ChallanDetail challanDetail = null; 
		 List<ChallanDetail> chdList = new ArrayList<>();
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 Criteria cr=session.createCriteria(ChallanDetail.class);
			 cr.add(Restrictions.eq(ChallanDetailCNTS.CHD_CHALLAN_CODE,chlnCode));
			 chdList = cr.list();
			 if(!chdList.isEmpty()){
				 challanDetail = chdList.get(0);
			 }
			 session.flush();
		 }catch(Exception e){
			 e.printStackTrace();
		 }
		 session.clear();
		 session.close();
		 return challanDetail;
	 }
	 
	 
	 @Override
	 public int saveChlnDetailToDB(ChallanDetail cd,Session session) {
		 return (int) session.save(cd);
	 }
	 
}
