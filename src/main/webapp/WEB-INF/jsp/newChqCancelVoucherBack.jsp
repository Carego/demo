<div ng-show="operatorLogin || superAdminLogin">
	<div class="row">
		<form name="newVoucherForm" ng-submit=voucherSubmit(newVoucherForm) class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="branchFaCodeId" name ="branchFaCode" ng-model="vs.branch.branchFaCode"  readonly ng-required="true" >
		       			<label for="code">Branch Code</label>	
		       		</div>

		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="branchNameId" name ="branchName" ng-model="vs.branch.branchName" readonly ng-required="true" >
		       			<label for="code">Branch Name</label>	
		       		</div>
		       		
		       			<div class="col s4 input-field">
		       				<input class="validate" type ="date" id="dateId" name ="dateTemp" ng-model="dateTemp"   readonly  >
		       			<label for="code">Date</label>	
		       		</div>
		    </div>
		    
		    <div class="row">
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="sheetNoId" name ="sheetNo" ng-model="vs.cashStmtStatus.cssCsNo" readonly ng-required="true" >
		       			<label for="code">Sheet No</label>	
		       		</div>
			   		 <div class="col s4 input-field">
		       				<input class="validate" type ="text" id="voucherTypeId" name ="voucherType" ng-model="vs.voucherType" readonly ng-required="true" >
		       			<label for="code">Voucher Type</label>	
		       		</div>
		       		
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="bankCodeId" name ="bankCode" ng-model="vs.bankCode"  ng-click = "openBankDB()" readonly>
		       			<label for="code">Bank Code</label>	
		       		</div>
		    </div>
		    
		    <div class="row">
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="usedChqNoId" name ="usedChqNoName" ng-model="usedChq.chqLChqNo" ng-click="openUsedChqDB()" ng-required="true" disabled="disabled" readonly="readonly">
		       			<label for="code">Cancelled Chq</label>	
		       		</div>
			   		 <div class="col s4 input-field">
		       				<input class="validate" type ="text" id="unUsedChqNoId" name ="unUsedChqNoName" ng-model="unUsedChq.chqLChqNo" ng-click="openUnUsedChqDB()" disabled="disabled" readonly="readonly">
		       			<label for="code">New Chq</label>	
		       		</div>
		       		
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="unUsedChqAmtId" name ="unUsedChqAmtName" ng-model="unUsedChq.chqLChqAmt" disabled="disabled"  readonly="readonly">
		       			<label for="code">Amt</label>	
		       		</div>
		    </div>
		    	   			   		
	   		 <div class="row">
				<div class="input-field col s12">
					<i class="mdi-editor-mode-edit prefix"></i>
 					<textarea id="textarea" class="materialize-textarea"  rows="3" cols="92"  name ="desc" ng-model="vs.desc" ng-required="true"></textarea>
 					<label>Description</label>
				</div>
    		 </div>	
	   		 
	   		  <div class="row">
	     		<div class="input-field col s12 center">
	       				<input class="validate" type ="submit" value="submit" >
	       		</div>
	       	 </div>
		  </form>
    </div>
    
   <div id ="bankDB" ng-hide="bankDBFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox1" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Bank Name </th>
 	  	  	  <th> Bank A/C No. </th>
 	  	  	  <th> Bank FaCode </th>
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="bank in bankACNList | filter:filterTextbox1">
		 	  <td><input type="radio"  name="bank"   value="{{ bank }}" ng-model="bnk" ng-click="saveBank(bank)"></td>
              <td>{{bank.bnkName}}</td>
              <td>{{bank.bnkAcNo}}</td>
              <td>{{bank.bnkFaCode}}</td>
              
          </tr>
      </table> 
	</div>
	
	<div id ="unUsedChqDB" ng-hide="unUsedChqDBFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox1" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Chq No.</th>
 	  	  	  <th> Chq Type </th>
 	  	  </tr>
 	  
		  <tr ng-repeat="unUsedChq in unUsedChqList | filter:filterTextbox1">
		 	  <td><input type="radio"  name="bank"   value="{{ unUsedChq }}" ng-model="unUsdChq" ng-click="saveUnUsedChq(unUsedChq)"></td>
              <td>{{unUsedChq.chqLChqNo}}</td>
              <td>{{unUsedChq.chqLChqType}}</td>
          </tr>
      </table> 
	</div> 

	<div id ="usedChqDB" ng-hide="usedChqDBFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox1" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Chq No.</th>
 	  	  	  <th> Chq Amt </th>
 	  	  	  <th> Chq Type </th>
 	  	  	  <th> Chq Used Dt </th>
 	  	  </tr>
 	  
		  <tr ng-repeat="usedChq in usedChqList | filter:filterTextbox1">
		 	  <td><input type="radio"  name="bank"   value="{{ usedChq }}" ng-model="usdChq" ng-click="saveUsedChq(usedChq)"></td>
              <td>{{usedChq.chqLChqNo}}</td>
              <td>{{usedChq.chqLChqAmt}}</td>
              <td>{{usedChq.chqLChqType}}</td>
              <td>{{usedChq.chqLUsedDt | date:'dd/MM/yyyy'}}</td>
          </tr>
      </table> 
	</div>
	
	<div id="saveVsDB" ng-hide="saveVsFlag"> 
		<div class="col s6 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >
			<table class="table-hover table-bordered table-bordered">
				<!-- <h2>Voucher Details</h2> -->
				<tr>
					<td>Branch Code</td>
					<td>{{vs.branch.branchFaCode}}</td>
				</tr>
				
				<tr>
					<td>Branch Name</td>
					<td>{{vs.branch.branchName}}</td>
				</tr>
				
				<tr>
					<td>Date</td>
					<td>{{dateTemp}}</td>
				</tr>
				
				<tr>
					<td>Sheet No</td>
					<td>{{vs.cashStmtStatus.cssCsNo}}</td>
				</tr>
				
				<tr>
					<td>Voucher Type</td>
					<td>{{vs.voucherType}}</td>
				</tr>
				
				<tr>
					<td>Bank Code</td>
					<td>{{vs.bankCode}}</td>
				</tr>
				
				<tr>
					<td>Cancelled Chq No</td>
					<td>{{usedChq.chqLChqNo}}</td>
				</tr>
				
				<tr>
					<td>Cancelled Chq Amt</td>
					<td>{{usedChq.chqLChqAmt}}</td>
				</tr>
				
				<tr>
					<td>New Chq No</td>
					<td>{{unUsedChq.chqLChqNo}}</td>
				</tr>
				
				<tr>
					<td>New Chq Amt</td>
					<td>{{unUsedChq.chqLChqAmt}}</td>
				</tr>
				
				<tr>
					<td>Description</td>
					<td>{{vs.desc}}</td>
				</tr>
			</table>
			<input type="button" value="Cancel" ng-click="back()"/>
			<input type="button" value="Save" id="saveId" ng-click="saveVS()"/>
		</div>
	</div>
    
 </div>   