package com.mylogistics.services;

import java.sql.Blob;

import org.springframework.stereotype.Service;

@Service
public class ImageService {

	private Blob image;
	
	private Blob decImg;
	
	private Blob chqImg;
	
	private Blob psImg;
	
	private Blob insImg;
	
	private Blob rcImg;
	
	//private Blob tcImg;

	public Blob getImage() {
		return image;
	}

	public void setImage(Blob image) {
		this.image = image;
	}

	public Blob getDecImg() {
		return decImg;
	}

	public void setDecImg(Blob decImg) {
		this.decImg = decImg;
	}

	public Blob getChqImg() {
		return chqImg;
	}

	public void setChqImg(Blob chqImg) {
		this.chqImg = chqImg;
	}

	public Blob getPsImg() {
		return psImg;
	}

	public void setPsImg(Blob psImg) {
		this.psImg = psImg;
	}

	public Blob getInsImg() {
		return insImg;
	}

	public void setInsImg(Blob insImg) {
		this.insImg = insImg;
	}

	public Blob getRcImg() {
		return rcImg;
	}

	public void setRcImg(Blob rcImg) {
		this.rcImg = rcImg;
	}

	/*public Blob getTcImg() {
		return tcImg;
	}

	public void setTcImg(Blob tcImg) {
		this.tcImg = tcImg;
	}*/
	
	
}
