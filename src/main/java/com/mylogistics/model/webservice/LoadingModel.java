package com.mylogistics.model.webservice;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.FetchMode;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mylogistics.model.webservice.VendorDetail.SelectedVendor;
import com.mylogistics.model.webservice.VendorDetail.UnSelectedVendor;

@Entity
@Table(name = "loading_app")
public class LoadingModel {

	@Id@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ldId", length = 10)
	public Integer ldId;
	
	@Column(name = "branchCode", length = 10)
	@NotNull(message = "Branch code can not be null !")
	@NotEmpty(message = "Branch code can not be empty !")
	private String branchCode;
	
	@Column(name = "userCode", length = 10)
	@NotNull(message = "User code can not be null !")
	@NotEmpty(message = "User code can not be empty !")
	private String userCode;
	
	@Column(name = "branchName", length = 100)
	@NotNull(message = "Branch name can not be null !")
	@NotEmpty(message = "Branch name can not be empty !")
	private String branchName;
	
	@Column(name = "officerName", length = 100)
	@NotNull(message = "Officer name can not be null !")
	@NotEmpty(message = "Officer name can not be empty !")
	private String officerName;
	
	@Column(name = "customerId", length = 10)
	@NotNull(message = "Customer ID can not be null !")
	@Min(value = 1, message = "Customer ID should be greater than 0 !")
	private Integer customerId;
	
	@Column(name = "customerName", length = 100)
	@NotNull(message = "Customer name can not be null !")
	@NotEmpty(message = "Customer name can not be empty !")
	private String customerName;
	
	@Column(name = "vehicleType", length = 50)
	@NotNull(message = "Vehicle type can not be null !")
	@NotEmpty(message = "Vehicle type can not be empty !")
	private String vehicleType;
	
	@Column(name = "sourceName", length = 50)
	@NotNull(message = "Source name can not be null !")
	@NotEmpty(message = "Source name can not be empty !")
	private String sourceName;
	
	@Column(name = "destinationName", length = 50)
	@NotNull(message = "Destination name can not be null !")
	@NotEmpty(message = "Destination name can not be empty !")	
	private String destinationName;
	
	@Column(name = "sourceId", length = 10)
	@NotNull(message = "Source ID can not be null !")
	@Min(value = 1, message = "Source ID should be greater than 0 !")
	private Integer sourceId;
	
	@Column(name = "destinationId", length = 10)
	@NotNull(message = "Destination ID can not be null !")
	@Min(value = 1, message = "Destination ID should be greater than 0 !")
	private Integer destinationId;
	
	@Column(name = "quantity", length = 10)
	@NotNull(message = "Quantity can not be null !")
	@Min(value = 1, message = "Quantity should be greater than 0 !")
	private Float quantity;
	
	@Column(name = "metricOrTon", length = 10)
	@NotNull(message = "Metric Or Ton can not be null !")
	@NotEmpty(message = "Destination name can not be empty !")
	private String metricOrTon;
	
	@Column(name = "orderNo", length = 10)
	@NotNull(message = "Order No. can not be null !")
	@NotEmpty(message = "Order No. can not be empty !")
	private String orderNo;
	
	@Column(name = "isPending")	
	@NotNull(message = "isPending can not be null !")	
	private Boolean isPending;
	
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "loadingModel_ldId")
	List<VendorDetail> vendorDetails = new ArrayList<VendorDetail>();
	
	@Transient
	private List<Map<String, Object>> ownBrkList = new ArrayList<Map<String,Object>>();
	
	@Column(name = "creationTS", insertable = false, updatable = true)
	private Calendar creationTS;
	
	public Integer getLdId() {
		return ldId;
	}

	public void setLdId(Integer ldId) {
		this.ldId = ldId;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getOfficerName() {
		return officerName;
	}

	public void setOfficerName(String officerName) {
		this.officerName = officerName;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}

	public String getSourceName() {
		return sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	public String getDestinationName() {
		return destinationName;
	}

	public void setDestinationName(String destinationName) {
		this.destinationName = destinationName;
	}

	public Integer getSourceId() {
		return sourceId;
	}

	public void setSourceId(Integer sourceId) {
		this.sourceId = sourceId;
	}

	public Integer getDestinationId() {
		return destinationId;
	}

	public void setDestinationId(Integer destinationId) {
		this.destinationId = destinationId;
	}

	public Float getQuantity() {
		return quantity;
	}

	public void setQuantity(Float quantity) {
		this.quantity = quantity;
	}

	public String getMetricOrTon() {
		return metricOrTon;
	}

	public void setMetricOrTon(String metricOrTon) {
		this.metricOrTon = metricOrTon;
	}

	public List<VendorDetail> getVendorDetails() {
		return vendorDetails;
	}

	public void setVendorDetails(List<VendorDetail> vendorDetails) {
		this.vendorDetails = vendorDetails;
	}

	public List<Map<String, Object>> getOwnBrkList() {
		return ownBrkList;
	}

	public void setOwnBrkList(List<Map<String, Object>> ownBrkList) {
		this.ownBrkList = ownBrkList;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public Boolean getIsPending() {
		return isPending;
	}

	public void setIsPending(Boolean isPending) {
		this.isPending = isPending;
	}
	
	
	
}