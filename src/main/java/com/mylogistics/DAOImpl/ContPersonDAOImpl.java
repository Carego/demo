package com.mylogistics.DAOImpl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.ContPersonDAO;
import com.mylogistics.constants.ContPersonCNTS;
import com.mylogistics.model.ContPerson;

public class ContPersonDAOImpl implements ContPersonDAO{
	
	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	public ContPersonDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	 @Transactional
	   public int saveContactPerson(ContPerson contPerson){
		 int temp;
		 try{
			 session = this.sessionFactory.openSession();
			 transaction = session.beginTransaction();
			 session.save(contPerson);
			 transaction.commit();
			 System.out.println("contactPerson Data is inserted successfully");
			 temp = 1;
			 session.flush();
		 }
		   
		 catch(Exception e){
			 e.printStackTrace();
			 temp = -1;
		 }
		 session.clear();
		 session.close();
		 return temp;
	 }
	 
	 @Transactional
	 @SuppressWarnings("unchecked")
	 public List<ContPerson> getContPerson(){
		 List<ContPerson> mobList = new ArrayList<ContPerson>();
		 try{
			 session = this.sessionFactory.openSession();
	         transaction = session.beginTransaction();
	         mobList = session.createCriteria(ContPerson.class).list();
	         transaction.commit();
	         session.flush();
		 }catch (Exception e) {
			 e.printStackTrace();
		 }
		 session.clear();
		 session.close();
		 return mobList;
	}
	 
	 @Transactional
	    public int saveOwnMobNo(ContPerson contPerson){
	        int temp;
	        try{
	            session = this.sessionFactory.openSession();
	            transaction =  session.beginTransaction();
	            session.save(contPerson);
	            transaction.commit();
	            temp=1;
	            session.flush();
	       }
	       catch(Exception e){
	           e.printStackTrace();
	           temp=-1;
	       }
	        session.clear();
	        session.close();
	        return temp;
	        
	   }
		
	
	 @Transactional
	 @SuppressWarnings("unchecked")
	 public List<String> getMobList(String code){
		 System.out.println("enter into getMobList function");
		 List<ContPerson> list = new ArrayList<ContPerson>();
		 List<String> moblist = new ArrayList<String>();
		 try{
			 session = this.sessionFactory.openSession();
	         transaction = session.beginTransaction();
	         Criteria cr=session.createCriteria(ContPerson.class);
	        /* mobList = session.createCriteria(ContPerson.class).list();*/
	         cr.add(Restrictions.eq(ContPersonCNTS.CP_REF_CODE,code));
	         list = cr.list();
	         if(!list.isEmpty()){
	        	 ContPerson contPerson = list.get(0);
	        	 moblist = contPerson.getCpMobile();
	         }
	         transaction.commit();
	         session.flush();
		 }catch (Exception e) {
			 e.printStackTrace();
		 }
		 session.clear();
		 session.close();
		 return moblist;
	}
	 
	 @Transactional
	 @SuppressWarnings("unchecked")
	 public int addMoreMobNo(String mobno, String code)
	 {
		 System.out.println("Enter into addMoreMobNo function");
		 int temp = -1;
		 List<ContPerson> contlist = new ArrayList<ContPerson>();
		 List<String> moblist = new ArrayList<String>();
		 try{
			 session = this.sessionFactory.openSession();
	         transaction = session.beginTransaction();
	         Criteria cr = session.createCriteria(ContPerson.class);   
	         cr.add(Restrictions.eq(ContPersonCNTS.CP_REF_CODE,code));
	         contlist =cr.list();
	         
	         if(!contlist.isEmpty())
	         {
	        	 ContPerson contPerson = contlist.get(0);
	        	 moblist = contPerson.getCpMobile();
	        	 if(moblist != null && !moblist.isEmpty())
	        	 {	
	        		 		if(moblist.size()>=10)
	        		 		{
	        		 			moblist.set(9, mobno);
	        		 			temp = 0;
	        		 		}
	        		 		else{
					        		 moblist.add(mobno);
					        		 session.update(contPerson);
					        		 temp = 0;
	        		 			} 
	        	 }else{
	        		 //moblist.add(mobno);
	        		 ArrayList<String> phNoList = new ArrayList<String>();
	        		 phNoList.add(mobno);
	        		 contPerson.setCpMobile(phNoList);
	        		 session.update(contPerson);
	        		 temp = 0;
	        	 }
	         }   
	         transaction.commit();
	         session.flush();
		 }catch (Exception e) {
			 e.printStackTrace();
		 }
		 session.clear();
		 session.close();
		 return temp;
	 }
	 
	 @Transactional
	 @SuppressWarnings("unchecked")
	 public List<ContPerson> getContactPerson(String Code){
		 List<ContPerson> contPersons = new ArrayList<ContPerson>();
		 System.out.println("Enter into getContactPerson function--- "+Code);
		 try{
			 session = this.sessionFactory.openSession();
	         transaction = session.beginTransaction();
	         Criteria cr = session.createCriteria(ContPerson.class);   
	         cr.add(Restrictions.eq(ContPersonCNTS.CP_REF_CODE,Code));
	         contPersons =cr.list();
	         transaction.commit();
	         session.flush();
		 }catch (Exception e) {
			 e.printStackTrace();
		 }
		 session.clear();
		 session.close();
		 return contPersons;
	}
	 
	 @Transactional
	 public int updateContPerson(ContPerson contPerson){
 		int temp;
 		Date date = new Date();
 		Calendar calendar = Calendar.getInstance();
 		calendar.setTime(date);
 		System.out.println("----------contPerson.getCpRefCode()"+contPerson.getCpRefCode());
		 try{
			 session = this.sessionFactory.openSession();
			 Criteria cr=session.createCriteria(ContPerson.class);
			 cr.add(Restrictions.eq(ContPersonCNTS.CP_REF_CODE,contPerson.getCpRefCode()));
			 transaction = session.beginTransaction();
			 contPerson.setCreationTS(calendar);
			 session.update(contPerson);
			 transaction.commit();
			 session.flush();
			 temp= 1;
		 }
		 catch(Exception e){
			 e.printStackTrace();
			 temp= -1;
		 }		
		 session.clear();
		 session.close();
		 return temp;
	 }
}
