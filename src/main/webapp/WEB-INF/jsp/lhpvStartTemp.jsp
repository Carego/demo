<div ng-show="operatorLogin || superAdminLogin">
	<div class="row">
		<form name="voucherForm" ng-submit=voucherSubmit(voucherForm) class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
		     		
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="branchId" name ="branchName" ng-model="branch.branchName"  readonly ng-required="true" >
		       			<label for="code">Branch</label>	
		       		</div>

		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="lhpvNameId" name ="lhpvName" ng-model="lt.ltLhpvNo" ng-click="selLhpvNo()" readonly ng-required="true" >
		       			<label for="code">Lhpv No</label>	
		       		</div>
		       		
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="date" id="lhpvDtId" name ="lhpvDtName" ng-model="ltDate" ng-required="true" >
		       			<label for="code">Lhpv Date</label>	
		       		</div>
		    </div>
		   
	      <div class="row">
	     		<div class="input-field col s12 center">
	       				<input class="validate" type ="submit" id="saveId" value="Start" disabled="disabled">
	       		</div>
	      </div>		
	     
	</form>
  </div> 
  
  <div id ="selLhpvDBId" ng-hide="selLhpvDBFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Lhpv No</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="lhpv in ltList | filter:filterTextbox">
		 	  <td><input type="radio"  name="lhpv"   value="{{ lhpv }}" ng-model="lhpvCode" ng-click="saveLhpvNo(lhpv)"></td>
              <td>{{lhpv.ltLhpvNo}}</td>
          </tr>
      </table> 
	</div>
	
</div>
