package com.mylogistics.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="owner_master")
public class OwnerMaster {
	
		@Id
		@Column(name="master_Id")
		@GeneratedValue(strategy=GenerationType.IDENTITY)
		private int master_Id;
		
		/*@Column(name="Log_Id",unique=true)
		private int Log_Id;*/
		
		@Column(name="phone_No")
		private long phone_No;
		
		@Column
		private String branch;
		
		@Column
		private String selected_States_of_North;
		
		@Column
		private String selected_States_of_South;
		
		@Column
		private String selected_States_of_Central;
		
		@Column
		private String selected_States_of_West;
		
		@Column
		private String selected_States_of_East;
		
		@Column
		private String selected_States_of_NorthEast;
		
		@Column
		private String other_Nations;
		
		@Column
		private String no_of_Chakka_per_Vehicle;
		
		@Column
		private String type_of_Truck;
		
		@Column
		private String type_of_Container;
		
		@Column
		private String type_of_Trailor_and_Platform;
		
		@Column
		private String type_of_Vendor;
		
		@Column
		private String number_of_Owned_Vehicles;
		
		@Column
		private String number_of_Vehicles_in_Group;
		
		@Column
		private String number_of_Persons_in_Group;
		
		@Column
		private String company_Name;
		
		@Column
		private String company_Owner_Name;
		
		@Column
		private String company_Owner_Mobile_1;
		
		@Column
		private String company_Owner_Mobile_2;
		
		@Column
		private String company_Office_Address;

		public int getMaster_Id() {
			return master_Id;
		}

		public void setMaster_Id(int master_Id) {
			this.master_Id = master_Id;
		}

		public long getPhone_No() {
			return phone_No;
		}

		public void setPhone_No(long phone_No) {
			this.phone_No = phone_No;
		}

		public String getBranch() {
			return branch;
		}

		public void setBranch(String branch) {
			this.branch = branch;
		}

		public String getSelected_States_of_North() {
			return selected_States_of_North;
		}

		public void setSelected_States_of_North(String selected_States_of_North) {
			this.selected_States_of_North = selected_States_of_North;
		}

		public String getSelected_States_of_South() {
			return selected_States_of_South;
		}

		public void setSelected_States_of_South(String selected_States_of_South) {
			this.selected_States_of_South = selected_States_of_South;
		}

		public String getSelected_States_of_Central() {
			return selected_States_of_Central;
		}

		public void setSelected_States_of_Central(String selected_States_of_Central) {
			this.selected_States_of_Central = selected_States_of_Central;
		}

		public String getSelected_States_of_West() {
			return selected_States_of_West;
		}

		public void setSelected_States_of_West(String selected_States_of_West) {
			this.selected_States_of_West = selected_States_of_West;
		}

		public String getSelected_States_of_East() {
			return selected_States_of_East;
		}

		public void setSelected_States_of_East(String selected_States_of_East) {
			this.selected_States_of_East = selected_States_of_East;
		}

		public String getSelected_States_of_NorthEast() {
			return selected_States_of_NorthEast;
		}

		public void setSelected_States_of_NorthEast(String selected_States_of_NorthEast) {
			this.selected_States_of_NorthEast = selected_States_of_NorthEast;
		}

		public String getOther_Nations() {
			return other_Nations;
		}

		public void setOther_Nations(String other_Nations) {
			this.other_Nations = other_Nations;
		}

		public String getNo_of_Chakka_per_Vehicle() {
			return no_of_Chakka_per_Vehicle;
		}

		public void setNo_of_Chakka_per_Vehicle(String no_of_Chakka_per_Vehicle) {
			this.no_of_Chakka_per_Vehicle = no_of_Chakka_per_Vehicle;
		}

		public String getType_of_Truck() {
			return type_of_Truck;
		}

		public void setType_of_Truck(String type_of_Truck) {
			this.type_of_Truck = type_of_Truck;
		}

		public String getType_of_Container() {
			return type_of_Container;
		}

		public void setType_of_Container(String type_of_Container) {
			this.type_of_Container = type_of_Container;
		}

		public String getType_of_Trailor_and_Platform() {
			return type_of_Trailor_and_Platform;
		}

		public void setType_of_Trailor_and_Platform(String type_of_Trailor_and_Platform) {
			this.type_of_Trailor_and_Platform = type_of_Trailor_and_Platform;
		}

		public String getType_of_Vendor() {
			return type_of_Vendor;
		}

		public void setType_of_Vendor(String type_of_Vendor) {
			this.type_of_Vendor = type_of_Vendor;
		}

		public String getNumber_of_Owned_Vehicles() {
			return number_of_Owned_Vehicles;
		}

		public void setNumber_of_Owned_Vehicles(String number_of_Owned_Vehicles) {
			this.number_of_Owned_Vehicles = number_of_Owned_Vehicles;
		}

		public String getNumber_of_Vehicles_in_Group() {
			return number_of_Vehicles_in_Group;
		}

		public void setNumber_of_Vehicles_in_Group(String number_of_Vehicles_in_Group) {
			this.number_of_Vehicles_in_Group = number_of_Vehicles_in_Group;
		}

		public String getNumber_of_Persons_in_Group() {
			return number_of_Persons_in_Group;
		}

		public void setNumber_of_Persons_in_Group(String number_of_Persons_in_Group) {
			this.number_of_Persons_in_Group = number_of_Persons_in_Group;
		}

		public String getCompany_Name() {
			return company_Name;
		}

		public void setCompany_Name(String company_Name) {
			this.company_Name = company_Name;
		}

		public String getCompany_Owner_Name() {
			return company_Owner_Name;
		}

		public void setCompany_Owner_Name(String company_Owner_Name) {
			this.company_Owner_Name = company_Owner_Name;
		}

		public String getCompany_Owner_Mobile_1() {
			return company_Owner_Mobile_1;
		}

		public void setCompany_Owner_Mobile_1(String company_Owner_Mobile_1) {
			this.company_Owner_Mobile_1 = company_Owner_Mobile_1;
		}

		public String getCompany_Owner_Mobile_2() {
			return company_Owner_Mobile_2;
		}

		public void setCompany_Owner_Mobile_2(String company_Owner_Mobile_2) {
			this.company_Owner_Mobile_2 = company_Owner_Mobile_2;
		}

		public String getCompany_Office_Address() {
			return company_Office_Address;
		}

		public void setCompany_Office_Address(String company_Office_Address) {
			this.company_Office_Address = company_Office_Address;
		}

		/*public int getLog_Id() {
			return Log_Id;
		}

		public void setLog_Id(int log_Id) {
			Log_Id = log_Id;
		}*/

		

}
