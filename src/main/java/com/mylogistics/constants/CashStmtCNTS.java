package com.mylogistics.constants;

public class CashStmtCNTS {

	public static final String CS_CSS_ID = "cssId";
	
	public static final String CS_ID = "csId";
	
	public static final String CS_NO = "csNo";
	
	public static final String CS_DESCRIPTION = "csDescription";
	
	public static final String CS_TV_NO = "csTvNo";
	
	public static final String CS_BRANCH_CODE = "csBranchCode";
	
	public static final String CS_DR_CR = "csDrCr";
	
	public static final String C_LEAVES = "cLeaves";
	
	public static final String CS_VOUCH_NO = "csVouchNo";
	
	public static final String CS_VOUCH_TYPE = "csVouchType";
	
	public static final String CS_PAY_TO = "csPayTo";
	
	public static final String CS_CTS = "creationTS";
	
	public static final String CS_CDT = "csDt";
	
	public static final String USER_BRANCH_CODE = "bCode";
	
	public static final String CS_FA_CODE = "csFaCode";
	
	public static final String CS_DT = "csDt";
	
	public static final String CS_AMT = "csAmt";
	
	public static final String CS_TYPE = "csType";
	
	public static final String CS_UPDATE = "csUpdate";
	
	public static final String PAY_MODE = "payMode";
}
