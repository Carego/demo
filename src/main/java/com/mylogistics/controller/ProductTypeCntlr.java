package com.mylogistics.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.ProductTypeDAO;
import com.mylogistics.model.ProductType;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;


@Controller
public class ProductTypeCntlr {

	@Autowired
	private ProductTypeDAO producttypeDAO;
	
	@Autowired
	private HttpSession httpSession;
	
	@RequestMapping(value = "/submitProductType", method = RequestMethod.POST)
    public @ResponseBody Object submitProductType(@RequestBody ProductType producttype){   
       Map<String,String> map = new HashMap<String,String>();
       
       User currentUser = (User)httpSession.getAttribute("currentUser");
       producttype.setUserCode(currentUser.getUserCode());
       producttype.setbCode(currentUser.getUserBranchCode());
       
       int temp=producttypeDAO.saveProductTypeToDB(producttype);
    	if (temp>=0) {
    		  map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			  map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;	
	}
	
	@RequestMapping(value="/getProductTypeDetails",method = RequestMethod.POST)
	public @ResponseBody Object getProductTypeDetails(){
		
		List<ProductType> producttypeList = producttypeDAO.getProductTypeData();
		
		Map<String,Object> map = new HashMap<String,Object>();
		if (!(producttypeList.isEmpty())) {
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("list",producttypeList);	
  		}else {
  			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;	
	}
	
	 @RequestMapping(value = "/updateProductType", method = RequestMethod.POST)
	 public @ResponseBody Object updateProductType(@RequestBody ProductType producttype){
		 System.out.println("Entered into updateProductType of controller----");
		 	
		 Map<String,String> map = new HashMap<String, String>();
		 		
		 int temp=producttypeDAO.updateProductType(producttype);
		 if (temp>=0) {
	        	
			 map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		 } else {
			 map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		 }	    	
		 return map;
	 }
	 
	 @RequestMapping(value="/getProductTypeList", method=RequestMethod.POST)
	 public @ResponseBody Object getProductTypeList(){
		 System.out.println("getProductTypeList");
		 Map<String, Object> map = new HashMap<>();
		 
		 List<String> productTypeList = producttypeDAO.getProductName();
		 
		 if (!productTypeList.isEmpty()) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("productTypeList", productTypeList);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		 
		 return map;
	 }
}
