package com.mylogistics.services.mail;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailParseException;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Service()
public class SmtpMailSender {
 
//	@Autowired
//	private JavaMailSenderImpl mailSender; // MailSender interface defines a strategy for sending simple mails
	
	/*
	
	public void sendEmail(String toMailId, String subject, String msgBody) {

		MimeMessage message = mailSender.createMimeMessage();
			
		try{
			MimeMessageHelper helper = new MimeMessageHelper(message, true);
			
			helper.setFrom(mailSender.getUsername());
			helper.setTo(toMailId);
			helper.setSubject(subject);
			helper.setText(msgBody);
			
		}catch (MessagingException e) {
			throw new MailParseException(e);
		}
		mailSender.send(message);
	}
	
	public void sendEmailWithAttachment(String toMailId, String subject, String msgBody, String filePath) {

		MimeMessage message = mailSender.createMimeMessage();
			
		try{
			MimeMessageHelper helper = new MimeMessageHelper(message, true);
			
			helper.setFrom(mailSender.getUsername());
			helper.setTo(toMailId);
			helper.setSubject(subject);
			helper.setText(msgBody);
			
			FileSystemResource fsr = new FileSystemResource(filePath);
			helper.addAttachment(fsr.getFilename(), fsr);
			
		}catch (MessagingException e) {
			throw new MailParseException(e);
		}
		mailSender.send(message);
	}
	*/
	
	

	public int sendCnmtOnMail( List<String> to,List<String> bcc, String subject, String text,
			String filePath) {

		Properties properties = new Properties();
		properties.put("mail.smtp.host", "smtp.gmail.com");
		properties.put("mail.smtp.socketFactory.port", "465");
		properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.port", "465");
		final String from="notifications@carego.co";
        final String password="carego@admin";

		// creating session
		Session session = Session.getInstance(properties, new Authenticator() {
			protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
				System.out.println("gmail=" + from + " to" + password);
				return new javax.mail.PasswordAuthentication(from, password);

			}
		});
		// compose message

		MimeMessage message = new MimeMessage(session);
		try {

			String receivers = "";
			for (String str : to) {
				if (receivers.equalsIgnoreCase("")) {
					receivers = str;
				} else {
					receivers = receivers + "," + str;
				}

			}
			
			String bccReceivers = "";
			for (String str : bcc) {
				if (bccReceivers.equalsIgnoreCase("")) {
					bccReceivers = str;
				} else {
					bccReceivers = bccReceivers + "," + str;
				}

			}

			message.addRecipients(Message.RecipientType.TO, InternetAddress.parse(receivers));
			message.addRecipients(Message.RecipientType.BCC, InternetAddress.parse(bccReceivers));
			message.setSubject(subject);
			// message.setText(text);

			BodyPart messageBodyPart1 = new MimeBodyPart();
			messageBodyPart1.setText(text);

			// 4) create new MimeBodyPart object and set DataHandler object to this object
			MimeBodyPart messageBodyPart = new MimeBodyPart();

			// BodyPart messageBodyPart = new MimeBodyPart();
			Multipart multipart = new MimeMultipart();
			messageBodyPart = new MimeBodyPart();
			// String filename = "/var/www/html/Erp_Image/BANK/DailyTrnsfr/CNMTexcel.xls";
			DataSource source = new FileDataSource(filePath);
			messageBodyPart.setDataHandler(new DataHandler(source));
			messageBodyPart.setFileName("CNMT.pdf");
			multipart.addBodyPart(messageBodyPart1);
			multipart.addBodyPart(messageBodyPart);
			message.setContent(multipart);

			// sending mail

			Transport.send(message);
			System.out.println("Success");

		} catch (AddressException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;

	}

	
	public void sendSMS(List<String> phoneNoList,String msg) throws IOException {

		  String myPasscode = "pwd2018";
	      String myUsername = "CareGo";    
	      String myMessage = URLEncoder.encode(msg, "UTF-8");
	      if(!phoneNoList.isEmpty())
	     for(String toPhoneNumber:phoneNoList) {
	    	 String requestUrl="http://www.apiconnecto.com/API/SMSHttp.aspx?UserId="+myUsername+"&pwd="+myPasscode+"&message="+myMessage+"&Contacts="+toPhoneNumber+"&SenderId=CAREGO&ServiceName=SMSTRANS";
	         System.out.println(requestUrl+"5ssss");
	         URL url = new URL(requestUrl);
	         HttpURLConnection uc = (HttpURLConnection)url.openConnection();
	         System.out.println(uc.getResponseMessage()+" ssss");
	         String resp=uc.getResponseMessage();
	         System.out.println(resp);

	   		uc.disconnect();
	     }
	    
		
	}
	
	
	
	
}
