package com.mylogistics.DAOImpl.report;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.report.EnquiryDAO;
import com.mylogistics.constants.ArrivalReportCNTS;
import com.mylogistics.constants.BillCNTS;
import com.mylogistics.constants.BillDetailCNTS;
import com.mylogistics.constants.CashStmtCNTS;
import com.mylogistics.constants.ChallanCNTS;
import com.mylogistics.constants.ChallanDetailCNTS;
import com.mylogistics.constants.CnmtCNTS;
import com.mylogistics.constants.Cnmt_ChallanCNTS;
import com.mylogistics.constants.FAMasterCNTS;
import com.mylogistics.constants.LhpvAdvCNTS;
import com.mylogistics.constants.LhpvBalCNTS;
import com.mylogistics.constants.LhpvCashSmryCNTS;
import com.mylogistics.constants.LhpvStatusCNTS;
import com.mylogistics.constants.LhpvSupCNTS;
import com.mylogistics.constants.MoneyReceiptCNTS;
import com.mylogistics.constants.NarrationCNTS;
import com.mylogistics.model.ArrivalReport;
import com.mylogistics.model.Bill;
import com.mylogistics.model.BillDetail;
import com.mylogistics.model.BillForwarding;
import com.mylogistics.model.Branch;
import com.mylogistics.model.CashStmt;
import com.mylogistics.model.Challan;
import com.mylogistics.model.ChallanDetail;
import com.mylogistics.model.Cnmt;
import com.mylogistics.model.Cnmt_Challan;
import com.mylogistics.model.Customer;
import com.mylogistics.model.FAMaster;
import com.mylogistics.model.MoneyReceipt;
import com.mylogistics.model.Narration;
import com.mylogistics.model.Station;
import com.mylogistics.model.bank.BankMstr;
import com.mylogistics.model.lhpv.LhpvAdv;
import com.mylogistics.model.lhpv.LhpvBal;
import com.mylogistics.model.lhpv.LhpvCashSmry;
import com.mylogistics.model.lhpv.LhpvStatus;
import com.mylogistics.model.lhpv.LhpvSup;
import com.mylogistics.services.CodePatternService;

public class EnquiryDAOImpl implements EnquiryDAO{

	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	@Autowired
	public EnquiryDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	@Transactional
	public List<String> getCodeListByType(String branchCode, String type) {
		System.out.println("getCodeListByTypeDB: "+branchCode+" "+type);
		List<String> typeCodeList = new ArrayList<>();
		
		try {
			session = this.sessionFactory.openSession();
			if (type.equalsIgnoreCase("cnmt")) {
				//get cnmt list
				Criteria cr = session.createCriteria(Cnmt.class);
				cr.add(Restrictions.eq(CnmtCNTS.USER_BRANCH_CODE,branchCode));
				
				ProjectionList projList = Projections.projectionList();
				projList.add(Projections.property(CnmtCNTS.CNMT_CODE));
				cr.setProjection(projList);
				
				typeCodeList = cr.list();
				
			} else if (type.equalsIgnoreCase("chln")) {
				//get chln list
				Criteria cr = session.createCriteria(Challan.class);
				cr.add(Restrictions.eq(ChallanCNTS.USER_BRANCH_CODE, branchCode));
				
				ProjectionList projList = Projections.projectionList();
				projList.add(Projections.property(ChallanCNTS.CHALLAN_CODE));
				cr.setProjection(projList);
				
				typeCodeList = cr.list();
				
			}else if (type.equalsIgnoreCase("loryNo")) {
				//get Lorry No. List
				Criteria cr = session.createCriteria(Challan.class);
				cr.add(Restrictions.eq(ChallanCNTS.USER_BRANCH_CODE, branchCode));
				
				ProjectionList projList = Projections.projectionList();
				projList.add(Projections.property(ChallanCNTS.CHLN_LRY_NO));
				cr.setProjection(projList);
				
				typeCodeList = cr.list();
				System.out.println("size before"+typeCodeList.size());
				//remove duplicates
				if (!typeCodeList.isEmpty()) {
					Set<String> set = new HashSet<>();
					set.addAll(typeCodeList);
					typeCodeList.clear();
					typeCodeList.addAll(set);
				}
				System.out.println("size after"+typeCodeList.size());
				
			}else if (type.equalsIgnoreCase("sedr")) {
				//get sedr list
				Criteria cr = session.createCriteria(ArrivalReport.class);
				cr.add(Restrictions.eq(ArrivalReportCNTS.USER_BRANCH_CODE, branchCode));
				
				ProjectionList projList = Projections.projectionList();
				projList.add(Projections.property(ArrivalReportCNTS.AR_CODE));
				cr.setProjection(projList);
				
				typeCodeList = cr.list();
				
			} else if (type.equalsIgnoreCase("bill")) {
				//get bill list
				Criteria cr = session.createCriteria(Bill.class);
				cr.add(Restrictions.eq(BillCNTS.USER_BRANCH_CODE, branchCode));
				
				ProjectionList projList = Projections.projectionList();
				projList.add(Projections.property(BillCNTS.Bill_NO));
				cr.setProjection(projList);
				
				typeCodeList = cr.list();
			} else if (type.equalsIgnoreCase("mr")) {
				//get mr list
				Criteria cr = session.createCriteria(MoneyReceipt.class);
				cr.add(Restrictions.eq(MoneyReceiptCNTS.USER_BRANCH_CODE, branchCode));
				
				ProjectionList projList = Projections.projectionList();
				projList.add(Projections.property(MoneyReceiptCNTS.MR_NO));
				cr.setProjection(projList);
				
				typeCodeList = cr.list();
				
				//remove duplicates
				if (!typeCodeList.isEmpty()) {
					Set<String> set = new HashSet<>();
					set.addAll(typeCodeList);
					typeCodeList.clear();
					typeCodeList.addAll(set);
				}
			}else if(type.equalsIgnoreCase("chq")){			
				List typeCodeListTemp = new ArrayList();
				typeCodeListTemp = session.createQuery("SELECT DISTINCT(csTvNo) FROM CashStmt WHERE bCode = :bCode AND payMode LIKE :payMode")
						.setParameter("bCode", branchCode)
						.setParameter("payMode", "%Q%")
						.list();				
				Iterator iterator = typeCodeListTemp.iterator();
				session.clear();
				session.flush();
				while(iterator.hasNext()){
					String chq = (String)iterator.next();
					if(chq != null) {
						if(chq.contains("(")){						
							chq = chq.substring((chq.indexOf("(")+1), chq.indexOf(")") );
							typeCodeList.add(chq);						
						}else
							typeCodeList.add(chq);
					}
				}
				System.out.println("Forward From CashStmt");
				List<String> lhpvAdvChqList = session.createQuery("SELECT DISTINCT(lhpvAdv.laChqNo)FROM LhpvAdv lhpvAdv WHERE lhpvAdv.bCode = :bCode AND lhpvAdv.laPayMethod = :payMethod AND lhpvAdv.laIsClose = :close")
						.setParameter("bCode", branchCode)
						.setCharacter("payMethod", 'Q')
						.setInteger("close", 0)						
						.list();
				List<String> lhpvBalChqList = session.createQuery("SELECT DISTINCT(lhpvBal.lbChqNo) FROM LhpvBal lhpvBal WHERE lhpvBal.bCode = :bCode AND lhpvBal.lbPayMethod = :payMethod AND lhpvBal.lbIsClose = :close")
						.setParameter("bCode", branchCode)
						.setCharacter("payMethod", 'Q')
						.setInteger("close", 0)
						.list();
				List<String> lhpvSupChqList = session.createQuery("SELECT DISTINCT(lhpvSup.lspChqNo) FROM LhpvSup lhpvSup WHERE lhpvSup.bCode = :bCode AND lhpvSup.lspPayMethod = :payMethod AND lhpvSup.lspIsClose = :close")
						.setParameter("bCode", branchCode)
						.setCharacter("payMethod", 'Q')
						.setInteger("close", 0)
						.list();
				System.out.println("CashStmt cheque list size : "+typeCodeList.size());				
				if(lhpvAdvChqList != null){
					typeCodeList.addAll(lhpvAdvChqList);
					System.out.println("LHPVAdv cheque list size : "+lhpvAdvChqList.size());
				}
				if(lhpvBalChqList != null){
					typeCodeList.addAll(lhpvBalChqList);
					System.out.println("LHPVBal cheque list size : "+lhpvBalChqList.size());
				}
				if(lhpvSupChqList != null){
					typeCodeList.addAll(lhpvSupChqList);
					System.out.println("LHPVSup cheque list size : "+lhpvSupChqList.size());
				}				
				// Remove duplicate entry for cheque  if exists
				if(!typeCodeList.isEmpty()){
					Set<String> set = new HashSet<String>();
					set.addAll(typeCodeList);
					typeCodeList.clear();
					typeCodeList.addAll(set);
					Collections.sort(typeCodeList);
				}				
				System.out.println("Total cheque list size : "+typeCodeList.size());				
			}
			
			session.flush();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		session.clear();
		session.close();
		
		return typeCodeList;
	}

	@Override
	@Transactional
	public List<Map<String, Object>> getEnquiry(String type, String enquiryCode, String faCodeFrom, String faCodeTo, String fromDate, String toDate) {		
		System.out.println("type="+type);
		System.out.println(type+"No="+enquiryCode);
		List<Map<String, Object>> enquiryList = new ArrayList<>();
		Map<String,Object> mapChln=new HashMap<>();
		try {
			session = this.sessionFactory.openSession();
			if (type.equalsIgnoreCase("cnmt")) {
				Criteria cr = session.createCriteria(Cnmt.class);
				cr.add(Restrictions.eq(CnmtCNTS.CNMT_CODE, enquiryCode));
				List<Cnmt> cnmtList = cr.list();
				System.out.println("size="+cnmtList.size());
				System.out.println("cnmtList.isEmpty()"+cnmtList.isEmpty());
				if (!cnmtList.isEmpty()) {
					if(cnmtList.get(0).isCancel()){
						mapChln.put("cnmtCode", cnmtList.get(0).getCnmtCode());
						mapChln.put("bCode", cnmtList.get(0).getbCode());
						mapChln.put("isCCA", cnmtList.get(0).getIsCCA());
						mapChln.put("cnmtDt", cnmtList.get(0).getCnmtDt());
						mapChln.put("isCancel", cnmtList.get(0).isCancel());
						mapChln.put("creationTS", cnmtList.get(0).getCreationTS());
						
						List<Narration> cnmtNarr = session.createCriteria(Narration.class)
								.add(Restrictions.eq(NarrationCNTS.NARR_CODE, enquiryCode))
								.add(Restrictions.eq(NarrationCNTS.NARR_TYPE, "cnmt"))
								.list();
						if(! cnmtNarr.isEmpty())
							mapChln.put("cnmtNarr", cnmtNarr.get(0).getNarrDesc());
						else
							mapChln.put("cnmtNarr", "");
						
						enquiryList.add(mapChln);						
						session.clear();
						session.close();
						return enquiryList;
					}
					
					Cnmt cnmt = cnmtList.get(0);
					Map<String, Object> map = getEnquiryByCnmt(cnmt, session, type, enquiryCode);
					enquiryList.add(map);
				}
				
			} else if (type.equalsIgnoreCase("chln")) {
				Criteria cr = session.createCriteria(Challan.class);
				cr.add(Restrictions.eq(ChallanCNTS.CHALLAN_CODE, enquiryCode));
				List<Challan> chlnList = cr.list();
				
				
				if (!chlnList.isEmpty()) {
					if(chlnList.get(0).isCancel()){
						
						List<Map<String, Object>> myList = new ArrayList<Map<String,Object>>();
						Map<String, Object> map = new HashMap<String, Object>();
						
						// By Mohd Furkan ----- For adding Narration
						List<Narration> chlnNarr = session.createCriteria(Narration.class)
								.add(Restrictions.eq(NarrationCNTS.NARR_CODE, enquiryCode))
								.add(Restrictions.eq(NarrationCNTS.NARR_TYPE, "chln"))
								.list();
						if(! chlnNarr.isEmpty())
							map.put("chlnNarr", chlnNarr.get(0).getNarrDesc());
						else
							map.put("chlnNarr", "");
						map.put("chlnCode", chlnList.get(0).getChlnCode());
						map.put("bCode", chlnList.get(0).getbCode());
						map.put("chlnDt", chlnList.get(0).getChlnDt());
						map.put("chlnLryNo", chlnList.get(0).getChlnLryNo());
						map.put("chlnTotalWt",chlnList.get(0).getChlnTotalWt());
						map.put("chlnChgWt",chlnList.get(0).getChlnChgWt());
						map.put("creationTS",chlnList.get(0).getCreationTS());
						map.put("chlnHeight", chlnList.get(0).getChlnHeight());
						
						myList.add(map);						
						mapChln.put("chlnList", myList);
						
						
						enquiryList.add(mapChln);
						session.clear();
						session.close();
						return enquiryList;
					}
					Challan challan = chlnList.get(0);
					cr = session.createCriteria(Cnmt_Challan.class);
					cr.add(Restrictions.eq(Cnmt_ChallanCNTS.Cnmt_Chln_chlnId, challan.getChlnId()));
					
					ProjectionList projectionList = Projections.projectionList();
					projectionList.add(Projections.property(Cnmt_ChallanCNTS.Cnmt_Chln_cnmtId));
					
					cr.setProjection(projectionList);
					
					List<Integer> cnmtIdList = cr.list();
					
					if (!cnmtIdList.isEmpty()) {
						for (Integer cnmtId : cnmtIdList) {
							Cnmt cnmt = (Cnmt) session.get(Cnmt.class, cnmtId);
							Map<String, Object> map = getEnquiryByCnmt(cnmt, session, type, enquiryCode);
							enquiryList.add(map);
						}
					}
				}
				
			} else if (type.equalsIgnoreCase("loryNo")) {
				Criteria cr = session.createCriteria(Challan.class);
				cr.add(Restrictions.eq(ChallanCNTS.CHLN_LRY_NO, enquiryCode));
				
				ProjectionList prjctionList = Projections.projectionList();
				prjctionList.add(Projections.property(ChallanCNTS.CHLN_ID));
				
				cr.setProjection(prjctionList);
				
				List<Integer> chlnList = cr.list();
				System.out.println("List SIze"+chlnList.size());
				
				if (!chlnList.isEmpty()) {
					for (Integer chlnId : chlnList) {
						Challan challan = (Challan) session.get(Challan.class, chlnId);
						cr = session.createCriteria(Cnmt_Challan.class);
						cr.add(Restrictions.eq(Cnmt_ChallanCNTS.Cnmt_Chln_chlnId, challan.getChlnId()));
						
						ProjectionList projectionList = Projections.projectionList();
						projectionList.add(Projections.property(Cnmt_ChallanCNTS.Cnmt_Chln_cnmtId));
						
						cr.setProjection(projectionList);
						
						List<Integer> cnmtIdList = cr.list();
						
						if (!cnmtIdList.isEmpty()) {
							for (Integer cnmtId : cnmtIdList) {
								Cnmt cnmt = (Cnmt) session.get(Cnmt.class, cnmtId);
								Map<String, Object> map = getEnquiryByCnmt(cnmt, session, type, enquiryCode);
								enquiryList.add(map);
							}
						}
					}
					
				}
				
			} else if (type.equalsIgnoreCase("sedr")) {
				Criteria cr = session.createCriteria(ArrivalReport.class);
				cr.add(Restrictions.eq(ArrivalReportCNTS.AR_CODE, enquiryCode));
				
				List<ArrivalReport> arrivalReportList = cr.list();
				
				if (!arrivalReportList.isEmpty()) {
					
					if(arrivalReportList.get(0).isCancel()){						
						mapChln.put("arCode", arrivalReportList.get(0).getArCode());
						mapChln.put("branchCode", arrivalReportList.get(0).getbCode());
						mapChln.put("arDt", arrivalReportList.get(0).getArDt());
						mapChln.put("isCancel", arrivalReportList.get(0).isCancel());
						mapChln.put("creationTS", arrivalReportList.get(0).getCreationTS());
						
						List<Narration> cnmtNarr = session.createCriteria(Narration.class)
								.add(Restrictions.eq(NarrationCNTS.NARR_CODE, enquiryCode))
								.add(Restrictions.eq(NarrationCNTS.NARR_TYPE, "sedr"))
								.list();
						if(! cnmtNarr.isEmpty())
							mapChln.put("arNarr", cnmtNarr.get(0).getNarrDesc());
						else
							mapChln.put("arNarr", "");
						
						enquiryList.add(mapChln);
						session.flush();
						session.clear();
						session.close();
						return enquiryList;
					}
					
					ArrivalReport arrivalReport = arrivalReportList.get(0);
					cr = session.createCriteria(Challan.class);
					cr.add(Restrictions.eq(ChallanCNTS.CHALLAN_CODE, arrivalReport.getArchlnCode()));
					List<Challan> chlnList = cr.list();
					
					if (!chlnList.isEmpty()) {
						Challan challan = chlnList.get(0);
						cr = session.createCriteria(Cnmt_Challan.class);
						cr.add(Restrictions.eq(Cnmt_ChallanCNTS.Cnmt_Chln_chlnId, challan.getChlnId()));
						
						ProjectionList projectionList = Projections.projectionList();
						projectionList.add(Projections.property(Cnmt_ChallanCNTS.Cnmt_Chln_cnmtId));
						
						cr.setProjection(projectionList);
						
						List<Integer> cnmtIdList = cr.list();
						
						if (!cnmtIdList.isEmpty()) {
							for (Integer cnmtId : cnmtIdList) {
								Cnmt cnmt = (Cnmt) session.get(Cnmt.class, cnmtId);
								Map<String, Object> map = getEnquiryByCnmt(cnmt, session, type, enquiryCode);
								enquiryList.add(map);
							}
						}
					}
				}
				
			} else if (type.equalsIgnoreCase("bill")) {
				System.out.println("billBlock....");
				System.out.println("billno="+enquiryCode);				
				Criteria cr = session.createCriteria(Bill.class);
				cr.add(Restrictions.eq(BillCNTS.Bill_NO, enquiryCode));				
				
				List<Bill> blList = cr.list();
				System.out.println("blList size="+blList.size());
				if (!blList.isEmpty()) {
					Bill bill = blList.get(0);
					
					List<BillDetail> billDetailList = bill.getBillDetList();
					System.out.println("billDetailList="+billDetailList.size());
					if (!billDetailList.isEmpty()) {
						for (BillDetail billDetail : billDetailList) {
							Cnmt cnmt = (Cnmt) session.get(Cnmt.class, billDetail.getBdCnmtId());
							Map<String, Object> map = getEnquiryByCnmt(cnmt, session, type, enquiryCode);
							enquiryList.add(map);
						}
					}
				}
				
			} else if (type.equalsIgnoreCase("mr")) {				
				enquiryList = getMrByMrNo(type, enquiryCode);				
			}else if(type.equalsIgnoreCase("chq")){
				enquiryList = getEnquiryByChq(enquiryCode);			
			}else if(type.equalsIgnoreCase("amt")){
				enquiryList = getEnquiryByAmt(type, enquiryCode, faCodeFrom, faCodeTo, fromDate, toDate);
			}
			session.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return enquiryList;
	}
	
	
	private Map<String, Object> getEnquiryByCnmt(Cnmt cnmt, Session session, String type, String enquiryCode){
		
		System.out.println("cnmtId: "+cnmt.getCnmtId());
		
		Map<String, Object> map = new HashMap<>();
		map.put(CnmtCNTS.CNMT_ID, cnmt.getCnmtId());
		map.put(CnmtCNTS.CNMT_CODE, cnmt.getCnmtCode());
		map.put("bCode", cnmt.getbCode());
		map.put(CnmtCNTS.CNMT_DT, cnmt.getCnmtDt());
		
		List<Narration> cnmtNarr = session.createCriteria(Narration.class)
				.add(Restrictions.eq(NarrationCNTS.NARR_CODE, cnmt.getCnmtCode()))
				.add(Restrictions.eq(NarrationCNTS.NARR_TYPE, "cnmt"))
				.list();
		if(! cnmtNarr.isEmpty())
			map.put("cnmtNarr", cnmtNarr.get(0).getNarrDesc());
		else
			map.put("cnmtNarr", "");
		
		//add station name
		Station station = (Station) session.get(Station.class, Integer.parseInt(cnmt.getCnmtFromSt()));
		map.put("cnmtFrmStnName", station.getStnName());
		station = (Station) session.get(Station.class, Integer.parseInt(cnmt.getCnmtToSt()));
		map.put("cnmtToStnName", station.getStnName());
		
		
		//add customer name
		Customer cust = (Customer) session.get(Customer.class, Integer.parseInt(cnmt.getCustCode()));
		map.put("custName", cust.getCustName());
		
		//add cngnor name
		cust = (Customer) session.get(Customer.class, Integer.parseInt(cnmt.getCnmtConsignor()));
		map.put("cngnorName", cust.getCustName());
		
		//add cngnee name
		cust = (Customer) session.get(Customer.class, Integer.parseInt(cnmt.getCnmtConsignee()));
		map.put("cngneeName", cust.getCustName());
		
		map.put(CnmtCNTS.CNMT_ACTUAL_WT, cnmt.getCnmtActualWt());
		map.put(CnmtCNTS.CREATION_TS, CodePatternService.getCurrentSqlDateFromUTC(cnmt.getCreationTS()));
		
		//detal data
		map.put(CnmtCNTS.CNMT_NO_OF_PKG, cnmt.getCnmtNoOfPkg());
		map.put(CnmtCNTS.CNMT_GUARANTEE_WT, cnmt.getCnmtGuaranteeWt());
		map.put(CnmtCNTS.CNMT_FREIGHT, cnmt.getCnmtFreight());
		map.put(CnmtCNTS.CNMT_TOT, cnmt.getCnmtTOT());
		map.put(CnmtCNTS.CNMT_DC, cnmt.getCnmtDC().replaceAll("\\s", ""));
		map.put(CnmtCNTS.CNMT_CCA, cnmt.getIsCCA());
		map.put(CnmtCNTS.CNMT_CANCEL, cnmt.isCancel());
		
		//add billAtName
		Branch branch = (Branch) session.get(Branch.class, Integer.parseInt(cnmt.getCnmtBillAt()));
		map.put("cnmtBillAtName", branch.getBranchName().replaceAll("\\s", ""));
		
		//Invoice no as a String
		/*StringBuilder invNos = new StringBuilder();
		if (cnmt.getCnmtInvoiceNo() != null) {
			for (Map<String, Object> invMap: cnmt.getCnmtInvoiceNo()) {
				invNos = invNos.append(invMap.get("invoiceNo")).append("\n");
			}
			System.err.println("invNo: "+invNos.toString());
			map.put(CnmtCNTS.CNMT_INVOICE_NO, invNos.toString().trim());
			System.err.println("invNo: "+invNos.toString().trim());
		}*/
		
		//invoice no as a List
		List<String> invNoList = new ArrayList<>();
		if (cnmt.getCnmtInvoiceNo() != null) {
			for (Map<String, Object> invMap: cnmt.getCnmtInvoiceNo()) {
				invNoList.add(String.valueOf(invMap.get("invoiceNo")));
			}
			map.put(CnmtCNTS.CNMT_INVOICE_NO+"List", invNoList);
		}
		map.put(CnmtCNTS.CNMT_VOG, cnmt.getCnmtVOG());
		map.put(CnmtCNTS.USER_CODE, cnmt.getUserCode());
		
		//add normal bill
		Map<String, Object> normalBillMap = new HashMap<>();
		
		System.out.println("CNMT BILL NO : "+cnmt.getCnmtBillNo());
		
		if (cnmt.getCnmtBillNo() != null) {
			Criteria cr = session.createCriteria(Bill.class);
			//cr.add(Restrictions.eq(BillCNTS.BILL_BRH_ID, Integer.parseInt(branchCode)));
			cr.add(Restrictions.eq(BillCNTS.Bill_NO, cnmt.getCnmtBillNo()));
			List<Bill> billList = cr.list();
			
			if (!billList.isEmpty()) {
				
				Bill billNormal = billList.get(0);
				normalBillMap.put(BillCNTS.BILL_ID, billNormal.getBlId());
				normalBillMap.put(BillCNTS.Bill_NO, billNormal.getBlBillNo());
				normalBillMap.put(BillCNTS.BILL_DT, billNormal.getBlBillDt());
				normalBillMap.put(BillCNTS.BILL_SUB_DT, billNormal.getBlBillSubDt());
				normalBillMap.put(BillCNTS.BILL_FINAL_TOT, billNormal.getBlFinalTot());
				normalBillMap.put(BillCNTS.BILL_TYPE, billNormal.getBlType());
				
				BillForwarding normalBF=billNormal.getBillForwarding();
				
				if(normalBF != null) {
					normalBillMap.put("bfNo", normalBF.getBfNo());
					normalBillMap.put("bfDt", normalBF.getCreationTS());
					normalBillMap.put("bfRecDt", normalBF.getBfRecDt());
				}else {
					normalBillMap.put("bfNo", "");
					normalBillMap.put("bfDt", "");
					normalBillMap.put("bfRecDt", "");
				}
				
				//add Money Receipt List
				List<Map<String, Object>> mrList = new ArrayList<>();
				
				SQLQuery sqlQuery = session.createSQLQuery("SELECT mrId From mr_bill where blId = :blId");
				sqlQuery.setParameter("blId", billNormal.getBlId());
				
				List<Integer> mrIdList = sqlQuery.list();
				
				double blTotAmtByMr = 0;
				
				if (!mrIdList.isEmpty()) {
					for (Integer mrId: mrIdList) {
						Map<String, Object> mrMap = new HashMap<>();
						MoneyReceipt moneyReceipt = (MoneyReceipt) session.get(MoneyReceipt.class, mrId);
						mrMap.put(MoneyReceiptCNTS.MR_ID, moneyReceipt.getMrId());
						mrMap.put(MoneyReceiptCNTS.MR_NO, moneyReceipt.getMrNo());
						mrMap.put(MoneyReceiptCNTS.MR_NET_AMT, moneyReceipt.getMrNetAmt());
						mrMap.put(MoneyReceiptCNTS.MR_Date, moneyReceipt.getMrDate());
						mrList.add(mrMap);
						blTotAmtByMr = blTotAmtByMr + moneyReceipt.getMrNetAmt();
					}
				}
				normalBillMap.put("blTotAmtByMr", blTotAmtByMr);
				normalBillMap.put("mrList", mrList);
			}
			
			//add normal bill detail
			if (cnmt.getCnmtBillDetId() > 0) {
				BillDetail billDetail = (BillDetail) session.get(BillDetail.class, cnmt.getCnmtBillDetId());
				
				if (billDetail != null) {
					//add wt
					if (billDetail.getBdBlBase().equalsIgnoreCase("receiveWt")) {
						normalBillMap.put("bdWt", billDetail.getBdRecWt());
					} else if (billDetail.getBdBlBase().equalsIgnoreCase("actualWt")) {
						normalBillMap.put("bdWt", billDetail.getBdActWt());
					} else { //default wt is chargeWt
						normalBillMap.put("bdWt", billDetail.getBdChgWt());
					}
					
					normalBillMap.put(BillDetailCNTS.BD_ID, billDetail.getBdId());
					normalBillMap.put(BillDetailCNTS.BD_RATE, billDetail.getBdRate());
					normalBillMap.put(BillDetailCNTS.BD_FREIGHT, billDetail.getBdFreight());
					normalBillMap.put(BillDetailCNTS.BD_UNLOAD_AMT, billDetail.getBdUnloadAmt());
					normalBillMap.put(BillDetailCNTS.BD_DET_AMT, billDetail.getBdDetAmt());
					normalBillMap.put(BillDetailCNTS.BD_OTH_CHG_AMT, billDetail.getBdOthChgAmt());
					normalBillMap.put(BillDetailCNTS.USER_CODE, billDetail.getUserCode());
				}
			}
		}
		
		map.put("billNormal", normalBillMap);
		
		//add supplementry bill list
		List<Map<String, Object>> supBillList = new ArrayList<>();
		
		if (cnmt.getCnmtSupBillNo() != null && !cnmt.getCnmtSupBillNo().isEmpty()) {
			
			for (String supBillNo: cnmt.getCnmtSupBillNo()) {
				
				Map<String, Object> supBillMap = new HashMap<>();
				
				Criteria cr = session.createCriteria(Bill.class);				
				cr.add(Restrictions.eq(BillCNTS.Bill_NO, supBillNo));
				List<Bill> billList = cr.list();
				
				if (!billList.isEmpty()) {
					
					Bill billSup = billList.get(0);
					supBillMap.put(BillCNTS.BILL_ID, billSup.getBlId());
					supBillMap.put(BillCNTS.Bill_NO, billSup.getBlBillNo());
					supBillMap.put(BillCNTS.BILL_DT, billSup.getBlBillDt());
					supBillMap.put(BillCNTS.BILL_SUB_DT, billSup.getBlBillSubDt());
					supBillMap.put(BillCNTS.BILL_FINAL_TOT, billSup.getBlFinalTot());
					supBillMap.put(BillCNTS.BILL_TYPE, billSup.getBlType());
					
					
					// bill forwarding
					BillForwarding supBF=billSup.getBillForwarding();
					
					if(supBF != null) {
						supBillMap.put("bfNo", supBF.getBfNo());
						supBillMap.put("bfDt", supBF.getCreationTS());
						supBillMap.put("bfRecDt", supBF.getBfRecDt());
					}else {
						supBillMap.put("bfNo", "");
						supBillMap.put("bfDt", "");
						supBillMap.put("bfRecDt", "");
					}
					
					
					//add Money Receipt List
					List<Map<String, Object>> mrList = new ArrayList<>();
					
					SQLQuery sqlQuery = session.createSQLQuery("SELECT mrId From mr_bill where blId = :blId");
					sqlQuery.setParameter("blId", billSup.getBlId());
					
					List<Integer> mrIdList = sqlQuery.list();
					
					double blTotAmtByMr = 0;
					
					if (!mrIdList.isEmpty()) {
						for (Integer mrId: mrIdList) {
							Map<String, Object> mrMap = new HashMap<>();
							MoneyReceipt moneyReceipt = (MoneyReceipt) session.get(MoneyReceipt.class, mrId);
							mrMap.put(MoneyReceiptCNTS.MR_ID, moneyReceipt.getMrId());
							mrMap.put(MoneyReceiptCNTS.MR_NO, moneyReceipt.getMrNo());
							mrMap.put(MoneyReceiptCNTS.MR_NET_AMT, moneyReceipt.getMrNetAmt());
							mrList.add(mrMap);
							blTotAmtByMr = blTotAmtByMr + moneyReceipt.getMrNetAmt();
						}
					}
					supBillMap.put("blTotAmtByMr", blTotAmtByMr);
					supBillMap.put("mrList", mrList);
					
					//add sup bill detail
					List<BillDetail> billDetSupList = billSup.getBillDetList();
					if (!billDetSupList.isEmpty()) {
						for (BillDetail billDetSup : billDetSupList) {
							if (billDetSup.getBdCnmtId() == cnmt.getCnmtId()) {
								//add wt
								if (billDetSup.getBdBlBase().equalsIgnoreCase("receiveWt")) {
									supBillMap.put("bdWt", billDetSup.getBdRecWt());
								} else if (billDetSup.getBdBlBase().equalsIgnoreCase("actualWt")) {
									supBillMap.put("bdWt", billDetSup.getBdActWt());
								} else { //default wt is chargeWt
									supBillMap.put("bdWt", billDetSup.getBdChgWt());
								}
								
								supBillMap.put(BillDetailCNTS.BD_ID, billDetSup.getBdId());
								supBillMap.put(BillDetailCNTS.BD_RATE, billDetSup.getBdRate());
								supBillMap.put(BillDetailCNTS.BD_FREIGHT, billDetSup.getBdFreight());
								supBillMap.put(BillDetailCNTS.BD_UNLOAD_AMT, billDetSup.getBdUnloadAmt());
								supBillMap.put(BillDetailCNTS.BD_DET_AMT, billDetSup.getBdDetAmt());
								supBillMap.put(BillDetailCNTS.BD_OTH_CHG_AMT, billDetSup.getBdOthChgAmt());
								supBillMap.put(BillDetailCNTS.USER_CODE, billDetSup.getUserCode());
								break;
							}
						}
					}
				}
				supBillList.add(supBillMap);
			}
		}
		
		map.put("billSupList", supBillList);
		
		//add challan list
		List<Map<String, Object>> challanList = new ArrayList<>();
		
		Criteria cr = session.createCriteria(Cnmt_Challan.class);
		cr.add(Restrictions.eq(Cnmt_ChallanCNTS.Cnmt_Chln_cnmtId, cnmt.getCnmtId()));
		
		ProjectionList projList = Projections.projectionList();
		projList.add(Projections.property(Cnmt_ChallanCNTS.Cnmt_Chln_chlnId));
		
		cr.setProjection(projList);
		
		List<Integer> chlnIdList = cr.list();
		
		if (!chlnIdList.isEmpty()) {
			for (Integer chlnId : chlnIdList) {
				Map<String, Object> chlnMap = new HashMap<>();
				Challan chln = (Challan) session.get(Challan.class, chlnId);
				
				// TODO
				List<Narration> chlnNarr = session.createCriteria(Narration.class)
						.add(Restrictions.eq(NarrationCNTS.NARR_CODE, chln.getChlnCode()))
						.add(Restrictions.eq(NarrationCNTS.NARR_TYPE, "chln"))
						.list();
				if(! chlnNarr.isEmpty())
					chlnMap.put("chlnNarr", chlnNarr.get(0).getNarrDesc());
				else
					chlnMap.put("chlnNarr", "");
				
				chlnMap.put(ChallanCNTS.CHLN_ID, chln.getChlnId());
				chlnMap.put(ChallanCNTS.CHALLAN_CODE, chln.getChlnCode());
				chlnMap.put("bCode", chln.getbCode());
				chlnMap.put(ChallanCNTS.CHLN_DT, chln.getChlnDt());
				
				station = (Station) session.get(Station.class, Integer.parseInt(chln.getChlnFromStn()));
				chlnMap.put("chlnFrmStnName", station.getStnName());
				station = (Station) session.get(Station.class, Integer.parseInt(chln.getChlnToStn()));
				chlnMap.put("chlnToStnName", station.getStnName());
				
				chlnMap.put(ChallanCNTS.CHLN_TOTAL_WT, chln.getChlnTotalWt());
				chlnMap.put(ChallanCNTS.CREATION_TS, CodePatternService.getCurrentSqlDateFromUTC(chln.getCreationTS()));
				chlnMap.put(ChallanCNTS.CHLN_ADVANCE, chln.getChlnAdvance());
				chlnMap.put(ChallanCNTS.CHLN_BALANCE, chln.getChlnBalance());
				chlnMap.put(ChallanCNTS.CHLN_NO_OF_PKG, chln.getChlnNoOfPkg());
				chlnMap.put(ChallanCNTS.CHLN_CHG_WT, chln.getChlnChgWt());
				chlnMap.put(ChallanCNTS.CHLN_LOADING_AMT, chln.getChlnLoadingAmt());
				chlnMap.put(ChallanCNTS.CHLN_EXTRA, chln.getChlnExtra());
				chlnMap.put("chlnRrNo",chln.getChlnRrNo());
				chlnMap.put("chlnTrainNo",chln.getChlnTrainNo());
				chlnMap.put(ChallanCNTS.CHLN_CANCEL, chln.isCancel());
				
				chlnMap.put(ChallanCNTS.CHLN_DETECTION, chln.getChlnDetection());
				chlnMap.put(ChallanCNTS.CHLN_TOOL_TAX, chln.getChlnToolTax());
				chlnMap.put(ChallanCNTS.CHLN_HEIGHT, chln.getChlnHeight());
				chlnMap.put(ChallanCNTS.CHLN_UNION, chln.getChlnUnion());
				chlnMap.put(ChallanCNTS.CHLN_TWO_POINT, chln.getChlnTwoPoint());
				chlnMap.put(ChallanCNTS.CHLN_WEIGHTMENT_CHG, chln.getChlnWeightmentChg());
				chlnMap.put(ChallanCNTS.CHLN_CRANE_CHG, chln.getChlnCraneChg());
				chlnMap.put(ChallanCNTS.CHLN_OTHERS, chln.getChlnOthers());
				
				
				//add lry no
				if (chln.getChlnLryNo() != null || !chln.getChlnLryNo().equalsIgnoreCase("")) {
					chlnMap.put(ChallanCNTS.CHLN_LRY_NO, chln.getChlnLryNo().replaceAll("\\s", ""));
				} else {
					//get lry no from challan detail
					cr = session.createCriteria(ChallanDetail.class);
					cr.add(Restrictions.eq(ChallanDetailCNTS.CHD_CHALLAN_CODE, chln.getChlnCode()));
					if (!cr.list().isEmpty()) {
						ChallanDetail challanDetail = (ChallanDetail) cr.list().get(0);
						chlnMap.put(ChallanCNTS.CHLN_LRY_NO, challanDetail.getChdChlnCode().replaceAll("\\s", ""));
					}
				}
				
				//add payAt(branch name)
				if (chln.getChlnPayAt() != null) {
					Branch payAtBranch = (Branch) session.get(Branch.class, Integer.parseInt(chln.getChlnPayAt()));
					chlnMap.put("chlnPayAtName", payAtBranch.getBranchName().replaceAll("\\s", ""));
				} else {
					chlnMap.put("chlnPayAtName", "");
				}
				
				//add ar no
				Map<String, Object> arMap = new HashMap<>();
				if (chln.getChlnArId()>0) {
					ArrivalReport ar = (ArrivalReport) session.get(ArrivalReport.class, chln.getChlnArId());
					
					List<Narration> arrNarr = session.createCriteria(Narration.class)
							.add(Restrictions.eq(NarrationCNTS.NARR_CODE, ar.getArCode()))
							.add(Restrictions.eq(NarrationCNTS.NARR_TYPE, "sedr"))
							.list();
					if(! arrNarr.isEmpty())
						arMap.put("arNarr", arrNarr.get(0).getNarrDesc());
					else
						arMap.put("arNarr", "");
					
					arMap.put(ArrivalReportCNTS.AR_ID, ar.getArId());
					arMap.put(ArrivalReportCNTS.AR_CODE, ar.getArCode());
					arMap.put(ArrivalReportCNTS.AR_DATE, ar.getArDt());
					arMap.put(ArrivalReportCNTS.BRANCH_CODE, ar.getBranchCode());
					
					arMap.put(ArrivalReportCNTS.AR_REP_DATE, ar.getArRepDt());
					arMap.put(ArrivalReportCNTS.AR_RCV_WEIGHT, ar.getArRcvWt());
					arMap.put(ArrivalReportCNTS.AR_PACKAGE, ar.getArPkg());
					arMap.put(ArrivalReportCNTS.AR_EXT_KM, ar.getArExtKm());
					arMap.put(ArrivalReportCNTS.AR_OVR_HGT, ar.getArOvrHgt());
					arMap.put(ArrivalReportCNTS.AR_PENLTY, ar.getArPenalty());
					arMap.put(ArrivalReportCNTS.AR_WT_SHRTG, ar.getArWtShrtg());
					arMap.put(ArrivalReportCNTS.AR_DR_RCVR_WT, ar.getArDrRcvrWt());
					arMap.put(ArrivalReportCNTS.AR_CLAIM, ar.getArClaim());
					arMap.put(ArrivalReportCNTS.AR_UNLOADING, ar.getArUnloading());
					arMap.put(ArrivalReportCNTS.AR_DETENTION, ar.getArDetention());
					arMap.put(ArrivalReportCNTS.AR_LATE_DELIVERY, ar.getArLateDelivery());
					arMap.put(ArrivalReportCNTS.AR_LATE_ACKNOWLEDGEMENT, ar.getArLateAck());
					arMap.put(ArrivalReportCNTS.AR_REP_DATE, ar.getArRepDt());
					arMap.put(ArrivalReportCNTS.AR_DESC, ar.getArDesc());
				}
				
				chlnMap.put("ar", arMap);
				
				//add lhpv Adv List
				List<Map<String, Object>> lhpvAdvList = new ArrayList<>();
				
				SQLQuery lhpvAdvQuery = session.createSQLQuery("SELECT laId FROM lhpvadv_challan WHERE chlnId = :chlnId");
				lhpvAdvQuery.setInteger("chlnId", chln.getChlnId());
				
				List<Integer> lhpvAdvIdList = lhpvAdvQuery.list();
				double advTotByLhpv = 0;
				
				if (!lhpvAdvIdList.isEmpty()) {
					for (Integer lhpvAdvId : lhpvAdvIdList) {
						LhpvAdv lhpvAdv = (LhpvAdv) session.get(LhpvAdv.class, lhpvAdvId);
						
						if (lhpvAdv.getLaPayMethod() == 'C' || lhpvAdv.getLaPayMethod() == 'c') {
							cr = session.createCriteria(LhpvCashSmry.class);
							cr.add(Restrictions.eq(LhpvCashSmryCNTS.LA_ID, lhpvAdv.getLaId()));
							List<LhpvCashSmry> lhpvCashSmryList = cr.list();	
							
							if (!lhpvCashSmryList.isEmpty()) {
								for (LhpvCashSmry lhpvCashSmry : lhpvCashSmryList) {
									
									Map<String, Object> lhpvAdvMap = new HashMap<>();
									lhpvAdvMap.put(LhpvAdvCNTS.LHPV_ADV_ID, lhpvAdv.getLaId());
									lhpvAdvMap.put(LhpvAdvCNTS.LHPV_ADV_FINAL_TOT, lhpvAdv.getLaFinalTot());
									
									lhpvAdvMap.put(LhpvAdvCNTS.LHPV_ADV, lhpvCashSmry.getLcsPayAmt());
									lhpvAdvMap.put(LhpvAdvCNTS.LHPV_ADV_CASH_DISC, lhpvCashSmry.getLcsCashDiscR());
									lhpvAdvMap.put(LhpvAdvCNTS.LHPV_ADV_MUNS, lhpvCashSmry.getLcsMunsR());
									lhpvAdvMap.put(LhpvAdvCNTS.LHPV_ADV_TDS, lhpvCashSmry.getLcsTdsR());
									lhpvAdvMap.put(LhpvAdvCNTS.LHPV_ADV_TOT_RCVR, (lhpvCashSmry.getLcsCashDiscR() + lhpvCashSmry.getLcsMunsR() + lhpvCashSmry.getLcsTdsR()));
									
									//add branch name
									Branch br = (Branch) session.get(Branch.class, Integer.parseInt(lhpvCashSmry.getbCode()));
									lhpvAdvMap.put("laBrName", br.getBranchName());
									
									//status by date and branch
									cr = session.createCriteria(LhpvStatus.class);									
									cr.add(Restrictions.eq(LhpvStatusCNTS.LS_DT, lhpvCashSmry.getLcsLhpvDt()));
									List<LhpvStatus> lhpvStatuList = cr.list();
									
									if (!lhpvStatuList.isEmpty()) {
										LhpvStatus lhpvStatus  = lhpvStatuList.get(0);
										lhpvAdvMap.put(LhpvStatusCNTS.LS_NO, lhpvStatus.getLsNo());
										lhpvAdvMap.put(LhpvStatusCNTS.LS_DT, lhpvStatus.getLsDt());
									}
									
									lhpvAdvList.add(lhpvAdvMap);
									advTotByLhpv = advTotByLhpv + lhpvCashSmry.getLcsPayAmt();
								}
							}
							
						} else {
							
							Map<String, Object> lhpvAdvMap = new HashMap<>();
							lhpvAdvMap.put(LhpvAdvCNTS.LHPV_ADV_ID, lhpvAdv.getLaId());
							lhpvAdvMap.put(LhpvAdvCNTS.LHPV_ADV_FINAL_TOT, lhpvAdv.getLaFinalTot());
							
							lhpvAdvMap.put(LhpvAdvCNTS.LHPV_ADV, lhpvAdv.getLaLryAdvP());
							lhpvAdvMap.put(LhpvAdvCNTS.LHPV_ADV_CASH_DISC, lhpvAdv.getLaCashDiscR());
							lhpvAdvMap.put(LhpvAdvCNTS.LHPV_ADV_MUNS, lhpvAdv.getLaMunsR());
							lhpvAdvMap.put(LhpvAdvCNTS.LHPV_ADV_TDS, lhpvAdv.getLaTdsR());
							lhpvAdvMap.put(LhpvAdvCNTS.LHPV_ADV_TOT_RCVR, lhpvAdv.getLaTotRcvrAmt());
							
							//add chq no
							if (lhpvAdv.getLaChqNo() != null) {
								lhpvAdvMap.put(LhpvAdvCNTS.LHPV_ADV_CHQ_NO, lhpvAdv.getLaChqNo());
							} else {
								lhpvAdvMap.put(LhpvAdvCNTS.LHPV_ADV_CHQ_NO, "");
							}
							
							
							//add branch name
							Branch br = (Branch) session.get(Branch.class, Integer.parseInt(lhpvAdv.getbCode()));
							lhpvAdvMap.put("laBrName", br.getBranchName());
							
							//add lhpv no and date
							LhpvStatus lhpvStatus  = lhpvAdv.getLhpvStatus();
							if(lhpvStatus != null){
								lhpvAdvMap.put(LhpvStatusCNTS.LS_NO, lhpvStatus.getLsNo());
								lhpvAdvMap.put(LhpvStatusCNTS.LS_DT, lhpvStatus.getLsDt());
							}else{
								lhpvAdvMap.put(LhpvStatusCNTS.LS_DT, lhpvAdv.getLaDt());
							}
							
							lhpvAdvList.add(lhpvAdvMap);
							advTotByLhpv = advTotByLhpv + lhpvAdv.getLaLryAdvP();
						}
					}
				}
				
				chlnMap.put("advTotByLhpv", advTotByLhpv);
				chlnMap.put("lhpvAdvList", lhpvAdvList);
				
				//add lhpv Bal List
				List<Map<String, Object>> lhpvBalList = new ArrayList<>();
				
				SQLQuery lhpvBalQuery = session.createSQLQuery("SELECT lbId FROM lhpvbal_challan WHERE chlnId = :chlnId");
				lhpvBalQuery.setInteger("chlnId", chln.getChlnId());
				
				List<Integer> lhpvBalIdList = lhpvBalQuery.list();
				double balTotByLhpv = 0;
				
				if (!lhpvBalIdList.isEmpty()) {
					for (Integer lhpvBalId : lhpvBalIdList) {
						
						LhpvBal lhpvBal = (LhpvBal) session.get(LhpvBal.class, lhpvBalId);
						
						if (lhpvBal.getLbPayMethod() == 'C' || lhpvBal.getLbPayMethod() == 'C') {
							cr = session.createCriteria(LhpvCashSmry.class);
							cr.add(Restrictions.eq(LhpvCashSmryCNTS.LB_ID, lhpvBal.getLbId()));
							List<LhpvCashSmry> lhpvCashSmryList = cr.list();
							
							if (!lhpvCashSmryList.isEmpty()) {
								for (LhpvCashSmry lhpvCashSmry : lhpvCashSmryList) {
									
									double lbLryBalP = lhpvCashSmry.getLcsPayAmt() - (lhpvCashSmry.getLcsOthExtKmP() +
											lhpvCashSmry.getLcsOthOvrHgtP() + lhpvCashSmry.getLcsOthPnltyP() + lhpvCashSmry.getLcsUnpDetP() +
											lhpvCashSmry.getLcsUnLoadingP());
									
									Map<String, Object> lhpvBalMap = new HashMap<>();
									lhpvBalMap.put(LhpvBalCNTS.LHPV_BAL_ID, lhpvBal.getLbId());
									lhpvBalMap.put(LhpvBalCNTS.LHPV_BAL_FINAL_TOT, lhpvBal.getLbFinalTot());
									
									lhpvBalMap.put(LhpvBalCNTS.LHPV_BAL_PAY, lbLryBalP);
									lhpvBalMap.put(LhpvBalCNTS.LHPV_BAL_CASH_DISC, lhpvCashSmry.getLcsCashDiscR());
									lhpvBalMap.put(LhpvBalCNTS.LHPV_BAL_MUNS, lhpvCashSmry.getLcsMunsR());
									lhpvBalMap.put(LhpvBalCNTS.LHPV_BAL_TDS, lhpvCashSmry.getLcsTdsR());
									lhpvBalMap.put(LhpvBalCNTS.LHPV_BAL_WT_SHRTG, lhpvCashSmry.getLcsWtShrtgCR());
									lhpvBalMap.put(LhpvBalCNTS.LHPV_BAL_DR_RCVR_WT, lhpvCashSmry.getLcsDrRcvrWtCR());
									lhpvBalMap.put(LhpvBalCNTS.LHPV_BAL_LATE_DEL, lhpvCashSmry.getLcsLateDelCR());
									lhpvBalMap.put(LhpvBalCNTS.LHPV_BAL_LATE_ACK, lhpvCashSmry.getLcsLateAckCR());
									lhpvBalMap.put(LhpvBalCNTS.LHPV_BAL_OTH_EXT_KM, lhpvCashSmry.getLcsOthExtKmP());
									lhpvBalMap.put(LhpvBalCNTS.LHPV_BAL_OTH_OVR_HGT, lhpvCashSmry.getLcsOthOvrHgtP());
									lhpvBalMap.put(LhpvBalCNTS.LHPV_BAL_OTH_PNLTY, lhpvCashSmry.getLcsOthPnltyP());
									lhpvBalMap.put(LhpvBalCNTS.LHPV_BAL_OTH_MISC, lhpvCashSmry.getLcsOthMiscP());
									lhpvBalMap.put(LhpvBalCNTS.LHPV_BAL_UNP_DET, lhpvCashSmry.getLcsUnpDetP());
									lhpvBalMap.put(LhpvBalCNTS.LHPV_BAL_UNLOADING, lhpvCashSmry.getLcsUnLoadingP());
									lhpvBalMap.put(LhpvBalCNTS.LHPV_BAL_TOT_RCVR_AMT, (lhpvCashSmry.getLcsCashDiscR() + lhpvCashSmry.getLcsMunsR() +
											lhpvCashSmry.getLcsTdsR() + lhpvCashSmry.getLcsWtShrtgCR()+ lhpvCashSmry.getLcsDrRcvrWtCR() +
											lhpvCashSmry.getLcsLateDelCR() + lhpvCashSmry.getLcsLateAckCR() + lhpvCashSmry.getLcsOthMiscP()));
									
									//add branch name
									Branch br = (Branch) session.get(Branch.class, Integer.parseInt(lhpvCashSmry.getbCode()));
									lhpvBalMap.put("lbBrName", br.getBranchName());
									
									//status by date and branch
									System.out.println("lhpvStatusList balance LhpvDt: "+lhpvCashSmry.getLcsLhpvDt());
									
									cr = session.createCriteria(LhpvStatus.class);									
									cr.add(Restrictions.eq(LhpvStatusCNTS.LS_DT, lhpvCashSmry.getLcsLhpvDt()));
									List<LhpvStatus> lhpvStatuList = cr.list();
									
									System.out.println("lhpv status list balance: "+lhpvStatuList.size());
									
									if (!lhpvStatuList.isEmpty()) {
										LhpvStatus lhpvStatus  = lhpvStatuList.get(0);
										lhpvBalMap.put(LhpvStatusCNTS.LS_NO, lhpvStatus.getLsNo());
										lhpvBalMap.put(LhpvStatusCNTS.LS_DT, lhpvStatus.getLsDt());
									}
									
									lhpvBalList.add(lhpvBalMap);
									balTotByLhpv = balTotByLhpv + lbLryBalP;
								}
							}
							
						} else {
							Map<String, Object> lhpvBalMap = new HashMap<>();
							lhpvBalMap.put(LhpvBalCNTS.LHPV_BAL_ID, lhpvBal.getLbId());
							lhpvBalMap.put(LhpvBalCNTS.LHPV_BAL_FINAL_TOT, lhpvBal.getLbFinalTot());
							
							lhpvBalMap.put(LhpvBalCNTS.LHPV_BAL_PAY, lhpvBal.getLbLryBalP());
							lhpvBalMap.put(LhpvBalCNTS.LHPV_BAL_CASH_DISC, lhpvBal.getLbCashDiscR());
							lhpvBalMap.put(LhpvBalCNTS.LHPV_BAL_MUNS, lhpvBal.getLbMunsR());
							lhpvBalMap.put(LhpvBalCNTS.LHPV_BAL_TDS, lhpvBal.getLbTdsR());
							lhpvBalMap.put(LhpvBalCNTS.LHPV_BAL_WT_SHRTG, lhpvBal.getLbWtShrtgCR());
							lhpvBalMap.put(LhpvBalCNTS.LHPV_BAL_DR_RCVR_WT, lhpvBal.getLbDrRcvrWtCR());
							lhpvBalMap.put(LhpvBalCNTS.LHPV_BAL_LATE_DEL, lhpvBal.getLbLateDelCR());
							lhpvBalMap.put(LhpvBalCNTS.LHPV_BAL_LATE_ACK, lhpvBal.getLbLateAckCR());
							lhpvBalMap.put(LhpvBalCNTS.LHPV_BAL_OTH_EXT_KM, lhpvBal.getLbOthExtKmP());
							lhpvBalMap.put(LhpvBalCNTS.LHPV_BAL_OTH_OVR_HGT, lhpvBal.getLbOthOvrHgtP());
							lhpvBalMap.put(LhpvBalCNTS.LHPV_BAL_OTH_PNLTY, lhpvBal.getLbOthPnltyP());
							lhpvBalMap.put(LhpvBalCNTS.LHPV_BAL_OTH_MISC, lhpvBal.getLbOthMiscP());
							lhpvBalMap.put(LhpvBalCNTS.LHPV_BAL_UNP_DET, lhpvBal.getLbUnpDetP());
							lhpvBalMap.put(LhpvBalCNTS.LHPV_BAL_UNLOADING, lhpvBal.getLbUnLoadingP());
							lhpvBalMap.put(LhpvBalCNTS.LHPV_BAL_TOT_RCVR_AMT, lhpvBal.getLbTotRcvrAmt());
							
							//add chq no
							if (lhpvBal.getLbChqNo() != null) {
								lhpvBalMap.put(LhpvBalCNTS.LHPV_BAL_CHQ_NO, lhpvBal.getLbChqNo());
							} else {
								lhpvBalMap.put(LhpvBalCNTS.LHPV_BAL_CHQ_NO, "");
							}
							
							
							//add branch name
							Branch br = (Branch) session.get(Branch.class, Integer.parseInt(lhpvBal.getbCode()));
							lhpvBalMap.put("lbBrName", br.getBranchName());
							
							//add lhpv no and date
							LhpvStatus lhpvStatus  = lhpvBal.getLhpvStatus();
							if(lhpvStatus != null){
								lhpvBalMap.put(LhpvStatusCNTS.LS_NO, lhpvStatus.getLsNo());
								lhpvBalMap.put(LhpvStatusCNTS.LS_DT, lhpvStatus.getLsDt());
							}else{
								lhpvBalMap.put(LhpvStatusCNTS.LS_DT, lhpvBal.getLbDt());
							}
							
							lhpvBalList.add(lhpvBalMap);
							balTotByLhpv = balTotByLhpv + lhpvBal.getLbLryBalP();
						}
					}
				}
				chlnMap.put("balTotByLhpv", balTotByLhpv);
				chlnMap.put("lhpvBalList", lhpvBalList);
				
				//add lhpv Sup List
				List<Map<String, Object>> lhpvSupList = new ArrayList<>();
				
				SQLQuery lhpvSupQuery = session.createSQLQuery("SELECT lspId FROM lhpvsup_challan WHERE chlnId = :chlnId");
				lhpvSupQuery.setInteger("chlnId", chln.getChlnId());
				
				List<Integer> lhpvSupIdList = lhpvSupQuery.list();
				double supTotByLhpv = 0;
				
				if (!lhpvSupIdList.isEmpty()) {
					for (Integer lhpvSupId : lhpvSupIdList) {
						LhpvSup lhpvSup = (LhpvSup) session.get(LhpvSup.class, lhpvSupId);
						
						if (lhpvSup.getLspPayMethod() == 'C' || lhpvSup.getLspPayMethod() == 'C') {
							cr = session.createCriteria(LhpvCashSmry.class);
							cr.add(Restrictions.eq(LhpvCashSmryCNTS.LSP_ID, lhpvSup.getLspId()));
							List<LhpvCashSmry> lhpvCashSmryList = cr.list();
							
							if (!lhpvCashSmryList.isEmpty()) {
								for (LhpvCashSmry lhpvCashSmry : lhpvCashSmryList) {
									Map<String, Object> lhpvSupMap = new HashMap<>();
									lhpvSupMap.put(LhpvSupCNTS.LHPV_SUP_ID, lhpvSup.getLspId());
									lhpvSupMap.put(LhpvSupCNTS.LHPV_SUP_FINAL_TOT, lhpvSup.getLspFinalTot());
									
									lhpvSupMap.put(LhpvSupCNTS.LHPV_SUP_WT_SHRTG, lhpvCashSmry.getLcsWtShrtgCR());
									lhpvSupMap.put(LhpvSupCNTS.LHPV_SUP_DR_RCVR_WT, lhpvCashSmry.getLcsDrRcvrWtCR());
									lhpvSupMap.put(LhpvSupCNTS.LHPV_SUP_LATE_DEL, lhpvCashSmry.getLcsLateDelCR());
									lhpvSupMap.put(LhpvSupCNTS.LHPV_SUP_LATE_ACK, lhpvCashSmry.getLcsLateAckCR());
									lhpvSupMap.put(LhpvSupCNTS.LHPV_SUP_OTH_EXT_KM, lhpvCashSmry.getLcsOthExtKmP());
									lhpvSupMap.put(LhpvSupCNTS.LHPV_SUP_OTH_OVR_HGT, lhpvCashSmry.getLcsOthOvrHgtP());
									lhpvSupMap.put(LhpvSupCNTS.LHPV_SUP_OTH_PNLTY, lhpvCashSmry.getLcsOthPnltyP());
									lhpvSupMap.put(LhpvSupCNTS.LHPV_SUP_OTH_MISC, lhpvCashSmry.getLcsOthMiscP());
									lhpvSupMap.put(LhpvSupCNTS.LHPV_SUP_UNP_DET, lhpvCashSmry.getLcsUnpDetP());
									lhpvSupMap.put(LhpvSupCNTS.LHPV_SUP_UNLOADING, lhpvCashSmry.getLcsUnLoadingP());
									lhpvSupMap.put(LhpvSupCNTS.LHPV_SUP_TOT_RCVR_AMT, lhpvCashSmry.getLcsDrRcvrWtCR() +
											lhpvCashSmry.getLcsLateDelCR() + lhpvCashSmry.getLcsLateAckCR() +
											lhpvCashSmry.getLcsWtShrtgCR() + lhpvCashSmry.getLcsOthMiscP());
									
									//status by date and branch
									cr = session.createCriteria(LhpvStatus.class);									
									cr.add(Restrictions.eq(LhpvStatusCNTS.LS_DT, lhpvCashSmry.getLcsLhpvDt()));
									List<LhpvStatus> lhpvStatuList = cr.list();
									
									if (!lhpvStatuList.isEmpty()) {
										LhpvStatus lhpvStatus  = lhpvStatuList.get(0);
										lhpvSupMap.put(LhpvStatusCNTS.LS_NO, lhpvStatus.getLsNo());
										lhpvSupMap.put(LhpvStatusCNTS.LS_DT, lhpvStatus.getLsDt());
									}
									
									lhpvSupList.add(lhpvSupMap);
									supTotByLhpv = supTotByLhpv + lhpvSup.getLspFinalTot();
								}
							}
							
						} else {
							Map<String, Object> lhpvSupMap = new HashMap<>();
							lhpvSupMap.put(LhpvSupCNTS.LHPV_SUP_ID, lhpvSup.getLspId());
							lhpvSupMap.put(LhpvSupCNTS.LHPV_SUP_FINAL_TOT, lhpvSup.getLspFinalTot());
							
							lhpvSupMap.put(LhpvSupCNTS.LHPV_SUP_WT_SHRTG, lhpvSup.getLspWtShrtgCR());
							lhpvSupMap.put(LhpvSupCNTS.LHPV_SUP_DR_RCVR_WT, lhpvSup.getLspDrRcvrWtCR());
							lhpvSupMap.put(LhpvSupCNTS.LHPV_SUP_LATE_DEL, lhpvSup.getLspLateDelCR());
							lhpvSupMap.put(LhpvSupCNTS.LHPV_SUP_LATE_ACK, lhpvSup.getLspLateAckCR());
							lhpvSupMap.put(LhpvSupCNTS.LHPV_SUP_OTH_EXT_KM, lhpvSup.getLspOthExtKmP());
							lhpvSupMap.put(LhpvSupCNTS.LHPV_SUP_OTH_OVR_HGT, lhpvSup.getLspOthOvrHgtP());
							lhpvSupMap.put(LhpvSupCNTS.LHPV_SUP_OTH_PNLTY, lhpvSup.getLspOthPnltyP());
							lhpvSupMap.put(LhpvSupCNTS.LHPV_SUP_OTH_MISC, lhpvSup.getLspOthMiscP());
							lhpvSupMap.put(LhpvSupCNTS.LHPV_SUP_UNP_DET, lhpvSup.getLspUnpDetP());
							lhpvSupMap.put(LhpvSupCNTS.LHPV_SUP_UNLOADING, lhpvSup.getLspUnLoadingP());
							lhpvSupMap.put(LhpvSupCNTS.LHPV_SUP_TOT_RCVR_AMT, lhpvSup.getLspTotRcvrAmt());
							
							//add lhpv no and date
							LhpvStatus lhpvStatus  = lhpvSup.getLhpvStatus();
							if(lhpvStatus != null){
								lhpvSupMap.put(LhpvStatusCNTS.LS_NO, lhpvStatus.getLsNo());
								lhpvSupMap.put(LhpvStatusCNTS.LS_DT, lhpvStatus.getLsDt());
							}else{
								lhpvSupMap.put(LhpvStatusCNTS.LS_DT, lhpvSup.getLspDt());
							}
							lhpvSupList.add(lhpvSupMap);
							supTotByLhpv = supTotByLhpv + lhpvSup.getLspFinalTot();
						}
					}
				}
				chlnMap.put("supTotByLhpv", supTotByLhpv);
				chlnMap.put("lhpvSupList", lhpvSupList);
				
				challanList.add(chlnMap);
			}
		}
		
		map.put("chlnList", challanList);
		
		return map;
	}
	
	public List<Map<String, Object>> getMrByMrNo(String type, String enquiryCode){		
		List<Map<String, Object>> mrList = new ArrayList();		
		Map<String, Object> mrMap = new HashMap<String, Object>();
		try{
			// Find MR accoring to branchCode and enquiryCode
			Criteria mrCriteria = session.createCriteria(MoneyReceipt.class);			
			mrCriteria.add(Restrictions.like(MoneyReceiptCNTS.MR_NO, "%"+enquiryCode+"%"));
			
			MoneyReceipt moneyReceipt = (MoneyReceipt)mrCriteria.list().get(0);
			//Find Customer and Bank according to MR
			Customer customer = (Customer) session.get(Customer.class, moneyReceipt.getMrCustId());
			BankMstr bankMstr = (BankMstr) session.get(BankMstr.class, moneyReceipt.getMrBnkId());
			
			mrMap.put("mrNo", moneyReceipt.getMrNo());
			mrMap.put("mrBrhId", moneyReceipt.getMrBrhId());
			if(bankMstr != null)
				mrMap.put("mrBankName", bankMstr.getBnkName());			
			mrMap.put("mrPayBy", moneyReceipt.getMrPayBy());
			mrMap.put("mrChqNo", moneyReceipt.getMrChqNo());
			mrMap.put("mrCustBankName", moneyReceipt.getMrCustBankName());
			if(customer != null)
				mrMap.put("mrCustName", customer.getCustName());
			mrMap.put("mrDt", moneyReceipt.getMrDate());
			mrMap.put("mrNetAmt", moneyReceipt.getMrNetAmt());
			mrMap.put("mrRemAmt", moneyReceipt.getMrRemAmt());
			mrMap.put("mrType", moneyReceipt.getMrType());
			mrMap.put("mrDesc", moneyReceipt.getMrDesc());
			// If Mr type is Direct Payment or Payment Detail, find Bill Detail
			// if Mr type is On Accounting, check its amount has been adjust to bills or not
			if(moneyReceipt.getMrType().equalsIgnoreCase("Direct Payment") || moneyReceipt.getMrType().equalsIgnoreCase("Payment Detail")){
				Bill bill = moneyReceipt.getBill();
				if(bill != null){
					System.out.println("Bill NO : "+bill.getBlBillNo());
					Map<String, Object> billMap = new HashMap<String, Object>();
					List<Map<String, Object>> billList = new ArrayList<Map<String, Object>>();
					billMap.put("billNo", bill.getBlBillNo());
					billMap.put("billDt", bill.getBlBillDt());
					billMap.put("frtAmt", moneyReceipt.getMrFreight());
					billMap.put("excAmt", moneyReceipt.getMrAccessAmt());;
					billMap.put("tdsAmt", moneyReceipt.getMrTdsAmt());
					billMap.put("dedAmt", moneyReceipt.getMrDedAmt());
					billMap.put("recAmt", moneyReceipt.getMrNetAmt());
					billList.add(billMap);
					mrMap.put("billDt", billList);
				}				
			}else if(moneyReceipt.getMrType().equalsIgnoreCase("On Accounting") && moneyReceipt.getMrRemAmt() < moneyReceipt.getMrNetAmt()){
				List<Map<String, Object>> mrFrPDList = moneyReceipt.getMrFrPDList();
				Iterator it = mrFrPDList.iterator();
				List<Map<String, Object>> billList = new ArrayList<Map<String, Object>>();
				while(it.hasNext()){
					Map map = (Map)it.next();
					String mrNo = (String) map.get("pdMr");
					Integer brhId = (Integer)map.get("brhId");
					Criteria mrCriteria2 = session.createCriteria(MoneyReceipt.class);
					mrCriteria2.add(Restrictions.eq(MoneyReceiptCNTS.MR_BRH_ID, brhId));
					mrCriteria2.add(Restrictions.eq(MoneyReceiptCNTS.MR_NO, mrNo));
					List mrList2 = mrCriteria2.list();
					if(mrList2.size() > 0){
						MoneyReceipt moneyReceipt2 = (MoneyReceipt)mrList2.get(0);
						Bill bill = moneyReceipt2.getBill();
						if(bill != null){							
							Map<String, Object> billMap = new HashMap<String, Object>();							
							billMap.put("billNo", bill.getBlBillNo());
							billMap.put("billDt", bill.getBlBillDt());
							billMap.put("frtAmt", moneyReceipt2.getMrFreight());
							billMap.put("excAmt", moneyReceipt2.getMrAccessAmt());;
							billMap.put("tdsAmt", moneyReceipt2.getMrTdsAmt());
							billMap.put("dedAmt", moneyReceipt2.getMrDedAmt());
							billMap.put("recAmt", moneyReceipt2.getMrNetAmt());
							billList.add(billMap);
							mrMap.put("billDt", billList);
						}
					}
				}
			}
			
			mrList.add(mrMap);
			
		}catch(Exception e){
			System.out.println("Error - getMrByMrNo : "+e);
		}		
		return mrList;
	}
	
	
	public List getBillDetByMr(String branchCode, String mrNo){
		List<Map<String, Object>> enquiryList = new ArrayList<Map<String,Object>>(); 
		//Map<String, Object> mrMap = new HashMap<String, Object>();		
		System.out.println("BRH ID:"+branchCode);
		System.out.println("MR NO:"+mrNo);
		session = this.sessionFactory.openSession();
		try{
			Criteria mrCriteria = session.createCriteria(MoneyReceipt.class);
			mrCriteria.add(Restrictions.eq(MoneyReceiptCNTS.MR_BRH_ID, Integer.parseInt(branchCode)));
			mrCriteria.add(Restrictions.like(MoneyReceiptCNTS.MR_NO, "%"+mrNo.trim()+"%"));
			List mrList = mrCriteria.list();
			System.out.println("MR list size : "+mrList.size());			
			if(!mrList.isEmpty()){
				MoneyReceipt moneyReceipt = (MoneyReceipt) mrList.get(0);
				if(moneyReceipt.getMrType().equalsIgnoreCase("Payment Detail") || moneyReceipt.getMrType().equalsIgnoreCase("Direct Payment")){
					Map<String, Object> billMap = new HashMap<String, Object>();
					Bill bill = moneyReceipt.getBill();
					billMap.put("billNo", bill.getBlBillNo());
					billMap.put("billDt", bill.getBlBillDt());
					billMap.put("frtAmt", moneyReceipt.getMrFreight());
					billMap.put("excAmt", moneyReceipt.getMrAccessAmt());
					billMap.put("tdsAmt", moneyReceipt.getMrTdsAmt());
					billMap.put("dedAmt", moneyReceipt.getMrDedAmt());
					billMap.put("recAmt", moneyReceipt.getMrNetAmt());
					enquiryList.add(billMap);
				}else if(moneyReceipt.getMrType().equalsIgnoreCase("On Accounting") && moneyReceipt.getMrRemAmt() < moneyReceipt.getMrNetAmt()){
					List<Map<String, Object>> mrFrPDList = moneyReceipt.getMrFrPDList();
					Iterator it = mrFrPDList.iterator();
					List<Map<String, Object>> billList = new ArrayList<Map<String, Object>>();
					while(it.hasNext()){
						Map<String, Object> billMap = new HashMap<String, Object>();
						Map map = (Map)it.next();
						mrNo = (String) map.get("pdMr");
						Integer brhId = (Integer)map.get("brhId");
						Criteria mrCriteria2 = session.createCriteria(MoneyReceipt.class);
						mrCriteria2.add(Restrictions.eq(MoneyReceiptCNTS.MR_BRH_ID, brhId));
						mrCriteria2.add(Restrictions.eq(MoneyReceiptCNTS.MR_NO, mrNo));
						List mrList2 = mrCriteria2.list();
						if(mrList2.size() > 0){
							MoneyReceipt moneyReceipt2 = (MoneyReceipt)mrList2.get(0);
							Bill bill = moneyReceipt2.getBill();
							if(bill != null){															
								billMap.put("billNo", bill.getBlBillNo());
								billMap.put("billDt", bill.getBlBillDt());
								billMap.put("frtAmt", moneyReceipt2.getMrFreight());
								billMap.put("excAmt", moneyReceipt2.getMrAccessAmt());;
								billMap.put("tdsAmt", moneyReceipt2.getMrTdsAmt());
								billMap.put("dedAmt", moneyReceipt2.getMrDedAmt());
								billMap.put("recAmt", moneyReceipt2.getMrNetAmt());
								enquiryList.add(billMap);
								//billList.add(billMap);
								//mrMap.put("billDt", billList);
							}
						}
					}
				}
			}
		}catch(Exception e){
			System.out.println("Error in getBillDetByMr : "+e);
		}
		session.clear();
		session.close();
		return enquiryList;
	}
	
	public List<Map<String, Object>> getEnquiryByAmt(String type, String enquiryCode, String faCodeFrom, String faCodeTo, String fromDate, String toDate){
		List enquiryList = new ArrayList<Map<String, Object>>();
		List<CashStmt> cashStmtListTemp = new ArrayList<CashStmt>();
		try{
			// Find amount from CashStmt
			
			Criteria cashStmtCriteria = session.createCriteria(CashStmt.class);			
			cashStmtCriteria.add(Restrictions.eq(CashStmtCNTS.CS_AMT, Double.parseDouble(enquiryCode)));
			cashStmtCriteria.addOrder(Order.desc(CashStmtCNTS.CS_ID));
			 
			java.util.Date dFrom = null;
			java.util.Date dTo = null;
			Date dFrom1 = null;
			Date dTo1 = null;
			
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Calendar calendar = Calendar.getInstance();
			
			if(! faCodeFrom.equalsIgnoreCase("") && !faCodeTo.equalsIgnoreCase("")){
				cashStmtCriteria.add(Restrictions.sqlRestriction("CAST(csFaCode  AS UNSIGNED) BETWEEN "+Integer.parseInt(faCodeFrom)+" AND "+Integer.parseInt(faCodeTo)+" "));
				cashStmtCriteria.add(Restrictions.like(CashStmtCNTS.CS_FA_CODE, faCodeFrom.substring(0, 2)+"%"));				
			}else if(! faCodeFrom.equalsIgnoreCase("")){
				cashStmtCriteria.add(Restrictions.sqlRestriction("CAST(csFaCode  AS UNSIGNED) = "+Integer.parseInt(faCodeFrom)+" "));
				cashStmtCriteria.add(Restrictions.like(CashStmtCNTS.CS_FA_CODE, faCodeFrom.substring(0, 2)+"%"));
			}else if(! faCodeTo.equalsIgnoreCase("")){
				cashStmtCriteria.add(Restrictions.sqlRestriction("CAST(csFaCode  AS UNSIGNED) = "+Integer.parseInt(faCodeTo)+" "));
				cashStmtCriteria.add(Restrictions.like(CashStmtCNTS.CS_FA_CODE, faCodeTo.substring(0, 2)+"%"));
			}				
			if(! fromDate.equalsIgnoreCase("") && ! toDate.equalsIgnoreCase("")){
				dFrom = dateFormat.parse(fromDate);
				dTo = dateFormat.parse(toDate);
				dFrom1 = new Date(dFrom.getTime());
				dTo1 = new Date(dTo.getTime());
				cashStmtCriteria.add(Restrictions.and(Restrictions.ge(CashStmtCNTS.CS_DT, dFrom1), Restrictions.le(CashStmtCNTS.CS_DT, dTo1)));
			}else if(! fromDate.equalsIgnoreCase("")){
				dFrom = dateFormat.parse(fromDate);
				dFrom1 = new Date(dFrom.getTime());
				cashStmtCriteria.add(Restrictions.eq(CashStmtCNTS.CS_DT, dFrom1));
			}else if(! toDate.equalsIgnoreCase("")){
				dTo = dateFormat.parse(toDate);
				dTo1 = new Date(dTo.getTime());
				cashStmtCriteria.add(Restrictions.eq(CashStmtCNTS.CS_DT, dTo1));
			}			
						
			List<CashStmt> cashStmtList = cashStmtCriteria.list();			
			session.flush();
			session.clear();
			Iterator<CashStmt> cashStmtIterator = cashStmtList.iterator();			
			while(cashStmtIterator.hasNext()){				
				CashStmt cashStmt = (CashStmt)cashStmtIterator.next();
				if(cashStmt.getCsTvNo() != null){
					if(cashStmt.getCsTvNo().contains("(")){
						String csTvNo = cashStmt.getCsTvNo();
						cashStmt.setCsTvNo(csTvNo.substring(0, csTvNo.indexOf("(")));
						cashStmt.setCsChqNo(csTvNo.substring(csTvNo.indexOf("(")+1, csTvNo.length()-1));
					}else{
						cashStmt.setCsChqNo(cashStmt.getCsTvNo());
						cashStmt.setCsTvNo(null);
					}
				}
				cashStmtListTemp.add(cashStmt);				
			}
			System.out.println("CashStmt List Size : "+cashStmtListTemp.size());
			if(!cashStmtListTemp.isEmpty()){
				Map map = new HashMap<String, Object>();
				map.put("cashStmtList", cashStmtListTemp);				
				enquiryList.add(map);
			}
			
			// Find amount from LHPV Adv
			Criteria lhpvAdvCriteria = session.createCriteria(LhpvAdv.class);			
			lhpvAdvCriteria.add(Restrictions.eq(LhpvAdvCNTS.LHPV_ADV_FINAL_TOT, Double.parseDouble(enquiryCode)));
			lhpvAdvCriteria.add(Restrictions.eq(LhpvAdvCNTS.LHPV_ADV_IS_CLOSE, false));
			lhpvAdvCriteria.addOrder(Order.desc(LhpvAdvCNTS.LHPV_ADV_LAID));
			if(! faCodeFrom.equalsIgnoreCase("") && !faCodeTo.equalsIgnoreCase("")){
				lhpvAdvCriteria.add(Restrictions.like(LhpvAdvCNTS.LHPV_ADV_BRKOWN, faCodeFrom.substring(0, 2)+"%"));
				lhpvAdvCriteria.add(Restrictions.sqlRestriction("CAST(laBrkOwn AS UNSIGNED) BETWEEN "+Integer.parseInt(faCodeFrom)+" AND "+Integer.parseInt(faCodeTo)+""));
			}else if(! faCodeFrom.equalsIgnoreCase("")){
				lhpvAdvCriteria.add(Restrictions.sqlRestriction("CAST(laBrkOwn  AS UNSIGNED) = "+Integer.parseInt(faCodeFrom)+""));
				lhpvAdvCriteria.add(Restrictions.like(LhpvAdvCNTS.LHPV_ADV_BRKOWN, faCodeFrom.substring(0,2)+"%"));
			}else if(! faCodeTo.equalsIgnoreCase("")){
				lhpvAdvCriteria.add(Restrictions.sqlRestriction("CAST(laBrkOwn  AS UNSIGNED) = "+Integer.parseInt(faCodeTo)+""));
				lhpvAdvCriteria.add(Restrictions.like(LhpvAdvCNTS.LHPV_ADV_BRKOWN, faCodeTo.substring(0,2)+"%"));
			}			
			if(! fromDate.equalsIgnoreCase("") && ! toDate.equalsIgnoreCase(""))				
				lhpvAdvCriteria.add(Restrictions.and(Restrictions.ge(LhpvAdvCNTS.LHPV_ADV_DT, dFrom1), Restrictions.le(LhpvAdvCNTS.LHPV_ADV_DT, dTo1)));
			else if(! fromDate.equalsIgnoreCase(""))				
				lhpvAdvCriteria.add(Restrictions.eq(LhpvAdvCNTS.LHPV_ADV_DT, dFrom1));
			else if(! toDate.equalsIgnoreCase(""))				
				lhpvAdvCriteria.add(Restrictions.eq(LhpvAdvCNTS.LHPV_ADV_DT, dTo1));			
			
			List<LhpvAdv> lhpvAdvListTemp = lhpvAdvCriteria.list();
			System.out.println("LHPV ADV LIST TEMP : "+lhpvAdvListTemp.size());
			List<LhpvAdv> lhpvAdvList = new ArrayList<LhpvAdv>();
			Iterator<LhpvAdv> lhpvAdvIterator = lhpvAdvListTemp.iterator();			
			
			while(lhpvAdvIterator.hasNext()){				
				LhpvAdv lhpvAdv = (LhpvAdv)lhpvAdvIterator.next();
				LhpvStatus lhpvStatus = lhpvAdv.getLhpvStatus();					
				lhpvAdv.setSheetNo(String.valueOf(lhpvStatus.getLsNo()));
				lhpvAdvList.add(lhpvAdv);				
			}		
			if(!lhpvAdvList.isEmpty()){
				Map map = new HashMap<String, Object>();
				map.put("lhpvAdvList", lhpvAdvList);
				enquiryList.add(map);
			}
			
			// Find amount from LHPV Bal
			Criteria lhpvBalCriteria = session.createCriteria(LhpvBal.class);			
			lhpvBalCriteria.add(Restrictions.eq(LhpvBalCNTS.LHPV_BAL_FINAL_TOT, Double.parseDouble(enquiryCode)));
			lhpvBalCriteria.add(Restrictions.eq(LhpvBalCNTS.LHPV_BAL_IS_CLOSE, false));
			lhpvBalCriteria.addOrder(Order.desc(LhpvBalCNTS.LHPV_BAL_ID));
			if(! faCodeFrom.equalsIgnoreCase("") && !faCodeTo.equalsIgnoreCase("")){
				lhpvBalCriteria.add(Restrictions.like(LhpvBalCNTS.LHPV_BAL_BRKOWN, faCodeFrom.substring(0, 2)+"%"));
				lhpvBalCriteria.add(Restrictions.sqlRestriction("CAST(lbBrkOwn AS UNSIGNED) BETWEEN "+Integer.parseInt(faCodeFrom)+" AND "+Integer.parseInt(faCodeTo)+""));				
			}else if(! faCodeFrom.equalsIgnoreCase("")){
				lhpvBalCriteria.add(Restrictions.sqlRestriction("CAST(lbBrkOwn  AS UNSIGNED) = "+Integer.parseInt(faCodeFrom)+""));
				lhpvBalCriteria.add(Restrictions.like(LhpvBalCNTS.LHPV_BAL_BRKOWN, faCodeFrom.substring(0,2)+"%"));
			}else if(! faCodeTo.equalsIgnoreCase("")){
				lhpvBalCriteria.add(Restrictions.sqlRestriction("CAST(lbBrkOwn  AS UNSIGNED) = "+Integer.parseInt(faCodeTo)+""));
				lhpvBalCriteria.add(Restrictions.like(LhpvBalCNTS.LHPV_BAL_BRKOWN, faCodeTo.substring(0,2)+"%"));
			}			
			if(! fromDate.equalsIgnoreCase("") && ! toDate.equalsIgnoreCase(""))				
				lhpvBalCriteria.add(Restrictions.and(Restrictions.ge(LhpvBalCNTS.LHPV_BAL_DT, dFrom1), Restrictions.le(LhpvBalCNTS.LHPV_BAL_DT, dTo1)));
			else if(! fromDate.equalsIgnoreCase(""))				
				lhpvBalCriteria.add(Restrictions.eq(LhpvBalCNTS.LHPV_BAL_DT, dFrom1));
			else if(! toDate.equalsIgnoreCase(""))				
				lhpvBalCriteria.add(Restrictions.eq(LhpvBalCNTS.LHPV_BAL_DT, dTo1));
			
			List<LhpvBal> lhpvBalListTemp = lhpvBalCriteria.list();
			List<LhpvBal> lhpvBalList = new ArrayList<LhpvBal>();
			Iterator<LhpvBal> lhpvBalIterator = lhpvBalListTemp.iterator();			
			
			while(lhpvBalIterator.hasNext()){				
				LhpvBal lhpvBal = (LhpvBal)lhpvBalIterator.next();
				LhpvStatus lhpvStatus = lhpvBal.getLhpvStatus();					
				lhpvBal.setSheetNo(String.valueOf(lhpvStatus.getLsNo()));
				lhpvBalList.add(lhpvBal);				
			}	
			System.out.println("LHPV Bal List Size : "+lhpvBalList.size());
			if(!lhpvBalList.isEmpty()){
				Map map = new HashMap<String, Object>();
				map.put("lhpvBalList", lhpvBalList);
				enquiryList.add(map);
			}
			
			// Find amount from LHPV Sup
			Criteria lhpvSupCriteria = session.createCriteria(LhpvSup.class);			
			lhpvSupCriteria.add(Restrictions.eq(LhpvSupCNTS.LHPV_SUP_FINAL_TOT, Double.parseDouble(enquiryCode)));
			lhpvSupCriteria.add(Restrictions.eq(LhpvSupCNTS.LHPV_SUP_IS_CLOSE, false));
			lhpvSupCriteria.addOrder(Order.desc(LhpvSupCNTS.LHPV_SUP_ID));			
			if(! faCodeFrom.equalsIgnoreCase("") && !faCodeTo.equalsIgnoreCase("")){
				lhpvSupCriteria.add(Restrictions.like(LhpvSupCNTS.LHPV_SUP_BRKOWN, faCodeFrom.substring(0, 2)+"%"));
				lhpvSupCriteria.add(Restrictions.sqlRestriction("CAST(lspBrkOwn AS UNSIGNED) BETWEEN "+Integer.parseInt(faCodeFrom)+" AND "+Integer.parseInt(faCodeTo)+""));		
			}else if(! faCodeFrom.equalsIgnoreCase("")){
				lhpvSupCriteria.add(Restrictions.sqlRestriction("CAST(lspBrkOwn  AS UNSIGNED) = "+Integer.parseInt(faCodeFrom)+""));
				lhpvSupCriteria.add(Restrictions.like(LhpvSupCNTS.LHPV_SUP_BRKOWN, faCodeFrom.substring(0,2)+"%"));
			}else if(! faCodeTo.equalsIgnoreCase("")){
				lhpvSupCriteria.add(Restrictions.sqlRestriction("CAST(lspBrkOwn  AS UNSIGNED) = "+Integer.parseInt(faCodeTo)+""));
				lhpvSupCriteria.add(Restrictions.like(LhpvSupCNTS.LHPV_SUP_BRKOWN, faCodeTo.substring(0,2)+"%"));
			}	
			if(! fromDate.equalsIgnoreCase("") && ! toDate.equalsIgnoreCase(""))				
				lhpvSupCriteria.add(Restrictions.and(Restrictions.ge(LhpvSupCNTS.LHPV_SUP_DT, dFrom1), Restrictions.le(LhpvSupCNTS.LHPV_SUP_DT, dTo1)));
			else if(! fromDate.equalsIgnoreCase(""))				
				lhpvSupCriteria.add(Restrictions.eq(LhpvSupCNTS.LHPV_SUP_DT, dFrom1));
			else if(! toDate.equalsIgnoreCase(""))				
				lhpvSupCriteria.add(Restrictions.eq(LhpvSupCNTS.LHPV_SUP_DT, dTo1));
			
			List<LhpvSup> lhpvSupListTemp = lhpvSupCriteria.list();
			List<LhpvSup> lhpvSupList = new ArrayList<LhpvSup>();
			Iterator<LhpvSup> lhpvSupIterator = lhpvSupListTemp.iterator();			
			
			while(lhpvSupIterator.hasNext()){				
				LhpvSup lhpvSup = (LhpvSup)lhpvSupIterator.next();
				LhpvStatus lhpvStatus = lhpvSup.getLhpvStatus();					
				lhpvSup.setSheetNo(String.valueOf(lhpvStatus.getLsNo()));
				lhpvSupList.add(lhpvSup);				
			}			
			System.out.println("LHPV Sup List Size : "+lhpvSupList.size());
			if(!lhpvSupList.isEmpty()){
				Map map = new HashMap<String, Object>();
				map.put("lhpvSupList", lhpvSupList);
				enquiryList.add(map);
			}		
			
		}catch(Exception e){
			System.out.println("Error in getEnquiryByAmt - EnquiryDaoImpl : "+e);
		}		
		return enquiryList;
	}
	
	public List<Map<String, Object>> getEnquiryByChq(String enquiryCode){
		List<Map<String, Object>> enquiryList = new ArrayList<Map<String, Object>>();
		List cashStmtEnqList = new ArrayList();
		List lhpvAdvEnqList = new ArrayList();
		List lhpvBalEnqList = new ArrayList();
		List lhpvSupEnqList = new ArrayList();
		Map<String, Object> cashStmtMap = new HashMap<String, Object>();
		Map<String, Object> lhpvAdvMap = new HashMap<String, Object>();
		Map<String, Object> lhpvBalMap = new HashMap<String, Object>();
		Map<String, Object> lhpvSupMap = new HashMap<String, Object>();
		List<CashStmt> cashStmtTemp = new ArrayList();		
		try{
			// Fetch Record from CashStmt According to payMode and cheque no.
			Criteria cashStmtCriteria = session.createCriteria(CashStmt.class);			
			cashStmtCriteria.add(Restrictions.like(CashStmtCNTS.PAY_MODE, "%Q%"));
			cashStmtCriteria.add(Restrictions.like(CashStmtCNTS.CS_TV_NO, "%"+enquiryCode+"%"));
			
			List<CashStmt> cashStmtList = cashStmtCriteria.list();
			
			System.out.println("Cash List Size : "+cashStmtList.size());
			Iterator<CashStmt> cashStmtIterator = cashStmtList.iterator();
			session.flush();
			session.clear();
			while(cashStmtIterator.hasNext()){				
				CashStmt cashStmt = (CashStmt)cashStmtIterator.next();				
				if(cashStmt.getCsTvNo().contains("(")){
					String csTvNo = cashStmt.getCsTvNo();
					cashStmt.setCsTvNo(csTvNo.substring(0, csTvNo.indexOf("(")));
					cashStmt.setCsChqNo(csTvNo.substring(csTvNo.indexOf("(")+1, csTvNo.length()-1));
				}else{
					cashStmt.setCsChqNo(cashStmt.getCsTvNo());
					cashStmt.setCsTvNo(null);
				}				
				cashStmtEnqList.add(cashStmt);				
			}				
			if(cashStmtEnqList.size() > 0){
				cashStmtMap.put("cashStmtList",cashStmtEnqList);
				enquiryList.add(cashStmtMap);						
			}			
			// Fetch Record from LhpvAdv According to bCode, payBy, isClose and cheque no..
			Criteria lhpvAdvCriteria = session.createCriteria(LhpvAdv.class);			
			lhpvAdvCriteria.add(Restrictions.eq(LhpvAdvCNTS.LHPV_ADV_PAY_BY, 'Q'));
			lhpvAdvCriteria.add(Restrictions.eq(LhpvAdvCNTS.LHPV_ADV_IS_CLOSE, false));
			lhpvAdvCriteria.add(Restrictions.eq(LhpvAdvCNTS.LHPV_ADV_CHQ_NO, enquiryCode));
			
			List<LhpvAdv> lhpvAdvList = lhpvAdvCriteria.list();
			Iterator<LhpvAdv> lhpvAdvIterator = lhpvAdvList.iterator();
			
			while(lhpvAdvIterator.hasNext()){				
				LhpvAdv lhpvAdv = (LhpvAdv)lhpvAdvIterator.next();
				LhpvStatus lhpvStatus = lhpvAdv.getLhpvStatus();					
				lhpvAdv.setSheetNo(String.valueOf(lhpvStatus.getLsNo()));
				lhpvAdvEnqList.add(lhpvAdv);				
			}
			if(lhpvAdvEnqList.size() > 0){
				lhpvAdvMap.put("lhpvAdvList", lhpvAdvEnqList);
				enquiryList.add(lhpvAdvMap);	
			}
			
			// Fetch Record from LhpvBal According to bCode, payBy, isClose and cheque no..
			Criteria lhpvBalCriteria = session.createCriteria(LhpvBal.class);			
			lhpvBalCriteria.add(Restrictions.eq(LhpvBalCNTS.LHPV_BAL_PAY_METHOD, 'Q'));
			lhpvBalCriteria.add(Restrictions.eq(LhpvBalCNTS.LHPV_BAL_IS_CLOSE, false));
			lhpvBalCriteria.add(Restrictions.eq(LhpvBalCNTS.LHPV_BAL_CHQ_NO, enquiryCode));
			
			List<LhpvBal> lhpvBalList = lhpvBalCriteria.list();
			Iterator<LhpvBal> lhpvBalIterator = lhpvBalList.iterator();
			
			while(lhpvBalIterator.hasNext()){
				Map<String, Object> map = new HashMap<String, Object>();
				LhpvBal lhpvBal = (LhpvBal)lhpvBalIterator.next();
				LhpvStatus lhpvStatus = lhpvBal.getLhpvStatus();
				lhpvBal.setSheetNo(String.valueOf(lhpvStatus.getLsNo()));
				lhpvBalEnqList.add(lhpvBal);				
			}
			if(lhpvBalEnqList.size() > 0){
				lhpvBalMap.put("lhpvBalList", lhpvBalEnqList);
				enquiryList.add(lhpvBalMap);	
			}
			// Fetch Record from LhpvSup According to bCode, payBy, isClose and cheque no..
			Criteria lhpvSupCriteria = session.createCriteria(LhpvSup.class);			
			lhpvSupCriteria.add(Restrictions.eq(LhpvSupCNTS.LHPV_SUP_PAY_METHOD, 'Q'));
			lhpvSupCriteria.add(Restrictions.eq(LhpvSupCNTS.LHPV_SUP_IS_CLOSE, false));
			lhpvSupCriteria.add(Restrictions.eq(LhpvSupCNTS.LHPV_SUP_CHQ_NO, enquiryCode));
			
			List<LhpvSup> lhpvSupList = lhpvSupCriteria.list();
			Iterator<LhpvSup> lhpvSupIterator = lhpvSupList.iterator();
			
			while(lhpvSupIterator.hasNext()){				
				LhpvSup lhpvSup = (LhpvSup)lhpvSupIterator.next();
				System.out.println("LHPV Sup : "+lhpvSup);
				LhpvStatus lhpvStatus = lhpvSup.getLhpvStatus();
				lhpvSup.setSheetNo(String.valueOf(lhpvStatus.getLsNo()));
				lhpvSupEnqList.add(lhpvSup);
			}
			if(lhpvSupEnqList.size() > 0){
				lhpvSupMap.put("lhpvSupList", lhpvSupEnqList);
				enquiryList.add(lhpvSupMap);	
			}			
		}catch(Exception e){
			System.out.println("Error in getEnquiryByChq - EnquiryDaoImp : "+e);
		}
		return enquiryList;
	}
	
	public List<FAMaster> getFaCodeByCode(String branchCode, String faCode){
		List<FAMaster> enquiryList = new ArrayList<FAMaster>();
		session = this.sessionFactory.openSession();
		try{			
			Criteria faMasterCriteria = session.createCriteria(FAMaster.class);
			faMasterCriteria.add(Restrictions.eq(FAMasterCNTS.B_CODE, branchCode));
			faMasterCriteria.add(Restrictions.like(FAMasterCNTS.FA_MFA_CODE, faCode+"%"));
			enquiryList = faMasterCriteria.list();
		}catch(Exception e){
			System.out.println("Error in getFaCodeByCode - EnquiryDaoImp : "+e);
		}
		session.flush();
		session.clear();
		session.close();
		return enquiryList;
	}
	
}