package com.mylogistics.constants;

public class FAMasterCNTS {

	public static final String FA_M_ID = "faMId";
	
	public static final String FA_MFA_CODE = "faMfaCode";
	
	public static final String FA_MFA_TYPE = "faMfaType";
	
	public static final String FA_MFA_NAME = "faMfaName";
	
	public static final String USER_CODE = "userCode";
	
	public static final String B_CODE = "bCode";
	
	public static final String CREATION_TS = "creationTS";
}
