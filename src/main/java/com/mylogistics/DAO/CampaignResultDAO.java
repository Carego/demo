package com.mylogistics.DAO;

import java.util.List;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.mylogistics.model.CampaignResults;

@Repository
public interface CampaignResultDAO {
	
	public int saveCampaignResult(CampaignResults campaignResults,org.hibernate.Session session);
	
	public List<CampaignResults> getCampaignResultByIndentId(int indentId);

	public boolean checkCampaignResultByIndentIdMasterId(int indentId, int masterId, Session session);

}
