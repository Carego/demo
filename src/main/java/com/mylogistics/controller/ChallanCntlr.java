package com.mylogistics.controller;

import java.net.URLEncoder;
import java.sql.Blob;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.sql.rowset.serial.SerialBlob;

import org.apache.commons.lang.NumberUtils;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.BranchMUStatsDAO;
import com.mylogistics.DAO.BranchSStatsDAO;
import com.mylogistics.DAO.BranchStockLeafDetDAO;
import com.mylogistics.DAO.BrokerDAO;
import com.mylogistics.DAO.ChallanDAO;
import com.mylogistics.DAO.ChallanDetailDAO;
import com.mylogistics.DAO.CnmtDAO;
import com.mylogistics.DAO.Cnmt_ChallanDAO;
import com.mylogistics.DAO.ContPersonDAO;
import com.mylogistics.DAO.ContToStnDAO;
import com.mylogistics.DAO.DailyContractDAO;
import com.mylogistics.DAO.EmployeeDAO;
import com.mylogistics.DAO.IndentDAO;
import com.mylogistics.DAO.OwnerDAO;
import com.mylogistics.DAO.RateByKmDAO;
import com.mylogistics.DAO.RegularContractDAO;
import com.mylogistics.DAO.StateDAO;
import com.mylogistics.DAO.StationDAO;
import com.mylogistics.DAO.VehicleEngagementDAO;
import com.mylogistics.DAO.VehicleMstrDAO;
import com.mylogistics.DAO.VehicleTypeDAO;
import com.mylogistics.DAO.VehicleVendorDAO;
import com.mylogistics.constants.ChallanCNTS;
import com.mylogistics.model.Branch;
import com.mylogistics.model.BranchMUStats;
import com.mylogistics.model.BranchMUStatsBk;
import com.mylogistics.model.BranchSStats;
import com.mylogistics.model.BranchStockLeafDet;
import com.mylogistics.model.Broker;
import com.mylogistics.model.Challan;
import com.mylogistics.model.ChallanDetail;
import com.mylogistics.model.Cnmt;
import com.mylogistics.model.Cnmt_Challan;
import com.mylogistics.model.ContPerson;
import com.mylogistics.model.ContToStn;
import com.mylogistics.model.DailyContract;
import com.mylogistics.model.Employee;
import com.mylogistics.model.Indent;
import com.mylogistics.model.Owner;
import com.mylogistics.model.RateByKm;
import com.mylogistics.model.RegularContract;
import com.mylogistics.model.State;
import com.mylogistics.model.Station;
import com.mylogistics.model.User;
import com.mylogistics.model.VehicleEngagement;
import com.mylogistics.model.VehicleType;
import com.mylogistics.model.VehicleVendorMstr;
import com.mylogistics.services.Chln_ChlnDetService;
import com.mylogistics.services.Cnmt_ChallanService;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.ContactMobileService;
import com.mylogistics.services.ModelService;
import com.mylogistics.services.MultiCnmtService;
import com.mylogistics.services.MultiCnmtServiceImpl;
import com.mylogistics.services.mail.SmtpMailSender;
//import java.util.Date;

@EnableTransactionManagement
@Controller
public class ChallanCntlr {

	private String chlnCode = null;
	public static Logger logger = Logger.getLogger(ChallanCntlr.class);

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private CnmtDAO cnmtDAO;

	@Autowired
	private Cnmt_ChallanDAO cnmt_ChallanDAO;

	@Autowired
	private ChallanDAO challanDAO;

	@Autowired
	private BranchDAO branchDAO;

	@Autowired
	private EmployeeDAO employeeDAO;

	@Autowired
	private StationDAO stationDAO;

	@Autowired
	private VehicleTypeDAO vehicleTypeDAO;

	@Autowired
	private BrokerDAO brokerDAO;

	@Autowired
	private OwnerDAO ownerDAO;

	@Autowired
	private StateDAO stateDAO;
	
	@Autowired
	private RateByKmDAO rateByKmDAO;

	@Autowired
	private ContPersonDAO contPersonDAO;

	@Autowired
	private ChallanDetailDAO challanDetailDAO;

	@Autowired
	private DailyContractDAO dailyContractDAO;

	@Autowired
	private RegularContractDAO regularContractDAO;

	@Autowired
	private HttpSession httpSession;

	@Autowired
	private BranchStockLeafDetDAO branchStockLeafDetDAO;

	@Autowired
	private BranchSStatsDAO branchSStatsDAO;

	@Autowired
	private BranchMUStatsDAO branchMUStatsDAO;
	
	@Autowired
	private ContToStnDAO contToStnDAO;
	
	@Autowired
	private VehicleMstrDAO vehicleMstrDAO;
	
	@Autowired
	private VehicleVendorDAO vehicleVendorDAO;
	
	@Autowired
	private VehicleEngagementDAO vehicleEngagementDAO;
	
	@Autowired
	private IndentDAO indentDAO;

	private MultiCnmtService multiCnmtService = new  MultiCnmtServiceImpl();
	private ModelService modelService = new ModelService();

	@RequestMapping(value="/getBranchDataForChallan",method = RequestMethod.POST)
	public @ResponseBody Object getBranchData(){
		System.out.println("enter into getBranchData function");
		List<Branch> branchList = branchDAO.getAllActiveBranches();
		Map<String,Object> map = new HashMap<String,Object>();
		if (!branchList.isEmpty()){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("list",branchList );
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;		
	}
	
	@RequestMapping(value="/getBranchDataForChallanByBrhName",method = RequestMethod.POST)
	public @ResponseBody Object getBranchDataByBrhName(@RequestBody String brhName){
		logger.info("Enter into getBranchDataForChallanByBrhName() : Branch Name = "+brhName);	
		System.out.println("Branch Name : "+brhName);
		List<Branch> branchList = branchDAO.getAllActiveBranchesByBrhName(brhName);
		Map<String,Object> map = new HashMap<String,Object>();
		if (!branchList.isEmpty()){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("list",branchList );
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		logger.info("Exit from getBranchDataForChallanByBrhName()");
		return map;		
	}

	@RequestMapping(value = "/getStationDataForChallan", method = RequestMethod.POST)
	public @ResponseBody Object getStationData() {

		Map<String, Object> map = new HashMap<String, Object>();
		List<Station> stationList = new ArrayList<Station>();
		stationList = stationDAO.getStationData();

		if(!stationList.isEmpty()){
			map.put("list", stationList);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	@RequestMapping(value = "/getStationDataForChallanByStation", method = RequestMethod.POST)
	public @ResponseBody Object getStationDataByStation(@RequestBody String stnName) {
		logger.info("Enter into /getStationDataForChallanByStation() : stnName = "+stnName);
		Map<String, Object> map = new HashMap<String, Object>();
		List<Station> stationList = new ArrayList<Station>();
		stationList = stationDAO.getStationDataByStationName(stnName);
		if(!stationList.isEmpty()){
			map.put("list", stationList);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		logger.info("Exit from /getStationDataForChallanByStation()....");
		return map;
	}

	@RequestMapping(value="/getListOfEmployeeForChallan",method = RequestMethod.POST)
	public @ResponseBody Object getListOfEmployee(){

		List<Employee> employeeList = employeeDAO.getAllActiveEmployees();
		Map<String,Object> map = new HashMap<String,Object>();
		if (!employeeList.isEmpty()){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("list",employeeList);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;		
	}
	
	@RequestMapping(value="/getListOfEmployeeForChallanByEmpName",method = RequestMethod.POST)
	public @ResponseBody Object getListOfEmployeeByEmpName(@RequestBody String empName){
		logger.info("Enter into /getListOfEmployeeForChallanByEmpName() : EmpName = "+empName);
		System.err.println("Hi.......");
		List<Employee> employeeList = employeeDAO.getAllActiveEmployeesByName(empName);
		Map<String,Object> map = new HashMap<String,Object>();
		if (!employeeList.isEmpty()){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("list",employeeList);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		logger.info("Exit from /getListOfEmployeeForChallanByEmpName()");
		return map;		
	}

	@RequestMapping(value="/getCnmtListForChallan",method = RequestMethod.POST)
	public @ResponseBody Object getCnmtList(){
		System.out.println("enter into getCnmtList fucntion");
		logger.info("Enter into /getCnmtListForChallan()...");
		//List<Cnmt> cnmtList = cnmtDAO.getCnmtCodeDtCon();
		Map<String,Object> map = new HashMap<String,Object>();
		User currentUser = (User) httpSession.getAttribute("currentUser");
		//List<Cnmt> cnmtList = cnmtDAO.getAllBiltyCodes(currentUser.getUserBranchCode());
		List<String> cnmtList = cnmtDAO.getAllCnCode(currentUser.getUserBranchCode());
		List<String> trCnmtList = cnmtDAO.getTransCnmt();		
		if(!trCnmtList.isEmpty()){
			for(int i=0;i<trCnmtList.size();i++){
				cnmtList.add(trCnmtList.get(i));
			}
		}
		
		if(!cnmtList.isEmpty()){
			/*List<String> cnCodeList = new ArrayList<String>();
			for(int i=0;i<cnmtList.size();i++){
				cnCodeList.add(cnmtList.get(i).getCnmtCode());
			}*/
			map.put("list",cnmtList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			logger.info("Result For /getCnmtListForChallan : "+ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			logger.info("Result For /getCnmtListForChallan : "+ConstantsValues.ERROR);
		}
/*
		List<Map<String,Object>> finalList = new ArrayList<Map<String,Object>>();
		if (!cnmtList.isEmpty()){
			for(int i=0;i<cnmtList.size();i++){
				Map<String,Object> cnmtMap = new HashMap<String, Object>();
				cnmtMap.put(CnmtCNTS.CNMT_ID, cnmtList.get(i).getCnmtId());
				cnmtMap.put(CnmtCNTS.CNMT_CODE,cnmtList.get(i).getCnmtCode() );
				cnmtMap.put(CnmtCNTS.BRANCH_CODE,cnmtList.get(i).getBranchCode());
				cnmtMap.put(CnmtCNTS.CNMT_DT,cnmtList.get(i).getCnmtDt());
				cnmtMap.put(CnmtCNTS.CUST_CODE,cnmtList.get(i).getCustCode());
				cnmtMap.put(CnmtCNTS.CNMT_CONSIGNOR,cnmtList.get(i).getCnmtConsignor());
				cnmtMap.put(CnmtCNTS.CNMT_CONSIGNEE,cnmtList.get(i).getCnmtConsignee());
				cnmtMap.put(CnmtCNTS.CNMT_NO_OF_PKG,cnmtList.get(i).getCnmtNoOfPkg());
				cnmtMap.put(CnmtCNTS.CNMT_ACTUAL_WT,cnmtList.get(i).getCnmtActualWt());
				cnmtMap.put(CnmtCNTS.CNMT_PAY_AT,cnmtList.get(i).getCnmtPayAt());
				cnmtMap.put(CnmtCNTS.CNMT_BILL_AT,cnmtList.get(i).getCnmtBillAt());
				cnmtMap.put(CnmtCNTS.CNMT_FREIGHT,cnmtList.get(i).getCnmtFreight());
				cnmtMap.put(CnmtCNTS.CNMT_VOG,cnmtList.get(i).getCnmtVOG());
				cnmtMap.put(CnmtCNTS.CNMT_EXTRA_EXP,cnmtList.get(i).getCnmtExtraExp());
				cnmtMap.put(CnmtCNTS.CNMT_DT_OF_DLY,cnmtList.get(i).getCnmtDtOfDly());
				cnmtMap.put(CnmtCNTS.CNMT_EMP_CODE,cnmtList.get(i).getCnmtEmpCode());
				cnmtMap.put(CnmtCNTS.CNMT_KM,cnmtList.get(i).getCnmtKm());
				cnmtMap.put(CnmtCNTS.CNMT_STATE,cnmtList.get(i).getCnmtState());
				cnmtMap.put(CnmtCNTS.CONTRACT_CODE,cnmtList.get(i).getContractCode());
				cnmtMap.put(CnmtCNTS.CNMT_INVOICE_NO,cnmtList.get(i).getCnmtInvoiceNo());
				cnmtMap.put(CnmtCNTS.CREATION_TS,cnmtList.get(i).getCreationTS());
				cnmtMap.put(CnmtCNTS.USER_BRANCH_CODE,cnmtList.get(i).getbCode());
				cnmtMap.put(CnmtCNTS.IS_VIEW,cnmtList.get(i).isView());


				finalList.add(cnmtMap);
			}
			System.out.println("list is not empty *********************");
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("list",finalList);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		System.out.println("data send--------->");*/
		return map;		
	}
	
	
	@RequestMapping(value="/getCnmtListForChallanByCnmt",method = RequestMethod.POST)
	public @ResponseBody Object getCnmtListForChallanByCnmt(@RequestBody List<String> cnmtCodeList){
		System.out.println("enter into getCnmtList fucntion");
		logger.info("Enter into /getCnmtListForChallan()...");
		
		Map<String,Object> map = new HashMap<String,Object>();
		User currentUser = (User) httpSession.getAttribute("currentUser");		
		//List<Cnmt> cnmtList = cnmtDAO.getAllCnCodeByCnmt(currentUser.getUserBranchCode(), cnmtCodeList);		
		//List<Cnmt> trCnmtList = cnmtDAO.getTransCnmtByCnmt(cnmtCodeList);	
		
		List<Map<String, Object>> cnmtList = cnmtDAO.getAllCnCodeByCnmt(currentUser.getUserBranchCode(), cnmtCodeList);		
		List<Map<String, Object>> trCnmtList = cnmtDAO.getTransCnmtByCnmt(cnmtCodeList);		
		if(!trCnmtList.isEmpty()){
			for(int i=0;i<trCnmtList.size();i++){
				cnmtList.add(trCnmtList.get(i));
			}
		}		
		if(!cnmtList.isEmpty()){			
			map.put("list",cnmtList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			logger.info("Result For /getCnmtListForChallan : "+ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			logger.info("Result For /getCnmtListForChallan : "+ConstantsValues.ERROR);
		}
		return map;		
	}

	@RequestMapping(value="/getStateLryPrefixForChallan",method = RequestMethod.POST)
	public @ResponseBody Object getStateLryPrefix(){
		System.out.println("enter into getStateLryPrefix function");
		List<String> stateList = stateDAO.getStateLryPrefix();
		Map<String,Object> map = new HashMap<String,Object>();
		if (!stateList.isEmpty()){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("list",stateList );
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;	
	}

	@RequestMapping(value = "/getVehicleTypeCodeForChallan", method = RequestMethod.POST)
	public @ResponseBody Object getVehicleTypeCode() {
		Map<String, Object> map = new HashMap<String, Object>();
		List<VehicleType> vtList = new ArrayList<VehicleType>();
		vtList = vehicleTypeDAO.getVehicleType();
		if (!vtList.isEmpty()) {	
			System.out.println("+++++++++++++++++++++++++++++++++");
			map.put("list", vtList);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	@RequestMapping(value = "/getVehicleTypeCodeForChallanByVhName", method = RequestMethod.POST)
	public @ResponseBody Object getVehicleTypeCodeByVhName(@RequestBody String vhType) {
		logger.info("Enter into getVehicleTypeCodeByVhName() : vhType = "+vhType);
		Map<String, Object> map = new HashMap<String, Object>();
		List<VehicleType> vtList = new ArrayList<VehicleType>();
		vtList = vehicleTypeDAO.getVehicleTypeByName(vhType);
		if (!vtList.isEmpty()) {	
			System.out.println("+++++++++++++++++++++++++++++++++");
			map.put("list", vtList);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		logger.info("Exit from getVehicleTypeCodeByVhName()");
		return map;
	}

	@RequestMapping(value = "/getBrokerCodeForCD", method = RequestMethod.POST)
	public @ResponseBody Object getBrokerCodeForCD() {
		Map<String, Object> map = new HashMap<String, Object>();
		//List<Broker> brkList = new ArrayList<Broker>();
		//brkList = brokerDAO.getBroker();
		List<Map<String,String>> brkList = brokerDAO.getBrkNCF();
		if (!brkList.isEmpty()) {	 
			map.put("list", brkList);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	@RequestMapping(value = "/getBrokerCodeForCDByBrkName", method = RequestMethod.POST)
	public @ResponseBody Object getBrokerCodeForCDByBrkName(@RequestBody String brkName) {
		logger.info("Enter inot getBrokerCodeForCDByBrkName() : brkName = "+brkName);
		Map<String, Object> map = new HashMap<String, Object>();		
		List<Map<String,String>> brkList = brokerDAO.getBrkNCFByName(brkName);
		if (!brkList.isEmpty()) {	 
			map.put("list", brkList);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		logger.info("Exit from getBrokerCodeForCDByBrkName()");
		return map;
	}

	@RequestMapping(value = "/getOwnerCodeForCD", method = RequestMethod.POST)
	public @ResponseBody Object getOwnerCodeForCD() {
		Map<String, Object> map = new HashMap<String, Object>();
		//List<Owner> ownList = new ArrayList<Owner>();
		//ownList = ownerDAO.getOwner();
		List<Map<String,String>> ownList = ownerDAO.getOwnNCF();
		if (!ownList.isEmpty()) {	
			/*for(int i=0;i<ownList.size();i++){
				ownList.get(i).setOwnPanImg(null);
				ownList.get(i).setOwnDecImg(null);
			}*/
			map.put("list", ownList);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}
	
	@RequestMapping(value = "/getOwnerCodeForCDByName", method = RequestMethod.POST)
	public @ResponseBody Object getOwnerCodeForCDByName(@RequestBody String ownName) {
		logger.info("Enter into getOwnerCodeForCDByName() : ownName = "+ownName);
		Map<String, Object> map = new HashMap<String, Object>();	
		List<Map<String,String>> ownList = ownerDAO.getOwnNCFByName(ownName);
		if (!ownList.isEmpty()) {				
			map.put("list", ownList);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		logger.info("Exit from getOwnerCodeForCDByName()");
		return map;
	}


	@RequestMapping(value="/getStateCodeDataForCD",method = RequestMethod.POST)
	public @ResponseBody Object getStateCodeData(){

		List<State> stList = stateDAO.getStateData();
		Map<String,Object> map = new HashMap<String,Object>();
		if (!stList.isEmpty()){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("list",stList );
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;	
	}


	@RequestMapping(value="/getMobileListForCD",method = RequestMethod.POST)
	public @ResponseBody Object getMobileList(@RequestBody String code){
		System.out.println("enter into getMobileList function===>"+code);
		//List<ContPerson> mobList = contPersonDAO.getContPerson();
		List<String> mobList = new ArrayList<String>();
		mobList = contPersonDAO.getMobList(code);
		Map<String,Object> map = new HashMap<String,Object>();
		if (mobList != null && !mobList.isEmpty()){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("list",mobList );
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;	
	}


	@RequestMapping(value="/updateMoblieNo",method = RequestMethod.POST)
	public @ResponseBody Object updateMoblieNo(@RequestBody ContactMobileService codemob){

		System.out.println("Enter into updateMobileNo function");

		Map<String,String> map = new HashMap<String, String>();

		String code = codemob.getCode();

		String mobileNo = codemob.getMobileno();
		System.out.println("mobileNo = "+mobileNo);
		System.out.println("code = "+code);
		int temp = contPersonDAO.addMoreMobNo(mobileNo, code);	

		if(temp == 0)
		{	
			System.out.println("yoooooooo yoooooooo hunny singh");
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);

		}else {
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}

		return map;
	}

	@RequestMapping(value="/savePISForCD",method = RequestMethod.POST)
	public @ResponseBody Object savePISForCD(@RequestBody State state){

		Map<String,String> map = new HashMap<String,String>();
		User currentUser = (User)httpSession.getAttribute("currentUser");
		state.setbCode(currentUser.getUserBranchCode());
		state.setUserCode(currentUser.getUserCode());
		int temp = stateDAO.savePanIssueState(state);

		if (temp>=0) {
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR );
		}
		return map;
	}

	@RequestMapping(value="/saveOWNMBForCD",method = RequestMethod.POST)
	public @ResponseBody Object saveOWNMBForCD(@RequestBody ContPerson contPerson){

		Map<String,String> map = new HashMap<String,String>();
		User currentUser = (User)httpSession.getAttribute("currentUser");
		contPerson.setbCode(currentUser.getUserBranchCode());
		contPerson.setUserCode(currentUser.getUserCode());
		int temp = contPersonDAO.saveOwnMobNo(contPerson);

		if (temp>=0) {
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR );
		}
		return map;
	}

	@RequestMapping(value="/submitchallandetail",method = RequestMethod.POST)
	public @ResponseBody Object submitchallandetail(@RequestBody ChallanDetail chd){
		Map<String,String> map = new HashMap<String,String>();
		User currentUser = (User)httpSession.getAttribute("currentUser");
		chd.setbCode(currentUser.getUserBranchCode());
		chd.setUserCode(currentUser.getUserCode());
		int temp = challanDetailDAO.saveChlnDetailToDB(chd);
		if (temp>=0) {
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}	 	
		return map;    
	}


	@RequestMapping(value = "/getTransitDay", method = RequestMethod.POST)
	public @ResponseBody Object getTransitDay(@RequestBody String cnmtCode) {
		System.out.println("enter into getTransitDay function");
		Map<String, Object> map = new HashMap<String, Object>();

		List<Cnmt> cnmtList = cnmtDAO.getCnmt(cnmtCode);
		if(!cnmtList.isEmpty()){
			Cnmt cnmt = cnmtList.get(0);
			int cnmtId = cnmt.getCmId();
			
			List<Cnmt_Challan> Cnmt_ChallanList = cnmt_ChallanDAO.getCnmt_ChallanByCnmtId(cnmtId);
			if(!Cnmt_ChallanList.isEmpty()){
				System.out.println("transhipment");
				int chlnId = Cnmt_ChallanList.get(Cnmt_ChallanList.size()-1).getChlnId();
				List<Challan> challanList = challanDAO.getChallanById(chlnId);
				Challan challan = challanList.get(0);
				String toStation = challan.getChlnToStn();
				map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				map.put("mc","true");
				map.put("fromStation", toStation);
				map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			}else{
				String contractCode = cnmt.getContractCode();
				System.out.println("contract code from client side ==="+contractCode);
				String contractType = contractCode.substring(0,3);
				System.out.println("contract type from client side ==="+contractType);
				List<DailyContract> dailyContList = new ArrayList<DailyContract>();
				List<RegularContract> regContList = new ArrayList<RegularContract>();
				if(contractType.equalsIgnoreCase("dly")){
					dailyContList = dailyContractDAO.getDailyContract(contractCode);
					if(!dailyContList.isEmpty()){
						DailyContract dailyContract = dailyContList.get(0);
						String fromStation = dailyContract.getDlyContFromStation();
						String stnCode = cnmt.getCnmtToSt();
						String stateCode = cnmt.getCnmtState();
						String vehicleType = cnmt.getCnmtVehicleType();
						String productType = cnmt.getCnmtProductType();
						java.sql.Date cnmtDt=cnmt.getCnmtDt();
						int transitDay = 0;
						//int transitDay = dailyContract.getDlyContTransitDay();
						if(dailyContract.getDlyContType().equalsIgnoreCase("W")){
							List<ContToStn> ContToStnList = contToStnDAO.getContToStnRateW(contractCode , stnCode , vehicleType);
							if(!ContToStnList.isEmpty()){
								transitDay = ContToStnList.get(0).getCtsToTransitDay();
							}
						}else if(dailyContract.getDlyContType().equalsIgnoreCase("Q")){
							List<ContToStn> ContToStnList = contToStnDAO.getContToStnRateQ(contractCode , stnCode , productType,vehicleType,cnmtDt);
							if(!ContToStnList.isEmpty()){
								transitDay = ContToStnList.get(0).getCtsToTransitDay();
							}
						}else if(dailyContract.getDlyContType().equalsIgnoreCase("K")){
							String toStation = dailyContract.getDlyContToStation();
							List<RateByKm> rbkmList = rateByKmDAO.getRbkmForChln(contractCode , fromStation , toStation , stateCode);
							if(!rbkmList.isEmpty()){
								transitDay = rbkmList.get(0).getRbkmTransitDay();
							}
						}else{
							System.out.println("invalid contract");
						}
					
						map.put("fromStation", fromStation);
						map.put("transitDay", transitDay);
						map.put("mc","false");
						map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
					}else{
						map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					}
				}else if(contractType.equalsIgnoreCase("reg")){
					regContList = regularContractDAO.getRegularContract(contractCode);
					if(!regContList.isEmpty()){
						RegularContract regularContract = regContList.get(0);
						String fromStation = regularContract.getRegContFromStation();
						String stnCode = cnmt.getCnmtToSt();
						String stateCode = cnmt.getCnmtState();
						String vehicleType = cnmt.getCnmtVehicleType();
						String productType = cnmt.getCnmtProductType();
						java.sql.Date cnmtDt=cnmt.getCnmtDt();
						int transitDay = 0;
						if(regularContract.getRegContType().equalsIgnoreCase("W")){
							List<ContToStn> ContToStnList = contToStnDAO.getContToStnRateW(contractCode , stnCode , vehicleType);
							if(!ContToStnList.isEmpty()){
								transitDay = ContToStnList.get(0).getCtsToTransitDay();
							}
						}else if(regularContract.getRegContType().equalsIgnoreCase("Q")){
							List<ContToStn> ContToStnList = contToStnDAO.getContToStnRateQ(contractCode , stnCode , productType,vehicleType,cnmtDt);
							if(!ContToStnList.isEmpty()){
								transitDay = ContToStnList.get(0).getCtsToTransitDay();
							}
						}else if(regularContract.getRegContType().equalsIgnoreCase("K")){
							String toStation = regularContract.getRegContToStation();
							List<RateByKm> rbkmList = rateByKmDAO.getRbkmForChln(contractCode , fromStation , toStation , stateCode);
							if(!rbkmList.isEmpty()){
								transitDay = rbkmList.get(0).getRbkmTransitDay();
							}
						}else{
							System.out.println("invalid contract");
						}
						//int transitDay = regularContract.getRegContTransitDay();
						map.put("fromStation", fromStation);
						map.put("transitDay", transitDay);
						map.put("mc","false");
						map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
					}else{
						map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					}
				}else{
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}
			}   
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		/*int cnmtId = cnmt.getCnmtId();
		List<Cnmt_Challan> Cnmt_ChallanList = cnmt_ChallanDAO.getCnmt_ChallanByCnmtId(cnmtId);
		if(!Cnmt_ChallanList.isEmpty()){
			int chlnId = Cnmt_ChallanList.get(Cnmt_ChallanList.size()-1).getChlnId();
			List<Challan> challanList = challanDAO.getChallanById(chlnId);
			Challan challan = challanList.get(0);
			String toStation = challan.getChlnToStn();
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("mc","true");
			map.put("fromStation", toStation);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else{
			String contractCode = cnmt.getContractCode();
			System.out.println("contract code from client side ==="+contractCode);
			String contractType = contractCode.substring(0,3);
			System.out.println("contract type from client side ==="+contractType);
			List<DailyContract> dailyContList = new ArrayList<DailyContract>();
			List<RegularContract> regContList = new ArrayList<RegularContract>();
			if(contractType.equalsIgnoreCase("dly")){
				dailyContList = dailyContractDAO.getDailyContract(contractCode);
				if(!dailyContList.isEmpty()){
					DailyContract dailyContract = dailyContList.get(0);
					String fromStation = dailyContract.getDlyContFromStation();
					String stnCode = cnmt.getCnmtToSt();
					String stateCode = cnmt.getCnmtState();
					String vehicleType = cnmt.getCnmtVehicleType();
					String productType = cnmt.getCnmtProductType();
					int transitDay = 0;
					//int transitDay = dailyContract.getDlyContTransitDay();
					if(dailyContract.getDlyContType().equalsIgnoreCase("W")){
						List<ContToStn> ContToStnList = contToStnDAO.getContToStnRateW(contractCode , stnCode , vehicleType);
						if(!ContToStnList.isEmpty()){
							transitDay = ContToStnList.get(0).getCtsToTransitDay();
						}
					}else if(dailyContract.getDlyContType().equalsIgnoreCase("Q")){
						List<ContToStn> ContToStnList = contToStnDAO.getContToStnRateQ(contractCode , stnCode , productType);
						if(!ContToStnList.isEmpty()){
							transitDay = ContToStnList.get(0).getCtsToTransitDay();
						}
					}else if(dailyContract.getDlyContType().equalsIgnoreCase("K")){
						String toStation = dailyContract.getDlyContToStation();
						List<RateByKm> rbkmList = rateByKmDAO.getRbkmForChln(contractCode , fromStation , toStation , stateCode);
						if(!rbkmList.isEmpty()){
							transitDay = rbkmList.get(0).getRbkmTransitDay();
						}
					}else{
						System.out.println("invalid contract");
					}
				
					map.put("fromStation", fromStation);
					map.put("transitDay", transitDay);
					map.put("mc","false");
					map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				}else{
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}
			}else if(contractType.equalsIgnoreCase("reg")){
				regContList = regularContractDAO.getRegularContract(contractCode);
				if(!regContList.isEmpty()){
					RegularContract regularContract = regContList.get(0);
					String fromStation = regularContract.getRegContFromStation();
					String stnCode = cnmt.getCnmtToSt();
					String stateCode = cnmt.getCnmtState();
					String vehicleType = cnmt.getCnmtVehicleType();
					String productType = cnmt.getCnmtProductType();
					int transitDay = 0;
					if(regularContract.getRegContType().equalsIgnoreCase("W")){
						List<ContToStn> ContToStnList = contToStnDAO.getContToStnRateW(contractCode , stnCode , vehicleType);
						if(!ContToStnList.isEmpty()){
							transitDay = ContToStnList.get(0).getCtsToTransitDay();
						}
					}else if(regularContract.getRegContType().equalsIgnoreCase("Q")){
						List<ContToStn> ContToStnList = contToStnDAO.getContToStnRateQ(contractCode , stnCode , productType);
						if(!ContToStnList.isEmpty()){
							transitDay = ContToStnList.get(0).getCtsToTransitDay();
						}
					}else if(regularContract.getRegContType().equalsIgnoreCase("K")){
						String toStation = regularContract.getRegContToStation();
						List<RateByKm> rbkmList = rateByKmDAO.getRbkmForChln(contractCode , fromStation , toStation , stateCode);
						if(!rbkmList.isEmpty()){
							transitDay = rbkmList.get(0).getRbkmTransitDay();
						}
					}else{
						System.out.println("invalid contract");
					}
					//int transitDay = regularContract.getRegContTransitDay();
					map.put("fromStation", fromStation);
					map.put("transitDay", transitDay);
					map.put("mc","false");
					map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
				}else{
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
		}   */
		return map;	
	}    


//	@RequestMapping(value="/submitChln&ChlnDet",method = RequestMethod.POST)
//	public @ResponseBody Object submitChallan(@RequestBody Cnmt_ChallanService cnmt_ChallanService){
//		logger.info("Enter into /submitChln&ChlnDet() ");
//		User currentUser = (User)httpSession.getAttribute("currentUser");
//		Map<String,Object> map = new HashMap<String, Object>();
//		Map<String,Object> finalResult = new HashMap<String,Object>();
//		Challan challan = cnmt_ChallanService.getChallan();
//
//		
//		ChallanDetail challanDetail = cnmt_ChallanService.getChallanDetail();
//
//		String chlanCode =challan.getChlnCode();
//		String actualSNo = chlanCode;
//		double daysLeft = getChlnDaysLast();
//		double average = getAvgMonthlyUsageForChln();		
//		String brsLeafDetStatus = "chln";
//		
//		
//		Session session = null;
//		Transaction transaction = null;
//		try{
//			session=this.sessionFactory.openSession();
//			transaction =  session.beginTransaction();
//			
//			
//
//			BranchStockLeafDet branchStockLeafDet = branchStockLeafDetDAO.deleteBSLD(actualSNo,brsLeafDetStatus,session);
//
//			if(branchStockLeafDet != null){
//				
//				BranchSStats branchSStats = new BranchSStats();
//				branchSStats.setBrsStatsBrCode(branchStockLeafDet.getBrsLeafDetBrCode());
//				branchSStats.setBrsStatsType("chln");
//				branchSStats.setBrsStatsSerialNo(branchStockLeafDet.getBrsLeafDetSNo());
//				branchSStats.setUserCode(currentUser.getUserCode());
//				branchSStats.setbCode(currentUser.getUserBranchCode());
//				branchSStats.setBrsStatsDt(challan.getChlnDt());
//				
//				int result = branchSStatsDAO.saveBranchSStats(branchSStats,session);
//				
//				System.out.println("after saving result = "+result);
//
//				Calendar cal = Calendar.getInstance();
//				Date currentDate = challan.getChlnDt();
//				cal.setTime(currentDate);
//				int currentMonth = cal.get(Calendar.MONTH)+1;
//				int currentYear = cal.get(Calendar.YEAR);
//
//				if(result==1){
//					BranchMUStats branchMUStats = new BranchMUStats();
//					branchMUStats.setBmusDaysLast(daysLeft);
//					branchMUStats.setBmusStType("chln");
//					branchMUStats.setBmusAvgMonthUsage(average);
//					branchMUStats.setbCode(currentUser.getUserBranchCode());
//					branchMUStats.setUserCode(currentUser.getUserCode());
//					branchMUStats.setBmusBranchCode(branchStockLeafDet.getBrsLeafDetBrCode());
//					branchMUStats.setBmusDt(currentDate);
//
//
//					long rowsCount = branchMUStatsDAO.totalBMUSRows("chln",session);
//					System.out.println("Rows count for Chln is-----------"+rowsCount);
//
//					if(rowsCount!=-1){
//						if(rowsCount==0){ 
//							branchMUStatsDAO.saveOrUpdateBMUS(branchMUStats,session);
//						}else{
//							List<BranchMUStats> listStats = branchMUStatsDAO.getLastData(branchStockLeafDet.getBrsLeafDetBrCode(),"chln",session);
//							BranchMUStats branchMUStats2 = listStats.get(0);
//							Date lastDate = listStats.get(0).getBmusDt();
//							int lastId = listStats.get(0).getBmusId();
//
//							System.out.println("The last date is ==========="+lastDate);
//							Calendar calender = Calendar.getInstance();
//							calender.setTime(lastDate);
//							int lastMonth = calender.get(Calendar.MONTH)+1;
//							System.out.println("lastMonth---------------"+lastMonth);
//							int lastyear = calender.get(Calendar.YEAR);
//							System.out.println("lastyear---------------"+lastyear);
//							if((currentMonth==lastMonth) && (currentYear==lastyear)){
//								branchMUStats.setBmusAvgMonthUsage(average);
//								branchMUStats.setBmusDt(currentDate);
//								branchMUStats.setBmusDaysLast(daysLeft);
//								branchMUStats.setBmusId(lastId);
//								branchMUStatsDAO.updateBMUS(branchMUStats,session);
//								System.out.println("After update");
//							}else if((currentMonth!=lastMonth) && (currentYear!=lastyear)){
//								System.out.println("Enter into function with id ---------"+lastId);
//								int delete = branchMUStatsDAO.deleteBMUS(lastId,session);
//								System.out.println("After deleting old entry---------------------"+delete);
//								if(delete==1){
//									int save =branchMUStatsDAO.saveOrUpdateBMUS(branchMUStats,session);
//									System.out.println("--------------------After saving new entry"+save);
//									if(save == 1){
//										BranchMUStatsBk bk = new BranchMUStatsBk();
//										bk.setbCode(branchMUStats2.getbCode());
//										bk.setBmusAvgMonthUsage(branchMUStats2.getBmusAvgMonthUsage());
//										bk.setBmusBranchCode(branchMUStats2.getBmusBranchCode());
//										bk.setBmusDaysLast(branchMUStats2.getBmusDaysLast());
//										bk.setBmusDt(branchMUStats2.getBmusDt());
//										bk.setBmusId(branchMUStats2.getBmusId());
//										bk.setBmusStType(branchMUStats2.getBmusStType());
//										bk.setUserCode(branchMUStats2.getUserCode());
//										branchMUStatsDAO.saveBMUSBK(bk,session);
//										System.out.println("---------------After saving into backup");
//									}else{
//										map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);	
//									}
//								}	else{
//									map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
//								}
//							}else if((currentMonth!=lastMonth) && (currentYear==lastyear)){
//								System.out.println("Enter into function with id ---------"+lastId);
//								int delete = branchMUStatsDAO.deleteBMUS(lastId,session);
//								System.out.println("After deleting old entry---------------------"+delete);
//								if(delete==1){
//									int save =branchMUStatsDAO.saveOrUpdateBMUS(branchMUStats,session);
//									System.out.println("--------------------After saving new entry"+save);
//									if(save == 1){
//										BranchMUStatsBk bk = new BranchMUStatsBk();
//										bk.setbCode(branchMUStats2.getbCode());
//										bk.setBmusAvgMonthUsage(branchMUStats2.getBmusAvgMonthUsage());
//										bk.setBmusBranchCode(branchMUStats2.getBmusBranchCode());
//										bk.setBmusDaysLast(branchMUStats2.getBmusDaysLast());
//										bk.setBmusDt(branchMUStats2.getBmusDt());
//										bk.setBmusId(branchMUStats2.getBmusId());
//										bk.setBmusStType(branchMUStats2.getBmusStType());
//										bk.setUserCode(branchMUStats2.getUserCode());
//										branchMUStatsDAO.saveBMUSBK(bk,session);
//										System.out.println("---------------After saving into backup");
//									}else{
//										map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
//									}
//								}else{		
//									map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
//								}
//							}
//						}	
//					}
//				} 
//			}
//			System.out.println("After inserting into branchSStats ");
//
//			
//			//TODO
//			//List<String> cnmtCodeList = cnmt_ChallanService.getCnmtCodeList();
//			List<Map<String,Object>> cnmtCodeList = new ArrayList<Map<String,Object>>();
//			cnmtCodeList = cnmt_ChallanService.getCnmtCodeList();
//			String cnmtCode = "";
//
//			if(cnmtCodeList.size() > 1){
//				
//				List<String> cnmtList = new ArrayList<String>();
//				for(int m=0;m<cnmtCodeList.size();m++){
//					cnmtList.add(String.valueOf(cnmtCodeList.get(m).get("cnmt")));
//				}
//				
//				
//				List<Cnmt> multiCnmtList = cnmtDAO.getAllCnmtByCode(cnmtList,session);
//				
//				challan.setView(false);
//				challan.setUserCode(currentUser.getUserCode());
//				challan.setbCode(currentUser.getUserBranchCode());
//				System.out.println("challan.lryNo = "+challan.getChlnLryNo());
//
//				challanDetail.setbCode(currentUser.getUserBranchCode());
//				challanDetail.setUserCode(currentUser.getUserCode());
//				int challanId = challanDAO.saveChlnAndChd(challan , challanDetail,session);
//				
//
//				//TODO
//				if(challanId > 0){
//					for(int i=0;i<multiCnmtList.size();i++){
//						Cnmt cnmt = multiCnmtList.get(i);
//						int pkg = 0;
//						double wt = 0.0;
//						for(int j=0;j<cnmtCodeList.size();j++){
//							if(cnmt.getCnmtCode().equalsIgnoreCase(String.valueOf(cnmtCodeList.get(j).get("cnmt")))){
//								pkg = Integer.parseInt(String.valueOf(cnmtCodeList.get(j).get("pkg")));
//								wt = Double.parseDouble(String.valueOf(cnmtCodeList.get(j).get("wt")));
//							}
//						}
//						cnmt.setIsDone(3);
//						int cnmtId = cnmtDAO.updateCnmt(cnmt,session);
//						Cnmt_Challan cnmt_Challan = new Cnmt_Challan();
//						cnmt_Challan.setChlnId(challanId);
//						cnmt_Challan.setCnmtId(cnmtId);
//						cnmt_Challan.setCnmtNoOfPkg(pkg);
//						cnmt_Challan.setCnmtTotalWt(wt);
//						cnmt_ChallanDAO.saveCnmt_Challan(cnmt_Challan,session);
//					}
//					finalResult.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
//				}
//				return finalResult;
//
//			}else if(cnmtCodeList.size() == 1){
//				System.out.println("*********************cnmtCodeList size = 1");
//				System.out.println("String.valueOf(cnmtCodeList.get(0).get(cnmt)) = "+String.valueOf(cnmtCodeList.get(0).get("cnmt")));
//				List<Cnmt> cnmtList = new ArrayList<Cnmt>();
//				cnmtList = cnmtDAO.getCnmt(String.valueOf(cnmtCodeList.get(0).get("cnmt")),session);
//
//				if(!cnmtList.isEmpty()){
//						Cnmt cnmt = cnmtList.get(0);
//						
//						if(challan.getChlnToStn().equalsIgnoreCase(cnmt.getCnmtToSt())){
//							challan.setView(false);
//							challan.setUserCode(currentUser.getUserCode());
//							challan.setbCode(currentUser.getUserBranchCode());
//							System.out.println("challan.lryNo = "+challan.getChlnLryNo());
//							
//							challanDetail.setbCode(currentUser.getUserBranchCode());
//							challanDetail.setUserCode(currentUser.getUserCode());
//							int challanId = challanDAO.saveChlnAndChd(challan , challanDetail,session);
//							
//							if(challanId > 0){
//								Cnmt_Challan cnmt_Challan = new Cnmt_Challan();
//								cnmt_Challan.setCnmtId(cnmt.getCnmtId());
//								cnmt_Challan.setChlnId(challanId);
//								cnmt_Challan.setCnmtNoOfPkg(Integer.parseInt(String.valueOf(cnmtCodeList.get(0).get("pkg"))));
//								cnmt_Challan.setCnmtTotalWt(Double.parseDouble(String.valueOf(cnmtCodeList.get(0).get("wt"))));
//								cnmt_ChallanDAO.saveCnmt_Challan(cnmt_Challan,session);
//								
//								cnmt.setTrans(false);
//								cnmtDAO.updateCnmt(cnmt,session);
//								finalResult.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
//							}else{
//								finalResult.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
//							}
//						}else{
//							challan.setView(false);
//							challan.setUserCode(currentUser.getUserCode());
//							challan.setbCode(currentUser.getUserBranchCode());
//							System.out.println("challan.lryNo = "+challan.getChlnLryNo());
//							//int challanId = challanDAO.saveChallan(challan);
//							
//							challanDetail.setbCode(currentUser.getUserBranchCode());
//							challanDetail.setUserCode(currentUser.getUserCode());
//							int challanId = challanDAO.saveChlnAndChd(challan , challanDetail, session);
//							
//							if(challanId > 0){
//								Cnmt_Challan cnmt_Challan = new Cnmt_Challan();
//								cnmt_Challan.setCnmtId(cnmt.getCnmtId());
//								cnmt_Challan.setChlnId(challanId);
//								cnmt_Challan.setCnmtNoOfPkg(Integer.parseInt(String.valueOf(cnmtCodeList.get(0).get("pkg"))));
//								cnmt_Challan.setCnmtTotalWt(Double.parseDouble(String.valueOf(cnmtCodeList.get(0).get("wt"))));
//								cnmt_ChallanDAO.saveCnmt_Challan(cnmt_Challan,session);
//								
//								cnmt.setTrans(true);
//								cnmtDAO.updateCnmt(cnmt,session);
//								finalResult.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
//							}else{
//								finalResult.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
//							}
//						}	
//				}else{
//					System.out.println("cnmt not exist");
//				}
//
//			}else{
//				finalResult.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
//			}
//			
//			transaction.commit();
//		}catch(Exception e){
//			e.printStackTrace();
//			transaction.rollback();
//			logger.info("Excption in submitCahalla()= "+e);
//		}finally{
//			session.clear();
//			session.close();
//		}
//		
//		return finalResult;
//		
//	}

	
	
	
	@RequestMapping(value="/submitChln&ChlnDet",method = RequestMethod.POST)
	public @ResponseBody Object submitChallan(@RequestBody Cnmt_ChallanService cnmt_ChallanService){
		logger.info("Enter into /submitChln&ChlnDet() ");
		User currentUser = (User)httpSession.getAttribute("currentUser");
		Map<String,Object> map = new HashMap<String, Object>();
		Map<String,Object> finalResult = new HashMap<String,Object>();
		Challan challan = cnmt_ChallanService.getChallan();

		ChallanDetail challanDetail = cnmt_ChallanService.getChallanDetail();

		String chlanCode =challan.getChlnCode();
		String actualSNo = chlanCode;
		double daysLeft = getChlnDaysLast();
		double average = getAvgMonthlyUsageForChln();		
		String brsLeafDetStatus = "chln";
		
		BranchStockLeafDet branchStockLeafDet = branchStockLeafDetDAO.deleteBSLD(actualSNo,brsLeafDetStatus);

		if(branchStockLeafDet != null){
			
			BranchSStats branchSStats = new BranchSStats();
			branchSStats.setBrsStatsBrCode(branchStockLeafDet.getBrsLeafDetBrCode());
			branchSStats.setBrsStatsType("chln");
			branchSStats.setBrsStatsSerialNo(branchStockLeafDet.getBrsLeafDetSNo());
			branchSStats.setUserCode(currentUser.getUserCode());
			branchSStats.setbCode(currentUser.getUserBranchCode());
			branchSStats.setBrsStatsDt(challan.getChlnDt());
			
			int result = branchSStatsDAO.saveBranchSStats(branchSStats);
			
			System.out.println("after saving result = "+result);

			Calendar cal = Calendar.getInstance();
			Date currentDate = challan.getChlnDt();
			cal.setTime(currentDate);
			int currentMonth = cal.get(Calendar.MONTH)+1;
			int currentYear = cal.get(Calendar.YEAR);

			if(result==1){
				BranchMUStats branchMUStats = new BranchMUStats();
				branchMUStats.setBmusDaysLast(daysLeft);
				branchMUStats.setBmusStType("chln");
				branchMUStats.setBmusAvgMonthUsage(average);
				branchMUStats.setbCode(currentUser.getUserBranchCode());
				branchMUStats.setUserCode(currentUser.getUserCode());
				branchMUStats.setBmusBranchCode(branchStockLeafDet.getBrsLeafDetBrCode());
				branchMUStats.setBmusDt(currentDate);


				long rowsCount = branchMUStatsDAO.totalBMUSRows("chln");
				System.out.println("Rows count for Chln is-----------"+rowsCount);

				if(rowsCount!=-1){
					if(rowsCount==0){ 
						branchMUStatsDAO.saveOrUpdateBMUS(branchMUStats);
					}else{
						List<BranchMUStats> listStats = branchMUStatsDAO.getLastData(branchStockLeafDet.getBrsLeafDetBrCode(),"chln");
						BranchMUStats branchMUStats2 = listStats.get(0);
						Date lastDate = listStats.get(0).getBmusDt();
						int lastId = listStats.get(0).getBmusId();

						System.out.println("The last date is ==========="+lastDate);
						Calendar calender = Calendar.getInstance();
						calender.setTime(lastDate);
						int lastMonth = calender.get(Calendar.MONTH)+1;
						System.out.println("lastMonth---------------"+lastMonth);
						int lastyear = calender.get(Calendar.YEAR);
						System.out.println("lastyear---------------"+lastyear);
						if((currentMonth==lastMonth) && (currentYear==lastyear)){
							branchMUStats.setBmusAvgMonthUsage(average);
							branchMUStats.setBmusDt(currentDate);
							branchMUStats.setBmusDaysLast(daysLeft);
							branchMUStats.setBmusId(lastId);
							branchMUStatsDAO.updateBMUS(branchMUStats);
							System.out.println("After update");
						}else if((currentMonth!=lastMonth) && (currentYear!=lastyear)){
							System.out.println("Enter into function with id ---------"+lastId);
							int delete = branchMUStatsDAO.deleteBMUS(lastId);
							System.out.println("After deleting old entry---------------------"+delete);
							if(delete==1){
								int save =branchMUStatsDAO.saveOrUpdateBMUS(branchMUStats);
								System.out.println("--------------------After saving new entry"+save);
								if(save == 1){
									BranchMUStatsBk bk = new BranchMUStatsBk();
									bk.setbCode(branchMUStats2.getbCode());
									bk.setBmusAvgMonthUsage(branchMUStats2.getBmusAvgMonthUsage());
									bk.setBmusBranchCode(branchMUStats2.getBmusBranchCode());
									bk.setBmusDaysLast(branchMUStats2.getBmusDaysLast());
									bk.setBmusDt(branchMUStats2.getBmusDt());
									bk.setBmusId(branchMUStats2.getBmusId());
									bk.setBmusStType(branchMUStats2.getBmusStType());
									bk.setUserCode(branchMUStats2.getUserCode());
									branchMUStatsDAO.saveBMUSBK(bk);
									System.out.println("---------------After saving into backup");
								}else{
									map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);	
								}
							}	else{
								map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
							}
						}else if((currentMonth!=lastMonth) && (currentYear==lastyear)){
							System.out.println("Enter into function with id ---------"+lastId);
							int delete = branchMUStatsDAO.deleteBMUS(lastId);
							System.out.println("After deleting old entry---------------------"+delete);
							if(delete==1){
								int save =branchMUStatsDAO.saveOrUpdateBMUS(branchMUStats);
								System.out.println("--------------------After saving new entry"+save);
								if(save == 1){
									BranchMUStatsBk bk = new BranchMUStatsBk();
									bk.setbCode(branchMUStats2.getbCode());
									bk.setBmusAvgMonthUsage(branchMUStats2.getBmusAvgMonthUsage());
									bk.setBmusBranchCode(branchMUStats2.getBmusBranchCode());
									bk.setBmusDaysLast(branchMUStats2.getBmusDaysLast());
									bk.setBmusDt(branchMUStats2.getBmusDt());
									bk.setBmusId(branchMUStats2.getBmusId());
									bk.setBmusStType(branchMUStats2.getBmusStType());
									bk.setUserCode(branchMUStats2.getUserCode());
									branchMUStatsDAO.saveBMUSBK(bk);
									System.out.println("---------------After saving into backup");
								}else{
									map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
								}
							}else{		
								map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
							}
						}
					}	
				}
			} 
		}
		System.out.println("After inserting into branchSStats ");

		
		//TODO
		//List<String> cnmtCodeList = cnmt_ChallanService.getCnmtCodeList();
		List<Map<String,Object>> cnmtCodeList = new ArrayList<Map<String,Object>>();
		cnmtCodeList = cnmt_ChallanService.getCnmtCodeList();
		String cnmtCode = "";

		if(cnmtCodeList.size() > 1){
			
			List<String> cnmtList = new ArrayList<String>();
			for(int m=0;m<cnmtCodeList.size();m++){
				cnmtList.add(String.valueOf(cnmtCodeList.get(m).get("cnmt")));
			}
			
			
			List<Cnmt> multiCnmtList = cnmtDAO.getAllCnmtByCode(cnmtList);
			
			challan.setView(false);
			challan.setUserCode(currentUser.getUserCode());
			challan.setbCode(currentUser.getUserBranchCode());
			System.out.println("challan.lryNo = "+challan.getChlnLryNo());

			challanDetail.setbCode(currentUser.getUserBranchCode());
			challanDetail.setUserCode(currentUser.getUserCode());
			int challanId = challanDAO.saveChlnAndChd(challan , challanDetail);
			

			//TODO
			if(challanId > 0){
				for(int i=0;i<multiCnmtList.size();i++){
					Cnmt cnmt = multiCnmtList.get(i);
					int pkg = 0;
					double wt = 0.0;
					for(int j=0;j<cnmtCodeList.size();j++){
						if(cnmt.getCnmtCode().equalsIgnoreCase(String.valueOf(cnmtCodeList.get(j).get("cnmt")))){
							pkg = Integer.parseInt(String.valueOf(cnmtCodeList.get(j).get("pkg")));
							wt = Double.parseDouble(String.valueOf(cnmtCodeList.get(j).get("wt")));
						}
					}
					cnmt.setIsDone(3);
					int cnmtId = cnmtDAO.updateCnmt(cnmt);
					Cnmt_Challan cnmt_Challan = new Cnmt_Challan();
					cnmt_Challan.setChlnId(challanId);
					cnmt_Challan.setCnmtId(cnmtId);
					cnmt_Challan.setCnmtNoOfPkg(pkg);
					cnmt_Challan.setCnmtTotalWt(wt);
					cnmt_ChallanDAO.saveCnmt_Challan(cnmt_Challan);
				}
				finalResult.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}
			return finalResult;

		}else if(cnmtCodeList.size() == 1){
			System.out.println("*********************cnmtCodeList size = 1");
			System.out.println("String.valueOf(cnmtCodeList.get(0).get(cnmt)) = "+String.valueOf(cnmtCodeList.get(0).get("cnmt")));
			List<Cnmt> cnmtList = new ArrayList<Cnmt>();
			cnmtList = cnmtDAO.getCnmt(String.valueOf(cnmtCodeList.get(0).get("cnmt")));

			if(!cnmtList.isEmpty()){
					Cnmt cnmt = cnmtList.get(0);
					
					if(challan.getChlnToStn().equalsIgnoreCase(cnmt.getCnmtToSt())){
						challan.setView(false);
						challan.setUserCode(currentUser.getUserCode());
						challan.setbCode(currentUser.getUserBranchCode());
						System.out.println("challan.lryNo = "+challan.getChlnLryNo());
						
						challanDetail.setbCode(currentUser.getUserBranchCode());
						challanDetail.setUserCode(currentUser.getUserCode());
						int challanId = challanDAO.saveChlnAndChd(challan , challanDetail);
						
						if(challanId > 0){
							Cnmt_Challan cnmt_Challan = new Cnmt_Challan();
							cnmt_Challan.setCnmtId(cnmt.getCnmtId());
							cnmt_Challan.setChlnId(challanId);
							cnmt_Challan.setCnmtNoOfPkg(Integer.parseInt(String.valueOf(cnmtCodeList.get(0).get("pkg"))));
							cnmt_Challan.setCnmtTotalWt(Double.parseDouble(String.valueOf(cnmtCodeList.get(0).get("wt"))));
							cnmt_ChallanDAO.saveCnmt_Challan(cnmt_Challan);
							
							cnmt.setTrans(false);
							cnmtDAO.updateCnmt(cnmt);
							finalResult.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
						}else{
							finalResult.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
						}
					}else{
						challan.setView(false);
						challan.setUserCode(currentUser.getUserCode());
						challan.setbCode(currentUser.getUserBranchCode());
						System.out.println("challan.lryNo = "+challan.getChlnLryNo());
						//int challanId = challanDAO.saveChallan(challan);
						
						challanDetail.setbCode(currentUser.getUserBranchCode());
						challanDetail.setUserCode(currentUser.getUserCode());
						int challanId = challanDAO.saveChlnAndChd(challan , challanDetail);
						
						if(challanId > 0){
							Cnmt_Challan cnmt_Challan = new Cnmt_Challan();
							cnmt_Challan.setCnmtId(cnmt.getCnmtId());
							cnmt_Challan.setChlnId(challanId);
							cnmt_Challan.setCnmtNoOfPkg(Integer.parseInt(String.valueOf(cnmtCodeList.get(0).get("pkg"))));
							cnmt_Challan.setCnmtTotalWt(Double.parseDouble(String.valueOf(cnmtCodeList.get(0).get("wt"))));
							cnmt_ChallanDAO.saveCnmt_Challan(cnmt_Challan);
							
							cnmt.setTrans(true);
							cnmtDAO.updateCnmt(cnmt);
							finalResult.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
						}else{
							finalResult.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
						}
					}	
			}else{
				System.out.println("cnmt not exist");
			}

			return finalResult;

		}else{
			finalResult.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			return finalResult;
		}
	}

	
	
	
	
	
	
	
	
	@RequestMapping(value = "/fetchMultiCnmtList", method = RequestMethod.GET)
	public @ResponseBody Object fetchMultiCnmtListt() {
		System.out.println("enter into fetchMultiCnmtList function");
		List<Map<String,Object>> multiCnmtList = new ArrayList<Map<String,Object>>();
		multiCnmtList = multiCnmtService.getAllMultiCnmt();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("list", multiCnmtList);
		return map;
	}

	@RequestMapping(value = "/addMultiCnmt", method = RequestMethod.POST)
	public @ResponseBody Object addMultiCnmt(@RequestBody Map<String,Object> clientMap) {

		System.out.println("enter into addMultiCnmt function");
		multiCnmtService.addMultiCnmt(clientMap);

		Map<String, Object> map = new HashMap<String, Object>();
		map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		return map;
	}

	@RequestMapping(value = "/removeMultiCnmt", method = RequestMethod.POST)
	public @ResponseBody Object removeMultiCnmt(@RequestBody Map<String,Object> clientMap) {

		System.out.println("enter into removeMultiCnmt function");
		multiCnmtService.deleteMultiCnmt(clientMap);

		Map<String, Object> map = new HashMap<String, Object>();
		map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		return map;
	}

	@RequestMapping(value = "/removeAllMultiCnmt", method = RequestMethod.POST)
	public @ResponseBody Object removeAllMultiCnmt() {

		System.out.println("enter into removeMultiCnmt function");
		multiCnmtService.deleteAllMultiCnmt();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		return map;
	}

	@RequestMapping(value = "/uploadChallanImage", method = RequestMethod.POST)
	public @ResponseBody Object uploadChallanImage(@RequestParam("file") MultipartFile file)throws Exception {
		System.out.println("enter into uploadChallanImage function---->"+file.getSize());
		byte [] byteArr = file.getBytes();
		System.out.println("challan image = "+byteArr);
		Blob blob = null;
		Map<String,Object> map = new HashMap<String, Object>();
		try{
			//blob = Hibernate.createBlob(fileContent);
			modelService.setBlob(null);
			blob = new SerialBlob(byteArr);
			System.out.println("blob ==="+blob.length());
			modelService.setBlob(blob); 
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);		
		}catch(Exception e){
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
			e.printStackTrace();
		}
		return map;
	}

	@RequestMapping(value = "/EditChallanSubmit", method = RequestMethod.POST)
	public @ResponseBody Object EditChallanSubmit(@RequestBody Challan challan) {

		int temp=challanDAO.updateChallan(challan);
		Map<String, Object> map = new HashMap<String, Object>();

		if(temp>0){
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return map;
	}

	@RequestMapping(value = "/challanDetails", method = RequestMethod.POST)
	public @ResponseBody Object challanDetails(@RequestBody String chlnCode) {
		Challan challan = new Challan();

		List<Challan> challans = challanDAO.getChallanList(chlnCode);

		Map<String, Object> map = new HashMap<String, Object>();
		Map<String,Object> chlnMap = new HashMap<String, Object>();

		if(!challans.isEmpty()){

			chlnMap.put(ChallanCNTS.CHLN_ID,challans.get(0).getChlnId());
			chlnMap.put(ChallanCNTS.BRANCH_CODE,challans.get(0).getBranchCode());
			chlnMap.put(ChallanCNTS.CHLN_ADVANCE,challans.get(0).getChlnAdvance());
			chlnMap.put(ChallanCNTS.CHLN_BALANCE,challans.get(0).getChlnBalance());
			chlnMap.put(ChallanCNTS.CHLN_BR_RATE,challans.get(0).getChlnBrRate());
			chlnMap.put(ChallanCNTS.CHLN_CHG_WT,challans.get(0).getChlnChgWt());
			chlnMap.put(ChallanCNTS.CHALLAN_CODE,challans.get(0).getChlnCode());
			chlnMap.put(ChallanCNTS.CHLN_CHG_WT,challans.get(0).getChlnChgWt());
			chlnMap.put(ChallanCNTS.CHLN_DT,challans.get(0).getChlnDt());
			chlnMap.put(ChallanCNTS.CHLN_EMP_CODE,challans.get(0).getChlnEmpCode());
			chlnMap.put(ChallanCNTS.CHLN_EXTRA,challans.get(0).getChlnExtra());
			chlnMap.put(ChallanCNTS.CHLN_FREIGHT,challans.get(0).getChlnFreight());
			chlnMap.put(ChallanCNTS.CHLN_FROM_STN,challans.get(0).getChlnFromStn());
			chlnMap.put(ChallanCNTS.CHLN_LOADING_AMT,challans.get(0).getChlnLoadingAmt());
			chlnMap.put(ChallanCNTS.CHLN_LRY_LOAD_TIME,challans.get(0).getChlnLryLoadTime());
			chlnMap.put(ChallanCNTS.CHLN_LRY_NO,challans.get(0).getChlnLryNo());
			chlnMap.put(ChallanCNTS.CHLN_LRY_RATE,challans.get(0).getChlnLryRate());
			chlnMap.put(ChallanCNTS.CHLN_LRY_REP_DT,challans.get(0).getChlnLryRepDT());
			chlnMap.put(ChallanCNTS.CHLN_LRY_RPT_TIME,challans.get(0).getChlnLryRptTime());
			chlnMap.put(ChallanCNTS.CHLN_NO_OF_PKG,challans.get(0).getChlnNoOfPkg());
			chlnMap.put(ChallanCNTS.CHLN_PAY_AT,challans.get(0).getChlnPayAt());
			chlnMap.put(ChallanCNTS.CHLN_RR_NO,challans.get(0).getChlnRrNo());
			chlnMap.put(ChallanCNTS.CHLN_STATISTICAL_CHG,challans.get(0).getChlnStatisticalChg());
			chlnMap.put(ChallanCNTS.CHLN_TDS_AMOUNT,challans.get(0).getChlnTdsAmt());
			chlnMap.put(ChallanCNTS.CHLN_TIME_ALLOW,challans.get(0).getChlnTimeAllow());
			chlnMap.put(ChallanCNTS.CHLN_TO_STN,challans.get(0).getChlnToStn());
			chlnMap.put(ChallanCNTS.CHLN_TOTAL_FREIGHT,challans.get(0).getChlnTotalFreight());
			chlnMap.put(ChallanCNTS.CHLN_TOTAL_WT,challans.get(0).getChlnTotalWt());
			chlnMap.put(ChallanCNTS.CHLN_TRAIN_NO,challans.get(0).getChlnTrainNo());
			chlnMap.put(ChallanCNTS.CHLN_VEHICLE_TYPE,challans.get(0).getChlnVehicleType());
			chlnMap.put(ChallanCNTS.CHLN_WT_SLIP,challans.get(0).getChlnWtSlip());
			chlnMap.put(ChallanCNTS.CREATION_TS,challans.get(0).getCreationTS());
			chlnMap.put(ChallanCNTS.USER_BRANCH_CODE,challans.get(0).getUserCode());
			chlnMap.put(ChallanCNTS.USER_BRANCH_CODE,challans.get(0).getbCode());
			chlnMap.put(ChallanCNTS.IS_VIEW,challans.get(0).isView());
			chlnMap.put(ChallanCNTS.USER_CODE,challans.get(0).getUserCode());

			/*Blob blob = challans.get(0).getChallanImage();
			if(blob == null){
				map.put("image","no");
			}else{
				map.put("image","yes");
			}*/

			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("challan", chlnMap);
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}

		return map;
	}	


	@RequestMapping(value = "/viewchallan", method = RequestMethod.POST)
	public @ResponseBody Object chdDetails(@RequestBody String chlnCode){
		System.out.println("enter into chdDetails function");
		Map<String,Object> map = challanDAO.getChallanNChallanDet(chlnCode);
		Map<String,Object> chlnMap = new HashMap<String, Object>();  

		Map<String , Object> finalMap = new HashMap<String, Object>();

		if ((Integer)(map.get("temp"))>0) {

			Challan challan = (Challan)map.get("challan");
			System.out.println("branch Code of challan === "+challan.getBranchCode());

			chlnMap.put(ChallanCNTS.CHLN_ID,challan.getChlnId());
			chlnMap.put(ChallanCNTS.BRANCH_CODE,challan.getBranchCode());
			chlnMap.put(ChallanCNTS.CHLN_LRY_NO,challan.getChlnLryNo());
			chlnMap.put(ChallanCNTS.CHLN_FROM_STN,challan.getChlnFromStn());
			chlnMap.put(ChallanCNTS.CHLN_TO_STN,challan.getChlnToStn());
			chlnMap.put(ChallanCNTS.CHLN_EMP_CODE,challan.getChlnEmpCode());
			chlnMap.put(ChallanCNTS.CHLN_LRY_RATE,challan.getChlnLryRate());
			chlnMap.put(ChallanCNTS.CHLN_CHG_WT,challan.getChlnChgWt());
			chlnMap.put(ChallanCNTS.CHLN_NO_OF_PKG,challan.getChlnNoOfPkg());
			chlnMap.put(ChallanCNTS.CHLN_TOTAL_WT,challan.getChlnTotalWt());
			chlnMap.put(ChallanCNTS.CHLN_FREIGHT,challan.getChlnFreight());
			chlnMap.put(ChallanCNTS.CHLN_LOADING_AMT,challan.getChlnLoadingAmt());
			chlnMap.put(ChallanCNTS.CHLN_EXTRA,challan.getChlnExtra());
			chlnMap.put(ChallanCNTS.CHLN_TOTAL_FREIGHT,challan.getChlnTotalFreight());
			chlnMap.put(ChallanCNTS.CHLN_ADVANCE,challan.getChlnAdvance());
			chlnMap.put(ChallanCNTS.CHLN_BALANCE,challan.getChlnBalance());
			chlnMap.put(ChallanCNTS.CHLN_PAY_AT,challan.getChlnPayAt());
			chlnMap.put(ChallanCNTS.CHLN_TIME_ALLOW,challan.getChlnTimeAllow());
			chlnMap.put(ChallanCNTS.CHLN_DT,challan.getChlnDt());
			chlnMap.put(ChallanCNTS.CHLN_BR_RATE,challan.getChlnBrRate());
			chlnMap.put(ChallanCNTS.CHLN_WT_SLIP,challan.getChlnWtSlip());
			chlnMap.put(ChallanCNTS.CHLN_VEHICLE_TYPE,challan.getChlnVehicleType());
			chlnMap.put(ChallanCNTS.CHLN_STATISTICAL_CHG,challan.getChlnStatisticalChg());
			chlnMap.put(ChallanCNTS.CHLN_TDS_AMOUNT,challan.getChlnTdsAmt());
			chlnMap.put(ChallanCNTS.CHLN_RR_NO,challan.getChlnRrNo());
			chlnMap.put(ChallanCNTS.CHLN_TRAIN_NO,challan.getChlnTrainNo());
			chlnMap.put(ChallanCNTS.CHLN_LRY_LOAD_TIME,challan.getChlnLryLoadTime());
			chlnMap.put(ChallanCNTS.CHLN_LRY_REP_DT,challan.getChlnLryRepDT());
			chlnMap.put(ChallanCNTS.CHLN_LRY_RPT_TIME,challan.getChlnLryRptTime());
			chlnMap.put(ChallanCNTS.CREATION_TS,challan.getCreationTS());
			chlnMap.put(ChallanCNTS.USER_BRANCH_CODE,challan.getbCode());
			chlnMap.put(ChallanCNTS.CHALLAN_CODE,challan.getChlnCode());
			chlnMap.put(ChallanCNTS.USER_CODE,challan.getUserCode());

			finalMap.put("challan",chlnMap);
			finalMap.put("challanDetail",map.get("challanDetail"));
			finalMap.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else {
			finalMap.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		return finalMap;
	}

	@RequestMapping(value="/getChallanList",method = RequestMethod.POST)
	public @ResponseBody Object getChallanList(){

		List<String> challanList= challanDAO.getChallanCode();
		Map<String,Object> map = new HashMap<String,Object>();
		if (!challanList.isEmpty()){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			map.put("list", challanList);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;	
	}
	
	@RequestMapping(value = "/getBrStLeafDetDataByChln", method = RequestMethod.POST)
	public @ResponseBody Object getBrStLeafDetDataByChln(@RequestBody String chlnCode) {
		logger.info("Enter into /getBrStLeafDetDataByChln() : chlnCode = "+chlnCode);		
		User currentUser = (User)httpSession.getAttribute("currentUser");
		String branchCode = currentUser.getUserBranchCode();
		Map<String, Object> map = new HashMap<String, Object>();
		//List<BranchStockLeafDet> bList = branchStockLeafDetDAO.getBranchStockLeafDetData(branchCode);
		String brhName = branchDAO.getBrNameByBrCode(branchCode);
		List<String> chlnList = branchStockLeafDetDAO.getAvlChln(branchCode, chlnCode);		
		if(!chlnList.isEmpty()){
			map.put("list", chlnList);
			map.put("brhName", brhName);
			map.put("brhCode", branchCode);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		/*if(!bList.isEmpty()){
			map.put("list", bList);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}*/
		return map;
	}
	
	@RequestMapping(value = "/getBrStLeafDetData", method = RequestMethod.POST)
	public @ResponseBody Object getBrStLeafDetData() {		
		System.out.println("enter into getBrStLeafDetData function");		
		logger.info("Enter into /getBrStLeafDetData()....");				
		User currentUser = (User)httpSession.getAttribute("currentUser");
		String branchCode = currentUser.getUserBranchCode();
		Map<String, Object> map = new HashMap<String, Object>();
		//List<BranchStockLeafDet> bList = branchStockLeafDetDAO.getBranchStockLeafDetData(branchCode);
		String brhName = branchDAO.getBrNameByBrCode(branchCode);
		List<String> chlnList = null;
		if(chlnCode == null)
			chlnList = branchStockLeafDetDAO.getAvlChln(branchCode);
		else
			chlnList = branchStockLeafDetDAO.getAvlChln(branchCode, chlnCode);
		
		if(!chlnList.isEmpty()){
			map.put("list", chlnList);
			map.put("brhName", brhName);
			map.put("brhCode", branchCode);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		/*if(!bList.isEmpty()){
			map.put("list", bList);
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}*/
		return map;
	}

	public double getChlnDaysLast() {

		Date date = new Date(new java.util.Date().getTime());
		Calendar calendar =Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MONTH, -1);
		Date endDate = new Date(calendar.getTime().getTime());
		double chlnLeft=0;
		User currentUser = (User)httpSession.getAttribute("currentUser");
		double daysLeft = 0;
		double chlnPerDayUsed = branchSStatsDAO.getLastMonthPerDayUsed(date,endDate,"chln");
		if(chlnPerDayUsed>0){
			chlnLeft = branchStockLeafDetDAO.getLeftStationary(currentUser.getUserBranchCode(),"chln");
			daysLeft=chlnLeft/chlnPerDayUsed;
		}


		return daysLeft;
	}


	public double getAvgMonthlyUsageForChln() {

		System.out.println("------Enter into getAvgMonthlyUsage function");

		Date todayDate = new Date(new java.util.Date().getTime());
		Date firstEntryDate = branchSStatsDAO.getFirstEntryDate("chln");
		double average=0.00;
		if(firstEntryDate != null){
			Calendar calendar =Calendar.getInstance();
			calendar.setTime(todayDate);
			calendar.add(Calendar.MONTH, -1);
			Date oneMonthPrevDate = new Date(calendar.getTime().getTime());

			calendar.add(Calendar.MONTH, -1);
			Date twoMonthPrevDate = new Date(calendar.getTime().getTime());

			calendar.add(Calendar.MONTH, -1);
			Date threeMonthPrevDate = new Date(calendar.getTime().getTime());

			double days = (double)( (todayDate.getTime() - firstEntryDate.getTime()) / (1000 * 60 * 60 * 24));

			double chlnForOneMnth = branchSStatsDAO.getNoOfStationary(todayDate, oneMonthPrevDate,"chln");
			double chlnForScndMnth = branchSStatsDAO.getNoOfStationary(oneMonthPrevDate, twoMonthPrevDate,"chln");
			double chlnForThirdMnth = branchSStatsDAO.getNoOfStationary(twoMonthPrevDate, threeMonthPrevDate,"chln");
			double chlnBwFrstAndScndMnth = branchSStatsDAO.getNoOfStationary(firstEntryDate, twoMonthPrevDate,"chln");
			double chlnBwFrstAndOneMnth=branchSStatsDAO.getNoOfStationary(firstEntryDate, oneMonthPrevDate,"chln");
			double chlnBwTodayAndFrstDate=branchSStatsDAO.getNoOfStationary(todayDate, firstEntryDate,"chln");



			if(days>=90){
				average = ((chlnForOneMnth*3) + (chlnForScndMnth*2) + chlnForThirdMnth)/6;
			}
			else if(days>=60 && days<90){
				int daysBwFrstAndScndMnth = (int)( (twoMonthPrevDate.getTime() - firstEntryDate.getTime()) / (1000 * 60 * 60 * 24));		

				average = (((chlnForOneMnth*3) + (chlnForScndMnth*2) + (chlnBwFrstAndScndMnth/daysBwFrstAndScndMnth))*30)/6;
			}
			else if(days>=30 && days<60){

				int daysBwFrstAndOneMnth = (int)( (oneMonthPrevDate.getTime() - firstEntryDate.getTime()) / (1000 * 60 * 60 * 24));
				average = (((chlnForOneMnth*2) +  (chlnBwFrstAndOneMnth/daysBwFrstAndOneMnth))*30)/3;
			}
			else if(days<30){
				int daysBwFrstAndToday = (int)( (todayDate.getTime() - firstEntryDate.getTime()) / (1000 * 60 * 60 * 24));
				if(daysBwFrstAndToday == 0){
					average = chlnBwTodayAndFrstDate*30;
				}else{
					average = ((chlnBwTodayAndFrstDate/daysBwFrstAndToday)*30);
				}
			}
		}	
		return average;
	}



	@RequestMapping(value = "/editChlnAndChlnDet", method = RequestMethod.POST)
	public @ResponseBody Object editChlnAndChlnDet(@RequestBody Chln_ChlnDetService chln_ChlnDetService){
		System.out.println("Entered into editChlnAndChlnDet of controller----");

		Map<String,String> map = new HashMap<String, String>();
		//Blob blob = modelService.getBlob();
		Challan challan = chln_ChlnDetService.getChallan();
		ChallanDetail challanDetail = chln_ChlnDetService.getChallanDetail();
		int temp=challanDAO.updateChallan(challan);
		if (temp>=0) {
			int res = challanDetailDAO.updateChallanDetail(challanDetail);
			if(res > 0){
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
			}else{
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			}
		}
		
		
	/*	if(blob != null){
			System.out.println("new challan image length added");
			challan.setChallanImage(blob);
			int temp=challanDAO.updateChallan(challan);
			if (temp>=0) {
				int res = challanDetailDAO.updateChallanDetail(challanDetail);
				if(res > 0){
					map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
				}else{
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}
			}
		}else{
			Blob preImg = challanDAO.getChallanImageByCode(challan.getChlnCode());
			if(preImg != null){
				challan.setChallanImage(preImg);
			}
			int temp=challanDAO.updateChallan(challan);
			if (temp>=0) {
				int res = challanDetailDAO.updateChallanDetail(challanDetail);
				if(res > 0){
					map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
				}else{
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				}
			}
		}*/
		return map;
	}

	
	@RequestMapping(value = "/saveDemoDt", method = RequestMethod.POST)
	public @ResponseBody Object saveDemoDt(@RequestBody String date){
		System.out.println("Entered into saveDemoDt of controller----"+date);
		/*SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try{
			Date actDate = formatter.parse(date);
			System.out.println("actDate = "+actDate);
		}catch(Exception e){
			e.printStackTrace();
		}*/
		Map<String,Object> map = new HashMap<String,Object>();
		
	
		return map;
	}	
	
	
	@RequestMapping(value = "/getVehicleMstrFC", method = RequestMethod.POST)
	public @ResponseBody Object getVehicleMstrFC(){
		System.out.println("Entered into getVehicleMstrFC of controller----");
		Map<String,Object> map = new HashMap<String,Object>();
		List<String> vehList = vehicleVendorDAO.getAllVehNo();
		if(!vehList.isEmpty()){
			map.put("list",vehList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}	
	
	@RequestMapping(value = "/getVehicleMstrFCByLryNo", method = RequestMethod.POST)
	public @ResponseBody Object getVehicleMstrFCByLryNo(@RequestBody String lryNo){
		logger.info("Enter into getVehicleMstrFCLryNo() : lryNoO = "+lryNo);		
		Map<String,Object> map = new HashMap<String,Object>();
		List<String> vehList = vehicleVendorDAO.getAllVehNoByLryNo(lryNo);
		if(!vehList.isEmpty()){
			map.put("list",vehList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		logger.info("Exit from getVehicleMstrFCByLryNo()");
		return map;
	}	
	
	
	
	@RequestMapping(value = "/selectVehFC", method = RequestMethod.POST)
	public @ResponseBody Object selectVehFC(@RequestBody String vehNo){
		logger.info("Enter into /selectVehFC : VehNo = "+vehNo);		
		Map<String,Object> map = new HashMap<String,Object>();
		if(vehNo != null){
			VehicleVendorMstr veh = vehicleVendorDAO.getVehByVehNo(vehNo);
			if(veh != null){
				
				Owner owner = veh.getOwner();
				Broker broker = veh.getBroker();
				
				map.put("ownName", owner.getOwnName());
				map.put("ownCode", owner.getOwnCode());
				if(broker != null) {
					map.put("brkName", broker.getBrkName());
					map.put("brkCode", broker.getBrkCode());
					
				}
				
				if(! owner.isOwnIsPanImg()){
					if(broker != null)
					if(! broker.isBrkIsPanImg()){
						map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
						map.put("msg", "Owner/Broker does not have PAN image !");
						logger.info("Owner/Broker does not have Pan Image : OwnID = "+owner.getOwnId()+" BrkID = "+broker.getBrkId());
						return map;	
					}
				}
				
				List<String> ownMobList = new ArrayList<>();
				List<String> brkMobList = new ArrayList<>();
								
				
				ownMobList = owner.getOwnPhNoList();
				if(broker != null)
				brkMobList = broker.getBrkPhNoList();			
				
				if(ownMobList != null && !ownMobList.isEmpty()){
					map.put("ownMobList",ownMobList);
				}else{
					map.put("ownMobList",new ArrayList<String>());
				}
				
				if(broker != null) {
					if(brkMobList != null && !brkMobList.isEmpty()){
						map.put("brkMobList",brkMobList);
					}else{
						map.put("brkMobList",new ArrayList<String>());
					}
				}else{
					map.put("brkMobList",new ArrayList<String>());
				}
								
				map.put("vehM",veh);
				map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
				logger.info("Success....");
			}else{
				map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
				logger.info("Error.....");
			}
		}else{
			logger.info("Error........");
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		logger.info("Exit from /selectVehFC()");
		return map;
	}
	
	
	
	@RequestMapping(value = "/chkOwnBrkPD", method = RequestMethod.POST)
	public @ResponseBody Object chkOwnBrkPD(@RequestBody Map<String,String> clientMap){
		logger.info("Enter into /chkOwnBrkPD() : OwnCode = "+clientMap.get("ownCode")+" : BrkCode = "+clientMap.get("brkCode"));
		
		String ownCode = clientMap.get("ownCode");
		String brkCode = clientMap.get("brkCode");
		
		Map<String,Object> map = new HashMap<String,Object>();		
		
		int ownPan = ownerDAO.checkOwnPan(ownCode);
		int  brkPan = 0;
		if(ownPan < 1)
			brkPan = brokerDAO.checkBrkPan(brkCode);
			
		if(ownPan > 0 || brkPan > 0){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		logger.info("Exit from /chkOwnBrkPD()");
		return map;
	}
	
	@RequestMapping(value = "/getChallanNo", method = RequestMethod.POST)
	public @ResponseBody Object getChallanNo(){
		System.out.println("Entered into getChallanNo controller----");
		Map<String,Object> map = new HashMap<String,Object>();
		
		List<String> challanNoList = challanDAO.getChlnCodeNotInChd();
		
		if (!challanNoList.isEmpty()) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("challanNoList", challanNoList);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	@RequestMapping(value = "/saveChallanDetail", method=RequestMethod.POST)
	public @ResponseBody Object saveChallanDetail(@RequestBody Map<String, String> chdService){
		System.out.println("saveChallanDetail()");
		Map<String, Object> map = new HashMap<>();
		
		int temp = challanDAO.saveChallanDetail(chdService);
		
		if (temp > 0) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else if(temp == -1){
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			map.put("errorType", "Database error");
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			map.put("errorType", "Owner or broker has no PAN Card");
		}
		
		return map;
	}
	
	@RequestMapping(value="/getChlnNoList", method=RequestMethod.POST)
	public @ResponseBody Object getChlnNoList(){
		System.out.println("getChlnNoList()");
		Map<String, Object> map = new HashMap<>();
		
		List<String> chlnNoList = challanDAO.getChallanCode();
		
		if (!chlnNoList.isEmpty()) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("chlnNoList", chlnNoList);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	@RequestMapping(value="/getChlnForEdit", method=RequestMethod.POST)
	public @ResponseBody Object getChlnForEdit(@RequestBody String chlnCode){
		System.out.println("getChlnForEdit(): "+chlnCode);
		
		Map<String,Object> chlnNChd = challanDAO.getChallanNChallanDet(chlnCode);
		
		//ChallanDetail challanDetail = (ChallanDetail) chlnNChd.get("challanDetail");
		
		int temp = (int) chlnNChd.get("temp");
		
		if (temp>0) {
			chlnNChd.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			Challan challan = (Challan) chlnNChd.get("challan");
			challan.setChlnLryRate(String.valueOf(Float.parseFloat(challan.getChlnLryRate())*1000));
			challan.setChlnChgWt(String.valueOf(Float.parseFloat(challan.getChlnChgWt())/1000));
			challan.setChlnTotalWt(String.valueOf(Float.parseFloat(challan.getChlnTotalWt())/1000));
			chlnNChd.put("challan", challan);
		} else {
			chlnNChd.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return chlnNChd;
	}
	
	
	@RequestMapping(value="/validateOwnBrkPan", method=RequestMethod.POST)
	public @ResponseBody Object validateOwnBrkPan(@RequestBody Map<String, Object> ownBrkId){
		System.out.println("validateOwnBrkPan()");
		logger.info("Enter into validateOwnBrkPan()...");
		Map<String, Object> map = new HashMap<>();
		
		System.out.println("owner id: "+ownBrkId.get("ownId"));
		System.out.println("broker id: "+ownBrkId.get("brkId"));
		
		logger.info("Owner ID : "+ownBrkId.get("ownId") +" :: Broker ID : "+ownBrkId.get("brkId"));
		
		boolean isPan = ownerDAO.isOwnBrkHasPan((Integer)ownBrkId.get("ownId"), (Integer)ownBrkId.get("brkId"));
		
		if (isPan) {
			logger.info("Pan img exists !");
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			map.put("msg", "Pan Validation success");
		}else {
			logger.info("Pan img does not exist !");
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			map.put("msg", "neither owner nor broker has pan");
		}
		
		return map;
	}
	
	@RequestMapping(value="/updateChlnNChd", method=RequestMethod.POST)
	public @ResponseBody Object updateChlnNChd(@RequestBody Chln_ChlnDetService chlnNChdService){
		System.out.println("updateChlnNChd()");
		Map<String, Object> map = new HashMap<>();
		
		Challan challan = chlnNChdService.getChallan();
		ChallanDetail challanDetail = chlnNChdService.getChallanDetail();
		
		challan.setChlnLryRate(String.valueOf(Float.parseFloat(challan.getChlnLryRate())/1000));
		challan.setChlnChgWt(String.valueOf(Float.parseFloat(challan.getChlnChgWt())*1000));
		challan.setChlnTotalWt(String.valueOf(Float.parseFloat(challan.getChlnTotalWt())*1000));
		
		int temp = challanDAO.updateChlnNChd(challan, challanDetail);
		
		if (temp>0) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	@RequestMapping(value="/getCnmtByChln", method=RequestMethod.POST)
	public @ResponseBody Object getCnmtByChln(@RequestBody String chlnCode){
		System.out.println("getCnmtByChln");
		
		Map<String, Object> map = challanDAO.getCnmtByChln(chlnCode);
		
		if ((Integer)map.get("temp")>0) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	@RequestMapping(value="/submitChlnNCnmt", method=RequestMethod.POST)
	public @ResponseBody Object submitChlnNCnmt(@RequestBody Cnmt_ChallanService chlnNCnmtService){
		System.out.println("submitChlnNCnmt()");
		Map<String, Object> map = new HashMap<>();
		
		int temp = challanDAO.submitChlnNCnmt(chlnNCnmtService);
		
		if (temp>0) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	@RequestMapping(value="/chlnDtlFrCncl", method=RequestMethod.POST)
	public @ResponseBody Object chlnDtlFrCncl(@RequestBody Map<String,String> chlnDtl){
		System.out.println("submitChlnNCnmt()");
		Map<String, Object> map = new HashMap<>();
		
		int temp = challanDAO.chlnDtlFrCncl(chlnDtl);
		
		if (temp>0) {
			map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
		} else {
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
		}
		
		return map;
	}
	
	
	
	@RequestMapping(value = "/getBblVehicleMstrFC", method = RequestMethod.POST)
	public @ResponseBody Object getBblVehicleMstrFC(@RequestBody String vehNo){
		System.out.println("Entered into getBblVehicleMstrFC of controller----");
		Map<String,Object> map = new HashMap<String,Object>();
		List<String> vehList = vehicleVendorDAO.getAllVehNoLike(vehNo);
		if(!vehList.isEmpty()){
			map.put("list",vehList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}	
	
	
	
	//TODO for app
	@RequestMapping(value="/submitChlnChlnDetByApp",method = RequestMethod.POST)
	public @ResponseBody Object submitChlnChlnDetByApp(@RequestBody Map<String, Object> clientMap){
		
		Map<String,Object> map = new HashMap<>();
		
		
		Cnmt cnmt=cnmtDAO.getCnmtFrFndAlctn(clientMap.get("cnmtCode").toString());
		Employee emp=employeeDAO.getCmpltEmp(clientMap.get("empId").toString());
		VehicleEngagement ve=vehicleEngagementDAO.getVehicleEngagementByVeId(cnmt.getvEId());
		VehicleVendorMstr vv=vehicleVendorDAO.getVehByVehNo(ve.getVehicleNo());
		Indent indent=indentDAO.getIndentById(ve.getCampaignResults().getIndentId());
		//System.out.println("broker code"+clientMap.get("brkCode").toString());
		Broker broker=brokerDAO.getBrkByCode(clientMap.get("chdBrCode").toString());
		float rate=Float.parseFloat(clientMap.get("chlnLryRate").toString());
		//float actualWt=(float) clientMap.get("actualWt");
		float chargeWt=Float.parseFloat(clientMap.get("chlnChgWt").toString());
		float extra=Float.parseFloat(clientMap.get("chlnExtra").toString());
		float statistical=Float.parseFloat(clientMap.get("chlnStatisticalChg").toString());
		
		double chlnFrt=Math.round(rate*chargeWt);
		double chlnTotalFrt=Math.round(chlnFrt+extra-statistical);
		
		double advance=Math.round(chlnTotalFrt*indent.getAdvancePercent()/100);
		double balance=chlnTotalFrt-advance;
		
		java.util.Date todayDt=new java.util.Date();
		Date chlnDt=new Date(todayDt.getTime());
		
		if(vv!=null) {
			
			Session session=this.sessionFactory.openSession();
			Transaction transaction=session.beginTransaction();
			try {
				Challan chln=new Challan();
				chln.setbCode(emp.getBranchCode());
				chln.setBranchCode(emp.getBranchCode());
				chln.setCancel(false);
				chln.setChlnAdvance(String.valueOf(advance));
				chln.setChlnArId(-1);
				chln.setChlnBalance(balance);
				chln.setChlnBalPayAlw(false);
				chln.setChlnBrRate(String.valueOf(rate));
				chln.setChlnChgWt(String.valueOf(chargeWt));
				//chln.setChlnCode("");
				chln.setChlnCraneChg(Float.parseFloat(clientMap.get("chlnCraneChg").toString()));
				chln.setChlnDetection(Float.parseFloat(clientMap.get("chlnDetention").toString()));
				chln.setChlnDt(chlnDt);
				chln.setChlnEdit(false);
				chln.setChlnEmpCode(emp.getEmpCode());
				chln.setChlnExtra(clientMap.get("chlnExtra").toString());
				chln.setChlnFreight(String.valueOf(chlnFrt));
				chln.setChlnFromStn(cnmt.getCnmtFromSt());
				chln.setChlnHeight(Float.parseFloat(clientMap.get("chlnHeight").toString()));
				chln.setChlnLoadingAmt(clientMap.get("chlnLoadingAmt").toString());
				chln.setChlnLryLoadTime(cnmt.getPlacedOn().get(Calendar.HOUR_OF_DAY)+":"+cnmt.getPlacedOn().get(Calendar.MINUTE));
				chln.setChlnLryNo(vv.getVvRcNo());
				chln.setChlnLryRate(String.valueOf(rate/1000));
				chln.setChlnLryRepDT(new Date(cnmt.getPlacedOn().getTimeInMillis()));
				chln.setChlnLryRptTime(cnmt.getPlacedOn().get(Calendar.HOUR_OF_DAY)+":"+cnmt.getPlacedOn().get(Calendar.MINUTE));
				chln.setChlnNoOfPkg(String.valueOf(cnmt.getCnmtNoOfPkg()));
				chln.setChlnOthers(Float.parseFloat(clientMap.get("chlnOthers").toString()));
				chln.setChlnPayAt("1");
				chln.setChlnRemAdv(advance);
				chln.setChlnRemBal(balance);
				chln.setChlnStatisticalChg(clientMap.get("chlnStatisticalChg").toString());
				chln.setChlnTdsAmt("");
				chln.setChlnTimeAllow(String.valueOf(indent.getTransitTime()));
				chln.setChlnToolTax(Float.parseFloat(clientMap.get("chlnTollTax").toString()));
				chln.setChlnToStn(cnmt.getCnmtToSt());
				chln.setChlnTotalFreight(chlnTotalFrt);
				chln.setChlnTotalWt(String.valueOf(cnmt.getCnmtActualWt()));
				chln.setChlnTrainNo("");
				chln.setChlnTwoPoint(Float.parseFloat(clientMap.get("chlnTwoPoint").toString()));
				chln.setChlnType("Normal");
				chln.setChlnUnion(Float.parseFloat(clientMap.get("chlnUnion").toString()));
				chln.setChlnWeightmentChg(Float.parseFloat(clientMap.get("chlnWeightmentChg").toString()));
				chln.setChlnWtSlip(clientMap.get("chlnWtSlip").toString());
				chln.setChlnRemarks(clientMap.get("chlnRemarks").toString());
				chln.setChlnVehicleType(indent.getVehicleType());
				int chlnId=challanDAO.saveChallan(chln,session);
				
				String bcode=null;
				if(Integer.parseInt(emp.getPrBranch().getBranchCode())<10) {
					bcode="0"+emp.getPrBranch().getBranchCode();
				}else {
					bcode=emp.getPrBranch().getBranchCode();
				}
				
				String chlnCode="CG"+bcode+chlnId;
				chln.setChlnCode(chlnCode);
				challanDAO.updateChallan(chln,session);
				
				ChallanDetail chd=new ChallanDetail();
				chd.setbCode(emp.getBranchCode());
				chd.setChdBrCode(broker.getBrkCode());
				if(broker.getBrkPhNoList() != null && !broker.getBrkPhNoList().isEmpty())
					chd.setChdBrMobNo(broker.getBrkPhNoList().get(0));
				
				
				chd.setChdChassisNo(vv.getVvChassisNo());
				chd.setChdChlnCode(chln.getChlnCode());
				//chd.setChdChlnDt("");
				//chd.setChdCode("");
				//chd.setChdDlIssueDt(chdDlIssueDt);
				chd.setChdDlNo(clientMap.get("chdDlNo").toString());
				//chd.setChdDlValidDt(chdDlValidDt);
				chd.setChdDvrMobNo(clientMap.get("chdDvrMobNo").toString());
				chd.setChdDvrName(clientMap.get("chdDvrName").toString());
				chd.setChdEdit(false);
				chd.setChdEngineNo(vv.getVvEngineNo());
				chd.setChdFitDocIssueDt(vv.getVvFitIssueDt());
				chd.setChdFitDocNo(vv.getVvFitNo());
				chd.setChdFitDocPlace("");
				chd.setChdFitDocValidDt(vv.getVvFitValidDt());
				chd.setChdInvalidPan(false);
				chd.setChdModel(vv.getVvModel());
				chd.setChdOwnCode(vv.getOwner().getOwnCode());
				if(vv.getOwner().getOwnPhNoList() != null && !vv.getOwner().getOwnPhNoList().isEmpty())
					chd.setChdOwnMobNo(vv.getOwner().getOwnPhNoList().get(0));
				chd.setChdPanHdrName(vv.getOwner().getOwnName());
				chd.setChdPanHdrType("owner");
				chd.setChdPanIssueSt("");
				chd.setChdPanNo(vv.getOwner().getOwnPanNo());
				chd.setChdPerIssueDt(vv.getVvPerIssueDt());
				chd.setChdPerNo(vv.getVvPerNo());
				chd.setChdPerState(vv.getVvPerState());
				chd.setChdPerValidDt(vv.getVvPerValidDt());
				chd.setChdPolicyCom(vv.getVvPolicyComp());
				chd.setChdPolicyNo(vv.getVvPolicyNo());
				chd.setChdRcIssueDt(vv.getVvRcIssueDt());
				chd.setChdRcNo(vv.getVvRcNo());
				chd.setChdRcValidDt(vv.getVvRcValidDt());
				chd.setChdTransitPassNo(vv.getVvTransitPassNo());
				chd.setChdType("normal");
				chd.setChdValidPan(false);
				chd.setUserCode(emp.getEmpCode());
				
				challanDetailDAO.saveChlnDetailToDB(chd,session);
				
				
				ve.setDriverName(clientMap.get("chdDvrName").toString());
				ve.setDriverNo(clientMap.get("chdDvrMobNo").toString());
				ve.setAlternateConsent(clientMap.get("alternateConsent").toString());
				ve.setDriverAlternateNo(clientMap.get("alternateDvrMobNo").toString());
				ve.setAlternateNetwork(clientMap.get("alternateNetwork").toString());
				ve.setConsent(clientMap.get("consent").toString());
				ve.setNetwork(clientMap.get("network").toString());
				ve.setPlaceReqStatus("Complete");
				/*
				"alternateDvrMobNo":"1234567890",
				"alternateDlNo":"12345",
				"alternateDlValidDt":"2019-01-01"*/
				vehicleEngagementDAO.updateVehicleEngagement(ve, session);
				
				
				Cnmt_Challan cc=new Cnmt_Challan();
				cc.setChlnId(chlnId);
				cc.setCnmtId(cnmt.getCnmtId());
				cc.setCnmtNoOfPkg(cnmt.getCnmtNoOfPkg());
				cc.setCnmtTotalWt(cnmt.getCnmtActualWt());
				cnmt_ChallanDAO.saveCnmt_Challan(cc, session);
				
				session.flush();
				session.clear();
				transaction.commit();
				
				map.put("chlnCode", chlnCode);
				map.put("isSuccess", true);
				map.put("message", "challan no.= "+chlnCode+" successfully generated ");
				
				
			}catch(Exception e) {
				map.put("chlnCode", null);
				map.put("isSuccess", false);
				map.put("message", e);
				e.printStackTrace();
				transaction.rollback();
			}finally {
				session.close();
				
				if(map.get("chlnCode")!=null) {
					try {
					List<String> phoneNoList=new ArrayList<>();
					phoneNoList.add("9810090729");
			        phoneNoList.add("9718663997");
			        phoneNoList.add("8468991703");
			        phoneNoList.add("9599282616");
			        phoneNoList.add("9873919313");
			        phoneNoList.add("9999966633");
			        phoneNoList.add("7760503926");
			        phoneNoList.add("7905189881");
					
					Station from=stationDAO.getStationByStnCode(cnmt.getCnmtFromSt());
					Station to=stationDAO.getStationByStnCode(cnmt.getCnmtToSt());
					
					String msg="Challan No."+map.get("chlnCode")+" is generated from  "+from.getStnName()+ " to "+to.getStnName()+".";
					SmtpMailSender mailSender=new SmtpMailSender();
					mailSender.sendSMS(phoneNoList, msg);
					}catch(Exception e) {
						e.printStackTrace();
					}
				}
			}
			
			
		}else {
			map.put("chlnCode", null);
			map.put("isSuccess", false);
			map.put("message", "Vehicle No. not registered!");
		}
		
		return map;
	}

	
	
	
}
