package org.apache.jsp.WEB_002dINF.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class header_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\n");
      out.write("<nav class=\"navbar navbar-inverse navbar-fixed-top\" style=\"background-image: url('resources/img/header.jpg');  background-size:cover;\">\n");
      out.write("\t<div style=\"background: rgba(0, 0, 0, 0.2)\">\n");
      out.write("      <div class=\"container-fluid\" >\n");
      out.write("        <div class=\"navbar-header\">\n");
      out.write("          <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navbar\" aria-expanded=\"false\" aria-controls=\"navbar\">\n");
      out.write("            <span class=\"sr-only\">Toggle navigation</span>\n");
      out.write("            <span class=\"icon-bar\"></span>\n");
      out.write("            <span class=\"icon-bar\"></span>\n");
      out.write("            <span class=\"icon-bar\"></span>\n");
      out.write("          </button>\n");
      out.write("          <a class=\"navbar-brand\"  href=\"\"><b>{{currentBranch}}</b></a>\n");
      out.write("          <a class=\"navbar-brand\"  href=\"\"><b>{{currentEmpName}}</b></a>\n");
      out.write("        </div>\n");
      out.write("        <div id=\"navbar\" class=\"navbar-collapse collapse\" >\n");
      out.write("          <div ng-if=\"operatorLogin\">\n");
      out.write("\n");
      out.write("          <ul class=\"nav navbar-nav navbar-right\">\n");
      out.write("            <li><a href=\"#/operator\">Dashboard</a></li>\n");
      out.write("            <li><a href=\"#/settings\">Settings</a></li>\n");
      out.write("            <li><a href=\"#/profile\">Profile</a></li>\n");
      out.write("            <li><a href=\"#help\">Help</a></li>\n");
      out.write("            <li><a href=\"\" ng-click=\"logout()\">Logout</a></li>\n");
      out.write("          </ul>\n");
      out.write("          </div>\n");
      out.write("          <div ng-if=\"adminLogin\">\n");
      out.write("          <ul class=\"nav navbar-nav navbar-right\">\n");
      out.write("\t\t\t<li><a href=\"#/admin\">Dashboard</a></li>\n");
      out.write("            <li><a href=\"#/settings\">Settings</a></li>\n");
      out.write("            <li><a href=\"#/profile\">Profile</a></li>\n");
      out.write("            <li><a href=\"#help\">Help</a></li>\n");
      out.write("            <li><a href=\"\" ng-click=\"logout()\">Logout</a></li>\n");
      out.write("          </ul>\n");
      out.write("          </div>\n");
      out.write("          <div ng-if=\"superAdminLogin\">\n");
      out.write("     \t       <ul class=\"nav navbar-nav navbar-right\">\n");
      out.write("\t\t\t\t<li><a href=\"#/superAdmin\">Dashboard</a></li>\n");
      out.write("            \t<li><a href=\"#/settings\">Settings</a></li>\n");
      out.write("            \t<li><a href=\"#/profile\">Profile</a></li>\n");
      out.write("            \t<li><a href=\"#help\">Help</a></li>\n");
      out.write("            \t<li><a href=\"\" ng-click=\"logout()\">Logout</a></li>\n");
      out.write("          </ul>\n");
      out.write("          </div>\n");
      out.write("          <form class=\"navbar-form navbar-right\">\n");
      out.write("            <input type=\"text\" class=\"form-control\" placeholder=\"Search...\">\n");
      out.write("          </form>\n");
      out.write("        </div>\n");
      out.write("      </div>\n");
      out.write("     </div>\n");
      out.write("</nav>\n");
      out.write("\n");
      out.write("\n");
      out.write("<!-- content visible if login true -->\n");
      out.write("<!-- <div ng-if=\"operatorLogin || adminLogin || superAdminLogin\">\n");
      out.write("<header>\n");
      out.write(" --><!-- <div class=\"navbar-fixed\">\n");
      out.write("<nav>\n");
      out.write("    <div class=\"nav-wrapper\">\n");
      out.write("    </div> \n");
      out.write("      <a href=\"#\" class=\"brand-logo\" style=\"text-decoration:none; padding-left:10px;\">My Logistics</a>\n");
      out.write("      <ul id=\"nav-mobile\" class=\"right hide-on-med-and-down\">\n");
      out.write("        <li><a >3</a></li>\n");
      out.write("        <li><a >2</a></li>\n");
      out.write("        <li><a >1</a></li>\n");
      out.write("      </ul>\n");
      out.write("\n");
      out.write("</nav>\n");
      out.write("\n");
      out.write("</div>\n");
      out.write(" --> \n");
      out.write(" <!-- <div class=\"navbar-fixed\">\n");
      out.write("    <nav>\n");
      out.write("      <div class=\"nav-wrapper\">\n");
      out.write("        <a class=\"brand-logo\" style=\"text-decoration:none; padding-left:10px;\">My Logistics</a>\n");
      out.write("      </div>\n");
      out.write("    </nav>\n");
      out.write("  </div>\n");
      out.write("</header>\n");
      out.write("</div>\n");
      out.write(" -->");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
