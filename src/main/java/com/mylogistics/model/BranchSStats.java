package com.mylogistics.model;

//import java.util.Date;
import java.sql.Date;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "branchsstats")
public class BranchSStats {
	
	@Id
	@Column(name="brsStatsId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int brsStatsId;
	
	@Column(name="brsStatsBrCode")
	private String brsStatsBrCode;
	
	@Column(name="brsStatsDt")
	private Date brsStatsDt;
	
	@Column(name="brsStatsType")
	private String brsStatsType;
	
	@Column(name="brsStatsSerialNo")
	private String brsStatsSerialNo;
	
	@Column(name="userCode")
	private String userCode;
	
	@Column(name="creationTS", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=false)
	private Calendar creationTS;
	
	@Column(name="bCode")
	private String bCode;

	public int getBrsStatsId() {
		return brsStatsId;
	}

	public void setBrsStatsId(int brsStatsId) {
		this.brsStatsId = brsStatsId;
	}

	public String getBrsStatsBrCode() {
		return brsStatsBrCode;
	}

	public void setBrsStatsBrCode(String brsStatsBrCode) {
		this.brsStatsBrCode = brsStatsBrCode;
	}

	public Date getBrsStatsDt() {
		return brsStatsDt;
	}

	public void setBrsStatsDt(Date brsStatsDt) {
		this.brsStatsDt = brsStatsDt;
	}

	public String getBrsStatsType() {
		return brsStatsType;
	}

	public void setBrsStatsType(String brsStatsType) {
		this.brsStatsType = brsStatsType;
	}

	public String getBrsStatsSerialNo() {
		return brsStatsSerialNo;
	}

	public void setBrsStatsSerialNo(String brsStatsSerialNo) {
		this.brsStatsSerialNo = brsStatsSerialNo;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public Calendar getCreationTS() {
		return creationTS;
	}

	public void setCreationTS(Calendar creationTS) {
		this.creationTS = creationTS;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}
	
	
}
