package com.mylogistics.DAO;

import java.sql.Blob;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.mylogistics.model.Broker;
import com.mylogistics.model.BrokerImg;
import com.mylogistics.model.Owner;
import com.mylogistics.model.OwnerImg;
import com.mylogistics.model.User;
import com.mylogistics.services.PanService;

@Repository
public interface BrokerDAO {
	
	//Sanjana: saves broker to the database
	 public int saveBroker(Broker broker);
	
	//Sanjana: retrieves the last row of broker contract table
	 public int getLastBrokerId();
	
	//Sanjana: returns the total number of rows in broker table
	 public long totalCount();
	
	//Sanjana: retrieves the list of broker whose isView is false
	 public List<Broker> getBrokerisViewFalse(String branchCode);
		
	//Sanjana: updates the broker isView to true
	 public int updateBrokerisViewTrue(String code[]);
		
	//Sanjana: retrieves broker from database on the basis of entered broker code
	 public List<Broker> getBrokerList(String brkCode);
	
	//Sanjana: updates the broker
	 public int updateBroker(Broker broker);
	 
	 public void updateBroker(Session session,Broker broker);
	 
	 public void updateBrkImg(Session session,BrokerImg img);
	 
	 public int saveBrkImg(Session session,BrokerImg img);
	 
	//Sanjana: get the list of brokerCode
	public List<String> getBrokerCode();
	
	//Sanjana: get the list of brokerCodes based on entered branchCode
	public List<String> getBrokerCodes(String branchCode);

	 public List<Broker> getBroker();
	 
	 public Broker getBrokerData(int id);
	 
	 //Navneet :get the list of Broker Codes whose Bank Details is not filled
	 public List<Map<String, String>> getBrkCodesIsBnkD();

	 public List<Map<String,String>> getBrkNCF();
	 
	 public List<Map<String,String>> getBrkNCFByName(String brkName);
	 
	 public int saveBrkAndImg(Broker broker , BrokerImg brokerImg, Blob panImg, Blob decImg);
	 
	 public List<Map<String, Object>> getBrkNameCodeId();
	 
	 public Broker getBrkByFaCode(String faCode);
	 
	 public int checkBrkPan(String brkCode);
	 
	 public Map<String, Object> saveBrkPan(PanService panService, BrokerImg brokerImg, float brkIntRate, boolean isPanImg, boolean isDecImg, Blob panImg, Blob decImg);
	 
	 public Blob getBrkPanImg(int brkImgId);
	 
	 public List<Map<String, Object>> getBrkNameCodeIdFa();
	 
	 public Broker findBrokerByBrh_Name(String branchCode, String brkName);
	 
	 public List<Broker> getBrokerByNameCode(Session session, User user, String brkNameCode, List<String> proList);

	public List<Map<String, Object>> getBrkNamePhCodeIdFa();

	public List<Map<String, Object>> getBrkNameCodeByPanNo(String panNo);
	
	public Boolean isBrokerExist(Session session,String brkPan);
	
	public int saveBrkAndImgN(Broker broker , BrokerImg brokerImg, Blob panImg, Blob decImg,Blob chqImg,Session session);
	 
	public List<Map<String, Object>> getBrkNameCodeIdByName(String brkName);

	public List<Broker> getBrkFrBnkVerify();
	
	public List<Broker> getBrokerInvalidAcc();
	
	public Map<String,String> bnkInfoValid(Integer brkId);
	
	public Map<String,String> bnkInfoInValid(int brkId);
	
	public Map<String,String> uploadChqImg(byte fileInBytes[],int brkId);
	
	public Map<String, String> uploadPanImg(byte[] fileInBytes, int brkId);
	
	public Map<String, String> uploadDecImg(byte[] fileInBytes, int brkId);


	public Map<String, String> bnkInfoUpdate(Broker newBrk);
	
	public List<Broker> getBrkFrAdvByName(String brkName);
	
	public Broker getBrkByCode(String brkCode,Session session);

	public Broker getBrkByCode(String brkCode);

	public List<Broker> getPhoneNo();

	public int saveBroker(Broker broker, Session session);
	 
	
	
}
