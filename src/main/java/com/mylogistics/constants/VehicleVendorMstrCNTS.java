package com.mylogistics.constants;

public class VehicleVendorMstrCNTS {

	public static final String VV_ID = "vvId";
	
	public static final String B_CODE = "bCode";
	
	public static final String VV_RCNO = "vvRcNo";
	
	public static final String VV_CHASSIS_NO = "vvChassisNo";
	
	public static final String VV_ENGINE_NO = "vvEngineNo";
	
}
