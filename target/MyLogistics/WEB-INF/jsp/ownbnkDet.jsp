<div ng-show="operatorLogin || superAdminLogin">
	<div class="container">
	<div class="row">
	<form class="card" name="BankDetOwnForm" ng-submit="addBankOwnDet(BankDetOwnForm)" style="align: center; margin-top: 40px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
		
		<div class="col s4 input-field">
		       	<input class="validate" type ="text" id="ownerCodeId" name ="ownerCode" ng-model="owner.ownCode"  ng-keyup = "getOwnCodeByIsBnk(owner.ownCode)"  ng-required="true" >
		       	<label for="code">Enter owner Name</label>	
		 </div>
		 <div class="input-field col s6">
		<input type="text" name="ownAcntHldrName" ng-model="owner.ownAcntHldrName" ng-required="true"><br>
		<label>Account Holder Name</label>
		</div>
		<div class="input-field col s6">
		<input type="text" name="accntNo" ng-model="owner.ownAccntNo" ng-required="true"><br>
		<label>Account Number</label>
		</div>
		<div class="input-field col s6">
		<input type="text" name="ifsc" ng-model="owner.ownIfsc" ng-pattern="/^[A-Z]{4}0[A-Z0-9]{6}$/" ng-required="true">
		<label>IFSC</label>
		</div>
		<div class="input-field col s6">
		<input type="text" name="micr" ng-model="owner.ownMicr" >
		<label>MICR</label>
		</div>
		
		<div class="input-field col s6" >
			<input type="text" name="bnkBranch" ng-model="owner.ownBnkBranch" ng-required = "true" >
			<label>Bank Name</label>
		</div>
		
		<div class="row" > <!-- ng-show="uploadCCFlag" -->
					<div class="input-field col s9">
						<input type="file" class="teal white-text btn" file-model="ccImage">
						<label>Choose File for Cancelled cheque</label>
					</div>
					<div class="col s3">
						<a class="btn waves-effect waves-light white-text col s12"
							type="button" ng-click="uploadCCImage(ccImage)">Upload</a>
					</div>
				</div>
		
		<div class="col s12 center">
			<input type="submit" value="Add Bank Details"/>
		</div>
	</form>
	</div>
	</div>
	
	  <div id ="ownerCodeDB" ng-hide="ownerCodeDBFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox.ownerCode" placeholder="Search by Broker Code ">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Owner Code </th>
 	  	  	  <th> Owner Name </th>
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="ownrCode in ownerCodeList | filter:filterTextbox.ownerCode ">
		 	  <td><input type="radio"  name="ownrCode"   value="{{ownrCode}}" ng-model="ownCode " ng-click="saveOwnerCode(ownrCode.ownCode)"></td>
              <td>{{ownrCode.ownCode}}</td>
              <td>{{ownrCode.ownName}}</td> 
          </tr>
      </table> 
	</div>
	
	
</div>