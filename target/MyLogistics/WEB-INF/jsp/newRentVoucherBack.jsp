<div ng-show="operatorLogin || superAdminLogin">
	<div class="row">
		<form name="newVoucherForm" ng-submit=voucherSubmit(newVoucherForm) class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);">
			<div class="row">
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="branchFaCodeId" name ="branchFaCode" ng-model="vs.branch.branchFaCode"  readonly ng-required="true" >
		       			<label for="code">Branch Code</label>	
		       		</div>

		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="branchNameId" name ="branchName" ng-model="vs.branch.branchName" readonly ng-required="true" >
		       			<label for="code">Branch Name</label>	
		       		</div>
		       		
		       			<div class="col s4 input-field">
		       				<input class="validate" type ="date" id="dateId" name ="dateTemp" ng-model="dateTemp"   readonly  >
		       			<label for="code">Date</label>	
		       		</div>
		    </div>
		    
		    <div class="row">
		     		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="sheetNoId" name ="sheetNo" ng-model="vs.cashStmtStatus.cssCsNo" readonly ng-required="true" >
		       			<label for="code">Sheet No</label>	
		       		</div>
			   		 <div class="col s4 input-field">
		       				<input class="validate" type ="text" id="voucherTypeId" name ="voucherType" ng-model="vs.voucherType" readonly ng-required="true" >
		       			<label for="code">Voucher Type</label>	
		       		</div>
		       		<div class="col s4 input-field">
				    		<select name="payBy" id="payById" ng-model="vs.payBy" ng-change="selectPayBy()" ng-init="vs.payBy = 'C'" ng-required="true">
								<option value='C'>By Cash</option>
								<option value='Q'>By Cheque</option>
								<option value='O'>By Online</option>
							</select>
							<label>Payment By</label>
					</div>
		    </div>
		    
		     <div class="row">
		     		<div class="col s4 input-field">
			    		<select name="chequeType" id="chequeTypeId" ng-model="vs.chequeType" ng-change="selectChqType()" disabled="disabled">
							<option value='C'>Computerized</option>
							<option value='M'>Manual</option>
						</select>
						<label>Cheque Type </label>
					</div>
					
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="bankCodeId" name ="bankCode" ng-model="vs.bankCode"  ng-click = "openBankCodeDB()" readonly disabled="disabled">
		       			<label for="code">Bank Code</label>	
		       		</div>
		       		
		       		<div class="col s4 input-field">
		       				<input class="validate" type ="text" id="chequeNoId" name ="chequeNo" ng-model="vs.chequeLeaves.chqLChqNo" ng-click="openChqDB()" readonly disabled="disabled">
		       			<label for="code">Cheque No</label>	
		       		</div>
		    </div>
		       	
		    <div class="row">
	     		<!-- <div class="col s4 input-field">
	       				<input type ="text" id="payToId" name ="payToName" ng-model="vs.payTo" disabled="disabled">
	       			<label for="code">Pay To</label>	
	       		</div>
	       		
	       		<div class="col s4 input-field">
					<input type ="text" name="phNoName" id="phNoId" ng-model="phNo" ng-click="openPhNoDB()" disabled="disabled" readonly ng-required="true">
					<label>Select Phone No.</label>
				</div> -->
				
				<div class="col s4 input-field">
		       			<input class="validate" type ="button" id="rentId" value="RENT" ng-click="selectRentId()" ng-required="true" >
		       	</div>
	   		</div>
	  
	        <div class="row"> 		
				 <div ng-show="rentList.length > 0">
					<table class="tblrow">
					<caption class="coltag tblrow">Rent Voucher Details</caption>
						<tr class="rowclr">
							<th class="colclr">Branch Name</th>
							<th class="colclr">Employee Name</th>
							<th class="colclr">Rent For</th>
							<th class="colclr">Landlord Name</th>
							<th class="colclr">Rent Amount</th>
							<th class="colclr">Tds Code</th>
							<th class="colclr">Tds Amount</th>
							<th class="colclr">Action</th>
						</tr>
						<tr class="tbl" ng-repeat="rent in rentList">
							<td class="rowcel">{{ rent.brhName }}</td>
							<td class="rowcel">{{ rent.empName }}</td>
							<td class="rowcel">{{ rent.rentFr }}</td>
							<td class="rowcel">{{ rent.landLord }}</td>
							<td class="rowcel">{{ rent.amt }}</td>
							<td class="rowcel">{{ rent.tdsCode }}</td>
							<td class="rowcel">{{ rent.tdsAmt }}</td>
							<td class="rowcel"><input type="button" value="remove" ng-click="removeRent($index,rent)" /></td>
						</tr>
						<tr class="tbl" >							
							<td class="rowcel" colspan="4">Total Amount</td>
							<td class="rowcel">{{amtSum}}</td>
							<td class="rowcel"></td>
							<td class="rowcel">{{tdsAmtSum}}</td>
							<td class="rowcel"></td>							
						</tr>
					</table>
				</div>
			</div>	
	
	   		 <div class="row">
				<div class="input-field col s12">
					<i class="mdi-editor-mode-edit prefix"></i>
 					<textarea id="textarea" class="materialize-textarea"  rows="3" cols="92"  name ="desc" ng-model="vs.desc" ng-required="true"></textarea>
 					<label>Description</label>
				</div>
    		 </div>	
	   		 
	   		  <div class="row">
	     		<div class="input-field col s12 center">
	       				<input class="validate" type ="submit" value="submit" >
	       		</div>
	   		 </div>
		  </form>
    </div>
    
   <div id ="rentDB" ng-hide="rentDBFlag">
		  <input type="text" name="filterRentbox" ng-model="filterRentbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>RentId</th>
 	  	 	  <th>LandLord Name</th>
 	  	 	  <th>Rent For</th>
 	  	 	  <th>Branch Name</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="rent in rentInfo | filter:filterRentbox">
		 	  <td><input type="radio"  name="rent"  value="{{ rent }}" ng-model="rentNo" ng-click="selectRent(rent)"></td>
              <td>{{ rent.rmId }}</td>
              <td>{{ rent.landLord }}</td>
              <td>{{ rent.rentFr }}</td>
              <td>{{ rent.brhName }}</td>
          </tr>
      </table> 
	</div>   
    
    <div id ="bankCodeDB" ng-hide="bankCodeDBFlag">
		  <input type="text" name="filterTextbox" ng-model="filterTextbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th> Bank Code </th>
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="bankCode in bankCodeList | filter:filterTextbox">
		 	  <td><input type="radio"  name="bankCode"   value="{{ bankCode }}" ng-model="bkCode" ng-click="saveBankCode(bankCode)"></td>
              <td>{{bankCode}}</td>
              
          </tr>
      </table> 
	</div>
	
	
	<div id ="selChqDB" ng-hide="selChqDBFlag">
		  <input type="text" name="filterTextChq" ng-model="filterTextChq" placeholder="Search...">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>Cheque No</th>
 	  	 
 	  	  </tr>
 	  
		  <tr ng-repeat="chq in chqList | filter:filterTextChq">
		 	  <td><input type="radio"  name="chq"   value="{{ chq }}" ng-model="chqCode" ng-click="saveChqNo(chq)"></td>
              <td>{{chq.chqLChqNo}}</td>
              
          </tr>
      </table> 
	</div>
	
	<div id ="tdsCodeDB" ng-hide="tdsCodeDBFlag">
		  <input type="text" name="filterTdsbox" ng-model="filterTdsbox" placeholder="Search">
 	  <table>
 	  	  <tr>
 	  	  	  <th></th>
 	  	  	  <th>FaCode</th>
 	  	  	  <th>Name</th>
 	  	  	  <th>Type</th>
 	  	  </tr>
 	  
		  <tr ng-repeat="faM in faMList | filter:filterTdsbox">
		 	  <td><input type="radio"  name="faM"   value="{{ faM }}" ng-model="faMCode" ng-click="saveTdsCode(faM)"></td>
              <td>{{faM.faMfaCode}}</td>
              <td>{{faM.faMfaName}}</td>
              <td>{{faM.faMfaType}}</td>
          </tr>
      </table> 
	</div>
	
	
<div id="rentPayDB" ng-hide="rentPayDBFlag">
	<form class="col s12 card" style="align: center; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" name="rentPayForm" ng-submit="submitRentPay(rentPayForm)">
		 <div class="row">
			    <div class="col s4 input-field">
					<input class="validate" type ="number" name="payAmt" id="payAmtId" ng-model="rs.amt" ng-required="true">
					<label>Rent Amount</label>
				</div>
				
				<div class="col s4 input-field">
					<input type ="text" name="brhName" id="brhId" ng-model="rs.brhName" ng-required="true" readonly />
					<label>Branch Name</label>
				</div>
				
				 <div class="col s4 input-field">
					<input type ="text" name="empName" id="empId" ng-model="rs.empName" readonly disabled="disabled">
					<label>Employee Name</label>
				</div>
	     </div>
	     
	     <div class="row">
	     
	     		<div class="col s4 input-field">
		   		  <input type="checkbox" ng-model="tdsCheck" ng-init="tdsCheck=false" ng-change="allowTds()"/>	
		   			<label>TDS</label>
		   		</div>
		   		
	     		<div class="col s4 input-field">
					<input type ="text" name="tdsName" id="tdsId" ng-model="rs.tdsCode" ng-click="openTdsCode()" readonly disabled="disabled" ng-required="tdsCodeReq">
					<label>TDS Code</label>
				</div>
				
			    <div class="col s4 input-field">
					<input type ="number" name="tdsAmt" id="tdsAmtId" ng-model="rs.tdsAmt" disabled="disabled" />
					<label>TDS Amount</label>
				</div>
			
	     </div>
	     
	      <div class="row">
	     		<div class="input-field col s12 center">
	       				<input class="validate" type ="submit" value="submit" >
	       		</div>
	       </div>		
	     
	</form>
	</div>
	
	
	 <div id="saveVsDB" ng-hide="saveVsFlag"> 
		<div class="col s6 card" style="align: center; margin-top: 0px; padding-top:40px;  background-color: rgba(125, 125, 125, 0.3);" >
			<table class="table-hover table-bordered table-bordered">
				<!-- <h2>Voucher Details</h2> -->
				<tr>
					<td>Branch Code</td>
					<td>{{vs.branch.branchFaCode}}</td>
				</tr>
				
				<tr>
					<td>Branch Name</td>
					<td>{{vs.branch.branchName}}</td>
				</tr>
				
				<tr>
					<td>Date</td>
					<td>{{dateTemp}}</td>
				</tr>
				
				<tr>
					<td>Sheet No</td>
					<td>{{vs.cashStmtStatus.cssCsNo}}</td>
				</tr>
				
				<tr>
					<td>Voucher Type</td>
					<td>{{vs.voucherType}}</td>
				</tr>
				
				<tr>
					<td>Payment By</td>
					<td>{{vs.payBy}}</td>
				</tr>
				
				<tr>
					<td>Cheque Type</td>
					<td>{{vs.chequeType}}</td>
				</tr>
				
				<tr>
					<td>Bank Code</td>
					<td>{{vs.bankCode}}</td>
				</tr>
				
				<tr>
					<td>Cheque No</td>
					<td>{{vs.chequeLeaves.chqLChqNo}}</td>
				</tr>
				
				<tr>
					<td>Pay To</td>
					<td>{{vs.payTo}}</td>
				</tr>
				
				<tr>
					<td>Description</td>
					<td>{{vs.desc}}</td>
				</tr>
			</table>
			<input type="button" value="Cancel" ng-click="back()"/>
			<input type="button" value="Save" id="saveId" ng-click="saveVS()"/>
			
		</div>
	</div>
    
</div>    