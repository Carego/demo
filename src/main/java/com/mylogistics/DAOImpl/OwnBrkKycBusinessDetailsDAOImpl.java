package com.mylogistics.DAOImpl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.mylogistics.DAO.OwnBrkKycBusinessDetailsDAO;
import com.mylogistics.model.OwnBrkKycBusinessDetails;

public class OwnBrkKycBusinessDetailsDAOImpl implements OwnBrkKycBusinessDetailsDAO{
	private SessionFactory sessionFactory;
	private Session session;
	private Transaction transaction;
	
	public OwnBrkKycBusinessDetailsDAOImpl(SessionFactory sessionFactory){
		this.sessionFactory = sessionFactory;
	}
	
	@Override
	public int saveOwnBrkBusinessDetail(Session session,OwnBrkKycBusinessDetails businessDetails) {
		return (int) session.save(businessDetails);
	}
	
	@Override
	public List<OwnBrkKycBusinessDetails> getOwnBrkBusinessDetailByCode(String ownBrkCode) {
		session=sessionFactory.openSession();
		List<OwnBrkKycBusinessDetails> ownBrkList=session.createCriteria(OwnBrkKycBusinessDetails.class)
				.add(Restrictions.eq("ownBrkCode", ownBrkCode)).list();
		session.close();
		return ownBrkList;
	}
	
}
