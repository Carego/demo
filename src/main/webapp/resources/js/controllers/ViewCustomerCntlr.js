'use strict';

var app = angular.module('application');

app.controller('ViewCustomerCntlr',['$scope','$location','$http','$filter',
                                    function($scope,$location,$http,$filter){

	$scope.CustomerCodeDBFlag=true;
	$scope.show = "true";

	$scope.OpenCustomerCodeDB = function(){
		$scope.CustomerCodeDBFlag=false;
		$('div#customerCodeDB').dialog({
			autoOpen: false,
			modal:true,
			title: "Customer Code",
			show: UDShow,
			hide: UDHide,
			position: UDPos,
			resizable: false,
			draggable: true,
			close: function(event, ui) { 
				$(this).dialog('destroy') ;
				$(this).hide();
			}
		});

		$('div#customerCodeDB').dialog('open');
	}

	$scope.saveCustomerCode = function(cst){
		$scope.custCode = cst.custCode;
		$scope.custCodeTemp = cst.custFaCode;
		$('div#CustomerCodeDB').dialog('close');
		$scope.CustomerCodeDBFlag=true;
	}

	$scope.getCustomerCodeList = function(){
		console.log("getCustomerCodeData------>");
		var response = $http.post($scope.projectName+'/getCustListFV');
		response.success(function(data, status, headers, config){
			if(data.result === "success"){
				$scope.custCodeList = data.list;
			}else{
				console.log(data);
			}
		});
		response.error(function(data, status, headers, config) {
			console.log("error in CustomerRepCntlr");
			$scope.errorToast(data);
		});
	}

	$scope.submitCustomer = function(custCode){
		$scope.code=$('#customerCode').val();
		if($scope.code === ""){
			$scope.alertToast("Please enter customer code....");
		}else{
			$scope.show = "false";
			var response = $http.post($scope.projectName+'/submitcustomer',custCode);
			response.success(function(data, status, headers, config) {
				if(data.result === "success"){
					console.log(data);
					$scope.customer = data.customer;
					$scope.custRepList = data.customerRepList;
					$scope.customer.creationTS =  $filter('date')($scope.customer.creationTS, 'MM/dd/yyyy hh:mm:ss');
					$scope.successToast(data.result);
				}else{
					console.log(data);
				}
			});
			response.error(function(data, status, headers, config) {
				$scope.errorToast(data);
			});
		}
	}

	if($scope.operatorLogin === true || $scope.superAdminLogin === true){
		$scope.getCustomerCodeList();
	 }else if($scope.logoutStatus === true){
		 $location.path("/");
	 }else{
		 console.log("****************");
	 } 
	
	

}]);