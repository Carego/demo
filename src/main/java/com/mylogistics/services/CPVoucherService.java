package com.mylogistics.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CPVoucherService {

	private VoucherService voucherService;
	
	private List<Map<String,Object>> faList = new ArrayList<Map<String,Object>>();

	public VoucherService getVoucherService() {
		return voucherService;
	}

	public void setVoucherService(VoucherService voucherService) {
		this.voucherService = voucherService;
	}

	public List<Map<String, Object>> getFaList() {
		return faList;
	}

	public void setFaList(List<Map<String, Object>> faList) {
		this.faList = faList;
	}
	
	
}
