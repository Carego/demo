package com.mylogistics.DAOImpl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mylogistics.DAO.CashStmtStatusDAO;
import com.mylogistics.constants.BankCSCNTS;
import com.mylogistics.constants.BranchCNTS;
import com.mylogistics.constants.CashStmtCNTS;
import com.mylogistics.constants.CashStmtStatusCNTS;
import com.mylogistics.constants.CashStmtTempCNTS;
import com.mylogistics.constants.FAMasterCNTS;
import com.mylogistics.constants.bank.AtmCardMstrCNTS;
import com.mylogistics.constants.bank.BankMstrCNTS;
import com.mylogistics.constants.bank.ChequeLeavesCNTS;
import com.mylogistics.model.BankCS;
import com.mylogistics.model.Branch;
import com.mylogistics.model.CashStmt;
import com.mylogistics.model.CashStmtStatus;
import com.mylogistics.model.CashStmtTemp;
import com.mylogistics.model.FAMaster;
import com.mylogistics.model.User;
import com.mylogistics.model.bank.AtmCardMstr;
import com.mylogistics.model.bank.BankMstr;
import com.mylogistics.model.bank.ChequeLeaves;
import com.mylogistics.model.lhpv.LhpvStatus;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.VoucherService;

//import java.util.Date;

public class CashStmtStatusDAOImpl implements CashStmtStatusDAO {
	
	private SessionFactory sessionFactory;
	@Autowired
	private HttpSession httpSession;
	@Autowired
	public CashStmtStatusDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public static Logger logger = Logger.getLogger(CashStmtStatusDAOImpl.class);

	@SuppressWarnings("unchecked")
	@Transactional
	public CashStmtStatus getLastCssByBranch(String bCode) {
		CashStmtStatus cashStmtStatus = null;
		Session session = this.sessionFactory.openSession();
		try {
			Query query = session
					.createQuery("from CashStmtStatus where bCode= :type and cssDt >= '2017-11-01' order by cssId DESC");
			query.setParameter("type", bCode);
			List<CashStmtStatus> cashStmtStatusList = query.setMaxResults(1).list();
			if (!cashStmtStatusList.isEmpty())
				cashStmtStatus = cashStmtStatusList.get(0);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.clear();
			session.close();
		}
		return cashStmtStatus;
	}

	@Override
	public CashStmtStatus getLastCssByBranch(String bCode,Session session) {
			Query query = session
					.createQuery("from CashStmtStatus where bCode= :type and cssDt >= '2017-11-01' order by cssId DESC");
			query.setParameter("type", bCode);
			List<CashStmtStatus>  cashStmtStatusList = query.setMaxResults(1).list();			
		if (cashStmtStatusList.isEmpty()) {
			return null;
		} else {
			return cashStmtStatusList.get(0);
		}
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public Map<String, Object> updateCSSByCS(int cssId, CashStmt cashStmt) {
		System.out.println("cashStmt tvNo = " + cashStmt.getCsTvNo());
		List<CashStmtStatus> cssList = new ArrayList<CashStmtStatus>();
		String bCode = cashStmt.getbCode();
		Map<String, Object> map = new HashMap<String, Object>();		
			
		String tvNo = cashStmt.getCsTvNo();	
		map.put("tvNo", tvNo);
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try {
			Criteria cr1 = session.createCriteria(CashStmtStatus.class);
			cr1.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID, cssId));
			cssList = cr1.list();

			if (!cssList.isEmpty()) {
				CashStmtStatus cashStmtStatus = cssList.get(0);
				java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus
						.getCssDt().getTime());
				cashStmt.setCsNo(cashStmtStatus);

				if (cashStmt.getCsVouchNo() > 0) {
					if (cashStmt.getCsTvNo() == null) {
						cashStmt.setCsTvNo(tvNo);
					}
					cashStmt.setCsTvNo(tvNo);
					cashStmtStatus.getCashStmtList().add(cashStmt);
					session.update(cashStmtStatus);
					map.put("vhNo", cashStmt.getCsVouchNo());
				} else {

					Criteria cr = session.createCriteria(CashStmt.class);
					cr.add(Restrictions.eq(CashStmtCNTS.CS_CDT, sqlDate));
					cr.add(Restrictions
							.eq(CashStmtCNTS.USER_BRANCH_CODE, bCode));
					ProjectionList proList = Projections.projectionList();
					proList.add(Projections.property("csVouchNo"));
					cr.setProjection(proList);

					List<Integer> voucherNoList = cr.list();

					System.out.println("todays cashStmt list = "
							+ voucherNoList.size());
					if (!voucherNoList.isEmpty()) {
						int voucherNo = 0;
						for(int i=0;i<voucherNoList.size();i++){
							if(voucherNo < voucherNoList.get(i)){
								voucherNo = voucherNoList.get(i);
							}
						}
						voucherNo = voucherNo + 1;
						System.out.println("voucher ===============> "+voucherNo);
						map.put("vhNo", voucherNo);
						cashStmt.setCsVouchNo(voucherNo);
						if (cashStmt.getCsTvNo() == null) {
							cashStmt.setCsTvNo(tvNo);
						}
						cashStmt.setCsTvNo(tvNo);
						cashStmtStatus.getCashStmtList().add(cashStmt);
						session.update(cashStmtStatus);
					} else {
						cashStmt.setCsVouchNo(1);
						if (cashStmt.getCsTvNo() == null) {
							cashStmt.setCsTvNo(tvNo);
						}
						map.put("vhNo", 1);
						cashStmt.setCsTvNo(tvNo);
						cashStmtStatus.getCashStmtList().add(cashStmt);
						session.update(cashStmtStatus);
					}
				}

			} else {
				map.put("vhNo", 0);
			}
			transaction.commit();
			session.flush();
		} catch (Exception e) {
			e.printStackTrace();
			map.put("vhNo", -1);
		}
		session.clear();
		session.close();
		return map;

	}

	@SuppressWarnings("unchecked")
	@Transactional
	public int updateCSSBySFId(int csId, int csfId) {
		System.out.println("enter into updateCSSBySFId function");
		int result = 0;
		List<CashStmt> csList = new ArrayList<CashStmt>();
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try {
			Criteria cr1 = session.createCriteria(CashStmt.class);
			cr1.add(Restrictions.eq(CashStmtCNTS.CS_ID, csId));
			csList = cr1.list();
			CashStmt cashStmt = csList.get(0);
			cashStmt.setCsSFId(csfId);
			session.update(cashStmt);

			transaction.commit();
			session.flush();
			result = 1;
		} catch (Exception e) {
			e.printStackTrace();
			result = -1;
		}
		session.clear();
		session.close();
		return result;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public int saveCsTemp(CashStmtTemp cashStmtTemp) {
		System.out.println("enter into updateCSSForTelV function--->");
		int res = 0;
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try {
			session.save(cashStmtTemp);
			transaction.commit();
			session.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return res;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public Map<String, Object> updateCSSForTelV(int cssId, CashStmt cashStmt) {
		System.out.println("enter into updateCSSForTelV function--->" + cssId);
		System.out.println("cashStmt tvNo = " + cashStmt.getCsTvNo());
		List<CashStmtStatus> cssList = new ArrayList<CashStmtStatus>();
		String bCode = cashStmt.getbCode();
		Map<String, Object> map = new HashMap<String, Object>();
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try {
			Criteria cr1 = session.createCriteria(CashStmtStatus.class);
			cr1.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID, cssId));
			cssList = cr1.list();

			if (!cssList.isEmpty()) {
				CashStmtStatus cashStmtStatus = cssList.get(0);
				cashStmt.setCsNo(cashStmtStatus);
				java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus
						.getCssDt().getTime());

				Criteria cr = session.createCriteria(CashStmt.class);
				cr.add(Restrictions.eq(CashStmtCNTS.CS_CDT, sqlDate));
				cr.add(Restrictions.eq(CashStmtCNTS.USER_BRANCH_CODE, bCode));
				ProjectionList proList = Projections.projectionList();
				proList.add(Projections.property("csVouchNo"));
				cr.setProjection(proList);

				List<Integer> voucherNoList = cr.list();

				System.out.println("todays cashStmt list = "
						+ voucherNoList.size());
				if (cashStmt.getCsVouchNo() > 0) {
					map.put("vhNo", cashStmt.getCsVouchNo());
					cashStmtStatus.getCashStmtList().add(cashStmt);
					session.update(cashStmtStatus);
				} else {
					if (!voucherNoList.isEmpty()) {
						
						int voucherNo = 0;
						for(int i=0;i<voucherNoList.size();i++){
							if(voucherNo < voucherNoList.get(i)){
								voucherNo = voucherNoList.get(i);
							}
						}
						voucherNo = voucherNo + 1;
						map.put("vhNo", voucherNo);
						cashStmt.setCsVouchNo(voucherNo);
						cashStmtStatus.getCashStmtList().add(cashStmt);
						session.update(cashStmtStatus);
					} else {
						cashStmt.setCsVouchNo(1);
						map.put("vhNo", 1);
						cashStmtStatus.getCashStmtList().add(cashStmt);
						session.update(cashStmtStatus);
					}
				}
			} else {
				map.put("vhNo", 0);
			}
			transaction.commit();
			session.flush();
		} catch (Exception e) {
			e.printStackTrace();
			map.put("vhNo", -1);
		}
		session.clear();
		session.close();
		return map;

	}

	@SuppressWarnings("unchecked")
	@Transactional
	public Map<String, Object> updateCSSForV(int cssId, CashStmt cashStmt) {
		System.out.println("enter into updateCSSForTelV function--->" + cssId);
		System.out.println("cashStmt tvNo = " + cashStmt.getCsTvNo());
		List<CashStmtStatus> cssList = new ArrayList<CashStmtStatus>();
		String bCode = cashStmt.getbCode();

		Map<String, Object> map = new HashMap<String, Object>();
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try {
			Criteria cr1 = session.createCriteria(CashStmtStatus.class);
			cr1.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID, cssId));
			cssList = cr1.list();

			if (!cssList.isEmpty()) {
				CashStmtStatus cashStmtStatus = cssList.get(0);
				cashStmt.setCsNo(cashStmtStatus);
				java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus
						.getCssDt().getTime());

				Criteria cr = session.createCriteria(CashStmt.class);
				cr.add(Restrictions.eq(CashStmtCNTS.CS_CDT, sqlDate));
				cr.add(Restrictions.eq(CashStmtCNTS.USER_BRANCH_CODE, bCode));
				ProjectionList proList = Projections.projectionList();
				proList.add(Projections.property("csVouchNo"));
				cr.setProjection(proList);

				List<Integer> voucherNoList = cr.list();

				System.out.println("todays cashStmt list = "
						+ voucherNoList.size());
				if (cashStmt.getCsVouchNo() > 0) {
					map.put("vhNo", cashStmt.getCsVouchNo());
					cashStmtStatus.getCashStmtList().add(cashStmt);
					session.update(cashStmtStatus);
				} else {
					if (!voucherNoList.isEmpty()) {
						int voucherNo = 0;
						
						for(int i=0;i<voucherNoList.size();i++){
							if(voucherNo < voucherNoList.get(i)){
								voucherNo = voucherNoList.get(i);
							}
						}
						
						voucherNo = voucherNo + 1;
						map.put("vhNo", voucherNo);
						cashStmt.setCsVouchNo(voucherNo);
						cashStmtStatus.getCashStmtList().add(cashStmt);
						session.update(cashStmtStatus);
					} else {
						cashStmt.setCsVouchNo(1);
						map.put("vhNo", 1);
						cashStmtStatus.getCashStmtList().add(cashStmt);
						session.update(cashStmtStatus);
					}
				}
			} else {
				map.put("vhNo", 0);
			}
			transaction.commit();
			session.flush();
		} catch (Exception e) {
			e.printStackTrace();
			map.put("vhNo", -1);
		}
		session.clear();
		session.close();
		return map;

	}

	@SuppressWarnings("unchecked")
	@Transactional
	public Map<String, Object> updateCSSForEleV(int cssId, CashStmt cashStmt) {
		System.out.println("enter into updateCSSForEleV function--->" + cssId);
		System.out.println("cashStmt tvNo = " + cashStmt.getCsTvNo());
		List<CashStmtStatus> cssList = new ArrayList<CashStmtStatus>();
		String bCode = cashStmt.getbCode();
		Map<String, Object> map = new HashMap<String, Object>();
		Date date = new Date(new java.util.Date().getTime());
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		System.out.println("unique id =====>" + calendar.getTimeInMillis());
		long milliSec = calendar.getTimeInMillis();
		String tvNo = String.valueOf(milliSec);
		map.put("tvNo", tvNo);
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try {
			Criteria cr1 = session.createCriteria(CashStmtStatus.class);
			cr1.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID, cssId));
			cssList = cr1.list();

			if (!cssList.isEmpty()) {
				CashStmtStatus cashStmtStatus = cssList.get(0);
				cashStmt.setCsNo(cashStmtStatus);
				java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus
						.getCssDt().getTime());

				Criteria cr = session.createCriteria(CashStmt.class);
				cr.add(Restrictions.eq(CashStmtCNTS.CS_CDT, sqlDate));
				cr.add(Restrictions.eq(CashStmtCNTS.USER_BRANCH_CODE, bCode));
				ProjectionList proList = Projections.projectionList();
				proList.add(Projections.property("csVouchNo"));
				cr.setProjection(proList);

				List<Integer> voucherNoList = cr.list();

				System.out.println("todays cashStmt list = "
						+ voucherNoList.size());
				if (cashStmt.getCsVouchNo() > 0) {
					map.put("vhNo", cashStmt.getCsVouchNo());
					if (cashStmt.getCsTvNo() == null) {
						cashStmt.setCsTvNo(tvNo);
					}
					cashStmtStatus.getCashStmtList().add(cashStmt);
					session.update(cashStmtStatus);
				} else {
					if (!voucherNoList.isEmpty()) {
						int voucherNo = 0;
						for(int i=0;i<voucherNoList.size();i++){
							if(voucherNo < voucherNoList.get(i)){
								voucherNo = voucherNoList.get(i);
							}
						}
						
						voucherNo = voucherNo + 1;
						map.put("vhNo", voucherNo);
						cashStmt.setCsVouchNo(voucherNo);
						if (cashStmt.getCsTvNo() == null) {
							cashStmt.setCsTvNo(tvNo);
						}
						cashStmtStatus.getCashStmtList().add(cashStmt);
						session.update(cashStmtStatus);
					} else {
						cashStmt.setCsVouchNo(1);
						if (cashStmt.getCsTvNo() == null) {
							cashStmt.setCsTvNo(tvNo);
						}
						map.put("vhNo", 1);
						cashStmtStatus.getCashStmtList().add(cashStmt);
						session.update(cashStmtStatus);
					}
				}
			} else {
				map.put("vhNo", 0);
			}
			transaction.commit();
			session.flush();
		} catch (Exception e) {
			e.printStackTrace();
			map.put("vhNo", -1);
		}
		session.clear();
		session.close();
		return map;

	}

	@SuppressWarnings("unchecked")
	@Transactional
	public Map<String, Object> updateCSSByCSWTDS(int cssId, CashStmt cashStmt,
			CashStmt csWithTds) {
		System.out.println("enter into updateCSSByCS function--->" + cssId);
		System.out.println("cashStmt tv No = " + cashStmt.getCsTvNo()
				+ " csWithTds tvNo =" + csWithTds.getCsTvNo());
		List<CashStmtStatus> cssList = new ArrayList<CashStmtStatus>();
		Map<String, Object> map = new HashMap<String, Object>();
		String bCode = cashStmt.getbCode();
		
		String tvNo = String.valueOf(cashStmt.getCsTvNo());
		map.put("tvNo", tvNo);
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try {
			Criteria cr1 = session.createCriteria(CashStmtStatus.class);
			cr1.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID, cssId));
			cssList = cr1.list();

			if (!cssList.isEmpty()) {
				CashStmtStatus cashStmtStatus = cssList.get(0);
				cashStmt.setCsNo(cashStmtStatus);
				csWithTds.setCsNo(cashStmtStatus);
				java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus
						.getCssDt().getTime());

				Criteria cr = session.createCriteria(CashStmt.class);
				cr.add(Restrictions.eq(CashStmtCNTS.CS_CDT, sqlDate));
				cr.add(Restrictions.eq(CashStmtCNTS.USER_BRANCH_CODE, bCode));
				ProjectionList proList = Projections.projectionList();
				proList.add(Projections.property("csVouchNo"));
				cr.setProjection(proList);

				List<Integer> voucherNoList = cr.list();

				System.out.println("todays cashStmt list = "
						+ voucherNoList.size());
				if (!voucherNoList.isEmpty()) {
					int voucherNo = 0;
					for(int i=0;i<voucherNoList.size();i++){
						if(voucherNo < voucherNoList.get(i)){
							voucherNo = voucherNoList.get(i);
						}
					}
					voucherNo = voucherNo + 1;
					map.put("vhNo", voucherNo);
					cashStmt.setCsVouchNo(voucherNo);
					if (cashStmt.getCsTvNo() == null) {
						cashStmt.setCsTvNo(tvNo);
					}
					csWithTds.setCsVouchNo(voucherNo);
					if (csWithTds.getCsTvNo() == null) {
						csWithTds.setCsTvNo(tvNo);
					}
					cashStmtStatus.getCashStmtList().add(cashStmt);
					cashStmtStatus.getCashStmtList().add(csWithTds);
					session.update(cashStmtStatus);
				} else {
					cashStmt.setCsVouchNo(1);
					if (cashStmt.getCsTvNo() == null) {
						cashStmt.setCsTvNo(tvNo);
					}
					csWithTds.setCsVouchNo(1);
					if (csWithTds.getCsTvNo() == null) {
						csWithTds.setCsTvNo(tvNo);
					}
					map.put("vhNo", 1);
					cashStmtStatus.getCashStmtList().add(cashStmt);
					cashStmtStatus.getCashStmtList().add(csWithTds);
					session.update(cashStmtStatus);
				}
			} else {
				map.put("vhNo", 0);
			}
			transaction.commit();
			session.flush();
		} catch (Exception e) {
			e.printStackTrace();
			map.put("vhNo", -1);
		}
		session.clear();
		session.close();
		return map;

	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<CashStmt> getAllCashStmt(int cssId) {
		System.out.println("enter into getAllCashStmt function");
		List<CashStmt> cashStmtList = new ArrayList<CashStmt>();
		List<CashStmt> actCSList = new ArrayList<CashStmt>();
		List<CashStmtStatus> cssList = new ArrayList<CashStmtStatus>();
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try {
			Criteria criteria = session.createCriteria(CashStmtStatus.class);
			criteria.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID, cssId));
			cssList = criteria.list();
			if (!cssList.isEmpty()) {
				Hibernate.initialize(cssList.get(0).getCashStmtList());
				cashStmtList = cssList.get(0).getCashStmtList();
			}

			for (int i = 0; i < cashStmtList.size(); i++) {
				if (cashStmtList.get(i).isCsIsVRev() == false) {
					actCSList.add(cashStmtList.get(i));
				}
			}

			transaction.commit();
			session.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return actCSList;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public int closeCSS(CashStmtStatus cashStmtStatus) {
		System.out.println("enter into closeCSS function");
		int temp = -1;
		User currentUser = (User)httpSession.getAttribute("currentUser");
		String bCode = cashStmtStatus.getbCode();
		String userCode = cashStmtStatus.getUserCode();
		double openBal = cashStmtStatus.getCssCloseBal();
		double bkOpenBal = cashStmtStatus.getCssBkCloseBal();
		int sheetNo = cashStmtStatus.getCssCsNo() + 1;
		Date dt = cashStmtStatus.getCssDt();
		Calendar c = Calendar.getInstance();
		c.setTime(dt);
		c.add(Calendar.DATE, 1);
		dt = new Date(c.getTime().getTime());
		c.setTime(dt);
		int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
		if (dayOfWeek == 1) {
			c.add(Calendar.DATE, 1);
			dt = new Date(c.getTime().getTime());
			System.out.println("sunday = " + dt);
		} else {
			System.out.println("Not sunday = " + dt);
		}
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try {
			session.merge(cashStmtStatus);
			
			List<CashStmtStatus> cssList = new ArrayList<>();
			Criteria criteria = session.createCriteria(CashStmtStatus.class);
			criteria.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID, cashStmtStatus.getCssId()));
			cssList = criteria.list();
			
			CashStmtStatus css = cssList.get(0);
			List<CashStmt> csList = css.getCashStmtList();
			
			if(!csList.isEmpty()){
				for(int i=0;i<csList.size();i++){
					List<CashStmt> csTempList = new ArrayList<>();
					criteria = session.createCriteria(CashStmt.class);
					criteria.add(Restrictions.eq(CashStmtCNTS.CS_ID,csList.get(i).getCsId()));
					csTempList = criteria.list();
					
					CashStmt cs = csTempList.get(0);
					cs.setCsIsClose(true);
					session.merge(cs);
				}
			}
			

			CashStmtStatus cashSS = new CashStmtStatus();
			cashSS.setbCode(bCode);
			cashSS.setUserCode(currentUser.getUserCode());
			cashSS.setCssOpenBal(openBal);
			cashSS.setCssBkOpenBal(bkOpenBal);
			cashSS.setCssDt(dt);
			cashSS.setCssCsNo(sheetNo);

			session.save(cashSS);
			
			LhpvStatus lhpvStatus = new LhpvStatus();
			lhpvStatus.setbCode(bCode);
			lhpvStatus.setUserCode(currentUser.getUserCode());
			lhpvStatus.setLsCrAmt(0);
			lhpvStatus.setLsDbAmt(0);
			lhpvStatus.setLsDt(dt);
			lhpvStatus.setLsNo(sheetNo);
			
			session.save(lhpvStatus);
			
			Branch branch = (Branch) session.get(Branch.class,Integer.parseInt(bCode));
			List<BankMstr> bankList = branch.getBankMstrList();
			
			if(!bankList.isEmpty()){
				for(int i=0;i<bankList.size();i++){
					BankMstr bankMstr = bankList.get(i);
					List<BankCS> bcsList = new ArrayList<>();
					criteria = session.createCriteria(BankCS.class);
					criteria.add(Restrictions.eq(BankCSCNTS.BCS_DT,cashStmtStatus.getCssDt()));
					criteria.add(Restrictions.eq(BankCSCNTS.BCS_BNK_ID,bankList.get(i).getBnkId()));
					bcsList = criteria.list();
					
					if(!bcsList.isEmpty()){
						BankCS bankCS = bcsList.get(0);
						bankCS.setBcsCloseBal(bankMstr.getBnkBalanceAmt());
						
						session.update(bankCS);
						
						BankCS newBankCS = new BankCS();
						newBankCS.setbCode(bCode);
						newBankCS.setUserCode(currentUser.getUserCode());
						newBankCS.setBcsBankCode(bankMstr.getBnkFaCode());
						newBankCS.setBcsBankId(bankMstr.getBnkId());
						newBankCS.setBcsBrhId(Integer.parseInt(bCode));
						newBankCS.setBcsDt(dt);
						newBankCS.setBcsOpenBal(bankMstr.getBnkBalanceAmt());
						
						session.save(newBankCS);
					}else{
						double amt = bankMstr.getBnkBalanceAmt();
						if(!csList.isEmpty()){
							for(int j=0;j<csList.size();j++){
								if(csList.get(j).getCsFaCode().equalsIgnoreCase(bankMstr.getBnkFaCode())){
									if(csList.get(j).getCsDrCr() == 'C'){
										amt = amt + csList.get(j).getCsAmt(); 
									}else{
										amt = amt - csList.get(j).getCsAmt();
									}
								}
							}
						}
						
						BankCS newBankCS1 = new BankCS();
						newBankCS1.setbCode(bCode);
						newBankCS1.setUserCode(currentUser.getUserCode());
						newBankCS1.setBcsBankCode(bankMstr.getBnkFaCode());
						newBankCS1.setBcsBankId(bankMstr.getBnkId());
						newBankCS1.setBcsBrhId(Integer.parseInt(bCode));
						newBankCS1.setBcsDt(cashStmtStatus.getCssDt());
						newBankCS1.setBcsOpenBal(amt);
						newBankCS1.setBcsCloseBal(bankMstr.getBnkBalanceAmt());
						
						session.save(newBankCS1);
						
						BankCS newBankCS = new BankCS();
						newBankCS.setbCode(bCode);
						newBankCS.setUserCode(currentUser.getUserCode());
						newBankCS.setBcsBankCode(bankMstr.getBnkFaCode());
						newBankCS.setBcsBankId(bankMstr.getBnkId());
						newBankCS.setBcsBrhId(Integer.parseInt(bCode));
						newBankCS.setBcsDt(dt);
						newBankCS.setBcsOpenBal(bankMstr.getBnkBalanceAmt());
						
						session.save(newBankCS);
						
					}
				}
			}

			session.flush();
			session.clear();
			transaction.commit();
			
			temp = 1;
		} catch (Exception e) {
				transaction.rollback();
			e.printStackTrace();
		}
		
		session.close();
		return temp;
	}


	
	@Override
	@SuppressWarnings("unchecked")
	public int closeCSS(CashStmtStatus cashStmtStatus,Session session) {
		System.out.println("enter into closeCSS function");
		int temp = -1;
		User currentUser = (User)httpSession.getAttribute("currentUser");
		String bCode = cashStmtStatus.getbCode();
		double openBal = cashStmtStatus.getCssCloseBal();
		double bkOpenBal = cashStmtStatus.getCssBkCloseBal();
		int sheetNo = cashStmtStatus.getCssCsNo() + 1;
		Date dt = cashStmtStatus.getCssDt();
		Calendar c = Calendar.getInstance();
		c.setTime(dt);
		c.add(Calendar.DATE, 1);
		dt = new Date(c.getTime().getTime());
		c.setTime(dt);
		int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
		if (dayOfWeek == 1) {
			c.add(Calendar.DATE, 1);
			dt = new Date(c.getTime().getTime());
			System.out.println("sunday = " + dt);
		} else {
			System.out.println("Not sunday = " + dt);
		}

			session.merge(cashStmtStatus);
			
			Criteria criteria = session.createCriteria(CashStmtStatus.class);
			criteria.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID, cashStmtStatus.getCssId()));
			List<CashStmtStatus> cssList = criteria.list();
			
			CashStmtStatus css = cssList.get(0);
			List<CashStmt> csList = css.getCashStmtList();
			
			if(! csList.isEmpty()){
				for(int i=0; i < csList.size(); i++) {
					criteria = session.createCriteria(CashStmt.class);
					criteria.add(Restrictions.eq(CashStmtCNTS.CS_ID, csList.get(i).getCsId()));
					List<CashStmt> csTempList = criteria.list();
					
					CashStmt cs = csTempList.get(0);
					cs.setCsIsClose(true);
					session.merge(cs);
				}
			}
			
			CashStmtStatus cashSS = new CashStmtStatus();
			cashSS.setbCode(bCode);
			cashSS.setUserCode(currentUser.getUserCode());
			cashSS.setCssOpenBal(openBal);
			cashSS.setCssBkOpenBal(bkOpenBal);
			cashSS.setCssDt(dt);
			cashSS.setCssCsNo(sheetNo);

			session.save(cashSS);
			
			LhpvStatus lhpvStatus = new LhpvStatus();
			lhpvStatus.setbCode(bCode);
			lhpvStatus.setUserCode(currentUser.getUserCode());
			lhpvStatus.setLsCrAmt(0);
			lhpvStatus.setLsDbAmt(0);
			lhpvStatus.setLsDt(dt);
			lhpvStatus.setLsNo(sheetNo);
			
			session.save(lhpvStatus);
			
			Branch branch = (Branch) session.get(Branch.class,Integer.parseInt(bCode));
			List<BankMstr> bankList = branch.getBankMstrList();
			
			if(!bankList.isEmpty()){
				for(int i=0;i<bankList.size();i++){
					BankMstr bankMstr = bankList.get(i);
					criteria = session.createCriteria(BankCS.class);
					criteria.add(Restrictions.eq(BankCSCNTS.BCS_DT,cashStmtStatus.getCssDt()));
					criteria.add(Restrictions.eq(BankCSCNTS.BCS_BNK_ID,bankList.get(i).getBnkId()));
					List<BankCS>  bcsList = criteria.list();
					
					if(!bcsList.isEmpty()){
						BankCS bankCS = bcsList.get(0);
						bankCS.setBcsCloseBal(bankMstr.getBnkBalanceAmt());
						
						session.update(bankCS);
						
						BankCS newBankCS = new BankCS();
						newBankCS.setbCode(bCode);
						newBankCS.setUserCode(currentUser.getUserCode());
						newBankCS.setBcsBankCode(bankMstr.getBnkFaCode());
						newBankCS.setBcsBankId(bankMstr.getBnkId());
						newBankCS.setBcsBrhId(Integer.parseInt(bCode));
						newBankCS.setBcsDt(dt);
						newBankCS.setBcsOpenBal(bankMstr.getBnkBalanceAmt());
						
						session.save(newBankCS);
					}else{
						double amt = bankMstr.getBnkBalanceAmt();
						if(!csList.isEmpty()){
							for(int j=0;j<csList.size();j++){
								if(csList.get(j).getCsFaCode().equalsIgnoreCase(bankMstr.getBnkFaCode())){
									if(csList.get(j).getCsDrCr() == 'C'){
										amt = amt + csList.get(j).getCsAmt(); 
									}else{
										amt = amt - csList.get(j).getCsAmt();
									}
								}
							}
						}
						
						BankCS newBankCS1 = new BankCS();
						newBankCS1.setbCode(bCode);
						newBankCS1.setUserCode(currentUser.getUserCode());
						newBankCS1.setBcsBankCode(bankMstr.getBnkFaCode());
						newBankCS1.setBcsBankId(bankMstr.getBnkId());
						newBankCS1.setBcsBrhId(Integer.parseInt(bCode));
						newBankCS1.setBcsDt(cashStmtStatus.getCssDt());
						newBankCS1.setBcsOpenBal(amt);
						newBankCS1.setBcsCloseBal(bankMstr.getBnkBalanceAmt());
						
						session.save(newBankCS1);
						
						BankCS newBankCS = new BankCS();
						newBankCS.setbCode(bCode);
						newBankCS.setUserCode(currentUser.getUserCode());
						newBankCS.setBcsBankCode(bankMstr.getBnkFaCode());
						newBankCS.setBcsBankId(bankMstr.getBnkId());
						newBankCS.setBcsBrhId(Integer.parseInt(bCode));
						newBankCS.setBcsDt(dt);
						newBankCS.setBcsOpenBal(bankMstr.getBnkBalanceAmt());
						
						session.save(newBankCS);
						
					}
				}
			}

			
			temp = 1;
		return temp;
	}

	
	
	
	
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<CashStmtStatus> getCSSById(int id) {
		System.out.println("enter into getCSSById function");
		List<CashStmtStatus> cssList = new ArrayList<CashStmtStatus>();
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try {
			Criteria criteria = session.createCriteria(CashStmtStatus.class);
			criteria.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID, id));
			cssList = criteria.list();
			Hibernate.initialize(cssList.get(0).getCashStmtList());
			transaction.commit();
			session.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return cssList;
	}


	@Override
	@SuppressWarnings("unchecked")
	public List<CashStmtStatus> getCSSById(int id,Session session) {
		System.out.println("enter into getCSSById function");

		List<CashStmtStatus> cssList = new ArrayList<CashStmtStatus>();
		
		Criteria criteria = session.createCriteria(CashStmtStatus.class);
			criteria.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID, id));
			cssList = criteria.list();
			Hibernate.initialize(cssList.get(0).getCashStmtList());
		
		return cssList;
	}

	
	
	
	@SuppressWarnings("unchecked")
	@Transactional
	public int saveCashPayment(VoucherService voucherService) {
		System.out.println("enter into saveCashPayment function");
		User currentUser = (User) httpSession.getAttribute("currentUser");
		Map<String, Object> map = new HashMap<String, Object>();
		CashStmtStatus cashStmtStatus1 = voucherService.getCashStmtStatus();
		String brFaCode = voucherService.getBranch().getBranchFaCode();
		List<Map<String, Object>> subFList = voucherService.getSubFList();
			
		String tvNo = voucherService.getFaCode();
		String payMode = "C";
		
		map.put("tvNo", tvNo);
		int voucherNo = 0;
		if (!subFList.isEmpty()) {
			Session session = this.sessionFactory.openSession();
			Transaction transaction = session.beginTransaction();
			try {
				List<CashStmtStatus> cssList = new ArrayList<>();
				java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus1
						.getCssDt().getTime());

				Criteria cr = session.createCriteria(CashStmt.class);
				cr.add(Restrictions.eq(CashStmtCNTS.CS_CDT, sqlDate));
				cr.add(Restrictions.eq(CashStmtCNTS.USER_BRANCH_CODE,
						currentUser.getUserBranchCode()));
				ProjectionList proList = Projections.projectionList();
				proList.add(Projections.property("csVouchNo"));
				cr.setProjection(proList);

				List<Integer> voucherNoList = cr.list();

				CashStmtStatus clientCSS = (CashStmtStatus) session.get(CashStmtStatus.class,voucherService.getCashStmtStatus().getCssId());
				
				List<CashStmt> csList = clientCSS.getCashStmtList();
				if(!csList.isEmpty()){
					for(int i=0;i<csList.size();i++){
						if(voucherNo < csList.get(i).getCsVouchNo()){
							voucherNo = csList.get(i).getCsVouchNo();
						}
					}
					voucherNo = voucherNo + 1;
					
				}else{
					voucherNo = 1;
				}

				for (int i = 0; i < subFList.size(); i++) {

					Map<String, Object> subMap = subFList.get(i);
					String faCode = (String) subMap.get("faCode");
					System.out.println("**************faCode = " + faCode);
					double amt = Double.parseDouble(String.valueOf(subMap.get("amt")));
					String desBrFaCode = "";
					desBrFaCode = (String) subMap.get("desBrFaCode");
					String tdsCode = (String) subMap.get("tdsCode");
					double tdsAmt = 0;
					if (subMap.get("tdsAmt") != null) {
						if (String.valueOf(subMap.get("tdsAmt")) != null) {
							tdsAmt = Double.parseDouble(String.valueOf(subMap
									.get("tdsAmt")));
						}
					}
					System.out.println("brFaCode ======> " + brFaCode);
					System.out.println("desBrFaCode ======> " + desBrFaCode);
					if (brFaCode.equalsIgnoreCase(desBrFaCode)) {
						if (tdsCode != null && tdsAmt > 0) {

							CashStmtStatus cashStmtStatus = voucherService
									.getCashStmtStatus();

							CashStmt cashStmt = new CashStmt();
							cashStmt.setbCode(currentUser.getUserBranchCode());
							cashStmt.setUserCode(currentUser.getUserCode());
							cashStmt.setCsDescription(voucherService.getDesc());
							cashStmt.setCsDrCr('D');
							cashStmt.setCsAmt(amt);
							cashStmt.setCsType(voucherService.getVoucherType());
							cashStmt.setCsTvNo(faCode);
							cashStmt.setCsVouchType("cash");
							cashStmt.setCsFaCode(faCode);
							cashStmt.setCsVouchNo(voucherNo);
							cashStmt.setCsPayTo(voucherService.getPayTo());
							cashStmt.setPayMode(payMode);

							sqlDate = new java.sql.Date(cashStmtStatus
									.getCssDt().getTime());
							cashStmt.setCsDt(sqlDate);

							cr = session.createCriteria(CashStmtStatus.class);
							cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,
									cashStmtStatus.getCssId()));
							cssList = cr.list();
							if (!cssList.isEmpty()) {
								CashStmtStatus csStatus = cssList.get(0);
								cashStmt.setCsNo(csStatus);
								csStatus.getCashStmtList().add(cashStmt);
								session.update(csStatus);
							}

							CashStmt csWithTds = new CashStmt();
							csWithTds.setbCode(currentUser.getUserBranchCode());
							csWithTds.setUserCode(currentUser.getUserCode());
							csWithTds
									.setCsDescription(voucherService.getDesc());
							csWithTds.setCsDrCr('C');
							csWithTds
									.setCsType(voucherService.getVoucherType());
							csWithTds.setCsTvNo(tdsCode);
							csWithTds.setCsVouchType("cash");
							csWithTds.setCsAmt(tdsAmt);
							csWithTds.setCsFaCode(tdsCode);
							csWithTds.setCsDt(sqlDate);
							csWithTds.setCsVouchNo(voucherNo);
							csWithTds.setCsPayTo(voucherService.getPayTo());
							csWithTds.setPayMode(payMode);

							cr = session.createCriteria(CashStmtStatus.class);
							cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,
									cashStmtStatus.getCssId()));
							cssList = cr.list();
							if (!cssList.isEmpty()) {
								CashStmtStatus csStatus = cssList.get(0);
								csWithTds.setCsNo(csStatus);
								csStatus.getCashStmtList().add(csWithTds);
								session.update(csStatus);
							}

						} else {

							CashStmtStatus cashStmtStatus = voucherService
									.getCashStmtStatus();

							CashStmt cashStmt = new CashStmt();
							cashStmt.setbCode(currentUser.getUserBranchCode());
							cashStmt.setUserCode(currentUser.getUserCode());
							cashStmt.setCsDescription(voucherService.getDesc());
							cashStmt.setCsDrCr('D');
							cashStmt.setCsAmt(amt);
							cashStmt.setCsType(voucherService.getVoucherType());
							cashStmt.setCsTvNo(faCode);
							cashStmt.setCsVouchType("cash");
							System.out.println("**************^^^^^^^faCode = "
									+ faCode);
							cashStmt.setCsFaCode(faCode);
							cashStmt.setCsVouchNo(voucherNo);
							cashStmt.setCsPayTo(voucherService.getPayTo());
							cashStmt.setPayMode(payMode);
							
							sqlDate = new java.sql.Date(cashStmtStatus
									.getCssDt().getTime());
							cashStmt.setCsDt(sqlDate);

							cr = session.createCriteria(CashStmtStatus.class);
							cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,
									cashStmtStatus.getCssId()));
							cssList = cr.list();
							if (!cssList.isEmpty()) {
								CashStmtStatus csStatus = cssList.get(0);
								cashStmt.setCsNo(csStatus);
								csStatus.getCashStmtList().add(cashStmt);
								session.update(csStatus);
							}
						}
					} else {
						System.out
								.println("********Inter Branch Case************");
						if (tdsCode != null && tdsAmt > 0) {

							CashStmtStatus cashStmtStatus = voucherService
									.getCashStmtStatus();

							CashStmt cashStmt = new CashStmt();
							cashStmt.setbCode(currentUser.getUserBranchCode());
							cashStmt.setUserCode(currentUser.getUserCode());
							cashStmt.setCsDescription(voucherService.getDesc());
							cashStmt.setCsDrCr('D');
							cashStmt.setCsAmt(amt - tdsAmt);
							cashStmt.setCsType(voucherService.getVoucherType());
							cashStmt.setCsTvNo(desBrFaCode);
							cashStmt.setCsVouchType("cash");
							cashStmt.setCsFaCode(desBrFaCode);
							cashStmt.setCsVouchNo(voucherNo);
							cashStmt.setCsPayTo(voucherService.getPayTo());
							cashStmt.setPayMode(payMode);
							
							sqlDate = new java.sql.Date(cashStmtStatus
									.getCssDt().getTime());
							cashStmt.setCsDt(sqlDate);

							cr = session.createCriteria(CashStmtStatus.class);
							cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,
									cashStmtStatus.getCssId()));
							cssList = cr.list();
							if (!cssList.isEmpty()) {
								CashStmtStatus csStatus = cssList.get(0);
								cashStmt.setCsNo(csStatus);
								csStatus.getCashStmtList().add(cashStmt);
								session.update(csStatus);
							}

							CashStmtTemp csWithTds = new CashStmtTemp();
							csWithTds.setbCode(currentUser.getUserBranchCode());
							csWithTds.setUserCode(currentUser.getUserCode());
							csWithTds.setCsDescription(voucherService.getDesc());
							csWithTds.setCsDrCr('C');
							csWithTds.setCsType(voucherService.getVoucherType());
							csWithTds.setCsTvNo(tdsCode);
							csWithTds.setCsVouchType("contra");
							csWithTds.setCsAmt(tdsAmt);
							csWithTds.setCsFaCode(tdsCode);
							csWithTds.setCsDt(sqlDate);
							csWithTds.setCsPayTo(voucherService.getPayTo());
							csWithTds.setCsBrhFaCode(desBrFaCode);
							csWithTds.setPayMode(payMode);

							session.save(csWithTds);

							CashStmtTemp cashStmt1 = new CashStmtTemp();
							cashStmt1.setbCode(currentUser.getUserBranchCode());
							cashStmt1.setUserCode(currentUser.getUserCode());
							cashStmt1
									.setCsDescription(voucherService.getDesc());
							cashStmt1.setCsDrCr('D');
							cashStmt1.setCsAmt(amt);
							cashStmt1
									.setCsType(voucherService.getVoucherType());
							cashStmt1.setCsTvNo(faCode);
							cashStmt1.setCsVouchType("contra");
							cashStmt1.setCsFaCode(faCode);
							cashStmt1.setCsPayTo(voucherService.getPayTo());
							cashStmt1.setCsDt(sqlDate);
							cashStmt1.setCsBrhFaCode(desBrFaCode);
							cashStmt1.setPayMode(payMode);
							
							session.save(cashStmt1);

							CashStmtTemp cashStmt2 = new CashStmtTemp();
							cashStmt2.setbCode(currentUser.getUserBranchCode());
							cashStmt2.setUserCode(currentUser.getUserCode());
							cashStmt2
									.setCsDescription(voucherService.getDesc());
							cashStmt2.setCsDrCr('C');
							cashStmt2.setCsAmt(amt - tdsAmt);
							cashStmt2
									.setCsType(voucherService.getVoucherType());
							cashStmt2.setCsTvNo(brFaCode);
							cashStmt2.setCsVouchType("contra");
							cashStmt2.setCsFaCode(brFaCode);
							cashStmt2.setCsPayTo(voucherService.getPayTo());
							cashStmt2.setCsDt(sqlDate);
							cashStmt2.setCsBrhFaCode(desBrFaCode);
							cashStmt2.setPayMode(payMode);
							
							session.save(cashStmt2);

						} else {

							CashStmtStatus cashStmtStatus = voucherService
									.getCashStmtStatus();

							CashStmt cashStmt = new CashStmt();
							cashStmt.setbCode(currentUser.getUserBranchCode());
							cashStmt.setUserCode(currentUser.getUserCode());
							cashStmt.setCsDescription(voucherService.getDesc());
							cashStmt.setCsDrCr('D');
							cashStmt.setCsAmt(amt);
							cashStmt.setCsType(voucherService.getVoucherType());
							cashStmt.setCsTvNo(desBrFaCode);
							cashStmt.setCsVouchType("cash");
							cashStmt.setCsFaCode(desBrFaCode);
							cashStmt.setCsVouchNo(voucherNo);
							cashStmt.setCsPayTo(voucherService.getPayTo());
							cashStmt.setPayMode(payMode);
							
							sqlDate = new java.sql.Date(cashStmtStatus
									.getCssDt().getTime());
							cashStmt.setCsDt(sqlDate);

							cr = session.createCriteria(CashStmtStatus.class);
							cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,
									cashStmtStatus.getCssId()));
							cssList = cr.list();
							if (!cssList.isEmpty()) {
								CashStmtStatus csStatus = cssList.get(0);
								cashStmt.setCsNo(csStatus);
								csStatus.getCashStmtList().add(cashStmt);
								session.update(csStatus);
							}

							CashStmtTemp cashStmt1 = new CashStmtTemp();
							cashStmt1.setbCode(currentUser.getUserBranchCode());
							cashStmt1.setUserCode(currentUser.getUserCode());
							cashStmt1
									.setCsDescription(voucherService.getDesc());
							cashStmt1.setCsDrCr('D');
							cashStmt1.setCsAmt(amt);
							cashStmt1
									.setCsType(voucherService.getVoucherType());
							cashStmt1.setCsTvNo(faCode);
							cashStmt1.setCsVouchType("contra");
							cashStmt1.setCsFaCode(faCode);
							cashStmt1.setCsPayTo(voucherService.getPayTo());
							cashStmt1.setCsDt(sqlDate);
							cashStmt1.setCsBrhFaCode(desBrFaCode);
							cashStmt1.setPayMode(payMode);
							
							session.save(cashStmt1);

							CashStmtTemp cashStmt2 = new CashStmtTemp();
							cashStmt2.setbCode(currentUser.getUserBranchCode());
							cashStmt2.setUserCode(currentUser.getUserCode());
							cashStmt2
									.setCsDescription(voucherService.getDesc());
							cashStmt2.setCsDrCr('C');
							cashStmt2.setCsAmt(amt);
							cashStmt2
									.setCsType(voucherService.getVoucherType());
							cashStmt2.setCsTvNo(brFaCode);
							cashStmt2.setCsVouchType("contra");
							cashStmt2.setCsFaCode(brFaCode);
							cashStmt2.setCsPayTo(voucherService.getPayTo());
							cashStmt2.setCsDt(sqlDate);
							cashStmt2.setCsBrhFaCode(desBrFaCode);
							cashStmt2.setPayMode(payMode);
							
							session.save(cashStmt2);
						}
					}

				}

				transaction.commit();
				session.flush();
			} catch (Exception e) {
				e.printStackTrace();
			}
			session.clear();
			session.close();
		} else {
			voucherNo = 0;
		}
		
		return voucherNo;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public int closePenCs(List<Integer> penCsIdList, int cssId) {
		System.out.println("enter into closePenCs function");
		int res = 0;
		User currentUser = (User) httpSession.getAttribute("currentUser");
		List<CashStmtStatus> cssList = new ArrayList<>();
		int voucherNo = 0;
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try {
			Criteria cr = session.createCriteria(CashStmtStatus.class);
			cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID, cssId));
			cssList = cr.list();

			java.sql.Date sqlDate = new java.sql.Date(cssList.get(0).getCssDt()
					.getTime());

			List<CashStmt> csVouchList = new ArrayList<CashStmt>();
			csVouchList = cssList.get(0).getCashStmtList();
			
			if(!csVouchList.isEmpty()){
				for(int i=0;i<csVouchList.size();i++){
					if(voucherNo < csVouchList.get(i).getCsVouchNo()){
						voucherNo = csVouchList.get(i).getCsVouchNo();
					}
				}
				voucherNo = voucherNo + 1;
			}else{
				voucherNo = 1;
			}
			
			
			if (!cssList.isEmpty()) {
				CashStmtStatus cashStmtStatus = cssList.get(0);
				for (int i = 0; i < penCsIdList.size(); i++) {
					List<CashStmtTemp> cstList = new ArrayList<>();
					cr = session.createCriteria(CashStmtTemp.class);
					cr.add(Restrictions.eq(CashStmtTempCNTS.CS_ID,
							penCsIdList.get(i)));
					cstList = cr.list();

					if (!cstList.isEmpty()) {

						CashStmtTemp cashStmtTemp = cstList.get(0);
						cashStmtTemp.setCsIsView(true);
						session.merge(cashStmtTemp);

						CashStmt cashStmt = new CashStmt();
						cashStmt.setbCode(currentUser.getUserBranchCode());
						cashStmt.setCsAmt(cashStmtTemp.getCsAmt());
						cashStmt.setCsChequeType(cashStmtTemp.getCsChequeType());
						cashStmt.setCsDescription(cashStmtTemp
								.getCsDescription());
						cashStmt.setCsDrCr(cashStmtTemp.getCsDrCr());
						cashStmt.setCsDt(sqlDate);
						cashStmt.setCsFaCode(cashStmtTemp.getCsFaCode());
						cashStmt.setCsIsClose(true);
						cashStmt.setCsIsVRev(false);
						cashStmt.setCsLhpvTempNo(cashStmtTemp.getCsLhpvTempNo());
						cashStmt.setCsMrNo(cashStmtTemp.getCsMrNo());
						cashStmt.setCsPayTo(cashStmtTemp.getCsPayTo());
						cashStmt.setCsSFId(cashStmtTemp.getCsSFId());
						cashStmt.setCsTravIdList(cashStmtTemp.getCsTravIdList());
						cashStmt.setCsTvNo(cashStmtTemp.getCsTvNo());
						cashStmt.setCsType(cashStmtTemp.getCsType());
						cashStmt.setUserCode(currentUser.getUserCode());
						cashStmt.setCsVouchNo(voucherNo);
						cashStmt.setCsVouchType(cashStmtTemp.getCsVouchType());

						List<CashStmtStatus> newCssList = new ArrayList<>();
						cr = session.createCriteria(CashStmtStatus.class);
						cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,
								cashStmtStatus.getCssId()));
						newCssList = cr.list();
						if (!newCssList.isEmpty()) {
							CashStmtStatus csStatus = newCssList.get(0);
							cashStmt.setCsNo(csStatus);
							csStatus.getCashStmtList().add(cashStmt);
							session.update(csStatus);
						}

					}
				}
			}
			transaction.commit();
			session.flush();
			res = 1;
		} catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return res;
	}

	
	@Override
	@SuppressWarnings("unchecked")
	public int closePenCs(List<Integer> penCsIdList, int cssId,Session session) {
		System.out.println("enter into closePenCs function");
		int res = 0;
		User currentUser = (User) httpSession.getAttribute("currentUser");
		int voucherNo = 0;
			Criteria cr = session.createCriteria(CashStmtStatus.class);
			cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID, cssId));
			List<CashStmtStatus> cssList = cr.list();

			java.sql.Date sqlDate = new java.sql.Date(cssList.get(0).getCssDt().getTime());

			List<CashStmt> csVouchList = cssList.get(0).getCashStmtList();
			
			if(!csVouchList.isEmpty()){
				for(int i=0; i < csVouchList.size(); i++){
					if(voucherNo < csVouchList.get(i).getCsVouchNo()){
						voucherNo = csVouchList.get(i).getCsVouchNo();
					}
				}
				voucherNo = voucherNo + 1;
			} else {
				voucherNo = 1;
			}			
			
			if (!cssList.isEmpty()) {
				CashStmtStatus cashStmtStatus = cssList.get(0);
				for (int i = 0; i < penCsIdList.size(); i++) {
					cr = session.createCriteria(CashStmtTemp.class);
					cr.add(Restrictions.eq(CashStmtTempCNTS.CS_ID, penCsIdList.get(i)));
					List<CashStmtTemp> cstList = cr.list();

					if (!cstList.isEmpty()) {

						CashStmtTemp cashStmtTemp = cstList.get(0);
						cashStmtTemp.setCsIsView(true);
						session.merge(cashStmtTemp);

						CashStmt cashStmt = new CashStmt();
						cashStmt.setbCode(currentUser.getUserBranchCode());
						cashStmt.setCsAmt(cashStmtTemp.getCsAmt());
						cashStmt.setCsChequeType(cashStmtTemp.getCsChequeType());
						cashStmt.setCsDescription(cashStmtTemp.getCsDescription());
						cashStmt.setCsDrCr(cashStmtTemp.getCsDrCr());
						cashStmt.setCsDt(sqlDate);
						cashStmt.setCsFaCode(cashStmtTemp.getCsFaCode());
						cashStmt.setCsIsClose(true);
						cashStmt.setCsIsVRev(false);
						cashStmt.setCsLhpvTempNo(cashStmtTemp.getCsLhpvTempNo());
						cashStmt.setCsMrNo(cashStmtTemp.getCsMrNo());
						cashStmt.setCsPayTo(cashStmtTemp.getCsPayTo());
						cashStmt.setCsSFId(cashStmtTemp.getCsSFId());
						cashStmt.setCsTravIdList(cashStmtTemp.getCsTravIdList());
						cashStmt.setCsTvNo(cashStmtTemp.getCsTvNo());
						cashStmt.setCsType(cashStmtTemp.getCsType());
						cashStmt.setUserCode(currentUser.getUserCode());
						cashStmt.setCsVouchNo(voucherNo);
						cashStmt.setCsVouchType(cashStmtTemp.getCsVouchType());

						cr = session.createCriteria(CashStmtStatus.class);
						cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID, cashStmtStatus.getCssId()));
						List<CashStmtStatus> newCssList = cr.list();
						if (!newCssList.isEmpty()) {
							CashStmtStatus csStatus = newCssList.get(0);
							cashStmt.setCsNo(csStatus);
							csStatus.getCashStmtList().add(cashStmt);
							session.update(csStatus);
						}

					}
				}
			}
		return res;
	}

	
	
	
	@SuppressWarnings("unchecked")
	@Transactional
	public int saveRentVoucher(VoucherService voucherService){
		System.out.println("enter into saveRentVoucher function");
		User currentUser = (User) httpSession.getAttribute("currentUser");
		Map<String, Object> map = new HashMap<String, Object>();
		CashStmtStatus cashStmtStatus1 = voucherService.getCashStmtStatus();
		String brFaCode = voucherService.getBranch().getBranchFaCode();
		System.out.println("brFaCode ==> "+brFaCode);
		List<Map<String, Object>> subFList = voucherService.getSubFList();
		
		Date date = new Date(new java.util.Date().getTime());
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		System.out.println("unique id =====>" + calendar.getTimeInMillis());
		long milliSec = calendar.getTimeInMillis();
		String tvNo = String.valueOf(milliSec);
		String payMode = "";
		map.put("tvNo", tvNo);
		int voucherNo = 0;
		if (!subFList.isEmpty()) {
			Session session = this.sessionFactory.openSession();
			Transaction transaction = session.beginTransaction();
			try {
				List<CashStmtStatus> cssList = new ArrayList<>();
				java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus1.getCssDt().getTime());

				Criteria cr = session.createCriteria(CashStmt.class);
				cr.add(Restrictions.eq(CashStmtCNTS.CS_CDT, sqlDate));
				cr.add(Restrictions.eq(CashStmtCNTS.USER_BRANCH_CODE,
						currentUser.getUserBranchCode()));
				ProjectionList proList = Projections.projectionList();
				proList.add(Projections.property("csVouchNo"));
				cr.setProjection(proList);

				List<Integer> voucherNoList = new ArrayList<Integer>();
				voucherNoList = cr.list();

				if (!voucherNoList.isEmpty()) {
					for(int i=0;i<voucherNoList.size();i++){
						if(voucherNo < voucherNoList.get(i)){
							voucherNo = voucherNoList.get(i);
						}
					}
					voucherNo = voucherNo + 1;
				} else {
					voucherNo = 1;
				}
				
				List<FAMaster> faMList = new ArrayList<>();
				cr=session.createCriteria(FAMaster.class);
				cr.add(Restrictions.eq(FAMasterCNTS.FA_MFA_NAME,ConstantsValues.RENT_FANAME));
				faMList =cr.list();
				
				String rentCode = "";
				if(!faMList.isEmpty()){
					rentCode = faMList.get(0).getFaMfaCode();
				}
				
				char payBy = voucherService.getPayBy();
				System.out.println("value of payBy = "+payBy);
				
				if(payBy == 'C'){
					payMode = "C";
					for(int i=0;i<subFList.size();i++){
						int rmId = (int) subFList.get(i).get(ConstantsValues.RM_ID);
						double rentAmt = Double.parseDouble(String.valueOf(subFList.get(i).get("amt")));
						String destBrFaCode = "";
						destBrFaCode = (String) subFList.get(i).get(ConstantsValues.BRH_FA_CODE);;
						String empFaCode = "";
						empFaCode = (String) subFList.get(i).get(ConstantsValues.EMP_FA_CODE);
						String tdsCode = "";
						tdsCode = (String) subFList.get(i).get("tdsCode");
						double tdsAmt = 0.0;
						tdsAmt = Double.parseDouble(String.valueOf(subFList.get(i).get("tdsAmt")));
						
						if(brFaCode.equalsIgnoreCase(destBrFaCode)){
							System.out.println("*********Same Branch Case*********");
							
							CashStmtStatus cashStmtStatus = voucherService.getCashStmtStatus();
							CashStmt cashStmt = new CashStmt();
							cashStmt.setbCode(currentUser.getUserBranchCode());
							cashStmt.setUserCode(currentUser.getUserCode());
							cashStmt.setCsDescription(voucherService.getDesc());
							cashStmt.setCsDrCr('D');
							cashStmt.setCsAmt(rentAmt);
							cashStmt.setCsType(voucherService.getVoucherType());
							cashStmt.setCsVouchType("cash");
							cashStmt.setCsFaCode(rentCode);
							cashStmt.setCsTvNo(tvNo);
							cashStmt.setCsVouchNo(voucherNo);
							cashStmt.setCsSFId(rmId);							
							cashStmt.setCsDt(sqlDate);
							cashStmt.setPayMode(payMode);
							
							cr = session.createCriteria(CashStmtStatus.class);
							cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
							cssList = cr.list();
							if (!cssList.isEmpty()) {
								CashStmtStatus csStatus = cssList.get(0);
								cashStmt.setCsNo(csStatus);
								csStatus.getCashStmtList().add(cashStmt);
								session.update(csStatus);
							}
							
							if(tdsCode != null && tdsCode != "" && tdsAmt > 0){
								
								CashStmt cashStmt1 = new CashStmt();
								cashStmt1.setbCode(currentUser.getUserBranchCode());
								cashStmt1.setUserCode(currentUser.getUserCode());
								cashStmt1.setCsDescription(voucherService.getDesc());
								cashStmt1.setCsDrCr('C');
								cashStmt1.setCsAmt(tdsAmt);
								cashStmt1.setCsType(voucherService.getVoucherType());
								cashStmt1.setCsVouchType("cash");
								cashStmt1.setCsFaCode(tdsCode);
								cashStmt1.setCsTvNo(tvNo);
								cashStmt1.setCsVouchNo(voucherNo);
								cashStmt1.setCsSFId(rmId);
								cashStmt1.setCsDt(sqlDate);
								cashStmt1.setPayMode(payMode);
								
								cr = session.createCriteria(CashStmtStatus.class);
								cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
								cssList = cr.list();
								if (!cssList.isEmpty()) {
									CashStmtStatus csStatus = cssList.get(0);
									cashStmt1.setCsNo(csStatus);
									csStatus.getCashStmtList().add(cashStmt1);
									session.update(csStatus);
								}
							}
						}else{
							System.out.println("*********Inter Branch Case********");
							
							CashStmtStatus cashStmtStatus = voucherService.getCashStmtStatus();
							CashStmt cashStmt = new CashStmt();
							cashStmt.setbCode(currentUser.getUserBranchCode());
							cashStmt.setUserCode(currentUser.getUserCode());
							cashStmt.setCsDescription(voucherService.getDesc());
							cashStmt.setCsDrCr('D');
							cashStmt.setCsAmt(rentAmt - tdsAmt);
							cashStmt.setCsType(voucherService.getVoucherType());
							cashStmt.setCsVouchType("cash");
							cashStmt.setCsFaCode(destBrFaCode);
							cashStmt.setCsTvNo(tvNo);
							cashStmt.setCsVouchNo(voucherNo);
							cashStmt.setCsSFId(rmId);
							cashStmt.setCsDt(sqlDate);
							cashStmt.setPayMode(payMode);
							
							cr = session.createCriteria(CashStmtStatus.class);
							cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
							cssList = cr.list();
							if (!cssList.isEmpty()) {
								CashStmtStatus csStatus = cssList.get(0);
								cashStmt.setCsNo(csStatus);
								csStatus.getCashStmtList().add(cashStmt);
								session.update(csStatus);
							}
							
							
							
							CashStmtTemp cashStmt1 = new CashStmtTemp();
							cashStmt1.setbCode(currentUser.getUserBranchCode());
							cashStmt1.setUserCode(currentUser.getUserCode());
							cashStmt1.setCsDescription(voucherService.getDesc());
							cashStmt1.setCsDrCr('C');
							cashStmt1.setCsType(voucherService.getVoucherType());
							cashStmt1.setCsTvNo(tvNo);
							cashStmt1.setCsVouchType("contra");
							cashStmt1.setCsAmt(rentAmt - tdsAmt);
							cashStmt1.setCsFaCode(brFaCode);
							cashStmt1.setCsDt(sqlDate);
							cashStmt1.setCsPayTo(voucherService.getPayTo());
							cashStmt1.setCsBrhFaCode(destBrFaCode);
							cashStmt1.setPayMode(payMode);

							session.save(cashStmt1);
							
							CashStmtTemp cashStmt2 = new CashStmtTemp();
							cashStmt2.setbCode(currentUser.getUserBranchCode());
							cashStmt2.setUserCode(currentUser.getUserCode());
							cashStmt2.setCsDescription(voucherService.getDesc());
							cashStmt2.setCsDrCr('D');
							cashStmt2.setCsType(voucherService.getVoucherType());
							cashStmt2.setCsTvNo(tvNo);
							cashStmt2.setCsVouchType("contra");
							cashStmt2.setCsAmt(rentAmt);
							cashStmt2.setCsFaCode(rentCode);
							cashStmt2.setCsDt(sqlDate);
							cashStmt2.setCsPayTo(voucherService.getPayTo());
							cashStmt2.setCsBrhFaCode(destBrFaCode);
							cashStmt2.setPayMode(payMode);

							session.save(cashStmt2);
							
							if(tdsCode != null && tdsCode != "" && tdsAmt > 0){
								CashStmtTemp cashStmt3 = new CashStmtTemp();
								cashStmt3.setbCode(currentUser.getUserBranchCode());
								cashStmt3.setUserCode(currentUser.getUserCode());
								cashStmt3.setCsDescription(voucherService.getDesc());
								cashStmt3.setCsDrCr('C');
								cashStmt3.setCsType(voucherService.getVoucherType());
								cashStmt3.setCsTvNo(tvNo);
								cashStmt3.setCsVouchType("contra");
								cashStmt3.setCsAmt(tdsAmt);
								cashStmt3.setCsFaCode(tdsCode);
								cashStmt3.setCsDt(sqlDate);
								cashStmt3.setCsPayTo(voucherService.getPayTo());
								cashStmt3.setCsBrhFaCode(destBrFaCode);
								cashStmt3.setPayMode(payMode);

								session.save(cashStmt3);
							}
			
						}
					
					}
				}else if(payBy == 'Q'){
					payMode = "Q";
					CashStmtStatus cashStmtStatus = voucherService.getCashStmtStatus();
					List<BankMstr> bankMstrList = new ArrayList<>();
					char chequeType = voucherService.getChequeType();
					System.out.println("chequeType = "+chequeType);
					String bankCode = voucherService.getBankCode();
					cr=session.createCriteria(BankMstr.class);
					cr.add(Restrictions.eq(BankMstrCNTS.BNK_FA_CODE, bankCode));
					bankMstrList = cr.list();
					
					if(!bankMstrList.isEmpty()){
						BankMstr bankMstr = bankMstrList.get(0);
						double totRentAmt = 0.0;
						double totTdsAmt = 0.0;
						
						
						for(int i=0;i<subFList.size();i++){
							double rentAmt = 0.0;
							rentAmt = Double.parseDouble(String.valueOf(subFList.get(i).get("amt")));
							double tdsAmt = 0.0;
							tdsAmt = Double.parseDouble(String.valueOf(subFList.get(i).get("tdsAmt")));
						
							totRentAmt = totRentAmt + rentAmt;
							totTdsAmt  = totTdsAmt + tdsAmt;
						}
						
						List<ChequeLeaves> chqL = new ArrayList<>();
						ChequeLeaves chequeLeaves = voucherService.getChequeLeaves();
						cr=session.createCriteria(ChequeLeaves.class);
						cr.add(Restrictions.eq(ChequeLeavesCNTS.CHQ_L_ID,chequeLeaves.getChqLId()));
						chqL = cr.list();
						
						ChequeLeaves chq = chqL.get(0);
						chq.setChqLChqAmt(totRentAmt - totTdsAmt);
						chq.setChqLUsedDt(new Date(new java.util.Date().getTime()));
						chq.setChqLUsed(true);
						tvNo = chq.getChqLChqNo();
						
						session.merge(chq);
						
						List<BankMstr> bnkList = new ArrayList<>(); 
						cr=session.createCriteria(BankMstr.class);
						cr.add(Restrictions.eq(BankMstrCNTS.BNK_ID,bankMstr.getBnkId()));
						bnkList = cr.list();
						
						BankMstr bank = bnkList.get(0);
						double balAmt = bank.getBnkBalanceAmt();
						double dedAmt = totRentAmt - totTdsAmt;
						double newBalAmt = balAmt - dedAmt;
						System.out.println("balAmt = "+balAmt);
						System.out.println("dedAmt = "+dedAmt);
						System.out.println("newBalAmt = "+newBalAmt);
						bank.setBnkBalanceAmt(newBalAmt);
						session.merge(bank);
						
						//main cashStmt entry for bank start
						CashStmt mainCashStmt = new CashStmt();
						mainCashStmt.setbCode(currentUser.getUserBranchCode());
						mainCashStmt.setUserCode(currentUser.getUserCode());
						mainCashStmt.setCsDescription(voucherService.getDesc());
						mainCashStmt.setCsDrCr('C');
						mainCashStmt.setCsAmt(totRentAmt - totTdsAmt);
						mainCashStmt.setCsType(voucherService.getVoucherType());
						mainCashStmt.setCsVouchType("bank");
						mainCashStmt.setCsChequeType(chequeType);
						mainCashStmt.setCsVouchNo(voucherNo);
						mainCashStmt.setCsPayTo(voucherService.getPayTo());
						mainCashStmt.setCsFaCode(voucherService.getBankCode());
						mainCashStmt.setCsTvNo(tvNo);
						mainCashStmt.setCsDt(sqlDate);
						mainCashStmt.setPayMode(payMode);
						
						cr = session.createCriteria(CashStmtStatus.class);
						cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
						cssList = cr.list();
						if (!cssList.isEmpty()) {
							CashStmtStatus csStatus = cssList.get(0);
							mainCashStmt.setCsNo(csStatus);
							csStatus.getCashStmtList().add(mainCashStmt);
							session.update(csStatus);
						}
						//main cashStmt entry for bank end
						
						
						for(int i=0;i<subFList.size();i++){
							int rmId = (int) subFList.get(i).get(ConstantsValues.RM_ID);
							double rentAmt = Double.parseDouble(String.valueOf(subFList.get(i).get("amt")));
							String destBrFaCode = "";
							destBrFaCode = (String) subFList.get(i).get(ConstantsValues.BRH_FA_CODE);;
							String empFaCode = "";
							empFaCode = (String) subFList.get(i).get(ConstantsValues.EMP_FA_CODE);
							String tdsCode = "";
							tdsCode = (String) subFList.get(i).get("tdsCode");
							double tdsAmt = 0.0;
							tdsAmt = Double.parseDouble(String.valueOf(subFList.get(i).get("tdsAmt")));
							
							if(brFaCode.equalsIgnoreCase(destBrFaCode)){
								System.out.println("*********Same Branch Case*********");
								
								CashStmt cashStmt = new CashStmt();
								cashStmt.setbCode(currentUser.getUserBranchCode());
								cashStmt.setUserCode(currentUser.getUserCode());
								cashStmt.setCsDescription(voucherService.getDesc());
								cashStmt.setCsDrCr('D');
								cashStmt.setCsAmt(rentAmt);
								cashStmt.setCsType(voucherService.getVoucherType());
								cashStmt.setCsVouchType("bank");
								cashStmt.setCsFaCode(rentCode);
								cashStmt.setCsTvNo(tvNo);
								cashStmt.setCsVouchNo(voucherNo);
								cashStmt.setCsSFId(rmId);								
								cashStmt.setCsDt(sqlDate);
								cashStmt.setPayMode(payMode);
								
								cr = session.createCriteria(CashStmtStatus.class);
								cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
								cssList = cr.list();
								if (!cssList.isEmpty()) {
									CashStmtStatus csStatus = cssList.get(0);
									cashStmt.setCsNo(csStatus);
									csStatus.getCashStmtList().add(cashStmt);
									session.update(csStatus);
								}
								
								if(tdsCode != null && tdsCode != "" && tdsAmt > 0){
									
									CashStmt cashStmt1 = new CashStmt();
									cashStmt1.setbCode(currentUser.getUserBranchCode());
									cashStmt1.setUserCode(currentUser.getUserCode());
									cashStmt1.setCsDescription(voucherService.getDesc());
									cashStmt1.setCsDrCr('C');
									cashStmt1.setCsAmt(tdsAmt);
									cashStmt1.setCsType(voucherService.getVoucherType());
									cashStmt1.setCsVouchType("bank");
									cashStmt1.setCsFaCode(tdsCode);
									cashStmt1.setCsTvNo(tvNo);
									cashStmt1.setCsVouchNo(voucherNo);
									cashStmt1.setCsSFId(rmId);
									cashStmt1.setCsDt(sqlDate);
									cashStmt1.setPayMode(payMode);
									
									cr = session.createCriteria(CashStmtStatus.class);
									cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
									cssList = cr.list();
									if (!cssList.isEmpty()) {
										CashStmtStatus csStatus = cssList.get(0);
										cashStmt1.setCsNo(csStatus);
										csStatus.getCashStmtList().add(cashStmt1);
										session.update(csStatus);
									}
								}
								
								
							}else{
								System.out.println("*********Inter Branch Case********");
								
								CashStmt cashStmt = new CashStmt();
								cashStmt.setbCode(currentUser.getUserBranchCode());
								cashStmt.setUserCode(currentUser.getUserCode());
								cashStmt.setCsDescription(voucherService.getDesc());
								cashStmt.setCsDrCr('D');
								cashStmt.setCsAmt(rentAmt - tdsAmt);
								cashStmt.setCsType(voucherService.getVoucherType());
								cashStmt.setCsVouchType("bank");
								cashStmt.setCsFaCode(destBrFaCode);
								cashStmt.setCsTvNo(tvNo);
								cashStmt.setCsVouchNo(voucherNo);
								cashStmt.setCsSFId(rmId);
								cashStmt.setCsDt(sqlDate);
								cashStmt.setPayMode(payMode);
								
								cr = session.createCriteria(CashStmtStatus.class);
								cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
								cssList = cr.list();
								if (!cssList.isEmpty()) {
									CashStmtStatus csStatus = cssList.get(0);
									cashStmt.setCsNo(csStatus);
									csStatus.getCashStmtList().add(cashStmt);
									session.update(csStatus);
								}
								
								
								
								CashStmtTemp cashStmt1 = new CashStmtTemp();
								cashStmt1.setbCode(currentUser.getUserBranchCode());
								cashStmt1.setUserCode(currentUser.getUserCode());
								cashStmt1.setCsDescription(voucherService.getDesc());
								cashStmt1.setCsDrCr('C');
								cashStmt1.setCsType(voucherService.getVoucherType());
								cashStmt1.setCsTvNo(tvNo);
								cashStmt1.setCsVouchType("contra");
								cashStmt1.setCsAmt(rentAmt - tdsAmt);
								cashStmt1.setCsFaCode(brFaCode);
								cashStmt1.setCsDt(sqlDate);
								// csWithTds.setCsVouchNo(voucherNo);
								cashStmt1.setCsPayTo(voucherService.getPayTo());
								cashStmt1.setCsBrhFaCode(destBrFaCode);
								cashStmt1.setPayMode(payMode);

								session.save(cashStmt1);
								
								CashStmtTemp cashStmt2 = new CashStmtTemp();
								cashStmt2.setbCode(currentUser.getUserBranchCode());
								cashStmt2.setUserCode(currentUser.getUserCode());
								cashStmt2.setCsDescription(voucherService.getDesc());
								cashStmt2.setCsDrCr('D');
								cashStmt2.setCsType(voucherService.getVoucherType());
								cashStmt2.setCsTvNo(tvNo);
								cashStmt2.setCsVouchType("contra");
								cashStmt2.setCsAmt(rentAmt);
								cashStmt2.setCsFaCode(rentCode);
								cashStmt2.setCsDt(sqlDate);
								cashStmt2.setCsPayTo(voucherService.getPayTo());
								cashStmt2.setCsBrhFaCode(destBrFaCode);
								cashStmt2.setPayMode(payMode);

								session.save(cashStmt2);
								
								if(tdsCode != null && tdsCode != "" && tdsAmt > 0){

									CashStmtTemp cashStmt3 = new CashStmtTemp();
									cashStmt3.setbCode(currentUser.getUserBranchCode());
									cashStmt3.setUserCode(currentUser.getUserCode());
									cashStmt3.setCsDescription(voucherService.getDesc());
									cashStmt3.setCsDrCr('C');
									cashStmt3.setCsType(voucherService.getVoucherType());
									cashStmt3.setCsTvNo(tvNo);
									cashStmt3.setCsVouchType("contra");
									cashStmt3.setCsAmt(tdsAmt);
									cashStmt3.setCsFaCode(tdsCode);
									cashStmt3.setCsDt(sqlDate);
									cashStmt3.setCsPayTo(voucherService.getPayTo());
									cashStmt3.setCsBrhFaCode(destBrFaCode);
									cashStmt3.setPayMode(payMode);

									session.save(cashStmt3);

								}
								
							}
						
						}
						
					}else{
						voucherNo = 0;
						System.out.println("invalid bank");
					}
					
					
				}else if(payBy == 'O'){
					payMode = "O";
					CashStmtStatus cashStmtStatus = voucherService.getCashStmtStatus();
					List<BankMstr> bankMstrList = new ArrayList<>();
					String bankCode = voucherService.getBankCode();
					cr=session.createCriteria(BankMstr.class);
					cr.add(Restrictions.eq(BankMstrCNTS.BNK_FA_CODE, bankCode));
					bankMstrList = cr.list();
					
					if(!bankMstrList.isEmpty()){
						BankMstr bankMstr = bankMstrList.get(0);
						double totRentAmt = 0.0;
						double totTdsAmt = 0.0;
						
						
						for(int i=0;i<subFList.size();i++){
							double rentAmt = 0.0;
							rentAmt = Double.parseDouble(String.valueOf(subFList.get(i).get("amt")));
							double tdsAmt = 0.0;
							tdsAmt = Double.parseDouble(String.valueOf(subFList.get(i).get("tdsAmt")));
						
							totRentAmt = totRentAmt + rentAmt;
							totTdsAmt  = totTdsAmt + tdsAmt;
						}
						
						List<BankMstr> bnkList = new ArrayList<>(); 
						cr=session.createCriteria(BankMstr.class);
						cr.add(Restrictions.eq(BankMstrCNTS.BNK_ID,bankMstr.getBnkId()));
						bnkList = cr.list();
						
						BankMstr bank = bnkList.get(0);
						double balAmt = bank.getBnkBalanceAmt();
						double newBalAmt = balAmt - (totRentAmt - totTdsAmt);
						bank.setBnkBalanceAmt(newBalAmt);
						session.merge(bank);
						
						//main cashStmt entry for bank start
						CashStmt mainCashStmt = new CashStmt();
						mainCashStmt.setbCode(currentUser.getUserBranchCode());
						mainCashStmt.setUserCode(currentUser.getUserCode());
						mainCashStmt.setCsDescription(voucherService.getDesc());
						mainCashStmt.setCsDrCr('C');
						mainCashStmt.setCsAmt(totRentAmt - totTdsAmt);
						mainCashStmt.setCsType(voucherService.getVoucherType());
						mainCashStmt.setCsVouchType("bank");
						mainCashStmt.setCsVouchNo(voucherNo);
						mainCashStmt.setCsPayTo(voucherService.getPayTo());
						mainCashStmt.setCsFaCode(voucherService.getBankCode());
						mainCashStmt.setCsTvNo(tvNo);
						mainCashStmt.setCsDt(sqlDate);
						mainCashStmt.setPayMode(payMode);
						
						cr = session.createCriteria(CashStmtStatus.class);
						cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
						cssList = cr.list();
						if (!cssList.isEmpty()) {
							CashStmtStatus csStatus = cssList.get(0);
							mainCashStmt.setCsNo(csStatus);
							csStatus.getCashStmtList().add(mainCashStmt);
							session.update(csStatus);
						}
						//main cashStmt entry for bank end
						
						
						for(int i=0;i<subFList.size();i++){
							int rmId = (int) subFList.get(i).get(ConstantsValues.RM_ID);
							double rentAmt = Double.parseDouble(String.valueOf(subFList.get(i).get("amt")));
							String destBrFaCode = "";
							destBrFaCode = (String) subFList.get(i).get(ConstantsValues.BRH_FA_CODE);;
							String empFaCode = "";
							empFaCode = (String) subFList.get(i).get(ConstantsValues.EMP_FA_CODE);
							String tdsCode = "";
							tdsCode = (String) subFList.get(i).get("tdsCode");
							double tdsAmt = 0.0;
							tdsAmt = Double.parseDouble(String.valueOf(subFList.get(i).get("tdsAmt")));
							
							if(brFaCode.equalsIgnoreCase(destBrFaCode)){
								System.out.println("*********Same Branch Case*********");
								
								CashStmt cashStmt = new CashStmt();
								cashStmt.setbCode(currentUser.getUserBranchCode());
								cashStmt.setUserCode(currentUser.getUserCode());
								cashStmt.setCsDescription(voucherService.getDesc());
								cashStmt.setCsDrCr('D');
								cashStmt.setCsAmt(rentAmt);
								cashStmt.setCsType(voucherService.getVoucherType());
								cashStmt.setCsVouchType("bank");
								cashStmt.setCsFaCode(rentCode);
								cashStmt.setCsTvNo(tvNo);
								cashStmt.setCsVouchNo(voucherNo);
								cashStmt.setCsSFId(rmId);								
								cashStmt.setCsDt(sqlDate);
								cashStmt.setPayMode(payMode);
								
								cr = session.createCriteria(CashStmtStatus.class);
								cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
								cssList = cr.list();
								if (!cssList.isEmpty()) {
									CashStmtStatus csStatus = cssList.get(0);
									cashStmt.setCsNo(csStatus);
									csStatus.getCashStmtList().add(cashStmt);
									session.update(csStatus);
								}
								
								if(tdsCode != null && tdsCode != "" && tdsAmt > 0){
									
									CashStmt cashStmt1 = new CashStmt();
									cashStmt1.setbCode(currentUser.getUserBranchCode());
									cashStmt1.setUserCode(currentUser.getUserCode());
									cashStmt1.setCsDescription(voucherService.getDesc());
									cashStmt1.setCsDrCr('C');
									cashStmt1.setCsAmt(tdsAmt);
									cashStmt1.setCsType(voucherService.getVoucherType());
									cashStmt1.setCsVouchType("bank");
									cashStmt1.setCsFaCode(tdsCode);
									cashStmt1.setCsTvNo(tvNo);
									cashStmt1.setCsVouchNo(voucherNo);
									cashStmt1.setCsSFId(rmId);
									cashStmt1.setCsDt(sqlDate);
									cashStmt1.setPayMode(payMode);
									
									cr = session.createCriteria(CashStmtStatus.class);
									cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
									cssList = cr.list();
									if (!cssList.isEmpty()) {
										CashStmtStatus csStatus = cssList.get(0);
										cashStmt1.setCsNo(csStatus);
										csStatus.getCashStmtList().add(cashStmt1);
										session.update(csStatus);
									}
								}
								
								
							}else{
								System.out.println("*********Inter Branch Case********");
								
								CashStmt cashStmt = new CashStmt();
								cashStmt.setbCode(currentUser.getUserBranchCode());
								cashStmt.setUserCode(currentUser.getUserCode());
								cashStmt.setCsDescription(voucherService.getDesc());
								cashStmt.setCsDrCr('D');
								cashStmt.setCsAmt(rentAmt - tdsAmt);
								cashStmt.setCsType(voucherService.getVoucherType());
								cashStmt.setCsVouchType("bank");
								cashStmt.setCsFaCode(destBrFaCode);
								cashStmt.setCsTvNo(tvNo);
								cashStmt.setCsVouchNo(voucherNo);
								cashStmt.setCsSFId(rmId);
								cashStmt.setCsDt(sqlDate);
								cashStmt.setPayMode(payMode);
								
								cr = session.createCriteria(CashStmtStatus.class);
								cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
								cssList = cr.list();
								if (!cssList.isEmpty()) {
									CashStmtStatus csStatus = cssList.get(0);
									cashStmt.setCsNo(csStatus);
									csStatus.getCashStmtList().add(cashStmt);
									session.update(csStatus);
								}
								
								
								
								CashStmtTemp cashStmt1 = new CashStmtTemp();
								cashStmt1.setbCode(currentUser.getUserBranchCode());
								cashStmt1.setUserCode(currentUser.getUserCode());
								cashStmt1.setCsDescription(voucherService.getDesc());
								cashStmt1.setCsDrCr('C');
								cashStmt1.setCsType(voucherService.getVoucherType());
								cashStmt1.setCsTvNo(tvNo);
								cashStmt1.setCsVouchType("contra");
								cashStmt1.setCsAmt(rentAmt - tdsAmt);
								cashStmt1.setCsFaCode(brFaCode);
								cashStmt1.setCsDt(sqlDate);
								cashStmt1.setCsPayTo(voucherService.getPayTo());
								cashStmt1.setCsBrhFaCode(destBrFaCode);
								cashStmt1.setPayMode(payMode);

								session.save(cashStmt1);
								
								CashStmtTemp cashStmt2 = new CashStmtTemp();
								cashStmt2.setbCode(currentUser.getUserBranchCode());
								cashStmt2.setUserCode(currentUser.getUserCode());
								cashStmt2.setCsDescription(voucherService.getDesc());
								cashStmt2.setCsDrCr('D');
								cashStmt2.setCsType(voucherService.getVoucherType());
								cashStmt2.setCsTvNo(tvNo);
								cashStmt2.setCsVouchType("contra");
								cashStmt2.setCsAmt(rentAmt);
								cashStmt2.setCsFaCode(rentCode);
								cashStmt2.setCsDt(sqlDate);
								cashStmt2.setCsPayTo(voucherService.getPayTo());
								cashStmt2.setCsBrhFaCode(destBrFaCode);
								cashStmt2.setPayMode(payMode);

								session.save(cashStmt2);
								
								if(tdsCode != null && tdsCode != "" && tdsAmt > 0){

									CashStmtTemp cashStmt3 = new CashStmtTemp();
									cashStmt3.setbCode(currentUser.getUserBranchCode());
									cashStmt3.setUserCode(currentUser.getUserCode());
									cashStmt3.setCsDescription(voucherService.getDesc());
									cashStmt3.setCsDrCr('C');
									cashStmt3.setCsType(voucherService.getVoucherType());
									cashStmt3.setCsTvNo(tvNo);
									cashStmt3.setCsVouchType("contra");
									cashStmt3.setCsAmt(tdsAmt);
									cashStmt3.setCsFaCode(tdsCode);
									cashStmt3.setCsDt(sqlDate);
									cashStmt3.setCsPayTo(voucherService.getPayTo());
									cashStmt3.setCsBrhFaCode(destBrFaCode);
									cashStmt3.setPayMode(payMode);

									session.save(cashStmt3);
								}
							}
						
						}
						
					}else{
						voucherNo = 0;
						System.out.println("invalid bank");
					}
					
				}else{
					System.out.println("invalid payment method");
				}
				
				transaction.commit();
				session.flush();
			}catch(Exception e){
				e.printStackTrace();
			}
			session.clear();
			session.close();
		}else{
			voucherNo = 0;
		}
		
		return voucherNo;
	}
	
	
	
	@SuppressWarnings("unchecked")
	@Transactional
	public Map<String,Object> saveAtmVoucher(VoucherService voucherService){
		System.out.println("enter into saveAtmVoucher function");
		User currentUser = (User) httpSession.getAttribute("currentUser");
		Map<String, Object> map = new HashMap<String, Object>();
		CashStmtStatus cashStmtStatus1 = voucherService.getCashStmtStatus();
		String brFaCode = voucherService.getBranch().getBranchFaCode();		
		String tvNo = voucherService.getAtmCode();
		map.put("tvNo", tvNo);
		String payMode = "A";
		int voucherNo = 0;
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try{
			List<CashStmtStatus> cssList = new ArrayList<>();
			java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus1.getCssDt().getTime());

			Criteria cr = session.createCriteria(CashStmt.class);
			cr.add(Restrictions.eq(CashStmtCNTS.CS_CDT, sqlDate));
			cr.add(Restrictions.eq(CashStmtCNTS.USER_BRANCH_CODE,currentUser.getUserBranchCode()));
			ProjectionList proList = Projections.projectionList();
			proList.add(Projections.property("csVouchNo"));
			cr.setProjection(proList);

			List<Integer> voucherNoList = cr.list();

			if (!voucherNoList.isEmpty()) {
				voucherNo = 0;
				for(int i=0;i<voucherNoList.size();i++){
					if(voucherNo < voucherNoList.get(i)){
						voucherNo = voucherNoList.get(i);
					}
				}
				voucherNo = voucherNo + 1;
			} else {
				voucherNo = 1;
			}
			map.put("vhNo",voucherNo);
			
			String atmCardNo = voucherService.getAtmCode();
			List<AtmCardMstr> atmList = new ArrayList<>();
			if(atmCardNo != null){
				
				List<FAMaster> faMList = new ArrayList<>();
				cr=session.createCriteria(FAMaster.class);
				cr.add(Restrictions.eq(FAMasterCNTS.FA_MFA_NAME,ConstantsValues.CASH_IN_HAND));
				faMList =cr.list();
				String cashFaCode = "";
				if(!faMList.isEmpty()){
					cashFaCode = faMList.get(0).getFaMfaCode();
				}
				
				cr = session.createCriteria(AtmCardMstr.class);
				cr.add(Restrictions.eq(AtmCardMstrCNTS.ATM_CARD_NO,atmCardNo));
				atmList = cr.list();
				if(!atmList.isEmpty()){
					AtmCardMstr atm = atmList.get(0);
					BankMstr bank = atm.getBankMstr();
					Branch branch = bank.getBranch();
					
					map.put("bnkName",bank.getBnkName());
					map.put("bnkCode",bank.getBnkFaCode());
					
					double balAmt = bank.getBnkBalanceAmt();
					double newBalAmt = balAmt - voucherService.getAmount();
					System.out.println("balAmt = "+balAmt);
					System.out.println("newBalAmt = "+newBalAmt);
					bank.setBnkBalanceAmt(newBalAmt);
					session.merge(bank);
					
					if(brFaCode.equalsIgnoreCase(branch.getBranchFaCode())){
						System.out.println("*********Same Branch Case**********");
						
						CashStmt cashStmt = new CashStmt();
						cashStmt.setbCode(currentUser.getUserBranchCode());
						cashStmt.setUserCode(currentUser.getUserCode());
						cashStmt.setCsDescription(voucherService.getDesc());
						cashStmt.setCsDrCr('D');
						cashStmt.setCsAmt(voucherService.getAmount());
						cashStmt.setCsType(voucherService.getVoucherType());
						cashStmt.setCsVouchType("bank");
						cashStmt.setCsFaCode(cashFaCode);
						cashStmt.setCsTvNo(tvNo);
						cashStmt.setCsVouchNo(voucherNo);
						cashStmt.setCsPayTo(voucherService.getPayTo());
						cashStmt.setCsDt(sqlDate);
						cashStmt.setPayMode(payMode);
						
						cr = session.createCriteria(CashStmtStatus.class);
						cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus1.getCssId()));
						cssList = cr.list();
						if (!cssList.isEmpty()) {
							CashStmtStatus csStatus = cssList.get(0);
							cashStmt.setCsNo(csStatus);
							csStatus.getCashStmtList().add(cashStmt);
							session.update(csStatus);
						}
						
						CashStmt cashStmt1 = new CashStmt();
						cashStmt1.setbCode(currentUser.getUserBranchCode());
						cashStmt1.setUserCode(currentUser.getUserCode());
						cashStmt1.setCsDescription(voucherService.getDesc());
						cashStmt1.setCsDrCr('C');
						cashStmt1.setCsAmt(voucherService.getAmount());
						cashStmt1.setCsType(voucherService.getVoucherType());
						cashStmt1.setCsVouchType("cash");
						cashStmt1.setCsFaCode(bank.getBnkFaCode());
						cashStmt1.setCsTvNo(tvNo);
						cashStmt1.setCsVouchNo(voucherNo);
						cashStmt1.setCsPayTo(voucherService.getPayTo());
						cashStmt1.setCsDt(sqlDate);
						cashStmt1.setPayMode(payMode);
						
						cr = session.createCriteria(CashStmtStatus.class);
						cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus1.getCssId()));
						cssList = cr.list();
						if (!cssList.isEmpty()) {
							CashStmtStatus csStatus = cssList.get(0);
							cashStmt1.setCsNo(csStatus);
							csStatus.getCashStmtList().add(cashStmt1);
							session.update(csStatus);
						}
					}else{
						System.out.println("*********Inter Branch Case**********");
						
						CashStmt cashStmt1 = new CashStmt();
						cashStmt1.setbCode(currentUser.getUserBranchCode());
						cashStmt1.setUserCode(currentUser.getUserCode());
						cashStmt1.setCsDescription(voucherService.getDesc());
						cashStmt1.setCsDrCr('C');
						cashStmt1.setCsAmt(voucherService.getAmount());
						cashStmt1.setCsType(voucherService.getVoucherType());
						cashStmt1.setCsVouchType("cash");
						cashStmt1.setCsFaCode(branch.getBranchFaCode());
						cashStmt1.setCsTvNo(tvNo);
						cashStmt1.setCsVouchNo(voucherNo);
						cashStmt1.setCsPayTo(voucherService.getPayTo());
						cashStmt1.setCsDt(sqlDate);
						cashStmt1.setPayMode(payMode);
						
						cr = session.createCriteria(CashStmtStatus.class);
						cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus1.getCssId()));
						cssList = cr.list();
						if (!cssList.isEmpty()) {
							CashStmtStatus csStatus = cssList.get(0);
							cashStmt1.setCsNo(csStatus);
							csStatus.getCashStmtList().add(cashStmt1);
							session.update(csStatus);
						}
						
						CashStmtTemp cashStmt3 = new CashStmtTemp();
						cashStmt3.setbCode(currentUser.getUserBranchCode());
						cashStmt3.setUserCode(currentUser.getUserCode());
						cashStmt3.setCsDescription(voucherService.getDesc());
						cashStmt3.setCsDrCr('C');
						cashStmt3.setCsAmt(voucherService.getAmount());
						cashStmt3.setCsType(voucherService.getVoucherType());
						cashStmt3.setCsVouchType("bank");
						cashStmt3.setCsFaCode(bank.getBnkFaCode());
						cashStmt3.setCsTvNo(tvNo);
						cashStmt3.setCsPayTo(voucherService.getPayTo());
						cashStmt3.setCsDt(sqlDate);
						cashStmt3.setCsBrhFaCode(branch.getBranchFaCode());
						cashStmt3.setPayMode(payMode);
						
						session.save(cashStmt3);
						
						CashStmtTemp cashStmt2 = new CashStmtTemp();
						cashStmt2.setbCode(currentUser.getUserBranchCode());
						cashStmt2.setUserCode(currentUser.getUserCode());
						cashStmt2.setCsDescription(voucherService.getDesc());
						cashStmt2.setCsDrCr('D');
						cashStmt2.setCsAmt(voucherService.getAmount());
						cashStmt2.setCsType(voucherService.getVoucherType());
						cashStmt2.setCsVouchType("bank");
						cashStmt2.setCsFaCode(brFaCode);
						cashStmt2.setCsTvNo(tvNo);
						cashStmt2.setCsPayTo(voucherService.getPayTo());
						cashStmt2.setCsDt(sqlDate);
						cashStmt2.setCsBrhFaCode(branch.getBranchFaCode());
						cashStmt2.setPayMode(payMode);
						
						session.save(cashStmt2);
						
					}
					
				}
			}else{
				logger.info("invalid atmCardNo");
			}
			transaction.commit();
		}catch(Exception e){
			e.printStackTrace();
			transaction.rollback();
			map.clear();
			map.put("vhNo", 0);
		}
		session.close();
		return map;
	}
	
	
	@SuppressWarnings("unchecked")
	@Transactional
	public CashStmtStatus getCssByDt(String brCode , Date cssDt){
		System.out.println("enter into getCssByDt function");
		List<CashStmtStatus> cssList = new ArrayList<>();
		CashStmtStatus cashStmtStatus = new CashStmtStatus();
		Session session = this.sessionFactory.openSession();
		try{
			Criteria cr = session.createCriteria(CashStmtStatus.class);
			cr.add(Restrictions.eq(CashStmtStatusCNTS.B_Code, brCode));
			cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_DT, cssDt));
			cssList = cr.list();
			System.out.println("size of cssList = "+cssList.size());
			if(!cssList.isEmpty()){
				System.out.println("+++++++++++++++++++");
				cashStmtStatus = cssList.get(0);	
				System.out.println("id="+cashStmtStatus.getCssId());
				List<CashStmt> csList=cashStmtStatus.getCashStmtList();
				System.out.println("csList="+csList.size());
				System.out.println();
				Hibernate.initialize(cashStmtStatus.getCashStmtList());				
			}else{
				cashStmtStatus = null;
			}			
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
			cashStmtStatus = null;
		}
		session.clear();
		session.close();
		return cashStmtStatus;
	}
	
	@Override
	public CashStmtStatus getCssByDt(String brCode , Date cssDt,Session session){
		System.out.println("enter into getCssByDt function");
		List<CashStmtStatus> cssList = new ArrayList<>();
		CashStmtStatus cashStmtStatus = new CashStmtStatus();
			Criteria cr = session.createCriteria(CashStmtStatus.class);
			cr.add(Restrictions.eq(CashStmtStatusCNTS.B_Code, brCode));
			cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_DT, cssDt));
			cssList = cr.list();
			System.out.println("size of cssList = "+cssList.size());
			if(!cssList.isEmpty()){
				System.out.println("+++++++++++++++++++");
				cashStmtStatus = cssList.get(0);	
				System.out.println("id="+cashStmtStatus.getCssId());
				List<CashStmt> csList=cashStmtStatus.getCashStmtList();
				System.out.println("csList="+csList.size());
				System.out.println();
			}else{
				cashStmtStatus = null;
			}			
		return cashStmtStatus;
	}
	
	
	
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<CashStmtStatus> getCssByDt(String brCode , Date cssDt, Date toDate){
		System.out.println("enter into getCssByDt function");
		List<CashStmtStatus> cssList = new ArrayList<>();	
		Session session = this.sessionFactory.openSession();
		try{
			Criteria cr = session.createCriteria(CashStmtStatus.class);
			cr.add(Restrictions.eq(CashStmtStatusCNTS.B_Code,brCode));
			if(cssDt != null && toDate != null){				
				cr.add(Restrictions.and(Restrictions.ge(CashStmtStatusCNTS.CSS_DT, cssDt), Restrictions.le(CashStmtStatusCNTS.CSS_DT, toDate)));
			}else if(toDate == null)
				cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_DT,cssDt));
			cssList = cr.list();
			System.out.println("size of cssList = "+cssList.size());
			if(!cssList.isEmpty()){
				System.out.println("+++++++++++++++++++");
				Iterator<CashStmtStatus> cashStmtStatusIterator = cssList.iterator();
				while(cashStmtStatusIterator.hasNext()){
					CashStmtStatus cashStmtStatus = (CashStmtStatus)cashStmtStatusIterator.next();
					Hibernate.initialize(cashStmtStatus.getCashStmtList());
				}				
			}else{
				cssList = null;
			}
			
			session.flush();
		}catch(Exception e){
			e.printStackTrace();
			cssList= null;
		}
		session.clear();
		session.close();
		return cssList;
	}
	
	
	@SuppressWarnings("unchecked")
	@Transactional
	public int delServTaxCS(){
		System.out.println("enter into delServTaxCS function");
		int count = 0;
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try{
			List<CashStmt> csList = new ArrayList<CashStmt>();
			Criteria cr = session.createCriteria(CashStmt.class);
			cr.add(Restrictions.eq("csType","BILL"));
			cr.add(Restrictions.eq("csFaCode","1110200"));
			csList = cr.list();
					
			System.out.println("size of csList = "+csList.size());
			for(int i=0;i<csList.size();i++){
				int id = csList.get(i).getCsId();
				CashStmt cashStmt = (CashStmt) session.get(CashStmt.class,id-1);
				cashStmt.setCsAmt(cashStmt.getCsAmt() + csList.get(i).getCsAmt());
				session.update(cashStmt);
				count = count + 1;
				System.out.println("***************  "+i);
				
			}
			transaction.commit();
			
			session.flush();
			
		}catch(Exception e){
			e.printStackTrace();
		}
		session.clear();
		session.close();
		return count;
	}
	
	@SuppressWarnings("unchecked")
    @Transactional
    public List<BankCS> getBankCsByDt(Date date, int brhId){
        System.out.println("enter into getBankCsByDt function");
        List<BankCS> bcsList = new ArrayList<>();
        Session session = this.sessionFactory.openSession();
        try{
            Criteria cr = session.createCriteria(BankCS.class);
            cr.add(Restrictions.eq(BankCSCNTS.BCS_BRH_ID,brhId));
            cr.add(Restrictions.eq(BankCSCNTS.BCS_DT,date));
            bcsList = cr.list();
            
            session.flush();
        }catch(Exception e){
            e.printStackTrace();
        }
        session.clear();
        session.close();
        return bcsList;
    }
	
	
	@Override
	@SuppressWarnings("unchecked")
    public List<BankCS> getBankCsByDt(Date date, int brhId,Session session){
        System.out.println("enter into getBankCsByDt function");
            
            Criteria cr = session.createCriteria(BankCS.class);
            cr.add(Restrictions.eq(BankCSCNTS.BCS_BRH_ID,brhId));
            cr.add(Restrictions.eq(BankCSCNTS.BCS_DT,date));
            List<BankCS>  bcsList = cr.list();
            
        return bcsList;
    }
		
	// Transaction Methods
	@Override
	public CashStmtStatus getCssByCssDtWithoutCs(Session session, Integer userId, String brCode , Date cssDt){
		logger.info("Enter into getCssByCssDtWithoutCs() : UserId = "+userId+" : UserBranchCode = "+brCode+" : CSS DT = "+cssDt); 
		CashStmtStatus cashStmtStatus = null;
		try{
			Criteria cr = session.createCriteria(CashStmtStatus.class);
			cr.add(Restrictions.eq(CashStmtStatusCNTS.B_Code, brCode));
			cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_DT, cssDt));
			List<CashStmtStatus> cssList = cr.list();			
			if(! cssList.isEmpty()){
				cashStmtStatus = cssList.get(0);
				logger.info("CashStmtStatus found : CSSID = "+cashStmtStatus.getCssId()+" : UserID = "+userId);				
			}else{
				logger.info("CashStmtStatus not found ! UserID = "+userId);
			}
		}catch(Exception e){
			cashStmtStatus = null;
			logger.error("Exception  in finding CashStmtStatus : UserID = "+userId+" : Exception = "+e);			
		}
		logger.info("Exit from getCssByCssDtWithoutCs() : UserID = "+userId);
		return cashStmtStatus;
	}
	
	@Override
	public CashStmtStatus getLastCssByBranch(Session session, Integer userId, String bCode) {
		logger.info("Enter into getLastCssByBranch() : UserID = "+userId+" : BranchCode = "+bCode);
		CashStmtStatus cashStmtStatus  = null;
		try {
			List<CashStmtStatus> cssList = session.createCriteria(CashStmtStatus.class)
					.add(Restrictions.eq(CashStmtStatusCNTS.B_Code, bCode))
					.addOrder(Order.desc(CashStmtStatusCNTS.CSS_ID))
					.setMaxResults(1)
					.list();
			
			System.err.println("CCS Size = "+cssList.size());
			if(! cssList.isEmpty()){
				logger.info("CashStmtStatus found : UserID = "+userId);
				cashStmtStatus = cssList.get(0);
			}			
		} catch (Exception e) {
			logger.error("UserID = "+userId+" : Exception = "+e);
		}
		logger.info("Exit from getLastCssByBranch() : UserID = "+userId);
		return cashStmtStatus;
	}
	
	@Override
	public CashStmtStatus getCssByDt(Session session, Integer userId, String brCode , Date cssDt){
		logger.info("Enter into getCssByDt() : UserID = "+userId+" : BranchCode = "+brCode+" : CssDt = "+cssDt);
		CashStmtStatus cashStmtStatus = null;
		try{
			Criteria cr = session.createCriteria(CashStmtStatus.class);
			cr.add(Restrictions.eq(CashStmtStatusCNTS.B_Code, brCode));
			cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_DT, cssDt));
			cr.setFetchMode(CashStmtStatusCNTS.CASH_STMT_LIST, FetchMode.SELECT);
			
			List<CashStmtStatus> cssList = cr.list();
			logger.info("UserID = "+userId+" : CashStmtStatus Size = "+cssList.size());
			if(! cssList.isEmpty())	
				cashStmtStatus = cssList.get(0);			
		}catch(Exception e){
			logger.error("UserID = "+userId+" : Exception = "+e);
		}
		return cashStmtStatus;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<BankCS> getBankCsByDt(Session session, Integer userId, Date date, int brhId){
		logger.info("Enter into getBankCsByDt() :UserID = "+userId+" : Date = "+date+" : BranchId = "+brhId);
		List<BankCS> bcsList = null;
		try{
			Criteria cr = session.createCriteria(BankCS.class);
			cr.add(Restrictions.eq(BankCSCNTS.BCS_BRH_ID, brhId));
			cr.add(Restrictions.eq(BankCSCNTS.BCS_DT, date));
			bcsList = cr.list();
			
			if(!bcsList.isEmpty() && brhId!=1)
			{
				BankCS bankCS=new BankCS();
				bankCS.setBcsBankId(3);
				bankCS.setBcsBankCode("0700003");
				bankCS.setBcsBrhId(1);
				bankCS.setBcsOpenBal(0.0);
				bankCS.setBcsCloseBal(0.0);
				
				BankCS bankCS2=new BankCS();
				bankCS2.setBcsBankId(10);
				bankCS2.setBcsBankCode("0700010");
				bankCS2.setBcsBrhId(1);
				bankCS2.setBcsOpenBal(0.0);
				bankCS2.setBcsCloseBal(0.0);
				
				BankCS bankCS3=new BankCS();
				bankCS3.setBcsBankId(44);
				bankCS3.setBcsBankCode("0700044");
				bankCS3.setBcsBrhId(1);
				bankCS3.setBcsOpenBal(0.0);
				bankCS3.setBcsCloseBal(0.0);
				BankCS bankCS4=new BankCS();
				bankCS4.setBcsBankId(46);
				bankCS4.setBcsBankCode("0700046");
				bankCS4.setBcsBrhId(1);
				bankCS4.setBcsOpenBal(0.0);
				bankCS4.setBcsCloseBal(0.0);
				BankCS bankCS5=new BankCS();
				bankCS5.setBcsBankId(47);
				bankCS5.setBcsBankCode("0700047");
				bankCS5.setBcsBrhId(1);
				bankCS5.setBcsOpenBal(0.0);
				bankCS5.setBcsCloseBal(0.0);
				bcsList.add(bankCS);
				bcsList.add(bankCS2);
				bcsList.add(bankCS3);
				bcsList.add(bankCS4);
				bcsList.add(bankCS5);
			}
			
			logger.info("UserID = "+userId+" : BankList Size = "+bcsList.size());
		}catch(Exception e){
			logger.error("UserID = "+userId+" : Exception = "+e);
		}
		logger.info("Exit from getBankCsByDt() UserID = "+userId);
		return bcsList;
	}
	
	public List<CashStmtStatus> getCss(Session session, User user, String branchId, Date fromDate, Date toDate){
		logger.info("UserID = "+user.getUserId() + " : Enter into getCss() : BrannchID = "+branchId+" : FrmDate = "+fromDate+" : ToDate = "+toDate);
		List<CashStmtStatus> cssList = new ArrayList<CashStmtStatus>();
		try{
			cssList = session.createCriteria(CashStmtStatus.class)
					.add(Restrictions.eq(CashStmtStatusCNTS.B_Code, branchId))
					.add(Restrictions.ge(CashStmtStatusCNTS.CSS_DT, fromDate))
					.add(Restrictions.le(CashStmtStatusCNTS.CSS_DT, toDate))
					.setFetchMode("cashStmtList", FetchMode.SELECT)
					.list();

		}catch(Exception e){
			logger.error("UserID = "+user.getUserId() + " : Exception = "+e);
		}
		logger.info("UserID = "+user.getUserId() + " : Exit from getCss() : Css Size = "+cssList.size());
		return cssList;
	}
	
	@Override
	public List<BankCS> getBankCs(Session session, User user, Integer branchId, Date date){
		logger.info("UserID = "+user.getUserId()+" : Enter into getBankCs() : BranchId = "+branchId + " : Date = "+date);
		List<BankCS> bcsList = new ArrayList<BankCS>();
		try{
			bcsList = session.createCriteria(BankCS.class)
					.add(Restrictions.eq(BankCSCNTS.BCS_BRH_ID, branchId))
					.add(Restrictions.eq(BankCSCNTS.BCS_DT, date))					
					.list();
		}catch(Exception e){
			logger.error("UserID = "+user.getUserId()+" : Exception = "+e);
		}
		logger.info("UserID = "+user.getUserId()+" : Enter into getBankCsByDt() : BackList Size = "+bcsList.size());
		return bcsList;
	}

	//TODO for back date data
	@Override
	public CashStmtStatus getLastCssByBranchBack(String bCode,Session session) {
		List<CashStmtStatus> cashStmtStatusList = new ArrayList<CashStmtStatus>();
			Query query = session
					.createQuery("from CashStmtStatus where bCode= :type and cssDt <= '2017-10-31' order by cssId DESC");
			query.setParameter("type", bCode);
			cashStmtStatusList = query.setMaxResults(1).list();			
		if (cashStmtStatusList.isEmpty()) {
			return null;
		} else {
			return cashStmtStatusList.get(0);
		}
	}

	
	@Override
	public CashStmtStatus getLastCssByBranchBack(String bCode) {
		List<CashStmtStatus> cashStmtStatusList = new ArrayList<CashStmtStatus>();
		Session session = this.sessionFactory.openSession();
		try{
			Query query = session
					.createQuery("from CashStmtStatus where bCode= :type and cssDt <= '2017-10-31' order by cssId DESC");
			query.setParameter("type", bCode);
			cashStmtStatusList = query.setMaxResults(1).list();			
		} catch (Exception e) {
			e.printStackTrace();
		}
		session.clear();
		session.close();
		if (cashStmtStatusList.isEmpty()) {
			return null;
		} else {
			return cashStmtStatusList.get(0);
		}	
				
	}
	
	@Override
	public CashStmtStatus getCashStmtStatus(String branchCode, Date cssDate) {
		CashStmtStatus cashStmtStatus = null;
		Criteria cr = this.sessionFactory.getCurrentSession().createCriteria(CashStmtStatus.class);
		
		cr.add(Restrictions.eq(CashStmtStatusCNTS.B_Code, branchCode));
		cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_DT, cssDate));
		List<CashStmtStatus> cssList = cr.list();
		
		if (!cssList.isEmpty())
			cashStmtStatus = cssList.get(0);
		
		return cashStmtStatus;
	}
	
	public void merge(CashStmtStatus css) {
		this.sessionFactory.getCurrentSession().merge(css);
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public Map<String, Object> updateCSSByCSN(Session session, int cssId, CashStmt cashStmt) {
		System.out.println("cashStmt tvNo = " + cashStmt.getCsTvNo());
		List<CashStmtStatus> cssList = new ArrayList<CashStmtStatus>();
		String bCode = cashStmt.getbCode();

		Map<String, Object> map = new HashMap<String, Object>();		
			
		String tvNo = cashStmt.getCsTvNo();	
		
		map.put("tvNo", tvNo);
		try {
			Criteria cr1 = session.createCriteria(CashStmtStatus.class);
			cr1.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID, cssId));
			cssList = cr1.list();

			if (!cssList.isEmpty()) {
				CashStmtStatus cashStmtStatus = cssList.get(0);
				java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus
						.getCssDt().getTime());
				cashStmt.setCsNo(cashStmtStatus);

				if (cashStmt.getCsVouchNo() > 0) {
					if (cashStmt.getCsTvNo() == null) {
						cashStmt.setCsTvNo(tvNo);
					}
					cashStmt.setCsTvNo(tvNo);
					cashStmtStatus.getCashStmtList().add(cashStmt);
					session.update(cashStmtStatus);
					map.put("vhNo", cashStmt.getCsVouchNo());
				} else {

					Criteria cr = session.createCriteria(CashStmt.class);
					cr.add(Restrictions.eq(CashStmtCNTS.CS_CDT, sqlDate));
					cr.add(Restrictions
							.eq(CashStmtCNTS.USER_BRANCH_CODE, bCode));
					ProjectionList proList = Projections.projectionList();
					proList.add(Projections.property("csVouchNo"));
					cr.setProjection(proList);

					List<Integer> voucherNoList = cr.list();

					System.out.println("todays cashStmt list = "
							+ voucherNoList.size());
					if (!voucherNoList.isEmpty()) {
						int voucherNo = 0;
						for(int i=0;i<voucherNoList.size();i++){
							if(voucherNo < voucherNoList.get(i)){
								voucherNo = voucherNoList.get(i);
							}
						}
						voucherNo = voucherNo + 1;
						System.out.println("voucher ===============> "+voucherNo);
						map.put("vhNo", voucherNo);
						cashStmt.setCsVouchNo(voucherNo);
						if (cashStmt.getCsTvNo() == null) {
							cashStmt.setCsTvNo(tvNo);
						}
						cashStmt.setCsTvNo(tvNo);
						cashStmtStatus.getCashStmtList().add(cashStmt);
						session.update(cashStmtStatus);
					} else {
						cashStmt.setCsVouchNo(1);
						if (cashStmt.getCsTvNo() == null) {
							cashStmt.setCsTvNo(tvNo);
						}
						map.put("vhNo", 1);
						cashStmt.setCsTvNo(tvNo);
						cashStmtStatus.getCashStmtList().add(cashStmt);
						session.update(cashStmtStatus);
					}
				}

			} else {
				map.put("vhNo", 0);
			}
		} catch (Exception e) {
			e.printStackTrace();
			map.put("vhNo", -1);
		}
		return map;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	@Transactional
	public Map<String, Object> saveRentVoucherN(VoucherService voucherService){
		logger.info("enter into saveRentVoucher function");
		User currentUser = (User) httpSession.getAttribute("currentUser");
		Map<String, Object> map = new HashMap<>();
		
		CashStmtStatus cashStmtStatus1 = voucherService.getCashStmtStatus();
		String brFaCode = voucherService.getBranch().getBranchFaCode();
		logger.info("brFaCode ==> "+brFaCode);
		List<Map<String, Object>> subFList = voucherService.getSubFList();
		
		Date date = new Date(new java.util.Date().getTime());
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		logger.info("unique id =====>" + calendar.getTimeInMillis());
		long milliSec = calendar.getTimeInMillis();
		String tvNo = String.valueOf(milliSec);
		String payMode = "";

		int voucherNo = 0;
		if (!subFList.isEmpty()) {
			Session session = this.sessionFactory.openSession();
			Transaction transaction = session.beginTransaction();
			try {
				List<CashStmtStatus> cssList = new ArrayList<>();
				java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus1.getCssDt().getTime());

				Criteria cr = session.createCriteria(CashStmt.class);
				cr.add(Restrictions.eq(CashStmtCNTS.CS_CDT, sqlDate));
				cr.add(Restrictions.eq(CashStmtCNTS.USER_BRANCH_CODE, currentUser.getUserBranchCode()));
				ProjectionList proList = Projections.projectionList();
				proList.add(Projections.property("csVouchNo"));
				cr.setProjection(proList);

				List<Integer> voucherNoList = cr.list();

				if (!voucherNoList.isEmpty()) {
					for(int i=0;i<voucherNoList.size();i++){
						if(voucherNo < voucherNoList.get(i)){
							voucherNo = voucherNoList.get(i);
						}
					}
					voucherNo = voucherNo + 1;
				} else {
					voucherNo = 1;
				}
				
				cr=session.createCriteria(FAMaster.class);
				cr.add(Restrictions.eq(FAMasterCNTS.FA_MFA_NAME,ConstantsValues.RENT_FANAME));
				List<FAMaster> faMList =cr.list();
				
				String rentCode = "";
				if(!faMList.isEmpty()){
					rentCode = faMList.get(0).getFaMfaCode();
				}
				
				char payBy = voucherService.getPayBy();
				logger.info("value of payBy = "+payBy);
				
				if(payBy == 'C'){
					payMode = "C";
					for(int i=0;i<subFList.size();i++){
						int rmId = (int) subFList.get(i).get(ConstantsValues.RM_ID);
						double rentAmt = Double.parseDouble(String.valueOf(subFList.get(i).get("amt")));
						String destBrFaCode = (String) subFList.get(i).get(ConstantsValues.BRH_FA_CODE);
						String tdsCode = (String) subFList.get(i).get("tdsCode");
						double tdsAmt = Double.parseDouble(String.valueOf(subFList.get(i).get("tdsAmt")));
						
						if(brFaCode.equalsIgnoreCase(destBrFaCode)){
							logger.info("*********Same Branch Case*********");
							
							CashStmtStatus cashStmtStatus = voucherService.getCashStmtStatus();
							CashStmt cashStmt = new CashStmt();
							cashStmt.setbCode(currentUser.getUserBranchCode());
							cashStmt.setUserCode(currentUser.getUserCode());
							cashStmt.setCsDescription(voucherService.getDesc());
							cashStmt.setCsDrCr('D');
							cashStmt.setCsAmt(rentAmt);
							cashStmt.setCsType(voucherService.getVoucherType());
							cashStmt.setCsVouchType("cash");
							cashStmt.setCsFaCode(rentCode);
							cashStmt.setCsTvNo(tvNo);
							cashStmt.setCsVouchNo(voucherNo);
							cashStmt.setCsSFId(rmId);							
							cashStmt.setCsDt(sqlDate);
							cashStmt.setPayMode(payMode);
							
							cr = session.createCriteria(CashStmtStatus.class);
							cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
							cssList = cr.list();
							if (!cssList.isEmpty()) {
								CashStmtStatus csStatus = cssList.get(0);
								cashStmt.setCsNo(csStatus);
								csStatus.getCashStmtList().add(cashStmt);
								session.update(csStatus);
							}
							
							if(tdsCode != null && tdsCode != "" && tdsAmt > 0){
								
								CashStmt cashStmt1 = new CashStmt();
								cashStmt1.setbCode(currentUser.getUserBranchCode());
								cashStmt1.setUserCode(currentUser.getUserCode());
								cashStmt1.setCsDescription(voucherService.getDesc());
								cashStmt1.setCsDrCr('C');
								cashStmt1.setCsAmt(tdsAmt);
								cashStmt1.setCsType(voucherService.getVoucherType());
								cashStmt1.setCsVouchType("cash");
								cashStmt1.setCsFaCode(tdsCode);
								cashStmt1.setCsTvNo(tvNo);
								cashStmt1.setCsVouchNo(voucherNo);
								cashStmt1.setCsSFId(rmId);
								cashStmt1.setCsDt(sqlDate);
								cashStmt1.setPayMode(payMode);
								
								cr = session.createCriteria(CashStmtStatus.class);
								cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
								cssList = cr.list();
								if (!cssList.isEmpty()) {
									CashStmtStatus csStatus = cssList.get(0);
									cashStmt1.setCsNo(csStatus);
									csStatus.getCashStmtList().add(cashStmt1);
									session.update(csStatus);
								}
							}
						}else{
							logger.info("*********Inter Branch Case********");
							
							CashStmtStatus cashStmtStatus = voucherService.getCashStmtStatus();
							CashStmt cashStmt = new CashStmt();
							cashStmt.setbCode(currentUser.getUserBranchCode());
							cashStmt.setUserCode(currentUser.getUserCode());
							cashStmt.setCsDescription(voucherService.getDesc());
							cashStmt.setCsDrCr('D');
							cashStmt.setCsAmt(rentAmt - tdsAmt);
							cashStmt.setCsType(voucherService.getVoucherType());
							cashStmt.setCsVouchType("cash");
							cashStmt.setCsFaCode(destBrFaCode);
							cashStmt.setCsTvNo(tvNo);
							cashStmt.setCsVouchNo(voucherNo);
							cashStmt.setCsSFId(rmId);
							cashStmt.setCsDt(sqlDate);
							cashStmt.setPayMode(payMode);
							
							cr = session.createCriteria(CashStmtStatus.class);
							cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
							cssList = cr.list();
							if (!cssList.isEmpty()) {
								CashStmtStatus csStatus = cssList.get(0);
								cashStmt.setCsNo(csStatus);
								csStatus.getCashStmtList().add(cashStmt);
								session.update(csStatus);
							}
							
							
							// new code TODO
							List<Branch> list=session.createCriteria(Branch.class)
									.add(Restrictions.eq(BranchCNTS.BRANCH_FA_CODE, destBrFaCode)).list();
							String bCode=list.get(0).getBranchCode();
							
							List<CashStmtStatus> newCssList=session.createCriteria(CashStmtStatus.class)
									.add(Restrictions.eq(CashStmtStatusCNTS.B_Code, bCode))
									.add(Restrictions.eq(CashStmtStatusCNTS.CSS_DT, sqlDate)).list();
							
							
							
							if(!newCssList.isEmpty()){
								int vchrNo=0;
								if(!newCssList.get(0).getCashStmtList().isEmpty()){
									for(int p=0;p<newCssList.get(0).getCashStmtList().size();p++){
										if(vchrNo < newCssList.get(0).getCashStmtList().get(p).getCsVouchNo()){
											vchrNo=newCssList.get(0).getCashStmtList().get(p).getCsVouchNo();
										}
									}
									vchrNo=vchrNo+1;
								}else{
									vchrNo=1;
								}
								
								
							
								CashStmt cashStmta = new CashStmt();
								cashStmta.setbCode(bCode);
								cashStmta.setUserCode(currentUser.getUserCode());
								cashStmta.setCsDescription(voucherService.getDesc());
								cashStmta.setCsDrCr('C');
								cashStmta.setCsAmt(rentAmt - tdsAmt);
								cashStmta.setCsType(voucherService.getVoucherType());
								cashStmta.setCsVouchType("contra");
								cashStmta.setCsFaCode(brFaCode);
								cashStmta.setCsTvNo(tvNo);
								cashStmta.setCsVouchNo(vchrNo);
								cashStmta.setCsSFId(rmId);
								cashStmta.setCsDt(sqlDate);
								cashStmta.setPayMode(payMode);
								
								
								CashStmtStatus csStatus = newCssList.get(0);
								csStatus.getCashStmtList().add(cashStmta);
								cashStmta.setCsNo(csStatus);
								
								
								
								
								CashStmt cashStmtb = new CashStmt();
								cashStmtb.setbCode(bCode);
								cashStmtb.setUserCode(currentUser.getUserCode());
								cashStmtb.setCsDescription(voucherService.getDesc());
								cashStmtb.setCsDrCr('D');
								cashStmtb.setCsAmt(rentAmt - tdsAmt);
								cashStmtb.setCsType(voucherService.getVoucherType());
								cashStmtb.setCsVouchType("contra");
								cashStmtb.setCsFaCode(rentCode);
								cashStmtb.setCsTvNo(tvNo);
								cashStmtb.setCsVouchNo(vchrNo);
								cashStmtb.setCsSFId(rmId);
								cashStmtb.setCsDt(sqlDate);
								cashStmtb.setPayMode(payMode);
								
								
								cashStmtb.setCsNo(csStatus);
								csStatus.getCashStmtList().add(cashStmtb);
								
								
								if(tdsCode != null && tdsCode != "" && tdsAmt > 0){
									
									CashStmt cashStmt1 = new CashStmt();
									cashStmt1.setbCode(bCode);
									cashStmt1.setUserCode(currentUser.getUserCode());
									cashStmt1.setCsDescription(voucherService.getDesc());
									cashStmt1.setCsDrCr('C');
									cashStmt1.setCsAmt(tdsAmt);
									cashStmt1.setCsType(voucherService.getVoucherType());
									cashStmt1.setCsVouchType("contra");
									cashStmt1.setCsFaCode(tdsCode);
									cashStmt1.setCsTvNo(tvNo);
									cashStmt1.setCsVouchNo(vchrNo);
									cashStmt1.setCsSFId(rmId);
									cashStmt1.setCsDt(sqlDate);
									cashStmt1.setPayMode(payMode);
									
										cashStmt1.setCsNo(csStatus);
										csStatus.getCashStmtList().add(cashStmt1);
								}
								session.update(csStatus);
								
							}else{
								CashStmtTemp cashStmt1 = new CashStmtTemp();
								cashStmt1.setbCode(currentUser.getUserBranchCode());
								cashStmt1.setUserCode(currentUser.getUserCode());
								cashStmt1.setCsDescription(voucherService.getDesc());
								cashStmt1.setCsDrCr('C');
								cashStmt1.setCsType(voucherService.getVoucherType());
								cashStmt1.setCsTvNo(tvNo);
								cashStmt1.setCsVouchType("contra");
								cashStmt1.setCsAmt(rentAmt - tdsAmt);
								cashStmt1.setCsFaCode(brFaCode);
								cashStmt1.setCsDt(sqlDate);
								cashStmt1.setCsPayTo(voucherService.getPayTo());
								cashStmt1.setCsBrhFaCode(destBrFaCode);
								cashStmt1.setPayMode(payMode);

								session.save(cashStmt1);
								
								CashStmtTemp cashStmt2 = new CashStmtTemp();
								cashStmt2.setbCode(currentUser.getUserBranchCode());
								cashStmt2.setUserCode(currentUser.getUserCode());
								cashStmt2.setCsDescription(voucherService.getDesc());
								cashStmt2.setCsDrCr('D');
								cashStmt2.setCsType(voucherService.getVoucherType());
								cashStmt2.setCsTvNo(tvNo);
								cashStmt2.setCsVouchType("contra");
								cashStmt2.setCsAmt(rentAmt);
								cashStmt2.setCsFaCode(rentCode);
								cashStmt2.setCsDt(sqlDate);
								cashStmt2.setCsPayTo(voucherService.getPayTo());
								cashStmt2.setCsBrhFaCode(destBrFaCode);
								cashStmt2.setPayMode(payMode);

								session.save(cashStmt2);
								
								if(tdsCode != null && tdsCode != "" && tdsAmt > 0){
									CashStmtTemp cashStmt3 = new CashStmtTemp();
									cashStmt3.setbCode(currentUser.getUserBranchCode());
									cashStmt3.setUserCode(currentUser.getUserCode());
									cashStmt3.setCsDescription(voucherService.getDesc());
									cashStmt3.setCsDrCr('C');
									cashStmt3.setCsType(voucherService.getVoucherType());
									cashStmt3.setCsTvNo(tvNo);
									cashStmt3.setCsVouchType("contra");
									cashStmt3.setCsAmt(tdsAmt);
									cashStmt3.setCsFaCode(tdsCode);
									cashStmt3.setCsDt(sqlDate);
									cashStmt3.setCsPayTo(voucherService.getPayTo());
									cashStmt3.setCsBrhFaCode(destBrFaCode);
									cashStmt3.setPayMode(payMode);

									session.save(cashStmt3);
								}

							}
							
						}
					
					}
				}else if(payBy == 'Q'){
					payMode = "Q";
					CashStmtStatus cashStmtStatus = voucherService.getCashStmtStatus();
					char chequeType = voucherService.getChequeType();
					logger.info("chequeType = "+chequeType);
					String bankCode = voucherService.getBankCode();
					cr=session.createCriteria(BankMstr.class);
					cr.add(Restrictions.eq(BankMstrCNTS.BNK_FA_CODE, bankCode));
					List<BankMstr> bankMstrList = cr.list();
					
					if(!bankMstrList.isEmpty()){
						BankMstr bankMstr = bankMstrList.get(0);
						double totRentAmt = 0.0;
						double totTdsAmt = 0.0;
						
						
						for(int i=0;i<subFList.size();i++){
							double rentAmt = Double.parseDouble(String.valueOf(subFList.get(i).get("amt")));
							double tdsAmt = Double.parseDouble(String.valueOf(subFList.get(i).get("tdsAmt")));
						
							totRentAmt = totRentAmt + rentAmt;
							totTdsAmt  = totTdsAmt + tdsAmt;
						}
						
						ChequeLeaves chequeLeaves = voucherService.getChequeLeaves();
						cr=session.createCriteria(ChequeLeaves.class);
						cr.add(Restrictions.eq(ChequeLeavesCNTS.CHQ_L_ID,chequeLeaves.getChqLId()));
						List<ChequeLeaves> chqL = cr.list();
						
						ChequeLeaves chq = chqL.get(0);
						chq.setChqLChqAmt(totRentAmt - totTdsAmt);
						chq.setChqLUsedDt(new Date(new java.util.Date().getTime()));
						chq.setChqLUsed(true);
						tvNo = chq.getChqLChqNo();
						
						session.merge(chq);
						
						cr=session.createCriteria(BankMstr.class);
						cr.add(Restrictions.eq(BankMstrCNTS.BNK_ID,bankMstr.getBnkId()));
						List<BankMstr> bnkList = cr.list();
						
						BankMstr bank = bnkList.get(0);
						double balAmt = bank.getBnkBalanceAmt();
						double dedAmt = totRentAmt - totTdsAmt;
						double newBalAmt = balAmt - dedAmt;
						logger.info("balAmt = "+balAmt);
						logger.info("dedAmt = "+dedAmt);
						logger.info("newBalAmt = "+newBalAmt);
						bank.setBnkBalanceAmt(newBalAmt);
						session.merge(bank);
						
						//main cashStmt entry for bank start
						CashStmt mainCashStmt = new CashStmt();
						mainCashStmt.setbCode(currentUser.getUserBranchCode());
						mainCashStmt.setUserCode(currentUser.getUserCode());
						mainCashStmt.setCsDescription(voucherService.getDesc());
						mainCashStmt.setCsDrCr('C');
						mainCashStmt.setCsAmt(totRentAmt - totTdsAmt);
						mainCashStmt.setCsType(voucherService.getVoucherType());
						mainCashStmt.setCsVouchType("bank");
						mainCashStmt.setCsChequeType(chequeType);
						mainCashStmt.setCsVouchNo(voucherNo);
						mainCashStmt.setCsPayTo(voucherService.getPayTo());
						mainCashStmt.setCsFaCode(voucherService.getBankCode());
						mainCashStmt.setCsTvNo(tvNo);
						mainCashStmt.setCsDt(sqlDate);
						mainCashStmt.setPayMode(payMode);
						
						cr = session.createCriteria(CashStmtStatus.class);
						cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
						cssList = cr.list();
						if (!cssList.isEmpty()) {
							CashStmtStatus csStatus = cssList.get(0);
							mainCashStmt.setCsNo(csStatus);
							csStatus.getCashStmtList().add(mainCashStmt);
							session.update(csStatus);
						}
						//main cashStmt entry for bank end
						
						
						for(int i=0;i<subFList.size();i++){
							int rmId = (int) subFList.get(i).get(ConstantsValues.RM_ID);
							double rentAmt = Double.parseDouble(String.valueOf(subFList.get(i).get("amt")));
							String destBrFaCode = (String) subFList.get(i).get(ConstantsValues.BRH_FA_CODE);
							String tdsCode = (String) subFList.get(i).get("tdsCode");
							double tdsAmt = Double.parseDouble(String.valueOf(subFList.get(i).get("tdsAmt")));
							
							if(brFaCode.equalsIgnoreCase(destBrFaCode)){
								logger.info("*********Same Branch Case*********");
								
								CashStmt cashStmt = new CashStmt();
								cashStmt.setbCode(currentUser.getUserBranchCode());
								cashStmt.setUserCode(currentUser.getUserCode());
								cashStmt.setCsDescription(voucherService.getDesc());
								cashStmt.setCsDrCr('D');
								cashStmt.setCsAmt(rentAmt);
								cashStmt.setCsType(voucherService.getVoucherType());
								cashStmt.setCsVouchType("bank");
								cashStmt.setCsFaCode(rentCode);
								cashStmt.setCsTvNo(tvNo);
								cashStmt.setCsVouchNo(voucherNo);
								cashStmt.setCsSFId(rmId);								
								cashStmt.setCsDt(sqlDate);
								cashStmt.setPayMode(payMode);
								
								cr = session.createCriteria(CashStmtStatus.class);
								cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
								cssList = cr.list();
								if (!cssList.isEmpty()) {
									CashStmtStatus csStatus = cssList.get(0);
									cashStmt.setCsNo(csStatus);
									csStatus.getCashStmtList().add(cashStmt);
									session.update(csStatus);
								}
								
								if(tdsCode != null && tdsCode != "" && tdsAmt > 0){
									
									CashStmt cashStmt1 = new CashStmt();
									cashStmt1.setbCode(currentUser.getUserBranchCode());
									cashStmt1.setUserCode(currentUser.getUserCode());
									cashStmt1.setCsDescription(voucherService.getDesc());
									cashStmt1.setCsDrCr('C');
									cashStmt1.setCsAmt(tdsAmt);
									cashStmt1.setCsType(voucherService.getVoucherType());
									cashStmt1.setCsVouchType("bank");
									cashStmt1.setCsFaCode(tdsCode);
									cashStmt1.setCsTvNo(tvNo);
									cashStmt1.setCsVouchNo(voucherNo);
									cashStmt1.setCsSFId(rmId);
									cashStmt1.setCsDt(sqlDate);
									cashStmt1.setPayMode(payMode);
									
									cr = session.createCriteria(CashStmtStatus.class);
									cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
									cssList = cr.list();
									if (!cssList.isEmpty()) {
										CashStmtStatus csStatus = cssList.get(0);
										cashStmt1.setCsNo(csStatus);
										csStatus.getCashStmtList().add(cashStmt1);
										session.update(csStatus);
									}
								}
								
								
							}else{
								logger.info("*********Inter Branch Case********");
								
								CashStmt cashStmt = new CashStmt();
								cashStmt.setbCode(currentUser.getUserBranchCode());
								cashStmt.setUserCode(currentUser.getUserCode());
								cashStmt.setCsDescription(voucherService.getDesc());
								cashStmt.setCsDrCr('D');
								cashStmt.setCsAmt(rentAmt - tdsAmt);
								cashStmt.setCsType(voucherService.getVoucherType());
								cashStmt.setCsVouchType("bank");
								cashStmt.setCsFaCode(destBrFaCode);
								cashStmt.setCsTvNo(tvNo);
								cashStmt.setCsVouchNo(voucherNo);
								cashStmt.setCsSFId(rmId);
								cashStmt.setCsDt(sqlDate);
								cashStmt.setPayMode(payMode);
								
								cr = session.createCriteria(CashStmtStatus.class);
								cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
								cssList = cr.list();
								if (!cssList.isEmpty()) {
									CashStmtStatus csStatus = cssList.get(0);
									cashStmt.setCsNo(csStatus);
									csStatus.getCashStmtList().add(cashStmt);
									session.update(csStatus);
								}
								
								
								// new code TODO
								List<Branch> list=session.createCriteria(Branch.class)
										.add(Restrictions.eq(BranchCNTS.BRANCH_FA_CODE, destBrFaCode)).list();
								String bCode=list.get(0).getBranchCode();
								
								List<CashStmtStatus> newCssList=session.createCriteria(CashStmtStatus.class)
										.add(Restrictions.eq(CashStmtStatusCNTS.B_Code, bCode))
										.add(Restrictions.eq(CashStmtStatusCNTS.CSS_DT, sqlDate)).list();
								
								
								
								if(!newCssList.isEmpty()){
									int vchrNo=0;
									if(!newCssList.get(0).getCashStmtList().isEmpty()){
										for(int p=0;p<newCssList.get(0).getCashStmtList().size();p++){
											if(vchrNo < newCssList.get(0).getCashStmtList().get(p).getCsVouchNo()){
												vchrNo=newCssList.get(0).getCashStmtList().get(p).getCsVouchNo();
											}
										}
										vchrNo=vchrNo+1;
									}else{
										vchrNo=1;
									}
									
									
								
									CashStmt cashStmta = new CashStmt();
									cashStmta.setbCode(bCode);
									cashStmta.setUserCode(currentUser.getUserCode());
									cashStmta.setCsDescription(voucherService.getDesc());
									cashStmta.setCsDrCr('C');
									cashStmta.setCsAmt(rentAmt - tdsAmt);
									cashStmta.setCsType(voucherService.getVoucherType());
									cashStmta.setCsVouchType("contra");
									cashStmta.setCsFaCode(brFaCode);
									cashStmta.setCsTvNo(tvNo);
									cashStmta.setCsVouchNo(vchrNo);
									cashStmta.setCsSFId(rmId);
									cashStmta.setCsDt(sqlDate);
									cashStmta.setPayMode(payMode);
									
									
									CashStmtStatus csStatus = newCssList.get(0);
									csStatus.getCashStmtList().add(cashStmta);
									cashStmta.setCsNo(csStatus);
									
									
									
									
									CashStmt cashStmtb = new CashStmt();
									cashStmtb.setbCode(bCode);
									cashStmtb.setUserCode(currentUser.getUserCode());
									cashStmtb.setCsDescription(voucherService.getDesc());
									cashStmtb.setCsDrCr('D');
									cashStmtb.setCsAmt(rentAmt - tdsAmt);
									cashStmtb.setCsType(voucherService.getVoucherType());
									cashStmtb.setCsVouchType("contra");
									cashStmtb.setCsFaCode(rentCode);
									cashStmtb.setCsTvNo(tvNo);
									cashStmtb.setCsVouchNo(vchrNo);
									cashStmtb.setCsSFId(rmId);
									cashStmtb.setCsDt(sqlDate);
									cashStmtb.setPayMode(payMode);
									
									
									cashStmtb.setCsNo(csStatus);
									csStatus.getCashStmtList().add(cashStmtb);
									
									
									if(tdsCode != null && tdsCode != "" && tdsAmt > 0){
										
										CashStmt cashStmt1 = new CashStmt();
										cashStmt1.setbCode(bCode);
										cashStmt1.setUserCode(currentUser.getUserCode());
										cashStmt1.setCsDescription(voucherService.getDesc());
										cashStmt1.setCsDrCr('C');
										cashStmt1.setCsAmt(tdsAmt);
										cashStmt1.setCsType(voucherService.getVoucherType());
										cashStmt1.setCsVouchType("contra");
										cashStmt1.setCsFaCode(tdsCode);
										cashStmt1.setCsTvNo(tvNo);
										cashStmt1.setCsVouchNo(vchrNo);
										cashStmt1.setCsSFId(rmId);
										cashStmt1.setCsDt(sqlDate);
										cashStmt1.setPayMode(payMode);
										
											cashStmt1.setCsNo(csStatus);
											csStatus.getCashStmtList().add(cashStmt1);
									}
									session.update(csStatus);
									
								}else{
									
									CashStmtTemp cashStmt1 = new CashStmtTemp();
									cashStmt1.setbCode(currentUser.getUserBranchCode());
									cashStmt1.setUserCode(currentUser.getUserCode());
									cashStmt1.setCsDescription(voucherService.getDesc());
									cashStmt1.setCsDrCr('C');
									cashStmt1.setCsType(voucherService.getVoucherType());
									cashStmt1.setCsTvNo(tvNo);
									cashStmt1.setCsVouchType("contra");
									cashStmt1.setCsAmt(rentAmt - tdsAmt);
									cashStmt1.setCsFaCode(brFaCode);
									cashStmt1.setCsDt(sqlDate);
									cashStmt1.setCsPayTo(voucherService.getPayTo());
									cashStmt1.setCsBrhFaCode(destBrFaCode);
									cashStmt1.setPayMode(payMode);

									session.save(cashStmt1);
									
									CashStmtTemp cashStmt2 = new CashStmtTemp();
									cashStmt2.setbCode(currentUser.getUserBranchCode());
									cashStmt2.setUserCode(currentUser.getUserCode());
									cashStmt2.setCsDescription(voucherService.getDesc());
									cashStmt2.setCsDrCr('D');
									cashStmt2.setCsType(voucherService.getVoucherType());
									cashStmt2.setCsTvNo(tvNo);
									cashStmt2.setCsVouchType("contra");
									cashStmt2.setCsAmt(rentAmt);
									cashStmt2.setCsFaCode(rentCode);
									cashStmt2.setCsDt(sqlDate);
									cashStmt2.setCsPayTo(voucherService.getPayTo());
									cashStmt2.setCsBrhFaCode(destBrFaCode);
									cashStmt2.setPayMode(payMode);

									session.save(cashStmt2);
									
									if(tdsCode != null && tdsCode != "" && tdsAmt > 0){

										CashStmtTemp cashStmt3 = new CashStmtTemp();
										cashStmt3.setbCode(currentUser.getUserBranchCode());
										cashStmt3.setUserCode(currentUser.getUserCode());
										cashStmt3.setCsDescription(voucherService.getDesc());
										cashStmt3.setCsDrCr('C');
										cashStmt3.setCsType(voucherService.getVoucherType());
										cashStmt3.setCsTvNo(tvNo);
										cashStmt3.setCsVouchType("contra");
										cashStmt3.setCsAmt(tdsAmt);
										cashStmt3.setCsFaCode(tdsCode);
										cashStmt3.setCsDt(sqlDate);
										cashStmt3.setCsPayTo(voucherService.getPayTo());
										cashStmt3.setCsBrhFaCode(destBrFaCode);
										cashStmt3.setPayMode(payMode);

										session.save(cashStmt3);
									}
								
								}
								
							}
						
						}
						
					}else{
						map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
						map.put(ConstantsValues.MSG, "Invalid bank");
					}
					
					
				}else if(payBy == 'O'){
					payMode = "O";
					CashStmtStatus cashStmtStatus = voucherService.getCashStmtStatus();
					String bankCode = voucherService.getBankCode();
					cr=session.createCriteria(BankMstr.class);
					cr.add(Restrictions.eq(BankMstrCNTS.BNK_FA_CODE, bankCode));
					List<BankMstr> bankMstrList = cr.list();
					
					if(!bankMstrList.isEmpty()){
						BankMstr bankMstr = bankMstrList.get(0);
						double totRentAmt = 0.0;
						double totTdsAmt = 0.0;
						
						
						for(int i=0;i<subFList.size();i++){
							double rentAmt = Double.parseDouble(String.valueOf(subFList.get(i).get("amt")));
							double tdsAmt = Double.parseDouble(String.valueOf(subFList.get(i).get("tdsAmt")));
						
							totRentAmt = totRentAmt + rentAmt;
							totTdsAmt  = totTdsAmt + tdsAmt;
						}
						
						cr=session.createCriteria(BankMstr.class);
						cr.add(Restrictions.eq(BankMstrCNTS.BNK_ID,bankMstr.getBnkId()));
						List<BankMstr> bnkList = cr.list();
						
						BankMstr bank = bnkList.get(0);
						double balAmt = bank.getBnkBalanceAmt();
						double newBalAmt = balAmt - (totRentAmt - totTdsAmt);
						bank.setBnkBalanceAmt(newBalAmt);
						session.merge(bank);
						
						//main cashStmt entry for bank start
						CashStmt mainCashStmt = new CashStmt();
						mainCashStmt.setbCode(currentUser.getUserBranchCode());
						mainCashStmt.setUserCode(currentUser.getUserCode());
						mainCashStmt.setCsDescription(voucherService.getDesc());
						mainCashStmt.setCsDrCr('C');
						mainCashStmt.setCsAmt(totRentAmt - totTdsAmt);
						mainCashStmt.setCsType(voucherService.getVoucherType());
						mainCashStmt.setCsVouchType("bank");
						mainCashStmt.setCsVouchNo(voucherNo);
						mainCashStmt.setCsPayTo(voucherService.getPayTo());
						mainCashStmt.setCsFaCode(voucherService.getBankCode());
						mainCashStmt.setCsTvNo(tvNo);
						mainCashStmt.setCsDt(sqlDate);
						mainCashStmt.setPayMode(payMode);
						
						cr = session.createCriteria(CashStmtStatus.class);
						cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
						cssList = cr.list();
						if (!cssList.isEmpty()) {
							CashStmtStatus csStatus = cssList.get(0);
							mainCashStmt.setCsNo(csStatus);
							csStatus.getCashStmtList().add(mainCashStmt);
							session.update(csStatus);
						}
						//main cashStmt entry for bank end
						
						
						for(int i=0;i<subFList.size();i++){
							int rmId = (int) subFList.get(i).get(ConstantsValues.RM_ID);
							double rentAmt = Double.parseDouble(String.valueOf(subFList.get(i).get("amt")));
							String destBrFaCode = (String) subFList.get(i).get(ConstantsValues.BRH_FA_CODE);;
							String tdsCode = (String) subFList.get(i).get("tdsCode");
							double tdsAmt = Double.parseDouble(String.valueOf(subFList.get(i).get("tdsAmt")));
							
							if(brFaCode.equalsIgnoreCase(destBrFaCode)){
								logger.info("*********Same Branch Case*********");
								
								CashStmt cashStmt = new CashStmt();
								cashStmt.setbCode(currentUser.getUserBranchCode());
								cashStmt.setUserCode(currentUser.getUserCode());
								cashStmt.setCsDescription(voucherService.getDesc());
								cashStmt.setCsDrCr('D');
								cashStmt.setCsAmt(rentAmt);
								cashStmt.setCsType(voucherService.getVoucherType());
								cashStmt.setCsVouchType("bank");
								cashStmt.setCsFaCode(rentCode);
								cashStmt.setCsTvNo(tvNo);
								cashStmt.setCsVouchNo(voucherNo);
								cashStmt.setCsSFId(rmId);								
								cashStmt.setCsDt(sqlDate);
								cashStmt.setPayMode(payMode);
								
								cr = session.createCriteria(CashStmtStatus.class);
								cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
								cssList = cr.list();
								if (!cssList.isEmpty()) {
									CashStmtStatus csStatus = cssList.get(0);
									cashStmt.setCsNo(csStatus);
									csStatus.getCashStmtList().add(cashStmt);
									session.update(csStatus);
								}
								
								if(tdsCode != null && tdsCode != "" && tdsAmt > 0){
									
									CashStmt cashStmt1 = new CashStmt();
									cashStmt1.setbCode(currentUser.getUserBranchCode());
									cashStmt1.setUserCode(currentUser.getUserCode());
									cashStmt1.setCsDescription(voucherService.getDesc());
									cashStmt1.setCsDrCr('C');
									cashStmt1.setCsAmt(tdsAmt);
									cashStmt1.setCsType(voucherService.getVoucherType());
									cashStmt1.setCsVouchType("bank");
									cashStmt1.setCsFaCode(tdsCode);
									cashStmt1.setCsTvNo(tvNo);
									cashStmt1.setCsVouchNo(voucherNo);
									cashStmt1.setCsSFId(rmId);
									cashStmt1.setCsDt(sqlDate);
									cashStmt1.setPayMode(payMode);
									
									cr = session.createCriteria(CashStmtStatus.class);
									cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
									cssList = cr.list();
									if (!cssList.isEmpty()) {
										CashStmtStatus csStatus = cssList.get(0);
										cashStmt1.setCsNo(csStatus);
										csStatus.getCashStmtList().add(cashStmt1);
										session.update(csStatus);
									}
								}
								
								
							}else{
								logger.info("*********Inter Branch Case********");
								
								CashStmt cashStmt = new CashStmt();
								cashStmt.setbCode(currentUser.getUserBranchCode());
								cashStmt.setUserCode(currentUser.getUserCode());
								cashStmt.setCsDescription(voucherService.getDesc());
								cashStmt.setCsDrCr('D');
								cashStmt.setCsAmt(rentAmt - tdsAmt);
								cashStmt.setCsType(voucherService.getVoucherType());
								cashStmt.setCsVouchType("bank");
								cashStmt.setCsFaCode(destBrFaCode);
								cashStmt.setCsTvNo(tvNo);
								cashStmt.setCsVouchNo(voucherNo);
								cashStmt.setCsSFId(rmId);
								cashStmt.setCsDt(sqlDate);
								cashStmt.setPayMode(payMode);
								
								cr = session.createCriteria(CashStmtStatus.class);
								cr.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID,cashStmtStatus.getCssId()));
								cssList = cr.list();
								if (!cssList.isEmpty()) {
									CashStmtStatus csStatus = cssList.get(0);
									cashStmt.setCsNo(csStatus);
									csStatus.getCashStmtList().add(cashStmt);
									session.update(csStatus);
								}
								
								// new code TODO
								List<Branch> list=session.createCriteria(Branch.class)
										.add(Restrictions.eq(BranchCNTS.BRANCH_FA_CODE, destBrFaCode)).list();
								String bCode=list.get(0).getBranchCode();
								
								List<CashStmtStatus> newCssList=session.createCriteria(CashStmtStatus.class)
										.add(Restrictions.eq(CashStmtStatusCNTS.B_Code, bCode))
										.add(Restrictions.eq(CashStmtStatusCNTS.CSS_DT, sqlDate)).list();
								
								
								
								if(!newCssList.isEmpty()){
									int vchrNo=0;
									if(!newCssList.get(0).getCashStmtList().isEmpty()){
										for(int p=0;p<newCssList.get(0).getCashStmtList().size();p++){
											if(vchrNo < newCssList.get(0).getCashStmtList().get(p).getCsVouchNo()){
												vchrNo=newCssList.get(0).getCashStmtList().get(p).getCsVouchNo();
											}
										}
										vchrNo=vchrNo+1;
									}else{
										vchrNo=1;
									}
									
									
								
									CashStmt cashStmta = new CashStmt();
									cashStmta.setbCode(bCode);
									cashStmta.setUserCode(currentUser.getUserCode());
									cashStmta.setCsDescription(voucherService.getDesc());
									cashStmta.setCsDrCr('C');
									cashStmta.setCsAmt(rentAmt - tdsAmt);
									cashStmta.setCsType(voucherService.getVoucherType());
									cashStmta.setCsVouchType("contra");
									cashStmta.setCsFaCode(brFaCode);
									cashStmta.setCsTvNo(tvNo);
									cashStmta.setCsVouchNo(vchrNo);
									cashStmta.setCsSFId(rmId);
									cashStmta.setCsDt(sqlDate);
									cashStmta.setPayMode(payMode);
									
									
									CashStmtStatus csStatus = newCssList.get(0);
									csStatus.getCashStmtList().add(cashStmta);
									cashStmta.setCsNo(csStatus);
									
									
									
									
									CashStmt cashStmtb = new CashStmt();
									cashStmtb.setbCode(bCode);
									cashStmtb.setUserCode(currentUser.getUserCode());
									cashStmtb.setCsDescription(voucherService.getDesc());
									cashStmtb.setCsDrCr('D');
									cashStmtb.setCsAmt(rentAmt - tdsAmt);
									cashStmtb.setCsType(voucherService.getVoucherType());
									cashStmtb.setCsVouchType("contra");
									cashStmtb.setCsFaCode(rentCode);
									cashStmtb.setCsTvNo(tvNo);
									cashStmtb.setCsVouchNo(vchrNo);
									cashStmtb.setCsSFId(rmId);
									cashStmtb.setCsDt(sqlDate);
									cashStmtb.setPayMode(payMode);
									
									
									cashStmtb.setCsNo(csStatus);
									csStatus.getCashStmtList().add(cashStmtb);
									
									
									if(tdsCode != null && tdsCode != "" && tdsAmt > 0){
										
										CashStmt cashStmt1 = new CashStmt();
										cashStmt1.setbCode(bCode);
										cashStmt1.setUserCode(currentUser.getUserCode());
										cashStmt1.setCsDescription(voucherService.getDesc());
										cashStmt1.setCsDrCr('C');
										cashStmt1.setCsAmt(tdsAmt);
										cashStmt1.setCsType(voucherService.getVoucherType());
										cashStmt1.setCsVouchType("contra");
										cashStmt1.setCsFaCode(tdsCode);
										cashStmt1.setCsTvNo(tvNo);
										cashStmt1.setCsVouchNo(vchrNo);
										cashStmt1.setCsSFId(rmId);
										cashStmt1.setCsDt(sqlDate);
										cashStmt1.setPayMode(payMode);
										
											cashStmt1.setCsNo(csStatus);
											csStatus.getCashStmtList().add(cashStmt1);
									}
									session.update(csStatus);
									
								}else{
									CashStmtTemp cashStmt1 = new CashStmtTemp();
									cashStmt1.setbCode(currentUser.getUserBranchCode());
									cashStmt1.setUserCode(currentUser.getUserCode());
									cashStmt1.setCsDescription(voucherService.getDesc());
									cashStmt1.setCsDrCr('C');
									cashStmt1.setCsType(voucherService.getVoucherType());
									cashStmt1.setCsTvNo(tvNo);
									cashStmt1.setCsVouchType("contra");
									cashStmt1.setCsAmt(rentAmt - tdsAmt);
									cashStmt1.setCsFaCode(brFaCode);
									cashStmt1.setCsDt(sqlDate);
									cashStmt1.setCsPayTo(voucherService.getPayTo());
									cashStmt1.setCsBrhFaCode(destBrFaCode);
									cashStmt1.setPayMode(payMode);

									session.save(cashStmt1);
									
									CashStmtTemp cashStmt2 = new CashStmtTemp();
									cashStmt2.setbCode(currentUser.getUserBranchCode());
									cashStmt2.setUserCode(currentUser.getUserCode());
									cashStmt2.setCsDescription(voucherService.getDesc());
									cashStmt2.setCsDrCr('D');
									cashStmt2.setCsType(voucherService.getVoucherType());
									cashStmt2.setCsTvNo(tvNo);
									cashStmt2.setCsVouchType("contra");
									cashStmt2.setCsAmt(rentAmt);
									cashStmt2.setCsFaCode(rentCode);
									cashStmt2.setCsDt(sqlDate);
									cashStmt2.setCsPayTo(voucherService.getPayTo());
									cashStmt2.setCsBrhFaCode(destBrFaCode);
									cashStmt2.setPayMode(payMode);

									session.save(cashStmt2);
									
									if(tdsCode != null && tdsCode != "" && tdsAmt > 0){

										CashStmtTemp cashStmt3 = new CashStmtTemp();
										cashStmt3.setbCode(currentUser.getUserBranchCode());
										cashStmt3.setUserCode(currentUser.getUserCode());
										cashStmt3.setCsDescription(voucherService.getDesc());
										cashStmt3.setCsDrCr('C');
										cashStmt3.setCsType(voucherService.getVoucherType());
										cashStmt3.setCsTvNo(tvNo);
										cashStmt3.setCsVouchType("contra");
										cashStmt3.setCsAmt(tdsAmt);
										cashStmt3.setCsFaCode(tdsCode);
										cashStmt3.setCsDt(sqlDate);
										cashStmt3.setCsPayTo(voucherService.getPayTo());
										cashStmt3.setCsBrhFaCode(destBrFaCode);
										cashStmt3.setPayMode(payMode);

										session.save(cashStmt3);

								}
								
						
 							  }
							}
						
						}
						
					}else{
						map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
						map.put(ConstantsValues.MSG, "Invalid bank");
					}
					
				}else{
					map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
					map.put(ConstantsValues.MSG, "Invalid payment");
				}
				transaction.commit();
				map.put(ConstantsValues.RESULT, ConstantsValues.SUCCESS);
			} catch(Exception e) {
				e.printStackTrace();
				transaction.rollback();
				map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
				map.put(ConstantsValues.MSG, "Rent detail not found.");
			}
			session.close();
		}else{
			map.put(ConstantsValues.RESULT, ConstantsValues.ERROR);
			map.put(ConstantsValues.MSG, "Rent detail not found.");
		}
		return map;
	}

	
	@SuppressWarnings("unchecked")
//	@Transactional
	@Override
	public Map<String, Object> updateCSSForTelV(Session session, int cssId, CashStmt cashStmt) {
		System.out.println("enter into updateCSSForTelV function--->" + cssId);
		System.out.println("cashStmt tvNo = " + cashStmt.getCsTvNo());
		List<CashStmtStatus> cssList = new ArrayList<CashStmtStatus>();
		String bCode = cashStmt.getbCode();
		Map<String, Object> map = new HashMap<String, Object>();
//		try {
			Criteria cr1 = session.createCriteria(CashStmtStatus.class);
			cr1.add(Restrictions.eq(CashStmtStatusCNTS.CSS_ID, cssId));
			cssList = cr1.list();

			if (!cssList.isEmpty()) {
				CashStmtStatus cashStmtStatus = cssList.get(0);
				cashStmt.setCsNo(cashStmtStatus);
				java.sql.Date sqlDate = new java.sql.Date(cashStmtStatus
						.getCssDt().getTime());

				Criteria cr = session.createCriteria(CashStmt.class);
				cr.add(Restrictions.eq(CashStmtCNTS.CS_CDT, sqlDate));
				cr.add(Restrictions.eq(CashStmtCNTS.USER_BRANCH_CODE, bCode));
				ProjectionList proList = Projections.projectionList();
				proList.add(Projections.property("csVouchNo"));
				cr.setProjection(proList);

				List<Integer> voucherNoList = cr.list();

				System.out.println("todays cashStmt list = "
						+ voucherNoList.size());
				if (cashStmt.getCsVouchNo() > 0) {
					map.put("vhNo", cashStmt.getCsVouchNo());
					cashStmtStatus.getCashStmtList().add(cashStmt);
					session.update(cashStmtStatus);
				} else {
					if (!voucherNoList.isEmpty()) {
						
						int voucherNo = 0;
						for(int i=0;i<voucherNoList.size();i++){
							if(voucherNo < voucherNoList.get(i)){
								voucherNo = voucherNoList.get(i);
							}
						}
						voucherNo = voucherNo + 1;
						map.put("vhNo", voucherNo);
						cashStmt.setCsVouchNo(voucherNo);
						cashStmtStatus.getCashStmtList().add(cashStmt);
						session.update(cashStmtStatus);
					} else {
						cashStmt.setCsVouchNo(1);
						map.put("vhNo", 1);
						cashStmtStatus.getCashStmtList().add(cashStmt);
						session.update(cashStmtStatus);
					}
				}
			} else {
				map.put("vhNo", 0);
			}
//		} catch (Exception e) {
//			e.printStackTrace();
//			map.put("vhNo", -1);
//		}
		return map;
	}
	
	@SuppressWarnings("unchecked")
//	@Transactional
	@Override
	public int updateCSSBySFId(Session session, int csId, int csfId) {
		System.out.println("enter into updateCSSBySFId function");
		int result = 0;
		List<CashStmt> csList = new ArrayList<CashStmt>();
//		try {
			Criteria cr1 = session.createCriteria(CashStmt.class);
			cr1.add(Restrictions.eq(CashStmtCNTS.CS_ID, csId));
			csList = cr1.list();
			CashStmt cashStmt = csList.get(0);
			cashStmt.setCsSFId(csfId);
			session.update(cashStmt);

			result = 1;
//		} catch (Exception e) {
//			e.printStackTrace();
//			result = -1;
//		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public int saveCsTemp(Session session, CashStmtTemp cashStmtTemp) {
		System.out.println("enter into saveCsTemp function--->");
		int res = 0;
	//	try {
			session.save(cashStmtTemp);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
		return res;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<BankCS> getBankCsByDt(Session session, Integer userId, Date date, ArrayList<Integer> brhId){
		logger.info("Enter into getBankCsByDt() :UserID = "+userId+" : Date = "+date+" : BranchId = "+brhId);
		List<BankCS> bcsList = new ArrayList<>();
		try{
			Criteria cr = session.createCriteria(BankCS.class);
			cr.add(Restrictions.in(BankCSCNTS.BCS_BRH_ID, brhId));
			cr.add(Restrictions.eq(BankCSCNTS.BCS_DT, date));
		List<BankCS> bcs = cr.list();
		
		for (BankCS bankCS : bcs) {
			if(bankCS.getBcsBrhId()==1)
			{
				if(bankCS.getBcsBankId()==3 || bankCS.getBcsBankId()==10 || bankCS.getBcsBankId()==44 || bankCS.getBcsBankId()==46 || bankCS.getBcsBankId()==47)
				{
					bcsList.add(bankCS);	
				}
			}
			else
			{
				bcsList.add(bankCS);
			}
		}
		
		
		
			logger.info("UserID = "+userId+" : BankList Size = "+bcsList.size());
		}catch(Exception e){
			logger.error("UserID = "+userId+" : Exception = "+e);
		}
		logger.info("Exit from getBankCsByDt() UserID = "+userId);
		return bcsList;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<BankCS> getBankCsForGurgaon(){
	
		Session session=this.sessionFactory.openSession();
		List<BankCS> bcsList = null;
		try{
			Criteria cr = session.createCriteria(BankCS.class);
			cr.add(Restrictions.eq(BankCSCNTS.BCS_BRH_ID, 1));
			bcsList = cr.list();
			
			if(!bcsList.isEmpty())
			{
				Comparator<BankCS> comparator=new Comparator<BankCS>() {
					
					@Override
					public int compare(BankCS arg0, BankCS arg1) {
						// TODO Auto-generated method stub
						if(arg0.getBcsId()==arg1.getBcsId())  
							return 0;  
							else if(arg0.getBcsId()>arg1.getBcsId())  
							return 1;  
							else  
							return -1;  
					}
				};
			Collections.sort(bcsList, comparator);
				
			}

			session.close();
		}catch(Exception e){

		}

		return bcsList;
	}
	
	
}