package com.mylogistics.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mylogistics.DAO.BranchDAO;
import com.mylogistics.DAO.ElectricityMstrDAO;
import com.mylogistics.DAO.EmployeeDAO;
import com.mylogistics.model.Branch;
import com.mylogistics.model.ElectricityMstr;
import com.mylogistics.model.Employee;
import com.mylogistics.model.User;
import com.mylogistics.services.ConstantsValues;
import com.mylogistics.services.EMstrService;
import com.mylogistics.services.TPMstrService;

@Controller
public class CRNAllotCntlr {
	
	@Autowired
	private BranchDAO branchDAO;
	
	@Autowired
	private EmployeeDAO employeeDAO;
	
	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private ElectricityMstrDAO electricityMstrDAO;
	
	@RequestMapping(value = "/getBrListFrCrnA" , method = RequestMethod.POST)  
	public @ResponseBody Object getBrListFrCrnA() {  
		System.out.println("Enter into getBrListFrCrnA---->");
		Map<String, Object> map = new HashMap<>();	
		List<Branch> branchList = branchDAO.getAllActiveBranches();
		if(!branchList.isEmpty()){
			map.put("brList",branchList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/getEmpListFrCrnA" , method = RequestMethod.POST)  
	public @ResponseBody Object getEmpListFrCrnA() {  
		System.out.println("Enter into getEmpListFrCrnA---->");
		Map<String, Object> map = new HashMap<>();	
		List<Employee> empList = employeeDAO.getAllActiveEmployees();
		if(!empList.isEmpty()){
			map.put("empList",empList);
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/saveEms" , method = RequestMethod.POST)  
	public @ResponseBody Object saveEms(@RequestBody EMstrService eMstrService) {  
		System.out.println("Enter into saveEms---->");
		Map<String, Object> map = new HashMap<String, Object>();	
		User currentUser = (User)httpSession.getAttribute("currentUser");
		ElectricityMstr electricityMstr = eMstrService.getElectricityMstr();
		Branch branch = eMstrService.getBranch();
		Employee employee = eMstrService.getEmployee();

		electricityMstr.setbCode(currentUser.getUserBranchCode());
		electricityMstr.setUserCode(currentUser.getUserCode());
		
		int temp = electricityMstrDAO.saveElecMstr(branch, employee, electricityMstr);
		if(temp > 0){
			map.put(ConstantsValues.RESULT,ConstantsValues.SUCCESS);
		}else{
			map.put(ConstantsValues.RESULT,ConstantsValues.ERROR);
		}
		return map;
	}	
}
